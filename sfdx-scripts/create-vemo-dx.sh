#!/bin/sh 
sfdx force:mdapi:deploy -d vemo-mdapi-preload/unpackaged/ -w 30
sfdx force:package:install --package 04t60000000U7orAAC -r -w 30
sfdx force:package:install --package 04t70000000OPVWAA4 -r -w 30
sfdx force:source:push --forceoverwrite
sfdx force:user:permset:assign --permsetname DXAdmin

sfdx force:apex:execute -f config/set-UserRole.txt
sfdx force:apex:execute -f config/create-new-scratch-org.txt

sfdx force:apex:execute -f config/insert-NewData.txt