sfdx force:user:permset:assign --permsetname DXAdmin
sfdx force:apex:execute -f config/set-UserRole.txt
sfdx force:apex:execute -f config/create-new-scratch-org.txt
sfdx force:apex:execute -f config/insert-NewData.txt