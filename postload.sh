#!/bin/sh 
sfdx force:mdapi:retrieve -r ./vemo-mdapi-postload -u vemo-prod -k ./vemo-mdapi-postload/package.xml
cd vemo-mdapi-postload
unzip unpackaged.zip
rm unpackaged/layouts/'Campaign-Campaign Layout.layout'
rm unpackaged/layouts/'CollaborationGroup-Group Layout.layout'
rm unpackaged/layouts/'Lead-Lead Layout.layout'
rm unpackaged/layouts/'ProfileSkill-Skill Layout.layout'
rm unpackaged/layouts/'ProfileSkillEndorsement-Endorsement Layout.layout'
rm unpackaged/layouts/'ProfileSkillUser-Skill User Layout.layout'
rm unpackaged/layouts/'SocialPost-Social Post Layout.layout'
rm unpackaged/layouts/'WorkBadgeDefinition-Badge Definition Layout.layout'
rm unpackaged/layouts/'WorkBadgeDefinition-Badge Layout 192.layout'

sfdx force:mdapi:deploy -d unpackaged/ -w 30
rm -R unpackaged
rm unpackaged.zip
cd ..
