/////////////////////////////////////////////////////////////////////////
// Trigger: EmploymentHistoryTrigger
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-10-18   Ranjeet Kumar   Created                                 
// 
/////////////////////////////////////////////////////////////////////////
trigger EmploymentHistoryTrigger on EmploymentHistory__c (after delete, after insert, after undelete, 
                              after update, before delete, before insert, before update) {
	//if(TriggerSettings.getSettings().EmploymentHistoryTrigger) {
    //    TriggerDispatch.TriggerContext tc = new TriggerDispatch.TriggerContext(Trigger.isExecuting, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete,
    //                                                                            Trigger.isBefore, Trigger.isAfter, Trigger.isUndelete,
    //                                                                            Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.size, 'EmploymentHistoryTriggerHandler');
    //    TriggerDispatch.dispatchTriggerHandler(tc);
    //}
}