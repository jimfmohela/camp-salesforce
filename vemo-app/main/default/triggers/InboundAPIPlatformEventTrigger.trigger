trigger InboundAPIPlatformEventTrigger on InboundAPI__e (after insert) {
    List<InboundAPILog__c> logs = new List<InboundAPILog__c>();
    InboundAPILog__c log = new InboundAPILog__c();
    for(InboundAPI__e pe : (List<InboundAPI__e>)Trigger.new){

        log.Request__c = pe.Request__c != null ? pe.Request__c.left(131072) : '';
        log.Response__c = pe.Response__c != null ? pe.Response__c.left(131072) : '';
        log.Endpoint__c = pe.Endpoint__c != null ? pe.Endpoint__c.left(255) : '';
        log.HTTPMethod__c = pe.HTTPMethod__c != null ? pe.HTTPMethod__c.left(10) : '';
        log.RequestBody__c = pe.RequestBody__c != null ? pe.RequestBody__c.left(131072) : '';
        log.ResponseBody__c = pe.ResponseBody__c != null ? pe.ResponseBody__c.left(131072) : '';
        log.StatusCode__c = pe.StatusCode__c != null ? pe.StatusCode__c : null;
        logs.add(log);
    }
    insert logs;
}