//////////////////////////////////////////////////////////////////////////
// Trigger: ReconciliationTrigger
//
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-06-27   Greg Cook       Created
//
/////////////////////////////////////////////////////////////////////////
trigger ReconciliationTrigger on Reconciliation__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {

    if(TriggerSettings.getSettings().reconciliationTrigger) {
        TriggerDispatch.TriggerContext tc = new TriggerDispatch.TriggerContext(Trigger.isExecuting, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete,
                Trigger.isBefore, Trigger.isAfter, Trigger.isUndelete,
                Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.size, 'ReconciliationTriggerHandler');
        TriggerDispatch.dispatchTriggerHandler(tc);
    }

}