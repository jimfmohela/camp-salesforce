trigger EmploymentHistoryTrigger2 on EmploymentHistory2__c (after delete, after insert, after undelete, 
                              after update, before delete, before insert, before update) {
	if(TriggerSettings.getSettings().EmploymentHistoryTrigger2) {
        TriggerDispatch.TriggerContext tc = new TriggerDispatch.TriggerContext(Trigger.isExecuting, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete,
                                                                                Trigger.isBefore, Trigger.isAfter, Trigger.isUndelete,
                                                                                Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.size, 'EmploymentHistoryTriggerHandler2');
        TriggerDispatch.dispatchTriggerHandler(tc);
    }
}