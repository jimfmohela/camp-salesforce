trigger InvestorOwnershipTrigger on InvestorOwnership__c (after delete, after insert, after undelete, 
                              after update, before delete, before insert, before update) {
	if(TriggerSettings.getSettings().investorOwnershipTrigger) {
        TriggerDispatch.TriggerContext tc = new TriggerDispatch.TriggerContext(Trigger.isExecuting, Trigger.isInsert, Trigger.isUpdate, Trigger.isDelete,
                                                                                Trigger.isBefore, Trigger.isAfter, Trigger.isUndelete,
                                                                                Trigger.new, Trigger.newMap, Trigger.old, Trigger.oldMap, Trigger.size, 'InvestorOwnershipTriggerHandler');
        TriggerDispatch.dispatchTriggerHandler(tc);
    }
}