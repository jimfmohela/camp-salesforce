trigger AgreementStatusChangeTrigger on AgreementStatusChange__e (after insert) {
    if(TriggerSettings.getSettings().agreementStatusChangeTrigger) {
        List<AgreementStatusChange__e> AgreementStatusChangeList = new List<AgreementStatusChange__e>();
        
        for(AgreementStatusChange__e aschange : Trigger.new){
            AgreementStatusChangeList.add(aschange);
        }
        AgreementStatusChangeTriggerHandler ascHandler = new AgreementStatusChangeTriggerHandler();
        ascHandler.processAgreementStatusChange(AgreementStatusChangeList);
        
        LogService.writeLogs();
    }
}