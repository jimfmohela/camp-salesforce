<apex:page id="loginPage" showHeader="false" applyHtmlTag="false" applyBodyTag="false" docType="html-5.0" controller="SiteLoginController">
    
    <apex:stylesheet value="{!URLFOR($Resource.VemoCommunity, '/VemoCommunity/css/bulma.min.css')}"/>
    <apex:stylesheet value="{!URLFOR($Resource.VemoCommunity, '/VemoCommunity/css/main.css')}"/>
    <apex:stylesheet value="https://use.typekit.net/zcj5bea.css"/>
    <apex:includeScript value="{!URLFOR($Resource.VemoCommunity, '/VemoCommunity/js/just-validate.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.VemoCommunity, '/VemoCommunity/js/fullStory.js')}" />

    <html>
        <head>
            <meta charset="utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <title>Vemo School App</title>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
        </head>
        <body>
            <apex:form >
                <apex:actionFunction name="loginAF" action="{!login}" reRender="false">
                    <apex:param name="usernameJS" value="" />
                    <apex:param name="passwordJS" value="" />
                </apex:actionFunction>
            </apex:form>
            <section class="is-fullheight">
                <div class="hero-body">
                    <div class="container has-text-centered">
                        <!--div class="column is-4 is-offset-4"-->
                            <div class="vemo-logo"></div>
                            <div id="incorrectCredentialMsg" class="app-error" style="display:none;">
                                <span><i class="app-icon icon-error"></i></span>
                                <div class="error-msg">
                                    Sorry, your username or password was entered incorrectly.
                                </div>
                            </div>  
                            <div class="box">
                                <h3 class="app-title">Well hello you!</h3>
                                <p class="app-subtitle"><span>Please enter in your username &amp; password below to sign into your School Admin Account</span></p>
                                <form class="js-form form">
                                    <div class="field">
                                        <div class="control has-icons-left">
                                            <input class="input is-large form__input form-control" type="email" placeholder="Username here" autocomplete="off" data-validate-field="email" name="email" id="email"/>
                                            <span class="icon is-small is-left">
                                                <i class="app-icon icon-email-fill"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="control has-icons-left has-icons-right">
                                            <input class="input is-large fs-block" type="password" placeholder="Password here" autocomplete="off" data-validate-field="password" name="password" id="password"/>
                                            <span class="icon is-small is-right">
                                                <i class="app-icon icon-visible" id="icon-visible"></i>
                                            </span>
                                            <span class="icon is-small is-left">
                                                <i class="app-icon icon-password-fill"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <button class="button is-large is-uppercase">Sign In</button>
                                </form>
                                <a class="forgot-password" href="{!$Page.SchoolAdminForgotPassword}">{!$Label.site.forgot_your_password_q}</a> 
                                <div class="terms">
                                    <a href="{!$Label.VemoTermsofUse}">Terms of Use</a> 
                                    <span class="divider">|</span>
                                    <a href="{!$Label.VemoPrivacyPolicy}">Privacy Policy</a>
                                </div>
                                <div class="copy-right"></div>                    
                            </div>
                        <!--/div-->
                    </div>
                </div>
            </section>
        </body>
        <script type="text/javascript">
         const Dates = {
            getYear: () => {
            return new Date().getFullYear()
            }
        }
        document.getElementsByClassName('copy-right')[0].innerHTML = `&copy; ${Dates.getYear()} Vemo Education`

        let passwordElem = document.getElementById('password');
        let iconVisElem = document.getElementById('icon-visible');
        let errorMsgElem = document.getElementById("incorrectCredentialMsg");
        if(errorMsgElem !== undefined){
            errorMsgElem.style.display = "none";
        }
        
        iconVisElem.addEventListener('click', function(){
            if(passwordElem.type === 'password') {
                passwordElem.type = 'text';
                this.classList.add('active');
            } else {
                passwordElem.type = 'password';
                this.classList.remove('active');
            }
        });
        
        new window.JustValidate('.js-form', {
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    strength: {
                        default: true
                    },
                    password: true,
                    minLength: 1,
                    maxLength: 20
                }
            },
            messages: {
                email: 'Please enter your username !',
                password: 'Please enter your password !'
            },
            submitHandler: function (form, values, ajax) {
                //console.log("values");
                //console.log(values);
                Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.SiteLoginController.login}', JSON.stringify(values), function(result, event){
                    if (event.status) {
                        //console.log("Server Response");
                        //console.log(result);
                        if(result.isSuccess == true){
                            //console.log(values.email, values.password);
                            //console.log(result.result);
                            //window.location.href = result.result;
                            loginAF(values.email, values.password);
                        }
                        else {
                            console.log("Login failed with message - " + result.result);
                            if(errorMsgElem !== undefined){
                                errorMsgElem.style.display = "block";
                            }
                        }
                    } 
                    else {
                        console.log("Exception");
                        console.log(event.message);
                    }
                }, 
                    {escape: true}
                );
            }
        });
        </script>
    </html>
    
</apex:page>