import { LightningElement, api, wire } from 'lwc';
import MOHELA_Logo from '@salesforce/resourceUrl/MOHELA_Logo';
import knowledge_finance_logo from '@salesforce/resourceUrl/knowledge_finance_logo';

import getRecord from 'lightning/uiRecordApi';

export default class ServicerInformation extends LightningElement {
    @api recordId;
}