public class VemoAPIEventTypeResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
        }
    }
  
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventTypeResource.handleGetV2()');
        String eventTypeIDparam = api.params.get('eventTypeID');
        
        List<EventTypeService.EventType> eventTypeList = new List<EventTypeService.EventType>();
        if(eventTypeIDparam != null){
            eventTypeList = EventTypeService.getEventTypeWithEventTypeID(VemoApi.parseParameterIntoIDSet(eventTypeIDparam));
        }
        else{
            eventTypeList = EventTypeService.getAllEventTypesMap();
        }
        List<EventTypeResourceOutputV2> results = new List<EventTypeResourceOutputV2>();
        for(EventTypeService.EventType eType : eventTypeList){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoEventTypeResourceOutputV2(eType));
            }else{
                results.add(new PublicEventTypeResourceOutputV2(eType));
            }
        }     
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public virtual class EventTypeResourceOutputV2{
        public String eventTypeID{get;set;}
        public String name{get;set;}
        public String description{get;set;}
    
        public EventTypeResourceOutputV2(){}

        public EventTypeResourceOutputV2(EventTypeService.EventType et){
            this.eventTypeID = et.eventTypeID;
            this.name = et.Name;
            this.description = et.description;
        }
    }
    
    public class PublicEventTypeResourceOutputV2 extends EventTypeResourceOutputV2{
        public PublicEventTypeResourceOutputV2(){}
        public PublicEventTypeResourceOutputV2(EventTypeService.EventType eType){
            super(eType);
        }
    }

    public class VemoEventTypeResourceOutputV2 extends EventTypeResourceOutputV2{
        public string mergeObject{get;set;}
        public string mergeTemplate{get;set;}
        public string mergeText{get;set;}
        
        public VemoEventTypeResourceOutputV2(){}        
        public VemoEventTypeResourceOutputV2(EventTypeService.EventType eType){
            super(eType);
            this.mergeObject = eType.mergeObject;
            this.mergeTemplate = eType.mergeTemplate;
            this.mergeText = eType.mergeText;
        }
    }
}