public with sharing class LogBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts{
    public enum JobType {PURGE} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    public LogBatch(){}
       
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('StudentAccountBatch.start()');
        if(job == JobType.PURGE){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Log__c where createdDate < YESTERDAY';
            }
        } 
        
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('LogBatch.execute()');
        
        if(job == JobType.PURGE){
            handlePurgeDeleted((List<Log__c>)scope);    
        }    
    }
    
    private static void handlePurgeDeleted(List<Log__c> logs){
        Database.delete(logs, false);
    }
   
    public void finish(Database.BatchableContext BC) {
        System.debug('LogBatch.finish()');
    }
}