/////////////////////////////////////////////////////////////////////////
// Class: DefermentQueries_TEST
// 
// Description: 
//  Unit test for DefermentQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-09-25   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class DefermentQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testGetDefermentMapWithDefermentId(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Test.startTest();
        Map<Id, Deferment__c> resultDefMap = DefermentQueries.getDefermentMapWithDefermentID(testDefMap.keySet());
        System.assertEquals(testDefMap.keySet().size(), resultDefMap.keySet().size());
        Test.stopTest();
    }
    static testMethod void getDefermentMapWithStudentIdTest(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Test.startTest();
        Map<Id, Deferment__c> resultDefMap = DefermentQueries.getDefermentMapWithStudentID(testDefMap.keySet());
        System.assertEquals(testDefMap.keySet().size(), resultDefMap.keySet().size());
        Test.stopTest();
    }
}