/////////////////////////////////////////////////////////////////////////
// Class: ReconciliationTriggerHandler
//
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-07-28   Greg Cook       Created
//
/////////////////////////////////////////////////////////////////////////
public with sharing class ReconciliationTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {

    /**************************Static Variables***********************************/

    /**************************State Control Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;

    /**************************Constructors**********************************************/

    /**************************Execution Control - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.TriggerContext tc){
        mainHasRun = true;

        if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);

        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);


    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.TriggerContext tc){
        inProgressHasRun = true;

        //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'ReconciliationTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);

        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'ReconciliationTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            TriggerDispatch.forwardTrigger(tc, this);
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeInsert(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onBeforeInsert()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> newReconciliationList = (List<Reconciliation__c>)tc.newList;
        //This is where you should call your business logic

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onBeforeUpdate()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> newReconciliationList = (List<Reconciliation__c>)tc.newList;
        List<Reconciliation__c> oldReconciliationList = (List<Reconciliation__c>)tc.oldList;
        Map<ID, Reconciliation__c> newReconciliationMap = (Map<ID, Reconciliation__c>)tc.newMap;
        Map<ID, Reconciliation__c> oldReconciliationMap = (Map<ID, Reconciliation__c>)tc.oldMap;
        //This is where you should call your business logic
        List<Reconciliation__c> processedReconciliationList = processForNewOutboundEmails(newReconciliationList, oldReconciliationMap);
        if(processedReconciliationList != null && processedReconciliationList.size() > 0){
            handleBeforeUpdate(processedReconciliationList);
        }
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onBeforeDelete()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> oldReconciliationList = (List<Reconciliation__c>)tc.oldList;
        Map<ID, Reconciliation__c> oldReconciliationMap = (Map<ID, Reconciliation__c>)tc.oldMap;
        //This is where you should call your business logic

    }

    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onAfterInsert()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> newReconciliationList = (List<Reconciliation__c>)tc.newList;
        Map<ID, Reconciliation__c> newReconciliationMap = (Map<ID, Reconciliation__c>)tc.newMap;
        //This is where you should call your business logic
        List<Reconciliation__c> processedReconciliationList = processForNewOutboundEmails(newReconciliationList, null);
        if(processedReconciliationList != null && processedReconciliationList.size() > 0){
            handleAfterInsert(processedReconciliationList);
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onAfterUpdate()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> newReconciliationList = (List<Reconciliation__c>)tc.newList;
        List<Reconciliation__c> oldReconciliationList = (List<Reconciliation__c>)tc.oldList;
        Map<ID, Reconciliation__c> newReconciliationMap = (Map<ID, Reconciliation__c>)tc.newMap;
        Map<ID, Reconciliation__c> oldReconciliationMap = (Map<ID, Reconciliation__c>)tc.oldMap;
        //This is where you should call your business logic

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterDelete(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onAfterDelete()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> oldReconciliationList = (List<Reconciliation__c>)tc.oldList;
        Map<ID, Reconciliation__c> oldReconciliationMap = (Map<ID, Reconciliation__c>)tc.oldMap;
        //This is where you should call your business logic

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.TriggerContext tc){
        system.debug('ReconciliationTriggerHandler.onAfterUndelete()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<Reconciliation__c> newReconciliationList = (List<Reconciliation__c>)tc.newList;
        Map<ID, Reconciliation__c> newReconciliationMap = (Map<ID, Reconciliation__c>)tc.newMap;
        //This is where you should call your business logic

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnInsert
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnInsert(List<Reconciliation__c> newReconciliationList){
        system.debug('ReconciliationTriggerHandler.setDefaultsOnInsert()');
        //Set here defaults on Insert
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnUpdate
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnUpdate(Map<ID, Reconciliation__c> oldReconciliationMap, Map<ID, Reconciliation__c> newReconciliationMap){
        system.debug('ReconciliationTriggerHandler.setDefaultsOnUpdate()');
        //Set here defaults on update
    }

    private void handleAfterInsert(List<Reconciliation__c> reconciliationList){
        if(reconciliationList != null && reconciliationList.size() > 0){
            Set<Id> reconciliationIds = new Set<Id>();
            for(Reconciliation__c reconciliatn : reconciliationList){
                reconciliationIds.add(reconciliatn.Id);
            }
            Map<Id, Reconciliation__c> reconciliationMap = ReconciliationQueries.getReconciliationMapWithReconciliationID(reconciliationIds);
            if(reconciliationMap != null && reconciliationMap.size() > 0){
                for(Reconciliation__c reconciliatn : reconciliationMap.values()){
                    reconciliatn.SendReconciliationSummary__c = false;
                }
                update reconciliationMap.values();
            }
        }
    }

    private void handleBeforeUpdate(List<Reconciliation__c> reconciliationList){
        if(reconciliationList != null && reconciliationList.size() > 0){
            for(Reconciliation__c reconciliatn : reconciliationList){
                reconciliatn.SendReconciliationSummary__c = false;
            }
        }
    }

    /*
        Method - processForNewOutboundEmails - This method will filter the record as per requirement for New Outbound Email.
   */

    private List<Reconciliation__c> processForNewOutboundEmails(List<Reconciliation__c> newReconciliationList, Map<Id, Reconciliation__c> reconciliationOldMap){
        List<Reconciliation__c> filteredReconciliatnList = new List<Reconciliation__c>();
        List<Reconciliation__c> processedReconciliationList = new List<Reconciliation__c>();
        if(newReconciliationList != null && newReconciliationList.size() > 0){
            for(Reconciliation__c reconciliatn : newReconciliationList){
                if(reconciliatn.SendReconciliationSummary__c && reconciliatn.Email__c != null){
                    if(reconciliationOldMap == null){
                        filteredReconciliatnList.add(reconciliatn);
                    }
                    else {
                        if(reconciliationOldMap.containsKey(reconciliatn.Id) && !reconciliationOldMap.get(reconciliatn.Id).SendReconciliationSummary__c){
                            filteredReconciliatnList.add(reconciliatn);
                        }
                    }
                }
            }
        }
        if(filteredReconciliatnList.size() > 0){
            processedReconciliationList = createAndInsertReqOutboundEmails(filteredReconciliatnList);
        }
        return processedReconciliationList;
    }

    /*
        Method createAndInsertReqOutboundEmails - This method will create a new Outboundemail per Reconciliation to send email for ReconciliationSummary.
    */

    private List<Reconciliation__c> createAndInsertReqOutboundEmails(List<Reconciliation__c> newReconciliationList){
        List<Reconciliation__c> processedReconciliationList = new List<Reconciliation__c>();
        if(newReconciliationList != null && newReconciliationList.size() > 0){
            List<EmailTemplate> emailTemplateList = new List<EmailTemplate>();
            emailTemplateList = [Select Id, Name, DeveloperName From EmailTemplate Where DeveloperName = 'ReconciliationSummary'];
            if(emailTemplateList.size() > 0){
                List<OutboundEmail__c> outboundEmailListToInsert = new List<OutboundEmail__c>();
                for(Reconciliation__c reconciliatn : newReconciliationList){
                    OutboundEmail__c outboundEmail = new OutboundEmail__c ();
                    outboundEmail.ToAddresses__c = reconciliatn.Email__c ;
                    outboundEmail.TemplateID__c = emailTemplateList[0].Id ;
                    outboundEmail.WhatID__c = reconciliatn.Id;
                    outboundEmail.SendviaSES__c = true ;
                    outboundEmailListToInsert.add(outboundEmail);
                    processedReconciliationList.add(reconciliatn);
                }
                if(outboundEmailListToInsert.size() > 0){
                    insert outboundEmailListToInsert;
                }
            }
        }
        return processedReconciliationList;
    }

    public class ReconciliationTriggerHandlerException extends Exception {}
}