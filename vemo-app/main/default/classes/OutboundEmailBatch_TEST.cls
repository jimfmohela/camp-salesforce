@isTest
public class OutboundEmailBatch_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
    	DatabaseUtil.setRunQueriesInMockingMode(false);
		dbUtil.queryExecutor = new UserContext(); 
        TestUtil.createStandardTestConditions();
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'SendEmail_Test_Template'});
    }

    static testMethod void sendEmailTest_WithoutTemplateId(){
		
		DatabaseUtil.setRunQueriesInMockingMode(false);
		dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        students.values()[0].PersonEmail = 'outboundemailbatchtest@test.com';
        students.values()[0].DoNotContact__c = true;
        update students.values();
        //dbUtil.updateRecords(students.values());

        OutboundEmail__c oEmail = new OutboundEmail__c();
        oEmail.SendviaSES__c = true;
        oEmail.WhatId__c = students.values()[0].Id;
        oEmail.ToAddresses__c = 'outboundemailbatchtest@test.com';
        oEmail.PlainTextBody__c = 'This is test email plain body.';
        oEmail.subject__c = 'Test - OutboundEmailBatch_TEST';
        insert oEmail;
        //dbUtil.insertRecord(oEmail);

        List<Attachment> AttList = new List<Attachment>();
        AttList = TestDataFactory.createAttachments(2, oEmail.Id);
        insert AttList;
        //dbUtil.insertRecords(attList);
        
        Test.startTest();
        OutboundEmailBatch oeb = new OutboundEmailBatch();
        oeb.job = OutboundEmailBatch.JobType.SEND_EMAIL;
        Database.executeBatch(oeb, 200);
        Test.stopTest();
    }

    static testMethod void sendEmailTest_WithTemplateId(){
		
		DatabaseUtil.setRunQueriesInMockingMode(false);
		dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        students.values()[0].PersonEmail = 'outboundemailbatchtest@test.com';
        students.values()[0].DoNotContact__c = true;
        update students.values();
        //dbUtil.updateRecords(students.values());

        List<EmailTemplate> availableEmailTemplates = new List<EmailTemplate>();
        availableEmailTemplates = TestUtil.getEmailTemplatesByName(new Set<String>{'SendEmail_Test_Template'});

        OutboundEmail__c oEmail = new OutboundEmail__c();
        oEmail.SendviaSES__c = true;
        oEmail.WhatId__c = students.values()[0].Id;
        oEmail.ToAddresses__c = 'outboundemailbatchtest@test.com';
        if(availableEmailTemplates.size() > 0){
            oEmail.TemplateId__c = availableEmailTemplates[0].Id;
        }
        oEmail.subject__c = 'Test - OutboundEmailBatch_TEST';
        insert oEmail;
        //dbUtil.insertRecord(oEmail);
        
        //OutboundEmailService createOutboundEmailV1TestTest
        List<OutboundEmailService.OutboundEmail> outboundEmails = new List<OutboundEmailService.OutboundEmail>();
		OutboundEmailService.OutboundEmail email = new OutboundEmailService.OutboundEmail();
		email.toAddresses = 'outboundemailbatchtest@test.com';
		email.templateDevName = 'SendEmail_Test_Template';
		email.whatID = students.values()[0].Id;
		email.targetObjectId = students.values()[0].PersonContactID;
		email.sendImmediate = true;
		outboundEmails.add(email);
		OutboundEmailService.createOutboundEmailV1(outboundEmails);

        Test.startTest();
        OutboundEmailBatch oeb = new OutboundEmailBatch();
        oeb.job = OutboundEmailBatch.JobType.SEND_EMAIL;
        Database.executeBatch(oeb, 200);
        Test.stopTest();
    }
}