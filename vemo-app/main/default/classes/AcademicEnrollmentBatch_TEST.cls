@isTest
public with sharing class AcademicEnrollmentBatch_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest static void RecalculateAcademicEnrollment_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        user userwithrole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        system.runAs(userWithRole){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        //Map<ID, Program__c> testPrograms = TestDataFactory.createAndInsertPrograms(TestUtil.TEST_THROTTLE, schools);
        //Map<Id, StudentProgram__c> testStudentProgramMap = TestDataFactory.createAndInsertStudentProgram(TestUtil.TEST_THROTTLE,testStudentAccountMap,testprograms);
        
        Test.startTest();
        AcademicEnrollmentBatch job = new AcademicEnrollmentBatch(testAcademicEnrollmentMap.keyset());
        job.job = AcademicEnrollmentBatch.JobType.RECALCULATE_ACADEMICENROLLMENT;
        Database.executeBatch(job, 200);        
        Test.stopTest();
        }
    }
}