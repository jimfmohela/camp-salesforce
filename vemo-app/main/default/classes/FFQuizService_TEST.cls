@isTest
public class FFQuizService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    @isTest public static void validateGetFFQuizAttemptMapWithStudentProgramID(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        
        Test.startTest();
        List<FFQuizService.FFQuizAttempt> quizAttemptList = FFQuizService.getFFQuizAttemptWithStudentProgramID(studentPrgMap.keySet());
        System.assertEquals(quizAttemptMap.size(), quizAttemptList.size());
        Test.stopTest();
    }
    
    @isTest public static void validateCreateFFQuizAttempt(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        List<FFQuizService.FFQuizAttempt> quizAttemptList = FFQuizService.getFFQuizAttemptWithStudentProgramID(studentPrgMap.keySet());
        
        List<FFQuizService.FFQuizAttempt> newQuizAttemptList = new List<FFQuizService.FFQuizAttempt>();
        for(FFQuizService.FFQuizAttempt f : quizAttemptList){
            f.fFQuizAttemptID=null;
            newQuizAttemptList.add(f);
        }
        
        Test.startTest();
        Set<ID> updatedIDs = FFQuizService.createFFQuizAttempt(newQuizAttemptList);
        System.assertEquals(quizAttemptMap.size(), quizAttemptList.size());
        Test.stopTest();
    }
    
    @isTest public static void validateUpdateFFQuizAttempt(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        List<FFQuizService.FFQuizAttempt> quizAttemptList = FFQuizService.getFFQuizAttemptWithStudentProgramID(studentPrgMap.keySet());
        
        Test.startTest();        
        Set<ID> updatedIDs = FFQuizService.updateFFQuizAttempt(quizAttemptList);
        System.assertEquals(quizAttemptList.size(), updatedIDs.size());
        Test.stopTest();
    }
    
    @isTest public static void validateDeleteFFQuizAttempt(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        List<FFQuizService.FFQuizAttempt> quizAttemptList = FFQuizService.getFFQuizAttemptWithStudentProgramID(studentPrgMap.keySet());
        
        Set<ID> recordIDs = new Set<ID>();
        for(FFQuizService.FFQuizAttempt f : quizAttemptList){
            recordIDs.add(f.fFQuizAttemptID);
        }
        Test.startTest();        
        Integer deletedRecord = FFQuizService.deleteFFQuizAttempt(recordIDs);
        System.assertEquals(quizAttemptList.size(), deletedRecord);
        Test.stopTest();
    }
    
    @isTest public static void validateGetFFQuizResponseMapWithFFQuizAttemptID(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<Id, FFQuizResponse__c> quizResponseMap = TestDataFactory.createAndInsertQuizResponse(quizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
        
        Test.startTest();
        List<FFQuizService.FFQuizResponse> quizResponseList = FFQuizService.getFFQuizResponseWithFFQuizAttemptID(quizAttemptMap.keySet());
        System.assertEquals(quizResponseMap.size(), quizResponseList.size());
        Test.stopTest();
    }
    
    @isTest public static void validateUpdateFFQuizResponse(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<Id, FFQuizResponse__c> quizResponseMap = TestDataFactory.createAndInsertQuizResponse(quizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
        List<FFQuizService.FFQuizResponse> quizResponseList = FFQuizService.getFFQuizResponseWithFFQuizAttemptID(quizAttemptMap.keySet());
        
        Test.startTest();        
        Set<ID> updatedIDs = FFQuizService.updateFFQuizResponse(quizResponseList);
        System.assertEquals(quizResponseMap.size(), updatedIDs.size());
        Test.stopTest();
    }
}