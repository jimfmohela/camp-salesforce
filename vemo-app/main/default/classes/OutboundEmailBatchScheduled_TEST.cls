@isTest
public with sharing class OutboundEmailBatchScheduled_TEST {
	
    @isTest
    static void sendEmailSchedule_Test(){
        Test.startTest();
        OutboundEmailBatchScheduled job = new OutboundEmailBatchScheduled();
        job.jobType = OutboundEmailBatch.JobType.SEND_EMAIL;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('sendEmailSchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
}