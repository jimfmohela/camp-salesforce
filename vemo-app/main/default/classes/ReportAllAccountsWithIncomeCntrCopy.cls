public with sharing class ReportAllAccountsWithIncomeCntrCopy{
    public List<primaryOwnerWrapper> primaryOwners {get;set;}
    public List<fundOwnerWrapper> fundOwners {get;set;}
    public List<paymentStreamOwnerWrapper> paymentStreamOwners {get;set;}
    public String selectedSchool {get;set;}
    transient public List<reportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    private Integer currentMonth;
    private Integer currentYear;
    transient public String fundOwner {get; set;}
    transient public String primaryOwner {get; set;}
    transient public String paymentStreamOwner {get; set;}
    
    public Set<Id> selectedPrimaryOwners {get;set;}
    public Set<Id> selectedFundOwners {get;set;}
    public Set<Id> selectedPaymentStreamOwners {get;set;}
    //private static Set<String> exclusionStatus = new Set<String>{'Draft','Invited','Application Incomplete','Application Complete','Application Under Review'};  

    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportAllAccountsWithIncomeCntrCopy(){
        selectedSchool = '';
        reportData = new List<reportDataWrapper>();    
        currentMonth = Date.today().month();
        currentYear = Date.today().year();
        primaryOwners = new List<primaryOwnerWrapper>();
        fundOwners = new List<fundOwnerWrapper>();
        paymentStreamOwners = new List<paymentStreamOwnerWrapper>();
        getOwners();
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    public List<SelectOption> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        List<SelectOption> output  = new List<SelectOption>();
        for(Account sch:result){
            output.add(new SelectOption(sch.id,sch.name));    
        }
        return output; 
    }
    
    ///////////////////////////////
    ///Get Owners
    ///////////////////////////////
    public void getOwners(){
        //List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By name ASC];
        Map<ID, Account> acctMap = new Map<ID, Account>();
        string query ='Select id,name From Account ';
        query += ' WHERE ' + generateRecordTypeStatement('Investor');
        query += ' '+ 'Order By name ASC';
        
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        
        for(Account acc: acctMap.values()){
            primaryOwners.add(new primaryOwnerWrapper(false,acc));
            fundOwners.add(new fundOwnerWrapper(false,acc));
            paymentStreamOwners.add(new paymentStreamOwnerWrapper(false,acc));    
        }
        primaryOwners.add(new primaryOwnerWrapper(false,null));
        fundOwners.add(new fundOwnerWrapper(false,null));
        paymentStreamOwners.add(new paymentStreamOwnerWrapper(false,null));
        system.debug('primary Owners: '+primaryOwners);
        //return primaryOwners;
    }
    
    
    
    private static String generateRecordTypeStatement(String recordTypeLabel){
        ID recordTypeID = (String)GlobalUtil.getRecordTypeIDByLabelName('Account', recordTypeLabel);
        return 'RecordTypeID = \''+ String.valueOf(recordTypeID) + '\'';
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        primaryOwner = '';
        fundOwner = '';
        paymentStreamOwner = '';
        selectedPrimaryOwners = new Set<Id>();
        for(primaryOwnerWrapper po:primaryOwners){
            if(po.selected == true){
                if(po.primaryOwner != null){
                    selectedPrimaryOwners.add(po.primaryOwner.id);
                    primaryOwner += po.primaryOwner.Name +',';
                }
                else{
                    selectedPrimaryOwners.add(null);
                }
            }
        }
        primaryOwner = primaryOwner.removeEnd(',');
        selectedfundOwners = new Set<Id>();
        for(fundOwnerWrapper fo:fundOwners){
            if(fo.selected == true){ 
                if(fo.fundOwner != null){
                        selectedFundOwners.add(fo.fundOwner.id);
                        fundOwner += fo.fundOwner.Name +',';
                }
                else{
                    selectedFundOwners.add(null);
                }
            }
        }
        fundOwner = fundOwner.removeEnd(',');
        selectedPaymentStreamOwners = new Set<Id>();
        for(paymentStreamOwnerWrapper pso:paymentStreamOwners){
            if(pso.selected == true){
                if(pso.paymentStreamOwner != null){
                        selectedPaymentStreamOwners.add(pso.paymentStreamOwner.id);
                        paymentStreamOwner += pso.paymentStreamOwner.Name +',';
                }
                else{
                    selectedPaymentStreamOwners.add(null);
                }
            }
        }
        paymentStreamOwner = paymentStreamOwner.removeEnd(',');
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //Final output Map 
        Map<String,ReportDataWrapper> outputMap = new Map<String,ReportDataWrapper>();  
        
        //Set of agreement ID's
        Set<ID> agreementIDs = new Set<ID>(); 
        
        //Map of Agreements by Student
        Map<ID,List<StudentProgram__c>> studentAgreementMap = new Map<ID,List<StudentProgram__c>>(); 
        
        //Map of Income by Student
        Map<ID,IncomeVerification__c> studentIncomeMap = new Map<ID,IncomeVerification__c>();
        
        //Map of Payments by Agreement
        Map<ID,Decimal> agreementPaymentMap = new Map<ID,Decimal>();
        
        //Map of last Month Payments by agreements
        Map<ID,Decimal> lastMonthPayments = new Map<ID,Decimal>();
        
        //Map of AgreementId and Delinquency Status
        Map<ID,String> agreementDelinquencyStatusMap = new Map<ID,String>();
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //get all agreements for selected program
        getAgreements(studentAgreementMap,agreementIDs);
        
        //get delinquency data
        getDelinquencyData(agreementIDs,agreementDelinquencyStatusMap);
        
        //get Income Data
        if(studentAgreementMap.keySet().size()>0){
            getIncomeData(studentAgreementMap.keySet(),studentIncomeMap);
        }
        
        //get payment data
        if(agreementIDs.size()>0){
            getPaymentData(agreementIDs,agreementPaymentMap,lastMonthPayments);         
        }
               
        //populate the final reportData list 
        reportData = buildReportData(studentAgreementMap ,studentIncomeMap,agreementPaymentMap,lastMonthPayments,agreementDelinquencyStatusMap);
        
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public List<ReportDataWrapper> buildReportData(Map<ID,List<StudentProgram__c>> studentAgreementMap,Map<ID,IncomeVerification__c> studentIncomeMap,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments,Map<ID,String> agreementDelinquencyStatusMap){
        List<ReportDataWrapper> output = new List<ReportDataWrapper>();
        
        for(ID studentID:studentAgreementMap.keySet()){
            ReportDataWrapper wrapper = new ReportDataWrapper();
            for(StudentProgram__c sp:studentAgreementMap.get(studentID)){
                wrapper.vemoAccountNumber = sp.StudentVemoAccountNumber__c;
                wrapper.studentID = sp.student__c;
                wrapper.studentName = sp.student__r.name;
                AgreementWrapper agre = new AgreementWrapper();
                agre.agreement = sp;
                //bucket by delinquency
                if(agreementDelinquencyStatusMap.containsKey(sp.id)){
                    agre.delinquencyBucket = agreementDelinquencyStatusMap.get(sp.id);
                }
                
                if(agreementPaymentMap.containsKey(sp.id)) agre.cumulativePayments = agreementPaymentMap.get(sp.id);
                if(lastMonthPayments.containsKey(sp.id)) agre.lastMonthPayments = lastMonthPayments.get(sp.id);
                
                wrapper.agreements.add(agre);
                
            }
            if(studentIncomeMap.containsKey(studentID)){
                wrapper.income = studentIncomeMap.get(studentID);
            }
            output.add(wrapper);
        }
        
        return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student Programs for the selected School
    ///////////////////////////////////////////////
    public void getAgreements(Map<ID,List<StudentProgram__c>> agreementsByStudent,Set<ID> agreementIDs){
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        /*List<StudentProgram__c> agreementList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                                                 PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,
                                                 ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                                                 MajorPostCertification__r.name,GradeLevelPostCertification__c,EnrollmentStatusPostCertification__c,
                                                 FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentTermPostCertification__c,
                                                 PaymentCapPostCertification__c,program__r.Name,
                                                 GraceMonthsAllowed__c,GraceMonthsRemaining2__c,PaymentTermAssessed__c,PaymentTermRemaining__c,
                                                 DefermentMonthsAllowed__c,DefermentMonthsRemaining2__c,MinimumIncomePerMonth__c,
                                                 DaysDelinquent__c,servicing__c
                                                 FROM StudentProgram__c
                                                 WHERE Status__c NOT IN :exclusionStatus AND Program__r.school__c =: selectedSchool AND CertificationDate__c <> null AND certificationDate__c < :dt
                                                 ORDER BY student__r.name ASC]; */
                                                 
        List<StudentProgram__c> agreementList = queryMethod();           
        Integer offset;
        for(StudentProgram__c agreement:agreementList){
            if(agreement.certificationDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.certificationDate__c);
                agreement.certificationDate__c = agreement.certificationDate__c.addSeconds(offset/1000);
            }
            if(agreement.ApplicationStartDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.ApplicationStartDate__c);
                agreement.ApplicationStartDate__c = agreement.ApplicationStartDate__c.addSeconds(offset/1000);
            }
            if(agreement.submittedDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.submittedDate__c);
                agreement.submittedDate__c = agreement.submittedDate__c.addSeconds(offset/1000);
            }
            
            if(!agreementsByStudent.containsKey(agreement.student__c)){
                agreementsByStudent.put(agreement.student__c,new List<StudentProgram__c>());
            }
            agreementsByStudent.get(agreement.student__c).add(agreement);
            agreementIDs.add(agreement.ID);
        }
    }
    
    public List<StudentProgram__c> queryMethod(){
    
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1);
        string query = 'SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,';
        query += 'PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,PaymentCapPostCertification__c,';
        query += 'ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,DaysDelinquent__c,';
        query += 'MajorPostCertification__r.name,GradeLevelPostCertification__c,EnrollmentStatusPostCertification__c,';
        query += 'FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentTermPostCertification__c,program__r.Name,';
        query += 'GraceMonthsAllowed__c,GraceMonthsRemaining2__c,PaymentTermAssessed__c,PaymentTermRemaining__c,servicing__c,';
        query += 'DefermentMonthsAllowed__c,DefermentMonthsRemaining2__c,MinimumIncomePerMonth__c,FundOwner__r.name,PaymentStreamOwner__r.name ';        
        query += 'FROM StudentProgram__c '; 
        
        
        query += 'WHERE Program__r.school__c = \''+selectedSchool+'\'';
        
        if(selectedPrimaryOwners.size()>0){            
            query += ' AND (PrimaryOwner__c IN ';
            query = partOfQueryMethod(selectedPrimaryOwners,'PrimaryOwner',query);
        }
        if(selectedFundOwners.size()>0){            
            query += ' AND (FundOwner__c IN ';
            query = partOfQueryMethod(selectedFundOwners,'FundOwner',query);
        }
        if(selectedPaymentStreamOwners.size()>0){            
            query += ' AND (PaymentStreamOwner__c IN ';
            query = partOfQueryMethod(selectedPaymentStreamOwners,'PaymentStreamOwner',query);
        }
                                        
        
        query += ' AND CertificationDate__c <> null AND certificationDate__c < ' + String.escapeSingleQuotes(String.valueOf(dt).replaceAll(' ', 'T')+'Z');
        query += ' AND Status__c != \'Application Under Review\'';
        query += ' AND Status__c != \'Draft\'';
        query += ' AND Status__c != \'Invited\'';
        query += ' AND Status__c != \'Application Incomplete\'';
        query += ' AND Status__c != \'Application Complete\'';
        query += ' ORDER BY student__r.name ASC ';
        
        system.debug('QUERY: '+query);
        DatabaseUtil db = new DatabaseUtil();
        List<StudentProgram__c> spList = new List<StudentProgram__c>();
        if(!Test.isRunningTest())
            spList = (List<StudentProgram__c>)db.query(query);
        else{
            spList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                     PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,
                     ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                     MajorPostCertification__r.name,GradeLevelPostCertification__c,EnrollmentStatusPostCertification__c,
                     FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentTermPostCertification__c,
                     PaymentCapPostCertification__c,program__r.Name,
                     GraceMonthsAllowed__c,GraceMonthsRemaining2__c,PaymentTermAssessed__c,PaymentTermRemaining__c,
                     DefermentMonthsAllowed__c,DefermentMonthsRemaining2__c,MinimumIncomePerMonth__c,
                     DaysDelinquent__c,servicing__c
                     FROM StudentProgram__c];
        }
        return spList;
    }
    
    public string partOfQueryMethod(Set<Id> idList,string field, string query){
        //string query;
        boolean nullVar = false;
        List<String> querAbleIds = new List<String>();
        for(Id poId : idList) {
            if(poId != null)              
                querAbleIds.add('\'' + poid + '\'');
            if(poId == null)
                nullVar = true;                
        }
        if(querAbleIds.size()>0){
            query += String.format(
                '{0}{1}{2}',
                new String[]{
                    '(', 
                    String.join(querAbleIds, ', '),
                    ')'
                }
            );
        }
        else
            query += '(null)';
        if(!nullVar){
            query += ')';
        }
        else{
            if(field == 'PrimaryOwner')
                query += ' OR PrimaryOwner__c = null)';
            else if(field == 'FundOwner')
                 query += ' OR FundOwner__c = null)';
             else if(field == 'PaymentStreamOwner')
                 query += ' OR PaymentStreamOwner__c = null)';

        }
            
        return query;
    }
    
    /////////////////////////////////////////////////
    ///Description: get delinquency data
    /////////////////////////////////////////////////
    private void getDelinquencyData(Set<ID> agreementIDs,Map<ID,String> agreementDelinquencyStatusMap){
        List<StudentProgramAudit__c> spaList = new List<StudentProgramAudit__c>();
        spaList = [SELECT daysdelinquent__c,studentprogram__c,auditdatetime__c,servicing__c
                   FROM StudentProgramAudit__c
                   WHERE monthEnd__c = true AND studentProgram__c IN :agreementIDs AND createdDate = LAST_MONTH];
        for(StudentProgramAudit__c audit:spaList){
                if(audit.DaysDelinquent__c >= 270){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'270+ Days Delinquent');        
                }
                else if(audit.DaysDelinquent__c < 270 && audit.DaysDelinquent__c >= 210){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'210-269 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 210 && audit.DaysDelinquent__c >= 180){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'180-209 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 180 && audit.DaysDelinquent__c >= 120){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'120-179 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 120 && audit.DaysDelinquent__c >= 90){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'90-119 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 90 && audit.DaysDelinquent__c >= 60){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'60-89 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 60 && audit.DaysDelinquent__c >= 30){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'30-59 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 30 && audit.DaysDelinquent__c >= 0 && audit.servicing__c == true){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'Current');
                }
        }
    }
    
    ////////////////////////////////////////////////////////////////
    ///Description: Get the Payment Data
    ////////////////////////////////////////////////////////////////
    public void getPaymentData(Set<ID> agreementIDs,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments){
        Datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        Integer month = date.today().month() == 1 ? 12 : date.today().month()-1;
        Integer year = date.today().month() == 1 ? (date.today().year()-1) : date.today().year();
        
         
        /*
        AggregateResult[] groupedResults = [SELECT agreement__c agrID,sum(AmountAllocated__c) 
                                            FROM PaymentAllocation__c
                                            WHERE agreement__c IN :agreementIDs AND createdDate < :dt
                                            GROUP BY agreement__c];    
    
        for(AggregateResult ar:groupedResults){
            String agreementID = (String)ar.get('agrID');
            Decimal sum = (Decimal)ar.get('expr0'); 
            agreementPaymentMap.put(agreementID,sum); 
        }
        */
        List<PaymentAllocation__c> payments = [SELECT id,agreement__c,amountAllocated__c,createddate
                                         FROM PaymentAllocation__c
                                         WHERE agreement__c IN :agreementIDs AND createdDate < :dt];
        for(PaymentAllocation__c payment:payments){
            if(!agreementPaymentMap.containsKey(payment.agreement__c)){
                agreementPaymentMap.put(payment.agreement__c,0);
            }
            agreementPaymentMap.put(payment.agreement__c,agreementPaymentMap.get(payment.agreement__c)+payment.amountallocated__c); 
            
            if(payment.createdDate.date().month() == month && payment.createdDate.date().year() == year){
                if(!lastMonthPayments.containsKey(payment.agreement__c)){
                   lastMonthPayments.put(payment.agreement__c,0);
                }
                lastMonthPayments.put(payment.agreement__c,lastMonthPayments.get(payment.agreement__c) + payment.amountAllocated__c);            
            }    
        }
    }
    
    //////////////////////////////////////////////////
    ///Get Income Data
    //////////////////////////////////////////////////
    public void getIncomeData(Set<ID> studentIDs,Map<ID,IncomeVerification__c> studentIncomeMap){    
        List<incomeVerification__c> ivList = new List<incomeVerification__c>();
        ivList = [Select id,student__c,beginDate__c,endDate__c,type__c,IncomePerMonth__c,DateVerified__c,
                  EmploymentHistory__r.name,EmploymentHistory__c,EmploymentHistory__r.Employer__c,EmploymentHistory__r.DateReported__c,EmploymentHistory__r.type__c,
                  EmploymentHistory__r.EmploymentStartDate__c,EmploymentHistory__r.EmploymentEndDate__c,EmploymentHistory__r.Category__c 
                  From IncomeVerification__c
                  Where Student__c IN :studentIDs AND status__c = 'Verified' AND type__c = 'Reported' 
                  Order By beginDate__c DESC,Student__r.name ASC,type__c DESC];
        Map<Id,List<IncomeVerification__c>> sortedIncomesByStudent = new Map<Id,List<IncomeVerification__c>>(); 
        for(IncomeVerification__c iv:ivList){
            if(!sortedIncomesByStudent.containsKey(iv.student__c)){
                sortedIncomesByStudent.put(iv.student__c,new List<IncomeVerification__c>());
            }
            sortedIncomesByStudent.get(iv.student__c).add(iv);
        }
        
        Date dt = Date.newInstance(date.today().year(),date.today().month(),1); 
        for(ID key:sortedIncomesByStudent.keySet()){
            List<incomeVerification__c> incomeList = sortedIncomesByStudent.get(key);
            //studentIncome.put(key,new IncomeVerification__c()); 
            Integer size = incomeList.size();
            for(integer i=0;i<incomeList.size();i++){
                if(incomeList[i].BeginDate__c < dt){
                    studentIncomeMap.put(key,incomeList[i]);
                }
                else{
                    continue;
                }
                if(!(i==(incomeList.size()-1))){
                    if(incomeList[i].BeginDate__c == incomeList[i+1].BeginDate__c){
                        if(incomeList[i].dateVerified__c > incomeList[i+1].DateVerified__c){
                            break;        
                        }
                        /* not reporting on estimated income
                        if(incomeList[i].type__c == incomeList[i+1].type__c){
                            if(incomeList[i].dateVerified__c > incomeList[i+1].DateVerified__c){
                                break;        
                            }        
                        }
                        else{
                            if(incomeList[i].type__c == 'Reported'){
                                break;
                            }        
                        }
                        */
                    }
                    else{
                        break;
                    }
                }
            }
        } 
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportAllAccountsWithIncomeCopy1Export');
        pg.setRedirect(false);
        return pg;
    }
    
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentID {get;set;}
        public String studentName {get;set;}
        public List<AgreementWrapper> agreements {get;set;}
        public IncomeVerification__c income {get;set;}
        
        public ReportDataWrapper(){
            this.agreements = new List<AgreementWrapper>();
            this.income = new IncomeVerification__c();
        }    
    }
    
    //////////////////////////////////////////
    ///Agreement Wrapper
    //////////////////////////////////////////
    public class AgreementWrapper{
        public StudentProgram__c agreement {get;set;}
        public String delinquencyBucket {get;set;}
        public Decimal cumulativePayments {get;set;}
        public Decimal lastMonthPayments {get;set;}
        public AgreementWrapper(){
            this.delinquencyBucket = '';
            this.cumulativePayments = 0;
            this.lastMonthPayments = 0;
        }
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Primary Owner
    ///////////////////////////////////
    public class primaryOwnerWrapper{
        public boolean selected {get;set;}
        public Account primaryOwner {get;set;}
        
        public primaryOwnerWrapper(boolean selected, Account primaryOwner){
            this.selected = selected;
            this.primaryOwner = primaryOwner;
        }  
        
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Fund Owner
    ///////////////////////////////////
    public class fundOwnerWrapper{
        public boolean selected {get;set;}
        public Account fundOwner {get;set;}
        
        public fundOwnerWrapper(boolean selected, Account fundOwner){
            this.selected = selected;
            this.fundOwner = fundOwner;
        }  
        
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Payment Stream Owner
    ///////////////////////////////////
    public class paymentStreamOwnerWrapper{
        public boolean selected {get;set;}
        public Account paymentStreamOwner {get;set;}
        
        public paymentStreamOwnerWrapper(boolean selected, Account paymentStreamOwner){
            this.selected = selected;
            this.paymentStreamOwner = paymentStreamOwner;
        }  
        
    }  
    
      
}