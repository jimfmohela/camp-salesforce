/*

/////////////////////////////DELINQUENCY///////////////////////////////////
OutboundEmailBatchScheduled job = new OutboundEmailBatchScheduled ();
job.jobType = OutboundEmailBatch.JobType.SEND_EMAIL;
String cronStr1 = '0 0 9 * * ? *';
System.schedule('Send Outbound Email', cronStr1, job);
/////////////////////////////////////////////////////////////////////////////

*/
public class OutboundEmailBatchScheduled implements Schedulable {
    
    public OutboundEmailBatch.JobType jobType {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        OutboundEmailBatch job = new OutboundEmailBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job,1);
    }
}