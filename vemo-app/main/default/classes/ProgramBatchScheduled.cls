/*
//Disbursement Confirmation Notifification
ProgramBatchScheduled job1 = new ProgramBatchScheduled();
//job1.job = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
//String cronStr = '0 0 * * * ? *'; //hourly
String cronStr = '0 0 9 * * ? *'; //nightly at 9am
System.schedule('Disbursement Confirmation Notification', cronStr, job1);
*/
public class ProgramBatchScheduled implements Schedulable {

    public ProgramBatch.JobType jobType {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        ProgramBatch job = new ProgramBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job);
    }
}