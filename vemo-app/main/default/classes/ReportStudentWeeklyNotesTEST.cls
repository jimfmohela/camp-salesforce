@isTEST
public class ReportStudentWeeklyNotesTEST{
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    public static string selectedSchool = '';
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta11@vemo.com123', communitynickname = 'testcommunity12');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
            //create students
            Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(10);
            
            
            ContentNote cn = new ContentNote();
            cn.Title = 'test1';
            String body = 'Please work';
            cn.Content = Blob.valueOf(body.escapeHTML4());
            //cn.TextPreview = 'Test';
            //cn.ShareType = 'V';
            dbUtil.insertRecord(cn);    
            
            ContentDocumentLink link2 = new ContentDocumentLink();
            link2.ContentDocumentId = cn.id;
            link2.LinkedEntityId = studentMap.values()[0].id;
            link2.ShareType = 'I';
            link2.Visibility = 'AllUsers';
            
            dbUtil.insertRecord(link2);
            
            ReportStudentWeeklyNotes cntrl = new ReportStudentWeeklyNotes();
            cntrl.runReport();
            cntrl.exportToExcel();
        }
    }
    
    @isTest public static void runReport(){
        /*ReportStudentWeeklyNotes cntrl = new ReportStudentWeeklyNotes();
        cntrl.runReport();
        cntrl.exportToExcel();*/
    }
}