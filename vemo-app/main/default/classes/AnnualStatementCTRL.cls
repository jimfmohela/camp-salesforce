public with sharing class AnnualStatementCTRL{

    public Id studentId {get;set;}
    public decimal totalAmount{get;set;}
    
    public account getAccount()
    {
        Map<ID, Account> acctMap = AccountQueries.getStudentMapWithStudentID(new set<id>{studentId});
        return acctMap.values()[0];
        //return [select name, VemoAccountNumber__c,personMailingStreet, personMailingCity, personMailingCountry, personMailingPostalCode, personMailingState from account where id =: studentId limit 1];
    }
    public List<StudentProgram__c> getstudentprograms()
    { 
        List<StudentProgram__c> stProgramList;
        
        Set<String> activeStatuses = new Set<String>{'Certified', 'Partially Funded', 'Fully Funded', 'Grace', 'Deferment', 
                                            'Payment', 'School','Internship', 'Leave of Absence'};
        
        stProgramList = [select Servicing__c,ExpectedGraduationDate__c,LastDateOfAttendance__c,PaymentTermRemaining__c,GraceMonthsRemaining2__c,DefermentMonthsRemaining2__c,ProgramName__c,VemoContractNumber__c, SchoolName__c, FundingAmountPostCertification__c, 
                        IncomeSharePostCertification__c, PaymentTermPostCertification__c, PaymentCapPostCertification__c, ServicingStartDate__c,MinimumIncomePerMonth__c, GraceMonthsAllowed__c, GraceMonthsUsed__c,DefermentMonthsAllowed__c, DefermentMonthsUsed__c, (select Amount__c,amountallocated__c,AssessmentDateTime__c,RemainingAllocation__c from Student_Program_Debits__r order by AssessmentDateTime__c desc) from StudentProgram__c 
                        where (status__c IN :activeStatuses OR (status__c =: 'Pause' AND Servicing__c= true)) and student__c =: studentId];
        totalAmount = 0;
        for(StudentProgram__c sp : stProgramList){
            if(sp.FundingAmountPostCertification__c != null)
                totalAmount = totalAmount + sp.FundingAmountPostCertification__c;
        }
        return stProgramList;
    }
}