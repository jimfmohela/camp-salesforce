/////////////////////////////////////////////////////////////////////////
// Class: AcademicEnrollmentService_TEST
// 
// Description: 
//  Test class for AcademicEnrollmentService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-04   Kamini Singh  Created              
/////////////////////////////////////////////////////////////////////////
@isTest
public class AcademicEnrollmentService_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetAcademicEnrollmentMapForAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        
        Test.startTest();
        List<AcademicEnrollmentService.AcademicEnrollment> resultAcademicEnrollmentList = AcademicEnrollmentService.getAcademicEnrollmentMapWithStudent(testStudentAccountMap.keyset());
        System.assertEquals(testAcademicEnrollmentMap.keySet().size(), resultAcademicEnrollmentList.size());
        Test.stopTest();
    }
    static testMethod void testGetAcademicEnrollmentMapForAuthUserwithRead(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        
        Test.startTest();
        List<AcademicEnrollmentService.AcademicEnrollment> resultAcademicEnrollmentList = AcademicEnrollmentService.getAcademicEnrollmentMapWithProvider(schools.keyset());
        System.assertEquals(testAcademicEnrollmentMap.keySet().size(), resultAcademicEnrollmentList.size());
        Test.stopTest();
    }
}