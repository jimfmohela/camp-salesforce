@isTest
public class PlaidBankService_TEST {

    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    @isTest public static void validategetPlaidBankWithPlaidBankID(){

        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, PlaidBank__c> plaidBanks = TestDataFactory.createAndInsertPlaidBank(students, 1); 
        Test.startTest();
        List<PlaidBankService.PlaidBank> plaidBankList = PlaidBankService.getPlaidBankWithPlaidBankID(plaidBanks.keySet());
        Test.stopTest();
        System.assertEquals(plaidBankList.size(), plaidBanks.size());
    }
    
    
    @isTest public static void validateGetPlaidBankWithStudentID(){

        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, PlaidBank__c> plaidBanks = TestDataFactory.createAndInsertPlaidBank(students, 1); 
        Test.startTest();
        List<PlaidBankService.PlaidBank> plaidBankList = PlaidBankService.getPlaidBankWithStudentID(students.keySet());
        Test.stopTest();
        System.assertEquals(plaidBankList.size(), plaidBanks.size());
    }
    
    @isTest public static void validateUpdatePlaidBank(){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, PlaidBank__c> plaidBanks = TestDataFactory.createAndInsertPlaidBank(students, 1); 
        List<PlaidBankService.PlaidBank> plaidBankListToUpdate = PlaidBankService.getPlaidBankWithStudentID(plaidBanks.keySet());
        for(PlaidBankService.PlaidBank pbObj : plaidBankListToUpdate){
            pbObj.plaidStatus = 'Linked';
        }
        Test.startTest();
        Set<ID> updatedBanks = PlaidBankService.updatePlaidBanks(plaidBankListToUpdate);
        Test.stopTest();
        Map<ID, PlaidBank__c> pbMap = PlaidBankQueries.getPlaidBankMapWithID(updatedBanks);
        //for(PlaidBank__c pb: pbMap.values()){
        //    system.assertEquals(pb.PlaidStatus__c, 'Linked');
        //}
    
    }
}