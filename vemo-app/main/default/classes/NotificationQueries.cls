/////////////////////////////////////////////////////////////////////////
// Class: NotificationQueries
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-05-17   Jared Hagemann  Created 
/////////////////////////////////////////////////////////////////////////
public without sharing class NotificationQueries {
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }

    public static Map<ID, Notification__c> getNotificationMap(){
        Map<ID, Notification__c> notificationMap = new Map<ID, Notification__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Id != null';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        notificationMap = new Map<ID, Notification__c>((List<Notification__c>)db.query(query));
        return notificationMap;     
    }

    public static Map<ID, Notification__c> getNotificationByID(Set<Id> notificationIds){
        Map<ID, Notification__c> notificationMap = new Map<ID, Notification__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(notificationIds);
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        notificationMap = new Map<ID, Notification__c>((List<Notification__c>)db.query(query));
        return notificationMap;     
    }

    public static Map<ID, Notification__c> getNotificationByStudentID(Set<Id> studentIds){
        Map<Id, Account> studentMap = AccountQueries.getStudentMapWithStudentID(studentIds);
        Set<Id> contactIds = new Set<Id>();
        for(Account stud : studentMap.values()){
            contactIds.add(stud.PersonContactID);
        }   
        Map<ID, Notification__c> notificationMap = new Map<ID, Notification__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Contact__c IN ' + DatabaseUtil.inSetStringBuilder(contactIds);
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        notificationMap = new Map<ID, Notification__c>((List<Notification__c>)db.query(query));
        return notificationMap;     
    }
    
    public static Map<ID, Notification__c> getNotificationByStudentIDNType(Set<Id> studentIds, string type){
        
        //string queryN = 'select id, (' + 'SELECT ' + getNotificationFieldNames() + ' FROM Notifications__r '+ ') from contact where account.id IN '+ DatabaseUtil.inSetStringBuilder(studentIds);
        //DatabaseUtil db = new DatabaseUtil();
        //List<Contact> conList = db.query(queryN);
        List<Contact> conList = new List<contact>();
        
        if(type !=null && type == 'Email'){
            conList = [select id,(select id,Name,Contact__c,HtmlBody__c ,toEmail__c,PlainText__c,Subject__c,
                                  createdDate from Notifications__r) 
                       from contact where account.id IN: studentIDs];
        }
        Map<ID, Notification__c> notificationMap = new Map<ID, Notification__c>();   
        for(Contact con : conList){
            for(Notification__c noti : con.Notifications__r)
                notificationMap.put(noti.id, noti);    
        }
        return notificationMap;     
    }
    
    public static Map<ID, Notification__c> getNotificationByNotificationIDNType(Set<Id> notificationIds, string type){
        
        set<string> accountIDSet = new set<string>();
        accountIDSet.add(UserService.userContext.contact.AccountId); //get Auth User
        
        //get all students from schoolId
        Map<Id, Account> studentMap = AccountQueries.getStudentMapWithSchoolID(accountIDSet);
        List<Contact> conList = new List<contact>();
        
        if(type !=null && type == 'Email'){
            conList = [select id,(select id,Name,Contact__c,HtmlBody__c ,toEmail__c,PlainText__c,Subject__c,
                                  createdDate from Notifications__r where Id IN: notificationIds) 
                       from contact where account.id IN: studentMap.keyset()];
        }
        
        Map<ID, Notification__c> notificationMap = new Map<ID, Notification__c>();    
        for(Contact con : conList){
            for(Notification__c noti : con.Notifications__r)
                notificationMap.put(noti.id, noti);    
        }
        return notificationMap;     
    }

    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
    
    private static String buildFilterString(){
        String filterStr = '';
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +  '\' ');
            }           
        }
        return filterStr;
    }

    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getNotificationFieldNames() + ' FROM Notification__c ';
        return soql;
    }

    private static String getNotificationFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'Contact__c, ';
        fieldNames += 'HtmlBody__c , ';
        fieldNames += 'PlainText__c, ';
        fieldNames += 'Subject__c, ';
        fieldNames += 'ToEmail__c, ';
        fieldNames += 'Contact__r.account.name, ';
        fieldNames += 'createdDate';
        return fieldNames;
    }
}