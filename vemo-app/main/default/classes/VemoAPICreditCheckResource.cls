/////////////////////////////////////////////////////////////////////////
// Class: VemoAPICreditCheckResource
// 
// Description: 
//  Direction Central for Credit Check Resource API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-12-18   Greg Cook       Created                          
// 
/////////////////////////////////////////////////////////////////////////
public class VemoAPICreditCheckResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){      
        System.debug('VemoAPICreditCheckResource.handleGetV1()');   
        String checkIDparam = api.params.get('creditCheckID');
        String studentIDparam = api.params.get('studentID');
        String agreementIDparam = api.params.get('agreementID');
        List<CreditCheckService.CreditCheck> ccs = new List<CreditCheckService.CreditCheck>();
        if(checkIDparam != null){
            ccs = CreditCheckService.getCreditCheckWithCreditCheckID(VemoApi.parseParameterIntoIDSet(checkIDparam));
        }
        else if(studentIDparam != null){
            ccs = CreditCheckService.getCreditCheckWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDparam));
        }
        else if(agreementIDparam != null){
            ccs = CreditCheckService.getCreditCheckWithAgreementID(VemoApi.parseParameterIntoIDSet(agreementIDparam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter creditCheckID, studentID, or agreementID');
        }
        Set<ID> ccIDs = new set<ID>();
        List<CreditCheckResourceOutputV1> results = new List<CreditCheckResourceOutputV1>();
        for(CreditCheckService.CreditCheck cc : ccs){
            results.add(new CreditCheckResourceOutputV1(cc));
            ccIDs.add(cc.creditCheckId);
        }
        
        //getting Transunion Attachment for creditcheck
        Map<ID,Attachment> AttachmentByCCID = new Map<ID, Attachment>();
        List<Attachment> atts = [select Id, Body, ParentID, ContentType, Name from Attachment where parentID IN :ccIDs AND Name LIKE 'TransUnionRaw%'];
        for(Attachment att: atts){
            AttachmentByCCID.put(att.ParentID, att);    
        }
        for(CreditCheckResourceOutputV1 ccresource: results){
            if(AttachmentByCCID.containsKey(ccresource.creditCheckID) && AttachmentByCCID.get(ccresource.creditCheckID) != null)
                ccresource.attachmentID = AttachmentByCCID.get(ccresource.creditCheckID).ID; 
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){ 
        System.debug('VemoAPICreditCheckResource.handlePostV1()');  
        List<CreditCheckService.CreditCheck> ccs = new List<CreditCheckService.CreditCheck>();
        List<CreditCheckResourceInputV1> checksJSON = (List<CreditCheckResourceInputV1>)JSON.deserialize(api.body, List<CreditCheckResourceInputV1>.class);
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        for(CreditCheckResourceInputV1 checkJSON : checksJSON){
            idsToVerify.add((ID)checkJSON.studentID);
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(CreditCheckResourceInputV1 checkJSON : checksJSON){
            if(verifiedAccountMap.containsKey((ID)checkJSON.studentID)){
                checkJSON.validatePOSTFields();
                ccs.add(creditCheckResourceToCreditCheck(checkJSON));
            }
        }       
        Set<Id> checkIDs = CreditCheckService.createCreditChecks(ccs);
        return (new VemoAPI.ResultResponse(checkIDs, checkIDs.size()));
    }

    
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPICreditCheckResource.handlePutV1()');   
        List<CreditCheckService.CreditCheck> ccs = new List<CreditCheckService.CreditCheck>();
        List<CreditCheckResourceInputV1> checksJSON = (List<CreditCheckResourceInputV1>)JSON.deserialize(api.body, List<CreditCheckResourceInputV1>.class);
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        //RINI - this needs to be updated to query any credit check based on creditCheckID using the query layer.  THis should solve the security
         for(CreditCheckResourceInputV1 checkJSON : checksJSON){
             idsToVerify.add((ID)checkJSON.creditCheckID);
         }
        Map<ID, CreditCheck__c> verifiedCreditCheckMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(idsToVerify); //authorized records should be returned
        //Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(CreditCheckResourceInputV1 checkJSON : checksJSON){
            if(verifiedCreditCheckMap.containsKey((ID)checkJSON.creditCheckID)){
                checkJSON.validatePUTFields();
                ccs.add(creditCheckResourceToCreditCheck(checkJSON));
            }
        }       
        Set<Id> checkIDs = CreditCheckService.updateCreditChecks(ccs);
        return (new VemoAPI.ResultResponse(checkIDs, checkIDs.size()));
    }
    
    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPICreditCheckResource.handleDeleteV1()');
        String checkIDparam = api.params.get('creditCheckID');
        
        Map<ID, CreditCheck__c> creditCheckMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(VemoApi.parseParameterIntoIDSet(checkIDparam));
        
        Set<ID> idsToVerify = new Set<ID>();
        for(CreditCheck__c cc: creditCheckMap.values()){
            idsToVerify.add(cc.student__c); 
        }
        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        Set<ID> creditChecksToBeDeleted = new Set<ID>();
        for(CreditCheck__c cc: creditCheckMap.values()){
            if(verifiedAccountMap.containsKey(cc.student__c)){
                creditChecksToBeDeleted.add(cc.id);
            }
        }
        Integer numToDelete = 0;
        if(creditChecksToBeDeleted.size()>0){
            numToDelete = CreditCheckService.deleteCreditChecks(creditChecksToBeDeleted); 
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    

    public static CreditCheckService.CreditCheck creditCheckResourceToCreditCheck(CreditCheckResourceInputV1 check){
        CreditCheckService.CreditCheck cc = new CreditCheckService.CreditCheck();
        cc.creditCheckID = check.creditCheckID;
//      cc.consentDateTimeStamp = check.consentDateTimeStamp;
//      cc.consentIPAddress = check.consentIPAddress;
        cc.FICOScore = check.FICOScore;
        cc.studentID = check.studentID; //convert from GUID
        cc.creditReportDateTimeStamp = check.creditReportDateTimeStamp; 
//      cc.jsonPayload = check.jsonPayload;
//      cc.status = check.status;
//      cc.creditCheckDeniedReason = check.creditCheckDeniedReason;
//      cc.creditCheckDeniedReasonText = check.creditCheckDeniedReasonText;
        return cc;
    }

    public class CreditCheckResourceInputV1{
        public String studentID {get;set;}
//      public DateTime consentDateTimeStamp {get;set;}
//      public String consentIPAddress {get;set;}
        public String creditCheckID {get;set;}
        public String FICOScore {get;set;}
        public DateTime creditReportDateTimeStamp {get;set;}
//      public String jsonPayload {get;set;}
//      public String status {get;set;}
//      public String creditCheckDeniedReasonText {get;set;}
//      public String creditCheckDeniedReason {get;set;}


        public CreditCheckResourceInputV1(){}

        public CreditCheckResourceInputV1(Boolean testValues){
            if(testValues){
                studentID = 'testStudentID';
//              consentDateTimeStamp = DateTime.Now();
//              consentIPAddress = '1.1.1.1';
//              jsonPayload = 'test';
            }
        }

        public void validatePOSTFields(){
            if(creditCheckID != null) throw new VemoAPI.VemoAPIFaultException('studentID cannot be created in POST');
        }
        public void validatePUTFields(){
            if(creditCheckID == null) throw new VemoAPI.VemoAPIFaultException('creditCheckID is a required input parameter on PUT');
        }
    }


    public class CreditCheckResourceOutputV1{

        public CreditCheckResourceOutputV1(){}
        
        public CreditCheckResourceOutputV1(CreditCheckService.CreditCheck check){
            studentID = check.studentID;
//          consentIPAddress = check.consentIPAddress;
//          consentDateTimeStamp = check.consentDateTimeStamp;
            creditCheckID = check.creditCheckID;
//          jsonPayload = check.jsonPayload;
            FICOScore = check.FICOScore;
            creditReportDateTimeStamp = check.creditReportDateTimeStamp; 
//          status = check.status;
//          creditCheckDeniedReason = check.creditCheckDeniedReason;
//          creditCheckDeniedReasonText = check.creditCheckDeniedReasonText;
        }
        public String studentID {get;set;}
//      public DateTime consentDateTimeStamp {get;set;}
//      public String consentIPAddress {get;set;}
        public String creditCheckID {get;set;}
//      public String jsonPayload {get;set;}
        public String FICOScore {get;set;}
        public String attachmentID {get;set;}
        public DateTime creditReportDateTimeStamp {get;set;}
//      public String status {get;set;}
//      public String creditCheckDeniedReasonText {get;set;}
//      public String creditCheckDeniedReason {get;set;}

    }


}