/////////////////////////////////////////////////////////////////////////
// Class: DefermentService_TEST 
// 
// Description: 
//  Unit test for DefermentQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-09-25   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class DefermentService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testGetDefermentWithStudentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Test.startTest();
        List<DefermentService.Deferment> resultDefList = DefermentService.getDefermentMapWithStudent(testStudentAccountMap.keySet());
        System.assertEquals(testDefMap.keySet().size(), resultDefList.size());
        Test.stopTest();
    }
    
    static testMethod void testGetDefermentMapWithDefermentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Test.startTest();
        List<DefermentService.Deferment> resultDefList = DefermentService.getDefermentMapWithDefermentID(testStudentAccountMap.keySet());
        System.assertEquals(testDefMap.keySet().size(), resultDefList.size());
        Test.stopTest();
    }

    static testMethod void testCreateDeferment(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        List<DefermentService.Deferment> defList = new List<DefermentService.Deferment>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            DefermentService.Deferment def = new DefermentService.Deferment();
            def.student = testStudentAccountMap.values().get(i).Id;
            defList.add(def);
        }
        Test.startTest();
        Set<ID> defIDs = DefermentService.createDeferments(defList);
        System.assertEquals(defList.size(), DefermentQueries.getDefermentMapWithDefermentID(defIDs).size());
        Test.stopTest();
    }

    static testMethod void testUpdateDeferment(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testdefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        List<DefermentService.Deferment> defList = new List<DefermentService.Deferment>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            DefermentService.Deferment def = new DefermentService.Deferment();
            def.DefermentID = testdefMap.values().get(i).Id;
            def.verified = 'false';
            defList.add(def);
        }
        Test.startTest();
        Set<ID> defIDs = DefermentService.updateDeferments(defList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, defList.size());
        for(Deferment__c def : DefermentQueries.getDefermentMapWithDefermentID(defIDs).Values()){
            System.assert(!def.Verified__c);
        }
    }

   static testMethod void testDeleteDeferment(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testdefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Test.startTest();
        Integer deleted = DefermentService.deleteDeferments(testdefMap.keySet());        
        Test.stopTest();
        System.assertEquals(testdefMap.keySet().size(), deleted);
    }
}