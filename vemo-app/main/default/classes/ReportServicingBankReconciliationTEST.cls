@isTEST
public class ReportServicingBankReconciliationTEST{
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    public static string selectedSchool = '';
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta11@vemo.com', communitynickname = 'testcommunity1');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
            //create students
            Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(10);
            //create school
            Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
            for(Account acc: schoolMap.Values()){
                acc.name = 'Purdue University-Main Campus';
            }
            Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
            dbUtil.updateRecords(schoolMap.Values());
            //create program
            Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1,schoolMap);
            //create student programs
            Map<ID, StudentProgram__c> spMap = TestDataFactory.createAndInsertStudentProgram(1,studentMap,programMap);
            //create Payment Method
            Map<ID, PaymentMethod__c> pmMap = TestDataFactory.createAndInsertPaymentMethod(1,studentMap);
            system.debug('pmMap:' + pmMap);
            //create Payment Instruction
            Map<ID, PaymentInstruction__c> piMap = TestDataFactory.createAndInsertPaymentInstruction(1,studentMap,pmMap);
            system.debug('piMap:' + piMap);
            for(PaymentInstruction__c pi: piMap.values()){
                pi.Status__c='Cleared';
                pi.DatePosted__c = date.today().addMonths(-1);
                pi.AmountPosted__c = 0;
            }
            dbUtil.updateRecords(piMap.values());
        
        }
    }
    
    @isTest public static void validateReportServicingBankReconciliation(){
       
        ReportServicingBankReconciliation cntrl = new ReportServicingBankReconciliation();
        cntrl.runReport();
        cntrl.exportToExcel();
    }
    
    @isTest public static void validateReportServicingBankReconciliationCopy(){
       
        ReportServicingBankReconciliationCopy cntrl = new ReportServicingBankReconciliationCopy();
        cntrl.runReport();
        cntrl.exportToExcel();
    }
    
    @isTest public static void validateReportServicingBankReconciliationCopy1(){
       
        ReportServicingBankReconciliationCopy1 cntrl = new ReportServicingBankReconciliationCopy1();
        cntrl.runReport();
        cntrl.exportToExcel();
    }

}