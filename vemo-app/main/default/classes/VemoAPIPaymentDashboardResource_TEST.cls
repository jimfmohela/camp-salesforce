@isTest
public class VemoAPIPaymentDashboardResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testHandleGetV2(){
        DatabaseUtil db = new DatabaseUtil();
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
                        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(5);
        for(Account student: students.values()){
            student.primarySchool__pc = schools.values().get(0).Id;
            
        }
        
        db.updateRecords(students.values());
        
                
        /*system.assertequals((string)schools.values().get(0).Id, (string)students.values().get(0).primaryschool__pc);
        for(account stud: students.values()){
            system.debug(stud.ID+'  ----  '+stud.PrimarySchool__pc);
        }*/
        
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        for(Integer i=0; i<3; i++){
            studProgramMap.values().get(i).Status__c = 'Grace';
            
        }
        for(Integer i=3; i<5; i++){
            studProgramMap.values().get(i).Status__c = 'Payment';
        }
        
        db.updateRecords(studProgramMap.values());
        
        
        for(StudentProgram__c studprog: studProgramMap.values()){
            system.debug(studprog.status__c);
        }
        
        Map<ID, PaymentMethod__c> testPaymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, students);
        
        Map<ID, PaymentInstruction__c> testPaymentInsturctionMap = TestDataFactory.createAndInsertPaymentInstruction(1, students, testPaymentMethodMap);
        for(PaymentInstruction__c pi: testPaymentInsturctionMap.values()){
            pi.AmountPosted__c = 3000;
            pi.DatePosted__c = date.today();
            pi.Status__c = 'Cleared';
             
        }
        
        db.updateRecords(testPaymentInsturctionMap.values()); 
               
        VemoAPIPaymentDashboardResource.PaymentDashboardResourceOutputV2 obj = new VemoAPIPaymentDashboardResource.PaymentDashboardResourceOutputV2();
        obj.monthToDatePaymentList = new List<VemoAPIPaymentDashboardResource.MonthToDatePayment>();
        obj.yearToDatePaymentList = new List<VemoAPIPaymentDashboardResource.YearToDatePayment>();
        obj.allPaymentList = new List<VemoAPIPaymentDashboardResource.allPayment>();
        obj.paymentTypeList = new List<VemoAPIPaymentDashboardResource.PaymentType>();
        obj.paymentStatusList = new List<VemoAPIPaymentDashboardResource.PaymentStatus>();
        obj.paymentPipeline = new List<VemoAPIPaymentDashboardResource.PaymentPipeline>();
        
        obj.monthToDatePaymentList.add(new VemoAPIPaymentDashboardResource.MonthToDatePayment(testPaymentInsturctionMap.values().get(0).AmountPosted__c, 1, 1)); 
        obj.yearToDatePaymentList.add(new VemoAPIPaymentDashboardResource.YearToDatePayment(testPaymentInsturctionMap.values().get(0).AmountPosted__c, 1, 2)); 
        obj.allPaymentList.add(new VemoAPIPaymentDashboardResource.allPayment(testPaymentInsturctionMap.values().get(0).AmountPosted__c, 1, 2019)); 
        obj.paymentTypeList.add(new VemoAPIPaymentDashboardResource.PaymentType('AutoPay', 200, 10.00));
        obj.paymentStatusList.add(new VemoAPIPaymentDashboardResource.PaymentStatus('Grace',200, 20.0)); 
        String monthName = VemoAPIPaymentDashboardResource.getMonth(12);
        obj.paymentPipeline.add(new VemoAPIPaymentDashboardResource.PaymentPipeline(date.today(), monthName , 20));
        
         
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        params.put('schoolID', (String)schools.values().get(0).Id);
        VemoAPI.APIInfo paymentdashboardApiInfo = TestUtil.initializeAPI('v2', 'GET', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse apiResult = (VemoAPI.ResultResponse)VemoAPIPaymentDashboardResource.handleAPI(paymentdashboardApiInfo);
        System.assertEquals(1, apiResult.numberOfResults);
        Test.StopTest();
    }
}