/////////////////////////////////////////////////////////////////////////
// Class: RenderStatementCTRL
// 
// Description: 
// 	Controller for RenderStatmentComponent
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-19   Jared Hagemann  Created              
/////////////////////////////////////////////////////////////////////////
public with sharing class RenderStatementCTRL {
	public StatementService.StatementV1 statement {get;set;}
	public RenderStatementCTRL() {
		Id studentId = (Id)ApexPages.currentPage().getParameters().get('id');
		Map<ID, StatementService.StatementV1> statementMap = StatementService.getCurrentStatementV1ByStudentWithStudentID(new Set<Id>{studentId});
		statement = statementMap.values().get(0);
		
	}

    @AuraEnabled
    public static String getCurrentStatementV1List(String studentId){
        List<StatementService.StatementV1> statementV1List = new List<StatementService.StatementV1>();
            if(studentId != null && !String.isBlank(studentId)){
                try {
                    Map<ID, StatementService.StatementV1> statementMap = new Map<Id, StatementService.StatementV1>();
                    statementMap = StatementService.getCurrentStatementV1ByStudentWithStudentID(new Set<Id>{studentId});
                    if(statementMap.size() > 0){
                        statementV1List = statementMap.values();
                    }
                } catch (Exception e) {
                    system.debug('>>>>> Error message --> ' + e.getMessage() + ' On Line Number --> ' + e.getLineNumber() + ' In Class RenderStatementCTRL');
                }
            }
        return JSON.serialize(statementV1List);
    }
}