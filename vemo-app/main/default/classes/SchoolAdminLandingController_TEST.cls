/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest public with sharing class SchoolAdminLandingController_TEST {
    @IsTest(SeeAllData=true) public static void testLandingController() {
        // Instantiate a new controller with all parameters in the page
        SchoolAdminLandingController controller = new SchoolAdminLandingController();
        PageReference pageRef = controller.forwardToStartPage();
        //PageRef is either null or an empty object in test context
        if(pageRef != null){
            String url = pageRef.getUrl();
            if(url != null){
                System.assertEquals(true, String.isEmpty(url));
                //show up in perforce
            }
        }
    }

    @IsTest(SeeAllData=true) public static void testForwardToCustomAuth() {
        // Instantiate a new controller with all parameters in the page
        SchoolAdminLandingController controller = new SchoolAdminLandingController();
        PageReference pageRef = controller.forwardToCustomPage();
        //PageRef is either null or an empty object in test context
        if(pageRef != null){
            String url = pageRef.getUrl();
            if(url != null){
                //System.assertEquals(true, String.isEmpty(url));
                //show up in perforce
            }
        }
    }
}