@isTest
private class EventBatch_TEST
{

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void instantiateEventInstance(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> events = TestDataFactory.createAndInsertEvent(students, eventTypes);
        
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: students.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes );
        
        Test.startTest();
        
        EventBatch job = new EventBatch ();
        job.job = EventBatch.JobType.INSTANTIATE_EVENTINSTANCE;
        Database.executeBatch(job, 200);
        Test.stopTest();
      }
}