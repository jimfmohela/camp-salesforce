@isTest
public class CreditCheckService_TEST {
    public static DatabaseUtil databaseutil = new DatabaseUtil();
    
    public static void setupData(){
        TestUtil.createStandardTestConditions();
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(2);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(2, 
                                                                                    TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, studentMap, programMap);
    }
    
    @isTest
    public static void runTests(){
        setupData();
        Test.startTest();        
        validateGetCreditCheckWithCreditCheckID();
        validateGetCreditCheckWithStudentID();
        validateGetCreditCheckWithAgreementID();
        validateCreateCreditChecks();
        validateUpdateCreditChecks();
        validateDeleteCreditChecks();
        Test.stopTest();
    }
    
    public static void validateGetCreditCheckWithCreditCheckID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(2, studentMap);

        List<CreditCheckService.CreditCheck> ccs = CreditCheckService.getCreditCheckWithCreditCheckID(ccMap.keySet());
        System.assertEquals(ccMap.size(), ccs.size());
    }
    
    public static void validateGetCreditCheckWithStudentID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();        
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(2, studentMap);
        Set<Id> studentIds = new Set<Id>();
        for(CreditCheck__c cc : ccMap.values()){
            studentIds.add(cc.Student__c);
        }
        List<CreditCheckService.CreditCheck> ccs = CreditCheckService.getCreditCheckWithStudentID(studentIds);
        System.assertEquals(ccMap.size(), ccs.size());
    }

    public static void validateGetCreditCheckWithAgreementID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();        
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(2, studentMap);

        Integer maxSize = agreementMap.size();
        if(ccMap.size()<maxSize) maxSize = ccMap.size();

        for(Integer i = 0; i<maxSize; i++){
            agreementMap.values()[i].CreditCheck__c = ccMap.values()[i].id;
        }
        //system.debug(agreementMap);
        databaseutil.updateRecords(agreementMap.values());
        //update agreementMap.values();
        system.debug('agreementMap:'+agreementMap);
        system.debug('ccMap:'+ccMap);


        List<CreditCheckService.CreditCheck> ccs = CreditCheckService.getCreditCheckWithAgreementID(agreementMap.keySet());
        System.assertEquals(ccMap.size(), ccs.size());
    }
    
    public static void validateCreateCreditChecks(){

        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        //databaseutil = new DatabaseUtil();
        List<CreditCheckService.CreditCheck> ccList = new List<CreditCheckService.CreditCheck>();
        for(Integer i = 0; i<2; i++){
            CreditCheckService.CreditCheck cc = new CreditCheckService.CreditCheck(true);
            cc.studentID = studentMap.values().get(i).id;
            ccList.add(cc);
        }
        MockedQueryExecutor.setRecordLimit(2);
        Set<ID> ccIDs = CreditCheckService.createCreditChecks(ccList);
        System.assertEquals(ccList.size(), CreditCheckQueries.getCreditCheckMap().size());
    }

    public static void  validateUpdateCreditChecks(){
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();

        List<CreditCheckService.CreditCheck> ccList = new List<CreditCheckService.CreditCheck>();
        for(Integer i = 0; i<2; i++){
            CreditCheckService.CreditCheck cc = new CreditCheckService.CreditCheck(true);
            cc.studentID = studentMap.values().get(i).ID;
            ccList.add(cc);
        }
        Set<ID> ccIDs = CreditCheckService.createCreditChecks(ccList);
        List<CreditCheckService.CreditCheck> newCCs = CreditCheckService.getCreditCheckWithCreditCheckID(ccIDs);
        /*for(CreditCheckService.CreditCheck cc : newCCs){
            cc.jsonPayload = 'updated payload';
        }*/
        Set<ID> updatedCCIDs = CreditCheckService.updateCreditChecks(newCCs);
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(updatedCCIDs);
        /*for(CreditCheck__c cc : ccMap.values()){
            System.assertEquals('updated payload', cc.JSONPayload__c);
        }*/
    }

    public static void validateDeleteCreditChecks(){
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(2, studentMap);

        Integer numDeleted = CreditCheckService.deleteCreditChecks(ccMap.keySet());
        System.assertEquals(ccMap.size(), numDeleted);
        //System.assertEquals(0, CreditCheckQueries.getCreditCheckMap().size());
    }
    
}