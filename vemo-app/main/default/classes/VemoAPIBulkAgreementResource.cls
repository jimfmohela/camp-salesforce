/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIBulkAgreementResource
// 
// Description: 
//   Bulk Agreement Resource API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-04   Kamini Singh       Created 
/////////////////////////////////////////////////////////////////////////
public class VemoAPIBulkAgreementResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }    
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){ 
        String schoolIDParam = api.params.get('schoolID');
        String programIDParam = api.params.get('programID');
        
        String query = generateSOQLSelect();
        if(schoolIDParam != null){
            Set<ID> schoolIds = VemoApi.parseParameterIntoIDSet(schoolIDParam);
            query += ' WHERE Deleted__c = false and Program__r.School__c IN ' + DatabaseUtil.inSetStringBuilder(schoolIds);
        }
        else if(programIDParam != null){
            Set<ID> programIds = VemoApi.parseParameterIntoIDSet(programIDParam);
            query += ' WHERE Deleted__c = false and Program__c IN ' + DatabaseUtil.inSetStringBuilder(programIds);
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter schoolID or programID');
        }
        
        List<AgreementResourceOutputV2> results = new List<AgreementResourceOutputV2>();
        if(schoolIdParam != null || programIDParam != null){
        
            query += ' '+ generateLIMITStatement();
            
            DatabaseUtil db = new DatabaseUtil();
            List<StudentProgram__c> agreements = db.query(query);
            
            for(StudentProgram__c agreement : agreements){
                results.add(new AgreementResourceOutputV2(agreement));
            }
        }
        
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getfieldNames() + ' FROM StudentProgram__c';
        return soql;
    }
    
    public static String getfieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Program__r.ProgramName__c, ';
        fieldNames += 'Program__r.Name, ';
        fieldNames += 'Student__r.Name, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'Program__c, ';
        
        fieldNames += 'MajorPostCertification__c, ';
        fieldNames += 'MajorPreCertification__c, ';
        fieldNames += 'MajorStudent__c, ';
        
        fieldNames += 'MajorPostCertification__r.name, ';
        fieldNames += 'MajorPreCertification__r.name, ';
        fieldNames += 'MajorStudent__r.name, ';
        
        fieldNames += 'VemoContractNumber__c, ';
        fieldNames += 'Status__c, ';
        fieldNames += 'RequestedAmount__c, ';
        fieldNames += 'AmountDisbursedToDate__c, ';
        fieldNames += 'Name, ';
        
        fieldNames += 'FundingAmountPostCertification__c, ';
        fieldNames += 'FundingAmountCertification__c, ';
        fieldNames += 'FundingAmountStudent__c, ';
        
        fieldNames += 'IncomeSharePostCertification__c, ';
        fieldNames += 'IncomeShareCertification__c, ';
        fieldNames += 'IncomeShareStudent__c, ';
        
        fieldNames += 'PaymentTermPostCertification__c, ';
        fieldNames += 'PaymentTermCertification__c, ';
        fieldNames += 'PaymentTermStudent__c, ';
        
        fieldNames += 'PaymentCapPostCertification__c, ';
        fieldNames += 'PaymentCapCertification__c, ';
        fieldNames += 'PaymentCapStudent__c, ';
        
        fieldNames += 'ExpectedGraduationDate__c ';
        
        return fieldNames;
    }
    
    public static String generateLIMITStatement(){
        String lim;
        if(DatabaseUtil.getPrimaryResource() == 'bulkAgreement') {
            lim = 'LIMIT '+ DatabaseUtil.getPageSize() + ' OFFSET ' + DatabaseUtil.getOffset();
        }else {
            lim = 'LIMIT 50000';
        }
        return lim;
    }
    
    public virtual class AgreementResourceOutputV2{
        public String agreementID {get;set;}
        public String programName {get;set;}
        public String programNumber {get;set;}
        public String studentName {get; set;}
        public String studentID {get;set;}
        public String programID {get;set;}
        public String majorID {get;set;}
        public String majorName {get;set;}
        public String vemoContractNumber {get;set;}
        public String agreementStatus {get;set;}
        public Decimal requestedAmount {get;set;}
        public Decimal amountDisbursedToDate {get;set;}
        public String studentProgramNumber {get;set;}
        public Decimal fundingAmount {get;set;}
        public Decimal incomeShare {get;set;}
        public Decimal paymentTerm {get;set;}
        public Decimal paymentCap {get;set;}
        public Date expectedGraduationDate {get;set;}
        
        public AgreementResourceOutputV2(StudentProgram__c studProg){
            this.agreementID = studProg.id;
            this.programName = studProg.Program__r.ProgramName__c;
            this.programNumber = studProg.Program__r.Name;
            this.studentName = studProg.Student__r.Name;
            this.studentID = studProg.Student__c;
            this.programID = studProg.Program__c;
            this.vemoContractNumber = studProg.VemoContractNumber__c;
            this.agreementStatus = studProg.Status__c;
            this.amountDisbursedToDate = studProg.AmountDisbursedToDate__c; 
            this.studentProgramNumber = studProg.Name;
            
            Set<String> completeStatuses = new Set<String>{'Certified', 'Partially Funded', 'Fully Funded', 'Grace', 'Deferment', 'Payment', 'Forgiven', 'Contract Satisfied'};
            Set<String> preCertified = new Set<String>{'Invited', 'Application Incomplete', 'Application Complete'};
            //Take Post Certification Values if they exist
            if(completeStatuses.contains(studProg.Status__c)){
                this.majorID = studProg.MajorPostCertification__c;
                this.majorName = studProg.MajorPostCertification__r.name;
                this.requestedAmount = studProg.FundingAmountPostCertification__c;
                this.fundingAmount = studProg.FundingAmountPostCertification__c;
                this.incomeShare = studProg.IncomeSharePostCertification__c;
                this.paymentTerm = studProg.PaymentTermPostCertification__c;
                this.paymentCap = studProg.PaymentCapPostCertification__c;

            }
            //Otherwise take Precertification values
            if(preCertified.contains(studProg.Status__c)){
                if(this.majorID == null){ 
                    this.majorID = studProg.MajorPreCertification__c;
                    this.majorName = studProg.MajorPreCertification__r.name;
                }
                if(this.requestedAmount == null)  this.requestedAmount = studProg.FundingAmountCertification__c;
                if(this.fundingAmount == null)  this.fundingAmount = studProg.FundingAmountCertification__c;
                if(this.incomeShare == null) this.incomeShare = studProg.IncomeShareCertification__c;
                if(this.paymentTerm == null) this.paymentTerm = studProg.PaymentTermCertification__c;
                if(this.paymentCap == null) this.paymentCap = studProg.PaymentCapCertification__c;
            }
            //Otherwise take student values
            if(this.majorID == null){
                this.majorID = studProg.MajorStudent__c;
                this.majorName = studProg.MajorStudent__r.name;
            }
            if(this.requestedAmount == null)  this.requestedAmount = studProg.FundingAmountStudent__c;
            if(this.fundingAmount == null)  this.fundingAmount = studProg.FundingAmountStudent__c;
            if(this.incomeShare == null) this.incomeShare = studProg.IncomeShareStudent__c;
            if(this.paymentTerm == null) this.paymentTerm = studProg.PaymentTermStudent__c;
            if(this.paymentCap == null) this.paymentCap = studProg.PaymentCapStudent__c;
            
            this.expectedGraduationDate = studProg.ExpectedGraduationDate__c;
        }
    }    

}