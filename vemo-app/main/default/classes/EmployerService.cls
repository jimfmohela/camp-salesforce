public with sharing class EmployerService{

    public static List<Employer> getEmployerWithStudentID(set<Id> studentIDs){    
        Map<Id, Employer__c> employerMap = EmployerQueries.getEmployerMapWithStudentID(studentIDs);
        List<Employer> employerList = new List<Employer>();
        for(Employer__c employer : employerMap.values()){
            employerList.add(new employer(employer));
        }
        return employerList;
    }
    
    public static List<Employer> getEmployerWithEmployerID(set<Id> employerIDs){    
        Map<Id, Employer__c> employerMap = EmployerQueries.getEmployerMapWithEmployerId(employerIDs);
        List<Employer> employerList = new List<Employer>();
        for(Employer__c employer : employerMap.values()){
            employerList.add(new employer(employer));
        }
        return employerList;
    }
    
    public static Set<ID> createEmployer(List<Employer> employerList){
        System.debug('EmployerService.createEmployers()');
        List<Employer__c> newEmployers = new List<Employer__c>();
        for(Employer emp : employerList){
            Employer__c newEmployer = employerToEmployer(emp);
            newEmployers.add(newEmployer);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newEmployers);
        Set<ID> employerIDs = new Set<ID>();
        for(Employer__c emp : newEmployers){
            employerIDs.add(emp.ID);
        }
        return employerIDs;
    }
    
    public static Set<ID> updateEmployer(List<Employer> employerList){
        System.debug('EmployerService.updateEmployers()');
        List<Employer__c> newEmployers = new List<Employer__c>();
        for(Employer emp : employerList){
            Employer__c newEmployer = employerToEmployer(emp);
            newEmployers.add(newEmployer);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newEmployers);
        Set<ID> employerIDs = new Set<ID>();
        for(Employer__c emp : newEmployers){
            employerIDs.add(emp.ID);
        }
        return employerIDs;
    }
    
    public static Integer deleteEmployer(Set<ID> employersIDs){
        System.debug('EmployerService.deleteEmployers()');
        Map<ID, Employer__c> employerMap = EmployerQueries.getEmployerMapWithEmployerID(employersIDs);
        Integer numToDelete = employerMap.size();
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.deleteRecords(employerMap.values());
        //delete ccMap.values();
        return numToDelete;
    }
    
    public static Employer__c employerToEmployer(Employer emp){
        Employer__c empObj = new Employer__c();
        if(emp.employerID != null) empObj.ID = emp.employerID; 
        if(emp.employerName != null) empObj.Name = emp.employerName;
        if(emp.jobTitle != null) empObj.JobTitle__c = emp.jobTitle; 
        if(emp.monthlyIncome != null) empObj.MonthlyIncome__c = emp.monthlyIncome;
        if(emp.studentID != null) empObj.Student__c = emp.studentId;
        if(emp.deleted != null) empObj.Deleted__c = emp.deleted;
        return empObj;
    } 
    
    public class Employer{
        public String employerID {get;set;}
        public String employerName {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String jobTitle {get;set;}
        public String studentID {get;set;}
        public Decimal monthlyIncome {get;set;}
        public Decimal transactionCount {get;set;}
        public Boolean deleted {get;set;}
        
        public Employer(){}
        
        public Employer(Employer__c emp){
            this.employerID = emp.Id;
            this.employerName = emp.Name;
            this.employmentEndDate = emp.EmploymentEndDate__c;
            this.employmentStartDate = emp.EmploymentStartDate__c;
            this.jobTitle = emp.JobTitle__c;
            this.studentID = emp.Student__c;
            this.monthlyIncome = emp.MonthlyIncome__c; 
            this.transactionCount = emp.TransactionCount__c;
            this.deleted = emp.deleted__c;
        }
    
    }            

}