@isTest
public class EmploymentHistoryService2_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEmploymentHistoryWithEmployentHistoryID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        List<EmploymentHistoryService2.EmploymentHistory> resultEmpHisList = EmploymentHistoryService2.getEmploymentHistoryWithEmployentHistoryID(testEmpHisMap.keySet());
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisList.size());
        Test.stopTest();
    }

    static testMethod void testGetEmploymentHistoryWithStudentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        List<EmploymentHistoryService2.EmploymentHistory> resultEmpHisList = EmploymentHistoryService2.getEmploymentHistoryWithStudentID(testStudentAccountMap.keySet());//, 'false', 'false', '', '', 'false', 'false', 'false', 'false');
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisList.size());
        Test.stopTest();
    }
    
    static testMethod void testCreateEmploymentHistory(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        //Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        List<EmploymentHistoryService2.EmploymentHistory> empHisList = new List<EmploymentHistoryService2.EmploymentHistory>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmploymentHistoryService2.EmploymentHistory empHis = new EmploymentHistoryService2.EmploymentHistory(true);
            empHis.studentID = testStudentAccountMap.values().get(i).Id;
            empHisList.add(empHis);
        }
        Test.startTest();
        Set<ID> empHisIDs = EmploymentHistoryService2.createEmploymentHistory(empHisList);
        System.assertEquals(empHisList.size(), EmploymentHistoryQueries2.getEmploymentHistoryMap().size());
        Test.stopTest();
    }

    static testMethod void testUpdateEmploymentHistory(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        List<EmploymentHistoryService2.EmploymentHistory> empHisList = new List<EmploymentHistoryService2.EmploymentHistory>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmploymentHistoryService2.EmploymentHistory empHis = new EmploymentHistoryService2.EmploymentHistory(true);
            empHis.employmentHistoryID = testEmpHisMap.values().get(i).Id;
            //empHis.verified = false;
            empHisList.add(empHis);
        }
        Test.startTest();
        Set<ID> empHisIDs = EmploymentHistoryService2.updateEmploymentHistory(empHisList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, empHisList.size());
        //for(EmploymentHistory__c empHis : EmploymentHistoryQueries.getEmploymentHistoryMap().values()){
        //    System.assert(!empHis.Verified__c);
        //}
    }

    static testMethod void testDeleteEmploymentHistory(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Integer deleted = EmploymentHistoryService2.deleteEmploymentHistory(testEmpHisMap.keySet());        
        Test.stopTest();
        System.assertEquals(testEmpHisMap.keySet().size(), deleted);
        System.assertEquals(0, EmploymentHistoryQueries2.getEmploymentHistoryMap().size());
    }
    
    

}