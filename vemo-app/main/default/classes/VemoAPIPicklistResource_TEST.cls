/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIPicklistResource_TEST
//
// Description:
// This is a test class for "VemoAPIPicklistResource"
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-09-19   Ranjeet Kumar   Created
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIPicklistResource_TEST {
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    static VemoAPI.APIInfo initializeAPI(String version, String method, Map<String, String> params, String body){
        VemoAPI.APIInfo apiInfo = new VemoAPI.APIInfo();
        apiInfo.method = method;
        apiInfo.version = version;
        apiInfo.params = params;
        apiInfo.body = body;
        return apiInfo;
    }
    
    
    static testMethod void handleGetV2_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TestDataFactory.createAndInsertDefaultResourceAttributeSettings();
        Map<String, String> picklistParams = new Map<String, String>();
        picklistParams.put('VEMO_AUTH', 'ABC');
        picklistParams.put('resource', 'agreement');
        picklistParams.put('attribute', 'cancelationreason');
        VemoAPI.APIInfo agrApiInfo = initializeAPI('v2', 'GET', picklistParams, null);
        
        Test.startTest();
        VemoAPI.ResultResponse returnResult = (VemoAPI.ResultResponse)VemoAPIPicklistResource.handleAPI(agrApiInfo);
        System.assertNotEquals(null, returnResult.numberOfResults);
        Test.stopTest();

    }
    
    static testMethod void handleGetV2WithInvalidParams_Test(){
        Map<String, String> picklistParams = new Map<String, String>();
        picklistParams.put('VEMO_AUTH', 'ABC');
        picklistParams.put('resource', 'invalidResource');
        picklistParams.put('attribute', 'invalidAttribute');
        VemoAPI.APIInfo agrApiInfo = initializeAPI('v2', 'GET', picklistParams, null);
        
        Test.startTest();
        //Will throw custom error, so using try-catch
        VemoAPI.ResultResponse returnResult = null;
        try {
            returnResult = (VemoAPI.ResultResponse)VemoAPIPicklistResource.handleAPI(agrApiInfo);
        } catch(Exception e){
            System.assertEquals(null, returnResult);
        }
        Test.stopTest();
    }
    
    static testMethod void handleGetV2WithMissingParams_Test(){
        Map<String, String> picklistParams = new Map<String, String>();
        picklistParams.put('VEMO_AUTH', 'ABC');
        picklistParams.put('resource', 'agreement');
        VemoAPI.APIInfo agrApiInfo = initializeAPI('v2', 'GET', picklistParams, null);
        
        Test.startTest();
        //Will throw custom error, so using try-catch
        VemoAPI.ResultResponse returnResult = null;
        try {
            returnResult = (VemoAPI.ResultResponse)VemoAPIPicklistResource.handleAPI(agrApiInfo);
        } catch(Exception e){
            System.assertEquals(null, returnResult);
        }
        Test.stopTest();
    }
    
    static testMethod void handleGetV2WithUnsupportedMethod_Test(){
        TestDataFactory.createAndInsertDefaultResourceAttributeSettings();
        Map<String, String> picklistParams = new Map<String, String>();
        picklistParams.put('VEMO_AUTH', 'ABC');
        picklistParams.put('resource', 'agreement');
        picklistParams.put('attribute', 'cancelationreason');
        VemoAPI.APIInfo agrApiInfo = initializeAPI('v2', 'POST-Test', picklistParams, null);
        
        Test.startTest();
        //Will throw custom error, so using try-catch
        VemoAPI.ResultResponse returnResult = null;
        try {
            returnResult = (VemoAPI.ResultResponse)VemoAPIPicklistResource.handleAPI(agrApiInfo);
        } catch(Exception e){
            System.assertEquals(null, returnResult);
        }
        Test.stopTest();
    }
}