@isTest
public class VemoAPIFFQuizResource_TEST {
  @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testHandleGetV1(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);

        Map<String, String> quizParams = new Map<String, String>();
        quizParams.put('agreementID', studentPrgMap.values()[0].id);
        quizParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo quizApiInfo = TestUtil.initializeAPI('v1', 'GET', quizParams, null);


        Test.startTest();
        VemoAPI.ResultResponse quizResult = (VemoAPI.ResultResponse)VemoAPIFFQuizResource.handleAPI(quizApiInfo);
        System.assertEquals(10, quizResult.numberOfResults);
        Test.stopTest();
    }
    
    static testMethod void testHandlePostV1(){
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        
        Map<ID, FFQuizAttempt__c> testQuizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<ID, FFQuizResponse__c> testQuizResponseMap = TestDataFactory.createAndInsertQuizResponse(testQuizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
        
        List<VemoAPIFFQuizResource.FFQuizResponse> qrList = new List<VemoAPIFFQuizResource.FFQuizResponse>();
        for(FFQuizResponse__c f : testQuizResponseMap.values()){            
            VemoAPIFFQuizResource.FFQuizResponse qr = new VemoAPIFFQuizResource.FFQuizResponse();
            qr.fFQuizResponseID = null;
            qrList.add(qr);            
        }
        
        List<VemoAPIFFQuizResource.FFQuizAttempt> qaList = new List<VemoAPIFFQuizResource.FFQuizAttempt>();        
        
        for(FFQuizAttempt__c f : testQuizAttemptMap.values()){            
            VemoAPIFFQuizResource.FFQuizAttempt qa = new VemoAPIFFQuizResource.FFQuizAttempt();
            qa.fFQuizAttemptID = null;
            if(qaList.size() == 0)
                qa.quizResponses = qrList;
            else
                qa.quizResponses = new List<VemoAPIFFQuizResource.FFQuizResponse>();
            qaList.add(qa);
            
        }
        String body = JSON.serialize(qaList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIFFQuizResource.handleAPI(apiInfo);
        Test.stopTest();
    }
    
    static testMethod void testHandlePutV1(){
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        
        Map<ID, FFQuizAttempt__c> testQuizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<ID, FFQuizResponse__c> testQuizResponseMap = TestDataFactory.createAndInsertQuizResponse(testQuizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
        
        List<VemoAPIFFQuizResource.FFQuizResponse> qrList = new List<VemoAPIFFQuizResource.FFQuizResponse>();
        for(FFQuizResponse__c f : testQuizResponseMap.values()){            
            VemoAPIFFQuizResource.FFQuizResponse qr = new VemoAPIFFQuizResource.FFQuizResponse();
            qr.fFQuizResponseID = f.id;
            qrList.add(qr);            
        }
        
        List<VemoAPIFFQuizResource.FFQuizAttempt> qaList = new List<VemoAPIFFQuizResource.FFQuizAttempt>();        
        
        for(FFQuizAttempt__c f : testQuizAttemptMap.values()){            
            VemoAPIFFQuizResource.FFQuizAttempt qa = new VemoAPIFFQuizResource.FFQuizAttempt();
            qa.fFQuizAttemptID = f.id;
            if(qaList.size() == 0)
                qa.quizResponses = qrList;
            else
                qa.quizResponses = new List<VemoAPIFFQuizResource.FFQuizResponse>();
            qaList.add(qa);
            
        }
        String body = JSON.serialize(qaList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIFFQuizResource.handleAPI(apiInfo);
        Test.stopTest();
    }
    
    static testMethod void testHandleDeleteV1(){
        
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        
        Map<ID, FFQuizAttempt__c> testQuizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        
        Map<String, String> params = new Map<String, String>();
        //params.put('VEMO_AUTH', 'testStudent_'+'abc');
        params.put('ffQuizAttemptID', testQuizAttemptMap.values().get(0).ID);

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIFFQuizResource.handleAPI(apiInfo);
        //System.assertEquals(1, result.numberOfResults);
        Test.stopTest();
    }
}