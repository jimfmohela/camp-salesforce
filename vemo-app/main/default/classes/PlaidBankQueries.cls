public class PlaidBankQueries {

    public static Map<ID, PlaidBank__c> getPlaidBankMapWithStudentID(set<Id> studentIDSet){
        Map<ID, PlaidBank__c> plaidBankMap = new Map<ID, PlaidBank__c>();
        String query = generateSOQLSelect();
        query += 'WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDSet);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        plaidBankMap = new Map<ID, PlaidBank__c>((List<PlaidBank__c>)db.query(query));
        return plaidBankMap;
    }
    
    public static Map<ID, PlaidBank__c> getPlaidBankMapWithId(set<Id> plaidBankIds){
        Map<ID, PlaidBank__c> plaidBankMap = new Map<ID, PlaidBank__c>();
        String query = generateSOQLSelect();
        query += 'WHERE Id IN ' + DatabaseUtil.inSetStringBuilder(plaidBankIds);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        plaidBankMap = new Map<ID, PlaidBank__c>((List<PlaidBank__c>)db.query(query));
        return plaidBankMap;
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM PlaidBank__c ';
        return soql;
    }
    
    private static String getFieldNames(){
        String fieldNames;
        
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'AccountNumber__c, ';
        fieldNames += 'AccessToken__c, ';
        fieldNames += 'ItemID__c, ';
        fieldNames += 'PlaidStatus__c, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'UpdatePlaidPassword__c ';
        return fieldNames;
   }
   
   
    public static String generateLIMITStatement(){
        String lim;
        lim = 'LIMIT 50000';
        
        return lim;
    }

}