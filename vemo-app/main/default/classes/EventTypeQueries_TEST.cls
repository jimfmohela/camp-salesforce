/////////////////////////////////////////////////////////////////////////
// Class: EventTypeQueries_TEST
// 
// Description: 
//  Unit test for EventTypeQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventTypeQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventTypeMap(){
        Map<Id, EventType__c> testEvntTypeMap = TestDataFactory.createAndInsertEventType(1);
        Test.startTest();
        Map<Id, EventType__c> resultEvntTypeMap = EventTypeQueries.getAllEventTypesMap();
        System.assertEquals(testEvntTypeMap.keySet().size(), resultEvntTypeMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testGetEventTypeMapWithEventTypeID(){
        Map<Id, EventType__c> testEvntTypeMap = TestDataFactory.createAndInsertEventType(1);
        Test.startTest();
        Map<Id, EventType__c> resultEvntTypeMap = EventTypeQueries.GetEventTypeMapWithEventTypeID(testEvntTypeMap.keyset());
        System.assertEquals(testEvntTypeMap.keySet().size(), resultEvntTypeMap.keySet().size());
        Test.stopTest();
    }
}