/////////////////////////////////////////////////////////////////////////
// Class: EventInstanceQueries_TEST
// 
// Description: 
//  Unit test for EventInstanceQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventInstanceQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventInstanceMapForAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap,Events );
        Test.startTest();
        Map<Id, EventInstance__c> resultEventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(testContactMap.keyset());
        System.assertEquals(testEventInstanceMap.keySet().size(), resultEventInstanceMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testEventInstanceMapForAuthUserwithRead(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        Test.startTest();
        Map<Id, EventInstance__c> resultEventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(testContactMap.keyset(), true);
        System.assertEquals(testEventInstanceMap.keySet().size(), resultEventInstanceMap.keySet().size());
        Test.stopTest();
    }
}