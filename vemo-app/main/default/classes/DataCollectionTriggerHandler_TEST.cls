@isTest

public class DataCollectionTriggerHandler_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();

    static testMethod void validateManageSharing(){
       DatabaseUtil.setRunQueriesInMockingMode(false);
       dbUtil.queryExecutor = new UserContext();
       
       User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
       }
       System.RunAs(USerWithRole){
       Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
       Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
       Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(1);
       for(account student: StudentMap.values()){
           student.PrimarySchool__pc = SchoolMap.values()[0].id;
       }
       update StudentMap.values(); 
       Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
       Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
       Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, StudentMap , programMap);
       
      /* Accessrule__c rule = new AccessRule__c();
       rule.StudentProgram__c = agreementMap.values()[0].id;
       rule.Account__c = agreementMap.values()[0].Program__r.School__c;
       rule.AccountEditAccess__c = true;
       
       Accessrule__c rule1 = new AccessRule__c();
       rule1.StudentProgram__c = agreementMap.values()[1].id;
       rule1.Account__c = agreementMap.values()[1].Program__r.School__c;
       rule1.AccountEditAccess__c = true;
       
       List<AccessRule__c> rules = new List<AccessRule__c>();
       rules.add(rule);
       rules.add(rule1);*/
       
       //Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
       
       List<DataCollection__c> dcsObjList = new List<DataCollection__c>();
       DataCollection__c dcObj = new DataCollection__c ();
       dcObj.Template__c = TemplateMap.values()[0].id;
       dcObj.Agreement__c = agreementMap.values()[0].id;
       dcObj.DataType__c = 'String';
       dcObj.Label__c = 'Test Question__c';
       dcObj.StringValue__c = 'Test answer'; 
       dcsObjList.add(dcObj);
       if(TriggerSettings.getSettings().DataCollectionTrigger != true){
           TriggerSettings.getSettings().DataCollectionTrigger = true;
       }
       dbUtil.insertRecords(dcsObjList);
       }
    }
}