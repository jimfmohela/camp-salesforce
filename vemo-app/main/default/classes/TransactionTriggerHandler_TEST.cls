@isTest
public with sharing class TransactionTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        TestUtil.createStandardTestConditions();
        Integer numberOfTestRecords = TestUtil.TEST_THROTTLE;
        if(numberOfTestRecords > 2){
            numberOfTestRecords = 2;
        }
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(numberOfTestRecords);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(numberOfTestRecords);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(2, agreements, TransactionService.disbursementRecType);
        TestUtil.setStandardConfiguration();
        }
    }

    @isTest public static void validateDML(){
    
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(studentMap.keySet(), TransactionService.disbursementRecType);
        //update txMap.values();
        //delete txMap.values();
        //undelete txMap.values();
        Test.startTest();
        dbUtil.updateRecords(txMap.values());
        dbUtil.deleteRecords(txMap.values());
        dbUtil.undeleteRecords(txMap.values());
        Test.stopTest();
    }


}