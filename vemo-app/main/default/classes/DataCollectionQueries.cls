public class DataCollectionQueries {
    
    public static Map<ID, DataCollection__c> getDataCollectionMapWithID(set<Id> dataCollectionIds){
        
        Map<ID, DataCollection__c> DataCollectionMap = new Map<ID, DataCollection__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(dataCollectionIds);
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        DataCollectionMap = new Map<ID, DataCollection__c>((List<DataCollection__c>)db.query(query));
        return DataCollectionMap;  
    }
    public static Map<ID, DataCollection__c> getDataCollectionMapWithAgreement(set<Id> agreementIds){
        
        Map<ID, DataCollection__c> DataCollectionMap = new Map<ID, DataCollection__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Agreement__c IN ' + DatabaseUtil.inSetStringBuilder(agreementIds);
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        DataCollectionMap = new Map<ID, DataCollection__c>((List<DataCollection__c>)db.query(query));
        return DataCollectionMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM DataCollection__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Agreement__c, ';
        fieldNames += 'BooleanValue__c, ';
        fieldNames += 'DataType__c, ';
        fieldNames += 'DateValue__c, ';
        fieldNames += 'Label__c, ';
        fieldNames += 'PicklistValue__c, ';
        fieldNames += 'StringValue__c, '; 
        fieldNames += 'Template__c ';
        return fieldNames;
   }
   
   
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }
  
      
}