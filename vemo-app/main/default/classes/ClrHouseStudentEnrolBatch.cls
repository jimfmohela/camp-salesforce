global class ClrHouseStudentEnrolBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful {
    public enum JobType {CLEARINGHOUSE} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    public ClrHouseStudentEnrolBatch(){}
    global ClrHouseStudentEnrolBatch (string query) {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(job == JobType.CLEARINGHOUSE){
            if(String.isEmpty(this.query)){
                query = 'SELECT id,RequestorReturnField__c,RequestorReturnFieldId__c from ClrHouseStudentEnrollment__c where Processed__c = false';
            }
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(job == JobType.CLEARINGHOUSE){
            handleClearingHouse((List<ClrHouseStudentEnrollment__c>)scope);
        }
    }
  
    global void finish(Database.BatchableContext BC) {
    }
    
    private static void handleClearingHouse(List<ClrHouseStudentEnrollment__c> clearingHouses){
        set<string> accountVemoNumberSet = new set<string>();
        List<ClrHouseStudentEnrollment__c> ClrHouseStudentEnrollmentList = new List<ClrHouseStudentEnrollment__c>();
        
        for(ClrHouseStudentEnrollment__c c: clearingHouses){
            if(c.RequestorReturnFieldId__c != null){
                accountVemoNumberSet.add(c.RequestorReturnFieldId__c);    
            }
        }
        
        if(accountVemoNumberSet != null && accountVemoNumberSet.size()>0){
            Map<id,account> accountMap = AccountQueries.getStudentMapWithVemoAccountNumber(accountVemoNumberSet);
            
            Map<string,account> vemoAccountNumberNAccountMap = new Map<string,account>();
            if(accountMap != null && accountMap.size()>0){
                for(Id i : accountMap.keyset()){
                    if(accountMap.get(i).VemoAccountNumber__c != null)
                        vemoAccountNumberNAccountMap.put(accountMap.get(i).VemoAccountNumber__c,accountMap.get(i));        
                }
            }
            for(ClrHouseStudentEnrollment__c c: clearingHouses){
                if(c.RequestorReturnFieldId__c != null && vemoAccountNumberNAccountMap.get(c.RequestorReturnFieldId__c) != null){
                    c.account__c = vemoAccountNumberNAccountMap.get(c.RequestorReturnFieldId__c).id;
                    c.Processed__c = true;
                    ClrHouseStudentEnrollmentList.add(c);
                }
            }
            
            if(ClrHouseStudentEnrollmentList != null && ClrHouseStudentEnrollmentList.size()>0)
                upsert ClrHouseStudentEnrollmentList;
        }
    }
}