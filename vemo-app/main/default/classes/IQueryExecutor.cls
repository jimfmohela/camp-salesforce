public interface IQueryExecutor{
    List<SObject> databaseQuery(string query);
    void updateRecords(List<SObject> objs);
    void updateRecord(SObject obj);
    void upsertRecords(List<SObject> objs);
    void upsertRecord(SObject obj);    
    void deleteRecords(List<SObject> objs);
    void deleteRecord(SObject obj);
    void insertRecords(List<SObject> objs);
    void insertRecord(SObject obj);
    void undeleteRecords(List<SObject> objs);
    void undeleteRecord(SObject objs);
}