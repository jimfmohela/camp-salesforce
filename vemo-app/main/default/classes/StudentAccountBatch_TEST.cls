@isTest
public with sharing class StudentAccountBatch_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @testSetup static void createSetUpRecord(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'Annual_Account_Statement'});
    }

   @isTest static void recurringPaymentGeneartion(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){ 
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        for(Account student : students.values()){
            student.AutoPayment__pc = true;
            student.AutoPaymentFrequency__pc = '1 Time Per Month';
            student.AutoPaymentDayOfMonth1__pc = '12';
        }
        //update students.values();
        dbUtil.updateRecords(students.values());

        //System.assertEquals(students.size(), TestUtil.TEST_THROTTLE);
        System.assertEquals(students.values()[0].AutoPayment__pc, true);

        Map<ID, PaymentMethod__c> paymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, students);
        for(PaymentMethod__c payment : paymentMethodMap.values()){
            payment.UseForRecurring__c = true;
        }
        //update paymentMethodMap.values();
        dbUtil.updateRecords(paymentMethodMap.values());
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        for(StudentProgram__c sp : studentPrgMap.values()){
            sp.MonthlyAmountDueToDate__c = 1000;
        }
        //update studentPrgMap.values();
        dbUtil.updateRecords(studentPrgMap.values());
        System.assertEquals(studentPrgMap.values()[0].MonthlyAmountDueToDate__c, 1000);

        Map<ID, PaymentInstruction__c> paymntInstrxnMap = TestDataFactory.createAndInsertPaymentInstruction(TestUtil.TEST_THROTTLE, students, paymentMethodMap);
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch();
        job.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }

    /*recurringPaymentGeneartion without recurring payment method, this will create case record */
    @isTest static void RPG_withoutRecurringPaymentMethod(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        for(Account student : students.values()){
            student.AutoPayment__pc = true;
            student.AutoPaymentFrequency__pc = '1 Time Per Month';
            student.AutoPaymentDayOfMonth1__pc = '12';
        }
        //update students.values();
        dbUtil.updateRecords(students.values());
        //System.assertEquals(students.size(), TestUtil.TEST_THROTTLE);
        System.assertEquals(students.values()[0].AutoPayment__pc, true);

        Map<ID, PaymentMethod__c> paymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);

        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch();
        job.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }

    @isTest static void recurringPaymentGeneartion_withBlankQuery(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'rini');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }

    @isTest static void AnnualStatementGeneration_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Integer i = 0;
        for(Account stud : students.values()){
            i ++;
            stud.PersonEmail = 'TestEmail'+ i + '@testdomain.com';
            stud.Receive_Annual_Statement__c = true;
        }
        //update students.values();
        dbUtil.updateRecords(students.values());
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.ANNUAL_STATEMENT_GENERATION;
        Database.executeBatch(job, 200);        
        Test.stopTest();
        }
    }

    @isTest static void CumulativeProgramFunding_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrograms = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        for(StudentProgram__c sp : studentPrograms.values()){
            sp.FundingAmountPostCertification__c = 100000;
        }
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.CUMULATIVE_PROGRAM_FUNDING;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }
    
    
    @isTest static void SetClearinghouseExtractFlag_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrograms = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, Transaction__c> disbursements = TestDataFactory.createAndInsertTransactions(1, studentPrograms, 'Disbursement');
        
        // for(Account sch: schools.values()){
        //     sch.SchoolSecurity__C = 'Purdue';
        // }
        //update schools.values();
        dbUtil.updateRecords(schools.values());
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.CLEARINGHOUSE;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
        
    }
    
    @isTest static void ResetClearinghouseExtractFlag_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrograms = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, Transaction__c> disbursements = TestDataFactory.createAndInsertTransactions(1, studentPrograms, 'Disbursement');
        
        // for(Account sch: schools.values()){
        //     sch.SchoolSecurity__C = 'Purdue';
        // }
        //update schools.values();
        dbUtil.updateRecords(schools.values());
        
        
        Test.startTest();
        for(StudentProgram__c sp : studentPrograms.values()){
            sp.Status__c = 'Cancelled';
        }
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.CLEARINGHOUSE;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }

    //test SUMMARIZE_INCOME Method
    @isTest static void testSUMMARIZE_INCOMEMethod(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'rini');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        for(Account student : students.values()){
            student.SummarizeIncome__c = true;
        }
        //update students.values();
        dbUtil.updateRecords(students.values());
        System.assertEquals(students.size(), TestUtil.TEST_THROTTLE);

        Map<ID, EmploymentHistory__c> empHistMap = TestDataFactory.createAndInsertEmploymentHistory(1, students);
        for(EmploymentHistory__c emp: empHistMap.values()){
            emp.verified__c = true;
            emp.EffectiveDate__c = Date.today().addDays(-1);
        }
        //update empHistMap.values();
        dbUtil.updateRecords(empHistMap.values());
        Map<Id, IncomeVerification__c> incVerMap = TestDataFactory.createAndInsertIncomeVerification(1, empHistMap);
        for(IncomeVerification__c iv: incVerMap.values()){
            iv.BeginDate__c = date.today().addMonths(2);
        }
        //update incVerMap.values();
        dbUtil.updateRecords(incVerMap.values());
        Test.startTest();
        StudentAccountBatch job = new StudentAccountBatch('');
        job.job = StudentAccountBatch.JobType.SUMMARIZE_INCOME;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }
}