public with sharing class ReportCreditCheck{
    public transient List<CreditCheck__c> ccList {get;set;}
    public String csv {get;set;}
    
    transient public Date fromcreditReportDateTimeStamp{get;set;}
    transient public Date tocreditReportDateTimeStamp{get;set;}
    
    public ReportCreditCheck(){
        fromcreditReportDateTimeStamp = date.today().addmonths(-1);
        tocreditReportDateTimeStamp = date.today();
        ccList = new List<CreditCheck__c>();  
        runReport();      
    }
    
    public void runReport(){
        tocreditReportDateTimeStamp = tocreditReportDateTimeStamp.addDays(1);
        ccList = [Select ID,Name,Student__c,CreditReportDateTimeStamp__c,
                  (Select Id,Name,ParentId FROM Attachments), 
                  (Select ID,Name,CreditCheck__c,Status__c,CreditCheckProcess__c FROM Credit_Check_Evaluations__r) 
                  FROM CreditCheck__c
                  Where CreditReportDateTimeStamp__c >=: fromcreditReportDateTimeStamp AND CreditReportDateTimeStamp__c <=: tocreditReportDateTimeStamp
                  ORDER BY Name desc];
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportCreditCheckExport');
        pg.setRedirect(false);
        return pg;
    }
    
    public void buildCsvString(){
        csv = 'Credit Check Number,Credit Report Date Time Stamp,Attachments Names,Credit Check Evaluations Numbers';
        csv += '\n';
        runReport();
        for(CreditCheck__c cc1 : ccList){
            String all_Attachment_Names = ''; String all_CCE_Numbers = '';
            
            for(Attachment att1 : cc1.Attachments){all_Attachment_Names = att1.Name + ', ' + all_Attachment_Names;}
            
            System.debug('CCE*** ' + cc1.Credit_Check_Evaluations__r.size());
            for(CreditCheckEvaluation__c cce1 : cc1.Credit_Check_Evaluations__r){all_CCE_Numbers = cce1.Name + ', ' + all_CCE_Numbers;}
            all_Attachment_Names = '"' + all_Attachment_Names + '"';  all_CCE_Numbers = '"' + all_CCE_Numbers + '"';
            
            csv += cc1.Name + ','; if(cc1.CreditReportDateTimeStamp__c != null) csv += cc1.CreditReportDateTimeStamp__c +','; else csv += ','; csv += all_Attachment_Names + ','; csv += all_CCE_Numbers + ','; csv += '\n';
        }
    } 
}