global class EventBatch implements Database.Batchable<sObject> {
    public enum JobType {INSTANTIATE_EVENTINSTANCE}
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    public EventBatch(){
    }
    
    global EventBatch(string query) {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(job == JobType.INSTANTIATE_EVENTINSTANCE){
        Date today = Date.today();
            if(String.isEmpty(this.query)){
                query = 'SELECT id,Account__c,WhatId__c,EventType__c,EventType__r.MergeObject__c,EventType__r.MergeText__c,SubscriptionsInstantiated__c from Event__c where SubscriptionsInstantiated__c = false  order by CreatedDate desc';
            }
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(job == JobType.INSTANTIATE_EVENTINSTANCE){
            handleEventSubscriptions((List<Event__c>)scope);
        }
    }
    
    private static void handleEventSubscriptions(List<Event__c> eventsList){
        set<Id> accountIdSet = new set<Id>();
        set<Id> eventTypeIDSet = new set<Id>();
        List<EventInstance__c> eInstanceList = new List<EventInstance__c>();
        
        for(Event__c e: eventsList){
            if(e.account__c != null) accountIdSet.add(e.account__c);
            if(e.EventType__c != null) eventTypeIDSet.add(e.EventType__c);
        }
        
        Map<string, List<EventSubscription__c>> accountNETypeWithESubsciptionMap = new Map<string, List<EventSubscription__c>>();
        Map<ID, EventSubscription__c> esMap = EventSubscriptionQueries.getEventSubscriptionMapWithEventTypes(accountIdSet, eventTypeIDSet);
        
        /*for(EventSubscription__c es : esMap.Values()){
            if(es.account__c != null && es.EventType__c != null)
                accountNETypeWithESubsciptionMap.put(es.account__c+''+es.eventType__c, es);
        }*/
        
        for(EventSubscription__c es : esMap.Values()){
            if(es.account__c != null && es.EventType__c != null){
                if(accountNETypeWithESubsciptionMap.get(es.account__c+''+es.eventType__c) != null && accountNETypeWithESubsciptionMap.get(es.account__c+''+es.eventType__c).size()>0){
                    List<EventSubscription__c> esList = accountNETypeWithESubsciptionMap.get(es.account__c+''+es.eventType__c);
                    esList.add(es);
                    accountNETypeWithESubsciptionMap.put(es.account__c+''+es.eventType__c,esList);
                }
                else{
                    List<EventSubscription__c> esList = new List<EventSubscription__c>();
                    esList.add(es);
                    accountNETypeWithESubsciptionMap.put(es.account__c+''+es.eventType__c,esList);
                }
            }
        }
        
        for(Event__c e: eventsList){
            if(e.account__c != null && e.eventtype__c != null && accountNETypeWithESubsciptionMap.get(e.account__c+''+e.eventType__c) != null){
                String mergeText;
                if(e.EventType__r.MergeObject__c != null && e.EventType__r.MergeText__c != null && e.WhatId__c != null){
                    mergeText = mergeText(e.EventType__r.MergeText__c,e.EventType__r.MergeObject__c,e.WhatId__c);
                }
                
                for(EventSubscription__c es : accountNETypeWithESubsciptionMap.get(e.account__c+''+e.eventType__c)){
                    if(es.contact__c != null){
                        EventInstance__c ei = new EventInstance__c();
                        ei.Contact__c = es.Contact__c;
                        if(e.EventType__r.MergeObject__c != null && e.EventType__r.MergeText__c != null && e.WhatId__c != null){
                            ei.EventAttributes__c = mergeText; //mergeText(e.EventType__r.MergeText__c,e.EventType__r.MergeObject__c,e.WhatId__c);
                        }
                        ei.email__c = es.email__c;
                        ei.Event__c = e.Id;
                        eInstanceList.add(ei);
                    }
                }
            }
            e.SubscriptionsInstantiated__c = true;
        }
        
        if(eInstanceList.size()>0)
            insert eInstanceList;
            
        if(eventsList.size()>0)
            update eventsList;
    }
    
    //private static String replace(String emailTemplate, String objectName, String recordId){
    public static string mergeText(String messageBody,String objectName, Id recId){
        pattern p = Pattern.compile('\\{[!a-zA-Z0-9_\\s\\.]*\\}');
        //String objectName = '';
        Matcher m = p.matcher(messageBody);
        String fieldName = '';
        Map<String,Set<String>> fieldMap = new Map<String,Set<String>>();
        Map<String,String> templateBodyMap = new Map<String,String>();
        if(recId != null){
            objectName = recId.getSObjectType().getDescribe().getName();
            fieldMap.put(objectName,new Set<String>{'Id'});
        }
    
        Set<String> fieldApiSet = new Set<String>();
    
        //find merge fieds and create map of objectApi and fieldApi for dynamic soql
        while(m.find()){
            fieldName = messageBody.substring(m.start()+2,m.end()-1);
    
            //Spliting Object API and field API
            String[] apiArry = fieldName.split('\\.',2);
            if(apiArry[0] == objectName){
                if(fieldMap.containsKey(apiArry[0])){
                    fieldMap.get(apiArry[0]).add(apiArry[1]);
                }
                else{
                    fieldMap.put(apiArry[0],new Set<String>{apiArry[1]});
                }  
            }else{
                System.debug('objectName>>'+objectName);
                System.debug('fieldName>>'+fieldName);
                System.debug('fieldMap>>'+fieldMap);
                fieldMap.get(objectName).add(fieldName);
            }
        }
    
        //iterate throught map and create soql query String
        List<String> fieldList = new List<String>();        
    
        for(Set<String> fields : fieldMap.values()){
            for(String f: fields){
                fieldList.add(f);
            }
        }
    
        String queryString = ' SELECT ' + String.join( fieldList, ',' )+' FROM '+ objectName +' WHERE Id =:recId';
        System.debug('query>>'+queryString);
    
        Database.QueryLocator q = Database.getQueryLocator(queryString);
        Database.QueryLocatorIterator it =  q.iterator();
        
        integer counter;
        
        while (it.hasNext()) {
            SObject obj = (SObject)it.next();
            
            for(String field : fieldList){
                counter = 0;
                sObject sObj = obj;
                string endVal ;
                
                if(field.contains('.')){    // relationship fields
                    List<string> fieldSplits = field.split('\\.');
                    for(string fSplit : fieldSplits ){
                        if(counter == fieldSplits.size()-1) {
                            endVal = (string)sObj.get(fSplit);
                        }
                        else{
                            sObj = sobj.getSobject(fSplit);
                        }
                        counter++;   
                    }
                    messageBody = messageBody.replace('{!' + objectName + '.' + field + '}', endVal).trim(); 
                }
                else{
                    if(obj.get(field) != null && obj.get(field) != ''){
                        messageBody = messageBody.replace('{!' + objectName + '.' + field + '}', String.valueOf(obj.get(field))).trim();
                    }else{
                        messageBody = messageBody.replace('{!' + objectName + '.' + field + '}','').trim();
                    }
                }
            }
        }
    
        return messageBody;
    }
  
    global void finish(Database.BatchableContext BC) {
    }
}