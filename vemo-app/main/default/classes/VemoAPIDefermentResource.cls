/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIDefermentResource
// 
// Description: 
//  Direction Central for Deferment API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-09-24   Kamini Singh       Created                            
// 
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIDefermentResource  {//implements VemoAPI.ResourceHandler
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIDefermentResource.handleGetV1');
        String DefermentIDParam = api.params.get('DefermentID');
        String studentIDParam = api.params.get('studentID');
        List<DefermentService.Deferment> vcs = new List<DefermentService.Deferment>();
        if(DefermentIDParam != null){
            vcs = DefermentService.getDefermentMapWithDefermentID(VemoApi.parseParameterIntoIDSet(DefermentIDParam));
        }
        else if(studentIDParam != null){
            vcs = DefermentService.getDefermentMapWithStudent(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: DefermentID or studentID required for GET');
        }
        List<DefermentResourceOutputV1> results = new List<DefermentResourceOutputV1>();
        for(DefermentService.Deferment vc : vcs){
            results.add(new DefermentResourceOutputV1(vc));
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIDefermentResource.handlePostV1');
        List<DefermentService.Deferment> newDeferments = new List<DefermentService.Deferment>();
        List<DefermentResourceInputV1> DefermentsJSON = (List<DefermentResourceInputV1>)JSON.deserialize(api.body, List<DefermentResourceInputV1>.class);
        for(DefermentResourceInputV1 DefermentRes : DefermentsJSON){
            DefermentRes.validatePOSTFields();
            newDeferments.add(DefermentResourceV1ToDeferment(DefermentRes));
        }
        Set<ID> DefermentIDs = DefermentService.createDeferments(newDeferments);

        return (new VemoAPI.ResultResponse(DefermentIDs, DefermentIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIDefermentResource.handlePutV1');
        List<DefermentService.Deferment> updateDeferments = new List<DefermentService.Deferment>();
        List<DefermentResourceInputV1> DefermentsJSON = (List<DefermentResourceInputV1>)JSON.deserialize(api.body, List<DefermentResourceInputV1>.class);
        for(DefermentResourceInputV1 DefermentRes : DefermentsJSON){
            DefermentRes.validatePUTFields();
            updateDeferments.add(DefermentResourceV1ToDeferment(DefermentRes));
        }
        Set<ID> DefermentIDs = DefermentService.updateDeferments(updateDeferments);

        return (new VemoAPI.ResultResponse(DefermentIDs, DefermentIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIDefermentResource.handleDeleteV1()');            
        String DefermentIDParam = api.params.get('DefermentID');       

        Map<Id, Deferment__c> defermentMap = DefermentQueries.getDefermentMapWithDefermentID(VemoApi.parseParameterIntoIDSet(DefermentIDParam));

        Set<ID> idsToVerify = new Set<ID>();
        for(Deferment__c de: defermentMap.values()){
            idsToVerify.add(de.student__c); 
        }
        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        Set<ID> defermentsToBeDeleted = new Set<ID>();
        for(Deferment__c de: defermentMap.values()){
            if(verifiedAccountMap.containsKey(de.student__c)){
                defermentsToBeDeleted.add(de.id);
            }
        }
        Integer numToDelete = 0;
        if(defermentsToBeDeleted.size()>0){
            numToDelete = DefermentService.deleteDeferments(defermentsToBeDeleted); 
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }

    public static DefermentService.Deferment DefermentResourceV1ToDeferment(DefermentResourceInputV1 DefermentRes){
        DefermentService.Deferment de = new DefermentService.Deferment();
        de.DefermentID = DefermentRes.DefermentID ;
        de.annualIncome = DefermentRes.annualIncome ;
        de.averageHoursPerWeek = DefermentRes.averageHoursPerWeek ;
        de.averageSalaryPerMonth = DefermentRes.averageSalaryPerMonth ;
        de.beginDate = DefermentRes.beginDate ;
        de.cityEmployment = DefermentRes.cityEmployment ;
        //de.contactDate = DefermentRes.contactDate ;           
        de.countryEmployment = DefermentRes.countryEmployment ;            
        de.countrySchool = DefermentRes.countrySchool ;            
        de.dateReported = DefermentRes.dateReported ;            
        de.dateRequestSubmitted = DefermentRes.dateRequestSubmitted ;            
        de.dateVerified = DefermentRes.dateVerified ;    
        de.employerAddress = DefermentRes.employerAddress ;            
        de.employerName = DefermentRes.employerName ;  
        de.endDate = DefermentRes.endDate ;
        de.enrollmentBeginDate = DefermentRes.enrollmentBeginDate ;
        de.enrollmentEndDate = DefermentRes.enrollmentEndDate ;          
        de.enrollmentStatus  = DefermentRes.enrollmentStatus  ;            
        de.explanation = DefermentRes.explanation ;
        //de.followUpDate = DefermentRes.followUpDate ;                      
        de.hourlyCompensation = DefermentRes.hourlyCompensation ;
        de.monthlyIncome = DefermentRes.monthlyIncome ;            
        de.noPaymentPaymentTerm = DefermentRes.noPaymentPaymentTerm;
        de.receivingUnemploymentBenefits =  DefermentRes.receivingUnemploymentBenefits;
        //de.resultsOutcome = DefermentRes.resultsOutcome ;
        de.schoolAddress = DefermentRes.schoolAddress ;   
        de.schoolName = DefermentRes.schoolName ;
        de.SchoolPhone = DefermentRes.SchoolPhone ;   
        de.servicingCommentsNotes = DefermentRes.servicingCommentsNotes ;
        de.stateEmployment = DefermentRes.stateEmployment ;               
        de.stateSchool = DefermentRes.stateSchool ;            
        de.status = DefermentRes.status ;              
        de.student = DefermentRes.studentID ;             
        de.type = DefermentRes.type ;  
        de.unemploymentBeginDate = DefermentRes.unemploymentBeginDate ; 
        de.verified =  DefermentRes.verified ;
        de.otherEnrollmentStatus = DefermentRes.otherEnrollmentStatus;
        de.employerPhoneNumber = DefermentRes.EmployerPhoneNumber;
        de.employmentStartDate = DefermentRes.EmploymentStartDate ;
        de.employmentEndDate = DefermentRes.EmploymentEndDate;
        de.employmentType = DefermentRes.EmploymentType;
        //de.jobPositionTypeofWork = DefermentRes.JobPositionTypeofWork;
        de.iWork35hrsorMoreaWeek = DefermentRes.IWork35hrsorMoreaWeek;  
        de.estimatedGraduationDate = DefermentRes.estimatedGraduationDate;  
        de.SchoolCity = DefermentRes.schoolCity;    
        return de;
    }

    public class DefermentResourceInputV1{
        public String DefermentID{get;set;}
        public Decimal annualIncome{get;set;}
        public Decimal averageHoursPerWeek{get;set;}
        public Decimal averageSalaryPerMonth{get;set;}
        public Date beginDate{get;set;}
        public String cityEmployment{get;set;}
        //public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String countrySchool{get;set;}
        public Date dateReported{get;set;}
        public Date dateRequestSubmitted {get;set;}
        public Date dateVerified{get;set;}
        public String employerAddress {get;set;}        
        public String employerName {get;set;}
        public Date endDate{get;set;}
        public Date enrollmentBeginDate{get;set;}
        public Date enrollmentEndDate {get;set;}
        public String enrollmentStatus {get;set;}        
        public String explanation {get;set;}
        //public Date followUpDate{get;set;}        
        public Decimal hourlyCompensation {get;set;}
        public Decimal monthlyIncome {get;set;}
        public String noPaymentPaymentTerm {get;set;}        
        public String receivingUnemploymentBenefits {get;set;}        
        //public String resultsOutcome {get;set;}   
        public String schoolAddress {get;set;}
        public String schoolName {get;set;} 
        public String SchoolPhone {get;set;} 
        public String servicingCommentsNotes {get;set;}      
        public String stateEmployment {get;set;}           
        public String stateSchool {get;set;}        
        public String status {get;set;}         
        public String studentID {get;set;}     
        public String type {get;set;}        
        public Date unemploymentBeginDate {get;set;}
        public String verified {get;set;}
        public String otherEnrollmentStatus {get;set;}
        public String employerPhoneNumber {get;set;}
        //public String jobPositionTypeofWork {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String employmentType {get;set;}
        public Boolean iWork35hrsorMoreaWeek {get;set;}
        public Date estimatedGraduationDate {get;set;}
        public String schoolCity {get;set;}

        public DefermentResourceInputV1(Boolean testValues){
            if(testValues){
                //student = 'testStudentID';    
            }
        }

        public void validatePOSTFields(){
            if(DefermentID != null) throw new VemoAPI.VemoAPIFaultException('DefermentID cannot be created in POST');
            if(StudentID == null) throw new VemoAPI.VemoAPIFaultException('Student is a required input parameter on POST');
        }

        public void validatePUTFields(){
            if(DefermentID == null) throw new VemoAPI.VemoAPIFaultException('DefermentID is a required input parameter on PUT');
        }
    }

    public class DefermentResourceOutputV1{
        public String DefermentID{get;set;}
        public Decimal annualIncome{get;set;}
        public Decimal averageHoursPerWeek{get;set;}
        public Decimal averageSalaryPerMonth{get;set;}
        public Date beginDate{get;set;}
        public String cityEmployment{get;set;}
        //public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String countrySchool{get;set;}
        public Date dateReported{get;set;}
        public Date dateRequestSubmitted {get;set;}
        public Date dateVerified{get;set;}
        public String employerAddress {get;set;}        
        public String employerName {get;set;}
        public Date endDate{get;set;}
        public Date enrollmentBeginDate{get;set;}
        public Date enrollmentEndDate {get;set;}
        public String enrollmentStatus {get;set;}        
        public String explanation {get;set;}
        //public Date followUpDate{get;set;}        
        public Decimal hourlyCompensation {get;set;}
        public Decimal monthlyIncome {get;set;}
        public String noPaymentPaymentTerm {get;set;}        
        public String receivingUnemploymentBenefits {get;set;}        
        //public String resultsOutcome {get;set;}   
        public String schoolAddress {get;set;}
        public String schoolName {get;set;} 
        public String SchoolPhone {get;set;} 
        public String servicingCommentsNotes {get;set;}      
        public String stateEmployment {get;set;}           
        public String stateSchool {get;set;}        
        public String status {get;set;}         
        public String studentID {get;set;}     
        public String type {get;set;}        
        public Date unemploymentBeginDate {get;set;}
        public String verified {get;set;}
        public String otherEnrollmentStatus {get;set;}
        public String employerPhoneNumber {get;set;}
        //public String jobPositionTypeofWork {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String employmentType {get;set;}
        public Boolean iWork35hrsorMoreaWeek {get;set;}
        public Date estimatedGraduationDate {get;set;}
        public String schoolCity {get;set;}

        public DefermentResourceOutputV1(DefermentService.Deferment de){
            this.DefermentID = de.DefermentID ;
            this.annualIncome = de.annualIncome ;
            this.averageHoursPerWeek = de.averageHoursPerWeek ;
            this.averageSalaryPerMonth = de.averageSalaryPerMonth ;
            this.beginDate = de.beginDate ;
            this.cityEmployment = de.cityEmployment ;
            //this.contactDate = de.contactDate ;           
            this.countryEmployment = de.countryEmployment ;            
            this.countrySchool = de.countrySchool ;            
            this.dateReported = de.dateReported ;            
            this.dateRequestSubmitted = de.dateRequestSubmitted ;            
            this.dateVerified = de.dateVerified ;    
            this.employerAddress = de.employerAddress ;            
            this.employerName = de.employerName ;  
            this.endDate = de.endDate ;
            this.enrollmentBeginDate = de.enrollmentBeginDate ;
            this.enrollmentEndDate = de.enrollmentEndDate ;          
            this.enrollmentStatus  = de.enrollmentStatus  ;            
            this.explanation = de.explanation ;
            //this.followUpDate = de.followUpDate ;                      
            this.hourlyCompensation = de.hourlyCompensation ;
            this.monthlyIncome = de.monthlyIncome ;            
            this.noPaymentPaymentTerm = de.noPaymentPaymentTerm;
            this.receivingUnemploymentBenefits =  de.receivingUnemploymentBenefits;
            //this.resultsOutcome = de.resultsOutcome ;
            this.schoolAddress = de.schoolAddress ;   
            this.schoolName = de.schoolName ;
            this.SchoolPhone = de.SchoolPhone ;   
            this.servicingCommentsNotes = de.servicingCommentsNotes ;
            this.stateEmployment = de.stateEmployment ;               
            this.stateSchool = de.stateSchool ;            
            this.status = de.status ;              
            this.studentID = de.student ;             
            this.type = de.type ;  
            this.unemploymentBeginDate = de.unemploymentBeginDate ; 
            this.verified =  de.verified ;
            this.otherEnrollmentStatus = de.otherEnrollmentStatus;
            this.iWork35hrsorMoreaWeek = de.IWork35hrsorMoreaWeek;
            this.employmentType = de.EmploymentType;
            this.employmentStartDate = de.EmploymentStartDate;  
            this.employmentEndDate = de.EmploymentEndDate; 
            //this.jobPositionTypeofWork  = de.JobPositionTypeofWork;
            this.employerPhoneNumber  = de.EmployerPhoneNumber; 
            this.estimatedGraduationDate = de.EstimatedGraduationDate;
            this.schoolCity = de.SchoolCity;
        }
    }
}