/////////////////////////////////////////////////////////////////////////
// Class: InvestorOwnershipTriggerHandler
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-08-08   Greg Cook  Created                              
// 
/////////////////////////////////////////////////////////////////////////
public with sharing class InvestorOwnershipTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {
  /**************************Static Variables***********************************/

    /**************************State acctrol Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution acctrol - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.Triggercontext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.Triggercontext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'InvestorOwnershipTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'InvestorOwnershipTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.Triggercontext tc){
        System.debug('InvestorOwnershipTriggerHandler.onBeforeInsert()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> newInvestorOwnershipList = (List<InvestorOwnership__c>)tc.newList;
        //This is where you should call your business logic
        setDefaultsOnInsert(newInvestorOwnershipList);

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.Triggercontext tc){
        System.debug('InvestorOwnershipTriggerHandler.onBeforeUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> newInvestorOwnershipList = (List<InvestorOwnership__c>)tc.newList;
        List<InvestorOwnership__c> oldInvestorOwnershipList = (List<InvestorOwnership__c>)tc.oldList;
        Map<ID, InvestorOwnership__c> newInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.newMap;
        Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.oldMap;
        //This is where you should call your business logic
        
        setDefaultsOnUpdate(oldInvestorOwnershipMap, newInvestorOwnershipMap); 

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.Triggercontext tc){
    System.debug('InvestorOwnershipTriggerHandler.onBeforeDelete()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> oldInvestorOwnershipList = (List<InvestorOwnership__c>)tc.oldList;
        Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.oldMap;
        //This is where you should call your business logic

    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.Triggercontext tc){
        System.debug('InvestorOwnershipTriggerHandler.onAfterInsert()');
         //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> newInvestorOwnershipList = (List<InvestorOwnership__c>)tc.newList;
        Map<ID, InvestorOwnership__c> newInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.newMap;
        Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.oldMap;
        //This is where you should call your business logic
        //manageAccessRules(oldInvestorOwnershipMap, newInvestorOwnershipMap);

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.Triggercontext tc){
    System.debug('InvestorOwnershipTriggerHandler.onAfterUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> newInvestorOwnershipList = (List<InvestorOwnership__c>)tc.newList;
        List<InvestorOwnership__c> oldInvestorOwnershipList = (List<InvestorOwnership__c>)tc.oldList;
        Map<ID, InvestorOwnership__c> newInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.newMap;
        Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.oldMap;
        //This is where you should call your business logic

        //manageAccessRules(oldInvestorOwnershipMap, newInvestorOwnershipMap);

   }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.Triggercontext tc){
    System.debug('InvestorOwnershipTriggerHandler.onAfterDelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> oldInvestorOwnershipList = (List<InvestorOwnership__c>)tc.oldList;
        Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.oldMap;
        //This is where you should call your business logic

     }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.Triggercontext tc){
    System.debug('InvestorOwnershipTriggerHandler.onAfterUndelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<InvestorOwnership__c> newInvestorOwnershipList = (List<InvestorOwnership__c>)tc.newList;
        Map<ID, InvestorOwnership__c> newInvestorOwnershipMap = (Map<ID, InvestorOwnership__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnInsert
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnInsert(List<InvestorOwnership__c> newInvestorOwnershipList){
    System.debug('InvestorOwnershipTriggerHandler.setDefaultsOnInsert()');
        for(InvestorOwnership__c cc : newInvestorOwnershipList){

        }
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnUpdate
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnUpdate(Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap, Map<ID, InvestorOwnership__c> newInvestorOwnershipMap){
    System.debug('InvestorOwnershipTriggerHandler.setDefaultsOnUpdate()'); 
        for(InvestorOwnership__c cc : newInvestorOwnershipMap.values()){

        }
    }

    //private void manageAccessRules(Map<ID, InvestorOwnership__c> oldInvestorOwnershipMap, Map<ID, InvestorOwnership__c> newInvestorOwnershipMap){
    //    System.debug('InvestorOwnershipTriggerHandler.manageAccessRules()');
    //    List<AccessRule__c> accessRulesToAdd = new List<AccessRule__c>();
    //    List<ID> agreementIDs = new List<ID>();
    //    List<ID> investorIDs = new List<ID>();

    //    for(InvestorOwnership__c ownership : newInvestorOwnershipMap.values()){
    //        agreementIDs.add(ownership.Agreement__c);
    //        investorIDs.add(ownership.Investor__c);
    //    }
    //    System.debug('agreementIDs:'+agreementIDs);
    //    System.debug('newInvestorOwnershipMap.keySet()'+newInvestorOwnershipMap.keySet());
    //    Map<ID, AccessRule__c> accessRuleMap = new Map<ID, AccessRule__c>([SELECT id, Account__c, StudentProgram__c from AccessRule__c where Account__c IN :investorIDs and StudentProgram__c IN :agreementIDs]);
    //    Map<ID, List<AccessRule__c>> accessRulesByInvestor = new Map<ID, List<AccessRule__c>>();
    //    for(AccessRule__c rule : accessRuleMap.values()){
    //        if(!accessRulesByInvestor.containsKey(rule.Account__c)){
    //            accessRulesByInvestor.put(rule.Account__c, new List<AccessRule__c>());
    //        }
    //        accessRulesByInvestor.get(rule.Account__c).add(rule);
    //    }
    //    System.debug('accessRulesByInvestor:'+accessRulesByInvestor);

    //    Map<ID, AccessRule__c> accessRulesToDelete = new Map<ID, AccessRule__c>();

    //    for(InvestorOwnership__c ownership : newInvestorOwnershipMap.values()){
    //        Boolean insertAccessRule = false;
    //        Boolean deleteAccessRule = false;
    //        if(oldInvestorOwnershipMap == null){
    //            if(ownership.OwnershipPercent__c > 0){
    //                //add access if none exists
    //                insertAccessRule = true;
    //            }
    //        } else if(oldInvestorOwnershipMap != null && ownership.OwnershipPercent__c != oldInvestorOwnershipMap.get(ownership.id).OwnershipPercent__c){
    //            if(ownership.OwnershipPercent__c > 0){
    //                //add access if none exists
    //                insertAccessRule = true;
    //            } else {
    //                deleteAccessRule = true;
    //            }


    //       } 
    //        if(insertAccessRule){
    //            Boolean accessRuleExists = false;
    //            if(accessRulesByInvestor.containsKey(ownership.Investor__c)){
    //                for(AccessRule__c rule : accessRulesByInvestor.get(ownership.Investor__c)){
    //                    if(rule.StudentProgram__c == ownership.Agreement__c){
    //                        accessRuleExists = true;
    //                    }
    //                }
    //            }
    //            if(!accessRuleExists){
    //                accessRulesToAdd.add(new AccessRule__c(StudentProgram__c = ownership.Agreement__c, Account__c = ownership.Investor__c));
    //            }
    //        }
    //        if(deleteAccessRule){
    //            System.debug('deleteAccessRules for Investor:'+ownership.Investor__c);
    //            if(accessRulesByInvestor.containsKey(ownership.Investor__c)){
    //                for(AccessRule__c rule : accessRulesByInvestor.get(ownership.Investor__c)){
    //                    if(rule.StudentProgram__c == ownership.Agreement__c) {
    //                        accessRulesToDelete.put(rule.id, rule); 
    //                    }
    //                }                    
    //            }                
    //        }

    //    }
    //    if(accessRulesToAdd.size()>0){
    //        insert accessRulesToAdd;
    //    }
    //    if(accessRulesToDelete.size()>0){
    //        delete accessRulesToDelete.values();
    //    }
    //}

    public class InvestorOwnershipHandlerException extends Exception {}
}