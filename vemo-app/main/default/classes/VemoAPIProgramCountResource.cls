public class VemoAPIProgramCountResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
        }
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIProgramCountResource.handleGetV2()');
        
        String schoolIDparam = api.params.get('schoolID');
        String programIDparam = api.params.get('programID');
        
        
        if(schoolIDparam == null && UserService.schoolAccount != null){
            schoolIdParam =  UserService.schoolAccount.id;
        }
        
        List<AggregateResult> arrList = new List<AggregateResult>();
        
        if(schoolIDparam != null && programIDparam != null){
            arrList =[SELECT Count(Id) Total, Status__c, Program__c,Program__r.EnrollmentType__c et, SUM(FundingAmountPostCertification__c) AmountCertified, SUM(AmountDisbursedToDate__c) AmountDisbursed FROM StudentProgram__c WHERE 
                                        Deleted__c = false AND Program__c =: programIdParam AND Status__c != null AND Program__r.school__c=: schoolIdParam
                                        GROUP BY Program__r.EnrollmentType__c,Program__c,Status__c];

        }
        else if(schoolIDparam != null){
            arrList =[SELECT Count(Id) Total, Status__c, Program__c,Program__r.EnrollmentType__c et, SUM(FundingAmountPostCertification__c) AmountCertified, SUM(AmountDisbursedToDate__c) AmountDisbursed FROM StudentProgram__c WHERE 
                                        Deleted__c = false AND Program__c != null AND Status__c != null AND Program__r.school__c=: schoolIdParam 
                                        GROUP BY Program__r.EnrollmentType__c,Program__c,Status__c];

        }
        
        string ProgramId, status, eType;
        double amountDisbursed = 0, amountCertified = 0 ;
        integer count;
        Map<Id, Map<String, Integer>> SPCountwithProgramNStatus = new Map<Id, Map<String, Integer>>();
        Map<Id, Map<String , Decimal>> SPAmountDisbursedWithProgramNStatus = new Map<Id, Map<String, Decimal>>();
        Map<Id, Map<String , Decimal>> SPAmountCertifiedWithProgramNStatus = new Map<Id, Map<String, Decimal>>();
        Map<Id, String> programIdNEType = new Map<Id, String>();
        
        for(AggregateResult ar : arrList){
            programId = string.valueOf(ar.get('Program__c'));
            status = string.valueOf(ar.get('Status__C'));
            count = integer.valueOf(ar.get('Total'));
            if(ar.get('et') != null)
                eType = string.valueOf(ar.get('et'));
            programIdNEType.put(programId, eType);
            
            amountDisbursed = double.valueOf(ar.get('AmountDisbursed') != null ? ar.get('AmountDisbursed') : 0);          
            amountCertified = double.valueOf(ar.get('AmountCertified') != null ? ar.get('AmountCertified') : 0);
            
                     
            if( !SPCountwithProgramNStatus.containsKey(programId)) 
                SPCountwithProgramNStatus.put( programId , new Map< string, integer >( ));
            if( !SPCountwithProgramNStatus.get(programId).containsKey(Status)) 
                SPCountwithProgramNStatus.get(programId).put(Status, 0);
            
            SPCountwithProgramNStatus.get(programId).put(Status,SPCountwithProgramNStatus.get(programId).get(Status)+count);
                
            // Map to store AmountDisbursed by program+status       
            if( !SPAmountDisbursedWithProgramNStatus.containsKey(programId)) 
                SPAmountDisbursedWithProgramNStatus.put( programId , new Map< string, Decimal>( ));
            if( !SPAmountDisbursedWithProgramNStatus.get(programId).containsKey(Status)) 
                SPAmountDisbursedWithProgramNStatus.get(programId).put(Status, 0);
            
            SPAmountDisbursedWithProgramNStatus.get(programId).put(Status,SPAmountDisbursedWithProgramNStatus.get(programId).get(Status)+amountDisbursed);    
                
            // Map to store AmountCertified by program+status           
            if( !SPAmountCertifiedWithProgramNStatus.containsKey(programId)) 
                SPAmountCertifiedWithProgramNStatus.put( programId , new Map< string, Decimal>( ));
            if( !SPAmountCertifiedWithProgramNStatus.get(programId).containsKey(Status)) 
                SPAmountCertifiedWithProgramNStatus.get(programId).put(Status, 0);
                
            SPAmountCertifiedWithProgramNStatus.get(programId).put(Status,SPAmountCertifiedWithProgramNStatus.get(programId).get(Status)+amountCertified);            
            
            
        }
        
        List<ProgramCountResource> results = new List<ProgramCountResource>();
        if(SPCountwithProgramNStatus  != null && SPCountwithProgramNStatus.size()>0){
            for(ID pId : SPCountwithProgramNStatus.keyset()){
               ProgramCountResource pr = new ProgramCountResource(); 
               pr.ProgramId = pId;
               
               if(programIdNEType.get(pId) != null)
                   pr.enrollmentType = programIdNEType.get(pId);
               pr.statusCountList = new List<StatusCount>();
               
               for(string st : SPCountwithProgramNStatus.get(pId).keyset()){
                   pr.statusCountList.add(new statusCount(st,SPCountwithProgramNStatus.get(pId).get(st), SPAmountDisbursedWithProgramNStatus.get(pId).get(st), SPAmountCertifiedWithProgramNStatus.get(pId).get(st)));
               }
               results.add(pr);
            }
        }
        
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public class ProgramCountResource{
        public String programID{get;set;}
        public string enrollmentType{get;set;}
        public List<StatusCount> statusCountList{get;set;}
    }
    
    public class StatusCount{
        public String status{get;set;}
        public Integer count {get;set;}
        public Decimal amountDisbursed {get;set;}
        public Decimal amountCertified {get;set;}
        public StatusCount(string st, integer cnt, Decimal amountDisbursed, Decimal amountCertified){
            status = st;
            count = cnt;
            this.amountDisbursed = amountDisbursed;
            this.amountCertified = amountCertified;
        }
    } 
}