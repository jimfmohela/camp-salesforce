/**************************** Details**********************

Class Name            :  CreateOutboundEmail_TEST 
Details               :  Covering CreateOutboundEmail and OutboundEmailImmediateQueueable both
Developed by          :  Kamini Singh
created date          :  13th Sep 2018
*********************************************************/
@isTest
public class CreateOutboundEmail_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();

    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        //TestUtil.createStandardTestConditions();
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schools);//TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(1, agreementMap, 'Disbursement');
        }
    }
    
    @isTest public static void validateCreateOutboudEmails4Process(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        List<ID> ids = new List<ID>();
        ids.addAll(agreementMap.keySet());
        test.startTest();
        CreateOutboundEmail.createOutboudEmails4Process(ids);
        test.stopTest();
          
    }
    
    @isTest public static void validateCreateOutboudEmailsForDisbursement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Program__c> programMap = ProgramQueries.getProgramMap();
        for(program__c prg: programMap.values()){
            prg.SendStudentDisbursementConfirmation__c = true;
        }
        update programMap.values();
        //dbUtil.updateRecords(programMap.values());
        Map<ID, transaction__c> txMap = TransactionQueries.getTransactionMap('Disbursement');
        for(transaction__c tx: txMap.values()){
            tx.Status__c = 'Complete';
            tx.Confirmed__c = true;
            
        }
        update txMap.values();
        //dbUtil.updateRecords(txMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(txMap.keySet());
        test.stopTest();        
    }
    
    @isTest public static void validateCreateOutboudEmailsForBill(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Bill__c bill = new bill__c();
        bill.Student__c = studentMap.values().get(0).id;
        bill.SendStatement__c = true;
        
        insert bill; 
        //dbUtil.insertRecord(bill);
        //set<ID> Billids = new set<ID>();
        Map<ID,Bill__c> bills = new map<id, bill__c>([Select id, SendStatement__c , Student__c from bill__c limit 50000]);
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(bills.keyset()); 
        test.stopTest();               
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendApplicationCertified(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendApplicationCertified__c = true;
            
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendApplicationComplete(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendApplicationComplete__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendApplicationCompleteSchoolInit(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendApplicationCompleteSchoolInit__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendSchoolInitPreCertifiedInvite(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendSchoolInitPreCertifiedInvite__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendApplicationUnderReview(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendApplicationUnderReview__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendApplicationReviewApproved(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendApplicationReviewApproved__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendNotCertified(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendNotCertified__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
    
    @isTest public static void validateCreateOutboundEmailForStudentProgram_SendCreditDenied(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        for(Account acc: studentMap.values()){
            acc.PersonEmail = 'test@test.com';
        }
        update studentMap.values();
        //dbUtil.updateRecords(studentMap.values());
        for(studentProgram__c sp: agreementMap.values()){
            sp.SendCreditDenied__c = true;
        }
        update agreementMap.values();
        //dbUtil.updateRecords(agreementMap.values());
        test.startTest();
        set<ID> ids = CreateOutboundEmail.createOutboudEmails(agreementMap.keySet());
        test.stopTest();     
    }
}