@isTest
public class EmploymentSeekingAttemptQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testGetEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptId(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap, 1);
        Test.startTest();
        Map<Id, EmploymentSeekingAttempt__c> resultAttMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(testAttMap.keySet());
        System.assertEquals(testAttMap.keySet().size(), resultAttMap.keySet().size());
        Test.stopTest();
    }
    static testMethod void getEmploymentSeekingAttemptMapWithDefermentIdTest(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap,1);
        Test.startTest();
        Map<Id, EmploymentSeekingAttempt__c> resultAttMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithDefermentID(testAttMap.keySet());
        System.assertEquals(testAttMap.keySet().size(), resultAttMap.keySet().size());
        Test.stopTest();
    }
}