@isTest
public class VemoAPIEmployerResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        
        Map<String, String> empHisParams = new Map<String, String>();
        empHisParams.put('employerID', TestUtil.createStringFromIDSet(employerMap.keySet()));
        empHisParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo empHisApiInfo = TestUtil.initializeAPI('v1', 'GET', empHisParams, null);

        Map<String, String> studParams = new Map<String, String>();
        studParams.put('studentID', TestUtil.createStringFromIDSet(testStudentAccountMap.keySet()));
        studParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo studApiInfo = TestUtil.initializeAPI('v1', 'GET', studParams, null);

        Test.startTest();
        VemoAPI.ResultResponse empHisResult = (VemoAPI.ResultResponse)VemoAPIEmployerResource.handleAPI(empHisApiInfo);
        System.assertEquals(employerMap.size(), empHisResult.numberOfResults);

        VemoAPI.ResultResponse studResult = (VemoAPI.ResultResponse)VemoAPIEmployerResource.handleAPI(studApiInfo);
        System.assertEquals(employerMap.size(), studResult.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePostV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmployerResource.EmployerInputV1> empHisList = new List<VemoAPIEmployerResource.EmployerInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmployerResource.EmployerInputV1 empHis = new VemoAPIEmployerResource.EmployerInputV1(true);
            empHis.studentID = testStudentAccountMap.values().get(i).ID;
            empHisList.add(empHis);
        }
        String body = JSON.serialize(empHisList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmployerResource.handleAPI(apiInfo);
        //System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePutV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmployerResource.EmployerInputV1> empHisList = new List<VemoAPIEmployerResource.EmployerInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmployerResource.EmployerInputV1 empHis = new VemoAPIEmployerResource.EmployerInputV1(true);
            empHis.employerID = employerMap.values().get(i).ID;
            //empHis.verified = false;
            empHisList.add(empHis);
        }
        String body = JSON.serialize(empHisList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmployerResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }

    static testMethod void testHandleDeleteV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('employerID', TestUtil.createStringFromIDSet(employerMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmployerResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        //System.assertEquals(0, EmployerQueries.getEmploymentHistoryMap().size());
        Test.stopTest();
    }
}