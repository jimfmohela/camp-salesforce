//This class must run WITHOUT SHARING
public without sharing class SharingService{
    

    public SharingService(){
    
    }
    
    public static void studentSharing(Set<Id> accessRuleIds){
        
        List<Sobject> studentShares = new List<SObject>();
        //List<AccessRule__c> AccessRules = [Select Id, Account__c, StudentProgram__c, StudentProgram__r.Student__c, AccountReadAccess__c, AccountEditAccess__c, OpportunityReadAccess__c, OpportunityEditAccess__c, CaseReadAccess__c, CaseEditAccess__c from AccessRule__c where Id IN: accessRuleIds];
        Map<ID, AccessRule__c> AccessRuleMapWithID = getAccessRuleMapWithID(accessRuleIds);
        set<Id> schoolIds = new set<Id>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            schoolIds.add(rule.Account__c);     
        }
        
        Map<Id,Group> GroupBySchoolId = getGroupBySchoolID(schoolIds);
        
        String ObjectName = 'AccountShare';
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName); 
         
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            /*AccountShare studentShare = new Accountshare();
            studentShare.AccountID = rule.StudentProgram__r.Student__c;
            studentShare.UserOrGroupId = GroupBySchoolId.get(rule.account__c).id;
            
            //Account Access Level
            if(rule.AccountEditAccess__c == true)
                studentShare.AccountAccessLevel = 'Edit';
            else
                studentShare.AccountAccessLevel = 'Read';
            
            //Opportunity Access Level
            if(rule.OpportunityEditAccess__c == true)    
                studentShare.OpportunityAccessLevel = 'Edit';
            else if(rule.OpportunityReadAccess__c == true)    
                studentShare.OpportunityAccessLevel = 'Read';
            else
                studentShare.OpportunityAccessLevel = 'None';
            
            //Case Access Level    
            if(rule.CaseEditAccess__c == true)
                studentShare.CaseAccessLevel = 'Edit';
            else if(rule.CaseReadAccess__c == true)
                studentShare.CaseAccessLevel = 'Read';  */
                
            try{        
                SObject studentShare = targetType.newSObject();
                studentShare.put('AccountID', rule.studentProgram__r.Student__c);
                //system.debug('rule.account__c:'+rule.account__c);
                //system.debug('GroupBySchoolId.get(rule.account__c):'+GroupBySchoolId.get(rule.account__c));
                studentShare.put('UserOrGroupID', GroupBySchoolId.get(rule.account__c).id);
                
                //Account Access Level
                if(rule.AccountEditAccess__c == true)
                    studentShare.put('AccountAccessLevel','Edit');
                else
                    studentShare.put('AccountAccessLevel','Read');
                
                //Opportunity Access Level
                if(rule.OpportunityEditAccess__c == true)    
                    studentShare.put('OpportunityAccessLevel','Edit');
                else if(rule.OpportunityReadAccess__c == true)    
                    studentShare.put('OpportunityAccessLevel','Read');
                else
                    studentShare.put('OpportunityAccessLevel','None');
                
                //Case Access Level    
                if(rule.CaseEditAccess__c == true)
                    studentShare.put('CaseAccessLevel','Edit');
                else if(rule.CaseReadAccess__c == true)
                    studentShare.put('CaseAccessLevel','Read');
                
                studentShares.add(studentShare); 
            }catch(exception e){
                system.debug('Error: '+e);
            }
        }
        
        if(studentShares.size()>0)
            insert studentShares;
    }
    
    public static void creditCheckSharing(Set<Id> accessRuleIds){
        List<SObject> creditCheckShares = new List<SObject>();
        
        //List<AccessRule__c> AccessRules = [Select Id, Account__c, StudentProgram__c, StudentProgram__r.Student__c, AccountReadAccess__c, AccountEditAccess__c, OpportunityReadAccess__c, OpportunityEditAccess__c, CaseReadAccess__c, CaseEditAccess__c, CreditCheck__c, CreditCheckReadAccess__c, CreditCheckEditAccess__c, Disbursement__c, DisbursementReadAccess__c, DisbursementEditAccess__c from AccessRule__c where Id IN: accessRuleIds];
        Map<ID, AccessRule__c> AccessRuleMapWithID = getAccessRuleMapWithID(accessRuleIds);
        set<Id> schoolIds = new set<Id>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            schoolIds.add(rule.Account__c);     
        }
        
        Map<Id,Group> GroupBySchoolId = getGroupBySchoolID(schoolIds);
        
        String ObjectName = 'CreditCheck__Share';
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName);
        
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            /*CreditCheck__Share creditCheckShare = new CreditCheck__Share();
            creditCheckShare.parentID = rule.CreditCheck__c;
            creditCheckShare.UserOrGroupId = GroupBySchoolId.get(rule.account__c).id;
            
            //Credit Check Access Level
            if(rule.CreditCheckEditAccess__c == true)
                creditCheckShare.AccessLevel = 'Edit'; 
            else
                creditCheckShare.AccessLevel = 'Read';*/
                
            //String ObjectName = 'CreditCheck__Share';
            //Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName); 
            try{                     
                SObject creditCheckShare = targetType.newSObject();    
                creditCheckShare.put('parentID', rule.CreditCheck__c); 
                creditCheckShare.put('UserOrGroupID', GroupBySchoolId.get(rule.account__c).id);
                
                //Credit Check Access Level
                if(rule.CreditCheckEditAccess__c == true)
                    creditCheckShare.put('AccessLevel','Edit'); 
                else
                    creditCheckShare.put('AccessLevel','Read'); 
                    
                creditCheckShares.add(creditCheckShare); 
            }catch(exception e){
                system.debug('Error: '+e);
            }   
        }
        
        if(creditCheckShares.size()>0)
            insert creditCheckShares;
    }
    
    public static void disbursementSharing(Set<Id> accessRuleIds){
        List<SObject> disbursementShares = new List<SObject>();
        
        //List<AccessRule__c> AccessRules = [Select Id, Account__c, StudentProgram__c, StudentProgram__r.Student__c, AccountReadAccess__c, AccountEditAccess__c, OpportunityReadAccess__c, OpportunityEditAccess__c, CaseReadAccess__c, CaseEditAccess__c, CreditCheck__c, CreditCheckReadAccess__c, CreditCheckEditAccess__c, Disbursement__c, DisbursementReadAccess__c, DisbursementEditAccess__c from AccessRule__c where Id IN: accessRuleIds];
        Map<ID, AccessRule__c> AccessRuleMapWithID = getAccessRuleMapWithID(accessRuleIds);
        set<Id> schoolIds = new set<Id>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            schoolIds.add(rule.Account__c);     
        }
        
        Map<Id,Group> GroupBySchoolId = getGroupBySchoolID(schoolIds);
        
        String ObjectName = 'Transaction__Share';
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName);
        
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            /*Transaction__Share DisbursementShare = new Transaction__Share();
            DisbursementShare.parentID = rule.Disbursement__c;
            DisbursementShare.UserOrGroupId = GroupBySchoolId.get(rule.account__c).id;
            
            //Transaction Access Level
            if(rule.DisbursementEditAccess__c == true)
                DisbursementShare.AccessLevel = 'Edit'; 
            else
                DisbursementShare.AccessLevel = 'Read';*/
                
            try{                              
                SObject DisbursementShare = targetType.newSObject();  
                DisbursementShare.put('parentID', rule.Disbursement__c);
                DisbursementShare.put('UserOrGroupID', GroupBySchoolId.get(rule.account__c).id);
                
                //Transaction Access Level
                if(rule.DisbursementEditAccess__c == true)
                    DisbursementShare.put('AccessLevel','Edit'); 
                else
                    DisbursementShare.put('AccessLevel','Read');   
                    
                DisbursementShares.add(DisbursementShare); 
            }catch(exception e){
                system.debug('Error: '+e);
            } 
              
        }
        
        if(DisbursementShares.size()>0)
            insert DisbursementShares;
    }
    
    public static void AcedemicEnrollmentSharing(Set<Id> accessRuleIds){
       // AccessRules = [Select Id, Account__c, StudentProgram__c, StudentProgram__r.Student__c, AccountReadAccess__c, AccountEditAccess__c, OpportunityReadAccess__c, OpportunityEditAccess__c, CaseReadAccess__c, CaseEditAccess__c, CreditCheck__c, CreditCheckReadAccess__c, CreditCheckEditAccess__c, Disbursement__c, DisbursementReadAccess__c, DisbursementEditAccess__c, AcademicEnrollment__c, AcademicEnrollmentEditAccess__c, AcademicEnrollmentReadAccess__c from AccessRule__c where Id IN: accessRuleIds];
        Map<ID, AccessRule__c> AccessRuleMapWithID = getAccessRuleMapWithID(accessRuleIds);
        
        List<SObject> AcademicEnrollmentShares = new List<SObject>();
        
        set<Id> schoolIds = new set<Id>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            schoolIds.add(rule.Account__c);     
        }
        
        Map<Id,Group> GroupBySchoolId = getGroupBySchoolID(schoolIds);
        String ObjectName = 'AcademicEnrollment__Share';
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName);
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            try{
                SObject AcademicEnrollmentShare = targetType.newSObject();  
                AcademicEnrollmentShare.put('parentID', rule.AcademicEnrollment__c);
                AcademicEnrollmentShare.put('UserOrGroupID', GroupBySchoolId.get(rule.account__c).id);
                
                //Transaction Access Level
                if(rule.AcademicEnrollmentEditAccess__c == true)
                    AcademicEnrollmentShare.put('AccessLevel','Edit'); 
                else
                    AcademicEnrollmentShare.put('AccessLevel','Read');   
                    
                AcademicEnrollmentShares.add(AcademicEnrollmentShare);                    
            } catch (Exception e){
                system.debug('Error: '+e);
            }
 
        }
        
        if(AcademicEnrollmentShares.size()>0)
            insert AcademicEnrollmentShares;
    
    }
    
    public static void DataCollectionSharing(Map<Id, DataCollection__c> dataCollectionMap){
        List<SObject> DataCollectionShares = new List<SObject>();
                    
        Map<Id, List<DataCollection__c>> dataCollectionMapByAgreementID = new Map<Id, List<DataCollection__c>>();
        for(DataCollection__c dc: dataCollectionMap.values()){
            //agreementIds.add(dc.Agreement__c);
            if(!dataCollectionMapByAgreementID.containsKey(dc.Agreement__c)){
                dataCollectionMapByAgreementID.put(dc.Agreement__c, new List<DataCollection__c>()); 
            }
            dataCollectionMapByAgreementID.get(dc.Agreement__c).add(dc); 
        }
        
        Map<ID, AccessRule__c> AccessRuleMapWithAgreementID = new Map<Id, AccessRule__c>();
        List<AccessRule__c> rules = [Select id, StudentProgram__c, Account__c from AccessRule__c where StudentProgram__c IN: dataCollectionMapByAgreementID.keySet()];
        for(AccessRule__c rule: rules){
            AccessRuleMapWithAgreementID.put(rule.StudentProgram__c, rule);     
        }
        
        set<Id> schoolIds = new set<Id>();
        for(AccessRule__c rule: AccessRuleMapWithAgreementID.values()){
            schoolIds.add(rule.Account__c);     
        }
        
        Map<Id,Group> GroupBySchoolId = getGroupBySchoolID(schoolIds);
        
        String ObjectName = 'DataCollection__Share';
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectName);
            
        for(Id id: dataCollectionMapByAgreementID.keySet()){
            try{
                if(AccessRuleMapWithAgreementID.containsKey(id)){
                    for(DataCollection__c dc: dataCollectionMapByAgreementID.get(id)){
                        SObject DataCollectionShare = targetType.newSObject();  
                        DataCollectionShare.put('parentID', dc.id);
                        DataCollectionShare.put('UserOrGroupID', GroupBySchoolId.get(AccessRuleMapWithAgreementID.get(id).Account__c).id);
                        
                        //DataCollection Access Level
                        DataCollectionShare.put('AccessLevel', 'Read');  
                            
                        DataCollectionShares.add(DataCollectionShare);    
                    }
                }
            } catch (Exception e){
                  system.debug('Error: '+e);
            }
        }            
               
        if(DataCollectionShares.size()>0)
            insert DataCollectionShares;
    
    }
    
    public static Map<ID, Group> getGroupBySchoolID(Set<ID> schoolIDs){
        system.debug('SharingService.getGroupBySchoolID()');       
        Map<Id, Group> GroupBySchoolID = new map<Id,Group>();
        //Map<Id, List<User>> usersByRoleID = new Map<Id, List<User>>();
        //set<ID> roleIDs = new set<ID>();
        //List<User> SchoolAdminUsers = [SELECT id, UserRoleID , ContactID, AccountId from User where AccountID IN: schoolIDs];
        //for(User admin: SchoolAdminUsers){
            //roleIDs.add(admin.UserRoleID); 
        //    if(!usersByRoleID.containsKey(admin.UserRoleID)){
        //        usersByRoleID.put(admin.UserRoleId, new List<user>()); 
        //    } 
        //    usersByRoleID.get(admin.UserRoleId).add(admin); 
        //}
        Map<Id, UserRole> userRolesById = new Map<Id, UserRole>([Select id, DeveloperName,PortalAccountId  from UserRole where PortalAccountId IN: schoolIDs and DeveloperName LIKE '%Manager%']); // Share with Portal Roles and Subordinates. So the record will be shared by Portal Manager and Portal User as well 
        List<Group> Groups = [Select Id, Name, RelatedId, Type From Group where RelatedId IN: userRolesById.keySet() and Type = 'RoleAndSubordinates'];
        system.debug('groups--'+groups);
        for(Group gr: Groups){
            if(!GroupBySchoolID.containsKey(userRolesByID.get(gr.relatedId).PortalAccountid)){
                GroupBySchoolID.put(userRolesByID.get(gr.relatedId).PortalAccountid, gr);
            }
            //GroupBySchoolID.put((gr.relatedId)[0],add(gr));
        } 
        
        system.debug('GroupBySchoolID---'+GroupBySchoolID);
        return GroupBySchoolID;   
    }
    
   // public static Integer deleteShares(Map<Id, AccessRule__c> ruleMap){
   //     System.debug('SharingService.deleteShares');
   //     Set<Id> studentIds = new Set<Id>();
   //     Set<Id> creditCheckIds = new Set<Id>();
   //     Set<Id> disbursementIds = new Set<Id>();
   //     //List<AccessRule__c> rules = [Select id, studentprogram__c, studentprogram__r.student__c from AccessRule__c where ID IN: ruleIds];
   //     for(AccessRule__c rule: [Select id, studentprogram__c, studentprogram__r.student__c, CreditCheck__c, Disbursement__c from AccessRule__c where ID IN: ruleMap.keyset()]){
   //         if(rule.studentprogram__c != null)studentIds.add(rule.studentprogram__r.student__c);
   //         else if(rule.creditcheck__c != null)creditCheckIds.add(rule.CreditCheck__c);
   //         else if(rule.disbursement__c != null)disbursementIds.add(rule.Disbursement__c);
   //     }
   //     system.debug(studentIds);
   //     Integer numToDelete;
        
   //     if(studentIds.size()>0){
   //         numToDelete = deleteShares('Account', studentIds);
   //     }
   //     if(creditCheckIds.size()>0){
   //         numToDelete = deleteShares('CreditCheck__c', creditCheckIds);
   //     }
   //     if(disbursementIds.size()>0){
   //         numToDelete = deleteShares('Transaction__c', disbursementIds);
   //     }    
   //     return numToDelete;
   // }
    
    public static Integer deleteShares(String SObjectName, Set<ID> parentIds){
        String ObjectName;
        if(SObjectName.endsWithIgnoreCase('__c'))
            ObjectName = SObjectName.replace('__c','__Share');
        else
            ObjectName = SObjectName + 'Share';
               
        String Query = 'Select Id from ' + ObjectName ; 
        
        if(SObjectName == 'Account'){
            Query = Query + ' where AccountID IN ' + DatabaseUtil.inSetStringBuilder(parentIds);
        }
        else{ 
            Query = Query + ' where parentID IN ' + DatabaseUtil.inSetStringBuilder(parentIds);
        }
        Query +=' AND RowCause = \'Manual\' ';
            
        System.debug('Query --'+Query);    
            
        DatabaseUtil db = new DatabaseUtil();
        List<Sobject> Shares = db.query(Query);
        Integer numToDelete = Shares.size();
        db.deleteRecords(Shares);
        return numToDelete;
        
    }
    
    public static Map<ID, AccessRule__c> getAccessRuleMapWithID(set<Id> accessRuleIds){
        Map<ID, AccessRule__c> accessRuleMap = new Map<ID, AccessRule__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(accessRuleIds);
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        accessRuleMap = new Map<ID, Accessrule__c>((List<AccessRule__c>)db.query(query));
        return accessRuleMap; 
     }
     
      private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM AccessRule__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Account__c, ';
        
        fieldNames += 'StudentProgram__c, ';
        fieldNames += 'StudentProgram__r.Student__c, ';
        fieldNames += 'AccountEditAccess__c, ';
        fieldNames += 'AccountReadAccess__c, ';
        fieldNames += 'CaseEditAccess__c, ';
        fieldNames += 'CaseReadAccess__c, ';
        fieldNames += 'OpportunityEditAccess__c, ';
        fieldNames += 'OpportunityReadAccess__c, ';
        
        fieldNames += 'CreditCheck__c, ';
        fieldNames += 'CreditCheckEditAccess__c, ';
        fieldNames += 'CreditCheckReadAccess__c, ';
        
        fieldNames += 'Disbursement__c, ';
        fieldNames += 'DisbursementEditAccess__c, ';
        fieldNames += 'DisbursementReadAccess__c, ';
        
        fieldNames += 'AcademicEnrollment__c, ';
        fieldNames += 'AcademicEnrollmentEditAccess__c, ';
        fieldNames += 'AcademicEnrollmentReadAccess__c';
        return fieldNames;
   }
   
   
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }
   // private static Integer deleteStudentShares(Set<Id> studentIds){
   //   List<AccountShare> studentShares = [Select id from AccountShare where AccountID IN: studentIds];
   //   Integer numToDelete = studentShares.size();
   //   DatabaseUtil dbUtil = new DatabaseUtil();
   //   dbUtil.deleteRecords(studentShares);
   //   return numToDelete;   
   // }
    
   // private static Integer deleteCreditCheckShares(Set<Id> creditCheckIds){
   //   List<CreditCheck__Share> creditCheckShares = [Select id from Creditcheck__Share where parentID IN: creditCheckIds];
   //   Integer numToDelete = creditCheckShares.size();
   //   DatabaseUtil dbUtil = new DatabaseUtil();
   //   dbUtil.deleteRecords(creditCheckShares);
   //   return numToDelete;       
   // }
    
   // private static Integer deleteDisbursementShares(Set<Id> disbursementIds){
   //   List<Transaction__Share> disbursementShares = [Select id from Transaction__Share where parentID IN: disbursementIds];
   //   Integer numToDelete = disbursementShares.size();
   //   DatabaseUtil dbUtil = new DatabaseUtil();
   //   dbUtil.deleteRecords(disbursementShares);
   //   return numToDelete;       
   // }
}