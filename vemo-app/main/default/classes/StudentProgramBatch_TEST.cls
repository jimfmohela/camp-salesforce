@isTest
private class StudentProgramBatch_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @testSetup static void createRequiredData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        USer USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);//TestUtil.TEST_THROTTLE
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        }
    }
    
    @isTest
    static void delinquency(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        integer days = -12;
        for(StudentProgram__c agreement : studentPrgMap.values()){
            //agreement.Deleted__c = true;
            agreement.NextPaymentDue__c  = 100;
            agreement.NextPaymentDueDate__c = Date.today().addDays(days);
            agreement.bypassautomation__c = true;
            agreement.Status__c  = 'Payment';
            days -= 15;
        }
        TriggerSettings.getSettings().studentProgramTrigger = false;
        dbUtil.updateRecords(studentPrgMap.values());
        //update studentPrgMap.values();
        dbUtil.updateRecords(studentPrgMap.values());
        TriggerSettings.getSettings().studentProgramTrigger = true;
        
        
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.DELINQUENCY;
        Database.executeBatch(job, 200);
        Test.stopTest();
        
    }
    
    @isTest
    static void purgeDeletedTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        for(StudentProgram__c agreement : studentPrgMap.values()){
            agreement.Deleted__c = true;
            agreement.Status__c = 'Draft';
        }
        //update studentPrgMap.values();
        dbUtil.updateRecords(studentPrgMap.values());
        
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.PURGE_DELETED;
        Database.executeBatch(job, 200);
        Test.stopTest();
        
    }
    
    @isTest
    static void monthEndAuditTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.MONTH_END_AUDIT;
        Database.executeBatch(job, 200);
        Test.stopTest();
        
    }
    
    @isTest
    static void generateFinalDisclosureTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrograms = StudentProgramQueries.getStudentProgramMap();
        
        for(StudentProgram__c sp : studentPrograms.values()){
            sp.GenerateFinalDisclosure__c = true;
        }
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.GENERATE_FINAL_DISCLOSURE;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    
    @isTest
    static void statusTransitionTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrograms = StudentProgramQueries.getStudentProgramMap();
        Integer count = 0;
        for(StudentProgram__c sp : studentPrograms.values()){
            count ++ ;
            if(count > 0 && count < 2){
                sp.GracePeriodEndDate__c = Date.today().addDays(-10);
                sp.Status__c = 'Grace';
            }
            else if(count > 2 && count < 5){
                sp.PauseEndDate__c = Date.today().addDays(-10);
                sp.Status__c = 'Pause';
            }
            else {
                //sp.DefermentEndDate__c = Date.today().addDays(-10);
                //sp.Status__c = 'Deferment';
                sp.ServicingStartDate__c = Date.today().addDays(-10);
                sp.Servicing__c = false;
            }
        }
        TriggerSettings.getSettings().studentProgramTrigger = false;
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        TriggerSettings.getSettings().studentProgramTrigger = true;
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.STATUS_TRANSITION;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    
    @isTest
    static void contractAssessmentTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrograms = StudentProgramQueries.getStudentProgramMap();
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, students);
        Map<Id, IncomeVerification__c> testIncVerMap = TestDataFactory.createAndInsertIncomeVerification(1, testEmpHisMap);        
        //Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(5, studentPrograms);
        /*Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(studentPrograms, agreementMonthStats);
        
        for(StudentProgramAmountDue__c spAmt: agreementAmountDue.values()){
            spAmt.Type__c = 'Monthly Amount';
            spAmt.ExcludeFromBalance__c = false;
        }
        dbUtil.updateRecords(agreementAmountDue.values());*/
        
        for(IncomeVerification__c iv: testIncVerMap.values()){
            iv.BeginDate__c = Date.Today().addMonths(-5);
            iv.Status__c = 'Verified';
        }
        dbUtil.updateRecords(testIncVerMap.values());
        studentPrograms.values()[0].AssessContract__c = true;
        studentPrograms.values()[0].ServicingStartDate__c = Date.today().addDays(-10);
        studentPrograms.values()[0].Servicing__c = true;
        studentPrograms.values()[0].Status__c = 'Payment';
        TriggerSettings.getSettings().studentProgramTrigger = false;
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        TriggerSettings.getSettings().studentProgramTrigger = true;
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.CONTRACT_ASSESSMENT;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    
    @isTest
    static void contractAssessmentTest1(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = AccountQueries.getStudentMap();
        Map<Id, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> studentPrograms = StudentProgramQueries.getStudentProgramMap();
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, students);
        Map<Id, IncomeVerification__c> testIncVerMap = TestDataFactory.createAndInsertIncomeVerification(2, testEmpHisMap);        
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(5, studentPrograms);
        Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(studentPrograms, agreementMonthStats);
        
        for(StudentProgramAmountDue__c spAmt: agreementAmountDue.values()){
            spAmt.Type__c = 'Monthly Amount';
            spAmt.ExcludeFromBalance__c = false;
        }
        dbUtil.updateRecords(agreementAmountDue.values());
        
        for(IncomeVerification__c iv: testIncVerMap.values()){
            iv.BeginDate__c = Date.Today().addMonths(-5);
            iv.Status__c = 'Verified';
        }
        dbUtil.updateRecords(testIncVerMap.values());
        studentPrograms.values()[0].AssessContract__c = true;
        studentPrograms.values()[0].ServicingStartDate__c = Date.today().addDays(-10);
        studentPrograms.values()[0].Servicing__c = true;
        studentPrograms.values()[0].Status__c = 'Payment';
        TriggerSettings.getSettings().studentProgramTrigger = false;
        //update studentPrograms.values();
        dbUtil.updateRecords(studentPrograms.values());
        TriggerSettings.getSettings().studentProgramTrigger = true;
        Test.startTest();
        StudentProgramBatch job = new StudentProgramBatch();
        job.job = StudentProgramBatch.JobType.CONTRACT_ASSESSMENT;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    
}