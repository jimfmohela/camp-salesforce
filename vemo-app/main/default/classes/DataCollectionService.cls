public with sharing class DataCollectionService {

    public static List<DataCollection> getDataCollectionMapWithId(set<Id> dataCollectionIds){    
        Map<Id, DataCollection__c> DataCollectionMap = DataCollectionQueries.getDataCollectionMapWithId(dataCollectionIds);
        List<DataCollection> DataCollectionList = new List<DataCollection>();
        for(DataCollection__c data : DataCollectionMap.values()){
            DataCollectionList.add(new DataCollection(data));
        }
        return DataCollectionList;
    }
    
    public static List<DataCollection> getDataCollectionMapWithAgreement(set<Id> agreementIds){    
        Map<Id, DataCollection__c> DataCollectionMap = DataCollectionQueries.getDataCollectionMapWithAgreement(agreementIds);
        List<DataCollection> DataCollectionList = new List<DataCollection>();
        for(DataCollection__c data : DataCollectionMap.values()){
            DataCollectionList.add(new DataCollection(data));
        }
        return DataCollectionList;
    } 
    
     public static Set<ID> createDataCollections(List<DataCollection> data){
        System.debug('DataCollectionService.createDataCollections()');
        List<DataCollection__c> newCollections = new List<DataCollection__c>();
        for(DataCollection dc : data){
            DataCollection__c newDC = dcTodc(dc);
            newCollections.add(newDC);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newCollections);
        //insert newCollections;
        Set<ID> dcIDs = new Set<ID>();
        for(DataCollection__c dc : newCollections){
            dcIDs.add(dc.ID);
        }
        return dcIDs;
    }
    
    public static Set<ID> updateDataCollections(List<DataCollection> data){
        System.debug('DataCollectionService.createDataCollections()');
        List<DataCollection__c> newCollections = new List<DataCollection__c>();
        for(DataCollection dc : data){
            DataCollection__c newDC = dcTodc(dc);
            newCollections.add(newDC);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newCollections);
        //update newCollections;
        Set<ID> dcIDs = new Set<ID>();
        for(DataCollection__c dc : newCollections){
            dcIDs.add(dc.ID);
        }
        return dcIDs;
    }
    
    public static DataCollection__c dcTodc(DataCollection dc){
        DataCollection__c dcObj = new DataCollection__c();
        if(dc.agreementId != null) dcObj.Agreement__c = dc.agreementID;
        if(dc.dataCollectionID != null) dcObj.ID = dc.dataCollectionID;
        if(dc.stringValue != null) dcObj.stringValue__c = dc.stringValue; 
        if(dc.booleanValue != null) dcObj.booleanValue__c = boolean.valueOf(dc.booleanValue);
        if(dc.dateValue != null) dcObj.dateValue__c = dc.dateValue;
        if(dc.dataType != null) dcObj.dataType__c = dc.dataType; 
        if(dc.picklistValue != null) dcObj.picklistValue__c = dc.picklistValue;     
        if(dc.label != null) dcObj.label__c = dc.label;
        if(dc.templateId != null) dcObj.template__c = dc.templateId;
        return dcObj;
    }   

    public class DataCollection{
        public String dataCollectionID{get;set;}
        public String agreementID{get;set;}
        public String booleanValue {get;set;}
        public String dataType {get;set;}
        public Date dateValue {get;set;}
        public String picklistValue {get;set;}
        public String stringValue {get;set;}
        public String templateId {get;set;}
        public String label {get;set;}

        public DataCollection(){}

        public DataCollection(DataCollection__c data){
            this.dataCollectionID = data.ID;
            this.agreementId = data.agreement__c;
            this.booleanValue = String.valueOf(data.booleanValue__c);
            this.dataType = data.dataType__c;
            this.dateValue = data.dateValue__c;
            this.picklistValue = data.picklistValue__c; 
            this.stringValue = data.stringValue__c;
            this.templateId = data.template__c;
            this.label = data.Label__c;
        }
    }
}