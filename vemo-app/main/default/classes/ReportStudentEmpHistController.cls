public with sharing class ReportStudentEmpHistController{
    //public String selectedSchool {get;set;}
    //transient public List<reportDataWrapper> reportData {get;set;}
    transient public List<ReportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportStudentEmpHistController(){
        //selectedSchool = '';
        //reportData = new List<reportDataWrapper>();    
        reportData = new List<ReportDataWrapper>();
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    /*public List<SelectOption> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        List<SelectOption> output  = new List<SelectOption>();
        for(Account sch:result){
            output.add(new SelectOption(sch.id,sch.name));    
        }
        return output; 
    }*/
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        
        getAgreements();
        
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public List<ReportDataWrapper> buildReportData(Map<ID,List<StudentProgram__c>> studentAgreementMap, Map<Id,String> studIdNEmployer,Map<Id,String> studIdNIncome,Map<Id,String> programIdNDefermentType){
        List<ReportDataWrapper> output = new List<ReportDataWrapper>();
        
        for(ID studentID:studentAgreementMap.keySet()){
            ReportDataWrapper wrapper = new ReportDataWrapper();
            for(StudentProgram__c sp:studentAgreementMap.get(studentID)){
                wrapper.vemoAccountNumber = sp.StudentVemoAccountNumber__c;
                wrapper.studentID = sp.student__c;
                //wrapper.studentName = sp.student__r.name;
                AgreementWrapper agre = new AgreementWrapper();                
                //agre.accountNumber = sp.Student__r.VemoAccountNumber__c;
                agre.accountName = sp.Student__r.Name;
                //agre.accountId = sp.Student__r.Id;
                agre.employer = studIdNEmployer.get(sp.Student__r.Id);
                agre.incomePerYear = studIdNIncome.get(sp.Student__r.Id);
                agre.deferementType = programIdNDefermentType.get(sp.Program__r.Id);
                if(sp.PaidToDate__c != null)
                    agre.paidToDate = sp.PaidToDate__c;         
                if(sp.ExpectedGraduationDate__c != null)
                    agre.expectedGraduationDate = sp.ExpectedGraduationDate__c;
                if(sp.ExpectedGraduationDate__c <= date.Today())
                    agre.graduationY_N = 'Y';
                else
                    agre.graduationY_N = 'N';                
                if(sp.DaysDelinquent__c != null)
                    agre.daysDelinquent = Integer.Valueof(sp.DaysDelinquent__c);
                if(sp.PaymentCapPostCertification__c != null)
                    agre.paymentCap = sp.PaymentCapPostCertification__c;
                if(sp.GracePeriodEndDate__c != null)
                    agre.graceEndDate = sp.GracePeriodEndDate__c;            
                if(sp.AmountDueToDate__c != null)
                    agre.amountDueToDate = sp.AmountDueToDate__c;
                if(sp.NextPaymentDue__c != null)
                    agre.nextPaymentDue = sp.NextPaymentDue__c;
                if(sp.Status__c != null)
                    agre.status = sp.Status__c;
                agre.monthsPaid = sp.Student_Program_Debits__r.size();            
                if(sp.PaymentTermPostCertification__c != null){  
                    agre.paymentTerm = sp.PaymentTermPostCertification__c;
                    agre.remainingPayments = Integer.valueof(agre.paymentTerm) - sp.Student_Program_Debits__r.size();          
                }
                if(sp.StudentEmail__c != null)
                    agre.studentEmail = sp.StudentEmail__c;
                    
                wrapper.agreements.add(agre);         
                
            }
            output.add(wrapper);
            
        }
        //system.debug(output);
        return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student
    ///////////////////////////////////////////////
    public void getAgreements(){
              
         List<StudentProgram__c> spList = [select id,Student__r.Id,Student__r.Name,StudentVemoAccountNumber__c,ExpectedGraduationDate__c,DaysDelinquent__c,
                     PaymentCapPostCertification__c,GracePeriodEndDate__c,PaymentTermPostCertification__c,AmountDueToDate__c,NextPaymentDue__c,PaidToDate__c,
                     MonthlyAmountDue__c,Status__c,StudentEmail__c,Program__r.Id,(select id from Student_Program_Debits__r where FullyAllocated__c = true) from StudentProgram__c
                     where PrimaryOwner__r.Name = 'Flowpoint Deal 1'];
                     
         //Map of Agreements by Student
         Map<ID,List<StudentProgram__c>> studentAgreementMap = new Map<ID,List<StudentProgram__c>>();
                     
         //Set<Id> studentIds = new Set<Id>();
         Set<Id> programIds = new Set<Id>();
         
         for(StudentProgram__c sp: spList){
             if(!studentAgreementMap.containsKey(sp.Student__r.Id)){
                studentAgreementMap.put(sp.student__c,new List<StudentProgram__c>());
             }
             studentAgreementMap.get(sp.Student__r.Id).add(sp);
             //studentIds.add(sp.Student__r.Id);
             programIds.add(sp.Program__r.Id);
         }
         
         List<Account> studentList = [select id,Name,VemoAccountNumber__c,(Select Employer__c from Employment_History__r where Employer__c != null
                        order by createddate desc limit 1 ),(Select IncomePerYear__c from Income_Verification__r order by createddate desc limit 1)
                        from Account where Id IN: studentAgreementMap.KeySet()];
                        
         Map<Id,String> studIdNEmployer = new Map<Id,String>();
         Map<Id,String> studIdNIncome = new Map<Id,String>(); 
         
         for(Account acc: studentList){
             if(acc.Employment_History__r.size()>0)
                 studIdNEmployer.put(acc.Id,acc.Employment_History__r[0].Employer__c);
             else
                 studIdNEmployer.put(acc.Id,'');
                 
             if(acc.Income_Verification__r.size()>0)
                 studIdNIncome.put(acc.Id,string.valueof(acc.Income_Verification__r[0].IncomePerYear__c));
             else
                 studIdNIncome.put(acc.Id,'');
         }
        
         List<Program__c> progList = [select id,(select DefermentType__c from Deferment_Rules__r order by createddate desc limit 1)
                                 from Program__c where Id IN: programIds];
                                 
         Map<Id,String> programIdNDefermentType = new Map<Id,String>();
         
         for(Program__c p: progList){
             if(p.Deferment_Rules__r.size()>0)
                 programIdNDefermentType.put(p.Id,p.Deferment_Rules__r[0].DefermentType__c);
             else
                 programIdNDefermentType.put(p.Id,'');
         }
                                 
         
         reportData = buildReportData(studentAgreementMap, studIdNEmployer,studIdNIncome,programIdNDefermentType);                                                
        
    }
    
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportStudentEmpHistExport');
        pg.setRedirect(false);
        return pg;
    }
    
    ////////////////////////////////////////
    ///Generate a csv string
    ////////////////////////////////////////
    public void buildCsvString(){
        csv = 'Vemo Account Number,Account Name,Employer,Income Per Year,Expected Graduation Date,Days Delinquent,';
        csv += 'Payment Cap (Post-Certification),Grace Period End Date,Graduation Y/N,Payment Term (Post-Certification),';
        csv += 'Amount Due To Date,Next Payment Due,Paid to Date,Status,Monthly Payment Amount,Months Fully Paid,';
        csv += 'Remaining Payments,Student Email,Deferement Type';
        csv += '\n';
        runReport();
        
        if(reportData == null || reportData.size()==0) return;
        
        
        
         for(reportDataWrapper row:reportData){
            Boolean firstrow = true;
            for(AgreementWrapper agre:row.agreements){
            
            csv += row.vemoAccountNumber + ',';
            csv += agre.accountName + ',';
            
            if(agre.employer <> null) csv += agre.employer + ',';
                else csv += ',';          
                
                if(agre.incomePerYear <> null) csv += agre.incomePerYear + ',';
                else csv += ',';
                
                if(agre.expectedGraduationDate <> null) csv += agre.expectedGraduationDate + ',';
                else csv += ',';
                
                if(agre.daysDelinquent <> null) csv += agre.daysDelinquent + ',';
                else csv += ',';            
                
                if(agre.paymentCap <> null) csv += agre.paymentCap + ',';    
                else csv += ',';
                
                if(agre.graceEndDate <> null) csv += agre.graceEndDate + ',';    
                else csv += ',';            
               
                if(agre.graduationY_N == 'Y') csv += 'Y,';    
                else csv += 'N,';
                
                if(agre.paymentTerm <> null) csv += agre.paymentTerm + ',';    
                else csv += ',';
                
                if(agre.amountDueToDate <> null) csv += agre.amountDueToDate + ',';    
                else csv += ',';
                
                if(agre.nextPaymentDue <> null) csv += agre.nextPaymentDue + ',';    
                else csv += ',';
                
                if(agre.paidToDate <> null) csv += agre.paidToDate + ',';    
                else csv += ',';
                
                if(agre.status <> null) csv += agre.status + ',';    
                else csv += ',';
                
                if(agre.monthlyPayment <> null) csv += agre.monthlyPayment + ',';    
                else csv += ',';
                
                if(agre.monthsPaid <> null) csv += agre.monthsPaid + ',';    
                else csv += ',';
                
                if(agre.remainingPayments <> null) csv += agre.remainingPayments + ',';    
                else csv += ',';
                
                if(agre.studentEmail <> null) csv += agre.studentEmail + ',';    
                else csv += ',';
                
                if(agre.deferementType <> null) csv += agre.deferementType + ',';    
                else csv += ',';
               
                csv += '\n';
            }
        }
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentID {get;set;}
        public String studentName {get;set;}
        public List<AgreementWrapper> agreements {get;set;}
        
        public ReportDataWrapper(){
            this.agreements = new List<AgreementWrapper>();
        }    
    }

    
    //////////////////////////////////////////
    ///Agreement Wrapper
    //////////////////////////////////////////
    public class AgreementWrapper{
        public String accountNumber {get;set;}
        public String accountId {get;set;}
        public String accountName {get;set;}
        public String employer {get;set;}
        public String incomePerYear {get;set;}
        public Date expectedGraduationDate{get;set;}
        public Integer daysDelinquent{get;set;}
        public Decimal paymentCap{get;set;}
        public Date graceEndDate{get;set;}
        public String graduationY_N{get;set;}
        public Decimal paymentTerm{get;set;}
        public Decimal amountDueToDate{get;set;}
        public Decimal nextPaymentDue{get;set;}
        public Decimal paidToDate{get;set;}
        public String status{get;set;}
        public Decimal monthlyPayment{get;set;}
        public Integer monthsPaid{get;set;}
        public Integer remainingPayments{get;set;}
        public String studentEmail{get;set;}
        public String deferementType{get;set;}    
    }  
    
      
}