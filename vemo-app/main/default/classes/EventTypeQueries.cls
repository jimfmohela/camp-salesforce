public class EventTypeQueries {
    
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }
    
    public static Map<ID, EventType__c> getEventTypeMapWithEventTypeID(Set<ID> eventTypeIDs){
        Map<ID, EventType__c> eventTypeMap = new Map<ID, EventType__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(eventTypeIDs);
        
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        eventTypeMap = new Map<ID, EventType__c>((List<EventType__c>)db.query(query));
        return eventTypeMap;  
    }
    
    public static Map<ID, EventType__c> getAllEventTypesMap(){
        Map<ID, EventType__c> eventTypeMap = new Map<ID, EventType__c>();
        String query = generateSOQLSelect();
        
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        eventTypeMap = new Map<ID, EventType__c>((List<EventType__c>)db.query(query));
        return eventTypeMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM EventType__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'name, ';
        fieldNames += 'Description__c, ';
        fieldNames += 'MergeObject__c, ';
        fieldNames += 'MergeTemplate__c, ';
        fieldNames += 'MergeText__c, ';
        fieldNames += 'OwnerID ';
        return fieldNames;
   }
   
   
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }
  
    private static String buildFilterString(){
        String filterStr = '';
       
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +   '\' ');
            }      
        }
        return filterStr;
    }  
}