@isTest
public with sharing class ReconciliationBatch_TEST {
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup
    static void insertRequiredRecords(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        //TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'Recon_Kick_Off','Recon_Submission_Confirmation'});
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'Recon_Kick_Off','Recon_Intro'});
    }
    
    @isTest
    static void sendIntro(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            reconciltn.Status__c = 'Eligible';
            reconciltn.AdobeSignStatus__c = 'None';
        }
        dbUtil.insertRecords(reconciliationList);
                
        Test.startTest();
        ReconciliationBatch job = new ReconciliationBatch();
        job.job = ReconciliationBatch.JobType.SEND_INTRO;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    
    @isTest
    static void sendKickOff(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            reconciltn.Status__c = 'Documents Submitted';
            reconciltn.SendKickOffEmail__c  = false;
        }
        dbUtil.insertRecords(reconciliationList);
                
        Test.startTest();
        ReconciliationBatch job = new ReconciliationBatch();
        job.job = ReconciliationBatch.JobType.SEND_KICK_OFF;
        Database.executeBatch(job, 200);
        Test.stopTest();
    }
    /*@isTest
    static void createAgreementsTest1(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        VemoAdobeSignSettings.getSettings().apiAccessPoint = 'TestAPIAccessPoint';
        VemoAdobeSignSettings.getSettings().accessToken = 'TestAccessToken';
        VemoAdobeSignSettings.getSettings().authorizationCode = 'TestAuthCode';
        VemoAdobeSignSettings.getSettings().clientID = 'TestClientId';
        VemoAdobeSignSettings.getSettings().clientSecret = 'TestClientSecret';
        VemoAdobeSignSettings.getSettings().redirectURL = 'TestRedirectURL';
        VemoAdobeSignSettings.getSettings().refreshToken = 'TestRefreshToken';

        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            reconciltn.Status__c = 'Eligible';
            reconciltn.AdobeSignStatus__c = 'None';
        }
        //insert reconciliationList;
        dbUtil.insertRecords(reconciliationList);

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ReconciliationBatch_TEST.AdobeSignServiceCalloutResponses());
        ReconciliationBatch job = new ReconciliationBatch();
        job.job = ReconciliationBatch.JobType.CREATE_AGREEMENTS;
        Database.executeBatch(job);
              
        Test.stopTest();
    }
    
    
    @isTest
    static void createAgreementsTest2(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        VemoAdobeSignSettings.getSettings().apiAccessPoint = 'TestAPIAccessPoint';
        VemoAdobeSignSettings.getSettings().accessToken = 'TestAccessToken';
        VemoAdobeSignSettings.getSettings().authorizationCode = 'TestAuthCode';
        VemoAdobeSignSettings.getSettings().clientID = 'TestClientId';
        VemoAdobeSignSettings.getSettings().clientSecret = 'TestClientSecret';
        VemoAdobeSignSettings.getSettings().redirectURL = 'TestRedirectURL';
        VemoAdobeSignSettings.getSettings().refreshToken = 'TestRefreshToken';

        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            reconciltn.Status__c = 'Eligible';
            reconciltn.AdobeSignStatus__c = 'Document Uploaded to Adobe';
        }
        //insert reconciliationList;
        dbUtil.insertRecords(reconciliationList);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ReconciliationBatch_TEST.AdobeSignServiceCalloutResponses());
        ReconciliationBatch job = new ReconciliationBatch();        
        job.job = ReconciliationBatch.JobType.CREATE_ESIGN;
        Database.executeBatch(job);
                
        Test.stopTest();
    }
    
    @isTest
    static void createAgreementsTest3(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        VemoAdobeSignSettings.getSettings().apiAccessPoint = 'TestAPIAccessPoint';
        VemoAdobeSignSettings.getSettings().accessToken = 'TestAccessToken';
        VemoAdobeSignSettings.getSettings().authorizationCode = 'TestAuthCode';
        VemoAdobeSignSettings.getSettings().clientID = 'TestClientId';
        VemoAdobeSignSettings.getSettings().clientSecret = 'TestClientSecret';
        VemoAdobeSignSettings.getSettings().redirectURL = 'TestRedirectURL';
        VemoAdobeSignSettings.getSettings().refreshToken = 'TestRefreshToken';

        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            reconciltn.AgreementSigned__c = true;
            //reconciltn.SendKickOffEmail__c = true;
            reconciltn.AdobeSignStatus__c = 'Document Awaiting Signature';
        }
        //insert reconciliationList;
        dbUtil.insertRecords(reconciliationList);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ReconciliationBatch_TEST.AdobeSignServiceCalloutResponses());
        ReconciliationBatch job = new ReconciliationBatch();        
        job.job = ReconciliationBatch.JobType.RETRIEVE_SIGNED_AGREEMENTS;
        Database.executeBatch(job);
                
        Test.stopTest();
    }
    
    @isTest
    static void createAgreementsTest4(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        VemoAdobeSignSettings.getSettings().apiAccessPoint = 'TestAPIAccessPoint';
        VemoAdobeSignSettings.getSettings().accessToken = 'TestAccessToken';
        VemoAdobeSignSettings.getSettings().authorizationCode = 'TestAuthCode';
        VemoAdobeSignSettings.getSettings().clientID = 'TestClientId';
        VemoAdobeSignSettings.getSettings().clientSecret = 'TestClientSecret';
        VemoAdobeSignSettings.getSettings().redirectURL = 'TestRedirectURL';
        VemoAdobeSignSettings.getSettings().refreshToken = 'TestRefreshToken';

        List<Reconciliation__c> reconciliationList = TestDataFactory.createReconciliations(4);
        for(Reconciliation__c reconciltn : reconciliationList){
            //reconciltn.AgreementSigned__c = true;
            reconciltn.SendSubmissionConfirmation__c = false;
            reconciltn.Status__c = 'Documents Submitted';
        }
        //insert reconciliationList;
        dbUtil.insertRecords(reconciliationList);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ReconciliationBatch_TEST.AdobeSignServiceCalloutResponses());
        ReconciliationBatch job = new ReconciliationBatch();        
        job.job = ReconciliationBatch.JobType.SEND_CONFIRMATION;
        Database.executeBatch(job);
                
        Test.stopTest();
    }

    public class AdobeSignServiceCalloutResponses implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            if(req.getEndpoint().toLowerCase().contains('refresh')) {
                res.setBody('{"access_token":"3AAABLblqZhATIhEcQJtq1Vu0vG118g2yGBr6XBckJkzeke2Lw6FfaEm0oBR3gs7lPPjoJXoMty8HpCuHgaWo1H1z2-BLImMo","refresh_token":"3AAABLblqZhDWaG_fdq_mBNF7QkjMgLAe_8GWoaToKjD0WKrI9gdajg2t5n5Wl53oKjTklEjUubc*","token_type":"Bearer","expires_in":3600}');
            }
            else if(req.getEndpoint().toLowerCase().contains('signingurls')){
                res.setBody('{"signingUrlSetInfos":[{"signingUrlSetName":"Test-URLSetName","signingUrls":[{"esignUrl":"https://TestUrl.com","email":"reconciliationbatch@test.com"}]}]}');
            }
            else if(req.getEndpoint().toLowerCase().contains('combineddocument')){
                req.setBodyAsBlob(blob.valueOf('This is just a test body for document.'));
            }
            else if(req.getEndpoint().toLowerCase().endsWith('agreements')){
                res.setBody('{"agreementID":"TestAGREEMENT1","embeddedCode":"Test-embeddedCode","expiration":null,"url":"https://TestUrl.com"}');
            }
            res.setStatus('success');
            res.setStatusCode(200);
            return res;


        }
    }*/

}