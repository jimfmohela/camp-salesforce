/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIGenericDocumentResource
// 
// Description: 
//  Handles all Generic Document API Functionality
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-01-04   Greg Cook       Created                         
// 
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIGenericDocumentResource implements VemoAPI.ResourceHandler {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIGenericDocumentResource.handleGetV1');
        String genericDocumentIDparam = api.params.get('genericDocumentID');
        String parentIDparam;
        if(String.isNotBlank(String.valueOf(api.params.get('parentID')))){
            parentIDparam = String.valueOf(api.params.get('parentID')).substring(0,15);
        }
        

        List<GenericDocumentService.GenericDocument> docs = new List<GenericDocumentService.GenericDocument>();
        if(genericDocumentIDparam != null){

            docs = GenericDocumentService.getGenericDocumentWithGenericDocumentID(VemoApi.parseParameterIntoIDSet(genericDocumentIDparam));
        }else if(parentIDparam != null){
            System.debug('parentIDparam:'+parentIDparam);
            docs = GenericDocumentService.getGenericDocumentWithParentID(new Set<String>{parentIDparam});
        }  
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: genericDocumentID required for GET');
        }
        List<GenericDocumentOutputV1> results = new List<GenericDocumentOutputV1>();
        for(GenericDocumentService.GenericDocument doc : docs){
            results.add(new GenericDocumentOutputV1(doc));
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIGenericDocumentResource.handleGetV2');
        String genericDocumentIDparam = api.params.get('genericDocumentID');
        String parentIDparam;
        if(String.isNotBlank(String.valueOf(api.params.get('parentID')))){
            parentIDparam = String.valueOf(api.params.get('parentID')).substring(0,15);
        }
        

        List<GenericDocumentService.GenericDocument> docs = new List<GenericDocumentService.GenericDocument>();
        if(genericDocumentIDparam != null){

            docs = GenericDocumentService.getGenericDocumentWithGenericDocumentID(VemoApi.parseParameterIntoIDSet(genericDocumentIDparam));
        }else if(parentIDparam != null){
            System.debug('parentIDparam:'+parentIDparam);
            docs = GenericDocumentService.getGenericDocumentWithParentID(new Set<String>{parentIDparam});
        }  
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: genericDocumentID required for GET');
        }
        List<GenericDocumentOutputV2> results = new List<GenericDocumentOutputV2>();
        for(GenericDocumentService.GenericDocument doc : docs){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoGenericDocumentOutputV2(doc));
            } else {
                results.add(new PublicGenericDocumentOutputV2(doc));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIGenericDocumentResource.handlePostV1');
        List<GenericDocumentService.GenericDocument> newGenDocs = new List<GenericDocumentService.GenericDocument>();
        List<GenericDocumentResourceInputV1> GenDocsJSON = (List<GenericDocumentResourceInputV1>)JSON.deserialize(api.body, List<GenericDocumentResourceInputV1>.class);
        
        Set<ID> parentIDsToVerify = new Set<ID>();
        Map<ID, String> objectTypesByID = new Map<ID, String>();
        Map<String, Set<ID>> objectIDsByObjectType = new Map<String, Set<ID>>();
        for(GenericDocumentResourceInputV1 genDocsRes : GenDocsJSON){
            ID theID = (ID) genDocsRes.parentID;
            String objectType = GlobalUtil.findObjectNameFromRecordIdPrefix(genDocsRes.parentID);
            parentIDsToVerify.add(theID);
            if(!objectIDsByObjectType.containsKey(objectType)){
                objectIDsByObjectType.put(objectType, new Set<ID>());
            }
            objectIDsByObjectType.get(objectType).add(theID);
            objectTypesByID.put((ID) genDocsRes.parentID, objectType);
        }
        System.debug('objectTypesByID:'+objectTypesByID);
        System.debug('objectIDsByObjectTYpe:'+objectIDsByObjectType); 

        Map<String, Map<ID,SObject>> verifiedObjectsByObjectType = new Map<String, Map<ID, SObject>>();
        for(String objType : objectIDsByObjectType.keySet()){
            System.debug('objType:'+objType);
            if(objType == 'IncomeVerification__c'){
                verifiedObjectsByObjectType.put(objType, IncomeVerificationQueries.getIncomeVerificationMapWithIncomeVerificationId(objectIDsByObjectType.get(objType)));             
            } else if(objType == 'EmploymentHistory__c'){
                verifiedObjectsByObjectType.put(objType, EmploymentHistoryQueries.getEmploymentHistoryMapWithEmploymentHistoryId(objectIDsByObjectType.get(objType)));             
            } else if(objType == 'EmploymentHistory2__c'){
                verifiedObjectsByObjectType.put(objType, EmploymentHistoryQueries2.getEmploymentHistoryMapWithEmploymentHistoryId(objectIDsByObjectType.get(objType)));             
            } else if(objType == 'ReconciliationDetail__c'){
                verifiedObjectsByObjectType.put(objType, ReconciliationQueries.getRDetailMapWithRDetailID(objectIDsByObjectType.get(objType)));             
            } else if(objType == 'Reconciliation__c'){
                verifiedObjectsByObjectType.put(objType, ReconciliationQueries.getReconciliationMapWithReconciliationID(objectIDsByObjectType.get(objType)));             
            } else if(objType == 'Deferment__c'){
				verifiedObjectsByObjectType.put(objType, DefermentQueries.getDefermentMapWithDefermentID(objectIDsByObjectType.get(objType)));             
			}
			else {
                throw new VemoAPI.VemoAPIFaultException('Unauthorized objectType');
            }
        }
        System.debug('verifiedObjectsByObjectTYpe:'+verifiedObjectsByObjectType); //NOT WORKING
        
        String objectType;
        for(GenericDocumentResourceInputV1 genDocsRes : GenDocsJSON){
            objectType = '';
            if(objectTypesByID.containsKey(genDocsRes.parentID)){
                objectType = objectTypesByID.get(genDocsRes.parentID);                
                if(verifiedObjectsByObjectType.containsKey(objectType)){
                    if(verifiedObjectsByObjectType.get(objectType).containsKey((ID)genDocsRes.parentID)){
                        genDocsRes.validatePOSTFields();
                        GenericDocumentService.GenericDocument genDoc = genDocumentResourceV1ToGenDoc(genDocsRes);
                        if(DatabaseUtil.filterByStudentID){
                            genDoc.participantID = DatabaseUtil.studentPersonAccountID;
                        }
                        newGenDocs.add(genDoc);                        
                    } else {
                        throw new VemoAPI.VemoAPIFaultException('Unauthorized parentID');
                    }
                }        
            }
        }
        Set<ID> genDocIDs = new Set<ID>();
        if(newGenDocs.size()>0){
            genDocIDs = GenericDocumentService.createGenericDocument(newGenDocs);            
        }
        return (new VemoAPI.ResultResponse(genDocIDs, genDocIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIGenericDocumentResource.handlePutV1');
        List<GenericDocumentService.GenericDocument> newGenDocs = new List<GenericDocumentService.GenericDocument>();
        List<GenericDocumentResourceInputV1> GenDocsJSON = (List<GenericDocumentResourceInputV1>)JSON.deserialize(api.body, List<GenericDocumentResourceInputV1>.class);

        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        for(GenericDocumentResourceInputV1 genDocsRes : GenDocsJSON){
            idsToVerify.add((ID) genDocsRes.genericDocumentID);
        }
        Map<ID, GenericDocument__c> verifiedGenDocMap = GenericDocumentQueries.getGenericDocumentMapByID(idsToVerify); //authorized records should be returned


        for(GenericDocumentResourceInputV1 genDocsRes : GenDocsJSON){
            if(verifiedGenDocMap.containsKey(genDocsRes.genericDocumentID)){ //only verified records should be updated
                genDocsRes.validatePUTFields();
                GenericDocumentService.GenericDocument genDoc = genDocumentResourceV1ToGenDoc(genDocsRes);
                newGenDocs.add(genDoc);
            }
        }
        Set<ID> genDocIDs = GenericDocumentService.updateGenericDocument(newGenDocs);
        return (new VemoAPI.ResultResponse(genDocIDs, genDocIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIGenericDocumentResource.handleDeleteV1');
        String genDocIDParam = api.params.get('genericDocumentID');   

        Set<ID> genDocIDs = VemoApi.parseParameterIntoIDSet(genDocIDParam);

        /* Check to make sure this authID can access these records*/
        Map<ID, GenericDocument__c> verifiedGenDocMap = GenericDocumentQueries.getGenericDocumentMapByID(genDocIDs); //authorized records should be returned
        

        Set<ID> genDocIDsToDelete = new Set<ID>();
        Integer numToDelete = 0;
        for(ID theID : genDocIDs){
            if(verifiedGenDocMap.containsKey(theID)){
                genDocIDsToDelete.add(theID);
            }
        }
        if(verifiedGenDocMap.size()>0){
            numToDelete = GenericDocumentService.deleteGenericDocument(genDocIDsToDelete);
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }

    public static GenericDocumentService.GenericDocument genDocumentResourceV1ToGenDoc(GenericDocumentResourceInputV1 disRes){
        GenericDocumentService.GenericDocument genDoc = new GenericDocumentService.GenericDocument();
        genDoc.genericDocumentID = disRes.genericDocumentID;
        genDoc.type = disRes.type;
        genDoc.status = disRes.status;
        genDoc.parentID = disRes.parentID;
        genDoc.parentObjectType = disRes.parentObjectType;
        genDoc.comments = disRes.comments;
        return genDoc;
    }

    public class GenericDocumentResourceInputV1{
        public String genericDocumentID {get;set;}
        public String type {get;set;}
        public String status {get;set;}
        public String parentID {get;set;}
        public String parentObjectType {get;set;}
        public String comments {get;set;}



        public GenericDocumentResourceInputV1(){}

        public GenericDocumentResourceInputV1(Boolean testValues){
            if(testValues){

            }
        }
        public void validatePOSTFields(){
            if(genericDocumentID != null) throw new VemoAPI.VemoAPIFaultException('genericDocumentID cannot be created in POST');               
        }
        public void validatePUTFields(){
            if(genericDocumentID == null) throw new VemoAPI.VemoAPIFaultException('genericDocumentID is a required input parameter on PUT');        
        }
    }

    //public static GenericDocumentService.GenericDocument genDocClassToGenDocObject(GenericDocument docClass){
    //  GenericDocument__c docObj = new GenericDocument__c();
    //  if(docClass.genericDocumentID != null) docObj.ID = docClass.genericDocumentID;
    //  if(docClass.type != null) docObj.Type__c = docClass.type;
    //  if(docClass.status != null) docObj.Status__c = docClass.status;

    //  return docObj;
    //} 

    public class GenericDocumentOutputV1{
        public String genericDocumentID {get;set;}
        public String type {get;set;}
        public String status {get;set;}
        public String attachmentID {get;set;}
        public String attachmentName {get;set;}
        //public String attachmentURL {get;set;} hide for security reasons
        public String parentID {get;set;}
        public String parentObjectType {get;set;}
        public String comments {get;set;}


        public GenericDocumentOutputV1(){

        }

        public GenericDocumentOutputV1(Boolean testValues){
            if(testValues){
                

            }
        }

        public GenericDocumentOutputV1(GenericDocumentService.GenericDocument doc){
            this.genericDocumentID = doc.genericDocumentID;
            this.type = doc.type;
            this.status = doc.status;
            this.attachmentID = doc.attachmentID;
            this.attachmentName = doc.attachmentName;
            //this.attachmentURL = doc.attachmentURL;
            this.parentID = doc.parentID;
            this.parentObjectType = doc.parentObjectType;
            this.comments = doc.comments;
        }
    }
    
    public virtual class GenericDocumentOutputV2{
        public String genericDocumentID {get;set;}
        
        public GenericDocumentOutputV2(){
        
        } 
        
        public GenericDocumentOutputV2(GenericDocumentService.GenericDocument doc){
            this.genericDocumentID = doc.genericDocumentID;
        }  
    }
    
    public class PublicGenericDocumentOutputV2 extends GenericDocumentOutputV2{
        public PublicGenericDocumentOutputV2(){

        }
        public PublicGenericDocumentOutputV2(GenericDocumentService.GenericDocument doc){
            super(doc);
        }
    }

    public class VemoGenericDocumentOutputV2 extends GenericDocumentOutputV2{
        public String privateInfo {get;set;}
        public VemoGenericDocumentOutputV2(){

        }        
        public VemoGenericDocumentOutputV2(GenericDocumentService.GenericDocument doc){
            super(doc);
            this.privateInfo = 'test';
        }
    }     
}