/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIDataCollectionTemplateResource_TEST
// 
// Description: 
//      Test class for VemoAPIDataCollectionTemplateResource
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-05   Rini Gupta  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIDataTemplateResource_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void validateVemoAPIDataTemplateResource(){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, DataCollectionTemplate__c> testDataCollectionTemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        
        Test.startTest();
        List<DataCollectionTemplateService.Template> resultDataCollectionTemplateMap = DataCollectionTemplateService.getTemplateWithProgramID(programMap.keyset());
        //System.assertEquals(testDataCollectionTemplateMap.keySet().size(), resultDataCollectionTemplateMap.size());
        
        List<VemoAPIDataCollectionTemplateResource> templateList = new List<VemoAPIDataCollectionTemplateResource>();
        for(DataCollectionTemplateService.Template template: resultDataCollectionTemplateMap){
            VemoAPIDataCollectionTemplateResource templateResource = new VemoAPIDataCollectionTemplateResource(template);    
            templateList.add(templateResource); 
        }
        
        Test.stopTest();
    }
    
}