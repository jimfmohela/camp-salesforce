@isTEST
public class ReportBillingVolumeController_TEST{
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    public static string selectedSchool = '';
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole1', email='rini.gupta1@vemo.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing1', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta1@vemo.com', communitynickname = 'testcommunity1');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        //create students
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(10);
        //create school
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account acc: schoolMap.Values()){
            acc.name = 'Purdue University-Main Campus';
        }
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        dbUtil.updateRecords(schoolMap.Values());
        //create program
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1,schoolMap);
        //create student programs
        Map<ID, StudentProgram__c> spMap = TestDataFactory.createAndInsertStudentProgram(1,studentMap,programMap);
        //create transactions
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(2,spMap,'Disbursement');
        Date dt = Date.newInstance(2017,1,5);
        for(transaction__c tx:txMap.values()){
            tx.transactionDate__c = dt.addDays(20);
        }
        dbUtil.updateRecords(txMap.values());
        
        for(ID theID:schoolMap.keySet()){
            selectedSchool = theID;
        }
        //create audit records
        List<StudentProgramAudit__c> auditToInsert = new List<StudentProgramAudit__c>();
        for(StudentProgram__c sp:spMap.values()){
            for(integer i=1;i<=12;i++){
                StudentProgramAudit__c spa = new StudentProgramAudit__c();
                spa.auditdatetime__c = Date.newInstance(2018,i,Date.daysInMonth(2018,i));
                spa.monthend__c = true;
                if(i==1 || i==2 || i==3 || i == 4){
                    spa.status__c = 'Certified';
                }
                else if(i==5 || i==6 || i==7){
                    spa.status__c = 'Grace';
                }
                else if(i==8 || i==9 || i==10){
                    spa.status__c = 'Deferment';
                }
                else if(i==11 || i==12){
                    spa.status__c = 'Cancelled';
                }
                auditToInsert.add(spa);
            }    
            
        }
        }
    }
    
    @isTest public static void validatePicklist(){
       
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        cntrl.getQuarter();
        cntrl.getSchool();
    }
        
    @isTest public static void validateQuarter1(){
        //Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        cntrl.selectedQuarter = 'Quarter 1 (Jan-Mar)';
        cntrl.selectedYear = '2018';
        //for(ID theID:schoolMap.keySet()){
            cntrl.selectedSchool = selectedSchool;
        //}
        cntrl.runReport();
        cntrl.generateCSV();
    }
    
    
    @isTest public static void validateQuarter2(){
        //Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        cntrl.selectedQuarter = 'Quarter 2 (Apr-Jun)';
        cntrl.selectedYear = '2018';
        //for(ID theID:schoolMap.keySet()){
            cntrl.selectedSchool = selectedSchool;
        //}
        cntrl.runReport();
        cntrl.generateCSV();
    }
    
    
    @isTest public static void validateQuarter3(){
        //Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        cntrl.selectedQuarter = 'Quarter 3 (Jul-Sep)';
        cntrl.selectedYear = '2018';
        //for(ID theID:schoolMap.keySet()){
            cntrl.selectedSchool = selectedSchool;
        //}
        cntrl.runReport();
        cntrl.generateCSV();
    }
    
    
    @isTest public static void validateQuarter4(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        //Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        cntrl.selectedQuarter = 'Quarter 4 (Oct-Dec)';
        cntrl.selectedYear = '2018';
        //for(ID theID:schoolMap.keySet()){
            cntrl.selectedSchool = selectedSchool;
        //}
        cntrl.runReport();
        cntrl.generateCSV();
    }

    @isTest public static void validateGetYear(){
        ReportBillingVolumeController cntrl = new ReportBillingVolumeController();
        List<SelectOption> options = cntrl.getYear();
        System.assert(Integer.valueOf(options[options.size()-1].getValue()) > Date.today().year(),'Error');
    }
}