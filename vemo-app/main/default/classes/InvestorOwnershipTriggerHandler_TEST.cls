@isTest
public class InvestorOwnershipTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest public static void investorOwnershipDMLsTest() {
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        
        //List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        //AccessRule__c accessRule = new AccessRule__c();
        //accessRule.Account__c = studentMap.values()[0].Id;
        //accessRule.Active__c = true;
        //accessRule.ParticipantAccount__c = studentMap.values()[0].Id;
        //accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        //accessRuleList.add(accessRule);
        
        //insert accessRuleList;
        //dbUtil.insertRecords(accessRuleList);
        Map<Id, InvestorOwnership__c> InvestorOwnershipMap = TestDataFactory.createAndInsertInvestorOwnership(studentMap, studentPrgMap);
        Test.startTest();
        
        //update InvestorOwnershipMap.values();
        //delete InvestorOwnershipMap.values();
        //undelete InvestorOwnershipMap.values();
        
        dbUtil.updateRecords(InvestorOwnershipMap.values());
        dbUtil.deleteRecords(InvestorOwnershipMap.values());
        dbUtil.undeleteRecords(InvestorOwnershipMap.values());
        
        Test.stopTest();
        }
    }

    @isTest public static void manageAccessRules_Test() {
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMAp);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        //Move insertion of AccessRule to TestDataFatory
        //List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        //AccessRule__c accessRule = new AccessRule__c();
        //accessRule.Account__c = studentMap.values()[0].Id;
        //accessRule.Active__c = true;
        //accessRule.ParticipantAccount__c = studentMap.values()[0].Id;
        //accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        //accessRuleList.add(accessRule);
        
        //insert accessRuleList;
        //dbUtil.insertRecords(accessRuleList);

        Test.startTest();
        Map<Id, InvestorOwnership__c> InvestorOwnershipMap = TestDataFactory.createAndInsertInvestorOwnership(studentMap, studentPrgMap);
        Test.stopTest();
        }
    }

    //Ownership percentage changed to zero
    @isTest public static void manageAccessRules_OPCZeroTest() {
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMAp);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        //Move insertion of AccessRule to TestDataFatory
        //List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        //AccessRule__c accessRule = new AccessRule__c();
        //accessRule.Account__c = studentMap.values()[0].Id;
        //accessRule.Active__c = true;
        //accessRule.ParticipantAccount__c = studentMap.values()[0].Id;
        //accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        //accessRuleList.add(accessRule);
        
        //insert accessRuleList;
        //dbUtil.insertRecords(accessRuleList);

        Test.startTest();
        Map<Id, InvestorOwnership__c> InvestorOwnershipMap = TestDataFactory.createAndInsertInvestorOwnership(studentMap, studentPrgMap);
        InvestorOwnershipMap.values()[0].OwnershipPercent__c = 80;
        update InvestorOwnershipMap.values();
        Test.stopTest();
        }
    }

    //Ownership percentage changed to null
    @isTest public static void manageAccessRules_NullOPTest() {
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        //Move insertion of AccessRule to TestDataFatory
        //List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        //AccessRule__c accessRule = new AccessRule__c();
        //accessRule.Account__c = studentMap.values()[0].Id;
        //accessRule.Active__c = true;
        //accessRule.ParticipantAccount__c = studentMap.values()[0].Id;
        //accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        //accessRuleList.add(accessRule);
        
        //insert accessRuleList;
        //dbUtil.insertRecords(accessRuleList);
        
        Test.startTest();
        Map<Id, InvestorOwnership__c> InvestorOwnershipMap = TestDataFactory.createAndInsertInvestorOwnership(studentMap, studentPrgMap);
        InvestorOwnershipMap.values()[0].OwnershipPercent__c = null;
        //update InvestorOwnershipMap.values();
        dbUtil.updateRecords(InvestorOwnershipMap.values());
        Test.stopTest();
        }
    }
}