/////////////////////////////////////////////////////////////////////////
// Class: EventSubscriptionService_TEST
// 
// Description: 
//  Test class for EventSubscriptionService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created              
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventSubscriptionService_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventSubscriptionMapWithAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes );
        
        Test.startTest();
        List<EventSubscriptionService.EventSubscription> resultEventSubscriptionList = EventSubscriptionService.getEventSubscriptionMapWithAuthUser();
        System.assertEquals(testEventSubscriptionMap.keySet().size(), resultEventSubscriptionList.size());
        Test.stopTest();
    }
    /*static testMethod void testGetEventSubscriptionMapForAuthUserwithRead(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(1,testContactMap );
        
        Test.startTest();
        List<EventSubscriptionService.EventSubscription> resultEventSubscriptionList = EventSubscriptionService.getEventSubscriptionMapForAuthUser(true);
        System.assertEquals(testEventSubscriptionMap.keySet().size(), resultEventSubscriptionList.size());
        Test.stopTest();
    }*/
    
    static testMethod void testcreateEventSubscription(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        //Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(1,testContactMap );
        List<EventSubscriptionService.EventSubscription> evntSubsList = new List<EventSubscriptionService.EventSubscription>();
        
        Map<id,EventType__c> testEvenTypeMap = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EventSubscriptionService.EventSubscription eventSubs = new EventSubscriptionService.EventSubscription();
            eventSubs.account = testStudentAccountMap.values().get(i).id;
            eventSubs.contact = testContactMap.values().get(i).id;
            eventSubs.eventType = testEvenTypeMap.values().get(i).id;
            evntSubsList.add(eventSubs);
        }
        Test.startTest();
        Set<ID> empHisIDs = EventSubscriptionService.createEventSubscription(evntSubsList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, evntSubsList.size());
    }
    
    static testMethod void testUpdateEventSubscription(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes );
        List<EventSubscriptionService.EventSubscription> evntSubsList = new List<EventSubscriptionService.EventSubscription>();
        
        Map<id,EventType__c> testEvenTypeMap = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EventSubscriptionService.EventSubscription eventSubs = new EventSubscriptionService.EventSubscription();
            eventSubs.eventSubscriptionID = testEventSubscriptionMap.values().get(i).id;
            eventSubs.account = testStudentAccountMap.values().get(i).id;
            eventSubs.contact = testContactMap.values().get(i).id;
            eventSubs.eventType = testEvenTypeMap.values().get(i).id;
            evntSubsList.add(eventSubs);
        }
        Test.startTest();
        Set<ID> empHisIDs = EventSubscriptionService.updateEventSubscription(evntSubsList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, evntSubsList.size());
    }
    
    static testMethod void testDeleteEventSubscription(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes);
        
        Test.startTest();
        Integer deletedNum = EventSubscriptionService.deleteEventSubscriptions(testEventSubscriptionMap.keyset());        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE*TestUtil.TEST_THROTTLE, deletedNum);
    }
}