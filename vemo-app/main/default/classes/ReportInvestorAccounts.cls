public with sharing class ReportInvestorAccounts{
    public transient Map<Id,Account> allAccounts {get;set;}
    public transient Set<Id> allAccountsIds {get;set;}
    public transient Map<Id,Integer> fundOwnerAccounts {get;set;}
    public transient Map<Id,Integer> primaryOwnerAccounts {get;set;}
    public transient Map<Id,Integer> paymentStreamOwnerAccounts {get;set;}
    //public transient Map<Id,String> accRecordType {get;set;}
    
    public ReportInvestorAccounts(){
        allAccounts = new Map<Id,Account>();
        //accRecordType = new Map<Id,String>();
        fundOwnerAccounts = new Map<Id,Integer>();
        primaryOwnerAccounts = new Map<Id,Integer>();
        paymentStreamOwnerAccounts = new Map<Id,Integer>();
        allAccountsIds = new Set<Id>();
        runReport();      
    }
    
    public void runReport(){
        allAccounts = new Map<Id,Account>([Select id,Name,RecordType.Name From Account where recordtype.name='Investor' order by name]);
        allAccountsIds = new Set<Id>();
        for(Account acc: allAccounts.values()){
            allAccountsIds.add(acc.id);
        }
        
        List<StudentProgram__c> spList = [SELECT id,FundOwner__c,FundOwner__r.Name,FundOwner__r.RecordType.Name,
                                          PrimaryOwner__c,PrimaryOwner__r.Name,PrimaryOwner__r.RecordType.Name,
                                          PaymentStreamOwner__c,PaymentStreamOwner__r.Name,PaymentStreamOwner__r.RecordType.Name
                                          FROM StudentProgram__c];
                                          
        for(StudentProgram__c sp: spList){
            if(sp.FundOwner__c != null){                
                allAccountsIds.add(sp.FundOwner__c);
                if(!fundOwnerAccounts.containsKey(sp.FundOwner__c))
                    fundOwnerAccounts.put(sp.FundOwner__c,0);
                fundOwnerAccounts.put(sp.FundOwner__c,fundOwnerAccounts.get(sp.FundOwner__c)+1);
            }
            if(sp.PrimaryOwner__c != null){
                allAccountsIds.add(sp.PrimaryOwner__c);
                if(!primaryOwnerAccounts.containsKey(sp.PrimaryOwner__c))
                    primaryOwnerAccounts.put(sp.PrimaryOwner__c,0);
                primaryOwnerAccounts.put(sp.PrimaryOwner__c,primaryOwnerAccounts.get(sp.PrimaryOwner__c)+1);
            }
            if(sp.PaymentStreamOwner__c != null){
                allAccountsIds.add(sp.PaymentStreamOwner__c);
                if(!paymentStreamOwnerAccounts.containsKey(sp.PaymentStreamOwner__c))
                    paymentStreamOwnerAccounts.put(sp.PaymentStreamOwner__c,0);
                paymentStreamOwnerAccounts.put(sp.PaymentStreamOwner__c,paymentStreamOwnerAccounts.get(sp.PaymentStreamOwner__c)+1);
            }
        }
        
        for(Id ids: allAccountsIds){
            if(!fundOwnerAccounts.containsKey(ids))
                fundOwnerAccounts.put(ids,0);
            if(!primaryOwnerAccounts.containsKey(ids))
                primaryOwnerAccounts.put(ids,0);
            if(!paymentStreamOwnerAccounts.containsKey(ids))
                paymentStreamOwnerAccounts.put(ids,0);
        }
        
        
    }
}