public with sharing class EventInstanceService {

    public static List<EventInstance> getEventInstanceMapForAuthUser(){
    
        set<Id> contactIDSet = new set<ID>();
        //contactIDSet.add(UserService.userContext.ContactId);
        if(UserService.userContext != null && UserService.userContext.VemoContact__c != null)
            contactIDSet.add(UserService.userContext.VemoContact__c);
        
        Map<Id, EventInstance__c> eventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(contactIDSet);
        List<EventInstance> eventInstanceList = new List<EventInstance>();
        for(EventInstance__c ei : eventInstanceMap.values()){
            eventInstanceList.add(new EventInstance(ei));
        }
        return eventInstanceList;
    }
    
    public static List<EventInstance> getEventInstanceMapForAuthUser(boolean read){
        
        set<Id> contactIDSet = new set<ID>();
        contactIDSet.add(UserService.userContext.ContactId);
        
        Map<Id, EventInstance__c> eventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(contactIDSet,read);
        List<EventInstance> eventInstanceList = new List<EventInstance>();
        for(EventInstance__c ei : eventInstanceMap.values()){
            eventInstanceList.add(new EventInstance(ei));
        }
        return eventInstanceList;
    }
    
    public static List<EventInstance> getEventLatestInstanceMapForAuthUser(string read){
        
        set<Id> contactIDSet = new set<ID>();
        contactIDSet.add(UserService.userContext.ContactId);
        Map<Id, EventInstance__c> eventInstanceMap;
        
        if(read != null && read != '')
            eventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(contactIDSet,boolean.valueOf(read));
        else
            eventInstanceMap = EventInstanceQueries.getEventInstanceMapForAuthUser(contactIDSet);
        
        List<EventInstance> eventInstanceList = new List<EventInstance>();
        Map<Id, EventInstance> whatIdNEventInstanceMap = new Map<Id, EventInstance>();
        
        for(EventInstance__c ei : eventInstanceMap.values()){
            
            if(ei.WhatID__c != null && whatIdNEventInstanceMap.get(ei.WhatID__c) == null){
                eventInstanceList.add(new EventInstance(ei));
                whatIdNEventInstanceMap.put(ei.WhatID__c,new EventInstance(ei));
            }
        }
        return eventInstanceList;
    }
    
    public static Set<ID> updateEventInstance(List<EventInstance> EventInstances, String markAllasRead){
    
        List<EventInstance__c> eInstanceList = new List<EventInstance__c>();
        for(EventInstance ei : EventInstances){
            if(! (markAllasRead != null && markAllasRead != ''))
                eInstanceList.add(EventInstanceToEventInstance(ei));
            else{
                EventInstance__c ein = new EventInstance__c();
                eIn.Id = ei.eventInstanceID;
                
                if(markAllasRead == 'true') ein.read__c = true;
                else if (markAllasRead == 'false') ein.read__c = false;
                eInstanceList.add(ein);
            }
        }
        update eInstanceList;
        Set<ID> eventInstanceIDs = new Set<ID>();
        for(EventInstance__c ei : eInstanceList){
            eventInstanceIDs.add(ei.ID);
        }
        return eventInstanceIDs;
    }
    
    /* public static Integer deleteEventInstances(Set<ID> eSubscriptionIDs){
        System.debug('EventInstanceService.deleteEventInstances()');
        Map<ID, EventInstance__c> esMap = EventInstanceQueries.getEventInstanceMapWithEventInstanceID(eSubscriptionIDs);
        Integer numToDelete = esMap.size();
        delete esMap.values();
        return numToDelete;
    }*/

    public static EventInstance__c EventInstanceToEventInstance(EventInstance ei){
        EventInstance__c eIns = new EventInstance__c();
        if(ei.eventInstanceID != null) eIns.ID = ei.eventInstanceID;
        if(ei.read != null) eIns.Read__c = ei.read;
        //if(ei.contact != null) eIns.Contact__c = ei.contact;
        //if(ei.email != null) eIns.Email__c = ei.email;
        //if(ei.event != null) eIns.Event__c = ei.event;
        return eIns;
    }

    public class EventInstance{
        public String eventInstanceID{get;set;}
        public boolean read{get;set;}
        public String contactID{get;set;}
        public boolean email{get;set;}
        public String eventID {get;set;}
        public String eventAttributes {get;set;}
        public String objectType {get;set;}
        public String whatID {get;set;}
        public String eventType{get;set;}
        public datetime createdDate {get;set;}
        
        public EventInstance(){}
        
        public EventInstance(EventInstance__c ei){
          this.eventInstanceID = ei.ID;
          this.read = ei.Read__c;
          this.contactID = ei.Contact__c;
          this.email = ei.Email__c;
          this.eventID = ei.Event__c;
          this.eventAttributes = ei.EventAttributes__c;
          this.objectType = ei.ObjectType__c;
          this.whatID = ei.WhatID__c;
          this.eventType = ei.EventType__c;
          this.createdDate = ei.createdDate;
        }
    }
}