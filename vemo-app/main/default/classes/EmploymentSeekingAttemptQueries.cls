public class EmploymentSeekingAttemptQueries {
       
    public static Map<ID, EmploymentSeekingAttempt__c> getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(Set<ID> EmploymentSeekingAttemptIDs){
        Map<ID, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = new Map<ID, EmploymentSeekingAttempt__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(EmploymentSeekingAttemptIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Deferment__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        //query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        EmploymentSeekingAttemptMap = new Map<ID, EmploymentSeekingAttempt__c>((List<EmploymentSeekingAttempt__c>)db.query(query));
        return EmploymentSeekingAttemptMap;  
    }

    public static Map<ID, EmploymentSeekingAttempt__c> getEmploymentSeekingAttemptMapWithDefermentID(Set<ID> defermentIDs){
        Map<ID, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = new Map<ID, EmploymentSeekingAttempt__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deferment__c IN ' + DatabaseUtil.inSetStringBuilder(defermentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Deferment__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        //query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        EmploymentSeekingAttemptMap = new Map<ID, EmploymentSeekingAttempt__c>((List<EmploymentSeekingAttempt__c>)db.query(query));
        return EmploymentSeekingAttemptMap;
    }

    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM EmploymentSeekingAttempt__c ';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'Deferment__c, ';
        fieldNames += 'Deferment__r.Student__c, ';
        fieldNames += 'CityEmployment__c, ';
        fieldNames += 'ContactDate__c, ';
        fieldNames += 'CountryEmployment__c, ';
        fieldNames += 'EmployerName__c, ';
        fieldNames += 'FollowUpDate__c, ';
        fieldNames += 'ResultsOutcome__c, ';
        fieldNames += 'StateEmployment__c, ';
        fieldNames += 'JobPositionTypeofWork__c ';
        
        return fieldNames;
    }

    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
    
    
}