/////////////////////////////////////////////////////////////////////////
// Class: AccessRuleTriggerHandler 
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-01-10   Rini Gupta       Created                              
// 
/////////////////////////////////////////////////////////////////////////

public with sharing class AccessRuleTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {
    
    /**************************Static Variables***********************************/

    /**************************State acctrol Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution acctrol - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.Triggercontext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.Triggercontext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'AccessRuleTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'AccessRuleTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            system.debug('tc.handler--'+tc.handler);
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onBeforeInsert()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> newAccessRuleList = (List<AccessRule__c>)tc.newList;
        //This is where you should call your business logic
    

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onBeforeUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> newAccessRuleList = (List<AccessRule__c>)tc.newList;
        List<AccessRule__c> oldAccessRuleList = (List<AccessRule__c>)tc.oldList;
        Map<ID, AccessRule__c> newAccessRuleMap = (Map<ID, AccessRule__c>)tc.newMap;
        Map<ID, AccessRule__c> oldAccessRuleMap = (Map<ID, AccessRule__c>)tc.oldMap;
        //This is where you should call your business logic
    

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onBeforeDelete()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> oldAccessRuleList = (List<AccessRule__c>)tc.oldList;
        Map<ID, AccessRule__c> oldAccessRuleMap = (Map<ID, AccessRule__c>)tc.oldMap;
        //This is where you should call your business logic
        Map<String, Map<ID, AccessRule__c>> accessRulesByObjectType = determineObjectType(oldAccessRuleMap);
        for(String objectType : accessRulesByObjectType.keyset()){
            if(objectType == 'StudentProgram__c'){
                deleteStudentShares(oldAccessRuleMap, null);
                deleteDataCollectionShares(oldAccessRuleMap, null);
            }
            else if(objectType == 'CreditCheck__c') deleteCreditCheckShares(oldAccessRuleMap, null);
            else if(objectType == 'Transaction__c') deleteDisbursementShares(oldAccessRuleMap, null);
            else if(objectType == 'AcademicEnrollment__c') deleteAcademicEnrollmentShares(oldAccessRuleMap, null);
        }
    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onAfterInsert()');
         //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> newAccessRuleList = (List<AccessRule__c>)tc.newList;
        Map<ID, AccessRule__c> newAccessRuleMap = (Map<ID, AccessRule__c>)tc.newMap;
        //This is where you should call your business logic

        Map<String, Map<ID, AccessRule__c>> accessRulesByObjectType = determineObjectType(newAccessRuleMap);

        for(String objectType : accessRulesByObjectType.keyset()){
            if(objectType == 'StudentProgram__c') insertStudentShare(null, newAccessRuleMap);
            else if(objectType == 'CreditCheck__c') insertCreditCheckShare(null, newAccessRuleMap);
            else if(objectType == 'Transaction__c') insertDisbursementShare(null, newAccessRuleMap);
            else if(objectType == 'AcademicEnrollment__c') insertAcademicEnrollmentShare(null, newAccessRuleMap);
            
        }
        
        //insertStudentShare(null, newAccessRuleMap);
        //insertCreditCheckShare(null, newAccessRuleMap);
        //insertDisbursementShare(null, newAccessRuleMap);

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onAfterUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> newAccessRuleList = (List<AccessRule__c>)tc.newList;
        List<AccessRule__c> oldAccessRuleList = (List<AccessRule__c>)tc.oldList;
        Map<ID, AccessRule__c> newAccessRuleMap = (Map<ID, AccessRule__c>)tc.newMap;
        Map<ID, AccessRule__c> oldAccessRuleMap = (Map<ID, AccessRule__c>)tc.oldMap;
        //This is where you should call your business logic
        
        Map<String, Map<ID, AccessRule__c>> accessRulesByObjectType = determineObjectType(newAccessRuleMap);
        for(String objectType : accessRulesByObjectType.keyset()){
            if(objectType == 'StudentProgram__c') insertStudentShare(null, newAccessRuleMap);
            else if(objectType == 'CreditCheck__c') insertCreditCheckShare(null, newAccessRuleMap);
            else if(objectType == 'Transaction__c') insertDisbursementShare(null, newAccessRuleMap);
            else if(objectType == 'AcademicEnrollment__c') insertAcademicEnrollmentShare(null, newAccessRuleMap);
            
        }
        //insertStudentShare(oldAccessRuleMap, newAccessRuleMap);
        //insertCreditCheckShare(oldAccessRuleMap, newAccessRuleMap);
        //insertDisbursementShare(oldAccessRuleMap, newAccessRuleMap);

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onAfterDelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> oldAccessRuleList = (List<AccessRule__c>)tc.oldList;
        Map<ID, AccessRule__c> oldAccessRuleMap = (Map<ID, AccessRule__c>)tc.oldMap;
        //This is where you should call your business logic
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.Triggercontext tc){
        system.debug('AccessRuleTriggerHandler.onAfterUndelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AccessRule__c> newAccessRuleList = (List<AccessRule__c>)tc.newList;
        Map<ID, AccessRule__c> newAccessRuleMap = (Map<ID, AccessRule__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    
    private void insertStudentShare(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Set<Id> AccessRuleId = new Set<Id>(); 
        for(AccessRule__c rule: newAccessRuleMap.values()){
            //if(rule.StudentProgram__c != null){
                if(oldAccessRuleMap != null){
                    if(newAccessRuleMap.get(rule.id).AccountReadAccess__c != oldAccessRuleMap.get(rule.id).AccountReadAccess__c 
                       || newAccessRuleMap.get(rule.id).AccountEditAccess__c != oldAccessRuleMap.get(rule.id).AccountEditAccess__c 
                       || newAccessRuleMap.get(rule.Id).CaseReadAccess__c != oldAccessRuleMap.get(rule.id).CaseReadAccess__c 
                       || newAccessRuleMap.get(rule.Id).CaseEditAccess__c != oldAccessRuleMap.get(rule.id).CaseEditAccess__c 
                       || newAccessRuleMap.get(rule.Id).OpportunityReadAccess__c != oldAccessRuleMap.get(rule.id).OpportunityReadAccess__c 
                       || newAccessRuleMap.get(rule.Id).OpportunityEditAccess__c != oldAccessRuleMap.get(rule.id).OpportunityEditAccess__c
                      ){
                        
                        AccessRuleId.add(rule.id); 
                    }
                }
                else{
                    AccessRuleId.add(rule.id); 
                }
            //}
        }
        
        if(AccessRuleId.size()>0)
            sharingService.studentSharing(AccessRuleId);
        
        
    }
    
    private void insertCreditCheckShare(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Set<Id> AccessRuleId = new Set<Id>();
        for(AccessRule__c rule: newAccessRuleMap.values()){
            //if(rule.CreditCheck__c != null){
                if(oldAccessRuleMap != null){
                    if(newAccessRuleMap.get(rule.id).CreditCheckReadAccess__c != oldAccessRuleMap.get(rule.id).CreditCheckReadAccess__c 
                       || newAccessRuleMap.get(rule.id).CreditCheckEditAccess__c != oldAccessRuleMap.get(rule.id).CreditCheckEditAccess__c
                      ){
                          AccessRuleId.add(rule.id); 
                      }
                }
                else{
                    AccessRuleId.add(rule.id); 
                }
            //}
        }
        
        if(AccessRuleId.size()>0)
            sharingService.creditCheckSharing(AccessRuleId);
    }
    
    private void insertDisbursementShare(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Set<Id> AccessRuleId = new Set<Id>();
        for(AccessRule__c rule: newAccessRuleMap.values()){
            //if(rule.Disbursement__c != null){
                if(oldAccessRuleMap != null){
                    if(newAccessRuleMap.get(rule.id).DisbursementReadAccess__c != oldAccessRuleMap.get(rule.id).DisbursementReadAccess__c 
                       || newAccessRuleMap.get(rule.id).DisbursementEditAccess__c != oldAccessRuleMap.get(rule.id).DisbursementEditAccess__c
                      ){
                          AccessRuleId.add(rule.id); 
                      }
                }
                else{
                    AccessRuleId.add(rule.id); 
                }
            //}
        }
        
        if(AccessRuleId.size()>0)
            sharingService.disbursementSharing(AccessRuleId);
    }
    
    private void insertAcademicEnrollmentShare(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Set<Id> AccessRuleId = new Set<Id>();
        for(AccessRule__c rule: newAccessRuleMap.values()){
            if(oldAccessRuleMap != null){
                if(newAccessRuleMap.get(rule.id).AcademicEnrollmentReadAccess__c != oldAccessRuleMap.get(rule.id).AcademicEnrollmentReadAccess__c
                   || newAccessRuleMap.get(rule.id).AcademicEnrollmentEditAccess__c != oldAccessRuleMap.get(rule.id).AcademicEnrollmentEditAccess__c
                  ){
                      AccessRuleId.add(rule.id); 
                  }
            }
            else{
                AccessRuleId.add(rule.id); 
            }
        }
        
        if(AccessRuleId.size()>0)
            sharingService.AcedemicEnrollmentSharing(AccessRuleId);
    }
    
    private void deleteStudentShares(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Map<ID, AccessRule__c> AccessRuleMapWithID = SharingService.getAccessRuleMapWithID(oldAccessRuleMap.keySet());
        Set<ID> parentIds = new Set<ID>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            parentIds.add(rule.StudentProgram__r.Student__c);
        }
        if(parentIds.size()>0){
            sharingService.deleteShares('Account', parentIDs); 
        }
    }
    
    private void deleteCreditCheckShares(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Map<ID, AccessRule__c> AccessRuleMapWithID = SharingService.getAccessRuleMapWithID(oldAccessRuleMap.keySet());
        Set<ID> parentIds = new Set<ID>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            parentIds.add(rule.CreditCheck__c);
        }
        if(parentIds.size()>0){
            sharingService.deleteShares('CreditCheck__c', parentIDs); 
        }
    }
    
    private void deleteDisbursementShares(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Map<ID, AccessRule__c> AccessRuleMapWithID = SharingService.getAccessRuleMapWithID(oldAccessRuleMap.keySet());
        Set<ID> parentIds = new Set<ID>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            parentIds.add(rule.Disbursement__c);
        }
        if(parentIds.size()>0){
            sharingService.deleteShares('Transaction__c', parentIDs); 
        }
    }
    
    private void deleteAcademicEnrollmentShares(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Map<ID, AccessRule__c> AccessRuleMapWithID = SharingService.getAccessRuleMapWithID(oldAccessRuleMap.keySet());
        Set<ID> parentIds = new Set<ID>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            parentIds.add(rule.AcademicEnrollment__c);
        }
        if(parentIds.size()>0){
            sharingService.deleteShares('AcademicEnrollment__c', parentIDs); 
        }
    }
    
    private void deleteDataCollectionShares(Map<Id, AccessRule__c> oldAccessRuleMap, Map<Id, AccessRule__c> newAccessRuleMap){
        Map<ID, AccessRule__c> AccessRuleMapWithID = SharingService.getAccessRuleMapWithID(oldAccessRuleMap.keySet());
        Set<ID> parentIds = new Set<ID>();
        for(AccessRule__c rule: AccessRuleMapWithID.values()){
            parentIds.add(rule.StudentProgram__c);
        }
        
        Map<Id, DataCollection__c> dataCollectionMapByAgreementId = DataCollectionQueries.getDataCollectionMapWithAgreement(parentIds);
        if(dataCollectionMapByAgreementId  != null && dataCollectionMapByAgreementId.size()>0){
            sharingService.deleteShares('DataCollection__c', dataCollectionMapByAgreementId.keySet()); 
        }
    }
        
    private Map<String, Map<ID, AccessRule__c>> determineObjectType(Map<ID, AccessRule__c> AccessRuleMap){
        Map<String, Map<ID, AccessRule__c>> accessrulesByObjectType = new Map<String, Map<ID, AccessRule__c>>();
        String objectType;
        for(AccessRule__c rule: AccessRuleMap.values()){
            if(rule.StudentProgram__c != null){
                objectType = 'StudentProgram__c';
            }
            else if(rule.Disbursement__c != null){
                objectType = 'Transaction__c';
            }
            else if(rule.CreditCheck__c != null){
                objectType = 'CreditCheck__c';
            }
            else if(rule.AcademicEnrollment__c != null){
                objectType = 'AcademicEnrollment__c';
            }
            
            if(!accessrulesByObjectType.containsKey(objectType)){
                accessrulesByObjectType.put(objectType, new Map<ID, AccessRule__c>());
            }
            accessrulesByObjectType.get(objectType).put(rule.id, rule);
        }
        return accessrulesByObjectType;
    }

}