@isTest
public with sharing class TransactionBatchTriggerHandler_TEST {
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
        Integer numberOfTestRecords = TestUtil.TEST_THROTTLE;
        if(numberOfTestRecords > 2){
            numberOfTestRecords = 2;
        }
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(numberOfTestRecords);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        //Map<ID, Program__c> otherPrograms = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(numberOfTestRecords);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(2, agreements, TransactionService.disbursementRecType);
        }
    }

    /*@isTest public static void validateDML(){
        TestUtil.setStandardConfiguration();
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Program__c> otherPrograms = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(2, agreements, TransactionService.disbursementRecType);
        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        txBatchList.add(new TransactionBatch__c(Program__c = programs.values()[0].id,
                TransactionsFrom__c = Date.Today().addDays(-1),
                TransactionsThrough__c = Date.Today().addDays(1),
                TransactionBatchStatus__c = 'Created'));

        Test.startTest();
        insert(txBatchList);

        txBatchList[0].TransactionBatchStatus__c = 'Select Transactions';
        update txBatchList;

        txBatchList[0].UpdateTransactionDateTime__c = Datetime.now().addMinutes(1);
        txBatchList[0].UpdateTransactionStatus__c = 'Cancelled';
        txBatchList[0].UpdateConfirmed__c = 'Confirmed';
        update txBatchList;

        txBatchList[0].TransactionBatchStatus__c = 'Release Transactions';
        update txBatchList;

        delete txBatchList;
        undelete txBatchList;
        Test.stopTest();
    }*/

    @isTest static void validateInsert(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Created';
        txBatchList.add(trxnBatch);
        Test.startTest();
        //insert txBatchList;
        dbUtil.insertRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void validateDeleteAndUndelete(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Created';
        txBatchList.add(trxnBatch);
        Test.startTest();
        insert txBatchList;
        delete txBatchList;
        undelete txBatchList;
        Test.stopTest();
    }


    @isTest static void insertTrxnWithStatusSelectTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Select Transactions';
        txBatchList.add(trxnBatch);
        Test.startTest();
        //insert txBatchList;
        dbUtil.insertRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void insertTrxnWithStatusReleaseTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Release Transactions';
        txBatchList.add(trxnBatch);
        Test.startTest();
        //insert txBatchList;
        dbUtil.insertRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void insertTrxnWithStatusFreezeTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Freeze Transactions';
        txBatchList.add(trxnBatch);
        Test.startTest();
        //insert txBatchList;
        dbUtil.insertRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void updateTrxnStatusToSelectTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Created';
        txBatchList.add(trxnBatch);
        Test.startTest();
        TriggerSettings.getSettings().transactionBatchTrigger = false;
        insert txBatchList;
        TriggerSettings.getSettings().transactionBatchTrigger = true;
        //txBatchList[0].UpdateTransactionDateTime__c = Datetime.now().addMinutes(1);
        txBatchList[0].UpdateTransactionStatus__c = 'Complete';
        txBatchList[0].UpdateConfirmed__c = 'Confirmed';
        txBatchList[0].TransactionBatchStatus__c = 'Select Transactions';
        //update txBatchList;
        dbUtil.updateRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void updateTrxnStatusToReleaseTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Created';
        txBatchList.add(trxnBatch);
        Test.startTest();
        TriggerSettings.getSettings().transactionBatchTrigger = false;
        insert txBatchList;
        TriggerSettings.getSettings().transactionBatchTrigger = true;
        //txBatchList[0].UpdateTransactionDateTime__c = Datetime.now().addMinutes(1);
        txBatchList[0].UpdateTransactionStatus__c = 'Complete';
        txBatchList[0].UpdateConfirmed__c = 'Confirmed';
        txBatchList[0].TransactionBatchStatus__c = 'Release Transactions';
        //update txBatchList;
        dbUtil.updateRecords(txBatchList);
        Test.stopTest();
    }

    @isTest static void updateTrxnStatusToFreezeTrxns_Test(){
        TestUtil.setStandardConfiguration();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programs = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> agreements = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Transaction__c> txMap = TransactionQueries.getTransactionMapWithStudentID(students.keySet(), TransactionService.disbursementRecType);

        List<TransactionBatch__c> txBatchList = new List<TransactionBatch__c>();
        TransactionBatch__c trxnBatch = new TransactionBatch__c ();
        trxnBatch.Program__c = programs.values()[0].id;
        trxnBatch.TransactionsFrom__c = Date.Today().addDays(-1);
        trxnBatch.TransactionsThrough__c = Date.Today().addDays(1);
        trxnBatch.TransactionBatchStatus__c = 'Created';
        txBatchList.add(trxnBatch);
        Test.startTest();
        TriggerSettings.getSettings().transactionBatchTrigger = false;
        insert txBatchList;
        TriggerSettings.getSettings().transactionBatchTrigger = true;
        //txBatchList[0].UpdateTransactionDateTime__c = Datetime.now().addMinutes(1);
        txBatchList[0].UpdateTransactionDate__c = Date.today();
        txBatchList[0].UpdateTransactionStatus__c = 'Complete';
        txBatchList[0].UpdateConfirmed__c = 'Confirmed';
        txBatchList[0].TransactionBatchStatus__c = 'Freeze Transactions';
        //update txBatchList;
        dbUtil.updateRecords(txBatchList);
        Test.stopTest();
    }

}