@isTest
public class EmploymentSeekingAttemptService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testGetEmploymentSeekingAttemptWithStudentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap, 1);
        Test.startTest();
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> resultAttList = EmploymentSeekingAttemptService.getEmploymentSeekingAttemptMapWithDefermentId(defermentMap.keyset());
        System.assertEquals(testAttMap.keySet().size(), resultAttList.size());
        Test.stopTest();
    }
    
    static testMethod void testGetEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap, 1);
        Test.startTest();
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> resultDefList = EmploymentSeekingAttemptService.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(testAttMap.keyset());
        System.assertEquals(testAttMap.keySet().size(), resultDefList.size());
        Test.stopTest();
    }

    static testMethod void testCreateEmploymentSeekingAttempt(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> attList = new List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmploymentSeekingAttemptService.EmploymentSeekingAttempt att = new EmploymentSeekingAttemptService.EmploymentSeekingAttempt();
            att.DefermentId = defermentMap.values().get(i).Id;
            attList.add(att);
        }
        Test.startTest();
        Set<ID> attIDs = EmploymentSeekingAttemptService.createEmploymentSeekingAttempts(attList);
        System.assertEquals(attList.size(), EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(attIDs).size());
        Test.stopTest();
    }

    static testMethod void testUpdateEmploymentSeekingAttempt(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap,1);
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> attList = new List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmploymentSeekingAttemptService.EmploymentSeekingAttempt att = new EmploymentSeekingAttemptService.EmploymentSeekingAttempt();
            att.EmploymentSeekingAttemptID = testAttMap.values().get(i).Id;
            att.contactDate = date.today();
            attList.add(att);
        }
        Test.startTest();
        Set<ID> attIDs = EmploymentSeekingAttemptService.updateEmploymentSeekingAttempts(attList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, attList.size());
        for(EmploymentSeekingAttempt__c att : EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(attIDs).Values()){
            System.assertequals(att.contactDate__c, date.today());
        }
    }

   static testMethod void testDeleteEmploymentSeekingAttempt(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> defermentMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap); 
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(defermentMap,1);
        
        Test.startTest();
        Integer deleted = EmploymentSeekingAttemptService.deleteEmploymentSeekingAttempts(testAttMap.keySet());        
        Test.stopTest();
        System.assertEquals(testAttMap.keySet().size(), deleted);
    }
}