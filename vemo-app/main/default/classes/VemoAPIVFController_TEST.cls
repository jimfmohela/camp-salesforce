@isTest
public with sharing class VemoAPIVFController_TEST {
    @isTest
    public static void validateController() {
        VemoAPIVFController controller = new VemoAPIVFController();
        VemoAPIVFController.callVemoAPI('GET', '/vemo/v1/student', '', 'VEMO_AUTH=testStudent_46&studentID=0017A00000RMCVHQA5');


    }
    @isTest
    public static void validateNotesMethod() {
        VemoAPIVFController controller = new VemoAPIVFController();
        VemoAPIVFController.createNote('test',true,'0013600001s17GE','body');
        VemoAPIVFController.getAllNotes('0013600001s17GE');
    }
    @isTest
    public static void validateCreateAttachment() {
        VemoAPIVFController controller = new VemoAPIVFController();
        VemoAPIVFController.createAttachment('test','pdf','0013600001s17GE','body');
    }
    @isTest
    public static void validateLoadUser() {
        VemoAPIVFController controller = new VemoAPIVFController();
        VemoAPIVFController.loadUser('test');
    }
}