@isTest
public with sharing class ReconciliationTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void createAndInsertReqRecords(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'ReconciliationSummary'});
    }

    @isTest
    static void insertTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        TestDataFactory.createAndInsertReconciliation(TestUtil.TEST_THROTTLE);
    }

    @isTest
    static void updateTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Reconciliation__c> reconciliations = TestDataFactory.createAndInsertReconciliation(TestUtil.TEST_THROTTLE);
        reconciliations.values()[0].Student__c = students.values()[0].Id;
        //update reconciliations.values();
        dbUtil.updateRecords(reconciliations.values());
    }

    @isTest
    static void deleteAndUndeleteTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Reconciliation__c> reconciliations = TestDataFactory.createAndInsertReconciliation(TestUtil.TEST_THROTTLE);
        //delete reconciliations.values();
        //undelete reconciliations.values();
        dbUtil.deleteRecords(reconciliations.values());
        dbUtil.undeleteRecords(reconciliations.values());
    }

    @isTest
    static void ReconciliationSummaryOnInsertTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        students.values()[0].PersonEmail = 'test_vemo@test.com';
        //update students.values();
        dbUtil.updateRecords(students.values());
        
        List<Reconciliation__c> reconciliations = TestDataFactory.createReconciliations(1);
        reconciliations[0].Student__c = students.values()[0].Id;
        reconciliations[0].SendReconciliationSummary__c = true;
        //insert reconciliations;
        dbUtil.insertRecords(reconciliations);
    }

    @isTest
    static void ReconciliationSummaryOnUpdateTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
         
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        students.values()[0].PersonEmail = 'test_vemo@test.com';
        //update students.values();
        dbUtil.updateRecords(students.values());
        
        Map<Id, Reconciliation__c> reconciliationMap = TestDataFactory.createAndInsertReconciliation(1);
        reconciliationMap.values()[0].Student__c = students.values()[0].Id;
        reconciliationMap.values()[0].SendReconciliationSummary__c = true;
        //update reconciliationMap.values();
        dbUtil.updateRecords(reconciliationMap.values());
    }
}