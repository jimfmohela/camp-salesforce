@isTest
public class ReportAllAccountsWithIncomeAllProgTEST{

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, PaymentMethod__c> paymentMethods = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, PaymentInstruction__c> PIs = TestDataFactory.createAndInsertPaymentInstruction(1, students, paymentMethods);
        Map<ID, PaymentAllocation__c> allocations = TestDataFactory.createAndInsertPaymentAllocation(3,PIs,agreements);
            
        Map<ID, EmploymentHistory__c> employmentHistory = TestDataFactory.createAndInsertEmploymentHistory(20,students);
        Map<ID, IncomeVerification__c> incomes = TestDataFactory.createAndInsertIncomeVerification(1, employmentHistory);
        
        for(StudentProgram__c sp:agreements.values()){
            sp.status__c = 'Grace';  
            sp.certificationDate__c = Datetime.now().addDays(-60);  
            sp.BypassAutomation__c = true;
        }
        
        //update agreements.values();
        dbUtil.updateRecords(agreements.values());
        
        for(IncomeVerification__c iv:incomes.values()){
            iv.BeginDate__c =Date.today().addDays(-120);
            iv.status__c = 'Verified'; 
            iv.type__c = 'Reported';
        }
        //update incomes.values();
        dbUtil.updateRecords(incomes.values());
        }
    }
    
    @isTest public static void validateGetSchool(){
               
        Test.StartTest(); 

            ReportAllAccountsWithIncomeAllProgram.runReport();
        
        Test.StopTest();  
    }
}