@isTest
public class FinancialFitnessReportCls_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    @isTest public static void report4StudentProgram(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<Id, FFQuizResponse__c> quizResponseMap = TestDataFactory.createAndInsertQuizResponse(quizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
    
        ApexPages.currentPage().getParameters().put('spID',studentPrgMap.values()[0].id);
        
        FinancialFitnessReportCls reportCls = new FinancialFitnessReportCls();
        
    }
    @isTest public static void report4QuizAttempt(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, FFQuizAttempt__c> quizAttemptMap = TestDataFactory.createAndInsertQuizAttempt(studentPrgMap.values()[0].id, TestUtil.TEST_THROTTLE);
        Map<Id, FFQuizResponse__c> quizResponseMap = TestDataFactory.createAndInsertQuizResponse(quizAttemptMap.values()[0].id, TestUtil.TEST_THROTTLE);
    
        ApexPages.currentPage().getParameters().put('qaID',quizAttemptMap.values()[0].id);
        
        FinancialFitnessReportCls reportCls = new FinancialFitnessReportCls();
        
    }
}