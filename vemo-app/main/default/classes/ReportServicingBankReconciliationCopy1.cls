public with sharing class ReportServicingBankReconciliationCopy1{
    
    transient public List<reportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    private Integer currentMonth;
    private Integer currentYear;
    private Map<string,string> schoolIdSchoolNameMap;
    transient public Map<String,List<StudentProgram__c>> spMap {get;set;}
    transient public Map<Id,Decimal> piIdLateFeeMap {get;set;}
    public Map<Id,Decimal> piIdAmountAllocatedMap;
    transient public Date fromDate{get;set;}
    transient public Date toDate{get;set;}
    transient public String schoolName{get;set;}

    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportServicingBankReconciliationCopy1(){
        reportData = new List<reportDataWrapper>();    
        currentMonth = Date.today().month();
        currentYear = Date.today().year();
        schoolIdSchoolNameMap = new Map<string,string>();
        spMap = new Map<String,List<StudentProgram__c>>();
        piIdAmountAllocatedMap = new Map<Id,Decimal>();
        
        if(toDate == null)
            toDate = Date.newInstance(date.today().year(),date.today().month(),1);
        if(fromDate == null)
            fromDate = Date.newInstance(date.today().year(),date.today().month()-1,1);
            
        schoolName = '';
        
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        //Map of Payment Instruction
        List<PaymentInstruction__c> pIList = new List<PaymentInstruction__c>();
        
        //School & Payment Instruction Map
        Map<String,List<PaymentInstruction__c>> schPIMap = new Map<String,List<PaymentInstruction__c>>();
        
        pIList=getPaymentInstruction();
        
        getDataInMap(schPIMap, pIList);
        
        spMap=getAgreements(pIList);
        
         //populate the final reportData list 
        reportData = buildReportData(schPIMap);
    }
    
    /////////////////////////////////////////////////////////////////
    ///Description: get the payment instruction data
    /////////////////////////////////////////////////////////////////
    private List<PaymentInstruction__c> getPaymentInstruction(){
        Date dt = Date.newInstance(date.today().year(),date.today().month(),1);
        Date dt1 = Date.newInstance(date.today().year(),date.today().month()-1,1); 
        List<PaymentInstruction__c> newPIList = new List<PaymentInstruction__c>();
        if(schoolName == '' || schoolName == null){
            newPIList = [Select id,Amount__c,AmountAllocated__c,AmountPosted__c,RemainingAllocation__c,DatePosted__c,Student__c,Student__r.Name,
                         Status__c,Name,Student__r.PrimarySchool__pc,Student__r.PrimarySchool__pr.Name,
                         (Select AmountAllocated__c,AllocationType__c from Payment_Allocations__r)
                         From PaymentInstruction__c 
                         WHERE DatePosted__c <: toDate AND DatePosted__c >=: fromDate AND (Status__c='Cleared' OR Status__c='Complete')
                         Order by Student__r.PrimarySchool__pr.Name, Name];
        }
        else{
            String var= '%'+schoolName+'%';
            newPIList = [Select id,Amount__c,AmountAllocated__c,AmountPosted__c,RemainingAllocation__c,DatePosted__c,Student__c,Student__r.Name,
                         Status__c,Name,Student__r.PrimarySchool__pc,Student__r.PrimarySchool__pr.Name,
                         (Select AmountAllocated__c,AllocationType__c from Payment_Allocations__r)
                         From PaymentInstruction__c 
                         WHERE DatePosted__c <: toDate 
                         AND DatePosted__c >=: fromDate 
                         AND (Status__c='Cleared' OR Status__c='Complete')
                         AND Student__r.PrimarySchool__pr.Name like :var
                         Order by Name];
        }
        
        return newPIList;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ///Description: get the Agreements for student
    /////////////////////////////////////////////////////////////////////////
    private Map<String,List<StudentProgram__c>> getAgreements(List<PaymentInstruction__c> pIList){
        List<Id> studIds = new List<Id>();
        for(PaymentInstruction__c pi : pIList){
            studIds.add(pi.Student__c);
        }
        List<StudentProgram__c> stdPrgList = new List<StudentProgram__c>();
        stdPrgList = [Select id,Student__c,Student__r.Name,PrimaryOwner__c,PrimaryOwner__r.Name,VemoContractNumber__c,PaymentStreamOwner__c,PaymentStreamOwner__r.Name,
                      FundOwner__c,FundOwner__r.Name,Status__c,Name,ProgramName__c,SchoolName__c
                      FROM StudentProgram__c
                      WHERE Student__c IN : studIds AND status__c != 'Cancelled'];                      
        //system.debug(stdPrgList);
        Map<String,List<StudentProgram__c>> studIdSPListMap = new Map<String,List<StudentProgram__c>>();
        
        for(StudentProgram__c sp: stdPrgList){
            if(!studIdSPListMap.containsKey(sp.Student__c))
                studIdSPListMap.put(sp.Student__c,new List<StudentProgram__c>());
            
            studIdSPListMap.get(sp.Student__c).add(sp);
        }
        return studIdSPListMap;
    }
    
    ////////////////////////////////////////////////////////////////////////
    ///Description: get the payment instruction data along with school in map
    /////////////////////////////////////////////////////////////////////////
    private Map<String,List<PaymentInstruction__c>> getDataInMap(Map<String,List<PaymentInstruction__c>> schPIMap,List<PaymentInstruction__c> pIList){
        piIdLateFeeMap = new Map<Id,Decimal>();
        piIdAmountAllocatedMap = new Map<Id,Decimal>();
        for(PaymentInstruction__c pi: pIList){
            if(!schPIMap.containsKey(pi.Student__r.PrimarySchool__pc))
                schPIMap.put(pi.Student__r.PrimarySchool__pc,new List<PaymentInstruction__c>());
                
            schPIMap.get(pi.Student__r.PrimarySchool__pc).add(pi);
            schoolIdSchoolNameMap.put(pi.Student__r.PrimarySchool__pc,pi.Student__r.PrimarySchool__pr.Name);
            
            for(PaymentAllocation__c pA: pi.Payment_Allocations__r){
                if(!piIdLateFeeMap.containsKey(pi.Id))
                    piIdLateFeeMap.put(pi.Id,0.00);
                if(pa.AllocationType__c == 'Fee'){                      
                    piIdLateFeeMap.put(pi.Id,piIdLateFeeMap.get(pi.Id)+pA.AmountAllocated__c);
                }
                
                if(!piIdAmountAllocatedMap.containsKey(pi.Id))
                    piIdAmountAllocatedMap.put(pi.Id,0.00);
                
                piIdAmountAllocatedMap.put(pi.Id,piIdAmountAllocatedMap.get(pi.Id)+pA.AmountAllocated__c);
                                
            }
            if(pi.Payment_Allocations__r.size()==0){
                piIdLateFeeMap.put(pi.Id,0.00);
                piIdAmountAllocatedMap.put(pi.Id,0.00);
            }
        }
        //system.debug(schPIMap);
        return schPIMap;
    }
    
     public List<ReportDataWrapper> buildReportData(Map<String,List<PaymentInstruction__c>> schPIMap){
         List<ReportDataWrapper> output = new List<ReportDataWrapper>();
         
         for(string s: schPIMap.keySet()){
             ReportDataWrapper rdw = new ReportDataWrapper();             
             rdw.school = s;
             rdw.schoolName = schoolIdSchoolNameMap.get(s);
             rdw.paymentInstructions = schPIMap.get(s);
             for(PaymentInstruction__c pi:schPIMap.get(s)){
                 rdw.totalAmount += piIdAmountAllocatedMap.get(pi.id);
                 rdw.totalAmountAllocated += pi.AmountAllocated__c;
                 rdw.totalAmountPosted += (pi.AmountPosted__c-piIdLateFeeMap.get(pi.id));
                 rdw.totalRemainingAllocated += pi.RemainingAllocation__c;
             }
             output.add(rdw);
         }
         //system.debug(output);
         return output;
     }
     
     ////////////////////////////////////////
     ///Call the export VF Page
     ////////////////////////////////////////
     public PageReference exportToExcel(){
        PageReference pg = new PageReference('/apex/ReportServicingBankReconciliationXLS1');
        pg.setRedirect(false);
        return pg;
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        public String school {get;set;}
        public String schoolName {get;set;}
        public decimal totalAmount {get;set;}
        public decimal totalAmountAllocated {get;set;}
        public decimal totalRemainingAllocated {get;set;}
        public decimal totalAmountPosted {get;set;}
        public List<PaymentInstruction__c> paymentInstructions {get;set;}        
        
        public ReportDataWrapper(){
            this.paymentInstructions = new List<PaymentInstruction__c>();
            this.totalAmount = 0;
            this.totalAmountAllocated = 0;
            this.totalRemainingAllocated = 0;
            this.totalAmountPosted = 0;            
        }    
    }
        
}