@isTest
public class EmployerService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEmployerWithEmployerID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        
        Test.startTest();
        List<EmployerService.Employer> resultEmpHisList = EmployerService.getEmployerWithEmployerID(employerMap.keySet());
        System.assertEquals(employerMap.keySet().size(), resultEmpHisList.size());
        Test.stopTest();
    }

    static testMethod void testGetEmploymentHistoryWithStudentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Test.startTest();
        List<EmployerService.Employer> resultEmpHisList = EmployerService.getEmployerWithStudentID(testStudentAccountMap.keySet());//, 'false', 'false', '', '', 'false', 'false', 'false', 'false');
        System.assertEquals(employerMap.keySet().size(), resultEmpHisList.size());
        Test.stopTest();
    }
    
    static testMethod void testCreateEmploymentHistory(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        //Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        List<EmployerService.Employer> empHisList = new List<EmployerService.Employer>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmployerService.Employer empHis = new EmployerService.Employer();
            empHis.studentID = testStudentAccountMap.values().get(i).Id;
            empHisList.add(empHis);
        }
        Test.startTest();
        Set<ID> empHisIDs = EmployerService.createEmployer(empHisList);
        System.assertEquals(empHisList.size(), EmployerQueries.getEmployerMapWithEmployerID(empHisIDs).size());
        Test.stopTest();
    }

    static testMethod void testUpdateEmploymentHistory(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        List<EmployerService.Employer> empHisList = new List<EmployerService.Employer>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            EmployerService.Employer empHis = new EmployerService.Employer();
            empHis.employerID = employerMap.values().get(i).Id;
            //empHis.verified = false;
            empHisList.add(empHis);
        }
        Test.startTest();
        Set<ID> empHisIDs = EmployerService.updateEmployer(empHisList);        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE, empHisList.size());
        //for(EmploymentHistory__c empHis : EmploymentHistoryQueries.getEmploymentHistoryMap().values()){
        //    System.assert(!empHis.Verified__c);
        //}
    }

    static testMethod void testDeleteEmployer(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        
        Test.startTest();
        Integer deleted = EmployerService.deleteEmployer(employerMap.keySet());        
        Test.stopTest();
        System.assertEquals(employerMap.keySet().size(), deleted);
        //System.assertEquals(0, EmployerQueries.getEmploymentHistoryMap().size());
    }
    
    

}