/////////////////////////////////////////////////////////////////////////
// Class: OpportunityQueries
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-07-13   Greg Cook       Created                                 
// 
/////////////////////////////////////////////////////////////////////////
public class OpportunityQueries {
    public static Map<ID, Opportunity> getOpportunityMapByID(){
        Map<ID, Opportunity> oppMap = new Map<ID, Opportunity>();
        String query = 'SELECT ' + getFieldNames() + ' FROM Opportunity LIMIT 50000';   
        DatabaseUtil db = new DatabaseUtil();
        oppMap = new Map<ID, Opportunity>((List<Opportunity>)db.query(query));
        return oppMap;
    }
    
    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'StageName, ';
        fieldNames += 'CloseDate ';
        return fieldNames;
    }
}