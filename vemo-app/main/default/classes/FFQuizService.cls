public with sharing class FFQuizService {
    
    public static List<FFQuizAttempt> getFFQuizAttemptWithFFQuizAttemptID(Set<ID> FFQuizAttemptIDs){
        Map<Id, FFQuizAttempt__c> FFQuizAttemptMap = FFQuizQueries.getFFQuizAttemptMapWithFFQuizAttemptID(FFQuizAttemptIDs);
        List<FFQuizAttempt> FFQuizAttemptList = new List<FFQuizAttempt>();
        for(FFQuizAttempt__c ffquiz : FFQuizAttemptMap.values()){
            FFQuizAttemptList.add(new FFQuizAttempt(ffquiz));
        }
        return FFQuizAttemptList;
    } 

    public static List<FFQuizAttempt> getFFQuizAttemptWithStudentProgramID(Set<ID> StudentProgramIDs){
        Map<Id, FFQuizAttempt__c> FFQuizAttemptMap = FFQuizQueries.getFFQuizAttemptMapWithStudentProgramID(StudentProgramIDs);
        List<FFQuizAttempt> FFQuizAttemptList = new List<FFQuizAttempt>();
        for(FFQuizAttempt__c ffquiz : FFQuizAttemptMap.values()){
            FFQuizAttemptList.add(new FFQuizAttempt(ffquiz));
        }
        return FFQuizAttemptList;
    }
    
    /*public static List<FFQuizResponse> getFFQuizResponseWithFFQuizResponseID(Set<ID> FFQuizResponseIDs){
        Map<Id, FFQuizResponse__c> FFQuizResponseMap = new Map<Id, FFQuizResponse__c>();
        String query;
        query = 'SELECT ' + getFFQuizResponseFieldNames() + ' FROM FFQuizResponse__c where id IN' + DatabaseUtil.inSetStringBuilder(FFQuizResponseIDs);
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        FFQuizResponseMap = new Map<ID, FFQuizResponse__c>((List<FFQuizResponse__c>)db.query(query));
        List<FFQuizResponse> FFQuizResponseList = new List<FFQuizResponse>();
        for(FFQuizResponse__c ffqr : FFQuizResponseMap.values()){
            FFQuizResponseList.add(new FFQuizResponse(ffqr));
        }
        return FFQuizResponseList;
    }*/ //Not Needed
    
    public static List<FFQuizResponse> getFFQuizResponseWithFFQuizAttemptID(Set<ID> FFQuizAttemptIDs){
       Map<Id, FFQuizResponse__c> FFQuizResponseMap = FFQuizQueries.getFFQuizResponseMapWithFFQuizAttemptID(FFQuizAttemptIDs);
       List<FFQuizResponse> FFQuizResponseList = new List<FFQuizResponse>();
       for(FFQuizResponse__c ffResponse : FFQuizResponseMap.values()){
           FFQuizResponseList.add(new FFQuizResponse(ffResponse));
       }
       return FFQuizResponseList;
    }

    public static Set<ID> createFFQuizAttempt(List<FFQuizAttempt> ffqaList){
        
        List<FFQuizAttempt__c> newffqaList = new List<FFQuizAttempt__c>();
        
        for(FFQuizAttempt ffqa : ffqaList){
            FFQuizAttempt__c  newffqa = fFQuizAttemptToFFQuizAttemptObj(ffqa);           
            newffqaList.add(newffqa);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newffqaList);
        //insert newffqaList;
        
        Set<ID> ffqaIDs = new Set<ID>();
        for(FFQuizAttempt__c  qa : newffqaList){
            ffqaIDs.add(qa.ID);
        }
        return ffqaIDs;
    }
    
    public static Set<ID> createFFQuizAttempt(Map<Integer, List<FFQuizResponse>> attemptOrder_QuizResponseMap, Set<ID> quizAttempt){
        System.debug('FFQuizService.createFFQuizAttempt()');
        
        List<FFQuizAttempt__c> qaList = [Select id,AttemptOrder__c from FFQuizAttempt__c where ID IN: quizAttempt];
        
        Map<Integer, ID> attemptOrder_QuizAttemptIDMap = new Map<Integer, ID>();
        
        for(FFQuizAttempt__c qa: qaList){
            attemptOrder_QuizAttemptIDMap.put(Integer.valueof(qa.AttemptOrder__c), qa.ID);
        }
        
        List<FFQuizResponse__c> newffqrList = new List<FFQuizResponse__c>();
        
        for(Integer i : attemptOrder_QuizResponseMap.keyset()){
            for(FFQuizResponse ffqr : attemptOrder_QuizResponseMap.get(i)){
                FFQuizResponse__c  newffqr = fFQuizResponseToFFQuizResponseObj(ffqr);
                newffqr.FFQuizAttempt__c = attemptOrder_QuizAttemptIDMap.get(i);         
                newffqrList.add(newffqr);
            }
        }
        
        //insert newffqrList;
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newffqrList);
        
        Set<ID> ffqrIDs = new Set<ID>();
        for(FFQuizResponse__c  qr : newffqrList){
            ffqrIDs.add(qr.ID);
        }
        return ffqrIDs;
    }
    
    public static Set<ID> updateFFQuizAttempt(List<FFQuizAttempt> ffqaList){
        List<FFQuizAttempt__c> newffqaList = new List<FFQuizAttempt__c>();
        
        for(FFQuizAttempt ffqa : ffqaList){
            FFQuizAttempt__c  newffqa = fFQuizAttemptToFFQuizAttemptObj(ffqa);           
            newffqaList.add(newffqa);
        }
        //update newffqaList;
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newffqaList);
        
        Set<ID> ffqaIDs = new Set<ID>();
        for(FFQuizAttempt__c  qa : newffqaList){
            ffqaIDs.add(qa.ID);
        }
        return ffqaIDs;
    }
    
    public static Set<ID> updateFFQuizResponse(List<FFQuizResponse> ffqrList){
        system.debug(ffqrList);
        List<FFQuizResponse__c> newffqrList = new List<FFQuizResponse__c>();
        
        for(FFQuizResponse ffqr : ffqrList){
            FFQuizResponse__c  newffqr = fFQuizResponseToFFQuizResponseObj(ffqr);           
            newffqrList.add(newffqr);
        }
        //upsert newffqrList;
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.upsertRecords(newffqrList);
        
        Set<ID> ffqaIDs = new Set<ID>();
        for(FFQuizResponse__c  qa : newffqrList){
            ffqaIDs.add(qa.ID);
        }
        return ffqaIDs;
    }


    public static Integer deleteFFQuizAttempt(Set<ID> FFQuizAttemptIDs){
        Map<Id, FFQuizAttempt__c> fFQuizAttemptMap = FFQuizQueries.getFFQuizAttemptMapWithFFQuizAttemptID(FFQuizAttemptIDs);
        Integer numToDelete = fFQuizAttemptMap.size();
        //delete fFQuizAttemptMap.values();
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.deleteRecords(fFQuizAttemptMap.values());
        return numToDelete;
        
    }

    
   /* private static String getFFQuizAttemptFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'AttemptOrder__c, ';
        fieldNames += 'ClientID__c, ';
        fieldNames += 'Contract__c, ';
        fieldNames += 'Result__c, ';
        fieldNames += 'ResultLink__c, ';
        fieldNames += 'StartTime__c, ';
        fieldNames += 'SubmitTime__c ';
        
        return fieldNames;
    }
    
    private static String getFFQuizResponseFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'AnswerID__c, ';
        fieldNames += 'AnswerText__c, ';
        fieldNames += 'CorrectAnswer__c, ';
        fieldNames += 'QuestionID__c, ';
        fieldNames += 'QuestionText__c, ';
        fieldNames += 'FFQuizAttempt__c ';        
        
        return fieldNames;
    }
    
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }*/
    
    
    public static FFQuizAttempt__c  fFQuizAttemptToFFQuizAttemptObj(FFQuizAttempt ffqa){
        FFQuizAttempt__c  newffqa = new FFQuizAttempt__c();
        if(ffqa.fFQuizAttemptID != null) newffqa.ID = ffqa.fFQuizAttemptID;
        if(ffqa.attemptOrder  != null) newffqa.AttemptOrder__c = ffqa.attemptOrder;
        if(ffqa.clientID  != null) newffqa.ClientID__c = ffqa.clientID;
        if(ffqa.contractID != null) newffqa.Contract__c = ffqa.contractID;
        if(ffqa.result != null) newffqa.result__c = ffqa.result;
        if(ffqa.resultLink  != null) newffqa.ResultLink__c = ffqa.resultLink;
        if(ffqa.startTime  != null) newffqa.StartTime__c = ffqa.startTime;
        if(ffqa.submitTime != null) newffqa.SubmitTime__c = ffqa.submitTime;
        
        return newffqa;
    }
    
    public static FFQuizResponse__c  fFQuizresponseToFFQuizResponseObj(FFQuizResponse ffqr){
        FFQuizResponse__c  newffqr = new FFQuizresponse__c();
        if(ffqr.fFQuizResponseID != null) newffqr.ID = ffqr.fFQuizResponseID;
        if(ffqr.answerID  != null) newffqr.answerID__c = ffqr.answerID;
        if(ffqr.answerText  != null) newffqr.answerText__c = ffqr.answerText;
        if(ffqr.correctAnswer != null) newffqr.correctAnswer__c = ffqr.correctAnswer;
        if(ffqr.questionID != null) newffqr.questionID__c = ffqr.questionID;
        if(ffqr.questionText  != null) newffqr.questionText__c = ffqr.questionText;
        if(ffqr.quizAttemptID != null) newffqr.FFQuizAttempt__c= ffqr.quizAttemptID;

        return newffqr;
    }
    
     public class FFQuizAttempt{
        public String fFQuizAttemptID {get;set;}
        public Integer attemptOrder {get;set;}
        public String clientID {get;set;}
        public String contractID {get;set;}
        public String result {get;set;}
        public String resultLink {get;set;}
        public String startTime {get;set;}
        public String submitTime {get;set;}
        
        public FFQuizAttempt(){}
    
        public FFQuizAttempt(FFQuizAttempt__c ffqa){
          this.fFQuizAttemptID = ffqa.ID;
          this.attemptOrder = Integer.valueof(ffqa.AttemptOrder__c);
          this.clientID = ffqa.ClientID__c;
          this.contractID = ffqa.Contract__c;
          this.result = ffqa.Result__c;
          this.resultLink = ffqa.ResultLink__c;  
          this.startTime = ffqa.StartTime__c ;
          this.submitTime = ffqa.SubmitTime__c;
          
        }
    }
    
    public class FFQuizResponse{
        public String fFQuizResponseID {get;set;}
        public String answerID {get;set;}
        public String answerText {get;set;}
        public boolean correctAnswer {get;set;}
        public String questionID {get;set;}
        public String questionText {get;set;}
        public String quizAttemptID {get;set;}
        
        public FFQuizResponse(){}
        
        public FFQuizResponse(FFQuizResponse__c ffqr){
          this.fFQuizResponseID = ffqr.ID;
          this.answerID = ffqr.AnswerID__c;
          this.answerText = ffqr.AnswerText__c;
          this.correctAnswer = ffqr.CorrectAnswer__c;
          this.questionID = ffqr.QuestionID__c;
          this.questionText = ffqr.QuestionText__c;
          this.quizAttemptID = ffqr.FFQuizAttempt__c;
        }
    }
}