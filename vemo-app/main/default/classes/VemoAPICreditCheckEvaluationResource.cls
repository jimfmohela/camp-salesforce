public class VemoAPICreditCheckEvaluationResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){      
        System.debug('VemoAPICreditCheckEvaluationResource.handleGetV1()');   
        String checkEvalIDparam = api.params.get('creditCheckEvaluationID');
        String creditCheckIDparam = api.params.get('creditCheckID');
        String agreementIDparam = api.params.get('agreementID');
        List<CreditCheckEvaluationService.CreditCheckEvaluation> ccs = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        if(checkEvalIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckEvalID(VemoApi.parseParameterIntoIDSet(checkEvalIDparam));
        }
        //else if(studentIDparam != null){
          //  ccs = CreditCheckService.getCreditCheckWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDparam));
        //}
        else if(creditCheckIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckID(VemoApi.parseParameterIntoIDSet(creditCheckIDparam));
        }
        else if(agreementIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithAgreementID(VemoApi.parseParameterIntoIDSet(agreementIDparam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter creditCheckEvaluationID, or creditCheckID missing');
        }
        List<CreditCheckEvaluationResourceOutputV1> results = new List<CreditCheckEvaluationResourceOutputV1>();
        
        Set<ID> AgreementIDsToVerify = new Set<ID>();
        
        for(CreditCheckEvaluationService.CreditCheckEvaluation cc : ccs){
           AgreementIDsToVerify.add((ID)cc.agreementID);
        }
        
        Map<Id, StudentProgram__c> VerifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(AgreementIDsToVerify);
        for(CreditCheckEvaluationService.CreditCheckEvaluation cc : ccs){
            if(VerifiedAgreementMap.containsKey((ID)cc.agreementID)){
                results.add(new CreditCheckEvaluationResourceOutputV1(cc));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){      
        System.debug('VemoAPICreditCheckEvaluationResource.handleGetV1()');   
        String checkEvalIDparam = api.params.get('creditCheckEvaluationID');
        String creditCheckIDparam = api.params.get('creditCheckID');
        String agreementIDparam = api.params.get('agreementID');
        List<CreditCheckEvaluationService.CreditCheckEvaluation> ccs = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        if(checkEvalIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckEvalID(VemoApi.parseParameterIntoIDSet(checkEvalIDparam));
        }
        //else if(studentIDparam != null){
          //  ccs = CreditCheckService.getCreditCheckWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDparam));
        //}
        else if(creditCheckIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckID(VemoApi.parseParameterIntoIDSet(creditCheckIDparam));
        }
        else if(agreementIDparam != null){
            ccs = CreditCheckEvaluationService.getCreditCheckEvaluationWithAgreementID(VemoApi.parseParameterIntoIDSet(agreementIDparam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter creditCheckEvaluationID, or creditCheckID missing');
        }
        List<CreditCheckEvaluationResourceOutputV2> results = new List<CreditCheckEvaluationResourceOutputV2>();
        
        Set<ID> AgreementIDsToVerify = new Set<ID>();
        
        for(CreditCheckEvaluationService.CreditCheckEvaluation cc : ccs){
           AgreementIDsToVerify.add((ID)cc.agreementID);
        }
        
        Map<Id, StudentProgram__c> VerifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(AgreementIDsToVerify);
        
        for(CreditCheckEvaluationService.CreditCheckEvaluation cc : ccs){
            if(VerifiedAgreementMap.containsKey((ID)cc.agreementID)){
                if(GlobalSettings.getSettings().vemoDomainAPI){
                    results.add(new VemoCreditCheckEvaluationResourceOutputV2(cc));
                }else{
                    results.add(new PublicCreditCheckEvaluationResourceOutputV2(cc));
                }
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){ 
        System.debug('VemoAPICreditCheckEvaluationResource.handlePostV1()');  
        List<CreditCheckEvaluationService.CreditCheckEvaluation> ccs = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        List<CreditCheckEvaluationResourceInputV1> checksJSON = (List<CreditCheckEvaluationResourceInputV1>)JSON.deserialize(api.body, List<CreditCheckEvaluationResourceInputV1>.class);
        
        Set<ID> AgreementIDsToVerify = new Set<ID>();
        
        for(CreditCheckEvaluationResourceInputV1 checkJSON : checksJSON){
           AgreementIDsToVerify.add((ID)checkJSON.agreementID);
        }
        
        Map<Id, StudentProgram__c> VerifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(AgreementIDsToVerify);
        
        for(CreditCheckEvaluationResourceInputV1 checkJSON : checksJSON){
            if(VerifiedAgreementMap.containsKey((ID)checkJSON.agreementID)){
                checkJSON.validatePOSTFields();
                ccs.add(creditCheckEvaluationResourceToCreditCheckEvaluation(checkJSON));
            }
        }       
        Set<Id> checkIDs = CreditCheckEvaluationService.createCreditCheckEvaluations(ccs);
        return (new VemoAPI.ResultResponse(checkIDs, checkIDs.size()));
    }

    
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPICreditCheckEvaluationResource.handlePutV1()');   
        List<CreditCheckEvaluationService.CreditCheckEvaluation> ccs = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        List<CreditCheckEvaluationResourceInputV1> checksJSON = (List<CreditCheckEvaluationResourceInputV1>)JSON.deserialize(api.body, List<CreditCheckEvaluationResourceInputV1>.class);
        
        Set<ID> IDsToVerify = new Set<ID>();
        
        for(CreditCheckEvaluationResourceInputV1 checkJSON : checksJSON){
           IDsToVerify.add((ID)checkJSON.creditCheckEvaluationID);
        }
        Map<Id, CreditCheckEvaluation__c> VerifiedCreditCheckEvaluationMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(IDsToVerify);
        for(CreditCheckEvaluationResourceInputV1 checkJSON : checksJSON){
            if(VerifiedCreditCheckEvaluationMap.containsKey((ID)checkJSON.creditCheckEvaluationID)){
                checkJSON.validatePUTFields();
                ccs.add(creditCheckEvaluationResourceToCreditCheckEvaluation(checkJSON));
            }
        }       
        Set<Id> checkIDs = CreditCheckEvaluationService.updateCreditCheckEvaluations(ccs);
        return (new VemoAPI.ResultResponse(checkIDs, checkIDs.size()));
    }
    
    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPICreditCheckEvaluationResource.handleDeleteV1()');
        String checkEvalIDparam = api.params.get('creditCheckEvaluationID');
        
        Map<ID, CreditCheckEvaluation__c> cceMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(VemoApi.parseParameterIntoIDSet(checkEvalIDparam));
        Set<ID> AgreementIDsToVerify = new Set<ID>();
        for(CreditCheckEvaluation__c cce: cceMap.values()){
           AgreementIDsToVerify.add(cce.Agreement__c);
        }
        
        Map<Id, StudentProgram__c> VerifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(AgreementIDsToVerify);
        Set<ID> CreditCheckEvaluationsToBeDeleted = new Set<ID>();
        for(CreditCheckEvaluation__c cce: cceMap.values()){
            if(VerifiedAgreementMap.containsKey(cce.Agreement__c)){
                CreditCheckEvaluationsToBeDeleted.add(cce.id);
            }
        }
        
        Integer numToDelete = 0;
        if(CreditCheckEvaluationsToBeDeleted.size()>0){
            numToDelete = CreditCheckEvaluationService.deleteCreditCheckEvaluations(CreditCheckEvaluationsToBeDeleted);
        } 
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    

    public static CreditCheckEvaluationService.CreditCheckEvaluation creditCheckEvaluationResourceToCreditCheckEvaluation(CreditCheckEvaluationResourceInputV1 check){
        CreditCheckEvaluationService.CreditCheckEvaluation cc = new CreditCheckEvaluationService.CreditCheckEvaluation();
        cc.creditCheckEvaluationID = check.creditCheckEvaluationID;  
        cc.creditCheckID = check.creditCheckID;
        cc.agreementID = check.agreementID;
        cc.status = check.status;
        cc.creditCheckDeniedReason = check.creditCheckDeniedReason;
        cc.creditCheckDeniedReasonText = check.creditCheckDeniedReasonText;
        cc.creditCheckProcess = check.creditCheckProcess ;
        return cc;
    }

    public class CreditCheckEvaluationResourceInputV1{
        public String creditCheckEvaluationID {get;set;}
        public String creditCheckID {get;set;}
        public String agreementID {get;set;}
        public String status {get;set;}
        public String creditCheckDeniedReasonText {get;set;}
        public String creditCheckDeniedReason {get;set;}
        public String creditCheckProcess {get;set;}


        public CreditCheckEvaluationResourceInputV1(){}

        public void validatePOSTFields(){
            if(creditCheckEvaluationID != null) throw new VemoAPI.VemoAPIFaultException('creditCheckEvaluationID cannot be created in POST');
            if(creditCheckID == null) throw new VemoAPI.VemoAPIFaultException('creditCheckID is a required input parameter on POST');
        }
        public void validatePUTFields(){
            if(creditCheckEvaluationID == null) throw new VemoAPI.VemoAPIFaultException('creditCheckEvaluationID is a required input parameter on PUT');
        }
    }


    public class CreditCheckEvaluationResourceOutputV1{

        public CreditCheckEvaluationResourceOutputV1(){}
        
        public CreditCheckEvaluationResourceOutputV1(CreditCheckEvaluationService.CreditCheckEvaluation check){
            creditCheckEvaluationID = check.creditCheckEvaluationID;
            creditCheckID = check.creditCheckID;
            agreementID = check.agreementID;
            status = check.status;
            creditCheckDeniedReason = check.creditCheckDeniedReason;
            creditCheckDeniedReasonText = check.creditCheckDeniedReasonText;
            creditCheckProcess = check.creditCheckProcess;
            createdDate = check.createdDate; 
        }
        public String creditCheckEvaluationID {get;set;}
        public String creditCheckID {get;set;}
        public String agreementID {get;set;}
        public String status {get;set;}
        public String creditCheckDeniedReasonText {get;set;}
        public String creditCheckDeniedReason {get;set;}
        public String creditCheckProcess {get;set;}
        public DateTime createdDate {get;set;}

    }
    
    public virtual class CreditCheckEvaluationResourceOutputV2{
        public String creditCheckEvaluationID {get;set;}
        
        public CreditCheckEvaluationResourceOutputV2(){}
        
        public CreditCheckEvaluationResourceOutputV2(CreditCheckEvaluationService.CreditCheckEvaluation check){
            this.creditCheckEvaluationID = check.creditCheckEvaluationID;
        }
    }
    
    public class PublicCreditCheckEvaluationResourceOutputV2 extends CreditCheckEvaluationResourceOutputV2{
        public PublicCreditCheckEvaluationResourceOutputV2(){

        }
        public PublicCreditCheckEvaluationResourceOutputV2(CreditCheckEvaluationService.CreditCheckEvaluation check){
            super(check);
        }
    }
    
    public class VemoCreditCheckEvaluationResourceOutputV2 extends CreditCheckEvaluationResourceOutputV2{

        public String privateInfo{get;set;}
        public VemoCreditCheckEvaluationResourceOutputV2(){

        }        
        public VemoCreditCheckEvaluationResourceOutputV2(CreditCheckEvaluationService.CreditCheckEvaluation check){
            super(check);
            this.privateInfo = 'Test';
        }
    } 


}