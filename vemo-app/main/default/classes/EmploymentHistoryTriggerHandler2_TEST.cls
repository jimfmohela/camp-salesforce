@isTest
public class EmploymentHistoryTriggerHandler2_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    static testMethod void DMLTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        System.assertEquals(TestUtil.TEST_THROTTLE, testEmpHisMap.size());
        testEmpHisMap.values()[0].DocUploaded__c = true;
        testEmpHisMap.values()[0].Verified__c = true;
        testEmpHisMap.values()[0].DateVerified__c = Date.today();
        update testEmpHisMap.values();
        delete testEmpHisMap.values();
        Test.stopTest();
    }
    
    /*static testMethod void notEmployedHereAnymoreTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Set<String> eventTypes = new Set<String>{
                'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION', 'UPDATE_HOURLY_COMPENSATION',
                'BONUS_COMPENSATION', 'COMMISSION_COMPENSATION', 'TIPS_COMPENSATION'
        };
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), testEmpHisMap.size());
        Map<Id, EmploymentHistory2__c> notEMployedHereAnymoreEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, new Set<String>{'NOT_EMPLOYED_HERE_ANYMORE'}
        );
        System.assertEquals(TestUtil.TEST_THROTTLE, notEMployedHereAnymoreEmpHisMap.size());
        Test.stopTest();
    }*/
    
    /*static testMethod void autoCloseEmpHisTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Set<String> eventTypes = new Set<String>{
                'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION', 'UPDATE_HOURLY_COMPENSATION',
                'BONUS_COMPENSATION', 'COMMISSION_COMPENSATION', 'TIPS_COMPENSATION'
        };
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), testEmpHisMap.size());
        Map<Id, EmploymentHistory2__c> updateEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), updateEmpHisMap.size());
        Test.stopTest();
    }*/
}