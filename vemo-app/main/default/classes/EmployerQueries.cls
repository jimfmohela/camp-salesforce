public class EmployerQueries {

    public static Map<ID, Employer__c> getEmployerMapWithStudentID(set<Id> studentIDs){
        Map<ID, Employer__c> employerMap = new Map<ID, Employer__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        employerMap = new Map<ID, Employer__c>((List<Employer__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return employerMap;      
    }
    
    public static Map<ID, Employer__c> getEmployerMapWithEmployerID(set<Id> employerIDs){
        Map<ID, Employer__c> employerMap = new Map<ID, Employer__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(employerIDs);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        employerMap = new Map<ID, Employer__c>((List<Employer__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return employerMap;      
    }
    
    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'EmploymentEndDate__c, ';
        fieldNames += 'EmploymentStartDate__c, ';
        fieldNames += 'JobTitle__c, ';
        fieldNames += 'MonthlyIncome__c, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'TransactionCount__c, ';
        fieldNames += 'Deleted__c ';
        return fieldNames;
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM Employer__c';
        return soql;
    }

    
    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        
        return lim;
    }
}