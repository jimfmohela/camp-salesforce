public class GenericDocumentViewApexCTRL {
    public static String objectType;

    @AuraEnabled
    public static List<GenericDocument__c> getGenericDocumentList(string recordId){
        System.debug('GenericDocumentViewApexCTRL.getGenericDocuments()');
        System.debug('recordId:'+recordId);
        String caseSensitiveId = recordId.substring(0, 15);
        System.debug('caseSensitiveId:'+caseSensitiveId);
        String query = '';
        
        if(Id.valueOf(recordId).getSObjectType().getDescribe().getName() == 'Reconciliation__c'){
            Map<Id, ReconciliationDetail__c> reconciliationDetailMap = ReconciliationQueries.getReconciliationDetailMapWithReconciliationID(new Set<ID>{caseSensitiveId});
            Set<string> reconciliationDetailIDs = new Set<string>();
            
            for(string rDetailId : reconciliationDetailMap.keyset())
                reconciliationDetailIDs.add(rDetailId.substring(0, 15));    
            //query = 'SELECT id, Type__c, Comments__c, Status__c, AttachmentID__c, AttachmentCleanURL__c, ParentID__c from GenericDocument__c where ParentID__c != null and (ParentID__c IN '+DatabaseUtil.inSetStringBuilder(reconciliationDetailIDs) + ' or ParentID__c = \''+recordId.substring(0, 15)+')\'';
              query = 'SELECT id, Type__c, Comments__c, Status__c, AttachmentID__c, AttachmentCleanURL__c, ParentID__c from GenericDocument__c where ParentID__c != null and (ParentID__c IN '+DatabaseUtil.inSetStringBuilder(reconciliationDetailIDs) + ' or ParentID__c = \''+recordId.substring(0, 15)+'\')';

        }
        else{
            query = 'SELECT id, Type__c, Comments__c, Status__c, AttachmentID__c, AttachmentCleanURL__c, ParentID__c from GenericDocument__c where ParentID__c != null and ParentID__c = \''+recordId.substring(0, 15)+'\'';
        }
        
        Map<ID, GenericDocument__c> docMap = new Map<ID, GenericDocument__c>((List<GenericDocument__c>)Database.query(query));
        //Need to make sure the keys are case sensitive as SOQL is case insensitive and the 15 digit key may be passed in
        Map<ID, GenericDocument__c> responseMap = new Map<ID, GenericDocument__c>();
        System.debug('docMap:'+docMap);
        for(GenericDocument__c genDoc : docMap.values()){
            System.debug('genDoc:'+genDoc);
            if(genDoc.ParentID__c.equals(caseSensitiveId) || genDoc.ParentID__c.equals(recordId)){//case sensitive
                responseMap.put(genDoc.id, genDoc);
            } 
        }
        System.debug('responseMap:'+responseMap);
        //return docMap.values();
        return responseMap.values();
    }
}