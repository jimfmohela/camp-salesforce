/////////////////////////////////////////////////////////////////////////
// Class: PaymentAllocationTriggerHandler_TEST
// 
// Description: 
//      Test class for PaymentAllocationTriggerHandler
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-10   Jared Hagemann  Created                                 
// 
/////////////////////////////////////////////////////////////////////////
@isTest
public with sharing class PaymentAllocationTriggerHandler_TEST {
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, PaymentMethod__c> paymentMethods = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, PaymentInstruction__c> paymentInstructionMap = TestDataFactory.createAndInsertPaymentInstruction(TestUtil.TEST_THROTTLE, students, paymentMethods);
        }
    }
    //to do jared - this is just a dummy method to get past test levels
    @isTest
    public static void testManageAgreementsInsert(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> studProgramMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, PaymentInstruction__c> paymentInstructionMap = PaymentInstructionQueries.getPaymentInstructionMap();
        Test.startTest();
            Map<Id, PaymentAllocation__c> paymentAllocationMap = TestDataFactory.createAndInsertPaymentAllocation(5, paymentInstructionMap, studProgramMap);
        Test.stopTest();
        Map<Id, StudentProgram__c> studProgramResultMap = StudentProgramQueries.getStudentProgramMap();
        for(StudentProgram__c sp : studProgramResultMap.values()){
            System.assertEquals(5000, sp.PaidToDate__c);
        }       
    }

    @isTest
    public static void testManageAgreementsUpdate(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> studProgramMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, PaymentInstruction__c> paymentInstructionMap = PaymentInstructionQueries.getPaymentInstructionMap();
        Map<Id, PaymentAllocation__c> paymentAllocationMap = TestDataFactory.createAndInsertPaymentAllocation(5, paymentInstructionMap, studProgramMap);
        for(PaymentAllocation__c pa : paymentAllocationMap.values()){
            pa.AmountAllocated__c = 500;
        }
        Test.startTest();
            //update paymentAllocationMap.values();
            dbUtil.updateRecords(paymentAllocationMap.values());
        Test.stopTest();
        Map<Id, StudentProgram__c> studProgramResultMap = StudentProgramQueries.getStudentProgramMap();
        for(StudentProgram__c sp : studProgramResultMap.values()){
            System.assertEquals(2500, sp.PaidToDate__c);
        }       
    }

    @isTest
    public static void testManageAgreementsDelete(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> studProgramMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, PaymentInstruction__c> paymentInstructionMap = PaymentInstructionQueries.getPaymentInstructionMap();
        Map<Id, PaymentAllocation__c> paymentAllocationMap = TestDataFactory.createAndInsertPaymentAllocation(5, paymentInstructionMap, studProgramMap);
        Test.startTest();
            //delete paymentAllocationMap.values();
            dbUtil.deleteRecords(paymentAllocationMap.values());
        Test.stopTest();
        Map<Id, StudentProgram__c> studProgramResultMap = StudentProgramQueries.getStudentProgramMap();
        for(StudentProgram__c sp : studProgramResultMap.values()){
            System.assertEquals(0, sp.PaidToDate__c);
        }   
    }
}