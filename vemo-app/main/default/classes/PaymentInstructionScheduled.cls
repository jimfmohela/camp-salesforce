/*
PaymentInstructionScheduled job = new PaymentInstructionScheduled();
job.jobType = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
String cronStr = '0 20 * * * ? *';
System.schedule('Generate ACH Batch Nightly', cronStr, job);
*/

public with sharing class PaymentInstructionScheduled implements Schedulable {
    public PaymentInstructionBatch.JobType jobType {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        PaymentInstructionBatch job = new PaymentInstructionBatch();
        job.job = this.jobType;
        job.query = this.query;
        if(job.job == PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH){
            Database.executeBatch(job,100);
        } else if(job.job == PaymentInstructionBatch.JobType.ALLOCATE_PAYMENTS){
            job.proofMode = false;
            Database.executeBatch(job,1);
        } else{
            Database.executeBatch(job);
        }   
    }
    
}