@isTest
public class AccessRuleTriggerHandler_TEST{

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup static void setupData() {
        TestUtil.createStandardTestConditions();
    }
    
    @isTest public static void testInsertStudentShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.AccountEditAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        }
    } 
    
    @isTest public static void testInsertCreditCheckShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta1@vemo.com', communitynickname = 'testcommunity1');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(account stud: studentMap.values()){
            stud.PrimarySchool__pc = schoolMap.values()[0].id;
        }
        update studentMap.values();
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, CreditCheck__c> cCheckMap = TestDataFactory.createAndInsertCreditCheck(1, studentMap);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.CreditCheck__c = cCheckMAp.values()[0].Id;
        accessRule.CreditCheckReadAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        }
    } 
    
    @isTest public static void testInsertDisbursementShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta2@vemo.com', communitynickname = 'testcommunity2');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(1, studentPrgMap , TransactionService.disbursementRecType);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.DisbursementEditAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.Disbursement__c = txMap.values()[0].id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        }
    }  
    
    @isTest public static void testInsertAcademicEnrollmentShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta3@vemo.com', communitynickname = 'testcommunity3');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
            
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schoolMAp, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(studentMAp, testSPOSMap);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.AcademicEnrollmentReadAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.AcademicEnrollment__c = testAcademicEnrollmentMap.values()[0].id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        }
    
    }
    
    @isTest public static void testDeleteStudentShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta4@vemo.com', communitynickname = 'testcommunity4');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, studentPrgMap );
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.AccountEditAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.StudentProgram__c = studentPrgMap.values()[0].Id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        dbUtil.deleteRecords(accessRuleList);
        }
    }
    
    @isTest public static void testDeleteDisbursementShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta5@vemo.com', communitynickname = 'testcommunity5');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, Transaction__c> txMap = TestDataFactory.createAndInsertTransactions(1, studentPrgMap , TransactionService.disbursementRecType);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.DisbursementEditAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.Disbursement__c = txMap.values()[0].id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        dbUtil.deleteRecords(accessRuleList);
        }
    }
    
    @isTest public static void testDeleteCreditCheckShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta6@vemo.com', communitynickname = 'testcommunity6');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(account stud: studentMap.values()){
            stud.PrimarySchool__pc = schoolMap.values()[0].id;
        }
        update studentMap.values();
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, CreditCheck__c> cCheckMap = TestDataFactory.createAndInsertCreditCheck(1, studentMap);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.CreditCheck__c = cCheckMAp.values()[0].Id;
        accessRule.CreditCheckReadAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        dbUtil.deleteRecords(accessRuleList);
        }
    }
    
    @isTest public static void testdeleteAcademicEnrollmentShare(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
         User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta7@vemo.com', communitynickname = 'testcommunity7');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap =  TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
            
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schoolMAp, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(studentMAp, testSPOSMap);
        
        List<AccessRule__c> accessRuleList = new List<AccessRule__c>();
        AccessRule__c accessRule = new AccessRule__c();
        accessRule.AcademicEnrollmentReadAccess__c = true;
        accessRule.Account__c = schoolMap.values()[0].Id;
        accessRule.AcademicEnrollment__c = testAcademicEnrollmentMap.values()[0].id;
        
        accessRuleList.add(accessRule);
        
        dbUtil.insertRecords(accessRuleList);
        dbUtil.deleteRecords(accessRuleList);
        }
    }
    
      
}