public with sharing class CreditCheckEvaluationService{
    
    public static List<CreditCheckEvaluation> getCreditCheckEvaluationWithCreditCheckEvalID(Set<ID> creditCheckEvalIDs){
        System.debug('CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckEvalID()');
        Map<ID, CreditCheckEvaluation__c> ccMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(creditCheckEvalIDs);
        List<CreditCheckEvaluation> ccList = new List<CreditCheckEvaluation>();
        for(CreditCheckEvaluation__c cc : ccMap.values()){
            CreditCheckEvaluation ccCls = new CreditCheckEvaluation(cc);
            ccList.add(ccCls);
        }
        return ccList;
    }
    
    public static List<CreditCheckEvaluation> getCreditCheckEvaluationWithCreditCheckID(Set<ID> creditCheckIDs){
        System.debug('CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckID()');
        Map<ID, CreditCheckEvaluation__c> ccMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckID(creditCheckIDs);
        List<CreditCheckEvaluation> ccList = new List<CreditCheckEvaluation>();
        for(CreditCheckEvaluation__c cc : ccMap.values()){
            CreditCheckEvaluation ccCls = new CreditCheckEvaluation(cc);
            ccList.add(ccCls);
        }
        return ccList;
    }
    
    public static List<CreditCheckEvaluation> getCreditCheckEvaluationWithAgreementID(Set<ID> agreementIDs){
        System.debug('CreditCheckEvaluationService.getCreditCheckEvaluationWithAgreementID()');
        Map<ID, CreditCheckEvaluation__c> ccMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithAgreementID(agreementIDs);
        List<CreditCheckEvaluation> ccList = new List<CreditCheckEvaluation>();
        for(CreditCheckEvaluation__c cc : ccMap.values()){
            CreditCheckEvaluation ccCls = new CreditCheckEvaluation(cc);
            ccList.add(ccCls);
        }
        return ccList;
    }
    
    public static Set<ID> createCreditCheckEvaluations(List<CreditCheckEvaluation> ccs){
        System.debug('CreditCheckEvaluationService.createCreditCheckEvaluations()');
        List<CreditCheckEvaluation__c> newCCs = new List<CreditCheckEvaluation__c>();
        for(CreditCheckEvaluation cc : ccs){
            CreditCheckEvaluation__c newCC = ccToCC(cc);
            newCCs.add(newCC);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newCCs);
        Set<ID> ccIDs = new Set<ID>();
        for(CreditCheckEvaluation__c cc : newCCs){
            ccIDs.add(cc.ID);
        }
        return ccIDs;
    }
    
     public static Set<ID> updateCreditCheckEvaluations(List<CreditCheckEvaluation> ccs){
        System.debug('CreditCheckEvaluationService.updateCreditCheckEvaluation()');
        List<CreditCheckEvaluation__c> newCCs = new List<CreditCheckEvaluation__c>();
        for(CreditCheckEvaluation cc : ccs){
            cc.creditCheckID = null;
            CreditCheckEvaluation__c newCC = ccToCC(cc);
            newCCs.add(newCC);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newCCs);
        Set<ID> ccIDs = new Set<ID>();
        for(CreditCheckEvaluation__c cc : newCCs){
            ccIDs.add(cc.ID);
        }
        return ccIDs;
    }
    
    public static Integer deleteCreditCheckEvaluations(Set<ID> creditCheckEvalIDs){
        System.debug('CreditCheckEvaluationService.deleteCreditCheckEvaluations()');
        Map<ID, CreditCheckEvaluation__c> ccMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(creditCheckEvalIDs);
        Integer numToDelete = ccMap.size();
        DatabaseUtil dbUtil = new DatabaseUtil();        
        dbUtil.deleteRecords(ccMap.values());
        return numToDelete;
    }
    
    public static CreditCheckEvaluation__c ccToCC(CreditCheckEvaluation cc){
        CreditCheckEvaluation__c ccObj = new CreditCheckEvaluation__c();
        if(cc.creditCheckEvaluationID != null) ccObj.ID = cc.creditCheckEvaluationID; 
        if(cc.creditCheckID != null) ccObj.CreditCheck__c = cc.creditCheckID;
        if(cc.agreementID != null) ccObj.Agreement__c = cc.agreementID;
        if(cc.status != null) ccObj.Status__c = cc.status;      
        if(cc.creditCheckDeniedReasonText != null) ccObj.CreditCheckDeniedReasonText__c = cc.creditCheckDeniedReasonText;
        if(cc.creditCheckDeniedReason != null) ccObj.CreditCheckDeniedReason__c = cc.creditCheckDeniedReason;
        if(cc.creditCheckProcess != null) ccObj.CreditCheckProcess__c = cc.creditCheckProcess;
        return ccObj;
    }
    
    
    public class CreditCheckEvaluation{
        public String creditCheckEvaluationID {get;set;}
        public String creditCheckID {get;set;}
        public String agreementID {get;set;}
        public String status {get;set;}
        public String creditCheckDeniedReasonText {get;set;}
        public String creditCheckDeniedReason {get;set;}
        public String creditCheckProcess {get;set;}
        public DateTime createdDate {get;set;}
        public CreditCheckEvaluation(){}

        public CreditCheckEvaluation(CreditCheckEvaluation__c cc){
            this.creditCheckEvaluationID = cc.ID;
            this.creditCheckID = cc.creditCheck__c;
            this.agreementID = cc.agreement__c; 
            this.status = cc.Status__c;
            this.creditCheckDeniedReasonText = cc.CreditCheckDeniedReasonText__c;
            this.creditCheckDeniedReason = cc.CreditCheckDeniedReason__c;
            this.creditCheckProcess = cc.creditCheckProcess__c;
            this.createdDate = cc.createdDate; 
        }
    }

}