public class EventSubscriptionQueries {
    
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }
    public static Map<ID, EventSubscription__c> getEventSubscriptionMapWithAuthUser(set<Id> contactIDSet){
        
        Map<ID, EventSubscription__c> eventSubscriptionMap = new Map<ID, EventSubscription__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Contact__c IN ' + DatabaseUtil.inSetStringBuilder(contactIdSet);
        
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        eventSubscriptionMap = new Map<ID, EventSubscription__c>((List<EventSubscription__c>)db.query(query));
        return eventSubscriptionMap;  
    }
    
    public static Map<ID, EventSubscription__c> getEventSubscriptionMapWithEventSubscriptionID(Set<ID> eventSubscriptionIDs){
        Map<ID, EventSubscription__c> eventSubscriptionMap = new Map<ID, EventSubscription__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(eventSubscriptionIDs);
        
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        eventSubscriptionMap = new Map<ID, EventSubscription__c>((List<EventSubscription__c>)db.query(query));
        return eventSubscriptionMap;  
    }
    
    public static Map<ID, EventSubscription__c> getEventSubscriptionMapWithEventTypes(Set<ID> accountIDs,Set<ID> eventTypeIDs){
        Map<ID, EventSubscription__c> eventSubscriptionMap = new Map<ID, EventSubscription__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Account__c IN ' + DatabaseUtil.inSetStringBuilder(accountIDs);
        
        query += ' and EventType__c IN '+DatabaseUtil.inSetStringBuilder(eventTypeIDs);
        
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        eventSubscriptionMap = new Map<ID, EventSubscription__c>((List<EventSubscription__c>)db.query(query));
        return eventSubscriptionMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM EventSubscription__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Account__c, ';
        fieldNames += 'Contact__c, ';
        fieldNames += 'Email__c, ';
        fieldNames += 'EventType__c, ';
        fieldNames += 'OwnerID ';
        return fieldNames;
   }
   
   
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }
  
    private static String buildFilterString(){
        String filterStr = '';
       
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +   '\' ');
            }      
        }
        return filterStr;
    }  
}