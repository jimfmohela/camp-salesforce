public class ReportBillingVolumeController{
    public Date fromDate;
    public Date toDate;
    public DateTime fromDateTime;
    public DateTime toDateTime;
    public Set<String> schoolStatus;
    public Set<String> servicingStatus;
    public Set<String> defermentStatus;
    public Set<String> cancelledStatus;
    public Integer month1 {get;set;}
    public Integer month2 {get;set;}
    public Integer month3 {get;set;}
    public Integer year;
    public List<reportDataWrapper> reportData {get;set;}
    public String selectedQuarter {get;set;}
    public String selectedYear {get;set;}
    public String selectedSchool {get;set;}
    public String selectedFundOwner {get;set;}
    public Map<Integer,String> monthMap {get;set;}
    public Boolean showResults {get;set;}
    public String csv {get;set;}

    /////////////////////////////////////////////////////
    ///Get options for the Quarters Picklist
    /////////////////////////////////////////////////////
    public List<SelectOption> getQuarter(){
        List<SelectOption> output = new List<SelectOption>();
        output.add(new SelectOption('Quarter 1 (Jan-Mar)','Quarter 1 (Jan-Mar)'));
        output.add(new SelectOption('Quarter 2 (Apr-Jun)','Quarter 2 (Apr-Jun)'));
        output.add(new SelectOption('Quarter 3 (Jul-Sep)','Quarter 3 (Jul-Sep)'));
        output.add(new SelectOption('Quarter 4 (Oct-Dec)','Quarter 4 (Oct-Dec)'));
        return output;
    }

    //////////////////////////////////////////////////////
    ///Get options for the Years picklist
    //////////////////////////////////////////////////////
    public List<SelectOption> getYear(){
        List<SelectOption> output = new List<SelectOption>();
        for(Integer i=2018;i<=2030;i++){
            output.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        return output;    
    }

    ///////////////////////////////////////////////////////
    ///Get options for the school picklist
    ///////////////////////////////////////////////////////
    public List<SelectOption> getSchool(){
        List<SelectOption> output = new List<SelectOption>();
        List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By Name ASC]; 
        for(Account acc:schools){
            output.add(new SelectOption(acc.id,acc.name)); 
            if(acc.name == 'Purdue University-Main Campus'){
                selectedSchool = acc.id;    
            }
        }
        return output;
    }
    
    ///////////////////////////////////////////////////////
    ///Get options for the school picklist
    ///////////////////////////////////////////////////////
    public List<SelectOption> getFundOwner(){
        List<SelectOption> output = new List<SelectOption>();
        List<Account> fo = [Select id,name From Account Where Name='PRF Fund 1' OR Name='PRF Fund 2' Order By Name ASC]; 
        for(Account acc:fo){
            output.add(new SelectOption(acc.id,acc.name)); 
            if(acc.name == 'PRF Fund 1'){
                selectedSchool = acc.id;    
            }
        }
        return output;
    }

    //////////////////////////////////////////////
    ////Constructor 
    //////////////////////////////////////////////
    public ReportBillingVolumeController(){
        schoolStatus = new Set<String>{'Certified','Partially Funded','Fully Funded','Leave of Absence','School'};
        servicingStatus = new Set<String>{'Grace','Payment','Internship'};
        defermentStatus = new Set<String>{'Deferment'};
        cancelledStatus = new Set<String>{'Cancelled'};
        monthMap = new Map<Integer,String>{1 => 'JAN',2 => 'FEB',3 => 'MAR',4 => 'APR',5 => 'MAY',6 => 'JUN',
                                           7 => 'JUL',8 => 'AUG',9 => 'SEP',10 => 'OCT',11 => 'NOV',12 => 'DEC'};
        showResults = false;
    }
    
    ////////////////////////////////////////////////
    ///Run report to build report data
    ////////////////////////////////////////////////
    public void runReport(){
        showResults = true;
        /*------------------set date range based on the selected quarter-------------------*/
        if(selectedQuarter <> null && selectedYear <> null && selectedSchool <> null){
            switch on selectedQuarter{
                when 'Quarter 1 (Jan-Mar)' {
                    fromDate = Date.newInstance(Integer.valueOf(selectedYear),1,1);
                    toDate = Date.newInstance(Integer.valueOf(selectedYear),3,31);
                    fromDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),1,1);
                    toDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),4,1);
        
                    month1 = 1;
                    month2 = 2;
                    month3 = 3;
                }
                when 'Quarter 2 (Apr-Jun)' {
                    fromDate = Date.newInstance(Integer.valueOf(selectedYear),4,1);
                    toDate = Date.newInstance(Integer.valueOf(selectedYear),6,30);
                    fromDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),4,1);
                    toDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),7,1);
        
                    month1 = 4;
                    month2 = 5;
                    month3 = 6;
                }
                when 'Quarter 3 (Jul-Sep)' {
                    fromDate = Date.newInstance(Integer.valueOf(selectedYear),7,1);
                    toDate = Date.newInstance(Integer.valueOf(selectedYear),9,30);
                    fromDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),7,1);
                    toDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),10,1);
        
                    month1 = 7;
                    month2 = 8;
                    month3 = 9;
                }
                when 'Quarter 4 (Oct-Dec)' {
                    fromDate = Date.newInstance(Integer.valueOf(selectedYear),10,1);
                    toDate = Date.newInstance(Integer.valueOf(selectedYear),12,31);
                    fromDateTime = DateTime.newInstance(Integer.valueOf(selectedYear),10,1);
                    toDateTime = DateTime.newInstance(Integer.valueOf(selectedYear)+1,1,1);
        
                    month1 = 10;
                    month2 = 11;
                    month3 = 12;
                }
            }
        }
        else{
            return;    
        }

        year = Integer.valueOf(selectedYear);

        /*----------------------------------------------------------------------------------------*/ 
        Map<String,List<Transaction__c>> agreementDisbursementMap = new Map<String,List<Transaction__c>>();
        Map<String,Set<ID>> studentAgreementMap = new Map<String,Set<ID>>();
        Map<String,String> statusByAgreementMonth = new Map<String,String>();
        Map<ID,StudentProgram__c> agreementMap = new Map<ID,StudentProgram__c>();
        /*-----------------------------------------------------------------------------------------*/

        getTransactions(agreementDisbursementMap,studentAgreementMap);
        getAgreementStatuses(agreementDisbursementMap.keySet(),statusByAgreementMonth);
        agreementMap = getAgreements(agreementDisbursementMap.keySet());
        buildReportData(agreementDisbursementMap,studentAgreementMap,statusByAgreementMonth,agreementMap);
        reportData.sort();
    }
    
    ///////////////////////////////////////////////////////
    ///Get Disbursements 
    ///////////////////////////////////////////////////////
    public void getTransactions(Map<String,List<Transaction__c>> agreementDisbursementMap,Map<String,Set<ID>> studentAgreementMap){
        List<Transaction__c> disbursements = new List<Transaction__c>();
        disbursements = [SELECT id,recordtype.name,Student__c,agreement__c,BalanceAmount__c,transactionDate__c
                          FROM Transaction__c
                          WHERE transactionDate__c <= :toDate
                          AND Status__c <> 'Cancelled'
                          AND Agreement__r.Program__r.School__c = :selectedSchool
                          AND agreement__r.FundOwner__c =: selectedFundOwner];
                          
        if(Test.isRunningTest()){                          
            disbursements = [SELECT id,recordtype.name,Student__c,agreement__c,BalanceAmount__c,transactionDate__c
                              FROM Transaction__c
                              ];
        }
        
        for(Transaction__c disb:disbursements){
            if(!agreementDisbursementMap.containsKey(disb.agreement__c)){
                agreementDisbursementMap.put(disb.agreement__c,new List<transaction__c>());
            }
            agreementDisbursementMap.get(disb.agreement__c).add(disb);    
    
            if(!studentAgreementMap.containsKey(disb.Student__c)){
                studentAgreementMap.put(disb.Student__c,new Set<ID>());
            }
            studentAgreementMap.get(disb.Student__c).add(disb.agreement__c);
        }
    }
    
    ////////////////////////////////////////////////
    ///Get Agreements
    ////////////////////////////////////////////////
    public Map<ID,StudentProgram__c> getAgreements(Set<String> agreementIDs){
        system.debug(selectedFundOwner);
        Map<ID,StudentProgram__c> agreementMap = new Map<ID,StudentProgram__c>([Select Id,name,status__c,closedDate__c,CurrentStatusDate__c,student__r.name,programName__c FROM StudentProgram__c WHERE ID IN :agreementIDs]);        
        system.debug(agreementMap.size());
        return agreementMap;
    }
    
    //////////////////////////////////////////////////////
    ///Get StudentProgramsAdudit for agreements statuses
    //////////////////////////////////////////////////////
    public void getAgreementStatuses(Set<String> agreementIDs,Map<String,String> statusByAgreementMonth){
        List<StudentProgramAudit__c> audits = [SELECT id,status__c,StudentProgram__c,AuditDateTime__c
                                            FROM StudentProgramAudit__c
                                            WHERE StudentProgram__c IN :agreementIDs 
                                            AND MonthEnd__c = true 
                                            AND AuditDateTime__c >= :fromDateTime 
                                            AND AuditDateTime__c <= :toDateTime];
    
        for(StudentProgramAudit__c spa:audits){
            String key = spa.StudentProgram__c + ':' + spa.AuditDateTime__c.date().month();
            statusByAgreementMonth.put(key,spa.status__c);
        }
    }
    
    ///////////////////////////////////////////////////////
    ///Build final report data
    ///////////////////////////////////////////////////////
    public void buildReportData(Map<String,List<Transaction__c>> agreementDisbursementMap,Map<String,Set<ID>> studentAgreementMap,Map<String,String> statusByAgreementMonth,Map<ID,studentProgram__c> agreementMap){
        
        reportData = new List<reportDataWrapper>();
        for(String studentID:studentAgreementMap.keySet()){
            reportDataWrapper studentData = new reportDataWrapper();
            Boolean addStudent = false;
            for(ID agreementID:studentAgreementMap.get(studentID)){
                agreementWrapper agreementData = new agreementWrapper();
                if(studentData.name == null){
                    studentData.name = agreementMap.get(agreementID).student__r.name;
                    studentData.accountID = agreementMap.get(agreementID).student__c;        
                }
                if(agreementData.agreementNumber == null){
                    agreementData.agreementNumber = agreementMap.get(agreementID).name;
                    agreementData.agreementID = agreementMap.get(agreementID).id;
                    agreementData.programName = agreementMap.get(agreementID).ProgramName__c;    
                }
                Decimal totalDisbursedAmount = 0; 
                Boolean transactionInReportingMonth1 = false;
                Boolean transactionInReportingMonth2 = false;
                Boolean transactionInReportingMonth3 = false;
                for(transaction__c disb:agreementDisbursementMap.get(agreementID)){
                    
                    /*************sum transaction amount**********/
                    if(disb.transactionDate__c < fromDate){
                        totalDisbursedAmount += disb.BalanceAmount__c;
                    }
                    if(disb.transactionDate__c >= fromDate){
                        if(disb.transactionDate__c.month() == month1){
                            agreementData.monthBucketData.get('MONTH1').disbursedAmount += disb.BalanceAmount__c; 
                            transactionInReportingMonth1 = true;       
                        }
                        else if(disb.transactionDate__c.month() == month2){
                            agreementData.monthBucketData.get('MONTH2').disbursedAmount += disb.BalanceAmount__c;  
                            transactionInReportingMonth2 = true;      
                        }       
                        else if(disb.transactionDate__c.month() == month3){
                            agreementData.monthBucketData.get('MONTH3').disbursedAmount += disb.BalanceAmount__c;  
                            transactionInReportingMonth3 = true;      
                        } 
                    }
                }
                
                /*******bucket by status*************/
                if(agreementMap.get(agreementID).currentStatusDate__c < fromDateTime && agreementMap.get(agreementID).status__c == 'Cancelled'){
                    continue;    
                }    
                if(agreementMap.get(agreementID).closedDate__c <> null){
                    Datetime closedDate = agreementMap.get(agreementID).closedDate__c;
                    if((closedDate.month() <= month1 && closedDate.year() == year) || (closedDate.year() < year)){
                        continue;
                    }    
                    else if(closedDate.month() == month2 && closedDate.year() == year){
                        agreementData.monthBucketData.get('MONTH1').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month1));
                        agreementData.monthBucketData.get('MONTH1').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount;
                        studentData.agreements.add(agreementData);
                        addStudent = true;
                        continue;
                    }
                    else if(closedDate.month() == month3 && closedDate.year() == year){
                        agreementData.monthBucketData.get('MONTH1').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month1));
                        agreementData.monthBucketData.get('MONTH1').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount;
                        agreementData.monthBucketData.get('MONTH2').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month2));
                        agreementData.monthBucketData.get('MONTH2').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount + agreementData.monthBucketData.get('MONTH2').disbursedAmount;
                        studentData.agreements.add(agreementData);
                        addStudent = true;
                        continue;
                    }
                    else{
                        agreementData.monthBucketData.get('MONTH1').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month1));
                        agreementData.monthBucketData.get('MONTH1').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount;
                        agreementData.monthBucketData.get('MONTH2').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month2));
                        agreementData.monthBucketData.get('MONTH2').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount + agreementData.monthBucketData.get('MONTH2').disbursedAmount;
                        agreementData.monthBucketData.get('MONTH3').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month3));
                        agreementData.monthBucketData.get('MONTH3').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount + agreementData.monthBucketData.get('MONTH2').disbursedAmount + agreementData.monthBucketData.get('MONTH3').disbursedAmount;
                        studentData.agreements.add(agreementData);
                        addStudent = true;
                        continue;
                    }
                }
                else{
                    agreementData.monthBucketData.get('MONTH1').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month1 ));
                    agreementData.monthBucketData.get('MONTH1').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount;
                    agreementData.monthBucketData.get('MONTH2').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month2 ));
                    agreementData.monthBucketData.get('MONTH2').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount + agreementData.monthBucketData.get('MONTH2').disbursedAmount;
                    agreementData.monthBucketData.get('MONTH3').bucketName = getBucket(statusByAgreementMonth.get(agreementID + ':' + month3 ));
                    agreementData.monthBucketData.get('MONTH3').totalDisbursedAmount = totalDisbursedAmount + agreementData.monthBucketData.get('MONTH1').disbursedAmount + agreementData.monthBucketData.get('MONTH2').disbursedAmount + agreementData.monthBucketData.get('MONTH3').disbursedAmount;
                    studentData.agreements.add(agreementData);
                    addStudent = true;
                }
            }
            reportData.add(studentData);    
        }
    }
    
    ///////////////////////////////////////
    ///Get Bucket Name by Status
    ///////////////////////////////////////
    public String getBucket(String status){
        System.debug('$$$:'+status);
        if(schoolStatus.contains(status)) return 'In School';
        if(servicingStatus.contains(status)) return 'In Servicing';
        if(defermentStatus.contains(status)) return 'In Deferment';
        if(cancelledStatus.contains(status)) return 'Cancelled';
        return null;
    }
    
    ////////////////////////////////////////
    ///Get the csv file 
    ////////////////////////////////////////
    public void generateCSV(){
        runReport();
        if(reportData <> null && reportData.size()>0){
            csv = generateHeader();
            for(ReportDataWrapper student:reportData){
                    
                for(agreementWrapper agreement:student.agreements){
                    csv += student.name + ',';
                    csv += agreement.agreementNumber + ',';
                    csv += agreement.programName + ',';
                    csv += 'Disbursed Amount' + ',';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH1').disbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH1').disbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH1').disbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH1').disbursedAmount + ',';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == null) csv += ',,,,';
                    
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH2').disbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH2').disbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH2').disbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH2').disbursedAmount + ',';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == null) csv += ',,,,';
                    
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH3').disbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH3').disbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH3').disbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH3').disbursedAmount + ',';
                    
                    csv += '\n';
                    csv += student.name + ',';
                    csv += agreement.agreementNumber + ',';
                    csv += agreement.programName + ',';
                    csv += 'Total Disbursed Amount' + ',';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH1').totalDisbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH1').totalDisbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH1').totalDisbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH1').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH1').totalDisbursedAmount + ','; 
                    if(agreement.monthBucketData.get('MONTH1').bucketName == null) csv += ',,,,';

                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH2').totalDisbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH2').totalDisbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH2').totalDisbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH2').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH2').totalDisbursedAmount + ','; 
                    if(agreement.monthBucketData.get('MONTH2').bucketName == null) csv += ',,,,';

                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In School') csv += agreement.monthBucketData.get('MONTH3').totalDisbursedAmount + ',,,,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In Servicing') csv += ',' + agreement.monthBucketData.get('MONTH3').totalDisbursedAmount + ',,,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'In Deferment') csv += ',,' + agreement.monthBucketData.get('MONTH3').totalDisbursedAmount + ',,';
                    if(agreement.monthBucketData.get('MONTH3').bucketName == 'Cancelled') csv += ',,,' + agreement.monthBucketData.get('MONTH3').totalDisbursedAmount + ','; 
                    csv += '\n';
                }     
            }
        }
    }

    //////////////////////////////////////////////////////
    ///Generate header for the CSV file
    //////////////////////////////////////////////////////
    private String generateHeader(){
        String csv = ',,,,';
        for(Integer i=month1;i<=(month1+2);i++){
            Integer count = 1;
            while(count<=4){
                csv += monthMap.get(i) + '-' + String.valueOf(year) + ',';
                count++;
            }
        }
        csv += '\n';
        csv += 'Student Name,Student Program Number,Program Name,,In School,In Servicing,In Deferment,Cancelled,In School,In Servicing,In Deferment,Cancelled,In School,In Servicing,In Deferment,Cancelled' + '\n';
        return csv;
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportBillingVolumeExport');
        pg.setRedirect(false);
        return pg;
    }

    //////////////////////////////////
    ///Wrapper to hold report data
    //////////////////////////////////
    public class ReportDataWrapper implements Comparable{
        public string name {get;set;}
        public string accountID {get;set;}
        public List<agreementWrapper> agreements {get;set;}
    
        public reportDataWrapper(){
            agreements = new List<agreementWrapper>();
        }
        
        public integer CompareTo(Object compareTo){
            ReportDataWrapper compareToStudent = (ReportDataWrapper)compareTo; 
            if(name.toLowerCase() == compareToStudent.name.toLowerCase()) return 0;
            if(name.toLowerCase() > compareToStudent.name.toLowerCase()) return 1;
            return -1;                                                            
        }  
    }
    
    ///////////////////////////////////////////
    ///Wrapper to hold agreement level data 
    ///////////////////////////////////////////
    public class agreementWrapper{
        public String agreementNumber {get;set;}
        public String agreementID {get;set;}
        public String programName {get;set;}
        public Map<String,monthBucketWrapper> monthBucketData {get;set;}
    
        public agreementWrapper(){
            monthBucketData = new Map<String,monthBucketWrapper>();
            monthBucketData.put('MONTH1',new monthBucketWrapper());
            monthBucketData.put('MONTH2',new monthBucketWrapper());
            monthBucketData.put('MONTH3',new monthBucketWrapper());
        }
    }
    
    //////////////////////////////////////////////////////////////////////
    ///Wrapper to hold individual bucket data for 1 agreement for 1 month
    //////////////////////////////////////////////////////////////////////
    public class monthBucketWrapper{
        public String bucketName {get;set;}
        public Decimal disbursedAmount {get;set;}
        public Decimal totalDisbursedAmount {get;set;}
    
        public monthBucketWrapper(){
            this.disbursedAmount = 0;
            this.totalDisbursedAmount = 0;        
        }     
    }
}