/////////////////////////////////////////////////////////////////////////
// Class: DataCollectionTemplateService_TEST
// 
// Description: 
//  Unit test for DataCollectionTemplateService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-05   Rini Gupta  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class DataCollectionTemplateService_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void ValidateGetTemplateWithProgramID(){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Test.startTest();
        List<DataCollectionTemplateService.Template> resultDataCollectionTemplateMap = DataCollectionTemplateService.getTemplateWithProgramID(programMap.keyset());
        System.assertEquals(TemplateMap.keySet().size(), resultDataCollectionTemplateMap.size());
        Test.stopTest();
    }
    
 }