public with sharing class StudentProgramBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts{
    public enum JobType {DELINQUENCY, PURGE_DELETED, MONTH_END_AUDIT, CONTRACT_ASSESSMENT,GENERATE_FINAL_DISCLOSURE, STATUS_TRANSITION, CALCULATE_AMOUNTS}
   
    public String query {get;set;}
    public JobType job {get;set;}
   
    Set<Id> agreementIds = new Set<Id>();
   
    public StudentProgramBatch() {

    }
   
    public StudentProgramBatch(Set<Id> agreementIds){
        this.agreementIds = agreementIds;
    }
    public StudentProgramBatch(String query) {
        this.query = query;
    }  
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('StudentProgramBatch.start()');
        if(job == JobType.DELINQUENCY){
            string paymentStatus = 'Payment';
            if(String.isEmpty(this.query)){
                query = 'SELECT id, DaysDelinquent__c, Student__c, Student__r.PersonContactID, Student__r.PersonEmail, Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c from StudentProgram__c WHERE DaysDelinquent__c > 0 and Status__c =: paymentStatus ORDER BY DaysDelinquent__c DESC';
            }
        } else if(job == JobType.PURGE_DELETED){
            if(String.isEmpty(this.query)){
                Set<String> deletableStatuses= new Set<String>{'Draft', 'Invited', 'Cancelled'};
                query = 'SELECT id from StudentProgram__c WHERE Deleted__c = true and Status__c IN :deletableStatuses';
            }
        } else if(job == JobType.MONTH_END_AUDIT){
            if(String.isEmpty(this.query)){
                query = 'SELECT id From StudentProgram__c Where Deleted__c = false';
            }
        } else if(job == JobType.CONTRACT_ASSESSMENT){
            if(String.isEmpty(this.query)){
                String dateToday = String.valueOf(Date.today());            
                query = 'SELECT id from StudentProgram__c WHERE (AssessmentDate__c <= '+dateToday + ' OR AssessContract__c = true)';
            }
        }
        else if(job == JobType.GENERATE_FINAL_DISCLOSURE){
            if(String.isEmpty(this.query)){
                query = 'SELECT id, CongaFinalDisclosureStatus__c, FinalDisclosureID__c, CongaFinalDisclosureID__c from StudentProgram__c where GenerateFinalDisclosure__c = true';
            }
        }
        else if(job == JobType.STATUS_TRANSITION){
            if(String.isEmpty(this.query)){
                query = 'SELECT ID, ServicingStartDate__c, Servicing__c, Status__c, GracePeriodEndDate__c, PauseEndDate__c, DefermentEndDate__c from StudentProgram__c where (Status__c = \'Grace\' and GracePeriodEndDate__c < TODAY) OR  (Status__c = \'Pause\' and PauseEndDate__c < TODAY) OR (Status__c = \'Deferment\' and DefermentEndDate__c < TODAY) OR (ServicingStartDate__c < TODAY)';
            }
        }
        else if(job == JobType.CALCULATE_AMOUNTS){
            if(String.isEmpty(this.query)){
                query  = 'SELECT id from StudentProgram__c WHERE ID IN' + DatabaseUtil.inSetStringBuilder(agreementIds);
            }
        }
        System.debug('job:'+job);
        System.debug('query:'+query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('StudentProgramBatch.execute()');
        System.debug('scope:'+scope);  
        if(job == JobType.DELINQUENCY){
            handleDelinquency((List<StudentProgram__c>)scope);
        }else if(job == JobType.PURGE_DELETED){
            handlePurgeDeleted((List<StudentProgram__c>)scope);
        }else if(job == JobType.MONTH_END_AUDIT){
            handleAudit((List<StudentProgram__c>)scope);
        }else if(job == JobType.CONTRACT_ASSESSMENT){
            Set<ID> agreementIDs = new Set<ID>();
            for(SObject obj : scope){
                agreementIDs.add(obj.id);
            }
            AssessmentService.assessContracts(agreementIDs);
        }
        else if(job == JobType.GENERATE_FINAL_DISCLOSURE){
            handleFinalDisclosure((List<StudentProgram__c>)scope);
        }
        else if(job == JobType.STATUS_TRANSITION){
            handleStatusTransition((List<StudentProgram__c>)scope);
        }
        else if(job == JobType.CALCULATE_AMOUNTS){
            Set<ID> agreementIDs = new Set<ID>();
            for(SObject obj : scope){
                agreementIDs.add(obj.id);
            }
            CalculateAmountsOnAgreement(agreementIds);
        }
    }
   
    public void finish(Database.BatchableContext BC) {
        System.debug('StudentProgramBatch.finish()');

    }
   
    private static void handlePurgeDeleted(List<StudentProgram__c> agreements){
        System.debug('StudentProgramBatch.handlePurgeDeleted()');  
        Set<ID> agreementIDs = new Set<ID>();
        for(StudentProgram__c agreement : agreements){
            agreementIDs.add(agreement.id);
        }
        Map<ID, Transaction__c> disbMap = TransactionQueries.getTransactionMapWithAgreementID(agreementIDs, TransactionService.disbursementRecType);
        Database.delete(disbMap.values(), false);
        Database.delete(agreements, false);
    }

       private static void handleDelinquency(List<StudentProgram__c> agreements){

        System.debug('StudentProgramBatch.handleDelinquency()');    
        List<Task> tasksToBeCreated = new List<Task>();
        List<OutboundEmail__c> emailToBeCreated = new List<OutboundEmail__c>();
        Set<String> studentCampusServiceEmailSet = new Set<String>();
       
        for(StudentProgram__c agreement : agreements){
            if(agreement.DaysDelinquent__c == 12 || agreement.DaysDelinquent__c == 27){
                if(agreement.Student__r != null && agreement.Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c != null){
                    studentCampusServiceEmailSet.add(agreement.Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c);
                }
            }
        }
        List<User> uList = [select id,name, email from User where email IN: studentCampusServiceEmailSet
                                                            AND profile.name != '8.0 - College Admin Default'
                                                            AND User.Profile.UserLicense.Name != 'Customer Community Plus'];
        Map<String, Id> userMap = new Map<String, Id>();
        for(User u: uList) userMap.put(u.email, u.id);
       
        for(StudentProgram__c agreement : agreements){
            if(agreement.DaysDelinquent__c == 12 || agreement.DaysDelinquent__c == 27 ||
               agreement.DaysDelinquent__c == 42 || agreement.DaysDelinquent__c == 57 ||
               agreement.DaysDelinquent__c == 72 || agreement.DaysDelinquent__c == 87 ||
               agreement.DaysDelinquent__c == 102 || agreement.DaysDelinquent__c == 117 ||   
               agreement.DaysDelinquent__c == 132 || agreement.DaysDelinquent__c == 147 ||                 
               agreement.DaysDelinquent__c == 162 || agreement.DaysDelinquent__c == 177 ||
               agreement.DaysDelinquent__c == 192 || agreement.DaysDelinquent__c == 207 ||
               agreement.DaysDelinquent__c == 222 || agreement.DaysDelinquent__c == 237 ||
               agreement.DaysDelinquent__c == 252){    
                 tasksToBeCreated.add(delinquencyCall(agreement,userMap));
                 
                 OutboundEmail__c delEmail = delinquencyEmail(agreement);
                 if(delEmail != null)
                     emailToBeCreated.add(delEmail);
            }

            else if(agreement.DaysDelinquent__c == 242 || agreement.DaysDelinquent__c == 267){ 
                tasksToBeCreated.add(delinquencyCall(agreement,userMap));
            }
        }
        if(tasksToBeCreated != null && tasksToBeCreated.size()>0){
            insert tasksToBeCreated;
        }
        if(emailToBeCreated != null && emailToBeCreated.size()>0){
          insert emailToBeCreated;
        }
    }

    private static Task delinquencyCall(StudentProgram__c agreement, Map<String,Id> UserMap){
        String owner;
        Task newTask;
        String taskType ='Call';
        if(agreement.DaysDelinquent__c == 12 || agreement.DaysDelinquent__c == 42 ||
               agreement.DaysDelinquent__c == 72  || agreement.DaysDelinquent__c == 102 ||
               agreement.DaysDelinquent__c == 132 || agreement.DaysDelinquent__c == 162 || 
               agreement.DaysDelinquent__c == 192 || agreement.DaysDelinquent__c == 222 ||
               agreement.DaysDelinquent__c == 242 || agreement.DaysDelinquent__c == 252){
            taskType = 'Call';
        }
        else if(agreement.DaysDelinquent__c == 27 || agreement.DaysDelinquent__c == 57 ||
               agreement.DaysDelinquent__c == 87  || agreement.DaysDelinquent__c == 117 ||
               agreement.DaysDelinquent__c == 147 || agreement.DaysDelinquent__c == 177 || 
               agreement.DaysDelinquent__c == 207 || agreement.DaysDelinquent__c == 237 ||
               agreement.DaysDelinquent__c == 267){
            taskType = 'Text';
        }
       
        if((agreement.DaysDelinquent__c == 12 || agreement.DaysDelinquent__c == 27) &&
                (agreement.Student__c != null && agreement.Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c != null && 
                UserMap != null && userMap.get(agreement.Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c) != null)){
           
            owner = userMap.get(agreement.Student__r.PrimarySchool__pr.StudentCampusServiceEmail__c);
        }
        else{
            try{
                owner = String.isNotBlank(KeyValuePair__c.getValues('DelinquencyTaskOwnerID').ValueText__c) ?
                                            KeyValuePair__c.getValues('DelinquencyTaskOwnerID').ValueText__c :
                                            UserInfo.getUserId();
            } catch (Exception e) {
                owner =  UserInfo.getUserId();
            }
           
        }
        newTask = new Task(Type = taskType,
                          ActivityDate = Date.today(),
                          Subject = 'Delinquency Notification: (' + String.valueOf((Integer)agreement.DaysDelinquent__c) + ' Days)',
                          WhatID = agreement.Student__c,
                          WhoID = agreement.Student__r.PersonContactID,
                          OwnerID = owner,
                          Description = 'This student is overdue. Please reach out to the student by telephone.');
        System.debug(newTask);
        return newTask;
    }
    private static OutboundEmail__c delinquencyEmail(StudentProgram__c agreement){
       
        EmailTemplate template;
       
        if(agreement.DaysDelinquent__c == 12)
            template = selectTemplate('Delinquency_Letter_11_Days');
        else if(agreement.DaysDelinquent__c == 27 || agreement.DaysDelinquent__c == 42)
            template = selectTemplate('Delinquency_Letter_30_Days');
        else if(agreement.DaysDelinquent__c == 57 || agreement.DaysDelinquent__c == 72)
            template = selectTemplate('Delinquency_Letter_60_Days');
        else if(agreement.DaysDelinquent__c == 87 || agreement.DaysDelinquent__c == 102)
            template = selectTemplate('Delinquency_Letter_90_Days');
        else if(agreement.DaysDelinquent__c == 117 || agreement.DaysDelinquent__c == 132)
            template = selectTemplate('Delinquency_Letter_120_Days');
        else if(agreement.DaysDelinquent__c == 147 || agreement.DaysDelinquent__c == 162 || agreement.DaysDelinquent__c == 177)
            template = selectTemplate('Delinquency_Letter_150_Days');
        else if(agreement.DaysDelinquent__c == 192 || agreement.DaysDelinquent__c == 207)
            template = selectTemplate('Delinquency_Letter_180_Days');
        else if(agreement.DaysDelinquent__c == 222 || agreement.DaysDelinquent__c == 237)
            template = selectTemplate('Delinquency_Letter_210_Days');
        else if(agreement.DaysDelinquent__c == 252)
            template = selectTemplate('Delinquency_Letter_240_Days');
       
        if(template != null){
            return new OutboundEmail__c(ToAddresses__c = agreement.Student__r.PersonEmail,
                                      TemplateID__c = template.id,
                                      WhatID__c = agreement.id,
                                      TargetObjectId__c = agreement.Student__r.PersonContactID,
                                      SendviaSES__c = true);
        }
        else
            return null;
    }
   
    private static EmailTemplate selectTemplate(string templateName){
        try{
            return [Select id from EmailTemplate where developerName =: templateName];            
        } catch (Exception e){
            return null;
        }
    }
   
    //private static Task delinquencyText(StudentProgram__c agreement){
    //  return new Task(Type = 'Other',
    //                        ActivityDate = Date.today(),
    //                        Subject = 'Delinquency Notification: (' + String.valueOf((Integer)agreement.DaysDelinquent__c) + ' Days)',
    //                    WhatID = agreement.Student__c,
    //                    WhoID = agreement.Student__r.PersonContactID,
    //                    Description = 'This student is overdue. Please reach out to the student by text message.');
    //}
   
    private static void handleFinalDisclosure(List<StudentProgram__c> agreements){
        System.debug('GenerateFinalDisclosureBatch.execute()');
        System.debug('session:'+UserInfo.getSessionId());
        system.debug('agreements before:'+agreements);
        for(StudentProgram__c agreement : agreements){
            agreement.CongaFinalDisclosureStatus__c = 'Generate';
            agreement.FinalDisclosureID__c = '';
            agreement.CongaFinalDisclosureID__c = '';
        }
        system.debug('agreements after:'+agreements);
        Database.update(agreements, false);
    }
    private static void handleAudit(List<StudentProgram__c> agreements){
        List<StudentProgramAudit__c> auditToCreate = new List<StudentProgramAudit__c>();
        List<StudentProgram__c> spList = new List<StudentProgram__c>();
        Set<ID> agreementIDs = new Set<ID>();
        String queryString = 'Select Id,';
       
        for(StudentProgram__c agreement:agreements){
            agreementIDs.add(agreement.id);
        }
       
        SObjectType studentProgramAuditDescribe = Schema.getGlobalDescribe().get('StudentProgramAudit__c');
        Map<String,Schema.SObjectField> studProgAudFields = studentProgramAuditDescribe.getDescribe().fields.getMap();
       
        ////generate a dynamic query string to query studentProgram records
        for(String fld : studProgAudFields.keySet()){
            if(fld.endsWith('__c') && fld != 'StudentProgram__c' && fld != 'auditdatetime__c' && fld != 'MonthEnd__c'){
                queryString += fld;
                queryString += ',';    
            }
        }
        queryString = queryString.removeEnd(',');
        queryString += ' From StudentProgram__c Where ID IN ' + DatabaseUtil.inSetStringBuilder(agreementIDs);
       
        spList = Database.query(queryString);
       
        for(StudentProgram__c agreement:spList){
            StudentProgramAudit__c audit = new StudentProgramAudit__c();
            audit.StudentProgram__c = agreement.Id;
            audit.monthEnd__c = true;
            for(String fld : studProgAudFields.keySet()){
                if(fld.endsWith('__c') && fld != 'StudentProgram__c' && fld != 'auditdatetime__c' && fld != 'MonthEnd__c'){
                    audit.put(fld, agreement.get(fld));
                }
            }
            auditToCreate.add(audit);    
        }
       
        if(auditToCreate.size()>0){
            insert auditToCreate;
        }
       
                       
    }    
    private static void handleStatusTransition(List<StudentProgram__c> agreements){
        System.debug('StudentProgramBatch.handleStatusTransition()');
        Map<ID, StudentProgram__c> agreementsToUpdate = new Map<ID, StudentProgram__c>();
        // Set<ID> agreementIDs = new Set<ID>();
        // for(StudentProgram__c agreement : agreements){
        //     agreementIDs.add(agreement.id);
        // }

        // Map<ID, StudentProgramMonthlyStatus__c> monthlyTracker = new Map<ID, StudentProgramMonthlyStatus__c>([SELECT id,
        //                                                                                                             Activity__c,
        //                                                                                                             Sequence__c
        //                                                                                                             Agreement__c
        //                                                                                                      FROM StudentProgramMonthlyStatus__c
        //                                                                                                      WHERE Agreement__c IN :agreementIDs
        //                                                                                                      ORDER BY Sequence__c asc]);
        // Map<ID, List<StudentProgramMonthlyStatus__c>> monthlyTrackersByAgreementID = new Map<ID, List<StudentProgramMonthlyStatus__c>>();
        // for(StudentProgramMonthlyStatus__c tracker : monthlyTracker.values()){
        //     if(!monthlyTrackersByAgreementID.containsKey(tracker.Agreement__c)){
        //         monthlyTrackersByAgreementID.put(tracker.Agreement__c, new List<StudentProgramMonthlyStatus__c>());
        //     }
        //     monthlyTrackersByAgreementID.get(tracker.Agreement__c).add(tracker);
        // }
        for(StudentProgram__c agreement : agreements){
            if(agreement.Status__c == 'Grace' && agreement.GracePeriodEndDate__c < Date.today()){
                agreementsToUpdate.put(agreement.id, new StudentProgram__c(id = agreement.id,
                                                             Status__c = 'Payment'));
            } else if(agreement.Status__c == 'Pause' && agreement.PauseEndDate__c < Date.today()){
                agreementsToUpdate.put(agreement.id, new StudentProgram__c(id = agreement.id,
                                                             Status__c = 'Payment'));
            } else if(agreement.Status__c == 'Deferment' && agreement.DefermentEndDate__c < Date.today()){
                agreementsToUpdate.put(agreement.id, new StudentProgram__c(id = agreement.id,
                                                             Status__c = 'Payment'));
            }
            if(agreement.ServicingStartDate__c < Date.today() && !agreement.Servicing__c){
                if(agreementsToUpdate.containsKey(agreement.id)){
                    agreementsToUpdate.get(agreement.id).Servicing__c = true;
                } else {
                    agreementsToUpdate.put(agreement.id, new StudentProgram__c(id= agreement.id,
                                                                                Servicing__c = true));
                }
            }
        }

        if(agreementsToUpdate.size()>0){
            update agreementsToUpdate.values();
        }

    }
   
    private static void CalculateAmountsOnAgreement(Set<ID> agreementIds){
        //Map<ID, StudentProgram__c> AgreementMapWithID = StudentProgramQueries.getStudentProgramMapWithAgreementID(agreementIds);
        Map<ID, StudentProgram__c> AgreementMapWithID = new Map<ID, StudentProgram__c>([Select id, AmountDisbursedTodate__c, CalculateAmounts__c from StudentProgram__c where ID IN: agreementIds]);
        //Map<ID, List<Transaction__c>> transByAgreementId = TransactionQueries.getTransactionMapByAgreementWithAgreementID(agreementIds, 'Disbursement');
        Map<ID, List<Transaction__c>> transByAgreementId = new Map<ID, List<Transaction__c>>();
        Map<ID, Transaction__c> transById = new Map<ID, Transaction__c>([Select id, Agreement__c, RecordType.Name, Confirmed__c, Status__c, Amount__c  from Transaction__c where Agreement__c IN: agreementIds]);
       
        for(Transaction__c tx: transById.values()){
            if(!transByAgreementId.containsKey(tx.Agreement__c)){
                transByAgreementId.put(tx.Agreement__c, new List<Transaction__c>());
            }
            transByAgreementId.get(tx.Agreement__c).add(tx);
        }
       
        List<StudentProgram__c> agreementsToUpdate = new List<StudentProgram__c>();
        for(studentProgram__c agreement : AgreementMapWithID.values()){
            Decimal DisbursedAmount = 0;
            if(transByAgreementID != null && transByAgreementID.containsKey(agreement.Id)){
                for(Transaction__c tx: transByAgreementID.get(agreement.Id)){
                    if(tx.RecordType.Name == 'Disbursement' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                        if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;  
                    }    
                    else if(tx.RecordType.Name == 'Disbursement Refund' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                        if(tx.Amount__c != null) DisbursedAmount -= tx.Amount__c;  
                         
                    }
                   
                   
                }
            }
            agreementsToUpdate.add(new StudentProgram__c(id = agreement.Id,
                                                         AmountDisbursedToDate__c =  DisbursedAmount,
                                                         CalculateAmounts__c = false));
            //system.debug('Updated:'+agreementsToUpdate);                                                                                                        
        }
        if(agreementsToUpdate.size()>0)
            update agreementsToUpdate;
    }
}