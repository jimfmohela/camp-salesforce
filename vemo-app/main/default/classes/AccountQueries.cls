/////////////////////////////////////////////////////////////////////////
// Class: AccountQueries
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-07-13   Greg Cook       Created                                 
// 2016-12-17   Greg Cook       Added getStudent(), added fields to getStudentMapByID
// 2016-12-20   Greg Cook       Changed all functions to maps, changed function names to overloaded functions
// 2016-12-24   Greg Cook       Added StudentMayByAuthIDWithAuthID, changed function names
// 2017-02-09   Greg Cook       Added filterCriteria
/////////////////////////////////////////////////////////////////////////
public class AccountQueries {
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }

    public static Map<String, Account> getStudentMapByAuthIDWithAuthID(Set<String> authIDs){
        System.debug('AccountQueries.getStudentMapByAuthIDWithAuthID');
        Map<String, Account> acctMapByAuthID = new Map<String, Account>();
        String query = generateSOQLSelect('student');
        query += ' WHERE AuthSystemUserID__pc IN ' + DatabaseUtil.inSetStringBuilder(authIDs);
        query += ' and ' + generateRecordTypeStatement('Student');

        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        Map<ID, Account> retMap = new Map<ID, Account>((List<Account>)db.query(query));

        for(Account acct : retMap.values()){
            acctMapByAuthID.put(acct.AuthSystemUserID__pc, acct);
        }
        return acctMapByAuthID;
    }

    public static Map<ID, Account> getStudentMap(){
        System.debug('AccountQueries.getStudentMap');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');
        query += ' WHERE ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
    }
    
    public static Map<ID, Account> getStudentMapWithPersonEmails(Set<String> personEmails){
        System.debug('AccountQueries.getStudentMapWithPersonEmails');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');
        query += ' WHERE ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' and PersonEmail IN ' + DatabaseUtil.inSetStringBuilder(personEmails)+ ' ';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
    }
    
    public static Map<ID, Account> getStudentMapWithPrimarySchoolStudentIds(Set<String> primarySchoolStudentIds){
        System.debug('AccountQueries.getStudentMapWithPrimarySchoolStudentIds');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');
        query += ' WHERE ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' and PrimarySchoolStudentID__pc IN ' + DatabaseUtil.inSetStringBuilder(primarySchoolStudentIds)+ ' ';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
    }

    public static Map<ID, Account> getStudentMapWithStudentID(Set<ID> studentIDs){
        System.debug('AccountQueries.getStudentMapWithStudentID');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');       
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        query += ' and ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
        
    } 
    
    public static Map<ID, Account> getStudentMapWithSchoolID(Set<String> schoolIDs){
        System.debug('AccountQueries.getStudentMapWithSchoolID');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');       
        query += ' WHERE PrimarySchool__pc IN ' + DatabaseUtil.inSetStringBuilder(schoolIDs);
        query += ' and ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(DatabaseUtil.studentPersonAccountID)+ ' ';
        }
        query += buildFilterString();
        
        query += ' '+ generateLIMITStatement();

        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
        
    }   

    public static Map<ID, Account> getStudentMapWithVemoAccountNumber(Set<string> vemoAccountNos){
        System.debug('AccountQueries.getStudentMapWithVemoAccountNumber');
        Map<ID, Account> acctMap = new Map<ID, Account>();
        String query = generateSOQLSelect('student');
        query += ' WHERE ' + generateRecordTypeStatement('Student');
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' and VemoAccountNumber__c IN ' + DatabaseUtil.inSetStringBuilder(vemoAccountNos)+ ' ';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        return acctMap;     
        
    }  

    ///////////////////////////////////////////////////////////////////////////////////////////
    // School Queries
    ///////////////////////////////////////////////////////////////////////////////////////////
    public static Map<ID, Account> getSchoolMap(){
        Map<ID, Account> schlMap = new Map<ID, Account>();
        String query = generateSOQLSelect('school');        
        query += ' WHERE ' + generateRecordTypeStatement('School - Available Prospect');
        query += 'or ' + generateRecordTypeStatement('School - Customer');
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        schlMap = new Map<ID, Account>((List<Account>)db.query(query));
        return schlMap;     
    }
    
    public static Map<ID, Account> getSchoolCustomerMap(){
        Map<ID, Account> schlMap = new Map<ID, Account>();
        String query = generateSOQLSelect('school');        
        query += ' WHERE ' + generateRecordTypeStatement('School - Customer');
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        schlMap = new Map<ID, Account>((List<Account>)db.query(query));
        return schlMap;     
    }
    public static Map<ID, Account> getSchoolMapWithSchoolID(Set<ID> schoolIDs){
        Map<ID, Account> schlMap = new Map<ID, Account>();
        String query = generateSOQLSelect('school');    
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(schoolIDs);          
        query += ' and ';
        query += '(' + generateRecordTypeStatement('School - Available Prospect');

        query += ' or ' + generateRecordTypeStatement('School - Customer') + ') ';
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        schlMap = new Map<ID, Account>((List<Account>)db.query(query));
        return schlMap;         
    }   

    public static Map<ID, Account> getSchoolMapWithSchoolName(Set<String> schoolNames){
        Map<ID, Account> schlMap = new Map<ID, Account>();
        String query = generateSOQLSelect('school');    
        query += ' WHERE Name IN ' + DatabaseUtil.inSetStringBuilder(schoolNames);  
        query += ' and ';
        query += '(' + generateRecordTypeStatement('School - Available Prospect');

        query += ' or ' + generateRecordTypeStatement('School - Customer') + ') ';
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        schlMap = new Map<ID, Account>((List<Account>)db.query(query));
        return schlMap;     
    }

    public static Map<ID, Account> getSchoolMapWithEntryPoint(Set<String> entryPoints){
        Map<ID, Account> schlMap = new Map<ID, Account>();
        String query = generateSOQLSelect('school');    
        query += ' WHERE EntryPoint__c IN ' + DatabaseUtil.inSetStringBuilder(entryPoints); 
        query += ' and ';
        query += '(' + generateRecordTypeStatement('School - Available Prospect');

        query += ' or ' + generateRecordTypeStatement('School - Customer') + ') ';
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and ID = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        schlMap = new Map<ID, Account>((List<Account>)db.query(query));
        return schlMap;     
    }

    private static String generateSOQLSelect(String type){
        String soql;
        if(type == 'student'){
            soql = 'SELECT ' + getStudentFieldNames() + ' FROM Account';
        }
        else if(type == 'school'){
            soql = 'SELECT ' + getSchoolFieldNames() + ' FROM Account';
        }
        return soql;
    }

    private static String getStudentFieldNames(){
        String fieldNames;
        //Account Fields

        fieldNames = 'id, ';
        fieldNames += 'DoNotContact__c, ';
        fieldNames += 'DueDayOfTheMonth__c, ';
        fieldNames += 'AssessmentDayOfMonth__c, ';
        fieldNames += 'AmountCertifiedToDate__c, ';
        fieldNames += 'AmountDisbursedToDate__c, ';
        fieldNames += 'RecordTypeID, ';
        fieldNames += 'Salutation, ';
        fieldNames += 'FirstName, ';
        fieldNames += 'MiddleName, ';
        fieldNames += 'LastName, ';
        fieldNames += 'Suffix, ';
        //fieldNames += 'SchoolSecurity__c, ';
        fieldNames += 'PersonBirthDate, ';
        fieldNames += 'PersonEmail, ';
        fieldNames += 'PersonDoNotCall, ';
        fieldNames += 'PersonHasOptedOutOfEmail, ';
        fieldNames += 'PersonHomePhone, ';
        fieldNames += 'PersonMailingCity, ';
        fieldNames += 'PersonMailingCountry, ';
        fieldNames += 'PersonMailingCountryCode, ';     
        fieldNames += 'PersonMailingPostalCode, ';
        fieldNames += 'PersonMailingState, ';
        fieldNames += 'PersonMailingStateCode, ';
        fieldNames += 'PersonMailingStreet, ';
        fieldNames += 'PersonOtherCity, ';
        fieldNames += 'PersonOtherCountry, ';
        fieldNames += 'PersonOtherCountryCode, ';       
        fieldNames += 'PersonOtherPostalCode, ';
        fieldNames += 'PersonOtherState, ';
        fieldNames += 'PersonOtherStateCode, ';
        fieldNames += 'PersonOtherStreet, ';
        fieldNames += 'PersonMobilePhone, ';
        fieldNames += 'PersonContactID, ';
        fieldNames += 'PortalPreferences__c, ';
                
        //Contact fields
        fieldNames += 'AuthSystemUserID__pc, ';
        fieldNames += 'AlternateEmail__pc, ';
        fieldNames += 'AlternateAdminEmail__pc, ';
        fieldNames += 'DriversLicenseOrStateID__pc, ';
        fieldNames += 'Residency__pc, ';
        fieldNames += 'PreferredMethodOfCommunication__pc, ';
        fieldNames += 'PreferredName__c, ';
        fieldNames += 'SSNTaxID__pc, ';
        fieldNames += 'HasOptedOutOfText__pc, ';
        fieldNames += 'TimeZone__pc, ';
        fieldNames += 'PrimarySchool__pc, ';
        fieldNames += 'PrimarySchool__pr.Name, ';
        fieldNames += 'PrimarySchool__pr.StudentCampusServiceEmail__c, ';
        fieldNames += 'PrimarySchoolStudentID__pc, ';
        fieldNames += 'PrimarySchoolGraduationDate__pc, ';
        fieldNames += 'PrimarySchoolEnrollmentStatus__pc, ';
        fieldNames += 'PrimarySchoolGradeLevel__pc, ';
        fieldNames += 'SchoolProgramOfStudy__pc, '; 
        fieldNames += 'CommonLineID__pc, '; 
        fieldNames += 'VemoAccountNumber__c, ';
        fieldNames += 'VerifiedAnnualIncome__pc, ';
        fieldNames += 'Credit__pc, ';
        fieldNames += 'AutoPayment__pc, ';
        fieldNames += 'AutoPaymentFrequency__pc, ';
        fieldNames += 'AutoPaymentDayOfMonth1__pc, ';
        fieldNames += 'AutoPaymentDayOfMonth2__pc, ';
        fieldNames += 'AutoPaymentDateActivated__pc, ';
        fieldNames += 'Age__pc, ';  
        fieldNames += 'DateIncomeVerified__pc, ';
        fieldNames += 'StewardshipPaymentDonorGUID__c, ';
        fieldNames += 'StewardshipDisbursementDonorGUID__c, ';
        fieldNames += 'UnpaidFees__c, ';
        fieldNames += 'CumulativeIncomeShare__pc, ';
        fieldNames += 'CumulativeIncomeShareCap__pc, ';
        fieldNames += 'PlaidStatus__pc, ';
        fieldNames += 'UpdatePlaidPassword__pc, ';
        fieldNames += 'PortalUsername__pc ';
                
        return fieldNames;
    }

    private static String getSchoolFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'GUID__c, ';
        fieldNames += 'Name, ';
        fieldNames += 'AmountCertifiedToDate__c, ';
        fieldNames += 'AmountDisbursedToDate__c, ';
        fieldNames += 'BillingStreet, ';
        fieldNames += 'BillingCity, ';
        fieldNames += 'BillingCountry, ';
        fieldNames += 'BillingCountryCode, ';
        fieldNames += 'BillingPostalCode, ';
        fieldNames += 'BillingState, ';
        fieldNames += 'BillingStateCode, ';
        fieldNames += 'CampusServiceName__c, ';
        fieldNames += 'CampusServiceEmail__c, ';
        fieldNames += 'CampusServiceMobile__c, ';
        fieldNames += 'CampusServiceAvailability__c, ';
        fieldNames += 'vemoPhone__c, ';
        fieldNames += 'VemoAccountNumber__c, ';
        fieldNames += 'EntryPoint__c, ';
        fieldNames += 'WebsiteStatus__c, ';
        fieldNames += 'WebStyle__c, ';
        fieldNames += 'DisplayName__c, ';
        fieldNames += 'SchoolLogoURL__c, ';
        fieldNames += 'TitleBlock__c, ';
        fieldNames += 'DescriptionBlock__c, ';
        fieldNames += 'DetailBlock__c, ';
        fieldNames += 'ReportingLink__c, ';
        //fieldNames += 'SchoolSecurity__c, ';
        fieldNames += 'StudentCampusServiceAvailability__c, ';
        fieldNames += 'StudentCampusServiceEmail__c, ';
        fieldNames += 'StudentCampusServiceMobile__c, ';
        fieldNames += 'VemoChatAddress__c, ';
        fieldNames += 'DoNotContact__c, ';
        fieldNames += 'StudentCampusServiceName__c, ';
        fieldNames += 'StudentIDCollected__c, ';
        fieldNames += 'StudentIDRequired__c, ';
        fieldNames += 'ErrorColor__c, ';
        fieldNames += 'ErrorColorContrast__c, ';
        fieldNames += 'PrimaryColor__c, ';
        fieldNames += 'PrimaryColorContrast__c, ';
        fieldNames += 'SuccessColor__c, ';
        fieldNames += 'SuccessColorContrast__c, ';
        fieldNames += 'ActivatePlaid__c ';
        return fieldNames;
    }
    
    private static String generateLIMITStatement(){
        String lim;
        if(DatabaseUtil.getPrimaryResource() == 'student' || DatabaseUtil.getPrimaryResource() == 'school') {
            lim = 'LIMIT '+ DatabaseUtil.getPageSize() + ' OFFSET ' + DatabaseUtil.getOffset();
        }else {
            lim = 'LIMIT 50000';
        }
        return lim;
    }

    private static String generateRecordTypeStatement(String recordTypeLabel){
        ID recordTypeID = (String)GlobalUtil.getRecordTypeIDByLabelName('Account', recordTypeLabel);
        return 'RecordTypeID = \''+ String.valueOf(recordTypeID) + '\'';
    }

    private static String buildFilterString(){
        String filterStr = '';
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +  '\' ');
            }           
        }
        return filterStr;
    }
}