public class ApexJobControl {
    
    @AuraEnabled
    public static List<ApexJobControl__c> getCustomSettingsRecord(){
        string query = 'select id,Name,BatchJob__c from ApexJobControl__c';
        List<ApexJobControl__c> detailList = Database.query(query);
        return detailList;
    }
    
    @AuraEnabled
    public static ApexJobControl__c getdetails(String jobName){
        string scheduleJobName = jobName+'%';
        string scheduleJobQuery = 'select id from CronTrigger where CronJobDetail.Name like \''+scheduleJobName+'\' limit 1';
        List<CronTrigger> checkScheduleJob = new List<CronTrigger>();
        checkScheduleJob = Database.query(scheduleJobQuery);
        
        string query = 'select id,Name,BatchJob__c,BatchJobJobType__c,ScheduleJob__c,Hour__c,Minutes__c,SchedulerActive__c,CronMode__c,CronSchedule__c from ApexJobControl__c where Name like \''+jobName+'\' limit 1';
        ApexJobControl__c detailList = Database.query(query);
        if(checkScheduleJob.size()==0)
            detailList.SchedulerActive__c = false;
        else
            detailList.SchedulerActive__c = true;
            
        if(detailList.Minutes__c == null)
            detailList.Minutes__c = 0;
            
        update detailList;
        return detailList;
    }
    
    @AuraEnabled
    public static string runJobNow(ApexJobControl__c apxjobCtr){
        system.debug(apxjobCtr);
        if (apxjobCtr.BatchJob__c == 'StudentProgramBatch'){
        
            StudentProgramBatch job = new StudentProgramBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'CONTRACT_ASSESSMENT'){
                job.job = StudentProgramBatch.JobType.CONTRACT_ASSESSMENT;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'PURGE_DELETED'){
                job.job = StudentProgramBatch.JobType.PURGE_DELETED;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'MONTH_END_AUDIT'){
                job.job = StudentProgramBatch.JobType.MONTH_END_AUDIT;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'DELINQUENCY'){
                job.job = StudentProgramBatch.JobType.DELINQUENCY;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'GENERATE_FINAL_DISCLOSURE'){
                job.job = StudentProgramBatch.JobType.GENERATE_FINAL_DISCLOSURE;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'STATUS_TRANSITION'){
                job.job = StudentProgramBatch.JobType.STATUS_TRANSITION;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'StudentAccountBatch'){
        
            StudentAccountBatch job = new StudentAccountBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'ANNUAL_STATEMENT_GENERATION'){
                job.job = StudentAccountBatch.JobType.ANNUAL_STATEMENT_GENERATION;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'RECURRING_PAYMENT_GENERATION'){
                job.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'CUMULATIVE_PROGRAM_FUNDING'){
                job.job = StudentAccountBatch.JobType.CUMULATIVE_PROGRAM_FUNDING;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'CLEARINGHOUSE'){
                job.job = StudentAccountBatch.JobType.CLEARINGHOUSE;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'SUMMARIZE_INCOME'){
                job.job = StudentAccountBatch.JobType.SUMMARIZE_INCOME;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'ClrHouseStudentEnrolBatch'){
        
            ClrHouseStudentEnrolBatch job = new ClrHouseStudentEnrolBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'CLEARINGHOUSE'){
                job.job = ClrHouseStudentEnrolBatch.JobType.CLEARINGHOUSE;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'StewardshipACHBatchBatch'){
        
            StewardshipACHBatchBatch job = new StewardshipACHBatchBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'QUERY_APPROVED_OPEN'){
                job.job = StewardshipACHBatchBatch.JobType.QUERY_APPROVED_OPEN;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'TransactionBatch'){
        
            TransactionBatch job = new TransactionBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'PURGE_DELETED_DISBURSEMENTS'){
                job.job = TransactionBatch.JobType.PURGE_DELETED_DISBURSEMENTS;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'OutboundEmailBatch'){
        
            OutboundEmailBatch job = new OutboundEmailBatch ();
            
            if(apxjobCtr.BatchJobJobType__c == 'SEND_EMAIL'){
                job.job = OutboundEmailBatch.JobType.SEND_EMAIL;
            }
            Database.executeBatch(job,1);
        }
        else if (apxjobCtr.BatchJob__c == 'PaymentInstructionBatch'){
        
            PaymentInstructionBatch job = new PaymentInstructionBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'ALLOCATE_PAYMENTS'){
                job.job = PaymentInstructionBatch.JobType.ALLOCATE_PAYMENTS;
                job.proofMode = false;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'SCHEDULE_INBOUND_ACH'){
                job.job = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'SCHEDULE_OUTBOUND_ACH'){
                job.job = PaymentInstructionBatch.JobType.SCHEDULE_OUTBOUND_ACH;
            }
            if(apxjobCtr.BatchJobJobType__c == 'SCHEDULE_INBOUND_ACH'){
                Database.executeBatch(job, 100);                
            } else if(apxjobCtr.BatchJobJobType__c == 'ALLOCATE_PAYMENTS'){
                Database.executeBatch(job, 1); 
            } else {
                Database.executeBatch(job);
            }

        }
        else if (apxjobCtr.BatchJob__c == 'ReconciliationBatch'){
        
            ReconciliationBatch job = new ReconciliationBatch();
            
            /*if(apxjobCtr.BatchJobJobType__c == 'CREATE_AGREEMENTS'){
                job.job = ReconciliationBatch.JobType.CREATE_AGREEMENTS;
            }*/
            if(apxjobCtr.BatchJobJobType__c == 'SEND_INTRO'){
                job.job = ReconciliationBatch.JobType.SEND_INTRO;
            }
            else if(apxjobCtr.BatchJobJobType__c == 'SEND_KICK_OFF'){
                job.job = ReconciliationBatch.JobType.SEND_KICK_OFF;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'ProgramBatch'){
        
            ProgramBatch job = new ProgramBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'DISB_CONF_NOTIFICATION'){
                job.job = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
            }
            Database.executeBatch(job);
        }
        
        else if (apxjobCtr.BatchJob__c == 'LogBatch'){
        
            LogBatch job = new LogBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'PURGE'){
                job.job = LogBatch.JobType.PURGE;
            }
            Database.executeBatch(job);
        }
        else if (apxjobCtr.BatchJob__c == 'EventBatch'){
        
            EventBatch job = new EventBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'INSTANTIATE_EVENTINSTANCE'){
                job.job = EventBatch.JobType.INSTANTIATE_EVENTINSTANCE;
            }
            Database.executeBatch(job, 150);
        }
        else if (apxjobCtr.BatchJob__c == 'EventInstanceBatch'){
        
            EventInstanceBatch job = new EventInstanceBatch();
            
            if(apxjobCtr.BatchJobJobType__c == 'SEND_EVENTINSTANCE_EMAIL'){
                job.job = EventInstanceBatch.JobType.SEND_EVENTINSTANCE_EMAIL;
            }
            Database.executeBatch(job);
        }
        
        update apxjobCtr;
        
        return 'Completed';
    }
    
    @AuraEnabled
    public static string schedule_UnscheduleTheJob(string mode, ApexJobControl__c apxjobCtr, integer minutes){
        if(apxjobCtr.SchedulerActive__c == true){
            minutes = Integer.valueOf(minutes);
            string cronexp;
            apxjobCtr.CronMode__c = mode;
            if(mode.toLowerCase() == 'hourly')
                cronexp = '0 0 * * * ?';
                
            if(mode.toLowerCase() == 'daily')
            {
                if(apxjobCtr.Hour__c == NULL){
                    apxjobCtr.Hour__c = 0;
                }
                cronexp = '0 0 ' + apxjobCtr.Hour__c +' * * ?';
            }
                
            if(apxjobCtr.ScheduleJob__c == 'StudentProgramBatchScheduled'){
                StudentProgramBatchScheduled scheduledJob = new StudentProgramBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'CONTRACT_ASSESSMENT'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.CONTRACT_ASSESSMENT;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'PURGE_DELETED'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'MONTH_END_AUDIT'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.MONTH_END_AUDIT;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'DELINQUENCY'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.DELINQUENCY;
                }
                
                else if(apxjobCtr.BatchJobJobType__c == 'GENERATE_FINAL_DISCLOSURE'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.GENERATE_FINAL_DISCLOSURE;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'STATUS_TRANSITION'){
                    scheduledJob.jobType = StudentProgramBatch.JobType.STATUS_TRANSITION;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'StudentAccountScheduled'){
                StudentAccountScheduled scheduledJob = new StudentAccountScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'RECURRING_PAYMENT_GENERATION'){
                    scheduledJob.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'ANNUAL_STATEMENT_GENERATION'){
                    scheduledJob.job = StudentAccountBatch.JobType.ANNUAL_STATEMENT_GENERATION;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'CUMULATIVE_PROGRAM_FUNDING'){
                    scheduledJob.job = StudentAccountBatch.JobType.CUMULATIVE_PROGRAM_FUNDING;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'CLEARINGHOUSE'){
                    scheduledJob.job = StudentAccountBatch.JobType.CLEARINGHOUSE;
                }
                else if(apxjobCtr.BatchJobJobType__c == 'SUMMARIZE_INCOME'){
                    scheduledJob.job = StudentAccountBatch.JobType.SUMMARIZE_INCOME;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'ClrHouseStudentEnrolBatchScheduled'){
                ClrHouseStudentEnrolBatchScheduled scheduledJob = new ClrHouseStudentEnrolBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'CLEARINGHOUSE'){
                    scheduledJob.jobType = ClrHouseStudentEnrolBatch.JobType.CLEARINGHOUSE;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'OutboundEmailBatchScheduled'){
                OutboundEmailBatchScheduled scheduledJob = new OutboundEmailBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'SEND_EMAIL'){
                    scheduledJob.jobType = OutboundEmailBatch.JobType.SEND_EMAIL;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'StewardshipACHBatchScheduled'){
                StewardshipACHBatchScheduled scheduledJob = new StewardshipACHBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'QUERY_APPROVED_OPEN'){
                    scheduledJob.jobType = StewardshipACHBatchBatch.JobType.QUERY_APPROVED_OPEN;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'PaymentInstructionScheduled'){
                PaymentInstructionScheduled scheduledJob = new PaymentInstructionScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'ALLOCATE_PAYMENTS'){
                    scheduledJob.jobType = PaymentInstructionBatch.JobType.ALLOCATE_PAYMENTS;
                }
                else if (apxjobCtr.BatchJobJobType__c == 'SCHEDULE_INBOUND_ACH'){
                    scheduledJob.jobType = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
                }
                else if (apxjobCtr.BatchJobJobType__c == 'SCHEDULE_OUTBOUND_ACH'){
                    scheduledJob.jobType = PaymentInstructionBatch.JobType.SCHEDULE_OUTBOUND_ACH;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'ReconciliationBatchScheduled'){
                ReconciliationBatchScheduled scheduledJob = new ReconciliationBatchScheduled();
                /*if (apxjobCtr.BatchJobJobType__c == 'CREATE_AGREEMENTS'){
                    scheduledJob.jobType = ReconciliationBatch.JobType.CREATE_AGREEMENTS;
                }*/
                if (apxjobCtr.BatchJobJobType__c == 'SEND_INTRO'){
                    scheduledJob.jobType = ReconciliationBatch.JobType.SEND_INTRO;
                }
                else if (apxjobCtr.BatchJobJobType__c == 'SEND_KICK_OFF'){
                    scheduledJob.jobType = ReconciliationBatch.JobType.SEND_KICK_OFF;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'TransactionBatchScheduled'){
                TransactionBatchScheduled scheduledJob = new TransactionBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'PURGE_DELETED_DISBURSEMENTS'){
                    scheduledJob.jobType = TransactionBatch.JobType.PURGE_DELETED_DISBURSEMENTS;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'ProgramBatchScheduled'){
                ProgramBatchScheduled scheduledJob = new ProgramBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'DISB_CONF_NOTIFICATION'){
                    scheduledJob.jobType = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            
            else if(apxjobCtr.ScheduleJob__c == 'LogBatchScheduled'){
                LogBatchScheduled scheduledJob = new LogBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'PURGE'){
                    scheduledJob.job = LogBatch.JobType.PURGE;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            
            else if(apxjobCtr.ScheduleJob__c == 'EventBatchScheduled'){
                EventBatchScheduled scheduledJob = new EventBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'INSTANTIATE_EVENTINSTANCE'){
                    scheduledJob.jobType = EventBatch.JobType.INSTANTIATE_EVENTINSTANCE;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
            else if(apxjobCtr.ScheduleJob__c == 'EventInstanceBatchScheduled'){
                EventInstanceBatchScheduled scheduledJob = new EventInstanceBatchScheduled();
                if (apxjobCtr.BatchJobJobType__c == 'SEND_EVENTINSTANCE_EMAIL'){
                    scheduledJob.jobType = EventInstanceBatch.JobType.SEND_EVENTINSTANCE_EMAIL;
                }
                System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c, cronexp, scheduledJob);
                
                if(mode.toLowerCase() == 'hourly'){
                    if(minutes == 30){
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                    }
                    if(minutes == 20){
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                    }
                    if(minutes == 10){
                        cronexp = '0 10 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '1', cronexp, scheduledJob);
                        
                        cronexp = '0 20 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '2', cronexp, scheduledJob);
                        
                        cronexp = '0 30 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '3', cronexp, scheduledJob);
                        
                        cronexp = '0 40 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '4', cronexp, scheduledJob);
                        
                        cronexp = '0 50 * * * ?';
                        System.schedule(apxjobCtr.Name + ' ' + apxjobCtr.CronMode__c + '5', cronexp, scheduledJob);
                    }
                }
            }
 
            update apxjobCtr;
            return 'Successfully Scheduled';
        }
        else{
            String JOB_NAME = 'SELECT Id, Name, JobType FROM CronJobDetail WHERE Name like \'%'+apxjobCtr.Name+'%\'';
            List<CronJobDetail> cronDetails = Database.query(JOB_NAME);
            Set<ID> cronTriggerIDs = new Set<ID>();
            for(CronJobDetail cjd : cronDetails){
                cronTriggerIDs.add(cjd.id);
            }
            List<CronTrigger> abort_job = [SELECT id from CronTrigger where CronJobDetailId = :cronTriggerIDs];
            for(CronTrigger ct : abort_job){
                system.abortJob(ct.id);
            }
            update apxjobCtr;
            return 'Successfully Unscheduled';
        }
    }
}