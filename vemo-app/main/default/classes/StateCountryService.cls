public with sharing class StateCountryService {    
	
    public static List<String> getStateOfCountry(String country){
    
        String objectName = 'Account'; 
        String controllingPicklist = 'BillingCountry';
        String dependentPicklist = 'BillingState';
        List<String> availableStates = new List<String>();
        availableStates = PicklistService.getDependentPickListValues(objectName, controllingPicklist, dependentPicklist, country);
        return availableStates;
    }
    
    /*
    public static Map<String, List<String>> getDependentPickListValues(String objectName, String controllingPicklist, String dependentPicklist){
    	Map<String, List<String>> controllingInfo = new Map<String, List<String>>();
    	
    	List<String> availableValues = new List<String>();
    	Schema.DescribeFieldResult controllingFieldInfo;
        Schema.DescribeFieldResult dependentFieldInfo;
        
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();
        
        for(Schema.SObjectField sfield : fieldMap.Values())
        {
            if(string.valueof(sfield).contains(controllingPicklist))
                controllingFieldInfo = sfield.getDescribe();
            if(string.valueof(sfield).contains(dependentPicklist))
                dependentFieldInfo = sfield.getDescribe();
        }
        
        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();
        
        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {            
            controllingInfo.put(currControllingValue.getLabel(), new List<String>());
        }        
        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);
    
            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);
    
            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();
            
            Integer baseCount = 0;
    
            for(Integer curr : hexString.getChars())
            {                
                Integer val = 0;
    
                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }
                
                if((val & 8) == 8)
                {
                    controllingInfo.get(controllingValues[baseCount + 0].getLabel()).add(currDependentValue.getLabel());
                }
                if((val & 4) == 4)
                {
                    controllingInfo.get(controllingValues[baseCount + 1].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 2) == 2)
                {
                    controllingInfo.get(controllingValues[baseCount + 2].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 1) == 1)
                {
                    controllingInfo.get(controllingValues[baseCount + 3].getLabel()).add(currDependentValue.getLabel());                    
                }
    
                baseCount += 4;
            }            
        }
        //system.debug(controllingInfo);
        return controllingInfo;
    }
    
    public class MyPickListInfo
    {
        public String validFor;
    }
    */
}