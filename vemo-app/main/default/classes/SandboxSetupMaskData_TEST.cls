@isTest
public with sharing class SandboxSetupMaskData_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @testSetup static void createSetUpRecord(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        List<Attachment> attList = TestDataFactory.createAttachments(1, students.values()[0].Id);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, PaymentMethod__c> PaymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, Transaction__c> disbursementMap = TestDataFactory.createAndInsertTransactions(1, studentPrgMap, TransactionService.disbursementRecType);
        Map<ID, EmploymentHistory__c> IdVSEmpHistMap = TestDataFactory.createAndInsertEmploymentHistory(1, students);
        }
    }
    
    static testMethod void SandboxSetupMaskDataBatchTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();

        SandboxSetupMaskData job = new SandboxSetupMaskData();
        Database.executeBatch(job, 200);
        //Database.executeBatch(new SandboxSetupMaskData ());

        //Database.executeBatch(new SandboxSetupMaskData ());

    }
}