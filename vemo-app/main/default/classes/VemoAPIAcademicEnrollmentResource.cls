public class VemoAPIAcademicEnrollmentResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v2') && (api.method == 'POST')){
            //return handlePostV2(api);
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
        }
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIAcademicEnrollment.handleGetV2()');
        List<AcademicEnrollmentService.AcademicEnrollment> esList = new List<AcademicEnrollmentService.AcademicEnrollment>();
        
        String studentIDParam = api.params.get('studentID');
        String providerIDParam = api.params.get('providerID');
        String statusParam = api.params.get('status');
        String orderbyParam = api.params.get('orderby');
        if(orderbyParam != null){
            System.debug('orderByParam:'+orderbyParam);
            if(orderbyParam == 'lastName desc' || orderbyParam == 'lastName+desc') DatabaseUtil.orderBy = 'Student__r.LastName desc NULLS LAST';
            else if(orderbyParam == 'lastName asc' || orderbyParam == 'lastName+asc') DatabaseUtil.orderBy = 'Student__r.LastName asc NULLS LAST';
            else if(orderbyParam == 'graduationDate desc' || orderbyParam == 'graduationDate+desc') DatabaseUtil.orderBy = 'GraduationDate__c desc NULLS LAST';
            else if(orderbyParam == 'graduationDate asc' || orderbyParam == 'graduationDate+asc' ) DatabaseUtil.orderBy = 'GraduationDate__c asc NULLS LAST';
            else if(orderbyParam == 'schoolProgramOfStudyID desc' || orderbyParam == 'schoolProgramOfStudyID+desc') DatabaseUtil.orderBy = 'SchoolProgramOfStudy__r.Name desc NULLS LAST';
            else if(orderbyParam == 'schoolProgramOfStudyID asc' || orderbyParam == 'schoolProgramOfStudyID+asc') DatabaseUtil.orderBy = 'SchoolProgramOfStudy__r.Name asc NULLS LAST';
            else if(orderbyParam == 'status desc' || orderbyParam == 'status+desc') DatabaseUtil.orderBy = 'AcademicEnrollmentStatus__c desc NULLS LAST';
            else if(orderbyParam == 'status asc' || orderbyParam == 'status+asc') DatabaseUtil.orderBy = 'AcademicEnrollmentStatus__c asc NULLS LAST';
            System.debug('DatabaseUtil.orderBy:'+DatabaseUtil.orderBy);
        }
        
        
        
        if(studentIDParam != null){
            if(statusParam != null && statusParam != 'null')
                esList = AcademicEnrollmentService.getAcademicEnrollmentMapWithStudentNStatus(VemoApi.parseParameterIntoIDSet(studentIDParam),
                                                                                       VemoApi.parseParameterIntoStringSet(statusParam));
            else
                esList = AcademicEnrollmentService.getAcademicEnrollmentMapWithStudent(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else if(providerIDParam != null){
            if(statusParam != null && statusParam != 'null')
                esList = AcademicEnrollmentService.getAcademicEnrollmentMapWithProviderNStatus(VemoApi.parseParameterIntoIDSet(providerIDParam),
                                                                                         VemoApi.parseParameterIntoStringSet(statusParam));
            else
                esList = AcademicEnrollmentService.getAcademicEnrollmentMapWithProvider(VemoApi.parseParameterIntoIDSet(providerIDParam));
        }
        else 
            throw new VemoAPI.VemoAPIFaultException('Required parameter studentID or providerID');
        
        Set<Id> studentIDs = new Set<Id>();
        Set<Id> providerIDs = new Set<Id>();
        Set<Id> schoolProgramOfStudyIDs = new Set<Id>();
        Set<Id> academicEnrollmentIDs = new Set<Id>();
        
        for(AcademicEnrollmentService.AcademicEnrollment es : esList){
            if(es.studentID != null) studentIDs.add(es.studentID);    
            if(es.providerID != null) providerIDs.add(es.providerID);
            if(es.schoolProgramOfStudyID != null) schoolProgramOfStudyIDs.add(es.schoolProgramOfStudyID);
            academicEnrollmentIDs.add(es.AcademicEnrollmentID);
        }
        
        Map<Id, StudentService.Student> studentWithStudentId = new Map<Id, StudentService.Student>(); 
        List<StudentService.Student> students = StudentService.getStudentsWithStudentID(studentIds);
        for(StudentService.Student student : students){
            studentWithStudentId.put(student.personAccountID, student);
        }
        
        Map<Id, ProgramOfStudyService.ProgramOfStudy> ProgramOfStudyWithPOSIDMap = new Map<Id, ProgramOfStudyService.ProgramOfStudy>(); 
        Map<Id, SchoolProgramsOfStudy__c> sPOSMap = SchoolProgramsOfStudyQueries.getSchoolProgramOfStudyMapWithSPOSID(schoolProgramOfStudyIDs);
        for(SchoolProgramsOfStudy__c sp : sPOSMap.values()){
            ProgramOfStudyWithPOSIDMap.put(sp.id , new ProgramOfStudyService.ProgramOfStudy(sp));
        }
        
        Map<Id, List<VemoAPIAgreementResource.AgreementResourceOutputV2>> AEIdNAgreementsMap = new Map<Id, List<VemoAPIAgreementResource.AgreementResourceOutputV2>>();
        //Map<ID, StudentProgram__c> spMap = StudentProgramQueries.getStudentProgramMapForAErecords(studenTids, providerIds, academicEnrollmentIDs, schoolProgramOfStudyIDs);
        Map<ID, StudentProgram__c> spMap = StudentProgramQueries.getStudentProgramMapForAErecords(studenTids, providerIds, academicEnrollmentIDs);
        
        for(StudentProgram__c sp: spMap.values()){
            if(sp.AcademicEnrollment__c != null){
                if(AEIdNAgreementsMap.get(sp.AcademicEnrollment__c) != null && AEIdNAgreementsMap.get(sp.AcademicEnrollment__c).size()>0){
                    List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreementList = AEIdNAgreementsMap.get(sp.AcademicEnrollment__c);
                    agreementList.add(new VemoAPIAgreementResource.AgreementResourceOutputV2(new AgreementService.Agreement(sp)));
                    AEIdNAgreementsMap.put(sp.AcademicEnrollment__c,agreementList);
                }
                else{
                    List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreementList = new List<VemoAPIAgreementResource.AgreementResourceOutputV2>();
                    agreementList.add(new VemoAPIAgreementResource.AgreementResourceOutputV2(new AgreementService.Agreement(sp)));
                    AEIdNAgreementsMap.put(sp.AcademicEnrollment__c,agreementList);
                }
            }
        }
        
        List<AcademicEnrollmentResourceOutputV2> results = new List<AcademicEnrollmentResourceOutputV2>();
        
        AcademicEnrollmentResourceOutputV2 result;
        StudentService.Student stud;
        ProgramOfStudyService.ProgramOfStudy pos;
        
        for(AcademicEnrollmentService.AcademicEnrollment es : esList){
            
            result = new AcademicEnrollmentResourceOutputV2();
            stud = new StudentService.Student();
            pos = new ProgramOfStudyService.ProgramOfStudy();
            
            if(studentWithStudentId.get(es.StudentID) != null)
                stud = studentWithStudentId.get(es.StudentID);
            
            if(ProgramOfStudyWithPOSIDMap.get(es.schoolProgramOfStudyID) != null)
                pos = ProgramOfStudyWithPOSIDMap.get(es.schoolProgramOfStudyID);
            
            if(GlobalSettings.getSettings().vemoDomainAPI){
                result = new VemoAcademicEnrollmentResourceOutputV2(es, stud, pos);
            }else{
                result = new PublicAcademicEnrollmentResourceOutputV2(es, stud, pos);
            }
            if(AEIdNAgreementsMap.get(result.AcademicEnrollmentID) != null)
                result.agreements.addAll(AEIdNAgreementsMap.get(result.AcademicEnrollmentID));
            results.add(result);
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIAcademicEnrollmentResource.handlePutV2()');   
        List<AcademicEnrollmentService.AcademicEnrollment> aes = new List<AcademicEnrollmentService.AcademicEnrollment>();
        List<AcademicEnrollmentResourceInputV2> aesJSON = (List<AcademicEnrollmentResourceInputV2>)JSON.deserialize(api.body, List<AcademicEnrollmentResourceInputV2>.class);
        
        Set<ID> IDsToVerify = new Set<ID>();
        
        for(AcademicEnrollmentResourceInputV2 aeJSON : aesJSON){
           IDsToVerify.add((ID)aeJSON.academicEnrollmentID);
        }
        Map<Id, AcademicEnrollment__c> VerifiedAcademicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithId(IDsToVerify);
        for(AcademicEnrollmentResourceInputV2 aeJSON : aesJSON){
            if(VerifiedAcademicEnrollmentMap.containsKey((ID)aeJSON.academicEnrollmentID)){
                aeJSON.validatePUTFields();
                aes.add(academicEnrollmentResourceToAcademicEnrollment(aeJSON));
            }
        }       
        Set<Id> AEIDs = AcademicEnrollmentService.updateAcademicEnrollments(aes);
        return (new VemoAPI.ResultResponse(AEIDs, AEIDs.size()));
    }
    
    public static AcademicEnrollmentService.AcademicEnrollment academicEnrollmentResourceToAcademicEnrollment(AcademicEnrollmentResourceInputV2 academicEnrollment){
        AcademicEnrollmentService.AcademicEnrollment ae = new AcademicEnrollmentService.AcademicEnrollment();
        ae.academicEnrollmentID = academicEnrollment.academicEnrollmentID;  
        ae.pendingStatus = academicEnrollment.pendingStatus;
        ae.pendingGrade = academicEnrollment.pendingGrade;
        ae.pendingLastDateOfAttendance = academicEnrollment.pendingLastDateOfAttendance;
        ae.pendingGraduationDate = academicEnrollment.pendingGraduationDate;
        ae.pendingSchoolProgramOfStudyId = academicEnrollment.pendingSchoolProgramOfStudyID;
        //ae.pendingUpdate = academicEnrollment.pendingUpdate;  
        ae.dateOfLastStatusChange  = academicEnrollment.dateOfLastStatusChange; 
        ae.changeRequestedBy = academicEnrollment.changeRequestedBy;
        ae.notesComments = academicEnrollment.notesComments;
        ae.pendingEffectiveDateOfChange = academicEnrollment.pendingEffectiveDateOfChange; 
        ae.pendingLeaveOfAbsenceEndDate = academicEnrollment.pendingLeaveOfAbsenceEndDate;
        ae.pendingLeaveOfAbsenceStartDate = academicEnrollment.PendingLeaveOfAbsenceStartDate;
        return ae;
    }
    
     public class AcademicEnrollmentResourceInputV2{
        public String academicEnrollmentID {get;set;}
        public String pendingStatus {get;set;}
        public String pendingGrade {get;set;}
        public Date pendingLastDateOfAttendance {get;set;}
        public Date pendingGraduationDate {get;set;}
        public String pendingSchoolProgramOfStudyID {get;set;}
        //public Boolean pendingUpdate {get;set;}
        public Date dateOfLastStatusChange {get;set;}
        public String changeRequestedBy {get;set;}
        public String notesComments {get;set;}
        public Date pendingEffectiveDateOfChange {get;set;}
        public Date pendingLeaveOfAbsenceEndDate {get;set;}
        public Date pendingLeaveOfAbsenceStartDate {get;set;}
        
        public AcademicEnrollmentResourceInputV2(){}

        public void validatePUTFields(){
            if(academicEnrollmentID == null) throw new VemoAPI.VemoAPIFaultException('academicEnrollmentID is a required input parameter on PUT');
        }
    }
    
    public virtual class AcademicEnrollmentResourceOutputV2{
        public String academicEnrollmentID{get;set;}
        public String grade{get;set;}
        public date graduationDate{get;set;}
        public Date lastDateofAttendance {get;set;}
        public String providerID{get;set;}        
        public String status{get;set;}
        public String studentID {get;set;}
        public String schoolProgramOfStudyID{get;set;}
        //public Boolean pendingUpdate {get;set;}
        public String pendingStatus {get;set;}
        public String pendingGrade {get;set;}
        public Date pendingGraduationDate {get;set;}
        public Date pendingLastDateOfAttendance {get;set;}
        public String pendingSchoolProgramOfStudyID {get;set;}
        public Boolean recordLocked {get;set;}
        public Date dateOfLastStatusChange {get;set;}
        public String changeRequestedBy {get;set;}
        public String notesComments {get;set;}
        public Date dateOfLastApprovedChange {get;set;}
        public Date pendingEffectiveDateOfChange {get;set;}
        public Date pendingLeaveOfAbsenceStartDate {get;set;}
        public Date pendingLeaveOfAbsenceEndDate {get;set;}
        public Date effectiveDateofChange {get;set;}
        public Date leaveofAbsenceStartDate {get;set;}
        public Date leaveofAbsenceEndDate {get;set;}
        
        // Student's Attributes
        public String firstName {get;set;}
        public String middleName {get;set;}
        public String lastName {get;set;}
        public Date birthdate {get;set;}
        public String email {get;set;}
        public String mobilePhone {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String postalCode {get;set;}
        public String state {get;set;}
        public String street {get;set;}
        public String stateCode {get;set;}
        public String vemoAccountNumber {get;set;} 
        public Decimal amountCertifiedToDate {get;set;} 
        public Decimal amountDisbursedToDate {get;set;}
        public String ssnTaxID {get;set;} 
        public String alternateServicingEmail {get;set;} 
            
        // POS Attributes    
        public String programOfStudyID {get;set;}    
        //public String schoolProgramOfStudyID {get;set;}    
        //public String schoolID {get;set;}
        public String definition {get;set;}
        public String description {get;set;}             
            
        //public VemoAPIStudentResource.StudentResourceOutputV2 studentv2 = new VemoAPIStudentResource.StudentResourceOutputV2();
        public List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreements = new List<VemoAPIAgreementResource.AgreementResourceOutputV2>();
        //public VemoAPIProgramOfStudyResource.ProgramOfStudyResourceOutputV2 programOfStudy = new VemoAPIProgramOfStudyResource.ProgramOfStudyResourceOutputV2();
        
        public AcademicEnrollmentResourceOutputV2(){}
        
        public AcademicEnrollmentResourceOutputV2(AcademicEnrollmentService.AcademicEnrollment ae, 
                                                    StudentService.Student stud, ProgramOfStudyService.ProgramOfStudy pos){
            this.academicEnrollmentID = ae.AcademicEnrollmentID;
            this.grade = ae.grade;
            this.graduationDate = ae.graduationDate;
            this.providerID = ae.providerID;
            this.status = ae.status;
            this.studentID = ae.studentID;
            this.schoolProgramOfStudyID = ae.schoolProgramOfStudyID;
            this.lastDateofAttendance = ae.lastDateofAttendance ;
            //this.pendingUpdate = ae.pendingUpdate;
            this.pendingStatus = ae.pendingStatus;
            this.pendingGrade = ae.pendingGrade;
            this.pendingGraduationDate = ae.pendingGraduationDate;
            this.pendingLastDateOfAttendance = ae.pendingLastDateOfAttendance;
            this.pendingSchoolProgramOfStudyID = ae.pendingSchoolProgramOfStudyID;
            this.dateOfLastStatusChange = ae.dateOfLastStatusChange;
            this.recordLocked = ae.recordLocked;  
            this.changeRequestedBy = ae.ChangeRequestedBy;
            this.notesComments = ae.notesComments;
            this.dateOfLastApprovedChange = ae.dateOfLastApprovedChange; 
            this.pendingEffectiveDateOfChange = ae.pendingEffectiveDateOfChange;
            this.pendingLeaveOfAbsenceEndDate = ae.pendingLeaveOfAbsenceEndDate;
            this.pendingLeaveOfAbsenceStartDate = ae.pendingLeaveOfAbsenceStartDate;
            this.effectiveDateofChange = ae.effectiveDateofChange;
            this.leaveofAbsenceStartDate = ae.LeaveofAbsenceStartDate;
            this.leaveofAbsenceEndDate = ae.leaveofAbsenceEndDate;
                       
            this.firstName = stud.firstName;
            this.middleName = stud.middleName;
            this.lastName = stud.lastName;
            this.birthdate = stud.birthdate;
            this.email = stud.email;
            this.mobilePhone = stud.mobilePhone;
            this.city = stud.mailingCity;
            this.country = stud.mailingCountry;
            this.postalCode = stud.mailingPostalCode;
            this.state = stud.mailingState;
            this.street = stud.mailingStreet;
            this.stateCode = stud.mailingStateCode;         
            this.vemoAccountNumber = stud.vemoAccountNumber;
            this.amountCertifiedToDate = stud.amountCertifiedToDate;
            this.amountDisbursedToDate= Stud.amountDisbursedToDate;
            this.ssnTaxID = stud.ssnTaxID;
            this.alternateServicingEmail = stud.alternateServicingEmail; 
            
            this.programOfStudyID = pos.programOfStudyID;
            //this.schoolProgramOfStudyID = pos.schoolProgramOfStudyID;
            //this.schoolID = pos.schoolID;
            this.definition = pos.definition;
            this.description = pos.description;
        }
    }
    
    public class PublicAcademicEnrollmentResourceOutputV2 extends AcademicEnrollmentResourceOutputV2{
        public PublicAcademicEnrollmentResourceOutputV2(){}
        public PublicAcademicEnrollmentResourceOutputV2(AcademicEnrollmentService.AcademicEnrollment aEnrol, 
                                                            StudentService.Student stud, ProgramOfStudyService.ProgramOfStudy pos){
            super(aEnrol, stud, pos);
        }
    }

    public class VemoAcademicEnrollmentResourceOutputV2 extends AcademicEnrollmentResourceOutputV2{
                
        public VemoAcademicEnrollmentResourceOutputV2(){}        
        public VemoAcademicEnrollmentResourceOutputV2(AcademicEnrollmentService.AcademicEnrollment aEnrol, 
                                                        StudentService.Student stud, ProgramOfStudyService.ProgramOfStudy pos){
            super(aEnrol, stud, pos);
        }
    }
}