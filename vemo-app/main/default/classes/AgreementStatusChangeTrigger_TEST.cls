@isTest
public with sharing class AgreementStatusChangeTrigger_TEST {
    @isTest
    public static void validateInsertTrigger() {
        AgreementStatusChange__e newEvent = new AgreementStatusChange__e();
        newEvent.FromStatus__c = 'Test';
        newEvent.StudentProgram__c = 'Test';
        newEvent.ToStatus__c = 'Test';
        Test.startTest();
        Database.SaveResult sr = EventBus.publish(newEvent);
        Test.stopTest();
    }
}