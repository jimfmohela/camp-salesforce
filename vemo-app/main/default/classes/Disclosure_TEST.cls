/////////////////////////////////////////////////////////////////////////
// Class: DisclosureTest
// 
// Description: 
//  Test class for DisclosureService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-03-13   Greg Cook       Created                          
/////////////////////////////////////////////////////////////////////////
@isTest
private class Disclosure_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> testAccounts = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, testAccounts);
        Map<ID, Program__c> testPrograms = TestDataFactory.createAndInsertPrograms(1, testAccounts);
        Map<ID, Program_Config__c> testProgramConfigs = TestDataFactory.createAndInsertProgramConfig(1, testPrograms);
        TestDataFactory.createAndInsertProgramConfigIncomes(testProgramConfigs);
        TestDataFactory.createAndInsertProgramConfigLoans(testProgramConfigs);
        
        Map<ID, Account> testStudents = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, StudentProgram__c> testStudentPrograms = TestDataFactory.createAndInsertStudentProgram(1, testStudents, testPrograms);
        }
    }
    
    @isTest private static void myUnitTest() {
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        List<StudentProgram__c> testStudentPrograms = StudentProgramQueries.getStudentProgramMap().values();
        Test.startTest();
            //test StudentProgramTriggerHandler trigger
            for(StudentProgram__c studentProgram : testStudentPrograms){
                studentProgram.FundingAmountPostCertification__c = 20000;
                //studentProgram.RequestedAmount__c = 10000;
                studentProgram.IncomeSharePostCertification__c = 3;
                studentProgram.PaymentTermPostCertification__c = 92;
                studentProgram.PaymentCapPostCertification__c = 25000;
            }
            update testStudentPrograms;
            
            //testStudentPrograms = [Select ID, Program__c, FundingAmountPostCertification__c, IncomeSharePostCertification__c, PaymentTermPostCertification__c, PaymentCapPostCertification__c From StudentProgram__c];
            
            //test DisclosureService.Calculation
            testStudentPrograms[0].PaymentCap__c = null;
            DisclosureService.Calculation(testStudentPrograms);
            
            //test no Funding Amount, line 78;
            testStudentPrograms[0].FundingAmountPostCertification__c = null;
            //testStudentPrograms[0].RequestedAmount__c = null;
            DisclosureService.Calculation(testStudentPrograms);
            //test PMT
            DisclosureService.PMT(null, null, null);
        Test.stopTest();
    }
}