public class EventInstanceQueries {
        
    public static Map<ID, EventInstance__c> getEventInstanceMapForAuthUser(set<Id> contactIDSet){
        
        Map<ID, EventInstance__c> EventInstanceMap = new Map<ID, EventInstance__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Contact__c IN ' + DatabaseUtil.inSetStringBuilder(contactIDSet);
        
        DatabaseUtil.orderBy = 'Event__r.CreatedDate desc';
        query += DatabaseUtil.generateOrderByStatement(); 
        query += ' '+ generateLIMITStatement();
        
        DatabaseUtil db = new DatabaseUtil();
        EventInstanceMap = new Map<ID, EventInstance__c>((List<EventInstance__c>)db.query(query));
        return EventInstanceMap;  
    }
    
    public static Map<ID, EventInstance__c> getEventInstanceMapForAuthUser(set<Id> contactIDSet, boolean read){
        
        Map<ID, EventInstance__c> EventInstanceMap = new Map<ID, EventInstance__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Contact__c IN ' + DatabaseUtil.inSetStringBuilder(contactIDSet);
        query += ' and read__c = '+String.valueOf(read);
        
        DatabaseUtil.orderBy = 'Event__r.CreatedDate desc';
        query += DatabaseUtil.generateOrderByStatement(); 
        query += ' '+ generateLIMITStatement();
        
        DatabaseUtil db = new DatabaseUtil();
        EventInstanceMap = new Map<ID, EventInstance__c>((List<EventInstance__c>)db.query(query));
        return EventInstanceMap;  
    }
    
    public static Map<ID, EventInstance__c> getEventInstanceMapWithEventInstanceID(set<Id> eventInstanceIDSet){
        
        Map<ID, EventInstance__c> EventInstanceMap = new Map<ID, EventInstance__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(eventInstanceIDSet);
        
        DatabaseUtil.orderBy = 'CreatedDate desc';
        query += DatabaseUtil.generateOrderByStatement(); 
        query += ' '+ generateLIMITStatement();
        
        DatabaseUtil db = new DatabaseUtil();
        EventInstanceMap = new Map<ID, EventInstance__c>((List<EventInstance__c>)db.query(query));
        return EventInstanceMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM EventInstance__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        
        fieldNames = 'id, ';
        fieldNames += 'read__c, ';
        fieldNames += 'Contact__c, ';
        fieldNames += 'Email__c, ';
        fieldNames += 'OutboundCreated__c, ';
        fieldNames += 'EventAttributes__c, ';
        fieldNames += 'Event__c, ';
        fieldNames += 'WhatID__c, ';
        fieldNames += 'ObjectType__c, ';
        fieldNames += 'EventType__c, ';
        fieldNames += 'CreatedDate';
        return fieldNames;
    }
   
    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
}