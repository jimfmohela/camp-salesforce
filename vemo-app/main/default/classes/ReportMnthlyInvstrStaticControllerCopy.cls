public with sharing class ReportMnthlyInvstrStaticControllerCopy{
    public List<primaryOwnerWrapper> primaryOwners {get;set;}
    public List<fundOwnerWrapper> fundOwners {get;set;}
    public List<paymentStreamOwnerWrapper> paymentStreamOwners {get;set;}
    public String selectedSchool {get;set;}
    transient public String school {get; set;}
    transient public String fundOwner {get; set;}
    transient public String primaryOwner {get; set;}
    transient public String paymentStreamOwner {get; set;}
    transient public List<reportDataWrapper> reportData {get;set;}   
    //transient public String csv {get;set;}
    private Integer currentMonth;
    private Integer currentYear;
    public Set<Id> selectedPrimaryOwners {get;set;}
    public Set<Id> selectedFundOwners {get;set;}
    public Set<Id> selectedPaymentStreamOwners {get;set;}
    
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportMnthlyInvstrStaticControllerCopy(){
        selectedSchool = '';
        reportData = new List<reportDataWrapper>();    
        currentMonth = Date.today().month();
        currentYear = Date.today().year();
        primaryOwners = new List<primaryOwnerWrapper>();
        fundOwners = new List<fundOwnerWrapper>();
        paymentStreamOwners = new List<paymentStreamOwnerWrapper>();
        getOwners();
        
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    public List<SelectOption> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        List<SelectOption> output  = new List<SelectOption>();
        for(Account sch:result){
            output.add(new SelectOption(sch.id,sch.name));    
        }
        return output; 
    }
    
    ///////////////////////////////
    ///Get Owners
    ///////////////////////////////
    public void getOwners(){
        //List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By name ASC];
        Map<ID, Account> acctMap = new Map<ID, Account>();
        string query ='Select id,name From Account ';
        query += ' WHERE ' + generateRecordTypeStatement('Investor');
        query += ' '+ 'Order By name ASC';
        
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        
        for(Account acc: acctMap.values()){
            primaryOwners.add(new primaryOwnerWrapper(false,acc));
            fundOwners.add(new fundOwnerWrapper(false,acc));
            paymentStreamOwners.add(new paymentStreamOwnerWrapper(false,acc));    
        }
        primaryOwners.add(new primaryOwnerWrapper(false,null));
        fundOwners.add(new fundOwnerWrapper(false,null));
        paymentStreamOwners.add(new paymentStreamOwnerWrapper(false,null));
        system.debug('primary Owners: '+primaryOwners);
        //return primaryOwners;
    }
    
    
    
    private static String generateRecordTypeStatement(String recordTypeLabel){
        ID recordTypeID = (String)GlobalUtil.getRecordTypeIDByLabelName('Account', recordTypeLabel);
        return 'RecordTypeID = \''+ String.valueOf(recordTypeID) + '\'';
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
    
        school = [select name from Account where id=:selectedSchool].name;
        primaryOwner = '';
        fundOwner = '';
        paymentStreamOwner = '';
        selectedPrimaryOwners = new Set<Id>();
        for(primaryOwnerWrapper po:primaryOwners){
            if(po.selected == true){
                if(po.primaryOwner != null){
                    selectedPrimaryOwners.add(po.primaryOwner.id);
                    primaryOwner += po.primaryOwner.Name +',';
                }
                else{
                    selectedPrimaryOwners.add(null);
                }
            }
        }
        primaryOwner = primaryOwner.removeEnd(',');
        selectedfundOwners = new Set<Id>();
        for(fundOwnerWrapper fo:fundOwners){
            if(fo.selected == true){ 
                if(fo.fundOwner != null){
                        selectedFundOwners.add(fo.fundOwner.id);
                        fundOwner += fo.fundOwner.Name +',';
                }
                else{
                    selectedFundOwners.add(null);
                }
            }
        }
        fundOwner = fundOwner.removeEnd(',');
        selectedPaymentStreamOwners = new Set<Id>();
        for(paymentStreamOwnerWrapper pso:paymentStreamOwners){
            if(pso.selected == true){
                if(pso.paymentStreamOwner != null){
                        selectedPaymentStreamOwners.add(pso.paymentStreamOwner.id);
                        paymentStreamOwner += pso.paymentStreamOwner.Name +',';
                }
                else{
                    selectedPaymentStreamOwners.add(null);
                }
            }
        }
        paymentStreamOwner = paymentStreamOwner.removeEnd(',');
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //Final output Map : Program Id is the key 
        Map<String,ReportDataWrapper> outputMap = new Map<String,ReportDataWrapper>();  
        
        //Map of all agreements
        Map<ID,StudentProgram__c> spMap = new Map<ID,StudentProgram__c>();     
        
        //Map of Agreements sorted by Program
        Map<ID,List<StudentProgram__c>> programAgreementMap = new Map<ID,List<StudentProgram__c>>(); 
        
        //Map of Allocated amount by Agreement
        Map<ID,Decimal> allocationByAgreement = new Map<ID,Decimal>();
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //get all agreements for selected school
        spMap = getAgreements();
        
        //sort the agreements by Program 
        sortAgreements(spMap,programAgreementMap);
        
        //get the payment allocation data
        if(spMap.KeySet().size()>0){
            getPaymentData(spMap,allocationByAgreement);    
        }
        
        //count original and remaining ISAs
        summarizeIsaData(programAgreementMap,allocationByAgreement,outputMap);
        
        
        //calculate percentages
        calculatePercentages(outputMap);
        
        reportData = outputMap.values();
    }
    
    
    //////////////////////////////////////////////
    ///Get All agreements for the selected School
    //////////////////////////////////////////////
    private Map<ID,StudentProgram__c> getAgreements(){
        
        List<StudentProgram__c> spList = queryMethod();
        
        Map<ID,StudentProgram__c> spMap = new Map<ID,StudentProgram__c>();
        for(StudentProgram__c sp: spList){
            if(sp.Transactions__r.size()>0){
                spMap.put(sp.Id,sp);
            }
        }
        return spMap;
    }
    
    ///////////////////////////////////////////////////
    ///sort the agreements by program
    ////////////////////////////////////////////////////
    private void sortAgreements(Map<ID,StudentProgram__c> spMap,Map<ID,List<StudentProgram__c>> programAgreementMap){
        for(StudentProgram__c sp:spMap.values()){            
            if(!programAgreementMap.containsKey(sp.program__c)){
                programAgreementMap.put(sp.program__c,new List<StudentProgram__c>());
            }
            programAgreementMap.get(sp.program__c).add(sp);
            
        }
    }
    
    ////////////////////////////////////////////////////////////////
    ///Description: Get the Payment Data
    ////////////////////////////////////////////////////////////////
    private void getPaymentData(Map<ID,StudentProgram__c> spMap,Map<ID,Decimal> allocationByAgreement){
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        
        AggregateResult[] groupedResults = [SELECT agreement__c agrID,sum(AmountAllocated__c) 
                                             FROM PaymentAllocation__c
                                             WHERE agreement__c IN :spMap.keySet() AND createdDate < :dt
                                             GROUP BY agreement__c];    
    
        for(AggregateResult ar:groupedResults){
            
            String agreementID = (String)ar.get('agrID');
            Decimal sum = (Decimal)ar.get('expr0'); 
            
            allocationByAgreement.put(agreementID,sum);
        }
    }
    
    /////////////////////////////////////////////////////////////////
    ///Description: get the monthly tracker data
    /////////////////////////////////////////////////////////////////
    /*private void getMonthlyTracker(Map<ID,StudentProgram__c> spMap,Map<String,ReportDataWrapper> outputMap){
        Integer month = Date.today().month();
        Integer year = Date.today().year();
        if(month == 1){
            month = 12;
            year -= 1;
        } 
        else month -= 1;
        String monthName = getMonthName(month);
        String yearName = String.ValueOf(year);
        
        /************************************ 90+ Delinquent *******************************************************
        AggregateResult[] groupedResults90Plus = [SELECT agreement__r.program__c prgID,count(id) 
                                            FROM StudentProgramMonthlyStatus__c
                                            WHERE agreement__c IN :spMap.keySet() AND activity__c = 'Payment'
                                            AND month__c = :monthName AND year__c = :yearName AND agreement__r.daysDelinquent__c >= 90
                                            GROUP BY agreement__r.program__c
                                           ];
        
        for(AggregateResult ar: groupedResults90Plus){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).isaNinetyPlusDel = count;
            }
        }
        /************************************************************************************************************        
        
        /************************************ 30 - 89 Delinquent *******************************************************
        AggregateResult[] groupedResults30To89 = [SELECT agreement__r.program__c prgID,count(id) 
                                            FROM StudentProgramMonthlyStatus__c
                                            WHERE agreement__c IN :spMap.keySet() AND activity__c = 'Payment'
                                            AND month__c = :monthName AND year__c = :yearName AND 
                                            agreement__r.daysDelinquent__c < 90 AND agreement__r.daysDelinquent__c >= 30
                                            GROUP BY agreement__r.program__c
                                           ];
        
        for(AggregateResult ar: groupedResults30To89){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).isaThirtyToEightyNineDel = count;
            }
        }
        /************************************************************************************************************        
        
        /********************************************* On Time *******************************************************
        AggregateResult[] groupedResultsOnTime = [SELECT agreement__r.program__c prgID,count(id) 
                                            FROM StudentProgramMonthlyStatus__c
                                            WHERE agreement__c IN :spMap.keySet() AND activity__c = 'Payment'
                                            AND month__c = :monthName AND year__c = :yearName AND agreement__r.daysDelinquent__c < 30
                                            GROUP BY agreement__r.program__c
                                           ];
        
        for(AggregateResult ar: groupedResultsOnTime){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).isaOnTimePayment = count;
            }
        }
        /************************************************************************************************************        
        
        /************************************ ISA in School *******************************************************
        AggregateResult[] groupedResultsSchool = [SELECT agreement__r.program__c prgID,count(id) 
                                                  FROM StudentProgramMonthlyStatus__c
                                                  WHERE agreement__c IN :spMap.keySet() 
                                                  AND (activity__c = 'Partially Funded' OR activity__c = 'Fully Funded' OR activity__c = 'Early Pay')
                                                  AND month__c = :monthName AND year__c = :yearName
                                                  GROUP BY agreement__r.program__c
                                                  ];
        
        for(AggregateResult ar: groupedResultsSchool){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).countInSchoolISA = count;
            }
        }
        /************************************************************************************************************
        
        /************************************ ISA in Grace *******************************************************
        AggregateResult[] groupedResultsGrace = [SELECT agreement__r.program__c prgID,count(id) 
                                                   FROM StudentProgramMonthlyStatus__c
                                                   WHERE agreement__c IN :spMap.keySet() AND activity__c = 'Grace'
                                                   AND month__c = :monthName AND year__c = :yearName
                                                   GROUP BY agreement__r.program__c
                                                   ];
        
        for(AggregateResult ar: groupedResultsGrace){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).countInGraceISA = count;
            }
        }
        /************************************************************************************************************
        
        /********************************************** No Payment Due**********************************************
        AggregateResult[] groupedResultsNoPaymentDue = [SELECT agreement__r.program__c prgID,count(id) 
                                                           FROM StudentProgramMonthlyStatus__c
                                                           WHERE agreement__c IN :spMap.keySet() AND 
                                                           (activity__c = 'Deferment' OR activity__c = 'Pause' OR
                                                            activity__c = 'School' OR activity__c = 'Leave of Absence' OR 
                                                            activity__c = 'Internship' OR activity__c = 'Refinanced' OR 
                                                            activity__c = 'Dispute') OR (activity__c = 'Payment' OR AND agreement__r.AmountDueToDate__c = 0)
                                                            AND month__c = :monthName AND year__c = :yearName 
                                                            GROUP BY agreement__r.program__c
                                                            ];
        
        for(AggregateResult ar: groupedResultsNoPaymentDue){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).isaNoPaymentDue = count;
            }
        }
        /************************************************************************************************************
    }*/
    
    ////////////////////////////////////////////////////////
    ///Description: Sort the agreements by program, agreements should be included based on the status as of last day of previous month
    ///till it has been closed Or moved into Pending Reconciliation Status
    ////////////////////////////////////////////////////////
    private void summarizeIsaData(Map<ID,List<StudentProgram__c>> programAgreementMap,Map<ID,Decimal> allocationByAgreement,Map<String,ReportDataWrapper> outputMap){
        for(ID programID:programAgreementMap.keySet()){
            if(!outputMap.containsKey(programID)){
                outputMap.put(programID,new ReportDataWrapper());
            }
            outputMap.get(programID).originalIsaAssests = programAgreementMap.get(programID).size();
            outputMap.get(programID).programName = programAgreementMap.get(programID)[0].program__r.programName__c;
            outputMap.get(programID).programNumber = programAgreementMap.get(programID)[0].program__r.name;
            //integer total = 0;
            //integer cancelled = 0;
            //Map<String,Integer> m = new Map<String,Integer>();
            for(StudentProgram__c agreement:programAgreementMap.get(programID)){
                if(agreement.fundingAmountPostCertification__c <> null){
                    outputMap.get(programID).sumOfFundingAmount += agreement.fundingAmountPostCertification__c;
                    if(agreement.Status__c == 'Forgiven' || agreement.Status__c == 'Cancelled' || agreement.Status__c == 'Closed')                    
                        outputMap.get(programID).sumOfCancelledAmount += agreement.fundingAmountPostCertification__c;    
                }
                if(agreement.paymentCapPostCertification__c <> null) outputMap.get(programID).sumOfPaymentCap += agreement.paymentCapPostCertification__c;
                if(agreement.incomeSharePostCertification__c <> null) outputMap.get(programID).sumOfIncomeShare += agreement.incomeSharePostCertification__c;
                if(agreement.paymentTermPostCertification__c <> null) outputMap.get(programID).sumOfPaymentTerm  += agreement.paymentTermPostCertification__c;
                
                Date lastDateOfPreviousMonth = Date.newinstance(Date.today().year(),Date.today().month(),1).addDays(-1);
                
                if(agreement.certificationDate__c.Date() <= lastDateOfPreviousMonth){
                    
                    
                    if(allocationByAgreement.containsKey(agreement.id)){
                        outputMap.get(programID).sumPayments += allocationByAgreement.get(agreement.id); 
                    }
                    
                    if((agreement.status__c == 'Cancelled' || agreement.status__c == 'Pending Reconciliation' || agreement.status__c == 'Closed') && agreement.CurrentStatusDate__c <> null){
                        if(agreement.status__c == 'Cancelled' || (agreement.status__c == 'Closed' && (agreement.ClosedReason__c == 'Forgiven' || agreement.ClosedReason__c == 'Refinanced' || agreement.ClosedReason__c == 'Transferred servicer')))
                            outputMap.get(programID).cancelledIsaAssets  += 1;
                        if((agreement.status__c == 'Closed' && agreement.ClosedReason__c == 'Contract satisfied') || agreement.status__c == 'Pending Reconciliation'){                       
                            outputMap.get(programID).contractuallySatisfiedIsaAssets  += 1;
                            if(agreement.certificationDate__c.Date() <= lastDateOfPreviousMonth){
                    
                                if(agreement.MonthlyAmountPaidToDate__c >= agreement.PaymentCapPostCertification__c && agreement.PaymentTermRemaining__c >0)
                                    outputMap.get(programID).byPayingThePaymentCap += 1;
                                    
                                if(agreement.PaymentTermRemaining__c == 0 && agreement.DefermentMonthsRemaining2__c > 0)
                                    outputMap.get(programID).byPayingTheMaximumNumberOfMonthlyPayments += 1;
                                    
                                if(agreement.PaymentTermRemaining__c == 0 && agreement.DefermentMonthsRemaining2__c == 0)
                                    outputMap.get(programID).byReachingTheEndOfThePaymentTerm += 1;
                            }
                        }
                        if(agreement.status__c == 'Closed' && (agreement.ClosedReason__c == 'Bankruptcy' || agreement.ClosedReason__c == 'Write off' || agreement.ClosedReason__c == 'Default') || agreement.status__c == 'Default')                        
                            outputMap.get(programID).defaultIsaAssets  += 1;
                        
                        if(agreement.CurrentStatusDate__c <= lastDateOfPreviousMonth){
                            continue;
                        }
                    }
                    else{                        
                        outputMap.get(programID).remainingISAAssets += 1;
                        if(agreement.status__c == 'Partially Funded' || agreement.status__c == 'Fully Funded' || agreement.status__c == 'Early Pay' || agreement.status__c == 'School' || agreement.status__c == 'Leave of Absence' || agreement.status__c == 'Internship'){
                            outputMap.get(programID).countInSchoolISA += 1;
                        }
                        if(agreement.status__c == 'Grace'){
                            outputMap.get(programID).countInGraceISA += 1;
                        }
                        if((agreement.status__c == 'Bankruptcy' || agreement.status__c == 'Deferment' || agreement.status__c == 'Pause' || agreement.status__c == 'Dispute') || (agreement.status__c == 'Payment' && (agreement.AmountDueToDate__c == 0 || agreement.AmountDueToDate__c == null))){
                            outputMap.get(programID).isaNoPaymentDue += 1;
                        }
                        if(agreement.status__c == 'Payment' && agreement.daysDelinquent__c >= 90){
                            outputMap.get(programID).isaNinetyPlusDel += 1;    
                        }
                        if(agreement.status__c == 'Payment' && agreement.daysDelinquent__c < 90 && agreement.daysDelinquent__c >= 30){
                            outputMap.get(programID).isaThirtyToEightyNineDel += 1;    
                        }
                        if(agreement.status__c == 'Payment' && agreement.daysDelinquent__c < 30 && agreement.AmountDueToDate__c > 0){
                            outputMap.get(programID).isaOnTimePayment += 1;    
                        }  
                    }
                }
                
                
                
                
                
            }
            
            
            //calculate averages
            if(outputMap.get(programID).originalIsaAssests>0){
                outputMap.get(programID).avgIncomeShare = (outputMap.get(programID).sumOfIncomeShare/outputMap.get(programID).originalIsaAssests).setScale(2);
                outputMap.get(programID).avgPaymentTerm = (outputMap.get(programID).sumOfPaymentTerm/outputMap.get(programID).originalIsaAssests).setScale(2);
            }
        }    
    }
    
    /////////////////////////////////////////////////
    ///Calculate percentages
    /////////////////////////////////////////////////
    private void calculatePercentages(Map<String,ReportDataWrapper> outputMap){
        for(String programID:outputMap.keySet()){
            if(outputMap.get(programID).originalIsaAssests>0){
               System.debug('@@@FLAG1');
               Decimal origIsa = outputMap.get(programID).originalIsaAssests;
               outputMap.get(programID).remainingIsapercentOrig = ((outputMap.get(programID).remainingIsaAssets/origIsa)*100).setScale(2);
               outputMap.get(programID).inPayIsaPercentOrig = ((outputMap.get(programID).countInPaymentISA/origIsa)*100).setScale(2); 
            }
            if(outputMap.get(programID).sumOfFundingAmount>0){
               outputMap.get(programID).sumPmntsPercentFundingAmt = ((outputMap.get(programID).sumPayments/outputMap.get(programID).sumOfFundingAmount)*100).setScale(2);
            }
            if(outputMap.get(programID).sumOfPaymentCap>0){
               outputMap.get(programID).sumPmntsPercentPayCap = ((outputMap.get(programID).sumPayments/outputMap.get(programID).sumOfPaymentCap)*100).setScale(2);
            }
        }
    }
    
    //////////////////////////////////////////////////
    ///Get month name by number
    //////////////////////////////////////////////////
    /*private Static String getMonthName(Integer monthNum){
        if(monthNum == 1) return 'January';
        if(monthNum == 2) return 'February';
        if(monthNum == 3) return 'March';
        if(monthNum == 4) return 'April';
        if(monthNum == 5) return 'May';
        if(monthNum == 6) return 'June';
        if(monthNum == 7) return 'July';
        if(monthNum == 8) return 'August';
        if(monthNum == 9) return 'September';
        if(monthNum == 10) return 'October';
        if(monthNum == 11) return 'November';
        if(monthNum == 12) return 'December';
        return '';    
    }*/
    
    public List<StudentProgram__c> queryMethod(){
    
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1);
        string query = 'SELECT id,program__c,program__r.name,program__r.ProgramName__c,Student__c,Status__c,AmountDueToDate__c,';
        query += 'CertificationDate__c,ClosedDate__c,ClosedReason__c,CurrentStatusDate__c,Servicing__c,PaymentCap__c,';
        query += 'FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentCapPostCertification__c,';
        query += 'PaymentTermPostCertification__c,LastDateOfAttendance__c,DaysDelinquent__c,MonthlyAmountDueToDate__c,';
        query += 'PaymentTermRemaining__c,DefermentMonthsRemaining2__c,MonthlyAmountPaidToDate__c,(select id from Transactions__r) ';
        query += 'FROM StudentProgram__c '; 
        
        
        query += 'WHERE Program__r.school__c = \''+selectedSchool+'\'';
        
        if(selectedPrimaryOwners.size()>0){            
            query += ' AND (PrimaryOwner__c IN ';
            query = partOfQueryMethod(selectedPrimaryOwners,'PrimaryOwner',query);
        }
        if(selectedFundOwners.size()>0){            
            query += ' AND (FundOwner__c IN ';
            query = partOfQueryMethod(selectedFundOwners,'FundOwner',query);
        }
        if(selectedPaymentStreamOwners.size()>0){            
            query += ' AND (PaymentStreamOwner__c IN ';
            query = partOfQueryMethod(selectedPaymentStreamOwners,'PaymentStreamOwner',query);
        }
                                        
        
        query += ' AND CertificationDate__c <> null AND certificationDate__c < ' + String.escapeSingleQuotes(String.valueOf(dt).replaceAll(' ', 'T')+'Z');
        query += ' AND Status__c != \'certified\'';
        query += ' AND Status__c != \'Application Incomplete\'';
        query += ' AND Status__c != \'Application Complete\'';
        query += ' ORDER BY Program__r.Name,CertificationDate__c ASC ';
        
        system.debug('QUERY: '+query);
        DatabaseUtil db = new DatabaseUtil();
        List<StudentProgram__c> spList = new List<StudentProgram__c>();
        if(!Test.isRunningTest())
            spList = (List<StudentProgram__c>)db.query(query);
        else{
            spList = [SELECT id,program__c,program__r.name,program__r.ProgramName__c,Student__c,Status__c,AmountDueToDate__c,
                      CertificationDate__c,ClosedDate__c,ClosedReason__c,CurrentStatusDate__c,Servicing__c,PaymentCap__c,
                      FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentCapPostCertification__c,
                      PaymentTermPostCertification__c,LastDateOfAttendance__c,DaysDelinquent__c,MonthlyAmountDueToDate__c,
                      PaymentTermRemaining__c,DefermentMonthsRemaining2__c,MonthlyAmountPaidToDate__c,(select id from Transactions__r)
                      FROM StudentProgram__c 
                      WHERE Program__r.school__c = :selectedSchool AND CertificationDate__c <> null AND certificationDate__c < :dt
                      ORDER BY CertificationDate__c ASC];
        }
        return spList;
    }
    
    public string partOfQueryMethod(Set<Id> idList,string field, string query){
        //string query;
        boolean nullVar = false;
        List<String> querAbleIds = new List<String>();
        for(Id poId : idList) {
            if(poId != null)              
                querAbleIds.add('\'' + poid + '\'');
            if(poId == null)
                nullVar = true;                
        }
        if(querAbleIds.size()>0){
            query += String.format(
                '{0}{1}{2}',
                new String[]{
                    '(', 
                    String.join(querAbleIds, ', '),
                    ')'
                }
            );
        }
        else
            query += '(null)';
        if(!nullVar){
            query += ')';
        }
        else{
            if(field == 'PrimaryOwner')
                query += ' OR PrimaryOwner__c = null)';
            else if(field == 'FundOwner')
                 query += ' OR FundOwner__c = null)';
             else if(field == 'PaymentStreamOwner')
                 query += ' OR PaymentStreamOwner__c = null)';

        }
            
        return query;
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        
        PageReference pg = new PageReference('/apex/ReportMonthlyInvestorStaticCopyExport');
        pg.setRedirect(false);
        return pg;
    }
    
    /////////////////////////////////////////
    ///Wrapper to hold report data
    ///////////////////////////////////////////
    public class ReportDataWrapper{
        public String programName {get;set;}
        public String programNumber {get;set;}
        public Integer originalIsaAssests {get;set;}
        public Integer cancelledIsaAssets {get;set;}
        public Integer contractuallySatisfiedIsaAssets {get;set;}
        public Integer defaultIsaAssets {get;set;}
        public Integer remainingIsaAssets {get;set;}
        public Decimal remainingIsapercentOrig {get;set;}
        public Decimal sumOfFundingAmount {get;set;}
        public Decimal sumOfCancelledAmount {get;set;}
        public Decimal sumOfPaymentCap {get;set;}
        public Decimal sumOfIncomeShare {get;set;}
        public Decimal sumOfPaymentTerm {get;set;}
        public Decimal avgIncomeShare {get;set;}
        public Decimal avgPaymentTerm {get;set;}
        public Integer countInPaymentISA {get;set;}
        public Integer countInSchoolISA {get;set;}
        public Integer countInGraceISA {get;set;}
        public Integer countInDefermentOrPauseISA {get;set;}
        public Decimal inPayIsaPercentOrig {get;set;}
        public Decimal sumPayments {get;set;}
        public Decimal sumPmntsPercentFundingAmt {get;set;}
        public Decimal sumPmntsPercentPayCap {get;set;}
        public Integer isaNeverPay {get;set;}
        public Integer isaPayCap {get;set;}
        public Integer isaNoPaymentDue {get;set;}
        public Integer isaOnTimePayment {get;set;}
        public Integer isaThirtyToEightyNineDel {get;set;}
        public Integer isaNinetyPlusDel {get;set;}
        public Integer byPayingThePaymentCap {get;set;}
        public Integer byPayingTheMaximumNumberOfMonthlyPayments {get;set;}
        public Integer byReachingTheEndOfThePaymentTerm {get;set;}
        
        public reportDataWrapper(){
            this.originalIsaAssests = 0;
            this.cancelledIsaAssets = 0;
            this.contractuallySatisfiedIsaAssets = 0;
            this.defaultIsaAssets = 0;
            this.remainingIsaAssets = 0;
            this.remainingIsapercentOrig = 0;
            this.sumOfFundingAmount = 0;
            this.sumOfCancelledAmount = 0;
            this.sumOfPaymentCap = 0;
            this.sumOfIncomeShare = 0;
            this.sumOfPaymentTerm = 0;
            this.avgIncomeShare = 0;
            this.avgPaymentTerm = 0;
            this.countInPaymentISA = 0;
            this.countInSchoolISA = 0;
            this.countInGraceISA = 0;
            this.countInDefermentOrPauseISA = 0;
            this.inPayIsaPercentOrig = 0;
            this.sumPayments = 0;
            this.sumPmntsPercentFundingAmt = 0;
            this.sumPmntsPercentPayCap = 0;
            this.isaNeverPay = 0;
            this.isaPayCap = 0;
            this.isaNoPaymentDue = 0;
            this.isaOnTimePayment = 0;
            this.isaThirtyToEightyNineDel = 0;
            this.isaNinetyPlusDel = 0;
            this.byPayingThePaymentCap = 0;
            this.byPayingTheMaximumNumberOfMonthlyPayments = 0;
            this.byReachingTheEndOfThePaymentTerm = 0;
        }
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Primary Owner
    ///////////////////////////////////
    public class primaryOwnerWrapper{
        public boolean selected {get;set;}
        public Account primaryOwner {get;set;}
        
        public primaryOwnerWrapper(boolean selected, Account primaryOwner){
            this.selected = selected;
            this.primaryOwner = primaryOwner;
        }  
        
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Fund Owner
    ///////////////////////////////////
    public class fundOwnerWrapper{
        public boolean selected {get;set;}
        public Account fundOwner {get;set;}
        
        public fundOwnerWrapper(boolean selected, Account fundOwner){
            this.selected = selected;
            this.fundOwner = fundOwner;
        }  
        
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all Payment Stream Owner
    ///////////////////////////////////
    public class paymentStreamOwnerWrapper{
        public boolean selected {get;set;}
        public Account paymentStreamOwner {get;set;}
        
        public paymentStreamOwnerWrapper(boolean selected, Account paymentStreamOwner){
            this.selected = selected;
            this.paymentStreamOwner = paymentStreamOwner;
        }  
        
    }
    
}