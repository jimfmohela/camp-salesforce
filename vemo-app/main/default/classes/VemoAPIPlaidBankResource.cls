public with sharing class VemoAPIPlaidBankResource{

    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            //return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }
        if((api.version == 'v1') && (api.method == 'DELETE')){
            //return handleDeleteV1(api);
        }
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIPlaidBankResource.handleGetV1()');   
        String plaidBankIDparam = api.params.get('plaidBankID');
        String studentIDparam = api.params.get('studentID');
        List<PlaidBankService.PlaidBank> pbList  = new List<PlaidBankService.PlaidBank>();
        if(plaidBankIDparam != null){
            pbList = PlaidBankService.getPlaidBankWithPlaidBankID(VemoApi.parseParameterIntoIDSet(plaidBankIDparam));
        }
        else if(studentIDparam != null){
            pbList = PlaidBankService.getPlaidBankWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDparam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter plaidBankID, or studentID missing');
        }
        
        List<PlaidBankResourceOutputV1> results = new List<PlaidBankResourceOutputV1>();
        
        Set<ID> StudentIDsToVerify = new Set<ID>();
        
        for(PlaidBankService.PlaidBank pb : pbList){
           StudentIDsToVerify.add((ID)pb.studentID);
        }
        
        Map<Id, Account> VerifiedAccountMap = AccountQueries.getStudentMapWithStudentID(StudentIDsToVerify);
        for(PlaidBankService.PlaidBank pb : pbList){
            if(VerifiedAccountMap.containsKey((ID)pb.studentID)){
                results.add(new PlaidBankResourceOutputV1(pb));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIPlaidBankResource.handlePutV1()');   
        List<PlaidBankService.PlaidBank> pbs = new List<PlaidBankService.PlaidBank>();
        List<PlaidBankResourceInputV1> pbsJSON = (List<PlaidBankResourceInputV1>)JSON.deserialize(api.body, List<PlaidBankResourceInputV1>.class);
        
        Set<ID> IDsToVerify = new Set<ID>();
        
        for(PlaidBankResourceInputV1 pbJSON : pbsJSON){
           IDsToVerify.add((ID)pbJSON.plaidBankId);
        }
        Map<Id, PlaidBank__c> VerifiedPlaidBankMap = PlaidBankQueries.getPlaidBankMapWithID(IDsToVerify);
        for(PlaidBankResourceInputV1 pbJSON : pbsJSON){
            if(VerifiedPlaidBankMap.containsKey((ID)pbJSON.plaidBankId)){
                pbJSON.validatePUTFields();
                pbs.add(plaidBankResourceToPlaidBank(pbJSON));
            }
        }       
        Set<Id> pbIDs = PlaidBankService.updatePlaidBanks(pbs);
        return (new VemoAPI.ResultResponse(pbIDs, pbIDs.size()));
    }
    
    public  static PlaidBankService.PlaidBank plaidBankResourceToPlaidBank(PlaidBankResourceInputV1 pbank){
        PlaidBankService.PlaidBank pb = new PlaidBankService.PlaidBank();
        pb.plaidBankID = pbank.plaidBankID;  
        pb.plaidStatus = pBank.plaidStatus;
               
        return pb;
    }
    
    public class PlaidBankResourceInputV1{
        public String plaidBankID {get;set;}
        public String plaidStatus {get;set;}
        
        public PlaidBankResourceInputV1(){}

        public void validatePUTFields(){
            if(plaidBankID == null) throw new VemoAPI.VemoAPIFaultException('plaidBankID is a required input parameter on PUT');
        }
    }
    
    
    public class PlaidBankResourceOutputV1{

        public PlaidBankResourceOutputV1(){}
        
        public PlaidBankResourceOutputV1(PlaidBankService.PlaidBank pb){
            this.plaidBankID = pb.plaidBankID;
            this.bankName = pb.bankName;
            this.studentID = pb.studentID;
            this.plaidStatus = pb.plaidStatus;
            this.itemId = pb.itemId;
            this.updatePlaidPassword = pb.updatePlaidPassword; 
            this.accountNumber = pb.accountNumber;
        }
        public String plaidBankID {get;set;}
        public String bankName {get;set;}
        public String studentID {get;set;}
        public String plaidStatus {get;set;}
        public String itemId {get;set;}
        public Boolean updatePlaidPassword {get;set;}
        public String accountNumber {get;set;}
        
    }

}