public with sharing class SchoolAdminSiteRegisterController {
    // PORTAL_ACCOUNT_ID is the account on which the contact will be created on and then enabled as a portal user.
    // you need to add the account owner into the role hierarchy before this will work - please see Customer Portal Setup help for more information.       
    private static Id PORTAL_ACCOUNT_ID = '001f0000017fvSD';
    
    public SchoolAdminSiteRegisterController () {
        System.debug('SchoolAdminStieRegisterController()');
        pageMessage = '';
    }
    
    public String username {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String pageMessage {get;set;}
    
    private boolean isValidPassword() {
        System.debug('SchoolAdminSiteRegisterController.isValidPassowrd()');
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
        System.debug('SchoolAdminSiteRegisterController.registerUser()');
        pageMessage = '';
        
        username = Apexpages.currentPage().getParameters().get('usernameJS');
        communityNickname = Apexpages.currentPage().getParameters().get('communityNickNameJS');
        email = Apexpages.currentPage().getParameters().get('emailJS');
        password = Apexpages.currentPage().getParameters().get('passwordJS');
        confirmPassword = Apexpages.currentPage().getParameters().get('verifyPasswordJS');
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            List<ApexPages.Message> pageMessages = new List<ApexPages.Message>();
            pageMessages = ApexPages.getMessages();
            if(pageMessages.size() > 0){
                pageMessage = pageMessages[pageMessages.size()-1].getSummary();
            }
            return null;
        }    
        User u = new User();
        u.Username = username;
        u.Email = email;
        u.CommunityNickname = communityNickname;
        
        //String accountId = PORTAL_ACCOUNT_ID;
        String accountId = null;
        
        // lastName is a required field on user, but if it isn't specified, we'll default it to the username
        String userId = Site.createPortalUser(u, accountId, password);
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(username, password, null);
            }
            else {
                PageReference page = System.Page.SchoolAdminSiteRegisterConfirm;
                page.setRedirect(true);
                return page;
            }
        }
        else {
            List<ApexPages.Message> pageMessages = new List<ApexPages.Message>();
            pageMessages = ApexPages.getMessages();
            if(pageMessages.size() > 0){
                pageMessage = pageMessages[pageMessages.size()-1].getSummary();
            }
        }
        return null;
    }
    
}