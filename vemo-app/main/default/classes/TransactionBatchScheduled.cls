/*
/////////////////////////////PURGE DELETED///////////////////////////////////
TransactionBatchScheduled job = new TransactionBatchScheduled();
job.jobType = TransactionBatch.JobType.PURGE_DELETED_DISBURSEMENTS;
String cronStr = '0 0 * * * ? *';
System.schedule('Purge Deleted Disbursements Hourly', cronStr, job);
/////////////////////////////////////////////////////////////////////////////
*/

public class TransactionBatchScheduled implements Schedulable {
    public TransactionBatch.JobType jobType {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        TransactionBatch job = new TransactionBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job);
    }
}