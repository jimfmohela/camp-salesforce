/////////////////////////////////////////////////////////////////////////
// Class: DataCollectionQueries_TEST
// 
// Description: 
//  Unit test for DataCollectionQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-05   Rini Gupta  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class DataCollectionQueries_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void validateGetDataCollectionMapWithID(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
        Test.startTest();
        Map<id,DataCollection__c> resultDataCollectionMap = DataCollectionQueries.getDataCollectionMapWithID(dataCollectionMap .keyset());
        System.assertEquals(DataCollectionMap.keySet().size(), resultDataCollectionMap.keyset().size());
        Test.stopTest();
    }
    
    static testMethod void validateGetDataCollectionMapWithAgreement(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
        Test.startTest();
        Map<id,DataCollection__c> resultDataCollectionMap = DataCollectionQueries.getDataCollectionMapWithAgreement(agreementMap.keyset());
        System.assertEquals(DataCollectionMap.keySet().size(), resultDataCollectionMap.keyset().size());
        Test.stopTest();
    }
    
 }