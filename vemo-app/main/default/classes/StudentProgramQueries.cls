/////////////////////////////////////////////////////////////////////////
// Class: StudentProgramQueries
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION----------------------------- 
// 2016-12-23   Greg Cook       Created
// 2018-09-20   Ranjeet Kumar   Added StudentID__c in getfieldNames()
// 2018-12-24   Ranjeet Kumar   Added ASDStatus__c in getfieldNames()
/////////////////////////////////////////////////////////////////////////
public class StudentProgramQueries {
    public static Map<ID, StudentProgram__c> getStudentProgramMap(){
        System.debug('StudentProgramQueries.getStudentProgramMap');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' WHERE Deleted__c = false and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';

        return studPrgMap;  
    }

    public static Map<ID, StudentProgram__c> getStudentProgramMapWithAgreementID(Set<ID> agreementIDs){
        System.debug('StudentProgramQueries.getStudentProgramMapWithAgreementID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and ID IN ' + DatabaseUtil.inSetStringBuilder(agreementIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;  
    }


    public static Map<ID, StudentProgram__c> getStudentProgramMapWithStudentID(Set<ID> studentIDs){
        System.debug('StudentProgramQueries.getStudentProgramMapWithStudentID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement(); 
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithStudentIDNStatus(Set<ID> studentIDs, Set<String> statuses){
        System.debug('StudentProgramQueries.getStudentProgramMapWithStudentIDNStatus');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        
        if(statuses != null && statuses.size()>0){
            query += ' and Status__c IN ' + DatabaseUtil.inSetStringBuilder(statuses);
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement(); 
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }


    public static Map<ID, StudentProgram__c> getStudentProgramMapWithStudentID(Set<ID> studentIDs, Boolean servicing){
        System.debug('StudentProgramQueries.getStudentProgramMapWithStudentID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        query += ' and Servicing__c = '+String.valueOf(servicing);
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }

    public static Map<ID, StudentProgram__c> getInvitedStudentProgramMapWithStudentID(Set<ID> studentIDs){
        System.debug('StudentProgramQueries.getInvitedStudentProgramMapWithStudentID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        query += 'and Status__c = \'Invited\' ';
        query += 'and Program__r.ProgramStatus__c = \'Open\' ';
        query += 'and PreCertified__c = true ';
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithSchoolID(Set<ID> schoolIDs){
        System.debug('StudentProgramQueries.getStudentProgramMapWithSchoolID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Program__r.School__c IN ' + DatabaseUtil.inSetStringBuilder(schoolIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';

        }
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        //System.debug('*****************************query = ' + query);
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        
        return studPrgMap;
    }
    
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithSchoolIDNStatus(Set<ID> schoolIDs, Set<string> statuses){
        System.debug('StudentProgramQueries.getStudentProgramMapWithSchoolIDNStatus');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Program__r.School__c IN ' + DatabaseUtil.inSetStringBuilder(schoolIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';

        }
        if(DatabaseUtil.filterBySchoolID){
            ID schoolID = DatabaseUtil.schoolID;
            query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        
        if(statuses != null && statuses.size()>0){
            query += ' and Status__c IN ' + DatabaseUtil.inSetStringBuilder(statuses);
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        //System.debug('*****************************query = ' + query);
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithProgramID(Set<ID> programIDs){
        System.debug('StudentProgramQueries.getStudentProgramMapWithProgramID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Program__c IN ' + DatabaseUtil.inSetStringBuilder(programIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+  DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithProgramIDNStatus(Set<ID> programIDs, Set<string> statuses){
        System.debug('StudentProgramQueries.getStudentProgramMapWithProgramIDNStatus');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Program__c IN ' + DatabaseUtil.inSetStringBuilder(programIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(statuses != null && statuses.size()>0){
            query += ' and Status__c IN ' + DatabaseUtil.inSetStringBuilder(statuses);
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    
    public static Map<ID, StudentProgram__c> getStudentProgramMapWithAcademicEnrollmentID(Set<ID> academicEnrollmentIds){
        System.debug('StudentProgramQueries.getStudentProgramMapWithAcademicEnrollmentID');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and AcademicEnrollment__c IN ' + DatabaseUtil.inSetStringBuilder(academicEnrollmentIds);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }
    
    //public static Map<ID, StudentProgram__c> getStudentProgramMapForAErecords(Set<ID> studentIDs, Set<ID> providerIDs, Set<ID> academicEnrollmentIDs, Set<ID> spsIDs){
    public static Map<ID, StudentProgram__c> getStudentProgramMapForAErecords(Set<ID> studentIDs, Set<ID> providerIDs, Set<ID> academicEnrollmentIDs){
        System.debug('StudentProgramQueries.getStudentProgramMapForAErecords');
        Map<ID, StudentProgram__c> studPrgMap = new Map<ID, StudentProgram__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Deleted__c = false and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        query += ' and Program__r.School__c IN ' + DatabaseUtil.inSetStringBuilder(providerIDs)+ ' ';
        query += ' and AcademicEnrollment__c IN ' + DatabaseUtil.inSetStringBuilder(academicEnrollmentIDs)+ ' ';
        
        /*query += ' and (( MajorPostCertification__c != ' + null + ' AND MajorPostCertification__c In '+DatabaseUtil.inSetStringBuilder(spsIDs)+ ' ) ';
        query += ' OR( MajorPostCertification__c = ' + null + ' AND MajorCertification__c != ' + null + ' AND MajorCertification__c In '+DatabaseUtil.inSetStringBuilder(spsIDs)+ ' ) ';
        query += ' OR( MajorPostCertification__c = ' + null + ' AND MajorCertification__c = ' + null + ' AND MajorPreCertification__c != ' + null + ' AND MajorPreCertification__c In '+DatabaseUtil.inSetStringBuilder(spsIDs)+ ' ) ';
        query += ' OR( MajorPostCertification__c = ' + null + ' AND MajorCertification__c = ' + null + ' AND MajorPreCertification__c = ' + null + ' AND MajorStudent__c  != ' + null + ' AND MajorStudent__c In '+DatabaseUtil.inSetStringBuilder(spsIDs)+ ' ) ';
        
        query += ')'; */    
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        //studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
        if(DatabaseUtil.paginationFlag == true){
            studPrgMap = getStudentProgramMapByPage(db.query(query), DatabaseUtil.getPage(), DatabaseUtil.getPageSize());
        }
        else{
            studPrgMap = new Map<ID, StudentProgram__c>((List<StudentProgram__c>)db.query(query));
            DatabaseUtil.orderBy = '';
        }
        //DatabaseUtil.orderBy = '';
        return studPrgMap;
    }

    public static String generateSOQLSelect(){
        String soql;
        if(DatabaseUtil.paginationFlag == true){
            soql = 'SELECT Id FROM StudentProgram__c'; 
        }
        else{
            soql = 'SELECT ' + getfieldNames() + ' FROM StudentProgram__c';
        }
        system.debug(soql);
        return soql;
    }

    public static String getfieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
//      fieldNames += 'CreatedBy, ';
        fieldNames += 'Name, ';
//      fieldNames += 'LastModifiedBy, ';

//      fieldNames += 'GUID__c, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'Student__r.Name, ';
//      fieldNames += 'Student__r.GUID__c, ';


        fieldNames += 'Program__c, ';
//      fieldNames += 'Program__r.GUID__c, ';

        fieldNames += 'Program__r.Name, ';
        fieldNames += 'Program__r.ProgramName__c, ';
        fieldNames += 'Program__r.ProgramStatus__c, ';
        fieldNames += 'Program__r.School__c, ';
        //fieldNames += 'Program__r.School__r.SchoolSecurity__c, ';
//      fieldNames += 'Program__r.School__r.GUID__c, ';
        fieldNames += 'Program__r.AgreementTemplate__c, ';
        fieldNames += 'Program__r.CumulativeIncomeShareCap__c, ';
        fieldNames += 'Program__r.DisclosureTemplate__c, ';     
        fieldNames += 'Program__r.EnrollmentBeginDate__c, ';
        fieldNames += 'Program__r.EnrollmentEndDate__c, ';
        fieldNames += 'Program__r.ProgramNotes__c, ';
        fieldNames += 'Program__r.RegistrationBeginDate__c, ';
        fieldNames += 'Program__r.RegistrationEndDate__c, ';
        
        fieldNames += 'AcademicEnrollment__c, ';
        
        fieldNames += 'AdditionalFundingOptionsSelected__c, ';
        fieldNames += 'AdobeSignAgreementID__c, ';
        fieldNames += 'AdobeSignJavascript__c, ';
        fieldNames += 'AdobeSignStatus__c, ';
        fieldNames += 'AdobeSignURL__c, ';
        fieldNames += 'AdobeSignWidgetID__c, ';
        fieldNames += 'AgreementSigned__c, ';

        fieldNames += 'AllocatedAmount__c, ';//will be replaced?
        fieldNames += 'ApplicationStartDate__c, ';
        fieldNames += 'ApprovalDisclosureDate__c, ';
        fieldNames += 'ASDStatus__c, ';
        fieldNames += 'AssessmentDate__c, ';
        fieldNames += 'AssessmentDayOfMonth__c, ';
        fieldNames += 'AssessContract__c, ';
        fieldNames += 'AttendanceBeginDate__c, ';
        fieldNames += 'AttendanceEndDate__c, ';
        fieldNames += 'AmountDisbursedToDate__c, ';

        fieldNames += 'Birthdate__c, '; //probably goes away
        fieldNames += 'BirthdateCertification__c, ';
        fieldNames += 'BirthdatePreCertification__c, ';
        fieldNames += 'BirthdatePostCertification__c, ';
        fieldNames += 'BirthdateStudent__c, ';

        fieldNames += 'BypassAutomation__c, ';
        fieldNames += 'CancellationReason__c, ';
        fieldNames += 'CancellationReasonDetails__c, ';
        fieldNames += 'CertificationComments__c, ';
        fieldNames += 'CertificationDate__c, ';
        fieldNames += 'CertificationDraftStatus__c, ';

        fieldNames += 'CongaFinalDisclosureID__c, ';
        fieldNames += 'CongaFinalDisclosureStatus__c, ';
        fieldNames += 'CongaUnsignedAgreementID__c, ';
        fieldNames += 'CongaUnsignedAgreementStatus__c, ';

        fieldNames += 'CreditCheck__c, ';
        fieldNames += 'CreditCheckDeniedReasonText__c, ';
        fieldNames += 'CreditCheckProcess__c, ';
        fieldNames += 'CreditCheckConsentDateTimeStamp__c, ';
        fieldNames += 'CreditCheckConsentIPAddress__c, ';

        fieldNames += 'CurrentStatusDate__c, ';
        fieldNames += 'CustomerWithdrawnReason__c, ';
        fieldNames += 'CustomerWithdrawnReasonOther__c, ';

        fieldNames += 'DaysDelinquent__c, ';
        fieldNames += 'Deferment__c, ';
        fieldNames += 'DefermentMonthsUsed__c, ';
        fieldNames += 'DefermentMonthsAllowed__c, ';
        fieldNames += 'DefermentMonthsRemaining2__c, ';
        fieldNames += 'DefermentBeginDate__c, ';
        fieldNames += 'DefermentEndDate__c, ';
        fieldNames += 'Deleted__c, ';

        fieldNames += 'EarlyPayment__c, ';
        fieldNames += 'EligibleToReapply__c, ';

        fieldNames += 'EnrollmentStatusCertification__c, ';
        fieldNames += 'EnrollmentStatusPreCertification__c, ';
        fieldNames += 'EnrollmentStatusPostCertification__c, ';
        fieldNames += 'EnrollmentStatusStudent__c, ';

        fieldNames += 'EstimatedAgreementEndDate__c, ';


        fieldNames += 'ExpectedGraduationDate__c, ';
        fieldNames += 'FinalDisclosureCleanURL__c, ';       
        fieldNames += 'FinalDisclosureID__c, ';

        fieldNames += 'FundingAmountCertification__c, ';
        fieldNames += 'FundingAmountPostCertification__c, ';
        fieldNames += 'FundingAmountStudent__c, ';

        fieldNames += 'FundingMaximumPreCertification__c, ';
        fieldNames += 'FundingMinimumPreCertification__c, ';
        fieldNames += 'FundingPurpose__c, ';

        fieldNames += 'GenerateAgreement__c, ';
        fieldNames += 'GenerateDisbursementSchedule__c, ';
        fieldNames += 'GenerateFinalDisclosure__c, ';

        fieldNames += 'GraceMonthsUsed__c, '; 
        fieldNames += 'GraceMonthsAllowed__c, ';
        fieldNames += 'GraceMonthsRemaining2__c, ';
        fieldNames += 'GracePeriodEndDate__c, ';

        fieldNames += 'GradeLevel__c, ';
        fieldNames += 'GradeLevelCertification__c, ';
        fieldNames += 'GradeLevelPreCertification__c, ';
        fieldNames += 'GradeLevelPostCertification__c, ';
        fieldNames += 'GradeLevelStudent__c, ';

        fieldNames += 'IncomeShare__c, '; //probably goes away
        fieldNames += 'IncomeShareCertification__c, ';
        fieldNames += 'IncomeSharePostCertification__c, ';
        fieldNames += 'IncomeShareStudent__c, ';

        fieldNames += 'LastDateOfAttendance__c, ';
        fieldNames += 'MajorCertification__c, ';
        fieldNames += 'MajorPreCertification__c, ';
        fieldNames += 'MajorPostCertification__c, ';
        fieldNames += 'MajorStudent__c, ';

        fieldNames += 'MinimumIncomePerMonth__c, ';
        //fieldNames += 'MinimumIncomePerYear__c, ';

        fieldNames += 'MonthlyAmountPaidToDate__c, ';
        fieldNames += 'MonthlyAmountDueToDate__c, ';
        fieldNames += 'MonthlyAmountDue__c, ';
        
        fieldNames += 'AmountDueToDate__c, ';

        fieldNames += 'NextPaymentDue__c, ';
        fieldNames += 'NextPaymentDueDate__c, ';
        fieldNames += 'NotCertifiedReason__c, ';

        fieldNames += 'PaidToDate__c, ';
        fieldNames += 'PaveTheWay__c, ';

        fieldNames += 'PaymentCap__c, '; //probably goes away
        fieldNames += 'PaymentCapCertification__c, ';
        fieldNames += 'PaymentCapPostCertification__c, ';
        fieldNames += 'PaymentCapStudent__c, ';

        fieldNames += 'PaymentTerm__c, ';//probably goes away
        fieldNames += 'PaymentTermCertification__c, ';
        fieldNames += 'PaymentTermPostCertification__c, ';
        fieldNames += 'PaymentTermStudent__c, ';
        fieldNames += 'PaymentTermAssessed__c, ';
        fieldNames += 'PaymentTermRemaining__c, ';

        fieldNames += 'PreCertified__c, ';
        fieldNames += 'QuizAttemptsData__c, ';
        fieldNames += 'QuizLocked__c, ';
        fieldNames += 'QuizState__c, ';
        fieldNames += 'QuizResponseLink__c, ';

        fieldNames += 'ReconciliationDueToDate__c, ';
        fieldNames += 'ReconciliationPaidTODate__c, ';
        fieldNames += 'ReconciliationDue__c, ';

        fieldNames += 'RegistrationExceptionProcess__c, ';
        //fieldNames += 'RemainingTerm__c, '; //deprecated


        fieldNames += 'RequestedAmount__c, '; //probably goes away
        fieldNames += 'Residency__c, ';//probably goes away
        fieldNames += 'ResidencyCertification__c, ';
        fieldNames += 'ResidencyPreCertification__c, ';
        fieldNames += 'ResidencyPostCertification__c, ';
        fieldNames += 'ResidencyStudent__c, ';

        fieldNames += 'RightToCancelDate__c, ';
        fieldNames += 'SchoolName__c, ';
        fieldNames += 'SchoolProgramOfStudy__c, '; //probably goes away
        fieldNames += 'Servicing__c, ';
        fieldNames += 'ServicingStartDate__c, ';
        fieldNames += 'SignedAgreementCleanURL__c, ';               
        fieldNames += 'SignedAgreementID__c, ';

        fieldNames += 'StateOfResidenceCertification__c, ';
        fieldNames += 'StateOfResidencePreCertification__c, ';
        fieldNames += 'StateOfResidencePostCertification__c, ';
        fieldNames += 'StateOfResidenceStudent__c, ';

        fieldNames += 'Status__c, ';
        fieldNames += 'StudentEmail__c, ';
        fieldNames += 'StudentID__c, ';
        fieldNames += 'SubmittedDate__c, ';

        fieldNames += 'VemoContractNumber__c, ';
        
        fieldNames += 'SendApplicationCertified__c, ';
        fieldNames += 'SendApplicationComplete__c, ';
        fieldNames += 'SendApplicationCompleteSchoolInit__c, ';
        fieldNames += 'SendSchoolInitPreCertifiedInvite__c, ';
        fieldNames += 'SendApplicationUnderReview__c, ';
        fieldNames += 'SendApplicationReviewApproved__c, ';
        fieldNames += 'SendApplicationReviewDenied__c, ';
        fieldNames += 'SendApplicationImmediatelyApproved__c, ';
        fieldNames += 'SendNotCertified__c, ';
        fieldNames += 'SendCreditDenied__c, ';
        fieldNames += 'Student__r.PersonEmail, ';
        fieldNames += 'Student__r.PersonContactID, ';
        fieldNames += 'Student__r.PrimarySchoolStudentID__pc, ';
        
        fieldNames += 'TotalFullyAllocatedAmountDue__c, ';
        fieldNames += 'Active__c, ';
        fieldNames += 'AgreementStatusFilter1__c, ';
        fieldNames += 'PaymentWindow__c, ';
		fieldNames += 'PaymentWindowRemainingMonths__c, ';
        fieldNames += 'PaymentMonthsUsed__c ';

        return fieldNames;
    }
    
    public static String generateLIMITStatement(){
        String lim;
        //if(DatabaseUtil.getPrimaryResource() == 'agreement') {
        //    lim = 'LIMIT '+ DatabaseUtil.getPageSize() + ' OFFSET ' + DatabaseUtil.getOffset();
        //}else {
            lim = 'LIMIT 50000';
        //}
        return lim;
    }
    
    public static Map<ID,StudentProgram__c> getStudentProgramMapByPage(List<StudentProgram__c> StudentProgramList, Integer page, Integer pageSize){
        DatabaseUtil.paginationFlag = false;
        Map<Id,StudentProgram__c> studentProgramMap = new Map<Id,StudentProgram__c>(); 
        integer startIndex = (page*pageSize)-pageSize;
        integer endIndex = (page*pageSize)-1;
        if(studentProgramList.size()<=endIndex){
            endIndex = studentProgramList.size()-1;    
        }    
        for(integer i = startIndex; i<= endIndex; i++){
            studentProgramMap.put(studentProgramList[i].Id,studentProgramList[i]);
        }
        //system.debug('studentProgramMap --'+studentProgramMap.size());
        Map<Id, StudentProgram__c> studPrgMap =  getStudentProgramMapWithAgreementID(studentProgramMap.keySet());
        return studPrgMap; 
    }
            
}