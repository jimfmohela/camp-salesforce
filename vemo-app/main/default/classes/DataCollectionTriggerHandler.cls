/////////////////////////////////////////////////////////////////////////
// Class: DataCollectionTriggerHandler 
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-26   Rini Gupta       Created                              
// 
/////////////////////////////////////////////////////////////////////////

public with sharing class DataCollectionTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {
    
    /**************************Static Variables***********************************/

    /**************************State acctrol Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution acctrol - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.Triggercontext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.Triggercontext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'DataCollectionTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'DataCollectionTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            system.debug('tc.handler--'+tc.handler);
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onBeforeInsert()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> newDataCollectionList = (List<DataCollection__c>)tc.newList;
        //This is where you should call your business logic
    

    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onBeforeUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> newDataCollectionList = (List<DataCollection__c>)tc.newList;
        List<DataCollection__c> oldDataCollectionList = (List<DataCollection__c>)tc.oldList;
        Map<ID, DataCollection__c> newDataCollectionMap = (Map<ID, DataCollection__c>)tc.newMap;
        Map<ID, DataCollection__c> oldDataCollectionMap = (Map<ID, DataCollection__c>)tc.oldMap;
        //This is where you should call your business logic
    

    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onBeforeDelete()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> oldDataCollectionList = (List<DataCollection__c>)tc.oldList;
        Map<ID, DataCollection__c> oldDataCollectionMap = (Map<ID, DataCollection__c>)tc.oldMap;
        //This is where you should call your business logic
        

    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onAfterInsert()');
         //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> newDataCollectionList = (List<DataCollection__c>)tc.newList;
        Map<ID, DataCollection__c> newDataCollectionMap = (Map<ID, DataCollection__c>)tc.newMap;
        //This is where you should call your business logic
        manageSharing(null, newDataCollectionMap);
        
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onAfterUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> newDataCollectionList = (List<DataCollection__c>)tc.newList;
        List<DataCollection__c> oldDataCollectionList = (List<DataCollection__c>)tc.oldList;
        Map<ID, DataCollection__c> newDataCollectionMap = (Map<ID, DataCollection__c>)tc.newMap;
        Map<ID, DataCollection__c> oldDataCollectionMap = (Map<ID, DataCollection__c>)tc.oldMap;
        //This is where you should call your business logic
        
        
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onAfterDelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> oldDataCollectionList = (List<DataCollection__c>)tc.oldList;
        Map<ID, DataCollection__c> oldDataCollectionMap = (Map<ID, DataCollection__c>)tc.oldMap;
        //This is where you should call your business logic
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.Triggercontext tc){
        system.debug('DataCollectionTriggerHandler.onAfterUndelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<DataCollection__c> newDataCollectionList = (List<DataCollection__c>)tc.newList;
        Map<ID, DataCollection__c> newDataCollectionMap = (Map<ID, DataCollection__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    
    private void manageSharing(Map<Id, DataCollection__c> oldDataCollectionMap, Map<Id, DataCollection__c> newDataCollectionMap){
        Set<Id> dataCollectionIds = new Set<ID>();
        Set<ID> agreementIds = new Set<ID>();
        for(DataCollection__c dc: newDataCollectionMap.values()){
            agreementIds.add(dc.Agreement__c);    
        }
        SharingService.DataCollectionSharing(newDataCollectionMap);
        
    }
    
    
}