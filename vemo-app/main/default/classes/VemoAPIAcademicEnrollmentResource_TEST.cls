/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIAcademicEnrollmentResource_TEST
// 
// Description: 
//      Test class for VemoAPIAcademicEnrollmentResource
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-04   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIAcademicEnrollmentResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        
        Map<String, String> studentParams = new Map<String, String>();
        studentParams.put('studentID', (String)testStudentAccountMap.values().get(0).ID);
        studentParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo studentApiInfo = TestUtil.initializeAPI('v2', 'GET', studentParams, null);
        
        Map<String, String> providerParams = new Map<String, String>();
        providerParams.put('providerID', (String)schools.values().get(0).ID);
        providerParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo providerApiInfo = TestUtil.initializeAPI('v2', 'GET', providerParams, null);

        Test.startTest();
        
        VemoAPI.ResultResponse studentResult = (VemoAPI.ResultResponse)VemoAPIAcademicEnrollmentResource.handleAPI(studentApiInfo);
        System.assertEquals(testAcademicEnrollmentMap.size(), studentResult.numberOfResults);
        
        VemoAPI.ResultResponse providerResult = (VemoAPI.ResultResponse)VemoAPIAcademicEnrollmentResource.handleAPI(providerApiInfo);
        System.assertEquals(testAcademicEnrollmentMap.size(), providerResult.numberOfResults);

        Test.stopTest();
    }
}