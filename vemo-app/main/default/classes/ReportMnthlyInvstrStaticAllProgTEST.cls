@isTest
public class ReportMnthlyInvstrStaticAllProgTEST{
    private static DatabaseUtil dbUtil = new DatabaseUtil();

    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(2, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);    
        Map<ID, PaymentMethod__c> paymentMethods = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, PaymentInstruction__c> PIs = TestDataFactory.createAndInsertPaymentInstruction(1, students, paymentMethods);
        Map<ID, PaymentAllocation__c> allocations = TestDataFactory.createAndInsertPaymentAllocation(3,PIs,agreements);
        Map<ID, StudentProgramMonthlyStatus__c> monthlyStatus = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(3, agreements);
        
        Datetime dt = Datetime.now().addyears(-2);
        integer cnt=0;
        for(Id agreementID:agreements.keySet()){
            agreements.get(agreementID).certificationDate__c = dt.addMonths(cnt);
            if(cnt<4){
                agreements.get(agreementID).expectedGraduationDate__c = dt.addMonths(cnt+6).date();
                agreements.get(agreementID).status__c = 'Closed';
                agreements.get(agreementID).ClosedDate__c = Date.today();
                if(cnt<2) agreements.get(agreementID).ClosedReason__c = 'Default';
                else agreements.get(agreementID).ClosedReason__c = 'Contract Satisfied';  
                        
            }
            if(cnt>=4 && cnt <6){
                agreements.get(agreementID).status__c = 'Cancelled';
                
            }
            if(cnt>=6 && cnt <7){
                agreements.get(agreementID).status__c = 'Pending Reconciliation';
            }
            cnt++;
        }
        //update agreements.values();
        dbUtil.updateRecords(agreements.values());
        }
    }
    
    /*@isTest public static void validateGetProgram(){
        Test.StartTest(); 
        ReportMnthlyInvstrStaticAllProgCtr cntrl = new ReportMnthlyInvstrStaticAllProgCtr();
        List<SelectOption> schools = cntrl.getschools();
        System.assertEquals(1,schools.size(),'Should be 1');
        Test.StopTest(); 
    }*/
    
    @isTest public static void testrunReport(){
        Test.startTest();
        ReportMnthlyInvstrStaticAllProgCtr cntrl = new ReportMnthlyInvstrStaticAllProgCtr();
        //List<SelectOption> schools = cntrl.getSchools();
        //cntrl.selectedSchool = schools[0].getValue();
        cntrl.runReport();
        Test.StopTest(); 
    }
    
    @isTest public static void testExport(){
        Test.startTest();
        ReportMnthlyInvstrStaticAllProgCtr cntrl = new ReportMnthlyInvstrStaticAllProgCtr();
        //List<SelectOption> schools = cntrl.getSchools();
        //cntrl.selectedSchool = schools[0].getValue();
        cntrl.exportToCSV();
        cntrl.buildCsvString(); 
        Test.StopTest();
    }  
    
    
        
                    
}