public without sharing class StudentAccountBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts{
    public enum JobType {RECURRING_PAYMENT_GENERATION, ANNUAL_STATEMENT_GENERATION,CUMULATIVE_PROGRAM_FUNDING, CLEARINGHOUSE, SUMMARIZE_INCOME, CALCULATE_AMOUNTS} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    Set<ID> accountIds = new Set<ID>();
    
    public StudentAccountBatch() {
    }
    
    public StudentAccountBatch(Set<ID> accountIds){
        this.accountIds = accountIds;
    }
    public StudentAccountBatch(String query) {
        this.query = query;
    }   
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('StudentAccountBatch.start()');
        if(job == JobType.RECURRING_PAYMENT_GENERATION){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Account WHERE RecordTypeID = \'' + GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') + '\'';
            }
        } 
        else if(job == JobType.ANNUAL_STATEMENT_GENERATION){
            if(String.isEmpty(this.query)){
                query = 'SELECT id,PersonContactID,PersonEmail from Account WHERE Receive_Annual_Statement__c = true AND RecordTypeID = \'' + GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') + '\'';
            }
        }
        else if(job == JobType.CUMULATIVE_PROGRAM_FUNDING){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Account where RecordTypeID = \''+String.valueOf(GlobalUtil.getRecordTypeIdByLabelName('Account', 'Student')+'\'');
            }
        }
        else if(job == JobType.CLEARINGHOUSE){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Account where RecordTypeID = \'' + GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') + '\'';
            }
        }
        else if(job == JobType.SUMMARIZE_INCOME){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Account where SummarizeIncome__c = true and RecordTypeID = \'' + GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') + '\'';
            }
        }
        else if(job == JobType.CALCULATE_AMOUNTS){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from Account where ID IN ' + DatabaseUtil.inSetStringBuilder(accountIds);// + ' AND RecordTypeID = \'' + GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') + '\'';
            }    
        }
        System.debug('job:'+job);
        System.debug('query:'+query);
        LogService.writeLogs();
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('StudentAccountBatch.execute()');
        System.debug(job);
        
        if(job == JobType.RECURRING_PAYMENT_GENERATION){
            Set<ID> scopeIDs = new Set<ID>();
            for(sObject sobj : scope){
                scopeIDs.add(sobj.id);
            }
    
            Set<String> preservicingAgreementStatus = new Set<String>{'Invited',
                                                                  'Application Incomplete',
                                                                  'Application Under Review',
                                                                  'Under Review',
                                                                  'Cancelled'};
            Set<String> closedStatus = new Set<String>{'Contract Satisfied',
                                                                      'Default',
                                                                      'Forgiven'};
            
            Map<ID, Account> studentMap = new Map<ID, Account>([SELECT id,
                                                                       RecordTypeID,
                                                                       AutoPayment__pc,
                                                                       AutoPaymentDateActivated__pc,
                                                                       AutoPaymentDayOfMonth1__pc,
                                                                       AutoPaymentDayOfMonth2__pc,
                                                                       AutoPaymentFrequency__pc,
                                                                       NextAutoPaymentDate__c,
                                                                       PersonContactID,
                                                                              (SELECT Student__c, Status__c
                                                                               FROM Student_Programs__r
                                                                               WHERE Status__c NOT IN :preservicingAgreementStatus
                                                                               and Status__c NOT IN :closedStatus)

                                                                    FROM Account
                                                                    WHERE RecordTypeID = :GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student')
                                                                    and id = :scopeIDs
                                                                    and AutoPayment__pc = true
                                                                    and (AutoPaymentFrequency__pc = '1 Time Per Month' OR AutoPaymentFrequency__pc = 'On Due Date')
                                                                    and AutoPaymentDayOfMonth1__pc != null
                                                                    and ID IN (SELECT Student__c 
                                                                               FROM StudentProgram__c
                                                                               WHERE Status__c NOT IN :preservicingAgreementStatus
                                                                               and Status__c NOT IN :closedStatus)]);
            System.debug('studentMap:'+studentMap);
            recurringPaymentGeneration(studentMap);
            LogService.writeLogs();
        } 
        else if(job == JobType.ANNUAL_STATEMENT_GENERATION){
            Set<ID> studentIDs = new Set<ID>();
            for(SObject obj : scope){
                studentIDs.add(obj.id);
            }
            sendAnnualStatements(studentIDs);
        }
        else if(job == JobType.CUMULATIVE_PROGRAM_FUNDING){
            cumulativeProgramFunding(scope);
        }
        else if(job == JobType.CLEARINGHOUSE){
            Set<ID> studentIDs = new Set<ID>();
            for(SObject obj : scope){
                studentIDs.add(obj.id);
            }
           Set<String> Schools = new Set<String>{'Berkeley College', 'Purdue', 'Clarkson University', 'Lackawanna College', 'Messiah College', 'Norwich', 'Point Loma'};
           Map<ID, Account> studentMap = new Map<ID, Account>([Select id, clearinghouse__c from Account where ID IN: studentIds AND RecordTypeID = :GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student') AND SchoolSecurity__c IN: Schools AND ID IN (Select Student__c from Transaction__c where schoolSecurity__c IN: Schools)]);
           set_ResetClearingHouseOnStudents(studentMap); 
        }
        else if(job == JobType.SUMMARIZE_INCOME){
            summarizeIncome(scope);
        }  
        else if(job == JobType.CALCULATE_AMOUNTS){
            Set<ID> accountIds = new Set<ID>();
            for(SObject obj : scope){
                accountIds.add(obj.id);
            }
            CalculateAmountsOnStudent(accountIds); 
            CalculateAmountsOnSchool(accountIds);   
        }      
        
        LogService.writeLogs(); 
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('StudentAccountBatch.finish()');
        LogService.writeLogs();
    }
    
    private static void sendAnnualStatements(Set<ID> studentIDs){
        Map<ID, Account> acctMap = AccountQueries.getStudentMapWithStudentID(studentIDs);
        
        List<OutboundEmail__c> emailToBeCreated = new List<OutboundEmail__c>();
        for(Account acct : acctMap.values()){
            emailToBeCreated.add(annualStatementEmail(acct));
            acct.Receive_Annual_Statement__c = false;
        } 
        if(emailToBeCreated != null && emailToBeCreated.size()>0){
            insert emailToBeCreated;
        }   
        upsert acctMap.values();
    } 
    
    private static OutboundEmail__c annualStatementEmail(Account student){
         
        EmailTemplate template = [Select id from EmailTemplate where DeveloperName =: 'Annual_Account_Statement'];
        
        if(template != null){
            return new OutboundEmail__c(ToAddresses__c = student.PersonEmail,
                                      TemplateID__c = template.id,
                                      WhatID__c = student.id,
                                      TargetObjectId__c = student.PersonContactID,
                                      SendviaSES__c = true);
        }
        else
            return null;
    }
    
    private static void recurringPaymentGeneration(Map<ID, Account> studentMap){
        System.debug('StudentAccountBatch.recurringPaymentGenration()');
        List<PaymentInstruction__c> paymentsToCreate = new List<PaymentInstruction__c>();
        List<Case> casesToCreate = new List<Case>();
        Map<ID, Account> accountsToUpdate = new Map<ID, Account>();
        Map<ID, PaymentMethod__c> paymentMethodMap = PaymentMethodQueries.getPaymentMethodMapWithCustomerID(studentMap.keySet());
        Map<ID, Integer> studentDelinquencyMap = StudentService.getStudentDelinquencyMap(studentMap.keyset());
        Map<ID, List<PaymentMethod__c>> paymentMethodsByStudentID = new Map<ID, List<PaymentMethod__c>>();
        for(PaymentMethod__c paymentMethod : paymentMethodMap.values()){
            if(paymentMethod.UseForRecurring__c){
                if(!paymentMethodsByStudentID.containsKey(paymentMethod.Customer__c)){
                    paymentMethodsByStudentID.put(paymentMethod.Customer__c, new List<PaymentMethod__c>());
                }
                paymentMethodsByStudentID.get(paymentMethod.Customer__c).add(paymentMethod);
            }
        }

        Set<ID> acctsToCreatePayment = new Set<ID>();
        for(Account acct : studentMap.values()){
            System.debug('Account:'+acct);
            System.debug('Open Agreements:'+acct.Student_Programs__r);
            if(paymentMethodsByStudentID.containsKey(acct.id)){
                System.debug('Recurring Payment:'+paymentMethodsByStudentID.get(acct.id)[0]);
                acctsToCreatePayment.add(acct.id);          
            } else {
                casesToCreate.add(new Case(Type = 'Problem',
                                           Origin = 'Other',
                                           Priority = 'High',
                                           ContactID = studentMap.get(acct.id).PersonContactID,
                                           Subject = 'No Recurring Payment Method Found for Account '+acct.id));
                //throw new StudentAccountBatchException('No Payment Method found for '+acct.id);
            }
        }
        Map<ID, PaymentInstruction__c> paymentMap = PaymentInstructionQueries.getPaymentInstructionMapWithPaymentStudentID(acctsToCreatePayment);
        Map<ID, List<PaymentInstruction__c>> paymentsByStudent = new Map<ID, List<PaymentInstruction__c>>();
        for(PaymentInstruction__c payment : paymentMap.values()){
            Set<String> activePayments = new Set<String>{'Created By Student',
                                                            'Automatically Created',
                                                            'Scheduled',
                                                            'Processing',
                                                            'Processed by Vemo'};
            if(activePayments.contains(payment.Status__c)){
                if(!paymentsByStudent.containsKey(payment.Student__c)){
                    paymentsByStudent.put(payment.Student__c, new List<PaymentInstruction__c>());
                }
                paymentsByStudent.get(payment.Student__c).add(payment);             
            }

        }

        Map<ID, StatementService.StatementV1> statements = StatementService.getCurrentStatementV1ByStudentWithStudentID(acctsToCreatePayment);
        Map<ID, Decimal> amountDueByStudentID = new Map<ID, Decimal>();
        

        Date systemDate = Date.today();//for testing
        //systemDate = Date.newInstance(2018, 4, 16);//for testing
        
        for(ID theID : acctsToCreatePayment){
            if(statements.containsKey(theID) && statements.get(theID).totalAmountDue > 0){
                System.debug('Statement '+statements.get(theID));
                amountDueByStudentID.put(theID, statements.get(theID).totalAmountDue);
            }
            if(paymentsByStudent.containsKey(theID)){
                System.debug('Student '+theID + ' has an existing Payment Instruction.  Skipping');
            } else if(!amountDueByStudentID.containsKey(theID)){
                System.debug('Student '+theID + ' has no total amount due.  Skipping');
            } else if((studentMap.get(theID).NextAutoPaymentDate__c <= Date.today()) ||
                      (studentMap.get(theID).NextAutoPaymentDate__c == null) ||
                      (studentDelinquencyMap.containsKey(theID) && studentDelinquencyMap.get(theID)>30)){

                System.debug('Create');
                System.debug('studentMap.get(theID).NextAutoPaymentDate__c'+studentMap.get(theID).NextAutoPaymentDate__c);
                try{
                    System.debug('delinquency:'+studentDelinquencyMap.get(theID));                  
                } catch (Exception e){
                    System.debug('no delinquency');
                }

                paymentsToCreate.add(new PaymentInstruction__c(Student__c = theID,
                                                               PaymentMethod__c = paymentMethodsByStudentID.get(theID)[0].id,
                                                               Status__c = 'Automatically Created',
                                                               TransactionDirection__c = 'Inbound',
                                                               Description__c = 'Auto Payment',
                                                               Date__c = systemDate,
                                                               Amount__c = amountDueByStudentID.get(theID)));

                Boolean advanceAutoPaymentDate = false; 
                if(studentMap.get(theID).NextAutoPaymentDate__c <= systemDate || studentMap.get(theID).NextAutoPaymentDate__c == null) advanceAutoPaymentDate = true;
                if(advanceAutoPaymentDate){
                    Integer nextPaymentYear = (systemDate.month() == 12) ? systemDate.year() + 1 : systemDate.year();
                    Integer nextPaymentMonth = (systemDate.month() == 12) ? 1 : systemDate.month() + 1;
                    Integer nextPaymentDay = Integer.valueOf(studentMap.get(theID).AutoPaymentDayOfMonth1__pc);
                    accountsToUpdate.put(theID, new Account(id = theID, 
                                                     NextAutoPaymentDate__c = Date.newInstance(nextPaymentYear, nextPaymentMonth, nextPaymentDay)));                
                }               
            } else {
                System.debug(theID + ' does Not meet the criteria to create payment instructions');
            }
        }
        if(paymentsToCreate.size()>0){
            insert paymentsToCreate;
        }
        if(casesToCreate.size()>0){
            Database.insert(casesToCreate, false);
        }
        if(accountsToUpdate.size()>0){
            Database.update(accountsToUpdate.values(), false);          
        }

        LogService.writeLogs();
    }
    
    private static void cumulativeProgramFunding(List<sObject> scope){
        Map<ID, sObject> scopeMap = new Map<ID, sObject>(scope);
        List<Account> acctsToUpdate = new List<Account>();
        //Get All Students as the main loop
        //Boolean debug = debugScope;
        Boolean debug = true;
        //Boolean debug = false;
        for(List<Account> students : [SELECT id,
                                             SchoolProgramOfStudy__pc,
                                             PrimarySchoolGradeLevel__pc
                                      FROM Account
                                      where ID = :scopeMap.keySet()
                                      and RecordTypeID = :GlobalUtil.getRecordTypeIdByLabelName('Account', 'Student')]){
            Map<ID, Account> studentMap = new Map<ID, Account>(students);
            if(debug) System.debug(studentMap);
            //Get All Student Programs and Sort them By Programs
            Map<ID, StudentProgram__c> agreementMap = new Map<ID, StudentProgram__c>([SELECT id,
                                                                                             Student__c,
                                                                                             Program__c,
                                                                                             Status__c,
                                                                                             FundingAmountPostCertification__c
                                                                                      FROM StudentProgram__c
                                                                                      Where Student__c in : studentMap.keySet()]);
            if(debug) System.debug(agreementMap);

            Map<ID, Map<ID, List<StudentProgram__c>>> studentProgramsByProgramByStudent = new Map<ID, Map<ID,List<StudentProgram__c>>>();

            for(StudentProgram__c agreement : agreementMap.values()){
                if(!StudentProgramTriggerHandler.cumulativeIncomeStatusExclusions.contains(agreement.Status__c)){


                    if(!studentProgramsByProgramByStudent.containsKey(agreement.Student__c)){
                        studentProgramsByProgramByStudent.put(agreement.Student__c, new Map<ID, List<StudentProgram__c>>());
                    }
                    if(!studentProgramsByProgramByStudent.get(agreement.Student__c).containsKey(agreement.Program__c)){
                        studentProgramsByProgramByStudent.get(agreement.Student__c).put(agreement.Program__c, new List<StudentProgram__c>());
                    }
                    studentProgramsByProgramByStudent.get(agreement.Student__c).get(agreement.Program__c).add(agreement);
                }
            }
            if(debug) System.debug(studentProgramsByProgramByStudent);


            for(Account student : students){
                if(debug) System.debug(student);
                if(studentProgramsByProgramByStudent.containsKey(student.id)){
                    for(ID programID : studentProgramsByProgramByStudent.get(student.id).keySet()){
                        //Get the program terms for this student and this program
                        if(debug) System.debug(programID);
                        if(debug) System.debug(student.SchoolProgramOfStudy__pc);
                        if(debug) System.debug(student.PrimarySchoolGradeLevel__pc);
                        if(student.SchoolProgramOfStudy__pc != null && student.PrimarySchoolGradeLevel__pc != null){
                            ContractTermsService.ContractTerm ct = ContractTermsService.getContractTermsWithCriteria(programId, student.SchoolProgramOfStudy__pc, student.PrimarySchoolGradeLevel__pc);
                            if(debug) System.debug(ct);
                            if(debug) System.debug('Maximum Funding Amount:'+ct.maximumFundingAmount);

                            Decimal cumulativeFunding = 0;
                            for(StudentProgram__c agreement : studentProgramsByProgramByStudent.get(student.id).get(programID)){
                                if(debug) System.debug(agreement);
                                cumulativeFunding += (agreement.FundingAmountPostCertification__c == null ? 0 : agreement.FundingAmountPostCertification__c);

                            }
                            if(debug) System.debug('Cumulative Funding:'+cumulativeFunding);
                            if(cumulativeFunding > ct.maximumFundingAmount){
                                String error = '***Overfunded Account:'+student.id+ ' with Program:'+programID+' (maximumFundingAmount='+ct.maximumFundingAmount+') and Cumulative Funding of '+cumulativeFunding;
                                System.debug(error);
                                acctsToUpdate.add(new Account(id = student.id,
                                                              Error__c = error));

                            }
                        }
                    }
                }
            }
        }
        if (acctsToUpdate.size()>0) {
            if(debug) System.debug('acctsToUpdate:'+acctsToUpdate);
            update acctsToUpdate;           
        }
    }
    
    private static void set_ResetClearingHouseOnStudents(Map <ID, Account> studentMap){
        Set<String> ClosedStatus = new Set<String>{'Contract Satisfied', 'Cancelled'};
        Map<ID, studentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMapWithStudentID(studentMap.keySet());
        Map<ID, List<StudentProgram__c>> agreementByStudentId = new Map<ID, List<StudentProgram__c>>();
        for(StudentProgram__c sp : agreementMap.values()){
            if(!agreementByStudentId.containsKey(sp.Student__c)){
                agreementByStudentId.put(sp.Student__c, new List<StudentProgram__c>());
            }
            agreementByStudentId.get(sp.Student__c).add(sp);
        }
        
        for(Id i: agreementByStudentId.keyset()){
            boolean setClearinghouseFlag = false;
            for(studentProgram__c s: agreementByStudentId.get(i)){
                if(!ClosedStatus.contains(s.status__c)){
                    setClearinghouseFlag = true;
                }
                //else
                    //setClearinghouseFlag = false;
            }
            if(setClearinghouseFlag == true)
                studentMap.get(i).clearingHouse__c = true;     
            else
                studentMap.get(i).clearingHouse__c = false;     
            
        }
        
        if(studentMap.size()>0)
           update studentMap.values();
    }
    
    private static void summarizeIncome(List<sObject> scope){
        //Event types for which we don't need to divide the Monthly Income by TotalSalaryDay
        Set<String> addAllAmountEventTypes = new Set<String>{
            //'BONUS_COMPENSATION', 'COMMISSION_COMPENSATION', 'TIPS_COMPENSATION'
            'Additional compensation'
        };
        Map<ID, Account> acctMapToUpdate = new Map<ID, Account>();
        Set<ID> acctIDs = new Set<ID>();
        for(Account acct : (List<Account>) scope){
            System.debug(acct);
            acctIDs.add(acct.id);
        }
        Map<ID, EmploymentHistory2__c> employmentHistoryMap = new Map<ID, EmploymentHistory2__c>([SELECT id, EffectiveDate__c, EventType__c, EmploymentEndDate__c, MonthlyIncomeTotal__c, Student__c from EmploymentHistory2__c where Status__c = 'Verified' /*Verified__c = true*/ and Student__c IN :acctIDs ]);
        Map<ID, List<EmploymentHistory2__c>> employmentHistByStudentID = new Map<ID, List<EmploymentHistory2__c>>();
        for(EmploymentHistory2__c empHist : employmentHistoryMap.values()){
            if(!employmentHistByStudentID.containsKey(empHist.Student__c)){
                employmentHistByStudentID.put(empHist.Student__c, new List<EmploymentHistory2__c>());
            }
            employmentHistByStudentID.get(empHist.Student__c).add(empHist);
        }
        System.debug('employmentHistByStudentID:'+employmentHistByStudentID);

        Map<Id,IncomeVerification__c> existingIncomeVerification = new Map<ID, IncomeVerification__c>([SELECT id, BeginDate__c, EndDate__c, Student__c FROM IncomeVerification__c where Student__c IN :acctIDs order by EndDate__c desc]);
        List<IncomeVerification__c> incomeVerificaitonToCreate = new List<IncomeVerification__c>();
        Map<ID, List<IncomeVerification__c>> incomeVerificationByStudentID = new Map<ID, List<IncomeVerification__c>>();
        for(IncomeVerification__c incVer : existingIncomeVerification.values()){
            if(!incomeVerificationByStudentID.containsKey(incVer.Student__c)){
                incomeVerificationByStudentID.put(incVer.Student__c, new List<IncomeVerification__c>());
            }
            incomeVerificationByStudentID.get(incVer.Student__c).add(incVer);
        }
        System.debug('incomeVerificationByStudentID:'+incomeVerificationByStudentID);

        List<IncomeVerificationDetail__c> incomeVerificationDetailsToCreate = new List<IncomeVerificationDetail__c>();
        Map<ID, List<IncomeVerificationDetail__c>> studentIncomeVerificationDetailsToAdd = new Map<ID, List<IncomeVerificationDetail__c>>();

        for(Account acct : (List<Account>) scope){
            System.debug('student:'+acct);
            Boolean keepGoing = true;


            if(incomeVerificationByStudentID.containsKey(acct.id)){
                if(incomeVerificationByStudentID.get(acct.id)[0].BeginDate__c.month() == Date.today().month()){
                    System.debug('already an income verification for this month for StudentID:'+acct.id);
                    keepGoing = false;
                }
            }

            if(!employmentHistByStudentID.containsKey(acct.id)){
                keepGoing = false;
                System.debug('No Employment History for StudentID:'+acct.id);
            }
            if(keepGoing){
                String guid = GlobalUtil.createGUID();
                if(!studentIncomeVerificationDetailsToAdd.containsKey(acct.id)){
                    studentIncomeVerificationDetailsToAdd.put(acct.id, new List<IncomeVerificationDetail__c>());
                }
                Decimal incomeForMonth = 0;
                for(EmploymentHistory2__c empHist : employmentHistByStudentID.get(acct.id)){
                    System.debug('empHist:'+empHist);
                    Boolean useEmpHist = true;
                    Date lastDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month(), 1).addMonths(1).addDays(-1);
                    Date firstDayOfMonth = Date.newInstance(Date.today().year(), Date.today().month(), 1);
                    if(empHist.EmploymentEndDate__c != null && empHist.EmploymentEndDate__c < firstDayOfMonth){
                        useEmpHist = false;
                        System.debug('End Date is in past month:'+empHist.id);
                    }
                    if(empHist.EffectiveDate__c == null){
                        useEmpHist = false;
                        System.debug('Effective Date is not defined:'+empHist.id);
                    }
                    if(useEmpHist == true){
                        Date salaryStartDate = empHist.EffectiveDate__c;
                        if(salaryStartDate < firstDayOfMonth){
                            salaryStartDate = firstDayOfMonth;
                        }
                        Date salaryEndDate = empHist.EmploymentEndDate__c;
                        if(salaryEndDate == null || salaryEndDate > lastDayOfMonth){
                            salaryEndDate = lastDayOfMonth;
                        }
                        Integer totalSalaryDay = salaryStartDate.daysBetween(salaryEndDate) + 1;
                        
                        //We don't need to divide the Amount by totalSalaryDay if it is a bonus,  commission or tip.
                        if(empHist.EventType__c != null && addAllAmountEventTypes.contains(empHist.EventType__c)){
                            totalSalaryDay = date.daysInMonth(Date.today().year(), Date.today().month());
                        }
                        system.debug('totalSalaryDay--'+totalSalaryDay);
                        incomeForMonth = incomeForMonth + (empHist.MonthlyIncomeTotal__c /date.daysInMonth(Date.today().year(), Date.today().month()))*totalSalaryDay;
                        IncomeVerificationDetail__c incVerDtl = new IncomeVerificationDetail__c();
                        incVerDtl.EmploymentHistory2__c = empHist.id;
                        IncomeVerification__c incVer = new IncomeVerification__c();
                        incVer.Guid__c = guid;
                        incVerDtl.IncomeVerification__r = incVer;
                        studentIncomeVerificationDetailsToAdd.get(acct.id).add(incVerDtl);                        
                    }
                }
                if(studentIncomeVerificationDetailsToAdd.containsKey(acct.id) && studentIncomeVerificationDetailsToAdd.get(acct.id).size()>0){
                    keepGoing = true;
                } else {
                    keepGoing = false;
                    System.debug('No Employment History Found for Current Month');
                }

                if(keepGoing){

                    IncomeVerification__c newIncomeVerification = new IncomeVerification__c();
                    Date nextBeginDate = Date.newInstance(Date.Today().year(), Date.Today().month(), 1);
                    Date nextEndDate = nextBeginDate.addMonths(1).addDays(-1);
                    newIncomeVerification.BeginDate__c = nextBeginDate;
                    newIncomeVerification.EndDate__c = nextEndDate;
                    newIncomeVerification.Student__c = acct.id;
                    newIncomeVerification.DateVerified__c = Date.today();
                    newIncomeVerification.Status__c = 'Verified';
                    newIncomeVerification.Verified__c = true;
                    newIncomeVerification.Type__c = 'Reported';
                    newIncomeVerification.IncomePerMonth__c = incomeForMonth;


                    newIncomeVerification.Guid__c = guid;

                    incomeVerificaitonToCreate.add(newIncomeVerification);

                    if(studentIncomeVerificationDetailsToAdd.containsKey(acct.id)){
                        incomeVerificationDetailsToCreate.addAll(studentIncomeVerificationDetailsToAdd.get(acct.id));                    
                    }
                }
            }
            acctMapToUpdate.put(acct.id, new Account(id = acct.id,
                                                     SummarizeIncome__c = false));

        }
        if(incomeVerificaitonToCreate.size()>0){
            insert incomeVerificaitonToCreate;
        }
        if(incomeVerificationDetailsToCreate.size()>0){
            insert incomeVerificationDetailsToCreate;
        }
        if(acctMapToUpdate.size()>0){
            update acctMapToUpdate.values();
        }
    }
    
    private static void CalculateAmountsOnStudent(Set<ID> studentIDs){
        Map<ID, Account> StudentsWithID = AccountQueries.getStudentMapWithStudentID(studentIDs);
        //Map<ID, StudentProgram__c> AgreementMapWithStudentID = StudentProgramQueries.getStudentProgramMapWithStudentID(studentIDs); 
        Map<ID, StudentProgram__c> AgreementMapWithStudentID = new Map<ID, StudentProgram__c>([Select id, Student__c, Status__c, Program__c, CertificationDate__c , FundingAmountPostCertification__c, AmountDisbursedTodate__c, CalculateAmounts__c from StudentProgram__c where Student__c IN: studentIDs]);
        Map<ID, List<Transaction__c>> transByAgreementId = new Map<ID, List<Transaction__c>>();
        Map<ID, Transaction__c> transById = new Map<ID, Transaction__c>([Select id, Agreement__c, RecordType.Name, Confirmed__c, Status__c, Amount__c  from Transaction__c where Agreement__c IN: AgreementMapWithStudentID.KeySet()]);
        
        //if(AgreementMapWithStudentID != null){
        //    transByAgreementId  = TransactionQueries.getTransactionMapByAgreementWithAgreementID(AgreementMapWithStudentID.keySet(), 'Disbursement');
        //}
        
        for(Transaction__c tx: transById.values()){
            if(!transByAgreementId.containsKey(tx.Agreement__c)){
                transByAgreementId.put(tx.Agreement__c, new List<Transaction__c>());
            }
            transByAgreementId.get(tx.Agreement__c).add(tx);
        }
        
        Map<ID, List<StudentProgram__c>> AgreementMapByStudentID = new Map<ID, List<StudentProgram__c>>();
        
        List<Account> studentsToUpdate = new List<Account>();
        
        if(AgreementMapWithStudentID != null){
            for(StudentProgram__c agreement: AgreementMapWithStudentID.values()){
                if(!AgreementMapByStudentID.containsKey(agreement.Student__c)){
                    AgreementMapByStudentID.put(agreement.Student__c, new List<StudentProgram__c>()); 
                }
                AgreementMapByStudentID.get(agreement.Student__c).add(agreement);
            }
        }
        
        for(Account stud: StudentsWithID.values()){
            Decimal CertifiedAmount = 0;
            Decimal DisbursedAmount = 0;
            if(AgreementMapByStudentID.containsKey(stud.Id)){
                for(StudentProgram__c sp: AgreementMapByStudentID.get(stud.Id)){
                    if(sp.CertificationDate__c != null && sp.Status__c != 'Cancelled'){
                        if(sp.FundingAmountPostCertification__c != null) CertifiedAmount += sp.FundingAmountPostCertification__c;
                    }
                    
                    if(transByAgreementID != null && transByAgreementID.containsKey(sp.id)){ 
                        for(Transaction__c tx: transByAgreementID.get(sp.id)){
                            if(tx.RecordType.Name == 'Disbursement' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;    
                            }
                            else if(tx.RecordType.Name == 'Disbursement Refund' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                    if(tx.Amount__c != null) DisbursedAmount -= tx.Amount__c;
                            } 
                            
                        }
                    }
                }
            }
            
            studentsToUpdate.add(new Account(id = stud.Id,
                                                AmountCertifiedToDate__c = CertifiedAmount,
                                                AmountDisbursedToDate__c = DisbursedAmount,
                                                CalculateAmounts__c = false)); 
        }
        
        if(studentsToUpdate.size()>0)
            update studentsToUpdate;
    }
    
    private static void CalculateAmountsOnSchool(Set<ID> schoolIDs){
        Map<ID, Account> SchoolsWithID = AccountQueries.getSchoolMapWithSchoolID(schoolIDs);
        //Map<ID, Account> StudentsWithID = AccountQueries.getStudentMapWithSchoolID(parentIds);
        //Map<ID, StudentProgram__c> AgreementMapWithSchoolID = StudentProgramQueries.getStudentProgramMapWithSchoolID(schoolIDs);
        Map<Id, StudentProgram__c> AgreementMapWithSchoolID = new Map<ID, StudentProgram__c>([Select id, Status__c, Program__c, Program__r.School__c, CertificationDate__c , FundingAmountPostCertification__c, AmountDisbursedTodate__c, CalculateAmounts__c from StudentProgram__c where Program__r.School__c IN: schoolIDs]);
        Map<ID, Transaction__c> transById = new Map<ID, Transaction__c>([Select id, Agreement__c, RecordType.Name, Confirmed__c, Status__c, Amount__c  from Transaction__c where Agreement__c IN: AgreementMapWithSchoolID.keySet()]);
        Map<ID, List<Transaction__c>> transByAgreementId = new Map<ID, List<Transaction__c>>();
        
       // if(AgreementMapWithSchoolID != null){
       //     transByAgreementId  = TransactionQueries.getTransactionMapByAgreementWithAgreementID(AgreementMapWithSchoolID.keySet(), 'Disbursement');
       // }
       
       for(Transaction__c tx: transById.values()){
            if(!transByAgreementId.containsKey(tx.Agreement__c)){
                transByAgreementId.put(tx.Agreement__c, new List<Transaction__c>());
            }
            transByAgreementId.get(tx.Agreement__c).add(tx);
       }
        
        Map<ID, List<StudentProgram__c>> AgreementMapBySchoolID = new Map<ID, List<StudentProgram__c>>();
        
        List<Account> schoolsToUpdate = new List<Account>();
        
        if(AgreementMapWithSchoolID != null){
            for(StudentProgram__c agreement: AgreementMapWithSchoolID.values()){
                if(!AgreementMapBySchoolID.containsKey(agreement.Program__r.School__c)){
                    AgreementMapBySchoolID.put(agreement.Program__r.School__c, new List<StudentProgram__c>()); 
                }
                AgreementMapBySchoolID.get(agreement.Program__r.School__c).add(agreement);
            }
        }
        
        for(Account sch: SchoolsWithID.values()){
            Decimal CertifiedAmount = 0;
            Decimal DisbursedAmount = 0;
            if(AgreementMapBySchoolID.containsKey(sch.Id)){
                for(StudentProgram__c sp: AgreementMapBySchoolID.get(sch.Id)){
                    if(sp.CertificationDate__c != null && sp.Status__c != 'Cancelled'){
                        if(sp.FundingAmountPostCertification__c != null) CertifiedAmount += sp.FundingAmountPostCertification__c;
                    }
                    
                    if(transByAgreementID != null && transByAgreementID.containsKey(sp.id)){ 
                        for(Transaction__c tx: transByAgreementID.get(sp.id)){
                            if(tx.RecordType.Name == 'Disbursement' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;    
                            }
                            else if(tx.RecordType.Name == 'Disbursement Refund' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                if(tx.Amount__c != null) DisbursedAmount -= tx.Amount__c;
                            }
                            
                        }
                    }
                }
            }
            
            schoolsToUpdate.add(new Account(id = sch.Id,
                                                AmountCertifiedToDate__c = CertifiedAmount,
                                                AmountDisbursedToDate__c = DisbursedAmount,
                                                CalculateAmounts__c = false)); 
        }
        
        if(schoolsToUpdate.size()>0)
            update schoolsToUpdate;
    }

    public class StudentAccountBatchException extends Exception {}
}