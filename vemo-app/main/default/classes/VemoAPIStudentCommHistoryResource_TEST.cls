@isTest
public class VemoAPIStudentCommHistoryResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    static testMethod void testHandleGetV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Contact> testContactMap = TestDataFactory.createAndInsertContacts(10,testStudentAccountMap);
        Map<ID, Notification__c> testNotificationMap = TestDataFactory.createAndInsertNotification(1, testContactMap);
        
        Map<String, String> notificationParams = new Map<String, String>();
        notificationParams.put('notificationID', TestUtil.createStringFromIDSet(testNotificationMap.keySet()));
        notificationParams.put('VEMO_AUTH', 'ABC');
        notificationParams.put('type', 'Email');
        VemoAPI.APIInfo notificationApiInfo = TestUtil.initializeAPI('v2', 'GET', notificationParams, null);

        Map<String, String> studentParams = new Map<String, String>();
        studentParams.put('studentID', TestUtil.createStringFromIDSet(testStudentAccountMap.keyset()));
        studentParams.put('VEMO_AUTH', 'ABC');
        studentParams.put('type', 'Email');
        VemoAPI.APIInfo commStudentAPIInfo = TestUtil.initializeAPI('v2', 'GET', studentParams, null);

        Test.startTest();
        VemoAPI.ResultResponse notificationResult = (VemoAPI.ResultResponse)VemoAPIStudentCommHistoryResource.handleAPI(notificationApiInfo);

        VemoAPI.ResultResponse commStudentResult = (VemoAPI.ResultResponse)VemoAPIStudentCommHistoryResource.handleAPI(commStudentAPIInfo);
        Test.stopTest();
    }
}