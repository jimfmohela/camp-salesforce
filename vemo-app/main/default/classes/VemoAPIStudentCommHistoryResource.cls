public class VemoAPIStudentCommHistoryResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
        }
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentCommHistoryResource.handleGetV2()');
        
        String studentIDparam = api.params.get('studentID');
        String notificationIDparam = api.params.get('notificationID');
        String typeparam = api.params.get('type');
        
        List<NotificationService.Notification> notificationList = new List<NotificationService.Notification>();
        
        if(typeParam != null){
            if(studentIdParam != null)
                notificationList = NotificationService.getNotificationWithStudentIDNType(VemoApi.parseParameterIntoIDSet(studentIdParam ),typeParam);
            else if(notificationIdParam != null)
                notificationList = NotificationService.getNotificationWithNotificationIDNType(VemoApi.parseParameterIntoIDSet(notificationIDparam),typeParam );
            
            else
                throw new VemoAPI.VemoAPIFaultException('Missing parameter: studentID or notificationID required for GET');
        }
        else
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: type required for GET');
        
        
        List<CommunicationHistory> results = new List<CommunicationHistory>();
        for(NotificationService.Notification notify : notificationList){
            CommunicationHistory ch = new CommunicationHistory(notify,typeParam);
            ch.emailResource = new EmailResource(notify);
            
            //if(notificationIdParam != null)
                
            results.add(ch);
        } 
        
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public class CommunicationHistory{
        public String studentCommunicationID{get;set;} 
        public String type {get;set;}
        public String studentID {get;set;}
        public Datetime communicationDateTime{get;set;}
        public EmailResource emailResource{get;set;}
        
        public CommunicationHistory(NotificationService.Notification notification, string type){
            this.studentCommunicationID = notification.notificationID ;
            this.type = type;
            this.studentId = notification.contact;    
            this.communicationDateTime = notification.createdDate;
        }
    }
    
    public virtual class EmailResource{
        public String notificationID{get;set;}    
        public String subject {get;set;}
        public String htmlBody {get;set;}
        public String plainText {get;set;}
        
        public EmailResource(NotificationService.Notification notification){
            this.notificationId = notification.notificationID ;
            this.subject = notification.subject;
            this.htmlBody = notification.htmlBody ;    
            this.plainText = notification.plainText;
        }
    }
}