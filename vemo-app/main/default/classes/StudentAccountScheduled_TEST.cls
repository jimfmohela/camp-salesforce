@isTest
public with sharing class StudentAccountScheduled_TEST {
	
    @isTest
    static void recurringPaymentGeneartionSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        StudentAccountScheduled BatchJob = new StudentAccountScheduled();
        BatchJob.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        System.schedule('StudentAccount_Batch_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
    
    @isTest
    static void annualStatementGeneartionSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        StudentAccountScheduled BatchJob = new StudentAccountScheduled();
        BatchJob.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        System.schedule('StudentAccount_Batch_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
    
    @isTest
    static void cumulativeProgramFundingSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        StudentAccountScheduled BatchJob = new StudentAccountScheduled();
        BatchJob.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
        System.schedule('StudentAccount_Batch_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
}