/**************************** Details**********************

Class Name            :  OutboundEmailImmediateQueueable
Associated Objects    :  Outbound Email
Details               :  Queueable job which send the emails (workflows) immediately. It is called from 
                         StudentProgramTriggerHandler
Developed by          :  Kamini Singh
Any Comment           :  Done
created date          :  4th Sep 2018
Last modified date    :  14th Sep 2018
*********************************************************/
public class OutboundEmailImmediateQueueable implements Queueable, Database.AllowsCallouts  {

    public set<Id> outboundEmailIds ; //Ids of outbound email records which needs to be send immediately

    public OutboundEmailImmediateQueueable(set<Id> outboundEmailIds){
        this.outboundEmailIds = outboundEmailIds;
    }

    public void execute(QueueableContext context) {
        if(outboundEmailIds != null && outboundEmailIds.size() > 0){
            
            String query = 'SELECT Id, TemplateID__c, TargetObjectId__c, whatId__c, toaddresses__c, CcAddresses__c, bccAddresses__c, PlainTextBody__c, HtmlBody__c, Subject__c ';
            query += 'From OutboundEmail__c Where SendviaSES__c = true AND Complete__c = false AND Id In : outboundEmailIds';
            
            List<OutboundEmail__c> outboundEmailList = new List<OutboundEmail__c>();
            outboundEmailList = Database.query(query);
            
            if(outboundEmailList.size() > 0){
                OutboundEmailService oService = new OutboundEmailService(); // call outbound service class to send the mails
                oService.sendEmail(outboundEmailList);
            }
        }
    }

}