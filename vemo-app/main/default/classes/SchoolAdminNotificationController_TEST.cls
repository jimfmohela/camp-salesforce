@isTest
public class SchoolAdminNotificationController_TEST{
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    public static void testSchoolAdminNotificationController(){
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(students, EventTypes);
        
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: students.values()){
            contactIds.add(acc.PersonContactId);
        }
        
        for(EventType__c et: eventTypes.values()){
            et.MergeObject__c = students.values().get(0).ID;
            et.MergeTemplate__c = '0000';
        }
        update eventTypes.values();
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, Contact> testContactMapForEI = new Map<Id, Contact>{
            testContactMap.values()[0].Id => testContactMap.values()[0]
        };
            Map<Id, Event__c> EventsForEI = new Map<Id, Event__c>{
            Events.values()[0].Id => Events.values()[0]
        };
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMapForEI, EventsForEI );
                
        Test.StartTest();
        SchoolAdminNotificationController saNotification = new SchoolAdminNotificationController();
        saNotification.eventInstanceId = testEventInstanceMap.values()[0].id;
        saNotification.getLoadData();
        Test.StopTest();
    }
}