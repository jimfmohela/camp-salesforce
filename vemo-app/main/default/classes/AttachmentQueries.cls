public class AttachmentQueries {
    public static Map<ID, Attachment> getAttachmentMap(){
        String query = generateSOQLSelect();
        query += ' '+ 'LIMIT 50000';
        DatabaseUtil db = new DatabaseUtil();
        return new Map<ID, Attachment>((List<Attachment>)db.query(query));
    }

    public static Map<ID, Attachment> getAttachmentMapWithAttachmentID(Set<ID> attachmentIDs){
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(attachmentIDs);
        query += ' '+ 'LIMIT 50000';
        DatabaseUtil db = new DatabaseUtil();
        return new Map<ID, Attachment>((List<Attachment>)db.query(query));
    }

    public static Map<ID, Attachment> getAttachmentMapWithParentID(Set<ID> parentIDs){
        String query = generateSOQLSelect();
        query += ' WHERE parentId IN ' + DatabaseUtil.inSetStringBuilder(parentIDs);
        query += ' '+ 'LIMIT 50000';
        DatabaseUtil db = new DatabaseUtil();
        return new Map<ID, Attachment>((List<Attachment>)db.query(query));
    }
    
    private static String generateSOQLSelect(){
        string query = 'SELECT ' + getAttachmentFieldNames() + ' FROM Attachment';
        return query;
    }

    private static String getAttachmentFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Body, ';
        fieldNames += 'ContentType, ';
        fieldNames += 'Name, ';
        fieldNames += 'ParentId';
        return fieldNames;
    }
}