/////////////////////////////////////////////////////////////////////////
// Class: DefermentQueries
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-09-23   Kamini Singh       Created                                 
// 
/////////////////////////////////////////////////////////////////////////
public class DefermentQueries {
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }
    
    public static Map<ID, Deferment__c> getDefermentMapWithDefermentID(Set<ID> defermentIDs){
        Map<ID, Deferment__c> defermentMap = new Map<ID, Deferment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(defermentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        defermentMap = new Map<ID, Deferment__c>((List<Deferment__c>)db.query(query));
        return defermentMap;  
    }

    public static Map<ID, Deferment__c> getDefermentMapWithStudentID(Set<ID> studentIDs){
        Map<ID, Deferment__c> defermentMap = new Map<ID, Deferment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c  = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        defermentMap = new Map<ID, Deferment__c>((List<Deferment__c>)db.query(query));
        return defermentMap;
    }

    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM Deferment__c ';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'RecordTypeID, ';
        fieldNames += 'RecordType.DeveloperName, ';
        fieldNames += 'AnnualIncome__c, ';
        fieldNames += 'AverageHoursPerWeek__c, ';
        fieldNames += 'AverageSalaryPerMonth__c, ';
        fieldNames += 'BeginDate__c, ';
        fieldNames += 'CityEmployment__c, ';
        //fieldNames += 'ContactDate__c, ';
        fieldNames += 'CountryEmployment__c, ';
        fieldNames += 'CountrySchool__c, ';
        fieldNames += 'DateReported__c, ';
        fieldNames += 'DateRequestSubmitted__c, ';
        fieldNames += 'DateVerified__c, ';
        fieldNames += 'EmployerAddress__c, ';
        fieldNames += 'EmployerName__c, ';
        fieldNames += 'EndDate__c, ';
        fieldNames += 'EnrollmentBeginDate__c, ';
        fieldNames += 'EnrollmentEndDate__c, ';
        fieldNames += 'EnrollmentStatus__c, ';
        //fieldNames += 'Explanation__c, ';
        fieldNames += 'Explanation1__c, ';
        //fieldNames += 'FollowUpDate__c, ';
        fieldNames += 'HourlyCompensation__c, ';
        fieldNames += 'MonthlyIncome__c, ';
        fieldNames += 'NoPaymentPaymentTerm__c, ';
        fieldNames += 'ReceivingUnemploymentBenefits__c, ';
        fieldNames += 'ResultsOutcome__c, ';
        fieldNames += 'SchoolAddress__c, ';
        fieldNames += 'SchoolName__c, ';
        fieldNames += 'SchoolPhone__c, ';
        fieldNames += 'ServicingCommentsNotes__c, ';
        fieldNames += 'StateEmployment__c, ';
        fieldNames += 'StateSchool__c, ';
        fieldNames += 'Status__c, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'Type__c, ';
        fieldNames += 'UnemploymentBeginDate__c, ';
        fieldNames += 'Verified__c, ';
        //fieldNames += 'OwnerID, ';
        fieldNames += 'OtherEnrollmentStatus__c, ';
        fieldNames += 'EmploymentEndDate__c, ';
        fieldNames += 'EmploymentStartDate__c, ';
        fieldNames += 'EmploymentType__c, ';
        fieldNames += 'IWork35hrsorMoreaWeek__c, ';
        //fieldNames += 'JobPositionTypeofWork__c, ';
        fieldNames += 'EmployerPhoneNumber__c, ';
        fieldNames += 'EstimatedGraduationDate__c, ';
        fieldNames += 'SchoolCity__c ';
        return fieldNames;
    }

    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
    
    private static String buildFilterString(){
        String filterStr = '';
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +   '\' ');
            }      
        }
        return filterStr;
    }  
}