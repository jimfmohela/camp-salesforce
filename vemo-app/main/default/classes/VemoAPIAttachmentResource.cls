/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIAttachmentResource
// 
// Description: 
//  Direction Central for Credit Check Resource API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-12-18   Greg Cook       Created                          
// 
/////////////////////////////////////////////////////////////////////////
public class VemoAPIAttachmentResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        // if((api.version == 'v1') && (api.method == 'PUT')){
        //     //return handlePutV1(api);
        // }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIAttachmentResource.handleGetV1()');
        String attachmentIDparam = api.params.get('attachmentID');
        String returnParam = api.params.get('return');
        List<Attachment> atts = AttachmentQueries.getAttachmentMapWithAttachmentID(VemoApi.parseParameterIntoIDSet(attachmentIDparam)).values();
        
        System.debug('atts:'+atts);
        //Need to verify the ownership of the parentId
        if(atts.size()>0){
            Attachment att = atts[0];
            String objectName = GlobalUtil.findObjectNameFromRecordIdPrefix(att.ParentId);
            System.debug('objectName:'+objectName);
            Map<ID, SObject> verifiedParentMap = new Map<ID, SOBject>();
            if(objectName == 'Account'){
                verifiedParentMap = AccountQueries.getStudentMapWithStudentID(new Set<ID>{att.ParentId});
            } else if (objectName == 'StudentProgram__c'){
                verifiedParentMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(new Set<ID>{att.ParentId});
            } else if (objectName == 'GenericDocument__c'){
                verifiedParentMap = GenericDocumentQueries.getGenericDocumentMapByID(new Set<ID>{att.ParentId});
            } else if (objectName == 'CreditCheck__c'){
                verifiedParentMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(new Set<ID>{att.ParentId});
            } else if (objectName == 'Program__c'){
                verifiedParentMap = ProgramQueries.getProgramMapWithProgramID(new Set<ID>{att.ParentId});
            }
            System.debug('verifiedParentMap:'+verifiedParentMap);

            if(verifiedParentMap.containsKey(att.ParentId)){
                if(returnParam=='resource'){
                    List<AttachmentResourceOutputV1> results = new List<AttachmentResourceOutputV1>();
                    for(Attachment atta : atts){
                        results.add(new AttachmentResourceOutputV1(atta));
                    }
                    system.debug('results:'+results);
                    return (new VemoAPI.ResultResponse(results, results.size()));           
                } if(returnParam=='base64'){

                    return (new VemoAPI.ResultResponse(att.body));                                   
                } else {
                    VemoAPI.binaryResponse = true;
                    VemoAPI.binaryDescription = att.id;
                    RestContext.response.responseBody = att.body;
                    RestContext.response.headers.put('Content-Type', att.ContentType);
                    return (new VemoAPI.ResultResponse(att.body));
                }                
            }
        }
        return new VEmoAPI.ResultResponse(null, 0);
    }
    
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        system.debug('VemoAPIAttachmentResource.handlePostV1()');
        RestRequest req = RestContext.request;
        ID parentRecParam = api.params.get('parentID');
        String contentTypeParam = api.params.get('contentType');
        if(String.isEmpty(contentTypeParam)) contentTypeParam = 'application/pdf';
        String fileName = api.params.get('fileName');
        System.debug('******************Attachment name = ' + fileName);
        if(String.isEmpty(fileName)) fileName = 'Attachment';

        String signedAgreementParam = api.params.get('signedAgreement');
        String finalDisclosureParam = api.params.get('finalDisclosure');

        if(signedAgreementParam == 'true'){
            fileName = 'signed-agreement-'+(String.valueOf(parentRecParam)).left(15)+'.pdf';
        }
        if(finalDisclosureParam == 'true'){
            fileName = 'final-disclosure-'+(String.valueOf(parentRecParam)).left(15)+'.pdf';
        }
 
        String objectName = GlobalUtil.findObjectNameFromRecordIdPrefix(parentRecParam);
        System.debug('objectName:'+objectName);
        Map<ID, SObject> verifiedParentMap = new Map<ID, SOBject>();
        if(objectName == 'Account'){
            verifiedParentMap = AccountQueries.getStudentMapWithStudentID(new Set<ID>{parentRecParam});
        } else if (objectName == 'StudentProgram__c'){
            verifiedParentMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(new Set<ID>{parentRecParam});
        } else if (objectName == 'GenericDocument__c'){
            verifiedParentMap = GenericDocumentQueries.getGenericDocumentMapByID(new Set<ID>{parentRecParam});
        } else if (objectName == 'CreditCheck__c'){
            verifiedParentMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(new Set<ID>{parentRecParam});
        } 
        System.debug('verifiedParentMap:'+verifiedParentMap);
        
        Set<String> supportedContentTypes = new Set<String>{'text/plain','text/rtf','application/rtf','application/msword',
                                                         'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                                         'application/pdf','image/jpeg','image/png','image/gif','text/xml','application/xml',
                                                         'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                                                         'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                                         'application/vnd.ms-excel'};
        
        if(supportedContentTypes.contains(contentTypeParam)){
            if(verifiedParentMap.containsKey(parentRecParam)){
                Blob content = (req.requestBody);
                Attachment a = new Attachment(ParentId = parentRecParam,
                                            Body = content,
                                            ContentType = contentTypeParam,
                                            Name = fileName);
                DatabaseUtil dbUtil = new DatabaseUtil();
                dbUtil.insertRecord(a);
                VemoAPI.binaryRequest = true;
                VemoAPI.binaryDescription = fileName;
                Set<ID> results = new Set<ID>{a.ID};
                return (new VemoAPI.ResultResponse(results, results.size()));
            } else {
                VemoAPI.binaryRequest = true;
                return (new VemoAPI.ResultResponse('Unauthorized parentID', 0));
            }
        }
        else {
            RestContext.response.statusCode = 400;
            VemoAPI.binaryRequest = true;
            return (new VemoAPI.ResultResponse('Accepted file formats are txt, rtf, doc, docx, pdf, jpg, jpeg, png, gif, xml', 0));
        }
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIAttachmentResource.handleDeleteV1()');
        String attachementIDParam = api.params.get('attachmentID');
        Set<ID> attIDs = VemoApi.parseParameterIntoIDSet(attachementIDParam);
        List<Attachment> atts = AttachmentQueries.getAttachmentMapWithAttachmentID(attIDs).values();
        List<Attachment> recsToDelete = new List<Attachment>();
        
        System.debug('atts:'+atts);
        //Need to verify the ownership of the parentId
        if(atts.size()>0){
            for(Attachment att : atts){
                String objectName = GlobalUtil.findObjectNameFromRecordIdPrefix(att.ParentId);
                System.debug('objectName:'+objectName);
                Map<ID, SObject> verifiedParentMap = new Map<ID, SOBject>();
                if(objectName == 'Account'){
                    verifiedParentMap = AccountQueries.getStudentMapWithStudentID(new Set<ID>{att.ParentId});
                } else if (objectName == 'StudentProgram__c'){
                    verifiedParentMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(new Set<ID>{att.ParentId});
                }                 

                System.debug('verifiedParentMap:'+verifiedParentMap);
                if(verifiedParentMap.containsKey(att.ParentId)){
                    recsToDelete.add(att);

                }
            }

            DatabaseUtil dbUtil = new DatabaseUtil();
            dbUtil.deleteRecords(recsToDelete);
            return (new VemoAPI.ResultResponse(true, recsToDelete.size()));
        }

        return (new VemoAPI.ResultResponse(null, 0));

    }

    public class AttachmentResourceOutputV1{
        public String attachmentID{get;set;}
        public String parentID{get;set;}
        public String contentType{get;set;}
        public String name{get;set;}

        public AttachmentResourceOutputV1(){}

        public AttachmentResourceOutputV1(Attachment att){
            this.attachmentID = att.ID;
            this.parentID = att.ParentId;
            this.contentType = att.ContentType;
            this.name = att.Name;
        }
    }
}