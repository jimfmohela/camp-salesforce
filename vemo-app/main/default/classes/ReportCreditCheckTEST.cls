@isTest
public class ReportCreditCheckTEST{
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest public static void validateReport(){
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(TestUtil.TEST_THROTTLE, studentMap);
        
        Test.startTest();
            ReportCreditCheck cntrl = new ReportCreditCheck();
            cntrl.exportToCSV();
            cntrl.buildCsvString();     
        Test.StopTest();  
    }                  
}