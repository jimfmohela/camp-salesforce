public with sharing class VemoAPIFFQuizResource implements VemoAPI.ResourceHandler {
    
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }  
        if((api.version == 'v1') && (api.method == 'DELETE')){
          return handleDeleteV1(api);
        }      
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIFFQuizResource.handleGetV1()');  
        
        String studentProgramIDParam = api.params.get('agreementID');
        
        List<FFQuizService.FFQuizAttempt> vcr = new List<FFQuizService.FFQuizAttempt>();
        
        if(studentProgramIDParam != null){
            vcr = FFQuizService.getFFQuizAttemptWithStudentProgramID(VemoApi.parseParameterIntoIDSet(studentProgramIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: studentID required for GET');
        }
        
        List<FFQuizAttempt> results = new List<FFQuizAttempt>();
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        for(FFQuizService.FFQuizAttempt ffJSON : vcr){
            idsToVerify.add((ID)ffJSON.contractID);
        }
        Map<ID, StudentProgram__c> verifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(idsToVerify); //authorized records should be returned
        
        for(FFQuizService.FFQuizAttempt vr : vcr){
            if(verifiedAgreementMap.containsKey((ID)vr.contractID)){
                FFQuizAttempt result = new FFQuizAttempt(vr);
                
                result.quizResponses = new List<FFQuizResponse>();
                List<FFQuizService.FFQuizResponse> responseDetails = FFQuizService.getFFQuizResponseWithFFQuizAttemptID(new Set<ID>{vr.fFQuizAttemptID});
                for(FFQuizService.FFQuizResponse responseDetail : responseDetails){
                    result.quizResponses.add(new FFQuizResponse(responseDetail));
                }
                results.add(result);
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){ 
        System.debug('VemoAPIFFQuizResource.handlePostV1()');  

        //String modeParam = api.params.get('mode');

        List<FFQuizService.FFQuizAttempt> qaList = new List<FFQuizService.FFQuizAttempt>();
        
        Map<Integer, List<FFQuizService.FFQuizResponse>> qrMap = new Map<Integer, List<FFQuizService.FFQuizResponse>>();
        List<FFQuizAttempt> FFQuizAttemptJSON = (List<FFQuizAttempt>)JSON.deserialize(api.body, List<FFQuizAttempt>.class);
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        for(FFQuizAttempt ffJSON : FFQuizAttemptJSON){
            idsToVerify.add((ID)ffJSON.contractID);
        }
        Map<ID, StudentProgram__c> verifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(idsToVerify); //authorized records should be returned
        
        for(FFQuizAttempt ffJSON: FFQuizAttemptJSON){
            if(verifiedAgreementMap.containsKey((ID)ffJSON.contractID)){ 
                qaList.add(fFQuizAttemptToFFQuizAttemptObj(ffJSON));
                List<FFQuizService.FFQuizResponse> qrList = new List<FFQuizService.FFQuizResponse>();
                if(ffJSON.quizResponses != null){
                    for(FFQuizResponse ffqrJSON: ffJSON.quizResponses){
                        qrList.add(fFQuizResponseToFFQuizResponseObj(ffqrJSON));
                    }
                }
            
                qrMap.put(ffJSON.attemptOrder, qrList);
            }
            
        }       
        Set<Id> FFQuizAttemptIDs = FFQuizService.createFFQuizAttempt(qaList);
        Set<Id> FFQuizResponseIDs = FFQuizService.createFFQuizAttempt(qrMap, FFQuizAttemptIDs);
        return (new VemoAPI.ResultResponse(FFQuizAttemptIDs, FFQuizAttemptIDs.size()));
    }
    
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){ 
        System.debug('VemoAPIFFQuizResource.handlePutV1()');  
        //String modeParam = api.params.get('mode');

        //if(modeParam == 'quizAttempt'){
        List<FFQuizService.FFQuizAttempt> qaList = new List<FFQuizService.FFQuizAttempt>();
        List<FFQuizService.FFQuizResponse> qrList = new List<FFQuizService.FFQuizResponse>();
                
        List<FFQuizAttempt> FFQuizAttemptJSON = (List<FFQuizAttempt>)JSON.deserialize(api.body, List<FFQuizAttempt>.class);
        
        
         /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        for(FFQuizAttempt ffJSON : FFQuizAttemptJSON){
            idsToVerify.add((ID)ffJSON.ffQuizAttemptID);
        }
        Map<ID, FFQuizAttempt__c> verifiedFFQuizAttemptMap = FFQuizQueries.getFFQuizAttemptMapWithFFQuizAttemptID(idsToVerify); //authorized records should be returned
        Map<ID, FFQuizResponse__c> verifiedFFQuizResponseMap = FFQuizQueries.getFFQuizResponseMapWithFFQuizAttemptID(idsToVerify);
        
        for(FFQuizAttempt ffJSON: FFQuizAttemptJSON){
            if(verifiedFFQuizAttemptMap.containsKey((ID)ffJSON.ffQuizAttemptID)){
                qaList.add(fFQuizAttemptToFFQuizAttemptObj(ffJSON));            
                if(ffJSON.quizResponses != null){
                    for(FFQuizResponse ffqrJSON: ffJSON.quizResponses){
                        if(verifiedFFQuizResponseMap.containsKey(ffqrJSON.fFQuizResponseID) || ffqrJSON.fFQuizResponseID == null){ //update the existing ones and create the new ones
                            qrList.add(fFQuizResponseToFFQuizResponseObj(ffqrJSON));
                        }
                    }
                }
            }   
            
        }       
        Set<Id> FFQuizAttemptIDs = FFQuizService.updateFFQuizAttempt(qaList);
        Set<Id> FFQuizResponseIDs = FFQuizService.updateFFQuizResponse(qrList);
        return (new VemoAPI.ResultResponse(FFQuizAttemptIDs, FFQuizAttemptIDs.size()));
        //}
        //return null;
    }
    
    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIFFQuizResource.handleDeleteV1()');
        String ffqaIDparam = api.params.get('ffQuizAttemptID');
        
        Map<ID, FFQuizAttempt__c> quizAttemptMap =  FFQuizQueries.getFFQuizAttemptMapWithFFQuizAttemptID(VemoApi.parseParameterIntoIDSet(ffqaIDparam));
        
        Set<ID> idsToVerify = new Set<ID>();
        //Set<ID> ffIDsToVerify = new Set<ID>(); 
        if(quizAttemptMap.size()>0){
            for(FFQuizAttempt__c quiz: quizAttemptMap.values()){
                idsToVerify.add(quiz.Contract__c);
                //ffIDsToVerify.add((ID)quiz.fFQuizAttemptID); 
            }
        }
        
        Map<ID, StudentProgram__c> verifiedAgreementMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(idsToVerify); //authorized records should be returned
        
        Set<ID> ffQuizToBeDeleted = new Set<ID>();
        for(FFQuizAttempt__c quiz: quizAttemptMap.values()){
            if(verifiedAgreementMap.containsKey(quiz.Contract__c)){
                ffQuizToBeDeleted.add(quiz.id); 
            }
        }
        Integer numToDelete = 0;
        if(ffQuizToBeDeleted.size()>0){
            numToDelete = FFQuizService.deleteFFQuizAttempt(ffQuizToBeDeleted);     
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    
    /*public void newMethod(){
        string s = '[{"submitTime":"ccd","startTime":"re","resultLink":"gt","result":"uy","quizResponses":[{"questionText":"cads","fFQuizResponseID":"a1Z0l000000MoCu","correctAnswer":true,"answerText":"nhj"},{"questionText":"cads1","fFQuizResponseID":"a1Z0l000000MoCf","correctAnswer":false,"answerText":"nhj1"}],"fFQuizAttemptID":"a1Y0l000000JRWS","contractID":"","attemptOrder":1},{"submitTime":"ccd2","startTime":"re2","resultLink":"gt2","result":"uy2","quizResponses":[{"questionText":"cads2","fFQuizResponseID":"a1Z0l000000MoCp","correctAnswer":false,"answerText":"nhj2"},{"questionText":"cads3","fFQuizResponseID":"a1Z0l000000MoCk","correctAnswer":true,"answerText":"nhj3"}],"fFQuizAttemptID":"a1Y0l000000JRWN","contractID":"","attemptOrder":2}]';
        List<FFQuizService.FFQuizAttempt> qaList = new List<FFQuizService.FFQuizAttempt>();
        List<FFQuizService.FFQuizResponse> qrList = new List<FFQuizService.FFQuizResponse>();
        List<FFQuizAttempt> FFQuizAttemptJSON = (List<FFQuizAttempt>)JSON.deserialize(s, List<FFQuizAttempt>.class);
        for(FFQuizAttempt ffJSON: FFQuizAttemptJSON){
            qaList.add(fFQuizAttemptToFFQuizAttemptObj(ffJSON));
            List<FFQuizResponse> FFQuizResponseJSON = (List<FFQuizResponse>)JSON.deserialize(String.valueof(ffJSON.quizResponses), List<FFQuizResponse>.class);
            for(FFQuizResponse ffqrJSON: ffJSON.quizResponses){
                qrList.add(fFQuizResponseToFFQuizResponseObj(ffqrJSON));
            }
            
        }       
        Set<Id> FFQuizAttemptIDs = FFQuizService.updateFFQuizAttempt(qaList);
        Set<Id> FFQuizResponseIDs = FFQuizService.updateFFQuizResponse(qrList);
    }*/
    
    public static FFQuizService.FFQuizAttempt fFQuizAttemptToFFQuizAttemptObj(FFQuizAttempt ffqaList){
        FFQuizService.FFQuizAttempt ffqa = new FFQuizService.FFQuizAttempt();
        ffqa.fFQuizAttemptID = ffqaList.fFQuizAttemptID;
        ffqa.attemptOrder = ffqaList.attemptOrder ;
        ffqa.clientID = ffqaList.clientID;
        ffqa.contractID = ffqaList.contractID;
        ffqa.result = ffqaList.result;
        ffqa.resultLink = ffqaList.resultLink;  
        ffqa.startTime = ffqaList.startTime ;
        ffqa.submitTime = ffqaList.submitTime;
        return ffqa;
    }
    
    public static FFQuizService.FFQuizResponse fFQuizResponseToFFQuizResponseObj(FFQuizResponse ffqrList){
        
        FFQuizService.FFQuizResponse ffqr = new FFQuizService.FFQuizResponse();
        ffqr.fFQuizResponseID = ffqrList.fFQuizResponseID;
        ffqr.answerID = ffqrList.answerID;
        ffqr.answerText = ffqrList.answerText;
        ffqr.correctAnswer = ffqrList.correctAnswer;
        ffqr.questionID = ffqrList.questionID;
        ffqr.questionText = ffqrList.questionText;
        ffqr.quizAttemptID = ffqrList.quizAttemptID;
        system.debug(ffqr);
        return ffqr;
    }
    
    public class FFQuizAttempt{
        public String fFQuizAttemptID {get;set;}
        public Integer attemptOrder {get;set;}
        public String clientID {get;set;}
        public String contractID {get;set;}
        public String result {get;set;}
        public String resultLink {get;set;}
        public String startTime {get;set;}
        public String submitTime {get;set;}
        public List<FFQuizResponse> quizResponses {get;set;}
        
        public FFQuizAttempt(){}
    
        public FFQuizAttempt(FFQuizService.FFQuizAttempt ffqa){
          this.fFQuizAttemptID = ffqa.fFQuizAttemptID;
          this.attemptOrder = ffqa.attemptOrder ;
          this.clientID = ffqa.clientID;
          this.contractID = ffqa.contractID;
          this.result = ffqa.result;
          this.resultLink = ffqa.resultLink;  
          this.startTime = ffqa.startTime ;
          this.submitTime = ffqa.submitTime;
          
        }
    }
    
    public class FFQuizResponse{
        public String fFQuizResponseID {get;set;}
        public String answerID {get;set;}
        public String answerText {get;set;}
        public boolean correctAnswer {get;set;}
        public String questionID {get;set;}
        public String questionText {get;set;}
        public String quizAttemptID {get;set;}
        
        public FFQuizResponse(){}
        
        public FFQuizResponse(FFQuizService.FFQuizResponse ffqr){
          this.fFQuizResponseID = ffqr.fFQuizResponseID;
          this.answerID = ffqr.answerID;
          this.answerText = ffqr.answerText;
          this.correctAnswer = ffqr.correctAnswer;
          this.questionID = ffqr.questionID;
          this.questionText = ffqr.questionText;
          this.quizAttemptID = ffqr.quizAttemptID;
        }
    }
 
}