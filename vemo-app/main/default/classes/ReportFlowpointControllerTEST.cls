@isTest
public class ReportFlowpointControllerTEST{

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
            Map<ID, Account> investors = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
            Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, investors);       
            Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, investors);
            Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
            Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(2, students, programs);
            Map<ID, EmploymentHistory__c> employmentHistory = TestDataFactory.createAndInsertEmploymentHistory(1,students);
            Map<ID, IncomeVerification__c> incomes = TestDataFactory.createAndInsertIncomeVerification(1, employmentHistory);
            try{
				Map<ID, Deferment__c> deferments = TestDataFactory.createAndInsertDeferments(students);
            }catch(exception e){
				system.debug(e.getmessage());
			}
            recordType rt = [select id from recordtype where name = 'Investor'];
            investors.values()[0].recordtype = rt;
            investors.values()[0].InstitutionShortName__c = 'NYCDA';
            dbUtil.updateRecords(investors.values());
            
            employmentHistory.values()[0].Verified__c = true;
            dbUtil.updateRecords(employmentHistory.values());
            
            incomes.values()[0].Type__c = 'Reported';
            incomes.values()[0].Status__c = 'Verified';
            dbUtil.updateRecords(incomes.values());
            
            integer i =0;
            for(StudentProgram__c sp:agreements.values()){
                sp.status__c = 'Grace';  
                sp.certificationDate__c = Datetime.now().addDays(-60);  
                sp.BypassAutomation__c = true;
                sp.PrimaryOwner__c = investors.values()[0].id;
                if(i == 0)
                    sp.Status__c = 'Payment';
                else
                    sp.Status__c = 'Deferment';
                i++;
            }
            
            //update agreements.values();
            dbUtil.updateRecords(agreements.values());
        }
        
        
    }
    
    @isTest public static void validateGetAgreements(){
        Account primaryOwner = [select id from account where InstitutionShortName__c = 'NYCDA'];     
        Test.StartTest(); 
            ReportFlowpointController ctrl = new ReportFlowpointController();
            ctrl.selectedPrimaryOwner = primaryOwner.id;
            ctrl.getPrimaryOwners();
            ctrl.runReport();
            ctrl.export();
        
        Test.StopTest();  
    }
}