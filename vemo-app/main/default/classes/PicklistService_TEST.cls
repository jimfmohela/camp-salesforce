/////////////////////////////////////////////////////////////////////////
// Class: PicklistService_TEST
//
// Description:
// This is a test class for "PicklistService"
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-09-19   Ranjeet Kumar   Created
/////////////////////////////////////////////////////////////////////////
@isTest
public class PicklistService_TEST {
    
    static testMethod void getPicklistValuesMain_Test(){
        List<ResourceAttributeMapping__c> ResourceAttributeMappingList = new List<ResourceAttributeMapping__c>();
        ResourceAttributeMappingList = TestDataFactory.createAndInsertDefaultResourceAttributeSettings();
        String resource = null;
        String attribute = null;
        String dependentAttribute = null;
        String controllingValue = null;
        
        for(ResourceAttributeMapping__c setting : ResourceAttributeMappingList){
            if(setting.DependentAttribute__c != null && setting.DependentAttribute__c .toLowerCase() == 'country'){
                resource = setting.Resource__c;
                attribute = setting.Attribute__c;
                dependentAttribute = setting.DependentAttribute__c;
                controllingValue = 'united States';
                break;
            }
        }
        Test.startTest();
        PicklistService.getPicklistValues(resource, attribute, dependentAttribute, controllingValue);
        Test.stopTest();
    }
    
    static testMethod void verifyParametersWithNoResource_Test(){
        PicklistService.verifyParameters(null, 'Test-Attribute', 'Test-DependentAttribute', 'Test-ControllingValue');
    }
    
    static testMethod void verifyParametersWithNoAttribute_Test(){
        PicklistService.verifyParameters('Test-Resource', null, 'Test-DependentAttribute', 'Test-ControllingValue');
    }
    
    static testMethod void verifyParametersWithNoDependentAttribute_Test(){
        PicklistService.verifyParameters('Test-Resource', 'Test-Attribute', null, 'Test-ControllingValue');
    }
    
    static testMethod void verifyParametersWithNoControllingValue_Test(){
        PicklistService.verifyParameters('Test-Resource', 'Test-Attribute', 'Test-DependentAttribute', null);
    }
    
    static testMethod void getResourceAttributeMapping_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        //dbUtil.queryExecutor = new UserContext(); 
        List<ResourceAttributeMapping__c> ResourceAttributeMappingList = new List<ResourceAttributeMapping__c>();
        ResourceAttributeMappingList = TestDataFactory.createAndInsertDefaultResourceAttributeSettings();
        
        List<ResourceAttributeMapping__c> ResourceAttributeMappings = new List<ResourceAttributeMapping__c>();
        Test.startTest();
        ResourceAttributeMappings = PicklistService.getResourceAttributeMapping(
                                                    ResourceAttributeMappingList[0].Resource__c, 
                                                    ResourceAttributeMappingList[0].Attribute__c, 
                                                    ResourceAttributeMappingList[0].DependentAttribute__c
                                                );
        System.assertEquals(ResourceAttributeMappingList[0].Id, ResourceAttributeMappings[0].Id);
        Test.stopTest();
    }
    
    static testMethod void checkPickListType_Test(){
        Test.startTest();
        Boolean isDependentPickList = PicklistService.checkPickListType('Account', 'Industry');
        System.assertEquals(false, isDependentPickList);
        Test.stopTest();
    }
    
    static testMethod void checkForStateCountryPicklist_Test(){
        Test.startTest();
        Boolean isStateCountryPicklist = PicklistService.checkForStateCountryPicklist('Account', 'BillingCountry');
        System.assertEquals(true, isStateCountryPicklist);
        Test.stopTest();
    }
    
    static testMethod void getLabelsOfPickListValues_Test(){
        Test.startTest();
        List<String> picklistLabels = PicklistService.getLabelsOfPickListValues('Account', 'Industry');
        System.assertNotEquals(null, picklistLabels);
        Test.stopTest();
    }
    
    static testMethod void getPickListValues_Test(){
        Test.startTest();
        List<String> picklistValues = PicklistService.getPickListValues('Account', 'Industry');
        System.assertNotEquals(null, picklistValues);
        Test.stopTest();
    }
    
    static testMethod void getDependentPickListValues_Test(){
        Test.startTest();
        List<String> picklistValues = PicklistService.getDependentPickListValues('Account', 'BillingCountryCode', 'BillingStateCode', 'United States');
        System.assertNotEquals(null, picklistValues);
        Test.stopTest();
    }
	
	static testMethod void createDefaultSettingsTest(){
        Test.startTest();
        PicklistService.createDefaultSettings();
        Test.stopTest();
    }
}