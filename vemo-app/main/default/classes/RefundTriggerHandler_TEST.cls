@isTest
public with sharing class RefundTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    public static void testDML() {
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        TestUtil.createStandardTestConditions();
        //create students
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(10);
        //create school
        /*Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1,schoolMap);
        //create student programs
        Map<ID, StudentProgram__c> spMap = TestDataFactory.createAndInsertStudentProgram(1,studentMap,programMap);
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(5, spMap);
        Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(spMap, agreementMonthStats);*/
        
        //Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);      
        List<Refund__c> refundsToAdd = new List<Refund__c>();
        refundsToAdd.add(new Refund__c(Account__c = studentMap.values()[0].id));
        //insert refundsToAdd;
        //update refundsToAdd;
        //delete refundsToAdd;
        dbUtil.insertRecords(refundsToAdd);
        for(Refund__c ref: refundsToAdd){
            ref.GenerateAmountDue__c = true;
        }
        dbUtil.updateRecords(refundsToAdd);
        dbUtil.deleteRecords(refundsToAdd);
        undelete refundsToAdd;
        
    }
}