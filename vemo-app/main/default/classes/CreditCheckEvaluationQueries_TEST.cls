@isTest
public class CreditCheckEvaluationQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, 
                                                                                    TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, CreditCheck__c> creditCheckMap = TestDataFactory.createAndInsertCreditCheck(1, 
                                                                                    studentMap);
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);

    }
    
    
    @isTest public static void validateGetCreditCheckEvaluationMapWithCreditCheckEvalID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        Map<ID, CreditCheckEvaluation__c> resultCCMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(cceMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.keySet().size());
        Test.stopTest();
    }
    
    
    @isTest public static void validateGetCreditCheckEvaluationMapWithCreditCheckID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        Map<ID, CreditCheckEvaluation__c> resultCCMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckID(ccMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.keySet().size());
        Test.stopTest();
    }
    
    @isTest public static void validateGetCreditCheckEvaluationMapWithAgreementID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        Map<ID, CreditCheckEvaluation__c> resultCCMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithAgreementID(agreementMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.keySet().size());
        Test.stopTest();
    }

}