@isTest
public class CreditCheckEvaluationTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup static void setupData() {
        TestUtil.createStandardTestConditions();
    }
    
     @isTest public static void testCopyCreditEvaluationToAgreementFields() {
         DatabaseUtil.setRunQueriesInMockingMode(false); 
         dbUtil.queryExecutor = new UserContext(); 
         
         /* Make sure the running user has a role otherwise an exception
           will be thrown. */
        user userwithrole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
                                    
         system.runAs(userWithRole){ 
         Map<Id, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);                              
         Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
         for(Account stud : studentMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
         }
         update studentMap.values();
         
         Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
         Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1,schools);                                                                                
         Map<ID, StudentProgram__c> agreementsMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
         Map<ID, CreditCheck__c> cCheckMap = TestDataFactory.createAndInsertCreditCheck(1, studentMap);
         
         Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
         Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
         List<CreditCheckEvaluation__c> ccList = new List<CreditCheckEvaluation__c>();
         
         for(CreditCheck__c cc: ccMap.values()){
             for(studentProgram__c sp: agreementMap.values()){
                 CreditCheckEvaluation__c cce = new CreditCheckEvaluation__c();
                 cce.CreditCheck__c = cc.id;
                 cce.Agreement__c = sp.id;
                 cce.CreditCheckProcess__c = 'In Progress';
                 ccList.add(cce); 
             }
                  
         }
         
          Test.startTest();
          system.debug('ccList:'+ccList[0]);
          //insert ccList;
          dbUtil.insertRecords(ccList);
          
          Map<ID, StudentProgram__c> agreementMapAfterInsert = StudentProgramQueries.getStudentProgramMap();
          for(studentProgram__c sp: agreementMapAfterInsert.values()){
              system.debug(sp.CreditCheckProcess__c);
              System.assertEquals(sp.CreditCheckProcess__c, 'In Progress');
              System.assertEquals(sp.CreditCheckDeniedReasonText__c, null);
          }
          
          for(CreditCheckEvaluation__c cce: ccList){
              cce.CreditCheckProcess__c = 'Credit Denied';
              cce.CreditCheckDeniedReason__c = 'Bankruptcy';
              cce.CreditCheckDeniedReasonText__c = 'Bankruptcy';
          }
          
          //update ccList;
          dbUtil.updateRecords(ccList);
          
          Map<ID, StudentProgram__c> agreementMapAfterUpdate = StudentProgramQueries.getStudentProgramMap();
          for(studentProgram__c sp: agreementMapAfterUpdate.values()){
              System.assertEquals(sp.CreditCheckProcess__c, 'Credit Denied' );
              System.assertEquals(sp.CreditCheckDeniedReasonText__c, 'Bankruptcy' );
          }
          
          Test.stopTest();
          }
     }
}