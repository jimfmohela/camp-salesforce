public class VemoAPIAcademicEnrollmentCountResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            return null;
        }
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIAcademicEnrollmentCountResource.handleGetV2()');
        
        String schoolIDparam = api.params.get('schoolID');
        
        if(schoolIDparam == null && UserService.schoolAccount != null){
            schoolIdParam =  UserService.schoolAccount.id;
        }
        
        List<AggregateResult> arrList = new List<AggregateResult>();
        
        if(schoolIDparam != null){
            arrList =[SELECT Count(Id) Total, AcademicEnrollmentStatus__c FROM AcademicEnrollment__c WHERE //AcademicEnrollmentStatus__c  != null AND  
                        Provider__c =: schoolIdParam   GROUP BY AcademicEnrollmentStatus__c];

        }
        List<AcademicEnrollmentCountResourceOutputV2> results = new List<AcademicEnrollmentCountResourceOutputV2>();
        
        string status;
        integer count = 0;
        for(AggregateResult ar : arrList){
            status = string.valueOf(ar.get('AcademicEnrollmentStatus__c'));
            count = integer.valueOf(ar.get('Total'));
            results.add(new AcademicEnrollmentCountResourceOutputV2(status, count));
        }
        
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
        
    public class AcademicEnrollmentCountResourceOutputV2{
        public String status{get;set;}
        public Integer count {get;set;}
        public AcademicEnrollmentCountResourceOutputV2(string st, integer cnt){
            status = st;
            count = cnt;
        }
    } 
}