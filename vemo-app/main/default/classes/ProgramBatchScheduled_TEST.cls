@isTest
public with sharing class ProgramBatchScheduled_TEST {
	
    @isTest
    static void disbConfNotification_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ProgramBatchScheduled BatchJob = new ProgramBatchScheduled ();
        BatchJob.jobType = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
        System.schedule('disbConfNotification_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
}