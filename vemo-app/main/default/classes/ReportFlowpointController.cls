public with sharing class ReportFlowpointController{
    public String selectedPrimaryOwner {get;set;}
    //transient public List<reportDataWrapper> reportData {get;set;}
    transient public List<ReportPaymentDataWrapper> reportPaymentData {get;set;}
    transient public List<ReportDefermentDataWrapper> reportDefermentData {get;set;}   
    transient public String csv {get;set;}
    
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportFlowpointController(){
        selectedPrimaryOwner = '';
        //reportData = new List<reportDataWrapper>();    
        reportPaymentData = new List<ReportPaymentDataWrapper>();
        reportDefermentData = new List<ReportDefermentDataWrapper>();
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        
        getAgreements();
        
    }
    
    public List<SelectOption> getPrimaryOwners(){
        //List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By name ASC];
        Map<ID, Account> acctMap = new Map<ID, Account>();
        string query ='Select id,name From Account ';
        query += ' WHERE ' + generateRecordTypeStatement('Investor');
        query += ' '+ 'Order By name ASC';
        
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        List<SelectOption> options = new List<SelectOption>();
        for(Id id: AcctMap.keyset()){
            options.add(new SelectOption(id,AcctMap.get(id).name));    
        }
        return options;
    }
    
    private static String generateRecordTypeStatement(String recordTypeLabel){
        ID recordTypeID = (String)GlobalUtil.getRecordTypeIDByLabelName('Account', recordTypeLabel);
        return 'RecordTypeID = \''+ String.valueOf(recordTypeID) + '\'';
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public void buildReportData(List<Account> studentPaymentList, List<Account> studentDefermentList){
        reportPaymentData = new List<ReportPaymentDataWrapper>();
        reportDefermentData = new List<ReportDefermentDataWrapper>();
        
        for(Account acc: studentPaymentList){
            if(acc.Income_Verification__r.size()>0){
                ReportPaymentDataWrapper wrapper = new ReportPaymentDataWrapper();            
                wrapper.vemoAccountNumber = acc.VemoAccountNumber__c;
                wrapper.studentID = acc.Id;
                wrapper.studentName = acc.name;                
                wrapper.inv=acc.Income_Verification__r[0];
                if(acc.Employment_History__r.size()>0)
                    wrapper.empHist=acc.Employment_History__r[0];
                          
                reportPaymentData.add(wrapper);
            }
        }
        
        for(Account acc: studentDefermentList){
            if(acc.Deferements__r.size()>0){
                ReportDefermentDataWrapper wrapper = new ReportDefermentDataWrapper();            
                wrapper.vemoAccountNumber = acc.VemoAccountNumber__c;
                wrapper.studentID = acc.Id;
                wrapper.studentName = acc.name;
                if(acc.Income_Verification__r.size()>0)
                    wrapper.inv=acc.Income_Verification__r[0];
                if(acc.Employment_History__r.size()>0)
                    wrapper.empHist=acc.Employment_History__r[0];
                          
                wrapper.def = acc.Deferements__r[0];
                reportDefermentData.add(wrapper);
            }
        }
        //system.debug(output);
        //return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student
    ///////////////////////////////////////////////
    public void getAgreements(){
              
         List<StudentProgram__c> spList = [select id,Student__r.Id,Student__r.Name,StudentVemoAccountNumber__c,Status__c
                                           FROM StudentProgram__c
                                           WHERE PrimaryOwner__r.Id =: selectedPrimaryOwner AND (Status__c = 'Payment' OR Status__c = 'Deferment')];
                     
         //Map of Agreements by Student
         //Map<ID,List<StudentProgram__c>> studentAgreementMap = new Map<ID,List<StudentProgram__c>>();
          
         Set<String> paymentStudentId = new Set<String>();
         Set<String> deferementStudentId = new Set<String>();           
         //Set<Id> studentIds = new Set<Id>();
         //Set<Id> programIds = new Set<Id>();
         
         for(StudentProgram__c sp: spList){
             if(sp.Status__c == 'Payment')
                 paymentStudentId.add(sp.Student__r.Id);
             else
                 deferementStudentId.add(sp.Student__r.Id);
             //studentIds.add(sp.Student__r.Id);
             //programIds.add(sp.Program__r.Id);
         }
         
         List<Account> studentPaymentList = new List<Account>();
         studentPaymentList = [select id,Name,VemoAccountNumber__c,
                               (Select Employer__c,EmploymentEndDate__c,EmploymentStartDate__c,Category__c,Type__c from Employment_History__r where Verified__c = true order by EmploymentStartDate__c desc limit 1 ),
                               (Select IncomePerMonth__c,BeginDate__c,EndDate__c,DateVerified__c from Income_Verification__r where Type__c = 'Reported' AND Status__c = 'Verified' order by BeginDate__c desc limit 1)
                               FROM Account 
                               WHERE Id IN: paymentStudentId];
                               
         List<Account> studentDeferementList = new List<Account>();
         studentDeferementList = [select id,Name,VemoAccountNumber__c,
                                 (Select Employer__c,EmploymentEndDate__c,EmploymentStartDate__c,Category__c,Type__c from Employment_History__r where Verified__c = true order by EmploymentStartDate__c desc limit 1 ),
                                 (Select IncomePerMonth__c,BeginDate__c,EndDate__c,DateVerified__c from Income_Verification__r where Type__c = 'Reported' AND Status__c = 'Rejected' order by BeginDate__c desc limit 1),
                                 (Select Type__c,MonthlyIncome__c,DateVerified__c,BeginDate__c,EndDate__c from Deferements__r where BeginDate__c != null AND EndDate__c != null AND (Type__c = 'Below minimum income, working full-time' OR Type__c = 'Below minimum income, working part-time') order by createddate desc limit 1)
                                 FROM Account 
                                 WHERE Id IN: deferementStudentId];                                 
         
         buildReportData(studentPaymentList, studentDeferementList);
        
    }
    
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference export(){
        PageReference pg = new PageReference('/apex/ReportFlowpointExport');
        pg.setRedirect(false);
        return pg;
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportPaymentDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentID {get;set;}
        public String studentName {get;set;}
        public IncomeVerification__c inv {get;set;}
        public EmploymentHistory__c empHist {get;set;}    
    }
    
    public class ReportDefermentDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentID {get;set;}
        public String studentName {get;set;}
        public IncomeVerification__c inv {get;set;}
        public EmploymentHistory__c empHist {get;set;}
        public deferment__c def {get;set;}
            
    }   
      
}