/////////////////////////////////////////////////////////////////////////
// Class: PaymentInstructionTriggerHandler_TEST
// 
// Description: 
//      Test class for PaymentInstructionTriggerHandler
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-17   Jared Hagemann  Created                                 
// 
/////////////////////////////////////////////////////////////////////////
@isTest
public with sharing class PaymentInstructionTriggerHandler_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    @isTest public static void testManageAllocations(){
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, PaymentMethod__c> testPaymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, testStudentAccountMap);
        Map<ID, PaymentInstruction__c> testPaymentInsturctionMap = TestDataFactory.createAndInsertPaymentInstruction(1, testStudentAccountMap, testPaymentMethodMap);

        //Add a few fees
        //5 fees @ $1000 apiece
        Map<ID, Fee__c> testFeeMap = TestDataFactory.createAndInsertFee(1, testStudentAccountMap);
        
        Map<Id, PaymentInstruction__c> paymenetInstructionMap = TestDataFactory.createAndInsertPaymentInstruction(1, testStudentAccountMap, testPaymentMethodMap);
        
        Map<ID, PaymentInstruction__c> paymentInstructionMapN = PaymentInstructionQueries.getPaymentInstructionMap();
        paymentInstructionMapN.values().get(0).Allocate__c = true;
        paymentInstructionMapN.values().get(0).Amount__c = 500;
        
        Test.startTest();        
        dbUtil.deleteRecords(paymentInstructionMapN.values());
        dbUtil.undeleteRecords(paymentInstructionMapN.values());
        for(PaymentInstruction__c pi: paymentInstructionMapN.values()){
            pi.status__c = 'Bounced';
        }
        dbUtil.updateRecords(paymentInstructionMapN.values());
        //update paymentInstructionMapN.values();
        Test.stopTest();
        
        
    }
}