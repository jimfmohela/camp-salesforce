public class VemoAPIEventInstanceResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }   
        /*if((api.version == 'v2') && (api.method == 'DELETE')){
            return handleDeleteV2(api);
        }*/
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventInstanceResource.handleGetV2()');
        string readParam = api.params.get('read');
        string latestOnlyParam = api.params.get('latestOnly');
        
        List<EventInstanceService.EventInstance> eInstanceList = new List<EventInstanceService.EventInstance>();
        
        if(latestOnlyParam != null && boolean.valueOf(latestOnlyParam) == true)
            eInstanceList = EventInstanceService.getEventLatestInstanceMapForAuthUser(readParam);
        else{
            if(readParam != null){
                eInstanceList = EventInstanceService.getEventInstanceMapForAuthUser(boolean.valueOf(readParam));
            }
            else{
                eInstanceList = EventInstanceService.getEventInstanceMapForAuthUser();
            }
        }
        
        List<EventInstanceResourceOutputV2> results = new List<EventInstanceResourceOutputV2>();
    
        for(EventInstanceService.EventInstance ei : eInstanceList){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoEventInstanceResourceOutputV2(ei));
            }else{
                results.add(new PublicEventInstanceResourceOutputV2(ei));
            }
        }      
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventInstanceResource.handlePutV2');
        List<EventInstanceService.EventInstance> updateEventInstances = new List<EventInstanceService.EventInstance>();
        
        String markAllasRead = api.params.get('MarkAllAsRead');
        if(! (markAllasRead != null && markAllasRead != '')){
            List<EventInstanceResourceInputV2> eInstanceJSON = (List<EventInstanceResourceInputV2>)JSON.deserialize(api.body, List<EventInstanceResourceInputV2>.class);
            
            
            for(EventInstanceResourceInputV2 es : eInstanceJSON){
                es.validatePUTFields();
                updateEventInstances.add(eventInstanceResourceV2ToEventInstanceObj(es));
            }
        }
        else{
            updateEventInstances = EventInstanceService.getEventInstanceMapForAuthUser();
        }
        Set<ID> eventInstanceIDs = EventInstanceService.updateEventInstance(updateEventInstances, markAllasRead);
        
        return (new VemoAPI.ResultResponse(eventInstanceIDs, eventInstanceIDs.size()));
    }
    
    /* public static VemoAPI.ResultResponse handleDeleteV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventInstanceResource.handleDeleteV2()');
        String eventInstanceIDparam = api.params.get('eventInstanceID');
        Integer numToDelete = EventInstanceService.deleteEventInstances(VemoApi.parseParameterIntoIDSet(eventInstanceIDparam)); 
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }*/
  
    public static EventInstanceService.EventInstance eventInstanceResourceV2ToEventInstanceObj(EventInstanceResourceInputV2 esRes){
        EventInstanceService.EventInstance es = new EventInstanceService.EventInstance();
        es.eventInstanceID = esRes.eventInstanceID;
        es.read = esRes.read;
        return es;
    }

    public virtual class EventInstanceResourceOutputV2{
        public String eventInstanceID{get;set;}
        public boolean read{get;set;}
        public String eventAttributes {get;set;}
        //public String objectType {get;set;}
        public String whatID {get;set;}
        public boolean email{get;set;}
        public String eventType{get;set;}
        public datetime createdDate {get;set;}
        public String eventID {get;set;}
        
        public EventInstanceResourceOutputV2(){}
        
        public EventInstanceResourceOutputV2(EventInstanceService.EventInstance es){
            this.eventInstanceID = es.eventInstanceID;
            this.read = es.read;
            this.eventAttributes = es.eventAttributes;
            //this.objectType = es.objectType;
            this.whatID = es.whatID;
            this.email = es.email;
            this.eventType = es.eventType;
            this.createdDate = es.createdDate;
            this.eventID = es.eventID;
        }
    }
    
    public class PublicEventInstanceResourceOutputV2 extends EventInstanceResourceOutputV2{
        public PublicEventInstanceResourceOutputV2(){}
        public PublicEventInstanceResourceOutputV2(EventInstanceService.EventInstance eInstance){
            super(eInstance);
        }
    }

    public class VemoEventInstanceResourceOutputV2 extends EventInstanceResourceOutputV2{
        public String contactID{get;set;}
        
        public VemoEventInstanceResourceOutputV2(){}        
        public VemoEventInstanceResourceOutputV2(EventInstanceService.EventInstance eInstance){
            super(eInstance);
            this.contactID = eInstance.contactID;
        }
    }
  
    public class EventInstanceResourceInputV2{
        public String eventInstanceID{get;set;}
        public boolean read{get;set;}
        
        public EventInstanceResourceInputV2(){}
        
        public void validatePUTFields(){
          if(eventInstanceID == null) throw new VemoAPI.VemoAPIFaultException('eventInstanceID is a required input parameter on PUT');
        }
    }
}