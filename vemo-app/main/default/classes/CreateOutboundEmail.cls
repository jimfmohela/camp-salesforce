/**************************** Details**********************

Class Name            :  CreateOutboundEmail
Associated Objects    :  Outbound Email
Details               :  Used to create outbound email records so that Workflow Emails can be sent through SES
                         Calling from StudentProgramTriggerHandler, TransactionTriggerHandler
Developed by          :  Kamini Singh
Any Comment           :  Done
created date          :  4th Sep 2018
Last modified date    :  14th Sep 2018
*********************************************************/

public class CreateOutboundEmail {
    
    public static Map<String, Id> emailTemplateDevNameVsId ;
    
    // Method is calling from proces builder when sendStatement = true for bill record
    @InvocableMethod
    public static void createOutboudEmails4Process(List<Id> recordIds){
        set<Id> outboundEmailIds = new set<Id>();
        outboundEmailIds = CreateOutboundEmail.createOutboudEmails(new Set<Id>(recordIds));
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createOutboudEmails
    //Details: Method calling from /*StudentProgramTriggerHandler and*/ TransactionTriggerHandler.
    //         Retruns the IDs of newly created outbound email records for sending the mails
    /////////////////////////////////////////////////////////////////////////
    public static set<Id> createOutboudEmails(set<Id> recordIds){
        
        set<Id> newOutboundEmailIds = new set<Id>();
        
        if(recordIds.size() > 0){
            findTemplates();     //Putting the values in template Map DevName-> Id
            
            Map<String, set<Id>> objNameVsIdset = new Map<String, Set<Id>>();
            
            for(Id recordId : recordIds){
                String objName = recordId.getSobjectType().getDescribe().getName();
                if(objName != null){
                    if(!objNameVsIdset.containsKey(objName)){
                        set<Id> emptyRecordIdSet = new Set<Id>();
                        objNameVsIdset.put(objName, emptyRecordIdSet);
                    }
                    objNameVsIdset.get(objName).add(recordId);
                }
            }
            if(objNameVsIdset.size() > 0){
                for(String objName : objNameVsIdset.keySet()){
                    if(objNameVsIdset.get(objName).size() > 0){
                        if(objName.toLowerCase() == 'StudentProgram__c'.toLowerCase()){
                            set<Id> newOEIds = new Set<Id>();
                            newOEIds = CreateOutboundEmail.createOutboundEmailForStudentProgram(objNameVsIdset.get(objName), null);
                            if(newOEIds != null && newOEIds.size() > 0){
                                newOutboundEmailIds.addAll(newOEIds);
                            }
                        }
                    }
                    if(objNameVsIdset.get(objName).size() > 0){
                        if(objName.toLowerCase() == 'Bill__c'.toLowerCase()){
                            set<Id> newOEIds = new Set<Id>();
                            newOEIds = CreateOutboundEmail.createOutboundEmailForBill(objNameVsIdset.get(objName));
                            if(newOEIds != null && newOEIds.size() > 0){
                                newOutboundEmailIds.addAll(newOEIds);
                            }
                        }
                    }
                    if(objNameVsIdset.get(objName).size() > 0){
                        if(objName.toLowerCase() == 'Transaction__c'.toLowerCase()){
                            set<Id> newOEIds = new Set<Id>();
                            newOEIds = CreateOutboundEmail.createOutboundEmailForDisbursement(objNameVsIdset.get(objName));
                            if(newOEIds != null && newOEIds.size() > 0){
                                newOutboundEmailIds.addAll(newOEIds);
                            }
                        }
                    }
                }
            }
        }
        return newOutboundEmailIds;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createOutboundEmailForStudentProgram
    //Details: Insert OE records only for SP.
    // Calling from student program Trigger Handler 
    /////////////////////////////////////////////////////////////////////////
    public static set<Id> createOutboundEmailForStudentProgram(set<Id> studPrgrmIds, Map<Id, StudentProgram__c> oldMap){
        System.debug('CreateOutboundEmail.createOutboundEmailForStudentProgram()');
        if(oldMap == null){
            oldMap = new Map<Id, StudentProgram__c>();
        }
        if(emailTemplateDevNameVsId == null){
            findTemplates();
        }
        set<Id> newOutboundEmailIds = new set<Id>();
        if(studPrgrmIds != null && studPrgrmIds.size() > 0){
            
            List<StudentProgram__c> studentProgramList = new List<StudentProgram__c>();
            
            Map<ID, StudentProgram__c> studPrgMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(studPrgrmIds);
            if(studPrgMap != null && studPrgMap.size() > 0){
            	
                List<OutboundEmail__c> outboundEmailList = new List<OutboundEmail__c>();
                List<StudentProgram__c> finalDisclosureSPs = new List<StudentProgram__c>();
                
                for(StudentProgram__c studPrgrm : studPrgMap.values()){
                    if(studPrgrm.Student__c != null && studPrgrm.Student__r.PersonEmail != null && studPrgrm.Student__r.PersonContactId != null){
                        String toAddress = studPrgrm.Student__r.PersonEmail;
                        Id whatId = studPrgrm.Id;
                        Id targetObjectId = studPrgrm.Student__r.PersonContactID;
                        Boolean sendViaSes = true;
                        
                        if(studPrgrm.SendApplicationCertified__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&   oldMap.get(studPrgrm.Id).SendApplicationCertified__c != studPrgrm.SendApplicationCertified__c))){
                            finalDisclosureSPs.add(studPrgrm); // add attachments also with the email
                        }
                        if(studPrgrm.SendApplicationComplete__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) && oldMap.get(studPrgrm.Id).SendApplicationComplete__c != studPrgrm.SendApplicationComplete__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsApplicationComplete') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsApplicationComplete');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationComplete__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendApplicationCompleteSchoolInit__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&  oldMap.get(studPrgrm.Id).SendApplicationCompleteSchoolInit__c != studPrgrm.SendApplicationCompleteSchoolInit__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsSchoolInitiatedApplicationComplete') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsSchoolInitiatedApplicationComplete');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationCompleteSchoolInit__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendSchoolInitPreCertifiedInvite__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&   oldMap.get(studPrgrm.Id).SendSchoolInitPreCertifiedInvite__c != studPrgrm.SendSchoolInitPreCertifiedInvite__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsSchoolInitiatedInvitation') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsSchoolInitiatedInvitation');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendSchoolInitPreCertifiedInvite__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendApplicationUnderReview__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&     oldMap.get(studPrgrm.Id).SendApplicationUnderReview__c != studPrgrm.SendApplicationUnderReview__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsCreditPending') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsCreditPending');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationUnderReview__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendApplicationReviewApproved__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&  oldMap.get(studPrgrm.Id).SendApplicationReviewApproved__c != studPrgrm.SendApplicationReviewApproved__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsCreditApprovedAfterReview') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsCreditApprovedAfterReview');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationReviewApproved__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendApplicationImmediatelyApproved__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&  oldMap.get(studPrgrm.Id).SendApplicationImmediatelyApproved__c != studPrgrm.SendApplicationImmediatelyApproved__c))){
                            System.debug('Send Application Immediately Approved');
                            if(emailTemplateDevNameVsId.get('AllSchoolsCreditApproved') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsCreditApproved');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationImmediatelyApproved__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendApplicationReviewDenied__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&  oldMap.get(studPrgrm.Id).SendApplicationReviewDenied__c != studPrgrm.SendApplicationReviewDenied__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsAdverseActionCreditDeniedAfterReview') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsAdverseActionCreditDeniedAfterReview');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendApplicationReviewDenied__c = false;
                                }
                            }
                        }
                        if(studPrgrm.SendNotCertified__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&   oldMap.get(studPrgrm.Id).SendNotCertified__c != studPrgrm.SendNotCertified__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsCancelledBySchool') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsCancelledBySchool');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendNotCertified__c = false;
                                }
                            }
                        }
                        
                        if(studPrgrm.SendCreditDenied__c && ((!oldMap.containsKey(studPrgrm.Id)) || (oldMap.containsKey(studPrgrm.Id) &&   oldMap.get(studPrgrm.Id).SendCreditDenied__c != studPrgrm.SendCreditDenied__c))){
                            if(emailTemplateDevNameVsId.get('AllSchoolsAdverseActionCreditNotApproved') != null){
                                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsAdverseActionCreditNotApproved');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    studPrgrm.SendCreditDenied__c = false;
                                }
                            }
                        }
                    }
                    studentProgramList.add(studPrgrm);
                }
                
                if(outboundEmailList != null && outboundEmailList.size() > 0)
                    insert outboundEmailList;
                // No need to make the SP checkboxes false
                //if(studentProgramList != null && studentProgramList.size()>0)
                    //update studentProgramList;   
                    
                if(finalDisclosureSPs != null && finalDisclosureSPs.size()>0){
                    outboundEmailList.addAll(createDisclosureOERecords(finalDisclosureSPs));
                }
                if(outboundEmailList != null && outboundEmailList.size() > 0){
                    for(OutboundEmail__c OBEmail : outboundEmailList){
                        newOutboundEmailIds.add(OBEmail.Id);
                    }
                }
            }
        }
        return newOutboundEmailIds;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createOutboundEmailForBill
    //Details: Insert OE records only for Bills.
    /////////////////////////////////////////////////////////////////////////
    public static set<Id> createOutboundEmailForBill(set<Id> billIds){
        set<Id> newOutboundEmailIds = new set<Id>();
        if(billIds != null && billIds.size() > 0){
            List<Bill__c> billList = new List<Bill__c>();
            billList = [Select Id, SendStatement__c, Email__c, Student__c, Student__r.PersonEmail, Student__r.PersonContactId From Bill__c Where Id In : billIds];
            if(billList.size() > 0){
                List<OutboundEmail__c> outboundEmailList = new List<OutboundEmail__c>();
                for(Bill__c bill : billList){
                    if(bill.Student__c != null){
                        String toAddress = null;
                        if(bill.Student__r.PersonEmail != null){
                            toAddress = bill.Student__r.PersonEmail;
                        }
                        if(bill.Email__c != null){
                            toAddress = bill.Email__c;
                        }
                        Id whatId = bill.Id;
                        Id targetObjectId = null;
                        if(bill.Student__r.PersonContactId != null){
                            targetObjectId = bill.Student__r.PersonContactId;
                        }
                        Boolean sendViaSes = true;
                        if(bill.SendStatement__c){
                            if(emailTemplateDevNameVsId.get('StatementV1') != null){
                                Id templateId = emailTemplateDevNameVsId.get('StatementV1');
                                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                                if(newOutboundEmail != null){
                                    outboundEmailList.add(newOutboundEmail);
                                    bill.SendStatement__c = false;
                                }
                            }
                        }
                    }
                }
                if(outboundEmailList.size() > 0){
                    insert outboundEmailList;
                    //No need to update
                    //update billList;
                    for(OutboundEmail__c OBEmail : outboundEmailList){
                        newOutboundEmailIds.add(OBEmail.Id);
                    }
                }
            }
        }
        return newOutboundEmailIds;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createOutboundEmailForDisbursement
    //Details: Insert OE records only for Disbursement.
    /////////////////////////////////////////////////////////////////////////
    public static set<Id> createOutboundEmailForDisbursement(set<Id> trxnIds){
        set<Id> newOutboundEmailIds = new set<Id>();
        
        if(trxnIds.size() > 0){
            Map<ID, Transaction__c> tranMap = TransactionQueries.getTransactionMapWithTransactionID(trxnIds,'Disbursement');
            
            if(tranMap != null && tranMap.size() > 0){
                List<OutboundEmail__c> outboundEmailList = new List<OutboundEmail__c>();
                for(Transaction__c trxn : tranMap.values()){
                    String toAddress = null;
                    if(trxn.Student__r.PersonEmail != null){
                        toAddress = trxn.Student__r.PersonEmail;
                    }
                    if(trxn.NotificationEmail__c != null){
                        toAddress = trxn.NotificationEmail__c;
                    }
                    Id whatId = trxn.Id;
                    Id targetObjectId = null;
                    if(trxn.Student__r.PersonContactId != null){
                        targetObjectId = trxn.Student__r.PersonContactId;
                    }
                    Boolean sendViaSes = true;
                    if(trxn.Status__c == 'Complete' && trxn.Confirmed__c && trxn.RecordTypeId != null && trxn.RecordType.DeveloperName == 'Disbursement' &&
                            trxn.Agreement__c != null && trxn.Agreement__r.Program__c != null && trxn.Agreement__r.Program__r.SendStudentDisbursementConfirmation__c
                            ){
                        if(emailTemplateDevNameVsId.get('AllSchoolsDisbursementConfirmation') != null){
                            Id templateId = emailTemplateDevNameVsId.get('AllSchoolsDisbursementConfirmation');
                            OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(toAddress, whatId, targetObjectId, templateId, sendViaSes);
                            if(newOutboundEmail != null){
                                outboundEmailList.add(newOutboundEmail);
                            }
                        }
                    }
                }
                if(outboundEmailList.size() > 0){
                    insert outboundEmailList;
                    for(OutboundEmail__c OBEmail : outboundEmailList){
                        newOutboundEmailIds.add(OBEmail.Id);
                    }
                }
            }
        }
        return newOutboundEmailIds;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createDisclosureOERecords
    //Details: Insert OE record only for SP (SendApplicationCertifield).
    //         Also attach disclosure attachment with the mail
    /////////////////////////////////////////////////////////////////////////
    public static List<OutboundEmail__c> createDisclosureOERecords(List<StudentProgram__c> stProgramsList)
    {
        set<string> attNamesSet = new set<string>();
        List<OutboundEmail__c> outboundEmailList = new List<OutboundEmail__c>();
        List<attachment> attListtoInsert = new List<attachment>();
        Map<id,Attachment> spIdNAttachMap = new Map<id,Attachment>();
        
        for(StudentProgram__c sp : stProgramsList){
            attNamesSet.add('final-disclosure-'+String.valueOf(sp.id).substring(0,15)+'.pdf');
        }
        List<attachment> attList = [SELECT id, Body, ContentType, Name,ParentId FROM Attachment where name IN:attNamesSet];
        
        for(attachment att : attList){
            spIdNAttachMap.put(att.parentId,att);
        }
        
        for(StudentProgram__c studPrgrm : stProgramsList){
            if(emailTemplateDevNameVsId.get('AllSchoolsStudentFinalDisclosure') != null){
                Id templateId = emailTemplateDevNameVsId.get('AllSchoolsStudentFinalDisclosure');
                OutboundEmail__c newOutboundEmail = CreateOutboundEmail.createOERecord(studPrgrm.Student__r.PersonEmail, studPrgrm.Id, studPrgrm.Student__r.PersonContactID, templateId, true);
                if(newOutboundEmail != null){
                    outboundEmailList.add(newOutboundEmail);
                    studPrgrm.SendApplicationCertified__c = false;
                }
            }
        }
        
        if(outboundEmailList != null && outboundEmailList.size()>0) insert outboundEmailList;
        
        for(OutboundEmail__c oe : outboundEmailList){
            if(spIdNAttachMap != null && spIdNAttachMap.get(oe.whatId__c) != null){
                Attachment att = spIdNAttachMap.get(oe.whatId__c);
                Attachment attCloneCopy = att.clone(false,false,false,false);  // Cloning the attachment for OE
                attCloneCopy.parentId = oe.id;      
                attListtoInsert.add(attCloneCopy);
            }
        }
        
        if(attListtoInsert != null && attListtoInsert.size()>0) insert attListtoInsert;
        // No need to make the SP checkboxes false
        //if(stProgramsList != null && stProgramsList.size()>0) upsert stProgramsList;
        
        return outboundEmailList;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createOERecord
    //Details: Return outbound Email record to Insert
    /////////////////////////////////////////////////////////////////////////
    public static OutboundEmail__c createOERecord(String toAddress, Id whatId, Id targetObjectId, Id templateId, Boolean sendViaSes){
        OutboundEmail__c newOutboundEmail = new OutboundEmail__c();
        newOutboundEmail.ToAddresses__c = toAddress;
        newOutboundEmail.WhatID__c = whatId;
        newOutboundEmail.TargetObjectId__c = targetObjectId;
        newOutboundEmail.TemplateID__c = templateId;
        newOutboundEmail.SendviaSES__c = sendViaSes;
        return newOutboundEmail;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: findTemplates
    //Details: Creating the Template Map <Name->Id> for all the templates used 
    //         for sending the mails
    /////////////////////////////////////////////////////////////////////////
    public static void findTemplates(){
         
         emailTemplateDevNameVsId = new Map<String, Id>();
         Set<String> templateDevNames = new Set<String>{'AllSchoolsApplicationComplete', 'AllSchoolsSchoolInitiatedApplicationComplete', 
                                'AllSchoolsSchoolInitiatedInvitation','AllSchoolsCreditPending', 'AllSchoolsCreditApproved',
                                'AllSchoolsCancelledBySchool', 'AllSchoolsAdverseActionCreditNotApproved', 'StatementV1', 
                                'AllSchoolsDisbursementConfirmation','AllSchoolsStudentFinalDisclosure','AllSchoolsCreditApprovedAfterReview','AllSchoolsAdverseActionCreditDeniedAfterReview'};
         
         List<EmailTemplate> templateList = [Select id, developerName from EmailTemplate where developerName IN: templateDevNames];    
         
         if(templateList != null && templateList.size()>0){ 
             for(EmailTemplate et: templateList){
                 emailTemplateDevNameVsId.put(et.developerName, et.id);    
             }
         }
    }
}