@isTest
public class PlaidBankQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    @isTest public static void validateGetPlaidBankMapWithStudentID(){

      Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
      Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
      Map<ID, PlaidBank__c> plaidBanks = TestDataFactory.createAndInsertPlaidBank(students, 1); 
      Test.startTest();
      Map<ID, PlaidBank__c> plaidBankMap = PlaidBankQueries.getPlaidBankMapWithStudentID(students.keySet());
      Test.stopTest();
      System.assertEquals(plaidBankMap.size(), plaidBanks.size());
    }
    
    @isTest public static void validateGetPlaidBankMapWithID(){

      Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
      Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
      Map<ID, PlaidBank__c> plaidBanks = TestDataFactory.createAndInsertPlaidBank(students, 1); 
      Test.startTest();
      Map<ID, PlaidBank__c> plaidBankMap = PlaidBankQueries.getPlaidBankMapWithID(plaidBanks.keySet());
      Test.stopTest();
      System.assertEquals(plaidBankMap.size(), plaidBanks.size());
    }
    
}