@isTest
public class EmploymentHistoryComponent_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();

    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEmploymentHistoryMapWithStudentId(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        Test.startTest();
        EmpHistCompController.getAllEmployer(testStudentAccountMap.values()[0].id);
        EmpHistCompController.getEmplHistory(testStudentAccountMap.values()[0].id,'Test Employer 0');
        //System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
}