public class ProgramBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts {
    public enum JobType {DISB_CONF_NOTIFICATION, CALCULATE_AMOUNTS} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    Set<Id> programIds = new Set<Id>();
    
    public ProgramBatch(Set<Id> programIDs) {
        this.programIds = programIDs;    
    }
    
    public ProgramBatch() {
        job = JobType.DISB_CONF_NOTIFICATION;
    }
    public ProgramBatch(String query) {
        this.query = query;
        job = JobType.DISB_CONF_NOTIFICATION;
    }   
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('ProgramBatch.start()');
        if(job == JobType.DISB_CONF_NOTIFICATION){
            if(String.isEmpty(this.query)){
                String dateToday = String.valueOf(Date.today());
                query = 'SELECT id from Program__c where DisbursementConfRequiredNotification__c = true and AutomaticallyConfirmTransactions__c = false';
                //query = 'SELECT id, Date__c, Amount__c, Status__c from PaymentInstruction__c WHERE Status__c = \'None\' and Date__c <='+dateToday + ' ORDER BY Date__c ASC';
            }
        }
        else if(job == JobType.CALCULATE_AMOUNTS){
            if(String.isEmpty(this.query)){
                query  = 'SELECT id from Program__c WHERE ID IN '  + DatabaseUtil.inSetStringBuilder(programIds);
            }
        }
        System.debug('job:'+job);
        System.debug('query:'+query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('ProgramBatch.execute()');
        Set<ID> scopeIDs = new Set<ID>();
        for(sObject sobj : scope){
            scopeIDs.add(sobj.id);
        }

        if(job == JobType.DISB_CONF_NOTIFICATION){
            Map<ID, Program__c> programMap = ProgramQueries.getProgramMapWithProgramID(scopeIDs);
            //Look for any disbursements waiting for confirmation
            Map<ID, Transaction__c> disbursementMap = new Map<ID, Transaction__c>([SELECT id,
                                                                                          Agreement__c,
                                                                                          Agreement__r.Program__c,
                                                                                          Agreement__r.Student__c,
                                                                                          Agreement__r.Student__r.PersonContactID,
                                                                                          Agreement__r.Status__c
                                                                                   FROM Transaction__c
                                                                                   WHERE Agreement__r.Program__c = :programMap.keySet()
                                                                                    and TransactionDate__c <= :Date.today()
                                                                                    and (Status__c = 'Scheduled'
                                                                                        or (Status__c = 'Complete' and Confirmed__c = false))]);
            Map<ID, List<Transaction__c>> validDisbursementsByProgramID = new Map<ID, List<Transaction__c>>();
            for(Transaction__c tx : disbursementMap.values()){
                Set<String> validStatus = new Set<String>{'Certified', 'Partially Funded'};
                if(validStatus.contains(tx.Agreement__r.Status__c)){
                    if(!validDisbursementsByProgramID.containsKey(tx.Agreement__r.Program__c)){
                        validDisbursementsByProgramID.put(tx.Agreement__r.Program__c, new List<Transaction__c>());
                    }
                    validDisbursementsByProgramID.get(tx.Agreement__r.Program__c).add(tx);                  
                }
            }
            System.debug('validDisbursementsByProgramID:'+validDisbursementsByProgramID);
            List<OutboundEmailService.OutboundEmail> outboundEmails = new List<OutboundEmailService.OutboundEmail>();
            for(ID theID : validDisbursementsByProgramID.keySet()){
                OutboundEmailService.OutboundEmail email = new OutboundEmailService.OutboundEmail();

                email.toAddresses = programMap.get(theID).SchoolEmailNotification2__c;
                //email.templateID = '00X7A000000EJ98';
                email.templateDevName = 'AllSchoolsAwaitingDisbursementConfirmation';
                email.whatID = theID;//validDisbursementsByProgramID.get(theID)[0].id;
                email.targetObjectId = validDisbursementsByProgramID.get(theID)[0].Agreement__r.Student__r.PersonContactID;
                email.sendImmediate = true;

                outboundEmails.add(email);


            }
            OutboundEmailService.createOutboundEmailV1(outboundEmails);         
        }
        else if(job == JobType.CALCULATE_AMOUNTS){
            Set<ID> programIDs = new Set<ID>();
            for(SObject obj : scope){
                programIDs.add(obj.id);
            }
            CalculateAmountsOnProgram(programIds); 
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('ProgramBatch.finish()');      
    }
    
    private static void CalculateAmountsOnProgram(Set<ID> programIDs){
        Map<ID, Program__c> ProgramsWithID = ProgramQueries.getProgramMapWithProgramID(programIDs);
        //Map<ID, StudentProgram__c> AgreementMapWithProgramID = StudentProgramQueries.getStudentProgramMapWithProgramID(programIDs); 
        Map<ID, StudentProgram__c> AgreementMapWithProgramID = new Map<ID, StudentProgram__c>([Select id, Status__c, Program__c, CertificationDate__c , FundingAmountPostCertification__c from StudentProgram__c where Program__c IN: programIDs]);
        Map<ID, Transaction__c> transById = new Map<ID, Transaction__c>([Select id, Agreement__c, RecordType.Name, Confirmed__c, Status__c, Amount__c  from Transaction__c where Agreement__c IN: AgreementMapWithProgramID.keySet()]);
        
        Map<ID, List<Transaction__c>> transByAgreementId = new Map<ID, List<Transaction__c>>();
        
        //if(AgreementMapWithProgramID != null){
        //    transByAgreementId  = TransactionQueries.getTransactionMapByAgreementWithAgreementID(AgreementMapWithProgramID.keySet(), 'Disbursement');
        //}
        
        for(Transaction__c tx: transById.values()){
            if(!transByAgreementId.containsKey(tx.Agreement__c)){
                transByAgreementId.put(tx.Agreement__c, new List<Transaction__c>());
            }
            transByAgreementId.get(tx.Agreement__c).add(tx);
        }
        
        Map<ID, List<StudentProgram__c>> AgreementMapByProgramID = new Map<ID, List<StudentProgram__c>>();
        
        List<Program__c> programsToUpdate = new List<Program__c>();
        
        if(AgreementMapWithProgramID != null){
            for(StudentProgram__c agreement: AgreementMapWithProgramID.values()){
                if(!AgreementMapByProgramID.containsKey(agreement.Program__c)){
                    AgreementMapByProgramID.put(agreement.program__c, new List<StudentProgram__c>()); 
                }
                AgreementMapByProgramID.get(agreement.program__c).add(agreement);
            }
        }
        
        for(Program__c prog: ProgramsWithID.values()){
            Decimal CertifiedAmount = 0;
            Decimal DisbursedAmount = 0;
            if(AgreementMapByProgramID.containsKey(prog.Id)){
                for(StudentProgram__c sp: AgreementMapByProgramID.get(prog.Id)){
                    if(sp.CertificationDate__c != null && sp.Status__c != 'Cancelled'){
                        if(sp.FundingAmountPostCertification__c != null) CertifiedAmount += sp.FundingAmountPostCertification__c;
                    }
                    
                    if(transByAgreementID != null && transByAgreementID.containsKey(sp.id)){ 
                        for(Transaction__c tx: transByAgreementID.get(sp.id)){
                            if(tx.RecordType.Name == 'Disbursement' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;
                            }     
                            else if(tx.RecordType.Name == 'Disbursement Refund' && tx.Status__c == 'Complete' && tx.Confirmed__c == true){
                                    if(tx.Amount__c != null) DisbursedAmount -= tx.Amount__c;
                            }   
                            
                            
                        }
                    }
                }
            }
            
            programsToUpdate.add(new Program__c(id = prog.Id,
                                                AmountCertifiedToDate__c = CertifiedAmount,
                                                AmountDisbursedToDate__c = DisbursedAmount,
                                                CalculateAmounts__c = false)); 
        }
        
        if(programsToUpdate.size()>0)
            update programsToUpdate;
    }
}