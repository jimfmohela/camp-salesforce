/**
 * An apex page controller that exposes the site login functionality
 */
global with sharing class SiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    
    global SiteLoginController () {
    }

    global PageReference login() {
    	username = '';
    	password = '';
    	username = Apexpages.currentPage().getParameters().get('usernameJS');
    	password = Apexpages.currentPage().getParameters().get('passwordJS');
    	System.debug('>>>>>username -->'+username);
    	System.debug('>>>>>username -->'+password);
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        PageReference redirectURL = Site.login(username, password, startUrl);
        return redirectURL;
    }
    
    @RemoteAction
    global Static ProcessingResult login(String credentialObj){
    	ProcessingResult resultObj = new ProcessingResult ();
    	resultObj.isSuccess = false;
    	resultObj.result = 'Opps ! An error had occured, please contact your admin !';
    	try {
    		System.debug(credentialObj);
	    	String username = '';
	    	String password = '';
	    	if(credentialObj != null){
				JSONParser parser = JSON.createParser(credentialObj);
				while (parser.nextToken() != null) {
				 	if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME && parser.getText() == 'email') {
				 		parser.nextToken();
				 		username = parser.getText();
				 	}
				 	else if(parser.getCurrentToken() == System.JSONToken.FIELD_NAME && parser.getText() == 'password'){
				 		parser.nextToken();
				 		password = parser.getText();
				 	}
				}
	    	}
	    	System.debug('>>>>>username-->'+username);
	    	System.debug('>>>>>password-->'+password);
	    	String startUrl = null;
	    	if(System.currentPageReference() != null){
	    		startUrl = System.currentPageReference().getParameters().get('startURL');
	    	}
	        PageReference redirectURL = Site.login(username, password, startUrl);
	        if(redirectURL != null){
	        	resultObj.isSuccess = true;
    			resultObj.result = redirectURL.getURL();
	        }
	        else {
	        	resultObj.result = 'Username or Password is invalid !';
	        }
	        
    	} catch(Exception e){
    		resultObj.result = 'Error - '+e.getMessage();
    	}
    	System.debug(JSON.serialize(resultObj));
    	return resultObj;
    }
    
    global class ProcessingResult {
    	global Boolean isSuccess {get;set;}
    	global String result {get;set;}
    }
}