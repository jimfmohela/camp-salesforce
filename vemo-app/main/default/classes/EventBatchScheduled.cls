public class EventBatchScheduled implements Schedulable {

    public EventBatch.JobType jobType {get;set;}
    public String query {get;set;}
    
    public void execute(SchedulableContext sc) {
        EventBatch job = new EventBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job, 150);
    }
}