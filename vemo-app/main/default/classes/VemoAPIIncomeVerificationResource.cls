/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIIncomeVerificationResource
// 
// Description: 
//  Direction Central for IncomeVerification API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-05-12   Jared Hagemann  Created              
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIIncomeVerificationResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }   
            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){  
        System.debug('VemoAPIIncomeVerificationResource.handleGetV1');
        String incomeVerificationIDParam = api.params.get('incomeVerificationID');
        String studentIDParam = api.params.get('studentID');
        String latestParam = api.params.get('latest');
        List<IncomeVerificationService.IncomeVerification> incVerList = new List<IncomeVerificationService.IncomeVerification>();
        if(incomeVerificationIDParam != null){
            incVerList = IncomeVerificationService.getIncomeVerificationWithIncomeVerificationID(VemoApi.parseParameterIntoIDSet(incomeVerificationIDParam));
        }
        else if(studentIDParam != null){
            incVerList = IncomeVerificationService.getIncomeVerificationWithStudentId(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter incomeVerificationID or studentID');
        }
        List<IncomeVerificationOutputV1> results = new List<IncomeVerificationOutputV1>();
        IncomeVerificationService.IncomeVerification latestIncomeVerification;
        
        if(latestParam == 'true'){
            for(IncomeVerificationService.IncomeVerification incVer : incVerList){
                if(incVer.verified == 'true' && incVer.beginDate <= Date.today()){
                    if(latestIncomeVerification == null || incVer.beginDate > latestIncomeVerification.beginDate) 
                        latestIncomeVerification = incVer;
                    
                    else if(incVer.beginDate == latestIncomeVerification.beginDate){
                        if(incVer.type == 'Reported') latestIncomeVerification = incVer;    
                    }
                }
            }           
        }
        
        Set<ID> idsToVerify = new Set<ID>();
        
        for(IncomeVerificationService.IncomeVerification incVer : incVerList){
            idsToVerify.add((ID)incVer.studentID);     
        }
        
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(IncomeVerificationService.IncomeVerification incVer : incVerList){
            if(verifiedAccountMap.containsKey((ID)incVer.studentID)){
                if(latestParam == 'true'){
                    if(latestIncomeVerification != null && incVer.incomeVerificationID == latestIncomeVerification.incomeVerificationID ){
                        results.add(new IncomeVerificationOutputV1(incVer));
                    }
                } else {
                    results.add(new IncomeVerificationOutputV1(incVer));                
                }
            }

        }
        return (new VemoAPI.ResultResponse(results, results.size()));

    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIIncomeVerificationResource.handlePostV1');
        List<IncomeVerificationService.IncomeVerification> newIncomeVerification = new List<IncomeVerificationService.IncomeVerification>();
        List<IncomeVerificationInputV1> incomeVerificationJSON = (List<IncomeVerificationInputV1>)JSON.deserialize(api.body, List<IncomeVerificationInputV1>.class);
        
        Set<ID> idsToVerify = new Set<ID>();
        for(IncomeVerificationInputV1 incVer : incomeVerificationJSON){
            idsToVerify.add((ID)incVer.studentID); 
        }
        
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(IncomeVerificationInputV1 incVer : incomeVerificationJSON){
            if(verifiedAccountMap.containsKey((ID)incVer.studentID)){
                incVer.validatePOSTFields();
                IncomeVerificationService.IncomeVerification incVerServ = new IncomeVerificationService.IncomeVerification();
                incVerServ = incomeVerificationInputV1ToIncomeVerification(incVer);
                newIncomeVerification.add(incVerServ);
            }
        }
        Set<ID> incVerIDs = IncomeVerificationService.createIncomeVerification(newIncomeVerification);
        return (new VemoAPI.ResultResponse(incVerIDs, newIncomeVerification.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIIncomeVerificationResource.handlePutV1');
        List<IncomeVerificationService.IncomeVerification> newIncomeVerification = new List<IncomeVerificationService.IncomeVerification>();
        List<IncomeVerificationInputV1> incomeVerificationJSON = (List<IncomeVerificationInputV1>)JSON.deserialize(api.body, List<IncomeVerificationInputV1>.class);
        
        //Set<ID> IncomeVerificationIdsToBeUpdated = new Set<ID>();
        Set<ID> idsToVerify = new Set<ID>();
        for(IncomeVerificationInputV1 incVer : incomeVerificationJSON){
            idsToVerify.add(incVer.incomeVerificationID); 
        }
        
        //Set<ID> idsToVerify = new Set<ID>();
        Map<ID, IncomeVerification__c> VerifiedIncomeVerificationMap = IncomeVerificationQueries.getIncomeVerificationMapWithIncomeVerificationId(idsToVerify); //authorized records should be returned
        //for(IncomeVerification__c incVer: incomeVerificationMap.values()){
        //    idsToVerify.add(incVer.student__c);
        //}
        //Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        for(IncomeVerificationInputV1 incVer : incomeVerificationJSON){
            if(VerifiedIncomeVerificationMap.containsKey((ID)incVer.incomeVerificationID)){
                incVer.validatePUTFields();
                IncomeVerificationService.IncomeVerification incVerServ = new IncomeVerificationService.IncomeVerification();
                incVerServ = incomeVerificationInputV1ToIncomeVerification(incVer);
                newIncomeVerification.add(incVerServ);
            }
        }
        Set<ID> incVerIDs = IncomeVerificationService.updateIncomeVerification(newIncomeVerification);
        return (new VemoAPI.ResultResponse(incVerIDs, newIncomeVerification.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIIncomeVerificationResource.handleDeleteV1');
        String incomeVerificationIDParam = api.params.get('incomeVerificationID');
        
        Map<ID, IncomeVerification__c> incomeVerificationMap = IncomeVerificationQueries.getIncomeVerificationMapWithIncomeVerificationId(VemoApi.parseParameterIntoIDSet(incomeVerificationIDParam));
        
        Set<ID> idsToVerify = new Set<ID>();
        for(IncomeVerification__c incVer: incomeVerificationMap.values()){
            idsToVerify.add(incVer.student__c); 
        }
        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        Set<ID> IncomeVerificationToBeDeleted = new Set<ID>();
        
        for(IncomeVerification__c incVer: IncomeVerificationMap.values()){
            if(verifiedAccountMap.containsKey(incVer.Student__c)){
                IncomeVerificationToBeDeleted.add(incVer.id); 
            }
        }
        Integer numToDelete = 0;
        if(IncomeVerificationToBeDeleted.size()>0){
           numToDelete = IncomeVerificationService.deleteIncomeVerificationy(IncomeVerificationToBeDeleted);    
        }
        
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }

    public class IncomeVerificationOutputV1{
        
        public IncomeVerificationOutputV1(IncomeVerificationService.IncomeVerification incVer){
            this.incomeVerificationID = incVer.incomeVerificationID;            
            this.incomePerYear = incVer.incomePerYear;
            this.incomePerMonth = incVer.incomePerMonth;
            //this.employmentHistoryID = incVer.employmentHistoryID;
//          this.dateVerified = incVer.dateVerified;
            this.studentID = incVer.studentID;
            this.verified = incVer.verified;
            this.beginDate = incVer.beginDate;
            this.dateReported = incVer.dateReported;
            this.type = incVer.type;
            this.endDate = incVer.endDate;
            this.status = incVer.status;
        }
        public String incomeVerificationID {get;set;}
        public Decimal incomePerYear {get;set;}
        public Decimal incomePerMonth {get;set;}
        //public String employmentHistoryID {get;set;}
//      public Date dateVerified {get;set;}
        public Date dateReported {get;set;}
        public Date beginDate {get;set;}
        public String studentID {get;set;}
        public String verified {get;set;}
        public String type {get;set;}
        public Date endDate {get;set;}
        public String status {get;set;}
    }

    public class IncomeVerificationInputV1{
        public String incomeVerificationID {get;set;}
        public Decimal incomePerMonth {get;set;}
        //public String employmentHistoryID {get;set;}
        public String studentID {get;set;}
        public Date dateReported {get;set;}
        public Date beginDate {get;set;}
        public Date endDate {get;set;}
        public String type {get;set;}
        public String status {get;set;}

        public IncomeVerificationInputV1(Boolean testValues){
            if(testValues){
                this.incomePerMonth = 1000; 
            }
        }

        public void validatePOSTFields(){
            if(incomeVerificationID != null) throw new VemoAPI.VemoAPIFaultException('incomeVerificationID cannot be created in POST');
            if(incomePerMonth == null) throw new VemoAPI.VemoAPIFaultException('incomePerMonth is a required parameter on POST');
        }
        public void validatePUTFields(){
            if(incomeVerificationID == null) throw new VemoAPI.VemoAPIFaultException('incomeVerificationID is a required input parameter on PUT');
            if(studentID != null) throw new VemoAPI.VemoAPIFaultException('studentID is not a writable input parameter on PUT');
        }
    }

    public static IncomeVerificationService.IncomeVerification incomeVerificationInputV1ToIncomeVerification(IncomeVerificationInputV1 input){
        IncomeVerificationService.IncomeVerification output = new IncomeVerificationService.IncomeVerification();
        output.incomeVerificationID = input.incomeVerificationID;           
        output.incomePerMonth = input.incomePerMonth;
        //output.employmentHistoryID = input.employmentHistoryID;
        output.studentID = input.studentID;
        output.dateReported = input.dateReported;
        output.beginDate = input.beginDate;
        output.endDate = input.endDate;
        output.type = input.type;
        output.status = input.status;
        return output;
    }
}