public with sharing class VemoAPIEmploymentSeekingAttemptResource  {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }           
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentSeekingAttemptResource.handleGetV1');
        String EmploymentSeekingAttemptIDParam = api.params.get('employmentSeekingAttemptID');
        String defermentIDParam = api.params.get('defermentID');
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> vcs = new List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt>();
        if(EmploymentSeekingAttemptIDParam != null){
            vcs = EmploymentSeekingAttemptService.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(VemoApi.parseParameterIntoIDSet(EmploymentSeekingAttemptIDParam));
        }
        else if(defermentIDParam != null){
            vcs = EmploymentSeekingAttemptService.getEmploymentSeekingAttemptMapWithDefermentID(VemoApi.parseParameterIntoIDSet(defermentIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: EmploymentSeekingAttemptID or defermentIDParam required for GET');
        }
        List<EmploymentSeekingAttemptResourceOutputV1> results = new List<EmploymentSeekingAttemptResourceOutputV1>();
        for(EmploymentSeekingAttemptService.EmploymentSeekingAttempt vc : vcs){
            results.add(new EmploymentSeekingAttemptResourceOutputV1(vc));
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentSeekingAttemptResource.handlePostV1');
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> newEmploymentSeekingAttempts = new List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt>();
        List<EmploymentSeekingAttemptResourceInputV1> EmploymentSeekingAttemptsJSON = (List<EmploymentSeekingAttemptResourceInputV1>)JSON.deserialize(api.body, List<EmploymentSeekingAttemptResourceInputV1>.class);
        
         /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        for(EmploymentSeekingAttemptResourceInputV1 attRes : EmploymentSeekingAttemptsJSON){
            idsToVerify.add((ID)attRes.defermentID);
        }
        Map<ID, Deferment__c> verifiedDefermentMap = DefermentQueries.getDefermentMapWithDefermentID(idsToVerify); //authorized records should be returned
        
        for(EmploymentSeekingAttemptResourceInputV1 EmploymentSeekingAttemptRes : EmploymentSeekingAttemptsJSON){
            if(verifiedDefermentMap.containsKey((ID)EmploymentSeekingAttemptRes.defermentID)){
                EmploymentSeekingAttemptRes.validatePOSTFields();
                newEmploymentSeekingAttempts.add(EmploymentSeekingAttemptResourceV1ToEmploymentSeekingAttempt(EmploymentSeekingAttemptRes));
            }
        }
        Set<ID> EmploymentSeekingAttemptIDs = EmploymentSeekingAttemptService.createEmploymentSeekingAttempts(newEmploymentSeekingAttempts);

        return (new VemoAPI.ResultResponse(EmploymentSeekingAttemptIDs, EmploymentSeekingAttemptIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentSeekingAttemptResource.handlePutV1');
        List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt> updateEmploymentSeekingAttempts = new List<EmploymentSeekingAttemptService.EmploymentSeekingAttempt>();
        List<EmploymentSeekingAttemptResourceInputV1> EmploymentSeekingAttemptsJSON = (List<EmploymentSeekingAttemptResourceInputV1>)JSON.deserialize(api.body, List<EmploymentSeekingAttemptResourceInputV1>.class);
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        for(EmploymentSeekingAttemptResourceInputV1 attRes : EmploymentSeekingAttemptsJSON){
            idsToVerify.add((ID)attRes.employmentSeekingAttemptID);
        }
        Map<ID, EmploymentSeekingAttempt__c> verifiedAttemptMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(idsToVerify); //authorized records should be returned
        
        for(EmploymentSeekingAttemptResourceInputV1 EmploymentSeekingAttemptRes : EmploymentSeekingAttemptsJSON){
            if(verifiedAttemptMap.containsKey((ID)EmploymentSeekingAttemptRes.employmentSeekingAttemptID)){
                EmploymentSeekingAttemptRes.validatePUTFields();
                updateEmploymentSeekingAttempts.add(EmploymentSeekingAttemptResourceV1ToEmploymentSeekingAttempt(EmploymentSeekingAttemptRes));
            }
        }
        Set<ID> EmploymentSeekingAttemptIDs = EmploymentSeekingAttemptService.updateEmploymentSeekingAttempts(updateEmploymentSeekingAttempts);

        return (new VemoAPI.ResultResponse(EmploymentSeekingAttemptIDs, EmploymentSeekingAttemptIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentSeekingAttemptResource.handleDeleteV1()');            
        String EmploymentSeekingAttemptIDParam = api.params.get('employmentSeekingAttemptID');       

        Map<Id, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(VemoApi.parseParameterIntoIDSet(EmploymentSeekingAttemptIDParam));

        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        for(EmploymentSeekingAttempt__c att : EmploymentSeekingAttemptMap.values()){
            idsToVerify.add((ID)att.Deferment__c);
        }
        Map<ID, Deferment__c> verifiedDefermentMap = DefermentQueries.getDefermentMapWithDefermentID(idsToVerify); //authorized records should be returned
        
        
        Set<ID> EmploymentSeekingAttemptsToBeDeleted = new Set<ID>();
        for(EmploymentSeekingAttempt__c de: EmploymentSeekingAttemptMap.values()){
            if(verifiedDefermentMap.containsKey(de.Deferment__c)){
                EmploymentSeekingAttemptsToBeDeleted.add(de.id);
            }
        }
        Integer numToDelete = 0;
        if(EmploymentSeekingAttemptsToBeDeleted.size()>0){
            numToDelete = EmploymentSeekingAttemptService.deleteEmploymentSeekingAttempts(EmploymentSeekingAttemptsToBeDeleted); 
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }

    public static EmploymentSeekingAttemptService.EmploymentSeekingAttempt EmploymentSeekingAttemptResourceV1ToEmploymentSeekingAttempt(EmploymentSeekingAttemptResourceInputV1 EmploymentSeekingAttemptRes){
        EmploymentSeekingAttemptService.EmploymentSeekingAttempt de = new EmploymentSeekingAttemptService.EmploymentSeekingAttempt();
        de.EmploymentSeekingAttemptID = EmploymentSeekingAttemptRes.EmploymentSeekingAttemptID ;
        de.DefermentId = EmploymentSeekingAttemptRes.DefermentID; 
        de.cityEmployment = EmploymentSeekingAttemptRes.cityEmployment ;
        de.contactDate = EmploymentSeekingAttemptRes.contactDate ;           
        de.countryEmployment = EmploymentSeekingAttemptRes.countryEmployment ;            
        de.employerName = EmploymentSeekingAttemptRes.employerName ;  
        de.followUpDate = EmploymentSeekingAttemptRes.followUpDate ;                      
        de.resultsOutcome = EmploymentSeekingAttemptRes.resultsOutcome ;
        de.stateEmployment = EmploymentSeekingAttemptRes.stateEmployment ;               
        de.jobPositionTypeofWork = EmploymentSeekingAttemptRes.JobPositionTypeofWork;
        
        return de;
    }

    public class EmploymentSeekingAttemptResourceInputV1{
        public String employmentSeekingAttemptID{get;set;}
        public String defermentID {get;set;}
        public String cityEmployment{get;set;}
        public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String employerName {get;set;}
        public Date followUpDate{get;set;}        
        public String resultsOutcome {get;set;}   
        public String stateEmployment {get;set;}           
        public String jobPositionTypeofWork {get;set;}
        

        public EmploymentSeekingAttemptResourceInputV1(Boolean testValues){
            if(testValues){
                //student = 'testStudentID';    
            }
        }

        public void validatePOSTFields(){
            if(employmentSeekingAttemptID != null) throw new VemoAPI.VemoAPIFaultException('employmentSeekingAttemptID cannot be created in POST');
            if(defermentID == null) throw new VemoAPI.VemoAPIFaultException('defermentId is a required input parameter on POST');
        }

        public void validatePUTFields(){
            if(EmploymentSeekingAttemptID == null) throw new VemoAPI.VemoAPIFaultException('EmploymentSeekingAttemptID is a required input parameter on PUT');
        }
    }

    public class EmploymentSeekingAttemptResourceOutputV1{
        public String employmentSeekingAttemptID{get;set;}
        public String defermentID {get;set;}
        public String cityEmployment{get;set;}
        public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String employerName {get;set;}
        public Date followUpDate{get;set;}        
        public String resultsOutcome {get;set;}   
        public String stateEmployment {get;set;}           
        public String jobPositionTypeofWork {get;set;}
                

        public EmploymentSeekingAttemptResourceOutputV1(EmploymentSeekingAttemptService.EmploymentSeekingAttempt de){
            this.EmploymentSeekingAttemptID = de.EmploymentSeekingAttemptID ;
            this.defermentID = de.defermentID;
            this.cityEmployment = de.cityEmployment ;
            this.contactDate = de.contactDate ;           
            this.countryEmployment = de.countryEmployment ;            
            this.employerName = de.employerName ;  
            this.followUpDate = de.followUpDate ;                      
            this.resultsOutcome = de.resultsOutcome ;
            this.stateEmployment = de.stateEmployment ;               
            this.jobPositionTypeofWork  = de.JobPositionTypeofWork;
            
        }
    }
}