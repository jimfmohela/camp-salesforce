public with sharing class ReportAllAccountsWithIncomeController{
    public String selectedSchool {get;set;}
    transient public List<reportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    private Integer currentMonth;
    private Integer currentYear;
    private static Set<String> exclusionStatus = new Set<String>{'Draft','Invited','Application Incomplete','Application Complete','Application Under Review'};  

    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportAllAccountsWithIncomeController(){
        selectedSchool = '';
        reportData = new List<reportDataWrapper>();    
        currentMonth = Date.today().month();
        currentYear = Date.today().year();
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    public List<SelectOption> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        List<SelectOption> output  = new List<SelectOption>();
        for(Account sch:result){
            output.add(new SelectOption(sch.id,sch.name));    
        }
        return output; 
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //Final output Map 
        Map<String,ReportDataWrapper> outputMap = new Map<String,ReportDataWrapper>();  
        
        //Set of agreement ID's
        Set<ID> agreementIDs = new Set<ID>(); 
        
        //Map of Agreements by Student
        Map<ID,List<StudentProgram__c>> studentAgreementMap = new Map<ID,List<StudentProgram__c>>(); 
        
        //Map of Income by Student
        Map<ID,IncomeVerification__c> studentIncomeMap = new Map<ID,IncomeVerification__c>();
        
        //Map of Payments by Agreement
        Map<ID,Decimal> agreementPaymentMap = new Map<ID,Decimal>();
        
        //Map of last Month Payments by agreements
        Map<ID,Decimal> lastMonthPayments = new Map<ID,Decimal>();
        
        //Map of AgreementId and Delinquency Status
        Map<ID,String> agreementDelinquencyStatusMap = new Map<ID,String>();
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //get all agreements for selected program
        getAgreements(studentAgreementMap,agreementIDs);
        
        //get delinquency data
        getDelinquencyData(agreementIDs,agreementDelinquencyStatusMap);
        
        //get Income Data
        if(studentAgreementMap.keySet().size()>0){
            getIncomeData(studentAgreementMap.keySet(),studentIncomeMap);
        }
        
        //get payment data
        if(agreementIDs.size()>0){
            getPaymentData(agreementIDs,agreementPaymentMap,lastMonthPayments);         
        }
               
        //populate the final reportData list 
        reportData = buildReportData(studentAgreementMap ,studentIncomeMap,agreementPaymentMap,lastMonthPayments,agreementDelinquencyStatusMap);
        
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public List<ReportDataWrapper> buildReportData(Map<ID,List<StudentProgram__c>> studentAgreementMap,Map<ID,IncomeVerification__c> studentIncomeMap,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments,Map<ID,String> agreementDelinquencyStatusMap){
        List<ReportDataWrapper> output = new List<ReportDataWrapper>();
        
        for(ID studentID:studentAgreementMap.keySet()){
            ReportDataWrapper wrapper = new ReportDataWrapper();
            for(StudentProgram__c sp:studentAgreementMap.get(studentID)){
                wrapper.vemoAccountNumber = sp.StudentVemoAccountNumber__c;
                wrapper.studentID = sp.student__c;
                wrapper.studentName = sp.student__r.name;
                AgreementWrapper agre = new AgreementWrapper();
                agre.agreement = sp;
                //bucket by delinquency
                if(agreementDelinquencyStatusMap.containsKey(sp.id)){
                    agre.delinquencyBucket = agreementDelinquencyStatusMap.get(sp.id);
                }
                
                if(agreementPaymentMap.containsKey(sp.id)) agre.cumulativePayments = agreementPaymentMap.get(sp.id);
                if(lastMonthPayments.containsKey(sp.id)) agre.lastMonthPayments = lastMonthPayments.get(sp.id);
                
                wrapper.agreements.add(agre);
                
            }
            if(studentIncomeMap.containsKey(studentID)){
                wrapper.income = studentIncomeMap.get(studentID);
            }
            output.add(wrapper);
        }
        
        return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student Programs for the selected School
    ///////////////////////////////////////////////
    public void getAgreements(Map<ID,List<StudentProgram__c>> agreementsByStudent,Set<ID> agreementIDs){
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        List<StudentProgram__c> agreementList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                                                 PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,
                                                 ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                                                 MajorPostCertification__r.name,GradeLevelPostCertification__c,EnrollmentStatusPostCertification__c,
                                                 FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentTermPostCertification__c,
                                                 PaymentCapPostCertification__c,
                                                 GraceMonthsAllowed__c,GraceMonthsRemaining2__c,PaymentTermAssessed__c,PaymentTermRemaining__c,
                                                 DefermentMonthsAllowed__c,DefermentMonthsRemaining2__c,MinimumIncomePerMonth__c,
                                                 DaysDelinquent__c,servicing__c
                                                 FROM StudentProgram__c
                                                 WHERE Status__c NOT IN :exclusionStatus AND Program__r.school__c =: selectedSchool AND CertificationDate__c <> null AND certificationDate__c < :dt
                                                 ORDER BY student__r.name ASC];            
        Integer offset;
        for(StudentProgram__c agreement:agreementList){
            if(agreement.certificationDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.certificationDate__c);
                agreement.certificationDate__c = agreement.certificationDate__c.addSeconds(offset/1000);
            }
            if(agreement.ApplicationStartDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.ApplicationStartDate__c);
                agreement.ApplicationStartDate__c = agreement.ApplicationStartDate__c.addSeconds(offset/1000);
            }
            if(agreement.submittedDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.submittedDate__c);
                agreement.submittedDate__c = agreement.submittedDate__c.addSeconds(offset/1000);
            }
            
            if(!agreementsByStudent.containsKey(agreement.student__c)){
                agreementsByStudent.put(agreement.student__c,new List<StudentProgram__c>());
            }
            agreementsByStudent.get(agreement.student__c).add(agreement);
            agreementIDs.add(agreement.ID);
        }
    }
    
    /////////////////////////////////////////////////
    ///Description: get delinquency data
    /////////////////////////////////////////////////
    private void getDelinquencyData(Set<ID> agreementIDs,Map<ID,String> agreementDelinquencyStatusMap){
        List<StudentProgramAudit__c> spaList = new List<StudentProgramAudit__c>();
        spaList = [SELECT daysdelinquent__c,studentprogram__c,auditdatetime__c,servicing__c
                   FROM StudentProgramAudit__c
                   WHERE monthEnd__c = true AND studentProgram__c IN :agreementIDs AND createdDate = LAST_MONTH];
        for(StudentProgramAudit__c audit:spaList){
                if(audit.DaysDelinquent__c >= 270){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'270+ Days Delinquent');        
                }
                else if(audit.DaysDelinquent__c < 270 && audit.DaysDelinquent__c >= 210){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'210-269 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 210 && audit.DaysDelinquent__c >= 180){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'180-209 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 180 && audit.DaysDelinquent__c >= 120){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'120-179 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 120 && audit.DaysDelinquent__c >= 90){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'90-119 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 90 && audit.DaysDelinquent__c >= 60){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'60-89 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 60 && audit.DaysDelinquent__c >= 30){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'30-59 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 30 && audit.DaysDelinquent__c >= 0 && audit.servicing__c == true){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'Current');
                }
        }
    }
    
    ////////////////////////////////////////////////////////////////
    ///Description: Get the Payment Data
    ////////////////////////////////////////////////////////////////
    public void getPaymentData(Set<ID> agreementIDs,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments){
        Datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        Integer month = date.today().month() == 1 ? 12 : date.today().month()-1;
        Integer year = date.today().month() == 1 ? (date.today().year()-1) : date.today().year();
        
         
        /*
        AggregateResult[] groupedResults = [SELECT agreement__c agrID,sum(AmountAllocated__c) 
                                            FROM PaymentAllocation__c
                                            WHERE agreement__c IN :agreementIDs AND createdDate < :dt
                                            GROUP BY agreement__c];    
    
        for(AggregateResult ar:groupedResults){
            String agreementID = (String)ar.get('agrID');
            Decimal sum = (Decimal)ar.get('expr0'); 
            agreementPaymentMap.put(agreementID,sum); 
        }
        */
        List<PaymentAllocation__c> payments = [SELECT id,agreement__c,amountAllocated__c,createddate
                                         FROM PaymentAllocation__c
                                         WHERE agreement__c IN :agreementIDs AND createdDate < :dt];
        for(PaymentAllocation__c payment:payments){
            if(!agreementPaymentMap.containsKey(payment.agreement__c)){
                agreementPaymentMap.put(payment.agreement__c,0);
            }
            agreementPaymentMap.put(payment.agreement__c,agreementPaymentMap.get(payment.agreement__c)+payment.amountallocated__c); 
            
            if(payment.createdDate.date().month() == month && payment.createdDate.date().year() == year){
                if(!lastMonthPayments.containsKey(payment.agreement__c)){
                   lastMonthPayments.put(payment.agreement__c,0);
                }
                lastMonthPayments.put(payment.agreement__c,lastMonthPayments.get(payment.agreement__c) + payment.amountAllocated__c);            
            }    
        }
    }
    
    //////////////////////////////////////////////////
    ///Get Income Data
    //////////////////////////////////////////////////
    public void getIncomeData(Set<ID> studentIDs,Map<ID,IncomeVerification__c> studentIncomeMap){    
        List<incomeVerification__c> ivList = new List<incomeVerification__c>();
        ivList = [Select id,student__c,beginDate__c,endDate__c,type__c,IncomePerMonth__c,DateVerified__c,
                  EmploymentHistory__r.name,EmploymentHistory__c,EmploymentHistory__r.Employer__c,EmploymentHistory__r.DateReported__c,EmploymentHistory__r.type__c,
                  EmploymentHistory__r.EmploymentStartDate__c,EmploymentHistory__r.EmploymentEndDate__c,EmploymentHistory__r.Category__c 
                  From IncomeVerification__c
                  Where Student__c IN :studentIDs AND status__c = 'Verified' AND type__c = 'Reported' 
                  Order By beginDate__c DESC,Student__r.name ASC,type__c DESC];
        Map<Id,List<IncomeVerification__c>> sortedIncomesByStudent = new Map<Id,List<IncomeVerification__c>>(); 
        for(IncomeVerification__c iv:ivList){
            if(!sortedIncomesByStudent.containsKey(iv.student__c)){
                sortedIncomesByStudent.put(iv.student__c,new List<IncomeVerification__c>());
            }
            sortedIncomesByStudent.get(iv.student__c).add(iv);
        }
        
        Date dt = Date.newInstance(date.today().year(),date.today().month(),1); 
        for(ID key:sortedIncomesByStudent.keySet()){
            List<incomeVerification__c> incomeList = sortedIncomesByStudent.get(key);
            //studentIncome.put(key,new IncomeVerification__c()); 
            Integer size = incomeList.size();
            for(integer i=0;i<incomeList.size();i++){
                if(incomeList[i].BeginDate__c < dt){
                    studentIncomeMap.put(key,incomeList[i]);
                }
                else{
                    continue;
                }
                if(!(i==(incomeList.size()-1))){
                    if(incomeList[i].BeginDate__c == incomeList[i+1].BeginDate__c){
                        if(incomeList[i].dateVerified__c > incomeList[i+1].DateVerified__c){
                            break;        
                        }
                        /* not reporting on estimated income
                        if(incomeList[i].type__c == incomeList[i+1].type__c){
                            if(incomeList[i].dateVerified__c > incomeList[i+1].DateVerified__c){
                                break;        
                            }        
                        }
                        else{
                            if(incomeList[i].type__c == 'Reported'){
                                break;
                            }        
                        }
                        */
                    }
                    else{
                        break;
                    }
                }
            }
        } 
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportAllAccountsWithIncomeExport');
        pg.setRedirect(false);
        return pg;
    }
    
    ////////////////////////////////////////
    ///Generate a csv string
    ////////////////////////////////////////
    public void buildCsvString(){
        csv = 'Vemo Account Number,Student Program Number,Vemo Contract Number,Status,School Name,Program Name,';
        csv += 'Primary Owner,Application Start Date,Submitted Date,Certification Date,Servicing Start Date,';
        csv += 'Major(Post-Certification),Grade Level(Post-Certification),Enrollment Status(Post-Certification),';
        csv += 'Funding Amount(Post-Certification),Income Share(Post-Certification),Payment Term(Post-Certification),';
        csv += 'Payment Cap(Post-Certification),Deferment Months Allowed,Deferment Months Remaining,Grace Months Allowed,Grace Months Remaining,Payment Term Assessed,';
        csv += 'Payment Term Remaining,Minimum Income Per Month,Sum Of Payments($),Current Month Payment($),Payment Status,Employment History Number,Employer Name,Date Reported,Type,Employment Start Date,';
        csv += 'Employment End Date,Category,Reported Income Per Month,Begin Date,End Date';
        csv += '\n';
        runReport();
        
        if(reportData == null || reportData.size()==0) return;
        
        
        for(reportDataWrapper row:reportData){
            Boolean firstrow = true;
            for(AgreementWrapper agre:row.agreements){
                csv += row.vemoAccountNumber + ',';
                csv += agre.agreement.name + ',';
                csv += agre.agreement.vemoContractNumber__c + ',';
                csv += agre.agreement.status__c + ',';
                
                if(agre.agreement.schoolName__c <> null) csv += agre.agreement.schoolName__c.escapeCsv() + ',';
                else csv += ',';
                
                if(agre.agreement.programName__c <> null) csv += agre.agreement.programName__c.escapeCsv() + ',';
                else csv += ',';
                
                if(agre.agreement.primaryOwner__r.name <> null) csv += agre.agreement.primaryOwner__r.name + ',';    
                else csv += ',';
                
                if(agre.agreement.ApplicationStartDate__c <> null) csv += agre.agreement.ApplicationStartDate__c + ',';    
                else csv += ',';
                
                if(agre.agreement.SubmittedDate__c <> null) csv += agre.agreement.SubmittedDate__c + ',';    
                else csv += ',';
                
                if(agre.agreement.CertificationDate__c <> null) csv += agre.agreement.CertificationDate__c + ',';    
                else csv += ',';
                
                if(agre.agreement.ServicingStartDate__c <> null) csv += agre.agreement.ServicingStartDate__c + ',';    
                else csv += ',';
                
                if(agre.agreement.majorPostCertification__r.name <> null) csv += agre.agreement.majorPostCertification__r.name.escapeCsv() + ',';    
                else csv += ',';
                
                if(agre.agreement.gradeLevelPostCertification__c <> null) csv += agre.agreement.gradeLevelPostCertification__c + ',';    
                else csv += ',';
                
                if(agre.agreement.EnrollmentStatusPostCertification__c <> null) csv += agre.agreement.EnrollmentStatusPostCertification__c + ',';    
                else csv += ',';
                
                if(agre.agreement.FundingAmountPostCertification__c <> null) csv += agre.agreement.FundingAmountPostCertification__c + ',';    
                else csv += ',';
                
                if(agre.agreement.IncomeSharePostCertification__c <> null) csv += agre.agreement.IncomeSharePostCertification__c + ',';    
                else csv += ',';
                
                if(agre.agreement.PaymentTermPostCertification__c <> null) csv += agre.agreement.PaymentTermPostCertification__c + ',';    
                else csv += ',';
                
                if(agre.agreement.PaymentCapPostCertification__c <> null) csv += agre.agreement.PaymentCapPostCertification__c + ',';    
                else csv += ',';
                
                //
                if(agre.agreement.DefermentMonthsAllowed__c <> null) csv += agre.agreement.DefermentMonthsAllowed__c + ',';    
                else csv += ',';
                
                if(agre.agreement.DefermentMonthsRemaining2__c <> null) csv += agre.agreement.DefermentMonthsRemaining2__c + ',';    
                else csv += ',';
                
                if(agre.agreement.GraceMonthsAllowed__c <> null) csv += agre.agreement.GraceMonthsAllowed__c + ',';    
                else csv += ',';
                
                if(agre.agreement.GraceMonthsRemaining2__c <> null) csv += agre.agreement.GraceMonthsRemaining2__c + ',';    
                else csv += ',';
                
                if(agre.agreement.PaymentTermAssessed__c <> null) csv += agre.agreement.PaymentTermAssessed__c + ',';    
                else csv += ',';
                
                if(agre.agreement.PaymentTermRemaining__c <> null) csv += agre.agreement.PaymentTermRemaining__c + ',';    
                else csv += ',';
                
                if(agre.agreement.MinimumIncomePerMonth__c <> null) csv += agre.agreement.MinimumIncomePerMonth__c + ',';    
                else csv += ',';
                
                csv += agre.cumulativePayments + ',';
                csv += agre.lastMonthPayments + ',';
                csv += agre.delinquencyBucket + ',';
                
                //      
                if(firstrow){
                    if(row.income<>null){          
                        if(row.income.EmploymentHistory__r.name <> null) csv += row.income.EmploymentHistory__r.name + ',';    
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.Employer__c <> null) csv += row.income.EmploymentHistory__r.Employer__c.escapeCsv() + ',';
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.DateReported__c <> null) csv += row.income.EmploymentHistory__r.DateReported__c + ',';
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.type__c <> null) csv += row.income.EmploymentHistory__r.type__c + ',';
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.EmploymentStartDate__c <> null) csv += row.income.EmploymentHistory__r.EmploymentStartDate__c + ',';
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.EmploymentEndDate__c <> null) csv += row.income.EmploymentHistory__r.EmploymentEndDate__c + ',';
                        else csv += ',';
                        
                        if(row.income.EmploymentHistory__r.Category__c <> null) csv += row.income.EmploymentHistory__r.Category__c.escapeCsv() + ',';
                        else csv += ',';
                        
                        if(row.income.IncomePerMonth__c <> null) csv += row.income.IncomePerMonth__c + ',';
                        else csv += ',';
                        
                        if(row.income.beginDate__c <> null) csv += row.income.beginDate__c + ',';
                        else csv += ',';
                        
                        if(row.income.endDate__c <> null) csv += row.income.endDate__c + ',';
                        else csv += ',';
                    }
                }
                
                firstrow=false;
                csv += '\n';
            }
            
            
        }
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentID {get;set;}
        public String studentName {get;set;}
        public List<AgreementWrapper> agreements {get;set;}
        public IncomeVerification__c income {get;set;}
        
        public ReportDataWrapper(){
            this.agreements = new List<AgreementWrapper>();
            this.income = new IncomeVerification__c();
        }    
    }
    
    //////////////////////////////////////////
    ///Agreement Wrapper
    //////////////////////////////////////////
    public class AgreementWrapper{
        public StudentProgram__c agreement {get;set;}
        public String delinquencyBucket {get;set;}
        public Decimal cumulativePayments {get;set;}
        public Decimal lastMonthPayments {get;set;}
        public AgreementWrapper(){
            this.delinquencyBucket = '';
            this.cumulativePayments = 0;
            this.lastMonthPayments = 0;
        }
    }  
    
      
}