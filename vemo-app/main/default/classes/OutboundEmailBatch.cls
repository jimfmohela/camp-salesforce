global class OutboundEmailBatch implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful {
    public enum JobType {SEND_EMAIL} 
    
    public String query {get;set;}
    public JobType job {get;set;}
  
    global OutboundEmailBatch() {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(job == JobType.SEND_EMAIL){
            if(String.isEmpty(this.query)){
                query = 'SELECT id,TemplateID__c,TargetObjectId__c,whatId__c,toaddresses__c,CcAddresses__c,bccAddresses__c,PlainTextBody__c,HtmlBody__c,Subject__c from OutboundEmail__c where SendviaSES__c = true AND Complete__c = false  order by CreatedDate desc';
            }
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(job == JobType.SEND_EMAIL){
            OutboundEmailService oService = new OutboundEmailService();
            oService.sendEmail(scope);
        }
    }
  
    global void finish(Database.BatchableContext BC) {
    }
}