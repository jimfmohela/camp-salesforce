public with sharing class DataCollectionTemplateService{
    
    public static List<Template> getTemplateWithProgramID(Set<ID> programIDs){
        Map<ID, DataCollectionTemplate__c> templateMap = new Map<ID, DataCollectionTemplate__c>();
        String query = generateSOQLSelect();
        query += ' WHERE program__c IN ' + DatabaseUtil.inSetStringBuilder(programIds);
        if(DatabaseUtil.filterBySchoolID){
           ID schoolID = DatabaseUtil.schoolID;
           query += ' and Program__r.School__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
        }
        DatabaseUtil.orderBy = 'CreatedDate asc';
        query += DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        system.debug('query--'+query );
        templateMap = new Map<ID, DataCollectionTemplate__c>((List<DataCollectionTemplate__c>)db.query(query));
        List<Template> temps = new List<Template>();
        for(DataCollectionTemplate__c template : templateMap.values()){
            Template temp = new Template(template);
            temps.add(temp);
        }
        return temps;
    }
     
     
     private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
     }
     
      private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getDataCollectionTemplateFieldNames() + ' FROM DataCollectionTemplate__c ';
        return soql;
      }
     
     private static String getDataCollectionTemplateFieldNames(){
        String fieldNames;
        fieldNames = 'Id, ';
        fieldNames += 'DataType__c, ';
        fieldNames += 'Label__c, ';
        fieldNames += 'PicklistChoices__c, ';
        fieldNames += 'Program__c, ';
        fieldNames += 'Required__c';
        
        return fieldNames;
     }
    
       
    public class Template{
        public String dataCollectionTemplateId {get;set;}
        public String DataType {get;set;}
        public String Label {get;set;}
        public String PicklistChoices {get;set;}
        public String programID {get;set;}
        public String required {get;set;}
        
        public Template(DataCollectionTemplate__c template){
            this.dataCollectionTemplateID = template.id;
            this.DataType = template.DataType__c;
            this.Label = template.Label__c;
            this.PicklistChoices = template.PicklistChoices__c; 
            this.programID = template.Program__c;
            this.required = String.valueOf(template.Required__c);
        }
    }


}