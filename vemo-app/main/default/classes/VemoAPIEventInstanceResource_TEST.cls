/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIEventInstanceResource_TEST
// 
// Description: 
//      Test class for VemoAPIEventInstanceResource
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-18   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIEventInstanceResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );

        Map<String, String> eventInsParams = new Map<String, String>();
        eventInsParams.put('read', 'false');
        eventInsParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo empHisApiInfo = TestUtil.initializeAPI('v2', 'GET', eventInsParams, null);

        Test.startTest();
        
        VemoAPI.ResultResponse eventInsResult = (VemoAPI.ResultResponse)VemoAPIEventInstanceResource.handleAPI(empHisApiInfo);
        System.assertEquals(testEventInstanceMap.size(), eventInsResult.numberOfResults);

        Test.stopTest();
    }

    static testMethod void testHandlePutV2(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEventInstanceResource.EventInstanceResourceInputV2> evntInstanceList = new List<VemoAPIEventInstanceResource.EventInstanceResourceInputV2>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEventInstanceResource.EventInstanceResourceInputV2 evntInstance = new VemoAPIEventInstanceResource.EventInstanceResourceInputV2();
            evntInstance.eventInstanceID = testEventInstanceMap.values().get(i).ID;
            evntInstance.read = true;
            evntInstanceList.add(evntInstance);
        }
        String body = JSON.serialize(evntInstanceList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEventInstanceResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }    
    /*static testMethod void testHandleDeleteV2(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('eventInstanceID', TestUtil.createStringFromIDSet(testEventInstanceMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEventInstanceResource.handleAPI(apiInfo);
        Test.stopTest();
    }*/
}