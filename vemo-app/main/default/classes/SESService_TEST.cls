@isTest
public with sharing class SESService_TEST {

    @isTest static void testPostCallout() {
        Test.setMock(HttpCalloutMock.class, new SESService_TEST.SESServiceHttpCalloutMock());
        SESService.MailAttachment ma = new SESService.MailAttachment();
        ma.name = 'Test-Attachment';
        ma.body = Blob.valueOf('This is test body');
        ma.contentType = 'text/plain';
        
        SESService.Email e = new SESService.Email();
        e.to = 'sesservicetest@test.com';
        e.bcc = 'sesservicetest@test.com';
        e.cc = 'sesservicetest@test.com';
        e.subject = 'Subject - Test';
        e.bodyText = 'This is test body';
        e.bodyHtml = 'This is test body';
        e.attachmentList = new List<SESService.MailAttachment>();
        e.attachmentList.add(ma);
        Boolean response = SESService.sendEmail(e);
    }
    
    public class SESServiceHttpCalloutMock implements HttpCalloutMock {
        
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response
            HttpResponse response = new HttpResponse();
            response.setBody('');
            response.setStatusCode(200);
            return response; 
        }
  }
}