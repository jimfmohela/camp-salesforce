@isTest
public class VemoAPIEmploymentHistory2Resource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);


        Map<String, String> empHisParams = new Map<String, String>();
        empHisParams.put('employmentHistoryID', TestUtil.createStringFromIDSet(testEmpHisMap.keySet()));
        empHisParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo empHisApiInfo = TestUtil.initializeAPI('v2', 'GET', empHisParams, null);

        Map<String, String> studParams = new Map<String, String>();
        studParams.put('studentID', TestUtil.createStringFromIDSet(testStudentAccountMap.keySet()));
        studParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo studApiInfo = TestUtil.initializeAPI('v2', 'GET', studParams, null);

        Test.startTest();
        VemoAPI.ResultResponse empHisResult = (VemoAPI.ResultResponse)VemoAPIEmploymentHistory2Resource.handleAPI(empHisApiInfo);
        System.assertEquals(testEmpHisMap.size(), empHisResult.numberOfResults);

        VemoAPI.ResultResponse studResult = (VemoAPI.ResultResponse)VemoAPIEmploymentHistory2Resource.handleAPI(studApiInfo);
        System.assertEquals(testEmpHisMap.size(), studResult.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePostV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
		Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
		
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2> empHisList = new List<VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2 empHis = new VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2(true);
            empHis.studentID = testStudentAccountMap.values().get(i).ID;
			empHis.employerID = employerMap.values().get(i).Id; 
            empHisList.add(empHis);
        }
        String body = JSON.serialize(empHisList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentHistory2Resource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePutV2(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2> empHisList = new List<VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2 empHis = new VemoAPIEmploymentHistory2Resource.EmploymentHistoryInputV2(true);
            empHis.employmentHistoryID = testEmpHisMap.values().get(i).ID;
            //empHis.verified = false;
            empHisList.add(empHis);
        }
        String body = JSON.serialize(empHisList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentHistory2Resource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }

    static testMethod void testHandleDeleteV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('employmentHistoryID', TestUtil.createStringFromIDSet(testEmpHisMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentHistory2Resource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        System.assertEquals(0, EmploymentHistoryQueries2.getEmploymentHistoryMap().size());
        Test.stopTest();
    }
}