/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIDefermentResource_TEST
// 
// Description: 
//      Test class for VemoAPIDefermentResource
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-09-25   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIDefermentResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);

        Map<String, String> defParams = new Map<String, String>();
        defParams.put('DefermentID', TestUtil.createStringFromIDSet(testDefMap.keySet()));
        defParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo defApiInfo = TestUtil.initializeAPI('v1', 'GET', DefParams, null);

        Map<String, String> studParams = new Map<String, String>();
        studParams.put('studentID', TestUtil.createStringFromIDSet(testStudentAccountMap.keySet()));
        studParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo studApiInfo = TestUtil.initializeAPI('v1', 'GET', studParams, null);

        Test.startTest();
        VemoAPI.ResultResponse defResult = (VemoAPI.ResultResponse)VemoAPIDefermentResource.handleAPI(DefApiInfo);
        System.assertEquals(testDefMap.size(), defResult.numberOfResults);

        VemoAPI.ResultResponse studResult = (VemoAPI.ResultResponse)VemoAPIDefermentResource.handleAPI(studApiInfo);
        System.assertEquals(testDefMap.size(), studResult.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePostV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIDefermentResource.DefermentResourceInputV1> defList = new List<VemoAPIDefermentResource.DefermentResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIDefermentResource.DefermentResourceInputV1 def = new VemoAPIDefermentResource.DefermentResourceInputV1(true);
            def.studentID = testStudentAccountMap.values().get(i).ID;
            defList.add(def);
        }
        String body = JSON.serialize(DefList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIDefermentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePutV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIDefermentResource.DefermentResourceInputV1> defList = new List<VemoAPIDefermentResource.DefermentResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIDefermentResource.DefermentResourceInputV1 def = new VemoAPIDefermentResource.DefermentResourceInputV1(true);
            def.DefermentID = testDefMap.values().get(i).ID;
            def.verified = 'false';
            defList.add(def);
        }
        String body = JSON.serialize(defList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIDefermentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }

    static testMethod void testHandleDeleteV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('DefermentID', TestUtil.createStringFromIDSet(testDefMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIDefermentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }
}