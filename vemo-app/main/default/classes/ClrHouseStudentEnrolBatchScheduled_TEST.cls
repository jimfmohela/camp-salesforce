@isTest
public class ClrHouseStudentEnrolBatchScheduled_TEST {
	
    @isTest
    static void ClrHouseStudentEnrolBatchScheduled(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ClrHouseStudentEnrolBatchScheduled BatchJob = new ClrHouseStudentEnrolBatchScheduled();
        BatchJob.jobType = ClrHouseStudentEnrolBatch.JobType.CLEARINGHOUSE;
        System.schedule('ClrStudentEnrolBatch_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
}