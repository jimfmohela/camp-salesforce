@isTest
public class NotificationNActivityViewCls_TEST{
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    public static void testNotificationNActivityViewCls(){
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Task tsk = new Task(WhatId = testStudentAccountMap.values()[0].id, Subject = 'Email: apex test', ActivityDate = date.today().addYears(-2), Status = 'Completed');
        dbUtil.insertRecord(tsk);
        
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<ID, Notification__c> testNotificationMap = TestDataFactory.createAndInsertNotification(1, testContactMap);
        
        
        Test.StartTest();
        apexpages.currentpage().getparameters().put('accId',testStudentAccountMap.values()[0].id);
        apexpages.currentpage().getparameters().put('record','Notification');
        apexpages.currentpage().getparameters().put('id',testNotificationMap.values()[0].id);
        NotificationNActivityViewCls na = new NotificationNActivityViewCls();
        
        apexpages.currentpage().getparameters().put('record','Activity');
        NotificationNActivityViewCls na1 = new NotificationNActivityViewCls();
        Test.StopTest();
    }
}