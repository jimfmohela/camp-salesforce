/*
StudentAccountScheduled job2 = new StudentAccountScheduled();
job2.job = StudentAccountBatch.JobType.RECURRING_PAYMENT_GENERATION;
String cronStr2 = '0 0 * * * ? *';
System.schedule('Auto Payment Instruction Creation - Hourly', cronStr2, job2);
*/
public class StudentAccountScheduled implements Schedulable {
    public StudentAccountBatch.JobType job {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        StudentAccountBatch batchJob = new StudentAccountBatch();
        batchJob.job = job;
        batchJob.query = this.query;
        Database.executeBatch(batchJob);
    }
}