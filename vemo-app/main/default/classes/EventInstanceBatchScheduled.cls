public class EventInstanceBatchScheduled implements Schedulable {

    public EventInstanceBatch.JobType jobType {get;set;}
    public String query {get;set;}
    
    public void execute(SchedulableContext sc) {
        EventInstanceBatch job = new EventInstanceBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job);
    }
}