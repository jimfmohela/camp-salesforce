/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIPicklistResource
//
// Description:
// This is used to handle all GET API calls related to Picklist service.
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-09-13   Ranjeet Kumar   Created
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIPicklistResource {

    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        /*
        if((api.version == 'v2') && (api.method == 'POST')){
            //need to add logic for PSOT if required
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            //need to add logic for PUT if required
        }
        if((api.version == 'v2') && (api.method == 'DELETE')){
            //need to add logic for DELETE if required
        }
        */
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }

    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        system.debug('VemoAPIPicklistResource.handleGetV1()');
        String resourceParam = api.params.get('resource');
        String attributeParam = api.params.get('attribute');
        String dependentAttributeParam = api.params.get('dependentAttribute');
        String controllingValueParam = api.params.get('dependentValue');
        
        if(resourceParam != null && attributeParam != null){
            PicklistService.PicklistServiceOutputV2 response = new PicklistService.PicklistServiceOutputV2();
            response = PicklistService.getPicklistValues(resourceParam, attributeParam, dependentAttributeParam, controllingValueParam);
            if(response.isSuccess == true){
                return (new VemoAPI.ResultResponse(response.availableValues, response.availableValues.size()));
            }
            else if(response.isSuccess == false){
                throw new VemoAPI.VemoAPIFaultException('Error: '+response.message);
            }
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: resource and attribute are required for GET');
        }
        return null;
    }
}