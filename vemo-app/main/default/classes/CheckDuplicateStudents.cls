public without sharing class CheckDuplicateStudents{
    
    public static Map<String, Account> CheckDuplicateStudentsByEmail(Set<String> personEmails){
        Map<ID, Account> acctMap = AccountQueries.getStudentMapWithPersonEmails(personEmails);
        Map<String, Account> studentsByEmailMap = new Map<String, Account>(); 
        for(Account stud: acctMap.values()){
            studentsByEmailMap.put(stud.personEmail,stud);    
        }
        return studentsByEmailMap;
    }
}