public with sharing class ReportMnthlyInvstrStaticAllProgCtr{
    public String selectedSchool {get;set;}
    transient public List<SchoolName_ReportDataWrapper> outerReportData {get;set;}
    transient public List<reportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    private Integer currentMonth;
    private Integer currentYear;
    
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportMnthlyInvstrStaticAllProgCtr(){
        selectedSchool = null;
        outerReportData = new List<SchoolName_ReportDataWrapper>();
        reportData = new List<reportDataWrapper>();    
        currentMonth = Date.today().month();
        currentYear = Date.today().year();
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    public List<SelectOption> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        List<SelectOption> output  = new List<SelectOption>();
        for(Account sch:result){
            output.add(new SelectOption(sch.id,sch.name));    
        }
        return output; 
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //Final output Map : Program Id is the key 
        Map<String,ReportDataWrapper> outputMap = new Map<String,ReportDataWrapper>();  
        
        //Map of all agreements
        Map<ID,StudentProgram__c> spMap = new Map<ID,StudentProgram__c>();     
        
        //Map of Agreements sorted by Program
        Map<ID,List<StudentProgram__c>> programAgreementMap = new Map<ID,List<StudentProgram__c>>(); 
        
        //Map of Allocated amount by Agreement
        Map<ID,Decimal> allocationByAgreement = new Map<ID,Decimal>();
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //get all agreements for selected school
        spMap = getAgreements();
        
        //sort the agreements by Program 
        sortAgreements(spMap,programAgreementMap);
        
        //get the payment allocation data
        if(spMap.KeySet().size()>0){
            getPaymentData(spMap,allocationByAgreement);    
        }
        
        //count original and remaining ISAs
        summarizeIsaData(programAgreementMap,allocationByAgreement,outputMap);
        
        //get the Student program Monthly status 
        if(spMap.KeySet().size()>0){
            getMonthlyTracker(spMap,outputMap);
        }
        
        
        
        //calculate percentages
        calculatePercentages(outputMap);
        Map<String,List<reportDataWrapper>> schoolNReportDataMap = new Map<String,List<reportDataWrapper>>();
        reportData = outputMap.values();
        system.debug(reportData);
        for(reportDataWrapper rdw : reportData){
            if(!schoolNReportDataMap.containsKey(rdw.schoolName)){
                schoolNReportDataMap.put(rdw.schoolName,new List<reportDataWrapper>());
            }
            schoolNReportDataMap.get(rdw.schoolName).add(rdw);
        }
        system.debug(schoolNReportDataMap);
        outerReportData = new List<SchoolName_ReportDataWrapper>();
        for(string s: schoolNReportDataMap.keySet()){
        system.debug(s);
            SchoolName_ReportDataWrapper sRdw = new SchoolName_ReportDataWrapper();
            sRdw.schoolName = s;
            sRdw.innerReportData = schoolNReportDataMap.get(s);
            outerReportData.add(sRdw);
        }
    }
    
    
    //////////////////////////////////////////////
    ///Get All agreements for the selected School
    //////////////////////////////////////////////
    private Map<ID,StudentProgram__c> getAgreements(){
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        Map<ID,StudentProgram__c> spMap = new Map<ID,StudentProgram__c>([SELECT id,program__c,program__r.name,program__r.ProgramName__c,Student__c,Status__c,Program__r.school__r.Name,
                                                                         CertificationDate__c,ClosedDate__c,ClosedReason__c,CurrentStatusDate__c,Servicing__c,
                                                                         FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentCapPostCertification__c,
                                                                         PaymentTermPostCertification__c,LastDateOfAttendance__c,DaysDelinquent__c,MonthlyAmountDueToDate__c
                                                                         FROM StudentProgram__c 
                                                                         WHERE /*Program__r.school__c = :selectedSchool AND*/ CertificationDate__c <> null AND certificationDate__c < :dt
                                                                         ORDER BY Program__r.school__r.Name]);
        
        return spMap;
    }
    
    ///////////////////////////////////////////////////
    ///sort the agreements by program
    ////////////////////////////////////////////////////
    private void sortAgreements(Map<ID,StudentProgram__c> spMap,Map<ID,List<StudentProgram__c>> programAgreementMap){
        for(StudentProgram__c sp:spMap.values()){
            if(!programAgreementMap.containsKey(sp.program__c)){
                programAgreementMap.put(sp.program__c,new List<StudentProgram__c>());
            }
            programAgreementMap.get(sp.program__c).add(sp);
        }
    }
    
    ////////////////////////////////////////////////////////////////
    ///Description: Get the Payment Data
    ////////////////////////////////////////////////////////////////
    private void getPaymentData(Map<ID,StudentProgram__c> spMap,Map<ID,Decimal> allocationByAgreement){
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        
        AggregateResult[] groupedResults = [SELECT agreement__c agrID,sum(AmountAllocated__c) 
                                             FROM PaymentAllocation__c
                                             WHERE agreement__c IN :spMap.keySet() AND createdDate < :dt
                                             GROUP BY agreement__c];    
    
        for(AggregateResult ar:groupedResults){
            
            String agreementID = (String)ar.get('agrID');
            Decimal sum = (Decimal)ar.get('expr0'); 
            
            allocationByAgreement.put(agreementID,sum);
        }
    }
    
    /////////////////////////////////////////////////////////////////
    ///Description: get the monthly tracker data
    /////////////////////////////////////////////////////////////////
    private void getMonthlyTracker(Map<ID,StudentProgram__c> spMap,Map<String,ReportDataWrapper> outputMap){
        Integer month = Date.today().month();
        Integer year = Date.today().year();
        if(month == 1){
            month = 12;
            year -= 1;
        } 
        else month -= 1;
        String monthName = getMonthName(month);
        String yearName = String.ValueOf(year);
        
        AggregateResult[] groupedResults = [SELECT agreement__r.program__c prgID,count(id) 
                                            FROM StudentProgramMonthlyStatus__c
                                            WHERE agreement__c IN :spMap.keySet() AND activity__c = 'Payment'
                                            AND month__c = :monthName AND year__c = :yearName
                                            GROUP BY agreement__r.program__c
                                           ];
        
        for(AggregateResult ar:groupedResults){
            String programID = (String)ar.get('prgID');
            Integer count = (Integer)ar.get('expr0');
            
            if(outputMap.containsKey(programID)){
                outputMap.get(programID).countInPaymentISA = count;
            }
        }
    }
    
    ////////////////////////////////////////////////////////
    ///Description: Sort the agreements by program, agreements should be included based on the status as of last day of previous month
    ///till it has been closed Or moved into Pending Reconciliation Status
    ////////////////////////////////////////////////////////
    private void summarizeIsaData(Map<ID,List<StudentProgram__c>> programAgreementMap,Map<ID,Decimal> allocationByAgreement,Map<String,ReportDataWrapper> outputMap){
        for(ID programID:programAgreementMap.keySet()){
            if(!outputMap.containsKey(programID)){
                outputMap.put(programID,new ReportDataWrapper());
            }
            outputMap.get(programID).originalIsaAssests = programAgreementMap.get(programID).size();
            outputMap.get(programID).programName = programAgreementMap.get(programID)[0].program__r.programName__c;
            outputMap.get(programID).programNumber = programAgreementMap.get(programID)[0].program__r.name;
            outputMap.get(programID).schoolName = programAgreementMap.get(programID)[0].program__r.school__r.Name;
            
            for(StudentProgram__c agreement:programAgreementMap.get(programID)){
                if(agreement.fundingAmountPostCertification__c <> null) outputMap.get(programID).sumOfFundingAmount += agreement.fundingAmountPostCertification__c;
                if(agreement.paymentCapPostCertification__c <> null) outputMap.get(programID).sumOfPaymentCap += agreement.paymentCapPostCertification__c;
                if(agreement.incomeSharePostCertification__c <> null) outputMap.get(programID).sumOfIncomeShare += agreement.incomeSharePostCertification__c;
                if(agreement.paymentTermPostCertification__c <> null) outputMap.get(programID).sumOfPaymentTerm  += agreement.paymentTermPostCertification__c;
                
                Date lastDateOfPreviousMonth = Date.newinstance(Date.today().year(),Date.today().month(),1).addDays(-1);
                
                if(agreement.certificationDate__c.Date() <= lastDateOfPreviousMonth){
                    
                    if(agreement.MonthlyAmountDueToDate__c == 0 && agreement.servicing__c == true){
                        outputMap.get(programID).isaNeverPay += 1;    
                    }
                    
                    if(allocationByAgreement.containsKey(agreement.id)){
                        if(agreement.fundingAmountPostCertification__c == allocationByAgreement.get(agreement.id)){
                            outputMap.get(programID).isaPayCap += 1;    
                        }
                        outputMap.get(programID).sumPayments += allocationByAgreement.get(agreement.id); 
                    }
                    
                    if(agreement.Status__c == 'Closed' && agreement.closedDate__c <> null){
                        if(agreement.closedDate__c.Date() <= lastDateOfPreviousMonth){
                            continue;        
                        }    
                    }
                    else if((agreement.status__c == 'Cancelled' || agreement.status__c == 'Pending Reconciliation') && agreement.CurrentStatusDate__c <> null){
                        if(agreement.CurrentStatusDate__c <= lastDateOfPreviousMonth){
                            continue;
                        }
                    }
                    else{
                        outputMap.get(programID).remainingISAAssets += 1; 
                        if(agreement.daysDelinquent__c > 90){
                            outputMap.get(programID).isaNinetyPlusDel += 1;    
                        }  
                    }
                }
                
                
                
                
                
            }
            
            //calculate averages
            if(outputMap.get(programID).originalIsaAssests>0){
                outputMap.get(programID).avgIncomeShare = (outputMap.get(programID).sumOfIncomeShare/outputMap.get(programID).originalIsaAssests).setScale(2);
                outputMap.get(programID).avgPaymentTerm = (outputMap.get(programID).sumOfPaymentTerm/outputMap.get(programID).originalIsaAssests).setScale(2);
            }
        }    
    }
    
    /////////////////////////////////////////////////
    ///Calculate percentages
    /////////////////////////////////////////////////
    private void calculatePercentages(Map<String,ReportDataWrapper> outputMap){
        for(String programID:outputMap.keySet()){
            if(outputMap.get(programID).originalIsaAssests>0){
               //System.debug('@@@FLAG1');
               Decimal origIsa = outputMap.get(programID).originalIsaAssests;
               outputMap.get(programID).remainingIsapercentOrig = ((outputMap.get(programID).remainingIsaAssets/origIsa)*100).setScale(2);
               outputMap.get(programID).inPayIsaPercentOrig = ((outputMap.get(programID).countInPaymentISA/origIsa)*100).setScale(2); 
            }
            if(outputMap.get(programID).sumOfFundingAmount>0){
               outputMap.get(programID).sumPmntsPercentFundingAmt = ((outputMap.get(programID).sumPayments/outputMap.get(programID).sumOfFundingAmount)*100).setScale(2);
            }
            if(outputMap.get(programID).sumOfPaymentCap>0){
               outputMap.get(programID).sumPmntsPercentPayCap = ((outputMap.get(programID).sumPayments/outputMap.get(programID).sumOfPaymentCap)*100).setScale(2);
            }
        }
    }
    
    //////////////////////////////////////////////////
    ///Get month name by number
    //////////////////////////////////////////////////
    private Static String getMonthName(Integer monthNum){
        if(monthNum == 1) return 'January';
        if(monthNum == 2) return 'February';
        if(monthNum == 3) return 'March';
        if(monthNum == 4) return 'April';
        if(monthNum == 5) return 'May';
        if(monthNum == 6) return 'June';
        if(monthNum == 7) return 'July';
        if(monthNum == 8) return 'August';
        if(monthNum == 9) return 'September';
        if(monthNum == 10) return 'October';
        if(monthNum == 11) return 'November';
        if(monthNum == 12) return 'December';
        return '';    
    }
    
    ////////////////////////////////////////
    ///Generate a csv string
    ////////////////////////////////////////
    public void buildCsvString(){
        runReport();
            
        if(outerReportData == null || outerReportData.size()==0) return;
        csv='';
        for(SchoolName_ReportDataWrapper s: outerReportData){
            csv += 'School Name: ' + ',' + s.schoolName + '\n\n';
            csv += ',';
            for(ReportDataWrapper row:s.innerReportData){
                csv += row.programName + ' : ' + row.programNumber + ',';    
            }
            csv += '\n' + 'Original ISA Assests' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.originalIsaAssests + ',';
            }
            csv += '\n' + 'Total All ISA Disbursements' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += '$' + row.sumOfFundingAmount + ',';
            }
            csv += '\n' + 'Max ISA Shared Payments' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += '$' + row.sumOfPaymentCap + ',';
            }
            csv += '\n' + 'WAVG Income Share' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.avgIncomeShare + '%' + ',';
            }
            csv += '\n' + 'WAVG Expected Payment Period (no deferment)' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.avgPaymentTerm + ',';
            }
            csv += '\n' + 'Remaining ISA Assets' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.remainingISAAssets + ',';
            }
            csv += '\n' + 'Remaining ISA Assets as % of Original' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.remainingIsapercentOrig  + '%' + ',';
            }
            csv += '\n' + 'Remaining ISA Assets in Payment Status' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.countInPaymentISA + ',';
            }
            csv += '\n' + 'ISA Assets in Payment Status as % of Original' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.inPayIsaPercentOrig + '%' + ',';
            }
            csv += '\n' + 'Shared Income Payments to-date' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += '$' + row.sumPayments + ',';
            }
            csv += '\n' + 'Shared Payments as % of Disbursed Amount' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.sumPmntsPercentFundingAmt + '%' + ',';
            }
            csv += '\n' + 'Shared Payments as % of Max Shared Payments' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.sumPmntsPercentPayCap + '%' + ',';
            }
            csv += '\n' + 'ISA 90+ Days Delinquent' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.isaNinetyPlusDel + ',';
            }
            csv += '\n' + 'ISA Never Pay' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.isaNeverPay + ',';
            }
            csv += '\n' + 'ISA Pay Cap' + ',';
            for(ReportDataWrapper row:s.innerReportData){
                 csv += row.isaPayCap + ',';
            }
            csv += '\n\n';
        }
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        
        PageReference pg = new PageReference('/apex/ReportMonthlyInvestorStaticAllProgExport');
        pg.setRedirect(false);
        return pg;
    }
    
    /////////////////////////////////////////
    ///Wrapper to hold report data
    ///////////////////////////////////////////
    public class ReportDataWrapper{
        public String schoolName {get;set;}
        public String programName {get;set;}
        public String programNumber {get;set;}
        public Integer originalIsaAssests {get;set;}
        public Integer remainingIsaAssets {get;set;}
        public Decimal remainingIsapercentOrig {get;set;}
        public Decimal sumOfFundingAmount {get;set;}
        public Decimal sumOfPaymentCap {get;set;}
        public Decimal sumOfIncomeShare {get;set;}
        public Decimal sumOfPaymentTerm {get;set;}
        public Decimal avgIncomeShare {get;set;}
        public Decimal avgPaymentTerm {get;set;}
        public Integer countInPaymentISA {get;set;}
        public Decimal inPayIsaPercentOrig {get;set;}
        public Decimal sumPayments {get;set;}
        public Decimal sumPmntsPercentFundingAmt {get;set;}
        public Decimal sumPmntsPercentPayCap {get;set;}
        public Integer isaNeverPay {get;set;}
        public Integer isaPayCap {get;set;}
        public Integer isaNinetyPlusDel {get;set;}
        
        public reportDataWrapper(){
            this.originalIsaAssests = 0;
            this.remainingIsaAssets = 0;
            this.remainingIsapercentOrig = 0;
            this.sumOfFundingAmount = 0;
            this.sumOfPaymentCap = 0;
            this.sumOfIncomeShare = 0;
            this.sumOfPaymentTerm = 0;
            this.avgIncomeShare = 0;
            this.avgPaymentTerm = 0;
            this.countInPaymentISA = 0;
            this.inPayIsaPercentOrig = 0;
            this.sumPayments = 0;
            this.sumPmntsPercentFundingAmt = 0;
            this.sumPmntsPercentPayCap = 0;
            this.isaNeverPay = 0;
            this.isaPayCap = 0;
            this.isaNinetyPlusDel = 0;
        }
    }
    
    /////////////////////////////////////////
    ///Wrapper to hold School name & report data
    ///////////////////////////////////////////
    public class SchoolName_ReportDataWrapper{
        public String schoolName {get;set;}
        public List<reportDataWrapper> innerReportData {get;set;}
    }
}