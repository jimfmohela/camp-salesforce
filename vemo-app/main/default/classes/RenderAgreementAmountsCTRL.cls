/////////////////////////////////////////////////////////////////////////
// Class: RenderAgreementAmountsCTRL
// 
// Description: 
// 	Controller for RenderAgreementAmountsComponent
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-16   Jared Hagemann  Created              
/////////////////////////////////////////////////////////////////////////
public with sharing class RenderAgreementAmountsCTRL {
	public AgreementService.Agreement agreement {get;set;}
	public RenderAgreementAmountsCTRL() {
		Id agreementID = (Id)ApexPages.currentPage().getParameters().get('id');
		List<AgreementService.Agreement> agreements = AgreementService.getAgreementWithAgreementID(new Set<Id>{agreementID});
		AgreementService.determineAmountsDue(agreements);
		agreement = agreements.get(0);
	}

    @AuraEnabled
	public static String getAgreementList(String agreementId){
        List<AgreementService.Agreement> agreementList = new List<AgreementService.Agreement>();
        if(agreementId != null){
            try {
                List<AgreementService.Agreement> agreements = new List<AgreementService.Agreement>();
                agreements = AgreementService.getAgreementWithAgreementID(new Set<Id>{agreementID});
                if(agreements.size() > 0){
                    AgreementService.determineAmountsDue(agreements);
                    agreementList.add(agreements[0]);
                }
            } catch (Exception e) {
                system.debug('>>>>> Error message --> ' + e.getMessage() + ' On Line Number --> ' + e.getLineNumber() + ' In Class RenderAgreementAmountsCTRL');
            }
        }
        return JSON.serialize(agreementList);
    }
}