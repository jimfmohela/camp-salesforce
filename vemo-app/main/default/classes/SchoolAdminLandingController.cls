public with sharing class SchoolAdminLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        System.debug('SchoolAdminLandingController.forwardToStartPage()');
        return Network.communitiesLanding();
    }
    
    public SchoolAdminLandingController() {
        System.debug('SchoolAdminLandingController()');
    }

    //Added this method to support custom Login for SchoolAdmin
    public PageReference forwardToCustomPage() {
        System.debug('SchoolAdminLandingController.forwardToCustomAuthPage()');
        return new PageReference(Site.getPathPrefix() + '/dashboard');
    }
}