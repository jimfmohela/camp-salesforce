public class ClrHouseStudentEnrolBatchScheduled implements Schedulable {
    public ClrHouseStudentEnrolBatch.JobType jobType {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        ClrHouseStudentEnrolBatch job = new ClrHouseStudentEnrolBatch();
        job.job = this.jobType;
        job.query = this.query;
        Database.executeBatch(job);
    }
}