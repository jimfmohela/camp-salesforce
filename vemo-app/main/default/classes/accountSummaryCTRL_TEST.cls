@isTest
public class accountSummaryCTRL_TEST{
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
     @TestSetup static void setupData(){
       /* TestUtil.createStandardTestConditions();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta2@vemo.com', communitynickname = 'testcommunity1');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        //create Programs with school
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(3,TestDataFactory.createAndInsertSchoolCustomerAccounts(2));
               
        //create students
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(2);
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schoolMAp);
        integer cnt = 0;
        for(Account acc:studentMap.values()){
            if(cnt<3){
                acc.PrimarySchool__pc = schoolMap.values()[0].id;
            }
            else{
                acc.PrimarySchool__pc = schoolMap.values()[1].id;
            }    
        }
        //update studentMap.values();
        dbUtil.updateRecords(studentMap.values());
               
        Map<ID, ProgramEligibility__c> eligibilityMap = TestDataFactory.createAndInsertProgramEligibility(2,programMap);
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(2);
        Map<ID, SchoolProgramsOfStudy__c> sposMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schoolMap, posMap);
        
        Map<ID, ContractTerm__c> termMap = TestDataFactory.createAndInsertContractTerms(1,programMap);
        Map<ID, TransactionSchedule__c> txSchMap = TestDataFactory.createAndInsertTransactionSchedule(2, programMap);
        Map<ID, IncomeBand__c> bandMap = TestDataFactory.createAndInsertIncomeBands(1, sposMap);
        
        Map<ID, StudentProgram__c> agreements = TestDataFactory.createAndInsertStudentProgram(3,studentMap,programMap);
        Map<ID, Transaction__c> transactionMap = TestDataFactory.createAndInsertTransactions(3,agreements, 'Disbursement');
        List<transaction__c> transactionList = new list<transaction__c>();//transactionMap.values();
        transactionList.addAll(transactionMap.values());
        transactionList[0].status__c = 'Pending'; 
        transactionList[1].status__c = 'Scheduled';
        //update transactionList;
        dbUtil.updateRecords(transactionList);
        Map<ID, EmploymentHistory__c> empHistory =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);*/
        
        /*
        Map<ID, IncomeVerification__c> incomeVerMap = TestDataFactory.createAndInsertIncomeVerification(4,empHistory); 
        List<incomeVerification__c> incList = new list<incomeVerification__c>();
        incList = incomeVerMap.values(); 
        incList[0].begindate__c = Date.today().toStartOfMonth();
        incList[0].status__c = true;
        incList[0].type__c = 'Estimated';
        incList[1].begindate__c = Date.today().toStartOfMonth();
        incList[1].status__c = true; 
        incList[1].type__c = 'Reported';
        
        //incList.add(incomeVerMap[0]);
        //incList.add(incomeVerMap[1]);
        update incList;
        */
       /* Map<ID, StudentProgramMonthlyStatus__c> monthlystatusMap = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(2,agreements);
        Map<ID, StudentProgramAmountDue__c> amountDueMap = TestDataFactory.createAndInsertStudentProgramAmountDue(agreements , monthlystatusMap );
        for(StudentProgramAmountDue__c amountdue: amountdueMap.values()){
            amountdue.AssessmentDateTime__c = datetime.now().addDays(-30);
        }
        //update amountdueMap.values();
        dbUtil.updateRecords(amountDueMap.values());*/
       /* for(Account acc:studentMap.values()){
            TestDataFactory.createAndInsertStudentProgram(1,new Map<ID,Account>{acc.id => acc},
                                                          ProgramQueries.getProgramMapWithSchoolID(new Set<ID>{acc.PrimarySchool__pc}));
        }
        
        for(ContractTerm__c ct:termMap.values()){
            ct.MinimumFundingAmount__c = 1000;
        }       
        update termMap.values();   */ 
        //}              
    } 
    
    @isTest
    public static void testgetRecords(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta1@vemo.com', communitynickname = 'testcommunity2');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
         Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
         Test.startTest();
         //accountSummaryCTRL obj = new accountSummaryCTRL();
         accountSummaryCTRL.getRecords(acc.id);
         Test.stopTest();
         }
    }

    /////////////////////////////////////////////////////////////
    ///Test Case: only 1 income record with a future begin date
    /////////////////////////////////////////////////////////////
    @isTest
    public static void validateIncomeScenario1(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta3@vemo.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, EmploymentHistory__c> empHistoryMap =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);
        //Map<ID, EmploymentHistory__c> empHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMap();
        Map<ID, IncomeVerification__c> incomeVerficationMap = TestDataFactory.createAndInsertIncomeVerification(1,empHistoryMap);
        
        incomeVerficationMap.values()[0].begindate__c = Date.today().addDays(120); 
        incomeVerficationMap.values()[0].incomePerMonth__c = 1000;                                               
        incomeVerficationMap.values()[0].status__c = 'Verified';
        //update incomeVerficationMap.values();
        dbUtil.updateRecords(incomeVerficationMap.values());
        Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
        Test.startTest();
        //accountSummaryCTRL obj = new accountSummaryCTRL();
        accountSummaryCTRL.wrapperClass output = accountSummaryCTRL.getRecords(acc.id);
        System.assertEquals(0,output.monthlyIncome,'Income should be 0');
        Test.stopTest();
        }
    }

    /////////////////////////////////////////////////////////////
    ///Test Case: only 1 income record with a past begin date
    /////////////////////////////////////////////////////////////
    @isTest
    public static void validateIncomeScenario2(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta4@vemo.com', communitynickname = 'testcommunity3');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        //Map<ID, EmploymentHistory__c> empHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMap();
        Map<ID, EmploymentHistory__c> empHistoryMap =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);
        Map<ID, IncomeVerification__c> incomeVerficationMap = TestDataFactory.createAndInsertIncomeVerification(1,empHistoryMap);
        
        incomeVerficationMap.values()[0].begindate__c = Date.today().addDays(-120); 
        incomeVerficationMap.values()[0].incomePerMonth__c = 1000;                                               
        incomeVerficationMap.values()[0].status__c = 'Verified';
        //update incomeVerficationMap.values();
        dbUtil.updateRecords(incomeVerficationMap.values());
        Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
        Test.startTest();
        //accountSummaryCTRL obj = new accountSummaryCTRL();
        accountSummaryCTRL.wrapperClass output = accountSummaryCTRL.getRecords(acc.id);
        System.assertEquals(1000,output.monthlyIncome,'Income should be 1000');
        Test.stopTest();
        }
    }

    /////////////////////////////////////////////////////////////
    ///Test Case: 2 income records. one with a future begin date
    ///another with past begin date
    /////////////////////////////////////////////////////////////
    @isTest
    public static void validateIncomeScenario3(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta5@vemo.com', communitynickname = 'testcommunity4');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, EmploymentHistory__c> empHistoryMap =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);
        //Map<ID, EmploymentHistory__c> empHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMap();
        Map<ID, IncomeVerification__c> incomeVerficationMap = TestDataFactory.createAndInsertIncomeVerification(2,empHistoryMap);
        
        incomeVerficationMap.values()[0].begindate__c = Date.today().addDays(-120); 
        incomeVerficationMap.values()[0].incomePerMonth__c = 1000;  
        incomeVerficationMap.values()[0].status__c = 'Verified';                                             
        incomeVerficationMap.values()[1].begindate__c = Date.today().addDays(120); 
        incomeVerficationMap.values()[1].incomePerMonth__c = 2000;
        incomeVerficationMap.values()[1].status__c = 'Verified';                                               
        
        //update incomeVerficationMap.values();
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        dbUtil.updateRecords(incomeVerficationMap.values());
        Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
        Test.startTest();
        //accountSummaryCTRL obj = new accountSummaryCTRL();
        accountSummaryCTRL.wrapperClass output = accountSummaryCTRL.getRecords(acc.id);
        System.assertEquals(1000,output.monthlyIncome,'Income should be 1000');
        Test.stopTest();
        }
    }

    /////////////////////////////////////////////////////////////
    ///Test Case: 2 income records with same begin date
    ///one of type Reported another of type estimated
    /////////////////////////////////////////////////////////////
    @isTest
    public static void validateIncomeScenario4(){
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta6@vemo.com', communitynickname = 'testcommunity5');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        //Map<ID, EmploymentHistory__c> empHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMap();
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, EmploymentHistory__c> empHistoryMap =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);
        Map<ID, IncomeVerification__c> incomeVerficationMap = TestDataFactory.createAndInsertIncomeVerification(2,empHistoryMap);
        
        incomeVerficationMap.values()[0].begindate__c = Date.today().addDays(-120);
        incomeVerficationMap.values()[0].type__c = 'Reported'; 
        incomeVerficationMap.values()[0].incomePerMonth__c = 1000;
        incomeVerficationMap.values()[0].status__c = 'Verified';                                               
        incomeVerficationMap.values()[1].begindate__c = Date.today().addDays(-120); 
        incomeVerficationMap.values()[1].type__c = 'Estimated';
        incomeVerficationMap.values()[1].incomePerMonth__c = 2000; 
        incomeVerficationMap.values()[1].status__c = 'Verified';                                              
        
        //update incomeVerficationMap.values();
        dbUtil.updateRecords(incomeVerficationMap.values());
        Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
        Test.startTest();
        //accountSummaryCTRL obj = new accountSummaryCTRL();
        accountSummaryCTRL.wrapperClass output = accountSummaryCTRL.getRecords(acc.id);
        System.assertEquals(1000,output.monthlyIncome,'Income should be 1000');
        Test.stopTest();
        }
    }

    /////////////////////////////////////////////////////////////
    ///Test Case: 2 income records with same begin date
    ///both of same type with different DateVerified
    /////////////////////////////////////////////////////////////
    @isTest
    public static void validateIncomeScenario5(){
        TriggerSettings.getSettings().IncomeVerificationTrigger = false;
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'rini.gupta7@vemo.com', communitynickname = 'testcommunity6');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id,Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, EmploymentHistory__c> empHistoryMap =  TestDataFactory.createAndInsertEmploymentHistory(1,studentMap);
        //Map<ID, EmploymentHistory__c> empHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMap();
        Map<ID, IncomeVerification__c> incomeVerficationMap = TestDataFactory.createAndInsertIncomeVerification(2,empHistoryMap);
        
        incomeVerficationMap.values()[0].begindate__c = Date.today().addDays(-120);
        incomeVerficationMap.values()[0].type__c = 'Reported';
        incomeVerficationMap.values()[0].dateVerified__c = Date.today();
        incomeVerficationMap.values()[0].incomePerMonth__c = 1000;
        incomeVerficationMap.values()[0].status__c = 'Verified';                                               
        incomeVerficationMap.values()[1].begindate__c = Date.today().addDays(-120); 
        incomeVerficationMap.values()[1].type__c = 'Reported';
        incomeVerficationMap.values()[1].dateVerified__c = Date.today().addDays(-10);
        incomeVerficationMap.values()[1].incomePerMonth__c = 2000; 
        incomeVerficationMap.values()[1].status__c = 'Verified';                                              
        
       // update incomeVerficationMap.values();
        dbUtil.updateRecords(incomeVerficationMap.values());
        Account acc = [Select id,personEmail,PrimarySchool__pc From Account Where recordType.developerName = 'Student' Limit 1];
        Test.startTest();
        //accountSummaryCTRL obj = new accountSummaryCTRL();
        accountSummaryCTRL.wrapperClass output = accountSummaryCTRL.getRecords(acc.id);
        System.assertEquals(1000,output.monthlyIncome,'Income should be 1000');
        Test.stopTest();
        }
    }
}