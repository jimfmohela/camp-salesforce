/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIStudentResource
// 
// Description: 
//  Direction Central for Student API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-12-17   Greg Cook       Moved functionality for handleGetV1, handlePostV1, handlePutV1, and handleDeleteV1 to StudentService
// 2016-12-24   Greg Cook       Added validatePOSTFields functions                             
// 
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIStudentResource implements VemoAPI.ResourceHandler {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v2') && (api.method == 'POST')){
            return handlePostV2(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        system.debug('VemoAPIStudentResource.handleGetV1()');
        String studentIDParam = api.params.get('studentID');
        String studentAuthIDParam = api.params.get('authSystemUserID');
        String studentEmailParam = api.params.get('email');
        List<StudentService.Student> students = new List<StudentService.Student>();
        if(studentIDParam != null){
            students = StudentService.getStudentsWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam));
        } else if(studentAuthIDParam != null){
            students = StudentService.getStudentsWithAuthID(VemoApi.parseParameterIntoStringSet(studentAuthIDParam));
        } else if(studentEmailParam != null){
            students = StudentService.getStudentsWithEmail(VemoApi.parseParameterIntoStringSet(studentEmailParam));
        } else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: studentID, authSystemUserID, or email required for GET');
        }
        List<StudentResourceOutputV1> results = new List<StudentResourceOutputV1>();
        for(StudentService.Student stud : students){
            results.add(new StudentResourceOutputV1(stud));
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentResource.handleGetV2()');
        String studentIDParam = api.params.get('studentID');
        String studentAuthIDParam = api.params.get('authSystemUserID');
        String studentEmailParam = api.params.get('email');
        String schoolParam = api.params.get('schoolID');
        String primarySchoolStudentParam = api.params.get('primarySchoolStudentID');
        List<StudentService.Student> students = new List<StudentService.Student>();

        if(studentIDParam != null){
            students = StudentService.getStudentsWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam));
        } else if(studentAuthIDParam != null){
            students = StudentService.getStudentsWithAuthID(VemoApi.parseParameterIntoStringSet(studentAuthIDParam));
        } else if(studentEmailParam != null){
            students = StudentService.getStudentsWithEmail(VemoApi.parseParameterIntoStringSet(studentEmailParam));
        } else if(schoolParam != null){
            students = StudentService.getStudentsWithSchoolID(VemoApi.parseParameterIntoStringSet(schoolParam));
        }else if(primarySchoolStudentParam != null){
            if(schoolParam != null){
                Set<String> schoolIdSet = VemoApi.parseParameterIntoStringSet(schoolParam);
                for(StudentService.Student stud : StudentService.getStudentsWithPrimarySchoolStudentId(VemoApi.parseParameterIntoStringSet(primarySchoolStudentParam ))){
                    if(schoolIdSet != null && schoolIdSet.size()>0 && schoolIdSet.contains(stud.primarySchoolID))
                        students.add(stud);
                }
            }
            else
                students = StudentService.getStudentsWithPrimarySchoolStudentId(VemoApi.parseParameterIntoStringSet(primarySchoolStudentParam ));
        } else{
            throw new VemoAPI.VemoAPIFaultException('Missing parameter: studentID, authSystemUserID, email, or schoolID required for GET');
        }


        List<StudentResourceOutputV2> results = new List<StudentResourceOutputV2>();
        for(StudentService.Student stud : students){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoStudentResourceOutputV2(stud));                    

            } else {
                results.add(new PublicStudentResourceOutputV2(stud));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }   
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        system.debug('VemoAPIStudentResource.handlePostV1()');
        List<StudentService.Student> newStudents = new List<StudentService.Student>();
        List<StudentService.Student> existingStudents = new List<StudentService.Student>();
        List<StudentResourceInputV1> studentsJSON = (List<StudentResourceInputV1>)JSON.deserialize(api.body, List<StudentResourceInputV1>.class);
        
        Set<String> personEmails = new Set<String>();
        for(StudentResourceInputV1 studRes : studentsJSON){
            personEmails.add(studRes.email);     
        }
        Map<String, Account> studentMap = CheckDuplicateStudents.CheckDuplicateStudentsByEmail(personEmails); //Check the duplicate students by Email       
        for(StudentResourceInputV1 studRes : studentsJSON){
            studRes.validatePOSTFields();
            StudentService.Student stud = new StudentService.Student();
            stud = studentResourceV1ToStudent(studRes);
            stud.authSystemUserID = api.studentAuthID;
            //stud.personAccountID = null;
            //
            //THis code needs to be uncommented out when its time to deploy this.  This code was applied to qa branch.
            if(stud.email != null) {
                String email = (stud.email).toLowerCase();
                if(studentMap.containsKey(email)){
                    stud.personAccountID = studentMap.get(email).Id;
                    existingStudents.add(stud);
                }
                else{
                    stud.personAccountID = null;
                    newStudents.add(stud);
                }                
            } else{
            stud.personAccountID = null;
            newStudents.add(stud);
        }  

            
            //The following code is not case sensitive so still fails to find the student
            // if(studentMap.containsKey(stud.email)){
            //     stud.personAccountID = studentMap.get(stud.email).Id;
            //     existingStudents.add(stud);
            // }
            // else{
            //     stud.personAccountID = null;
            //     newStudents.add(stud);
            // }
        }
        Set<ID> studentIDs = new Set<ID>();
        if(existingStudents.size()>0){
            studentIDs = StudentService.updateStudents(existingStudents);    
        }
        else
            studentIDs = StudentService.createStudents(newStudents);
            
        return (new VemoAPI.ResultResponse(studentIDs, studentIDs.size()));
    }
    public static VemoAPI.ResultResponse handlePostV2(VemoAPI.APIInfo api){
        system.debug('VemoAPIStudentResource.handlePostV2()');
        return handlePostV1(api);
    }
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentResource.handlePutV1()');
        List<StudentService.Student> students = new List<StudentService.Student>();
        List<StudentResourceInputV1> studentsJSON = (List<StudentResourceInputV1>)JSON.deserialize(api.body, List<StudentResourceInputV1>.class);
        

        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        for(StudentResourceInputV1 studRes : studentsJSON){
            idsToVerify.add((ID) studRes.studentID);
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned

        for(StudentResourceInputV1 studRes : studentsJSON){
            if(verifiedAccountMap.containsKey(studRes.studentID)){ //only verified records should be updated
                studRes.validatePUTFields();
                StudentService.Student stud = new StudentService.Student();
                stud = studentResourceV1ToStudent(studRes);
                students.add(stud);                
            }
        }
        Set<ID> studentIDs = StudentService.updateStudents(students);

        return (new VemoAPI.ResultResponse(studentIDs, studentIDs.size()));
    }
    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentResource.handlePutV2()');
        return handlePutV1(api);

    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentResource.handleDeleteV1()');            
        String studentIDParam = api.params.get('studentID');        

        Set<ID> studentIDs = VemoApi.parseParameterIntoIDSet(studentIDParam);

        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(studentIDs); //authorized records should be returned
        

        Set<ID> studentIDsToDelete = new Set<ID>();
        Integer numToDelete = 0;
        for(ID theID : studentIDs){
            if(verifiedAccountMap.containsKey(theID)){
                studentIDsToDelete.add(theID);
            }
        }
        if(verifiedAccountMap.size()>0){
            numToDelete = StudentService.deleteStudents(studentIDsToDelete);            
        }

        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    public static VemoAPI.ResultResponse handleDeleteV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentResource.handleDeleteV2()');            
        return handleDeleteV1(api);
    }
    public static StudentService.Student studentResourceV1ToStudent(StudentResourceInputV1 studRes){
        StudentService.Student stud = new StudentService.Student();
        if(String.isNotBlank(studRes.studentID)) stud.personAccountID = studRes.studentID;
        stud.ssnTaxID = studRes.ssnTaxID;
        stud.firstName = studRes.firstName; 
        stud.middleName = studRes.middleName;
        if(String.isNotBlank(studRes.lastName)) stud.lastName = studRes.lastName;
        stud.email = studRes.email;
        stud.birthdate = studRes.birthdate;
        if(String.isNotBlank(studRes.authSystemUserID)) stud.authSystemUserID = studRes.authSystemUserID;
        stud.primarySchoolID = studRes.primarySchoolID;
        stud.primarySchoolStudentID = studRes.primarySchoolStudentID;
        stud.primarySchoolEmail = studRes.primarySchoolEmail;
        stud.driversLicenseOrStateID = studRes.driversLicenseOrStateID;
        stud.primarySchoolGraduationDate = studRes.primarySchoolGraduationDate;
        stud.primarySchoolEnrollmentStatus = studRes.primarySchoolEnrollmentStatus;
        stud.primarySchoolGradeLevel = studRes.primarySchoolGradeLevel;
        stud.residency = studRes.residency;
        stud.preferredMethodOfCommunication = studRes.preferredMethodOfCommunication;
        stud.preferredName = studRes.preferredName;
        stud.salutation = studRes.salutation;
        stud.suffix = studRes.suffix;
        stud.doNotCall = studRes.doNotCall;
        stud.doNotEmail = studRes.doNotEmail;
        stud.homePhone = studRes.homePhone;
        stud.mailingCity = studRes.city;
        stud.mailingCountry = studRes.country;
        stud.mailingPostalCode = studRes.postalCode;
        stud.mailingState = studRes.state;
        stud.mailingStreet = studRes.street;
        stud.otherCity = studRes.otherCity;
        stud.otherCountry = studRes.otherCountry;
        stud.otherPostalCode = studRes.otherPostalCode;
        stud.otherState = studRes.otherState;
        stud.otherStreet = studRes.otherStreet;
        stud.mobilePhone = studRes.mobilePhone;
        stud.doNotText = studRes.doNotText;
        stud.timeZone = studRes.timeZone;
        stud.schoolProgramOfStudyID = studRes.schoolProgramOfStudyID;
        stud.commonLineID = studRes.commonLineID;
        //stud.schoolSecurity = studRes.schoolSecurity;
        stud.portalPreferences = studRes.portalPreferences;
        //stud.autoPayment = studRes.autoPayment;
        stud.autoPaymentFrequency = studRes.autoPaymentFrequency;
        stud.autoPaymentDayOfMonth1 = studRes.autoPaymentDayOfMonth1;
        stud.autoPaymentDayOfMonth2 = studRes.autoPaymentDayOfMonth2;
        stud.alternateEmail = studRes.alternateEmail;
        //stud.autoPaymentDateActivated = studres.autoPaymentDateActivated;
        stud.plaidStatus = studRes.plaidStatus;
        stud.portalUsername = studRes.portalUsername;

        return stud;
    }   
    public class StudentResourceInputV1{
        public String studentID {get;set;}
        public String ssnTaxID {get;set;}
        public String firstName {get;set;}
        public String middleName {get;set;}
        public String lastName {get;set;}
        public String email {get;set;}
        public String authSystemUserID {get;set;}
        public Date birthdate {get;set;}
        public String primarySchoolID {get;set;}
        public String primarySchoolStudentID {get;set;}
        public String primarySchoolEmail {get;set;}
        public Date primarySchoolGraduationDate {get;set;}
        public String primarySchoolEnrollmentStatus {get;set;}
        public String primarySchoolGradeLevel {get;set;}
        public String driversLicenseOrStateID {get;set;}
        public String residency {get;set;}
        public String preferredMethodOfCommunication {get;set;}
        public String preferredName {get;set;}
        public String salutation {get;set;}
        public String suffix {get;set;}
        public Boolean doNotCall {get;set;}
        public Boolean doNotEmail {get;set;}
        public String homePhone {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String postalCode {get;set;}
        public String state {get;set;}
        public String street {get;set;}
        public String othercity {get;set;}
        public String otherCountry {get;set;}
        public String otherPostalCode {get;set;}
        public String otherState {get;set;}
        public String otherStreet {get;set;}
        public String mobilePhone {get;set;}
        public Boolean doNotText {get;set;}
        public String timeZone {get;set;}
        public String schoolProgramOfStudyID {get;set;}
        public String commonLineID {get;set;}
        //public String schoolSecurity {get;set;}
        public String portalPreferences {get;set;}
        //public String autoPayment {get;set;}
        public String autoPaymentFrequency {get;set;}
        public String autoPaymentDayOfMonth1 {get;set;}
        public String autoPaymentDayOfMonth2 {get;set;}
        //public Date autoPaymentDateActivated {get;set;}
        public String alternateEmail {get;set;}
        public String plaidStatus {get;set;}
        public String portalUsername {get;set;}

        public StudentResourceInputV1(Boolean testValues){
            if(testValues){
                this.firstName = 'Test First';
                this.middleName = 'Test Middle';
                this.lastName = 'Test Last';
                this.email = 'email@email.com';
                this.authSystemUserID = '123';
                this.birthdate = Date.today();
                this.primarySchoolID = null;//'123';
                this.primarySchoolStudentID  = '123';
                this.primarySchoolEmail  = 'email@email.com';
                this.primarySchoolGraduationDate  = Date.today();
                this.primarySchoolEnrollmentStatus  = 'Full Time';
                this.primarySchoolGradeLevel  = 'Freshman';
                this.driversLicenseOrStateID  = '123';
                this.residency  = 'US Citizen';
                this.preferredMethodOfCommunication  = 'Call';
                this.preferredName = 'test preferredName';
                this.salutation  = '123';
                this.suffix  = '123';
//              this.guid = '123';
                this.doNotCall = true;
                this.doNotEmail = true;
                this.homePhone  = '123';
                this.city  = '123';
                this.country  = 'United States';
                this.postalCode  = '123';
                this.state  = 'Colorado';
                this.street  = '123';
                this.otherCity  = '123';
                this.otherCountry  = 'United States';
                this.otherPostalCode  = '123';
                this.otherState  = 'Colorado';
                this.otherStreet  = '123';
                this.mobilePhone  = '123';
                this.doNotText = true;
                this.timeZone  = '(GMT-7) Mountain Standard Time (America/Denver)';
                this.studentID = null;
                this.commonLineID = '123';
            }
        }
        public void validatePOSTFields(){
            if(doNotCall == null) doNotCall = false;
            if(doNotEmail == null) doNotEmail = false;
            if(doNotText == null) doNotText = false;
            if(lastName == null) throw new VemoAPI.VemoAPIFaultException('lastName is a required input parameter on POST');
            if(authSystemUserID == null) throw new VemoAPI.VemoAPIFaultException('authSystemUserID is a required input parameter on POST');
            if(studentID != null) throw new VemoAPI.VemoAPIFaultException('studentID cannot be created in POST');                
        }
        public void validatePUTFields(){
            if(studentID == null) throw new VemoAPI.VemoAPIFaultException('studentID is a required input parameter on PUT');         
        }
    }
    public class StudentResourceOutputV1{
        public String studentID {get;set;}

        public String ssnTaxID {get;set;}
        public String firstName {get;set;}
        public String middleName {get;set;}
        public String lastName {get;set;}
        public String email {get;set;}
        public String authSystemUserID {get;set;}
        public Date birthdate {get;set;}
        public String primarySchoolID {get;set;}
        public String primarySchoolName {get;set;}
        public String primarySchoolStudentID {get;set;}
        public String primarySchoolEmail {get;set;}
        public Date primarySchoolGraduationDate {get;set;}
        public String primarySchoolEnrollmentStatus {get;set;}
        public String primarySchoolGradeLevel {get;set;}
        public String driversLicenseOrStateID {get;set;}
        public String residency {get;set;}
        public String preferredMethodOfCommunication {get;set;}
        public String preferredName {get;set;}
        public String salutation {get;set;}
        public String suffix {get;set;}
        public Boolean doNotCall {get;set;}
        public Boolean doNotEmail {get;set;}
        public String homePhone {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String postalCode {get;set;}
        public String state {get;set;}
        public String stateCode {get;set;}
        public String street {get;set;}
        public String otherCity {get;set;}
        public String otherCountry {get;set;}
        public String otherPostalCode {get;set;}
        public String otherState {get;set;}
        public String otherStateCode {get;set;}
        public String otherStreet {get;set;}
        public String mobilePhone {get;set;}
        public Boolean doNotText {get;set;}
        public String timeZone {get;set;}
        public String schoolProgramOfStudyID {get;set;}
        public String portalPreferences {get;set;}

        public String commonLineID {get;set;}
        public String accountNumber {get;set;}
        public Decimal credit {get;set;}
        public String autoPayment {get;set;}
        public Date autoPaymentDateActivated {get;set;}

        public String autoPaymentFrequency {get;set;}
        public String autoPaymentDayOfMonth1 {get;set;}
        public String autoPaymentDayOfMonth2 {get;set;}

        //public String schoolSecurity {get;set;}
        
        public Decimal cumulativeIncomeShare {get;set;}
        public Decimal cumulativeIncomeShareCap {get;set;} 
        public String alternateEmail {get;set;}
        
        public String plaidStatus {get;set;}
        public Boolean updatePlaidPassword {get;set;}
        public String studentCampusServiceEmail {get;set;}
        public String portalUsername {get;set;}
        

        public StudentResourceOutputV1(StudentService.Student stud){
            this.studentID = stud.personAccountID;
            if(String.isNotEmpty(stud.ssnTaxID)) this.ssnTaxID = '***-**-'+stud.ssnTaxID.right(4);
            this.firstName = stud.firstName;
            this.middleName = stud.middleName;
            this.lastName = stud.lastName;
            this.email = stud.email;
            this.birthdate = stud.birthdate;
            this.authSystemUserID = stud.authSystemUserID;
            this.primarySchoolID = stud.primarySchoolID;
            this.primarySchoolName = stud.primarySchoolName;
            this.primarySchoolStudentID = stud.primarySchoolStudentID;
            this.primarySchoolEmail = stud.primarySchoolEmail;
            this.driversLicenseOrStateID = stud.driversLicenseOrStateID;
            this.primarySchoolGraduationDate = stud.primarySchoolGraduationDate;
            this.primarySchoolEnrollmentStatus = stud.primarySchoolEnrollmentStatus;
            this.primarySchoolGradeLevel = stud.primarySchoolGradeLevel;
            this.residency = stud.residency;
            this.preferredMethodOfCommunication = stud.preferredMethodOfCommunication;
            this.preferredName = stud.preferredName;
            this.salutation = stud.salutation;
            this.suffix = stud.suffix;
            this.doNotCall = stud.doNotCall;
            this.doNotEmail = stud.doNotEmail;
            this.homePhone = stud.homePhone;
            this.city = stud.mailingCity;
            this.country = stud.mailingCountry;
            this.postalCode = stud.mailingPostalCode;
            this.state = stud.mailingState;
            this.stateCode = stud.mailingStateCode;
            this.street = stud.mailingStreet;
            this.otherCity = stud.otherCity;
            this.otherCountry = stud.otherCountry;
            this.otherPostalCode = stud.otherPostalCode;
            this.otherState = stud.otherState;
            this.otherStateCode = stud.otherStateCode;
            this.otherStreet = stud.otherStreet;
            this.mobilePhone = stud.mobilePhone;
            this.doNotText = stud.doNotText;
            this.timeZone = stud.timeZone;
            this.schoolProgramOfStudyID = stud.schoolProgramOfStudyID;
            this.portalPreferences = stud.portalPreferences;
            this.commonLineID = stud.commonLineID;
            this.accountNumber = stud.accountNumber;
            this.credit = stud.credit;
            this.autoPayment = stud.autoPayment;
            this.autoPaymentDateActivated = stud.autoPaymentDateActivated;
            this.autoPaymentFrequency = stud.autoPaymentFrequency;
            this.autoPaymentDayOfMonth1 = stud.autoPaymentDayOfMonth1;
            this.autoPaymentDayOfMonth2 = stud.autoPaymentDayOfMonth2;
            //this.schoolSecurity = stud.schoolSecurity;
            this.cumulativeIncomeShare = stud.cumulativeIncomeShare;
            this.cumulativeIncomeShareCap = stud.cumulativeIncomeShareCap;
            this.alternateEmail = stud.alternateEmail;
            this.plaidStatus = stud.plaidStatus;
            this.updatePlaidPassword = stud.updatePlaidPassword;
            this.studentCampusServiceEmail = stud.studentCampusServiceEmail; 
            this.portalUsername = stud.PortalUsername;

        }
    }
    public virtual class StudentResourceOutputV2{
        public String studentID {get;set;}
        public String firstName {get;set;}
        public String middleName {get;set;}
        public String lastName {get;set;}
        public Date birthdate {get;set;}
        public String email {get;set;}
        public String mobilePhone {get;set;}
        public String city {get;set;}
        public String country {get;set;}
        public String postalCode {get;set;}
        public String state {get;set;}
        public String street {get;set;}
        public String vemoAccountNumber {get;set;} 
        public Decimal amountCertifiedToDate {get;set;} 
        public Decimal amountDisbursedToDate {get;set;} 
        public String alternateEmail {get;set;}  
        public String primarySchoolStudentID {get;set;}  
        
        public StudentResourceOutputV2(){

        }
        public StudentResourceOutputV2(StudentService.Student stud){
            this.studentID = stud.personAccountID;
            this.firstName = stud.firstName;
            this.middleName = stud.middleName;
            this.lastName = stud.lastName;
            this.birthdate = stud.birthdate;
            this.email = stud.email;
            this.mobilePhone = stud.mobilePhone;
            this.city = stud.mailingCity;
            this.country = stud.mailingCountry;
            this.postalCode = stud.mailingPostalCode;
            this.state = stud.mailingState;
            this.street = stud.mailingStreet;
            this.vemoAccountNumber = stud.vemoAccountNumber;
            this.amountCertifiedToDate = stud.amountCertifiedToDate;
            this.amountDisbursedToDate= Stud.amountDisbursedToDate;
            this.alternateEmail = stud.alternateEmail;
            this.primarySchoolStudentID = stud.primarySchoolStudentID;
            
        }    

    } 
    public class PublicStudentResourceOutputV2 extends StudentResourceOutputV2{
        public PublicStudentResourceOutputV2(){

        }
        public PublicStudentResourceOutputV2(StudentService.Student stud){
            super(stud);
        }
    }

    public class VemoStudentResourceOutputV2 extends StudentResourceOutputV2{
        public String ssnTaxID {get;set;}
        public VemoStudentResourceOutputV2(){

        }        
        public VemoStudentResourceOutputV2(StudentService.Student stud){
            super(stud);
            if(String.isNotEmpty(stud.ssnTaxID)) this.ssnTaxID = '***-**-'+stud.ssnTaxID.right(4);
        }
    }       
}