global class EventInstanceBatch implements Database.Batchable<sObject> {
    public enum JobType {SEND_EVENTINSTANCE_EMAIL}
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    public EventInstanceBatch(){
    }
    
    global EventInstanceBatch (string query) {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(job == JobType.SEND_EVENTINSTANCE_EMAIL){
        Date today = Date.today();
            if(String.isEmpty(this.query)){
                query = 'SELECT id,Contact__c,Contact__r.email, Email__c,EventAttributes__c , Event__r.EventType__r.name, Event__r.EventType__r.MergeTemplate__c,Event__r.WhatId__c from EventInstance__c where Email__c = true AND OutboundCreated__c = false';
            }
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(job == JobType.SEND_EVENTINSTANCE_EMAIL){
            handleEventInstance((List<EventInstance__c>)scope);
        }
    }
    
    private static void handleEventInstance(List<EventInstance__c> eInstanceList){
    
        List<OutboundEmail__c> emailsToSend = new List<OutboundEmail__c>();
        
        for(EventInstance__c ei: eInstanceList){
            
            if(ei.event__c !=null && ei.Event__r.EventType__c != null && 
                                        ei.Event__r.EventType__r.MergeTemplate__c != null &&
                                        ei.Event__r.WhatId__c != null && ei.contact__c != null && ei.contact__r.email != null){
                
                OutboundEmail__c oe = new OutboundEmail__c();
                oe.SendviaSES__c = true;
                oe.TemplateID__c = ei.Event__r.EventType__r.MergeTemplate__c;
                oe.WhatID__c = ei.id;
                oe.TargetObjectId__c = ei.contact__c;
                oe.ToAddresses__c = ei.contact__r.email;
                emailsToSend.add(oe);
                
                ei.OutboundCreated__c = true;
            }
            else if(ei.EventAttributes__c != null && ei.EventAttributes__c != '' && ei.Event__c != null 
                && ei.Event__r.WhatId__c != null && ei.Event__r.EventType__c != null
                 && ei.contact__c != null && ei.contact__r.email != null){
                
                OutboundEmail__c oe = new OutboundEmail__c();
                oe.SendviaSES__c = true;
                oe.PlainTextBody__c = ei.EventAttributes__c ;
                oe.subject__c = ei.Event__r.EventType__r.name;
                oe.WhatID__c = ei.Id;
                oe.TargetObjectId__c = ei.contact__c;
                oe.ToAddresses__c = ei.contact__r.email;
                emailsToSend.add(oe);
                
                ei.OutboundCreated__c = true;
            }
        }
        
        if(emailsToSend.size()>0)
            insert emailsToSend;
        if(eInstanceList != null && eInstanceList.size()>0)
            update eInstanceList;
    }
  
    global void finish(Database.BatchableContext BC) {     
    }
}