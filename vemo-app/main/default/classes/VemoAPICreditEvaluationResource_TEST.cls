@isTest
public class VemoAPICreditEvaluationResource_TEST {
  @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, 
                                                                                    TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        Map<ID, CreditCheck__c> ccMap = TestDataFactory.createAndInsertCreditCheck(1, studentMap);
    }
    
    
     static testMethod void testHandleGetV1(){
        setupData(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        
           
        Map<String, String> cceParams = new Map<String, String>();
        cceParams.put('creditCheckEvaluationID', (String)cceMap.values().get(0).Id);
        cceParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo cceApiInfo = TestUtil.initializeAPI('v1', 'GET', cceParams, null);
        
        Map<String, String> ccParams = new Map<String, String>();
        ccParams.put('creditCheckID', (String)ccMap.values().get(0).Id);
        ccParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo ccApiInfo = TestUtil.initializeAPI('v1', 'GET', ccParams, null);
        
        Map<String, String> agreementParams = new Map<String, String>();
        agreementParams.put('agreementID', (String)agreementMap.values().get(0).Id);
        agreementParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo agreementApiInfo = TestUtil.initializeAPI('v1', 'GET', agreementParams, null);
        
        Test.startTest();
        MockedQueryExecutor.setRecordLimit(1);
        MockedQueryExecutor.setResetRecordLimit(false);        
        VemoAPI.ResultResponse cceResult = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(cceApiInfo);
        System.assertEquals(1, cceResult.numberOfResults);
        MockedQueryExecutor.setRecordLimit(null);        
        VemoAPI.ResultResponse ccResult = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(ccApiInfo);
        //System.assertEquals(1, ccResult.numberOfResults);
        
        VemoAPI.ResultResponse agreementResult = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(agreementApiInfo);
        
        Test.stopTest();    
    }
    
    static testMethod void testHandlePostV1(){
        setupData(); 
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckID(ccMap.keyset());
        
        List<VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1> ccResMap = new List<VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1>();
        for(Integer i = 0; i<ccMap.size(); i++){
            VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1 ccRes = new VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1();
            ccRes.creditCheckID = ccMap.values().get(i).ID;
            ccRes.AgreementID = agreementMap.values().get(0).ID; 
            ccResMap.add(ccRes);
        }
        
        String body = JSON.serialize(ccResMap);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }
    
     static testMethod void testHandlePutV1(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap);
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        
        List<VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1> ccResMap = new List<VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1>();
        for(CreditCheckEvaluation__c cc : cceMap.values()){
        VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1 ccRes = new VemoAPICreditCheckEvaluationResource.CreditCheckEvaluationResourceInputV1();
        ccRes.creditCheckEvaluationID = cc.ID;
        ccRes.status = 'Approved';
        ccResMap.add(ccRes);
        }
        String body = JSON.serialize(ccResMap);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(apiInfo);
        System.assertEquals(ccResMap.size(), result.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandleDeleteV1(){
        setupData(); 
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap);
        //System.assertEquals(CreditCheckQueries.getCreditCheckMap().size(), TestUtil.TEST_THROTTLE, 'Did not create credit checks');
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');
        params.put('creditCheckEvaluationID', (String)cceMap.values().get(0).Id);
        
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);
        
        Test.startTest();
        MockedQueryExecutor.setRecordLimit(1);
        MockedQueryExecutor.setResetRecordLimit(false);                
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPICreditCheckEvaluationResource.handleAPI(apiInfo);
        System.assertEquals(1, result.numberOfResults);
        Test.stopTest();
    }
}