@isTest
public with sharing class ProgramTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @testSetup
    static void CreateSetupRecords(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'AllSchoolsAwaitingDisbursementConfirmation'});
    }

    @isTest
    static void TestMethod1 (){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(3);
        Map<Id, Program__c> prgMap = TestDataFactory.createAndInsertPrograms(3, schools);
        for(Program__c program : prgMap.values()){
            program.DisbursementConfRequiredNotification__c = true;
            program.AutomaticallyConfirmTransactions__c = false;
            program.SchoolEmailNotification2__c = 'Test.email@test.com';
        }
        //update prgMap.values();
        dbUtil.updateRecords(prgMap.values());
        dbUtil.deleteRecords(prgMap.values());
        dbUtil.undeleteRecords(prgMap.values());
    }
}