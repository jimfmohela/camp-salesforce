@isTest
public class ReportStudentEmpHistControllerTest{
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    
    
    @isTest public static void validateGetSchool(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        /*Map<ID, PaymentMethod__c> paymentMethods = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, PaymentInstruction__c> PIs = TestDataFactory.createAndInsertPaymentInstruction(1, students, paymentMethods);
        Map<ID, PaymentAllocation__c> allocations = TestDataFactory.createAndInsertPaymentAllocation(3,PIs,agreements);*/
            
        Map<ID, EmploymentHistory__c> employmentHistory = TestDataFactory.createAndInsertEmploymentHistory(20,students);
        Map<ID, IncomeVerification__c> incomes = TestDataFactory.createAndInsertIncomeVerification(1, employmentHistory);
        
        Account acct = new Account(Name = 'Flowpoint Deal 1',
                RecordTypeID = (String) GlobalUtil.getRecordTypeIDByLabelName('Account', 'Investor'));
        //Database.insert(acct,true);
        dbUtil.insertRecord(acct);
        for(StudentProgram__c sp:agreements.values()){
            sp.status__c = 'Draft';  
            sp.PrimaryOwner__c = acct.Id;  
            //sp.BypassAutomation__c = true;
        }        
        //update agreements.values();
        dbUtil.updateRecords(agreements.values());
              
        Test.StartTest(); 
            
            ReportStudentEmpHistController cntrl = new ReportStudentEmpHistController();
            //List<SelectOption> schools = cntrl.getSchools();
            //cntrl.selectedSchool = schools[0].getValue();
            cntrl.runReport();
            //System.assertEquals(4,cntrl.reportData.size(),'Number of rows in report should be 4');
        
        Test.StopTest(); 
        } 
    }
    
    @isTest public static void testRunAndExport(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        /*Map<ID, PaymentMethod__c> paymentMethods = TestDataFactory.createAndInsertPaymentMethod(1, students);
        Map<ID, PaymentInstruction__c> PIs = TestDataFactory.createAndInsertPaymentInstruction(1, students, paymentMethods);
        Map<ID, PaymentAllocation__c> allocations = TestDataFactory.createAndInsertPaymentAllocation(3,PIs,agreements);*/
            
        Map<ID, EmploymentHistory__c> employmentHistory = TestDataFactory.createAndInsertEmploymentHistory(20,students);
        Map<ID, IncomeVerification__c> incomes = TestDataFactory.createAndInsertIncomeVerification(1, employmentHistory);
        
        Account acct = new Account(Name = 'Flowpoint Deal 1',
                RecordTypeID = (String) GlobalUtil.getRecordTypeIDByLabelName('Account', 'Investor'));
        //Database.insert(acct,true);
        dbUtil.insertRecord(acct);
        for(StudentProgram__c sp:agreements.values()){
            sp.status__c = 'Draft';  
            sp.PrimaryOwner__c = acct.Id;  
            //sp.BypassAutomation__c = true;
        }        
        //update agreements.values();
        dbUtil.updateRecords(agreements.values());
        Test.StartTest(); 
            //setupData();
            ReportStudentEmpHistController  cntrl = new ReportStudentEmpHistController();
            //List<SelectOption> schools = cntrl.getSchools();
            //cntrl.selectedSchool = schools[0].getValue();
            cntrl.exportToCSV();
            cntrl.buildCsvString(); 
            
        Test.StopTest(); 
        }  
    }      
                    
}