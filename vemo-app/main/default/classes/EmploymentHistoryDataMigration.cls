public class EmploymentHistoryDataMigration{

    public static void migrateEploymentHistoryData(Set<ID> studentIds){
                Date referencedDate = Date.today();
                Date lastDayOfPriorMonth = Date.newInstance(referencedDate.year(), referencedDate.month()-1, date.daysInMonth(referencedDate.year(), referencedDate.month()-1));
                //System.debug('lastDayOfPriorMonth -->'+lastDayOfPriorMonth);
               
                List<IncomeVerification__c> incomeVeriftnList = [SELECT Id, Status__c, DataConversionStatus__c, EmploymentHistory__c, EmploymentHistory__r.Employer__c, EmploymentHistory__r.JobTitle__c, EmploymentHistory__r.DateReported__c, 
                                                                 EmploymentHistory__r.DateVerified__c, EmploymentHistory__r.Verified__c, EmploymentHistory__r.EmploymentStartDate__c, EmploymentHistory__r.PaymentSchedule__c, 
                                                                 EmploymentHistory__r.EmploymentEndDate__c, EmploymentHistory__r.Category__c, EmploymentHistory__r.Type__c, DateReported__c, DateVerified__c, Verified__c, BeginDate__c, 
                                                                 EndDate__c, Type__c, Student__c, IncomePerMonth__c, IncomePerYear__c, AnnualIncome__c  
                                                                 From IncomeVerification__c where Student__C IN: studentIds AND DataConversionStatus__c = 'Original' Order By BeginDate__c desc]; 
                    //query += 'where Student__c IN '+ DatabaseUtil.inSetStringBuilder(studentIds);
                    //query += 'where DataConversionStatus__c = \'Original\' ';
                    //orderBy = 'Order By BeginDate__c descivIdSet];
                Map<Id, EmploymentHistory2__c> IncmVeriftnIdVsEmpHistMap = new Map<Id, EmploymentHistory2__c>();
                List<Employer__c> listOfEmployersToInsert = new List<Employer__c>();
                //Set<Id> empHistIdsToBeUpdated = new Set<Id>();
                Set<String> incmVerIDStringSet = new Set<String>();
                List<IncomeVerification__c> incomeVerificationListToBeUpdated = new List<IncomeVerification__c>();
                
                Map<ID, List<IncomeVerification__c>> IncomeVerificationMapByEmploymentHistoryIDMap = new Map<ID, List<IncomeVerification__c>>();
                Map<ID, List<IncomeVerification__c>> ReportedIVsByStudentIdMap = new Map<ID, List<IncomeVerification__c>>();
                Map<ID, List<IncomeVerification__c>> EstimatedIVsByStudentIdMap = new Map<ID, List<IncomeVerification__c>>();
                Map<ID, IncomeVerification__c> IncompleteIVsByIDMap = new Map<ID, IncomeVerification__c>();
                for(IncomeVerification__c incmVerftn : incomeVeriftnList){
                    if(incmVerftn.Status__c == 'Incomplete'){
                        IncompleteIVsByIDMap.put(incmVerftn.ID, incmVerftn); 
                    }
                    if(incmVerftn.EmploymentHistory__c != null){
                        if(!IncomeVerificationMapByEmploymentHistoryIDMap.containsKey(incmVerftn.EmploymentHistory__c)){
                            IncomeVerificationMapByEmploymentHistoryIDMap.put(incmVerftn.EmploymentHistory__c, new List<IncomeVerification__c>());    
                        } 
                        IncomeVerificationMapByEmploymentHistoryIDMap.get(incmVerftn.EmploymentHistory__c).add(incmVerftn);
                    }
                    else if(incmVerftn.Type__c == 'Reported'){
                        if(!ReportedIVsByStudentIdMap.containsKey(incmVerftn.Student__c)){
                            ReportedIVsByStudentIdMap.put(incmVerftn.Student__c, new List<IncomeVerification__c>());    
                        } 
                        ReportedIVsByStudentIdMap.get(incmVerftn.Student__c).add(incmVerftn);
                    }
                    else if(incmVerftn.Type__c == 'Estimated'){
                        if(!EstimatedIVsByStudentIdMap.containsKey(incmVerftn.Student__c)){
                            EstimatedIVsByStudentIdMap.put(incmVerftn.Student__c, new List<IncomeVerification__c>());    
                        } 
                        EstimatedIVsByStudentIdMap.get(incmVerftn.Student__c).add(incmVerftn);
                    }
                   
                }
                
                system.debug('IncomeVerificationMapByEmploymentHistoryIDMap--'+ IncomeVerificationMapByEmploymentHistoryIDMap);
                system.debug('ReportedIVsByStudentIdMap--'+ ReportedIVsByStudentIdMap);
                system.debug('EstimatedIVsByStudentIdMap--'+EstimatedIVsByStudentIdMap);
                
                Map<ID, List<GenericDocument__c>> GenericDocumentIDByParentIDMap = new Map<ID, List<GenericDocument__c>>();
                for(GenericDocument__c doc: [Select Id, ParentID__c from GenericDocument__c where ParentID__c IN: IncompleteIVsByIDMap.keySet()]){
                    if(!GenericDocumentIDByParentIDMap.containsKey(doc.parentID__c)){
                        GenericDocumentIDByParentIDMap.put(doc.ParentID__c, new List<GenericDocument__c>());     
                    }
                    GenericDocumentIDByParentIDMap.get(doc.parentID__c).add(doc);
                }
                
                Map<ID, EmploymentHistory__c> EmploymentHistoryByIDMap = new Map<ID,EmploymentHistory__c>([Select id, Student__c,MonthlyIncomeReported__c, Category__c,Type__c,PaymentSchedule__c,DateReported__c,DateVerified__c,EmploymentStartDate__c,EmploymentEndDate__c, Employer__c, JobTitle__c from EmploymentHistory__c where ID IN: IncomeVerificationMapByEmploymentHistoryIDMap.keySet()]);
                for(ID employmentHistoryID: IncomeVerificationMapByEmploymentHistoryIDMap.keySet()){
                    String guid = GlobalUtil.createGUID();
                    Boolean first = true;
                    
                    Employer__c employer = new Employer__c();
                    employer.GUID__c = guid;
                    
                    for(IncomeVerification__c inc: IncomeVerificationMapByEmploymentHistoryIDMap.get(employmentHistoryID)){
                        system.debug('order--- ' + inc.BeginDate__c);
                        Boolean keepGoing = true;
                        if(inc.Status__c == 'Incomplete'){
                            if(!GenericDocumentIDByParentIDMap.containsKey(inc.ID)){
                                keepGoing = false;    
                            }
                            //else
                            //    keepGoing = false;
                        }
                        inc.DataConversionStatus__c = 'Converted';
                        incomeVerificationListToBeUpdated.add(inc);
                        if(keepGoing){
                            incmVerIDStringSet.add(inc.id); 
                            //inc.DataConversionStatus__c = 'Converted';
                            //if(inc.EndDate__c == null){  
                            //    inc.EndDate__c = lastDayOfPriorMonth;
                            //}
                            //incomeVerificationListToBeUpdated.add(inc);
                            
                            EmploymentHistory2__c empHist = new EmploymentHistory2__c ();
                            empHist.DataConversionStatus__c = 'New'; 
                            empHist.EmployerMD__r = employer;                
                            //empHist.Category__c = 'Employee';
                            //empHist.PaymentSchedule__c = 'Monthly';  
                            empHist.Category__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Category__c;
                            //empHist.PaymentSchedule__c = EmploymentHistoryByIDMap.get(employmentHistoryID).PaymentSchedule__c;
                            empHist.DateReported__c = inc.DateReported__c;//EmploymentHistoryByIDMap.get(employmentHistoryID).DateReported__c;
                            empHist.DateVerified__c = inc.DateVerified__c;//EmploymentHistoryByIDMap.get(employmentHistoryID).DateVerified__c;
                            empHist.EmploymentStartDate__c = inc.BeginDate__c;//EmploymentHistoryByIDMap.get(employmentHistoryID).EmploymentStartDate__c;
                            //if(EmploymentHistoryByIDMap.get(employmentHistoryID).EmploymentEndDate__c != null)
                            //    empHist.EmploymentEndDate__c = EmploymentHistoryByIDMap.get(employmentHistoryID).EmploymentEndDate__c;
                            //else 
                            empHist.EmploymentEndDate__c = inc.EndDate__c;
                             
                            empHist.MonthlyIncomeType__c = inc.Type__c;    
                            empHist.Type__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Type__c;
                            if(inc.Status__c == 'Incomplete')
                                empHist.Status__c = 'Incomplete Info';
                            else if(inc.Status__c == 'Verified')
                                empHist.Status__c = 'Verified';
                            else if(inc.Status__c == 'Pending Verification')
                                empHist.Status__c = 'Under Review';  
                            else if(inc.Status__c == 'Rejected')
                                empHist.Status__c = 'Rejected';      
                             
                            empHist.EventType__c = 'New Employer with monthly salary'; //'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION';
                            empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Salary Based Income');
                            empHist.Student__c = inc.Student__c;
                            empHist.MonthlyIncomeReported__c = inc.IncomePerMonth__c;
                            empHist.IncomeVerificationId__c = inc.Id;
                            
                            if(first && inc.EndDate__c != null){
                                empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Employment Update');
                                empHist.EventType__c = 'Not Employed Here Anymore'; //'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION';    
                                //first = false;
                            }
                            first = false;
                                                   
                            IncmVeriftnIdVsEmpHistMap.put(inc.Id, empHist);
                        }     
                    }   
                    Employer__c newEmployer = new Employer__c();
                    newEmployer.Name = EmploymentHistoryByIDMap.get(employmentHistoryID).employer__c;
                    newEmployer.Student__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Student__c;//studentId; //inc.Student__c;
                    newEmployer.MonthlyIncome__c = EmploymentHistoryByIDMap.get(employmentHistoryID).MonthlyIncomeReported__c;//monthlyIncome; ////inc.IncomePerMonth__c;
                    newEmployer.JobTitle__c = EmploymentHistoryByIDMap.get(employmentHistoryID).JobTitle__c;
                    newEmployer.GUID__c = guid;  
                        
                    listOfEmployersToInsert.add(newEmployer); 
                }
                for(ID studentID1 : ReportedIVsByStudentIdMap.keySet()){
                    String guid = GlobalUtil.createGUID();
                    Boolean first = true;
                    Decimal monthlyIncome = ReportedIVsByStudentIdMap.get(studentID1)[0].IncomePerMonth__c;
                    Boolean mostRecentVerifiedIV = false;
                    Employer__c employer = new Employer__c();
                    employer.GUID__c = guid;
                    
                    for(IncomeVerification__c inc: ReportedIVsByStudentIdMap.get(studentID1)){
                        Boolean keepGoing = true;
                        if(inc.Status__c == 'Incomplete'){
                            if(!GenericDocumentIDByParentIDMap.containsKey(inc.ID)){
                                keepGoing = false;    
                            }
                            //else
                            //    keepGoing = false;
                        }
                        inc.DataConversionStatus__c = 'Converted';
                        incomeVerificationListToBeUpdated.add(inc);
                        if(keepGoing){
                            incmVerIDStringSet.add(inc.id); 
                            //inc.DataConversionStatus__c = 'Converted';
                            //if(inc.EndDate__c == null){  
                            //    inc.EndDate__c = lastDayOfPriorMonth;
                            //} 
                            //incomeVerificationListToBeUpdated.add(inc);
                            
                            EmploymentHistory2__c empHist = new EmploymentHistory2__c ();
                            empHist.DataConversionStatus__c = 'New'; 
                            empHist.EmployerMD__r = employer;                
                            //empHist.Category__c = 'Employee';
                            //empHist.PaymentSchedule__c = 'Monthly';  
                            //empHist.Category__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Category__c;
                            //empHist.PaymentSchedule__c = EmploymentHistoryByIDMap.get(employmentHistoryID).PaymentSchedule__c;
                            empHist.DateReported__c = inc.DateReported__c;
                            empHist.DateVerified__c = inc.DateVerified__c;
                            empHist.EmploymentStartDate__c = inc.BeginDate__c;
                            empHist.EmploymentEndDate__c = inc.EndDate__c;
                            empHist.MonthlyIncomeType__c = inc.Type__c;
                            //empHist.Type__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Type__c;
                            //empHist.Status__c = inc.Status__c;
                            if(inc.Status__c == 'Incomplete')
                                empHist.Status__c = 'Incomplete Info';
                            else if(inc.Status__c == 'Verified'){
                                empHist.Status__c = 'Verified';
                                if(mostRecentVerifiedIV == false){
                                    mostRecentVerifiedIV = true; 
                                    monthlyIncome = inc.IncomePerMonth__c;
                                }
                            }    
                            else if(inc.Status__C == 'Pending Verification')
                                empHist.Status__c = 'Under Review';  
                            else if(inc.Status__C == 'Rejected')
                                empHist.Status__c = 'Rejected';          
                                
                            empHist.EventType__c = 'New Employer with monthly salary'; //'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION';
                            empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Salary Based Income');
                            empHist.Student__c = inc.Student__c;
                            empHist.MonthlyIncomeReported__c = inc.IncomePerMonth__c;
                            empHist.IncomeVerificationId__c = inc.Id;
                            
                            if(first && inc.EndDate__c != null){
                                empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Employment Update');
                                empHist.EventType__c = 'Not Employed Here Anymore';
                                //first = false;
                            }
                            first = false;                        
                            IncmVeriftnIdVsEmpHistMap.put(inc.Id, empHist);
                        }
                    }  
                    
                    Employer__c newEmployer = new Employer__c();
                    newEmployer.Name = 'Employer Not Identified - Reported Income';
                    newEmployer.Student__c = studentId1;
                    newEmployer.MonthlyIncome__c = monthlyIncome; 
                    //newEmployer.JobTitle__c = 
                    newEmployer.GUID__c = guid;  
                        
                    listOfEmployersToInsert.add(newEmployer);  
                }
                for(ID studentID2 : EstimatedIVsByStudentIdMap.keySet()){
                    String guid = GlobalUtil.createGUID();
                    Boolean first = true;
                    
                    Decimal monthlyIncome = EstimatedIVsByStudentIdMap.get(studentID2)[0].IncomePerMonth__c;
                    Boolean mostRecentVerifiedIV = false;
                    
                    Employer__c employer = new Employer__c();
                    employer.GUID__c = guid;
                    
                    for(IncomeVerification__c inc: EstimatedIVsByStudentIdMap.get(studentID2)){
                        Boolean keepGoing = true;
                        if(inc.Status__c == 'Incomplete'){
                            if(!GenericDocumentIDByParentIDMap.containsKey(inc.ID)){
                                keepGoing = false;    
                            }
                            //else
                            //    keepGoing = false;
                        }
                        inc.DataConversionStatus__c = 'Converted';
                        incomeVerificationListToBeUpdated.add(inc);
                        if(keepGoing){
                            incmVerIDStringSet.add(inc.id); 
                            //inc.DataConversionStatus__c = 'Converted';
                            //if(inc.EndDate__c == null){  
                            //    inc.EndDate__c = lastDayOfPriorMonth;
                            //} 
                            //incomeVerificationListToBeUpdated.add(inc);
                            
                            EmploymentHistory2__c empHist = new EmploymentHistory2__c ();
                            empHist.DataConversionStatus__c = 'New'; 
                            empHist.EmployerMD__r = employer;                
                            //empHist.Category__c = 'Employee';
                            //empHist.PaymentSchedule__c = 'Monthly';  
                            //empHist.Category__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Category__c;
                            //empHist.PaymentSchedule__c = EmploymentHistoryByIDMap.get(employmentHistoryID).PaymentSchedule__c;
                            empHist.DateReported__c = inc.DateReported__c;
                            empHist.DateVerified__c = inc.DateVerified__c;
                            empHist.EmploymentStartDate__c = inc.BeginDate__c;
                            empHist.EmploymentEndDate__c = inc.EndDate__c;
                            empHist.MonthlyIncomeType__c = inc.Type__c;
                            //empHist.Type__c = EmploymentHistoryByIDMap.get(employmentHistoryID).Type__c;
                            //empHist.Status__c = inc.Status__c;
                            if(inc.Status__c == 'Incomplete')
                                empHist.Status__c = 'Incomplete Info';
                            else if(inc.Status__c == 'Verified'){
                                empHist.Status__c = 'Verified';
                                if(mostRecentVerifiedIV == false){
                                    mostRecentVerifiedIV = true; 
                                    monthlyIncome = inc.IncomePerMonth__c;
                                }
                            }    
                            else if(inc.Status__c == 'Pending Verification')
                                empHist.Status__c = 'Under Review'; 
                            else if(inc.Status__c == 'Rejected')
                                empHist.Status__c = 'Rejected';        
                                
                            empHist.EventType__c = 'New Employer with monthly salary'; //'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION';
                            empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Salary Based Income');
                            empHist.Student__c = inc.Student__c;
                            empHist.MonthlyIncomeReported__c = inc.IncomePerMonth__c;
                            empHist.IncomeVerificationId__c = inc.Id;
                            
                            if(first && inc.EndDate__c != null){
                                empHist.RecordTypeID = GlobalUtil.getRecordTypeIDByLabelName('EmploymentHistory2__c', 'Employment Update');
                                empHist.EventType__c = 'Not Employed Here Anymore';
                                //first = false;
                            }
                            first = false;                        
                            IncmVeriftnIdVsEmpHistMap.put(inc.Id, empHist);
                        }
                    }  
                    
                    Employer__c newEmployer = new Employer__c();
                    newEmployer.Name = 'Employer Not Identified - Estimated Income';
                    newEmployer.Student__c = studentId2;
                    newEmployer.MonthlyIncome__c = monthlyIncome;
                    //newEmployer.JobTitle__c = 
                    newEmployer.GUID__c = guid;  
                        
                    listOfEmployersToInsert.add(newEmployer);  
                }
                    
                
                if(listOfEmployersToInsert.size()>0)
                    insert listOfEmployersToInsert; // insert employers before inserting employmenthistory2
                
                if(IncmVeriftnIdVsEmpHistMap.size() > 0){
                    //Get existing employment history which has been created earlier by data conversion Batch
                    //empHistListToDelete = [Select Id,IncomeVerificationId__c From EmploymentHistory__c Where IncomeVerificationId__c In : incmVerIDStringSet];
                    insert IncmVeriftnIdVsEmpHistMap.values();
                    
                    Map<Id, GenericDocument__c> genericDocMap = new Map<Id, GenericDocument__c>();
                    genericDocMap = GenericDocumentQueries.getGenericDocumentMapByParentID(incmVerIDStringSet);
                    System.debug('genericDocMap size -->'+genericDocMap.size());
                    if(genericDocMap != null && genericDocMap.size() > 0){
                        List<GenericDocument__c> genDocListToInsert = new List<GenericDocument__c>();
                        for(GenericDocument__c genDoc : genericDocMap.values()){
                            if(genDoc.ParentID__c != null && IncmVeriftnIdVsEmpHistMap.containsKey(genDoc.ParentID__c)){
                                //GenericDocument__c newGenDoc = new GenericDocument__c ();
                                //newGenDoc = genDoc.clone();
                                //newGenDoc.ParentID__c = IncmVeriftnIdVsEmpHistMap.get(genDoc.ParentID__c).Id;
                                //genDocListToInsert.add(newGenDoc);
                                //Id parentid = (ID)genDoc.parentID__c;
                                GenericDocument__c doc = new GenericDocument__c(id = genDoc.id,
                                                                                ParentObjectType__c = 'EmploymentHistory__c',
                                                                                ParentId__c = IncmVeriftnIdVsEmpHistMap.get((ID)genDoc.ParentID__c).Id); 
                                genDocListToInsert.add(doc);                                                 
                            }
                        }
                        if(genDocListToInsert.size() > 0){
                            update genDocListToInsert; 
                        }
                    }
                    
                    // Migrating Attachments
                    Map<Id, Attachment> AttachmentsById= new Map<ID, Attachment>([Select id, Name, OwnerId, Body, Description, ContentType, ParentId, IsPrivate from Attachment where ParentID IN: incmVerIDStringSet]);
                    List<Attachment> attachmentsToInsert = new List<Attachment>();
                    if(AttachmentsById.size()>0){
                        for(Attachment att: AttachmentsById.values()){
                            Attachment newAtt = new Attachment();
                            newAtt .Name = att.Name;
                            newAtt .OwnerId = att.OwnerId;
                            newAtt .body = att.body;
                            newAtt .Description = att.Description;
                            newAtt .ContentType = att.ContentType;
                            newAtt.ParentId = IncmVeriftnIdVsEmpHistMap.get(att.ParentID).Id;
                            newAtt.IsPrivate = att.IsPrivate;
                            attachmentsToInsert.add(newAtt); 
                        }
                        if(attachmentsToInsert.size()>0){
                         insert attachmentsToInsert;                    
                        }
                        
                    }
                    
                    //Migrating Notes
                    List<Note> notes = [Select id, ParentId, Title, OwnerID, ISPrivate, Body from Note where ParentID IN : incmVerIDStringSet];
                    List<Note> NotesToInsert = new List<Note>();
                    if(Notes != null && Notes.size()>0){
                        for(Note note: Notes){
                            Note newNote = new note(); 
                            newNote.Title = note.Title;
                            newNote.Body = note.Body;
                            newNote.OwnerId = note.OwnerId;
                            newNote.IsPrivate = note.IsPrivate;
                            newNote.ParentID = IncmVeriftnIdVsEmpHistMap.get(note.parentid).id;
                            NotesToInsert.add(newNote);
                        }
                        if(NotesToInsert.size()>0){
                            insert NotesToInsert;
                        }
                    }
                    
                    //Migrating ContentNotes
                    
                    List<ContentDocumentLink> documentLinks = [Select id, LinkedEntityId, ShareType, ContentDocumentID from ContentDocumentLink where LinkedEntityId IN: incmVerIDStringSet];
                    List<ContentDocumentLink> LinksToInsert = new List<ContentDocumentLink>();
                    List<ContentDocumentLink> LinksToDelete = new List<ContentDocumentLink>();
                    if(documentLinks != null && documentLinks.size()>0){
                        for(ContentDocumentLink cd: documentLinks){
                            ContentDocumentLink newDocLink = cd.clone();
                            newDocLink.LinkedEntityID = IncmVeriftnIdVsEmpHistMap.get(cd.LinkedEntityID).id;
                            LinksToInsert.add(newDocLink);
                            LinksToDelete.add(cd);
                        }
                        
                        insert LinksToInsert;
                        delete LinksToDelete;
                    }
                }
                
                if(incomeVerificationListToBeUpdated.size() > 0){
                    update incomeVerificationListToBeUpdated;
                }
    }
}