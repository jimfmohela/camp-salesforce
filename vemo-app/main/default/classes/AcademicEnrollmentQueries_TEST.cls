/////////////////////////////////////////////////////////////////////////
// Class: AcademicEnrollmentQueries_TEST
// 
// Description: 
//  Unit test for AcademicEnrollmentQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-04   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class AcademicEnrollmentQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetAcademicEnrollmentMapForAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        Test.startTest();
        Map<Id, AcademicEnrollment__c> resultAcademicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithStudent(testStudentAccountMap.keyset());
        System.assertEquals(testAcademicEnrollmentMap.keySet().size(), resultAcademicEnrollmentMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testAcademicEnrollmentMapForAuthUserwithRead(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        for(Account stud : testStudentAccountMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(TestUtil.TEST_THROTTLE);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(testStudentAccountMap, testSPOSMap);
        Test.startTest();
        Map<Id, AcademicEnrollment__c> resultAcademicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithProvider(schools.keyset());
        System.assertEquals(testAcademicEnrollmentMap.keySet().size(), resultAcademicEnrollmentMap.keySet().size());
        Test.stopTest();
    }
}