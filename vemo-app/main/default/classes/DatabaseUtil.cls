public class DatabaseUtil {
    public static Boolean filterByAccessRules = false;
    public static ID accessingAccount = null;

    public static Integer pageSize = 50000;
    public static Integer page = 1;
    public static String primaryResource = null;
    public static Boolean paginationFlag = false;

    public static Boolean filterByStudentID = false;
    public static Boolean filterBySchoolID = false;


    public static ID schoolID = null;
    public static ID studentPersonAccountID = null;
    public static Boolean runQueriesInUserMode = true;
    private static Boolean runQueriesInMockingMode = Test.isRunningTest();

    public Boolean withSharing {get;set;}
    
    public static Boolean filterByDate = false;
    public static Date fromDate = null;
    public static Date toDate = null;
    public static Boolean filterByConfirmFlag = false;
    public static Boolean confirmFlag = null;
    public IQueryExecutor queryExecutor = null;

    public static String orderBy = null;
    
    public DatabaseUtil(){
        if(runQueriesInMockingMode){
            queryExecutor  = new MockedQueryExecutor();            
        }else{
            if(runQueriesInUserMode){
                queryExecutor  = new UserContext();            
            } else {
                queryExecutor  = new SystemContext();
            }
        }        
    }
    
    public DatabaseUtil(Boolean withSharing){
        this(); 
        this.withSharing = withSharing;
    }

    public List<SObject> query(String query){
        system.debug('runQueriesInUserMode: '+runQueriesInUserMode);
        LogService.debugUntruncated(query);
        List<SObject> retList = queryExecutor.databaseQuery(query);
        //LogService.debugUntruncated(String.valueOf(retList));
        //orderBy = null;
        return retList;
    }
    
    public void updateRecords(List<SObject> objs){
        queryExecutor.updateRecords(objs);
    }
    
    public void updateRecord(SObject obj){
        queryExecutor.updateRecord(obj);
    }
    
    public void upsertRecords(List<SObject> objs){
        queryExecutor.upsertRecords(objs);
    }
    
    public void upsertRecord(SObject obj){
        queryExecutor.upsertRecord(obj);
    }
    
    public void deleteRecords(List<SObject> objs){
        queryExecutor.deleteRecords(objs);
    }
    
    public void deleteRecord(SObject obj){
        queryExecutor.deleteRecord(obj);
    }
    
    public void insertRecord(SObject obj){
        queryExecutor.insertRecord(obj);
    }    
    
    public void insertRecords(List<SObject> objs){
        queryExecutor.insertRecords(objs);
    }
    
    public void undeleteRecords(List<SObject> objs){
        queryExecutor.undeleteRecords(objs);
    }
    
    public void undeleteRecord(SObject objs){
        queryExecutor.undeleteRecord(objs);
    }
    
    public static void setRunQueriesInMockingMode(Boolean status){
        runQueriesInMockingMode = status;
    } 
    
    public static String inSetStringBuilder(Set<String> strings){
        String retStr = '(\'';
        Boolean firstString = true;
        for(String str : strings){
            if(!firstString){
                retStr += '\',\'';              
            }
            retStr += str;
            firstString = false;
        }
        retStr += '\')';
        system.debug(retStr);
        return retStr;
    }

    public static String inSetStringBuilder(Set<ID> ids){
        String retStr = '(\'';
        Boolean firstString = true;
        for(Id str : ids){
            if(!firstString){
                retStr += '\',\'';              
            }
            retStr += str;
            firstString = false;
        }
        retStr += '\')';
        system.debug(retStr);
        return retStr;
    }

    public static String inStringBuilder(String str){
        String retStr = '\'';
        retStr += str;
        retStr += '\'';
        system.debug(retStr);
        return retStr;
    }

    public static Integer getOffset(){
        System.debug('pageSize' + pageSize);
        System.debug('page' + page);
        Integer offset = getPageSize()*(-1)+(getPageSize()*getPage());
        System.debug('offset'+offset);
        return offset;
    }
    public static Integer getPageSize(){
        return pageSize;
    }
    public static Integer getPage(){
        return page;
    }
    public static String getPrimaryResource(){
        return primaryResource;
    }
    public static String generateOrderByStatement(){
        if(String.isNotBlank(DatabaseUtil.orderBy)){
            return ' ORDER BY '+DatabaseUtil.orderBy;
        } else return null;
    }
}