public with sharing class EventTypeService {

    public static List<EventType> getEventTypeWithEventTypeID(Set<ID> EventTypeIDs){
        Map<Id, EventType__c> eventTypeMap = EventTypeQueries.getEventTypeMapWithEventTypeId(eventTypeIDs);
        List<EventType> eventTypeList = new List<EventType>();
        for(EventType__c et : EventTypeMap.values()){
            eventTypeList.add(new EventType(et));
        }
        return eventTypeList;
    }
    
    public static List<EventType> getAllEventTypesMap(){
        Map<Id, EventType__c> eventTypeMap = EventTypeQueries.getAllEventTypesMap();
        List<EventType> eventTypeList = new List<EventType>();
        for(EventType__c et : EventTypeMap.values()){
            eventTypeList.add(new EventType(et));
        }
        return eventTypeList;
    }

    public class EventType{
    
        public String EventTypeID {get;set;}
        public string name {get;set;}
        public String description{get;set;}
        public string mergeObject {get;set;}
        public string MergeTemplate{get;set;}
        public string MergeText{get;set;}
    
        public EventType(){}

        public EventType(EventType__c et){
            this.eventTypeID = et.ID;
            this.name = et.name;  
            this.description = et.Description__c;
            this.mergeObject = et.MergeObject__c;    
            this.mergeTemplate = et.MergeTemplate__c;
            this.mergeText = et.MergeText__c;
        }
    }
}