@isTest
public with sharing class EventBatchScheduled_TEST {
  
    @isTest
    static void instantiateEventInstance_Test(){
        
        Test.startTest();
        
        EventBatchScheduled job = new EventBatchScheduled();
        job.jobType = EventBatch.JobType.INSTANTIATE_EVENTINSTANCE;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('delinquencySchedule_Test', CRON_EXP, job);
        
        Test.stopTest();
    }
}