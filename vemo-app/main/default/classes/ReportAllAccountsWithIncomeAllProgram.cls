public with sharing class ReportAllAccountsWithIncomeAllProgram{
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    @AuraEnabled
    public static List<reportDataWrapper> runReport(){
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        //Final output Map 
        Map<String,ReportDataWrapper> outputMap = new Map<String,ReportDataWrapper>();  
        
        //Set of agreement ID's
        Set<ID> agreementIDs = new Set<ID>(); 
        
        //Map of Agreements by Student
        Map<ID,List<StudentProgram__c>> studentAgreementMap = new Map<ID,List<StudentProgram__c>>(); 
        
        //Map of Income by Student
        Map<ID,IncomeVerification__c> studentIncomeMap = new Map<ID,IncomeVerification__c>();
        
        //Map of Payments by Agreement
        Map<ID,Decimal> agreementPaymentMap = new Map<ID,Decimal>();
        
        //Map of last Month Payments by agreements
        Map<ID,Decimal> lastMonthPayments = new Map<ID,Decimal>();
        
        //Map of AgreementId and Delinquency Status
        Map<ID,String> agreementDelinquencyStatusMap = new Map<ID,String>();
        /////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //get all agreements for selected program
        agreements(studentAgreementMap,agreementIDs);
        
        //get delinquency data
        delinquencyData(agreementIDs,agreementDelinquencyStatusMap);
        
        //get Income Data
        if(studentAgreementMap.keySet().size()>0){
            incomeData(studentAgreementMap.keySet(),studentIncomeMap);
        }
        
        //get payment data
        if(agreementIDs.size()>0){
            paymentData(agreementIDs,agreementPaymentMap,lastMonthPayments);         
        }
               
        //populate the final reportData list 
        List<reportDataWrapper> reportData = buildReportData(studentAgreementMap ,studentIncomeMap,agreementPaymentMap,lastMonthPayments,agreementDelinquencyStatusMap);
        return reportData;
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public static List<ReportDataWrapper> buildReportData(Map<ID,List<StudentProgram__c>> studentAgreementMap,Map<ID,IncomeVerification__c> studentIncomeMap,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments,Map<ID,String> agreementDelinquencyStatusMap){
        List<ReportDataWrapper> output = new List<ReportDataWrapper>();
        
        for(ID studentID:studentAgreementMap.keySet()){                  
            for(StudentProgram__c sp:studentAgreementMap.get(studentID)){
                ReportDataWrapper wrapper = new ReportDataWrapper();
                wrapper.vemoAccountNumber = sp.StudentVemoAccountNumber__c;
                wrapper.studentID = sp.student__c;
                wrapper.studentName = sp.student__r.name;
                wrapper.studentProgramId = sp.Id;
                wrapper.studentProgramNumber = sp.Name;
                wrapper.vemoContactNumber = sp.VemoContractNumber__c;
                wrapper.status = sp.Status__c;
                wrapper.schoolName = sp.SchoolName__c;
                wrapper.programId = sp.Program__c;
                wrapper.programName = sp.ProgramName__c;
                wrapper.programNumber = sp.program__r.name;
                wrapper.primaryOwnerName = sp.primaryOwner__r.name;
                wrapper.fundOwnerName = sp.FundOwner__r.name;
                wrapper.paymentStreamOwnerName = sp.PaymentStreamOwner__r.name;
                if(sp.ApplicationStartDate__c!=null)
                    wrapper.applicationStartDate = sp.ApplicationStartDate__c;
                if(sp.SubmittedDate__c!=null)
                    wrapper.submittedDate = sp.SubmittedDate__c;
                if(sp.CertificationDate__c!=null)
                    wrapper.certificationDate = sp.CertificationDate__c;
                wrapper.servicingStartDate = sp.ServicingStartDate__c;
                wrapper.majorPostCertificationName = sp.majorPostCertification__r.name;
                wrapper.gradeLevelPostCertification = sp.GradeLevelPostCertification__c;
                wrapper.enrollmentStatusPostCertification = sp.EnrollmentStatusPostCertification__c;
                wrapper.fundingAmountPostCertification = sp.FundingAmountPostCertification__c;
                wrapper.incomeSharePostCertification = sp.IncomeSharePostCertification__c;
                wrapper.paymentTermPostCertification = sp.PaymentTermPostCertification__c;
                wrapper.paymentCapPostCertification = sp.PaymentCapPostCertification__c;
                wrapper.defermentMonthsAllowed = sp.DefermentMonthsAllowed__c;
                wrapper.defermentMonthsRemaining = sp.DefermentMonthsRemaining2__c;
                wrapper.graceMonthsAllowed = sp.GraceMonthsAllowed__c;
                wrapper.graceMonthsRemaining = sp.GraceMonthsRemaining2__c;
                wrapper.paymentTermAssessed = sp.PaymentTermAssessed__c;
                wrapper.paymentTermRemaining = sp.PaymentTermRemaining__c;
                wrapper.minimumIncomePerMonth = sp.MinimumIncomePerMonth__c;
                /*AgreementWrapper agre = new AgreementWrapper();
                agre.agreement = sp;*/
                //bucket by delinquency
                if(agreementDelinquencyStatusMap.containsKey(sp.id)){
                    wrapper.delinquencyBucket = agreementDelinquencyStatusMap.get(sp.id);
                }
                
                if(agreementPaymentMap.containsKey(sp.id)) wrapper.cumulativePayments = agreementPaymentMap.get(sp.id);
                if(lastMonthPayments.containsKey(sp.id)) wrapper.lastMonthPayments = lastMonthPayments.get(sp.id);
                
                if(studentIncomeMap.containsKey(studentID)){
                    wrapper.employmentHistoryName = studentIncomeMap.get(studentID).EmploymentHistory__r.name;
                    wrapper.employmentHistoryEmployer = studentIncomeMap.get(studentID).EmploymentHistory__r.Employer__c;
                    wrapper.employmentHistoryDateReported = studentIncomeMap.get(studentID).EmploymentHistory__r.DateReported__c;
                    wrapper.employmentHistoryType = studentIncomeMap.get(studentID).EmploymentHistory__r.Type__c;
                    wrapper.employmentHistoryStartDate = studentIncomeMap.get(studentID).EmploymentHistory__r.EmploymentStartDate__c;
                    wrapper.employmentHistoryEndDate = studentIncomeMap.get(studentID).EmploymentHistory__r.EmploymentEndDate__c;
                    wrapper.employmentHistoryCatogery = studentIncomeMap.get(studentID).EmploymentHistory__r.Category__c;
                    wrapper.incomePerMonth = studentIncomeMap.get(studentID).IncomePerMonth__c;
                    wrapper.beginDate = studentIncomeMap.get(studentID).BeginDate__c;
                    wrapper.endDate = studentIncomeMap.get(studentID).EndDate__c;
                }
                output.add(wrapper);
            }
            
            
        }
        
        return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student Programs for the selected School
    ///////////////////////////////////////////////
    public static void agreements(Map<ID,List<StudentProgram__c>> agreementsByStudent,Set<ID> agreementIDs){
        Set<String> exclusionStatus = new Set<String>{'Draft','Invited','Application Incomplete','Application Complete','Application Under Review'};  
        datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        List<StudentProgram__c> agreementList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                                                 PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,program__r.Name,
                                                 ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                                                 MajorPostCertification__r.name,GradeLevelPostCertification__c,EnrollmentStatusPostCertification__c,
                                                 FundingAmountPostCertification__c,IncomeSharePostCertification__c,PaymentTermPostCertification__c,
                                                 PaymentCapPostCertification__c,PaymentStreamOwner__r.name,FundOwner__r.name,
                                                 GraceMonthsAllowed__c,GraceMonthsRemaining2__c,PaymentTermAssessed__c,PaymentTermRemaining__c,
                                                 DefermentMonthsAllowed__c,DefermentMonthsRemaining2__c,MinimumIncomePerMonth__c,
                                                 DaysDelinquent__c,servicing__c
                                                 FROM StudentProgram__c
                                                 WHERE Status__c NOT IN :exclusionStatus AND /*Program__r.school__c =: selectedSchool AND*/ CertificationDate__c <> null AND certificationDate__c < :dt
                                                 ORDER BY SchoolName__c ASC];            
        Integer offset;
        for(StudentProgram__c agreement:agreementList){
            if(agreement.certificationDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.certificationDate__c);
                agreement.certificationDate__c = agreement.certificationDate__c.addSeconds(offset/1000);
            }
            if(agreement.ApplicationStartDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.ApplicationStartDate__c);
                agreement.ApplicationStartDate__c = agreement.ApplicationStartDate__c.addSeconds(offset/1000);
            }
            if(agreement.submittedDate__c <> null){
                offset = UserInfo.getTimezone().getOffset(agreement.submittedDate__c);
                agreement.submittedDate__c = agreement.submittedDate__c.addSeconds(offset/1000);
            }
            
            if(!agreementsByStudent.containsKey(agreement.student__c)){
                agreementsByStudent.put(agreement.student__c,new List<StudentProgram__c>());
            }
            agreementsByStudent.get(agreement.student__c).add(agreement);
            agreementIDs.add(agreement.ID);
        }
    }
    
    /////////////////////////////////////////////////
    ///Description: get delinquency data
    /////////////////////////////////////////////////
    private static void delinquencyData(Set<ID> agreementIDs,Map<ID,String> agreementDelinquencyStatusMap){
        List<StudentProgramAudit__c> spaList = new List<StudentProgramAudit__c>();
        spaList = [SELECT daysdelinquent__c,studentprogram__c,auditdatetime__c,servicing__c
                   FROM StudentProgramAudit__c
                   WHERE monthEnd__c = true AND studentProgram__c IN :agreementIDs AND createdDate = LAST_MONTH];
        for(StudentProgramAudit__c audit:spaList){
                if(audit.DaysDelinquent__c >= 270){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'270+ Days Delinquent');        
                }
                else if(audit.DaysDelinquent__c < 270 && audit.DaysDelinquent__c >= 210){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'210-269 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 210 && audit.DaysDelinquent__c >= 180){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'180-209 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 180 && audit.DaysDelinquent__c >= 120){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'120-179 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 120 && audit.DaysDelinquent__c >= 90){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'90-119 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 90 && audit.DaysDelinquent__c >= 60){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'60-89 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 60 && audit.DaysDelinquent__c >= 30){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'30-59 Days Delinquent');
                }
                else if(audit.DaysDelinquent__c < 30 && audit.DaysDelinquent__c >= 0 && audit.servicing__c == true){
                    agreementDelinquencyStatusMap.put(audit.studentProgram__c,'Current');
                }
        }
    }
    
    ////////////////////////////////////////////////////////////////
    ///Description: Get the Payment Data
    ////////////////////////////////////////////////////////////////
    public static void paymentData(Set<ID> agreementIDs,Map<ID,Decimal> agreementPaymentMap,Map<ID,Decimal> lastMonthPayments){
        Datetime dt = datetime.newInstance(date.today().year(),date.today().month(),1); 
        Integer month = date.today().month() == 1 ? 12 : date.today().month()-1;
        Integer year = date.today().month() == 1 ? (date.today().year()-1) : date.today().year();
        
        List<PaymentAllocation__c> payments = [SELECT id,agreement__c,amountAllocated__c,createddate
                                         FROM PaymentAllocation__c
                                         WHERE agreement__c IN :agreementIDs AND createdDate < :dt];
        for(PaymentAllocation__c payment:payments){
            if(!agreementPaymentMap.containsKey(payment.agreement__c)){
                agreementPaymentMap.put(payment.agreement__c,0);
            }
            agreementPaymentMap.put(payment.agreement__c,agreementPaymentMap.get(payment.agreement__c)+payment.amountallocated__c); 
            
            if(payment.createdDate.date().month() == month && payment.createdDate.date().year() == year){
                if(!lastMonthPayments.containsKey(payment.agreement__c)){
                   lastMonthPayments.put(payment.agreement__c,0);
                }
                lastMonthPayments.put(payment.agreement__c,lastMonthPayments.get(payment.agreement__c) + payment.amountAllocated__c);            
            }    
        }
    }
    
    //////////////////////////////////////////////////
    ///Get Income Data
    //////////////////////////////////////////////////
    public static void incomeData(Set<ID> studentIDs,Map<ID,IncomeVerification__c> studentIncomeMap){    
        List<incomeVerification__c> ivList = new List<incomeVerification__c>();
        ivList = [Select id,student__c,beginDate__c,endDate__c,type__c,IncomePerMonth__c,DateVerified__c,
                  EmploymentHistory__r.name,EmploymentHistory__c,EmploymentHistory__r.Employer__c,EmploymentHistory__r.DateReported__c,EmploymentHistory__r.type__c,
                  EmploymentHistory__r.EmploymentStartDate__c,EmploymentHistory__r.EmploymentEndDate__c,EmploymentHistory__r.Category__c 
                  From IncomeVerification__c
                  Where Student__c IN :studentIDs AND status__c = 'Verified' AND type__c = 'Reported' 
                  Order By beginDate__c DESC,Student__r.name ASC,type__c DESC];
        Map<Id,List<IncomeVerification__c>> sortedIncomesByStudent = new Map<Id,List<IncomeVerification__c>>(); 
        for(IncomeVerification__c iv:ivList){
            if(!sortedIncomesByStudent.containsKey(iv.student__c)){
                sortedIncomesByStudent.put(iv.student__c,new List<IncomeVerification__c>());
            }
            sortedIncomesByStudent.get(iv.student__c).add(iv);
        }
        
        Date dt = Date.newInstance(date.today().year(),date.today().month(),1); 
        for(ID key:sortedIncomesByStudent.keySet()){
            List<incomeVerification__c> incomeList = sortedIncomesByStudent.get(key);
            //studentIncome.put(key,new IncomeVerification__c()); 
            Integer size = incomeList.size();
            for(integer i=0;i<incomeList.size();i++){
                if(incomeList[i].BeginDate__c < dt){
                    studentIncomeMap.put(key,incomeList[i]);
                }
                else{
                    continue;
                }
                if(!(i==(incomeList.size()-1))){
                    if(incomeList[i].BeginDate__c == incomeList[i+1].BeginDate__c){
                        if(incomeList[i].dateVerified__c > incomeList[i+1].DateVerified__c){
                            break;        
                        }                        
                    }
                    else{
                        break;
                    }
                }
            }
        } 
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        @AuraEnabled
        public String vemoAccountNumber {get;set;}
        @AuraEnabled
        public String studentID {get;set;}
        @AuraEnabled
        public String studentName {get;set;}
        @AuraEnabled
        public string studentProgramId {get;set;}
        @AuraEnabled
        public string studentProgramNumber {get;set;}
        @AuraEnabled
        public string vemoContactNumber {get;set;}
        @AuraEnabled
        public string status {get;set;}
        @AuraEnabled
        public string schoolName {get;set;}
        @AuraEnabled
        public string programId {get;set;}
        @AuraEnabled
        public string programName {get;set;}
        @AuraEnabled
        public string programNumber {get;set;}
        @AuraEnabled
        public string primaryOwnerName {get;set;}
        @AuraEnabled
        public string fundOwnerName {get;set;}
        @AuraEnabled
        public string paymentStreamOwnerName {get;set;}
        @AuraEnabled
        public dateTime applicationStartDate {get;set;}
        @AuraEnabled
        public dateTime submittedDate {get;set;}
        @AuraEnabled
        public dateTime certificationDate {get;set;}
        @AuraEnabled
        public date servicingStartDate {get;set;}
        @AuraEnabled
        public string majorPostCertificationName {get;set;}
        @AuraEnabled
        public string gradeLevelPostCertification {get;set;}
        @AuraEnabled
        public string enrollmentStatusPostCertification {get;set;}
        @AuraEnabled
        public decimal fundingAmountPostCertification {get;set;}
        @AuraEnabled
        public decimal incomeSharePostCertification {get;set;}
        @AuraEnabled
        public decimal paymentTermPostCertification {get;set;}
        @AuraEnabled
        public decimal paymentCapPostCertification {get;set;}
        @AuraEnabled
        public decimal defermentMonthsAllowed {get;set;}
        @AuraEnabled
        public decimal defermentMonthsRemaining {get;set;}
        @AuraEnabled
        public decimal graceMonthsAllowed {get;set;}
        @AuraEnabled
        public decimal graceMonthsRemaining {get;set;}
        @AuraEnabled
        public decimal paymentTermAssessed {get;set;}
        @AuraEnabled
        public decimal paymentTermRemaining {get;set;}
        @AuraEnabled
        public decimal minimumIncomePerMonth {get;set;}
        //@AuraEnabled
        //public List<AgreementWrapper> agreements {get;set;}
        @AuraEnabled
        public string delinquencyBucket {get;set;}
        @AuraEnabled
        public decimal cumulativePayments {get;set;}
        @AuraEnabled
        public decimal lastMonthPayments {get;set;}
        @AuraEnabled
        public string employmentHistoryName {get;set;}
        @AuraEnabled
        public string employmentHistoryEmployer {get;set;}
        @AuraEnabled
        public date employmentHistoryDateReported {get;set;}
        @AuraEnabled
        public string employmentHistoryType {get;set;}
        @AuraEnabled
        public date employmentHistoryStartDate {get;set;}
        @AuraEnabled
        public date employmentHistoryEndDate {get;set;}
        @AuraEnabled
        public string employmentHistoryCatogery {get;set;}
        @AuraEnabled
        public decimal incomePerMonth {get;set;}
        @AuraEnabled
        public date beginDate {get;set;}
        @AuraEnabled
        public date endDate {get;set;}
         
    }    
}