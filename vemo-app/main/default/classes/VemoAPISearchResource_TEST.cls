@isTest
public class VemoAPISearchResource_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static VemoAPI.APIInfo initializeAPI(String version, String method, Map<String, String> params, String body){
        VemoAPI.APIInfo apiInfo = new VemoAPI.APIInfo();
        apiInfo.method = method;
        apiInfo.version = version;
        apiInfo.params = params;
        apiInfo.body = body;
        return apiInfo;
    }
    
    static testMethod void validateHandleGetV2(){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        
        Map<ID, ProgramOfStudy__c> posMap = TestDataFactory.createAndInsertProgramOfStudy(1);
        Map<ID, SchoolProgramsOfStudy__c> testSPOSMap = TestDataFactory.createAndInsertSchoolProgramsOfStudy(schools, posMap);
        
        Map<Id, AcademicEnrollment__c> testAcademicEnrollmentMap = TestDataFactory.createAndInsertAcademicEnrollment(students, testSPOSMap);
        for(StudentProgram__c sp: studProgramMap.values()){
            sp.AcademicEnrollment__c = testAcademicEnrollmentMap.values()[0].id; 
        }
        
        Map<String, String> params = new Map<String, String>();
        params.put('search', 'test');
        VemoAPI.APIInfo searchApiInfo = initializeAPI('v2', 'GET', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse searchResult = (VemoAPI.ResultResponse)VemoAPISearchResource.handleAPI(searchApiInfo);
        System.assertEquals(1, searchResult.numberOfResults);
        Test.stopTest();
    }
}