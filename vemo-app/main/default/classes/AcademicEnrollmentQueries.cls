public class AcademicEnrollmentQueries {

    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }
    
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithStudent(set<Id> studentIDSet){
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDSet);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithStudentNStatus(set<Id> studentIDSet, Set<String> statuses){
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIDSet);
        
        if(statuses != null && statuses.size()>0){
            query += ' and AcademicEnrollmentStatus__c IN ' + DatabaseUtil.inSetStringBuilder(statuses);
        }
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithProvider(set<Id> providerIDSet){
        System.debug('AcademicEnrollmentQueries.getAcademicEnroolementMapWithProvider()');
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Provider__c IN ' + DatabaseUtil.inSetStringBuilder(providerIDSet);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        System.debug('DatabaseUtil.orderBy:'+DatabaseUtil.orderBy);
        query += ' '+ DatabaseUtil.generateOrderByStatement();

        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithProviderNStatus(set<Id> providerIDSet, Set<String> statuses){
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Provider__c IN ' + DatabaseUtil.inSetStringBuilder(providerIDSet);
        
        if(statuses != null && statuses.size()>0){
            query += ' and AcademicEnrollmentStatus__c IN ' + DatabaseUtil.inSetStringBuilder(statuses);
        }
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithId(set<Id> academicEnrollmentIds){
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(academicEnrollmentIds);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    
    //Used to find whtheather AE exists or not
    public static Map<ID, AcademicEnrollment__c> getAcademicEnrollmentMapWithMultiFields (
        set<Id> studentIds, set<Id> providerIds
    ){
        
        Map<ID, AcademicEnrollment__c> academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds);
        query += ' AND Provider__c IN ' + DatabaseUtil.inSetStringBuilder(providerIds);
        
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
            if(DatabaseUtil.filterBySchoolID){
                ID schoolID = DatabaseUtil.schoolID;
                query += ' and Provider__c = ' + DatabaseUtil.inStringBuilder(schoolID)+ ' ';
            }
        }
        
        query += buildFilterString();
        if(String.isEmpty(DatabaseUtil.orderBy)) DatabaseUtil.orderBy = 'CreatedDate desc';
        query += ' '+ DatabaseUtil.generateOrderByStatement();
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        academicEnrollmentMap = new Map<ID, AcademicEnrollment__c>((List<AcademicEnrollment__c>)db.query(query));
        DatabaseUtil.orderBy = '';
        return academicEnrollmentMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getFieldNames() + ' FROM AcademicEnrollment__c';
        return soql;
    }

    private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Grade__c, ';
        fieldNames += 'GraduationDate__c, ';
        fieldNames += 'Provider__c, ';
        fieldNames += 'SchoolProgramOfStudy__c, ';
        //fieldNames += 'Status__c, ';
        fieldNames += 'AcademicEnrollmentStatus__c, ';
        fieldNames += 'LastDateofAttendance__c, ';
        fieldNames += 'Student__c, ';
        fieldNames += 'PendingStatus__c, ';
        //fieldNames += 'PendingUpdate__c, ';
        fieldNames += 'PendingGrade__c, ';
        fieldNames += 'PendingGraduationDate__c, ';
        fieldNames += 'PendingLastDateOfAttendance__c, ';
        fieldNames += 'PendingSchoolProgramOfStudy__c, ';
        fieldNames += 'Record_Locked__c, ';
        fieldNames += 'Controlling_Case__c, ';
        fieldNames += 'DateOfLastStatusChange__c, ';
        fieldNames += 'NotesComments__c, ';
        fieldNames += 'ChangeRequestedBy__c, ';
        fieldNames += 'DateOfLastApprovedChange__c, ';
        fieldNames += 'PendingEffectiveDateOfChange__c, ';
        fieldNames += 'PendingLeaveOfAbsenceEndDate__c, ';
        fieldNames += 'PendingLeaveOfAbsenceStartDate__c, ';
        fieldNames += 'EffectiveDateofChange__c, ';
        fieldNames += 'LeaveofAbsenceEndDate__c, ';
        fieldNames += 'LeaveofAbsenceStartDate__c ';
        return fieldNames;
   }
   
   
    public static String generateLIMITStatement(){
        String lim;
        if(DatabaseUtil.getPrimaryResource() == 'academicEnrollment') {
            lim = 'LIMIT '+ DatabaseUtil.getPageSize() + ' OFFSET ' + DatabaseUtil.getOffset();
        }else {
            lim = 'LIMIT 50000';
        }
        return lim;
    }
  
    private static String buildFilterString(){
        String filterStr = '';
       
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +   '\' ');
            }      
        }
        return filterStr;
    }  
}