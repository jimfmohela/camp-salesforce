public with sharing class AgreementStatusChangeTriggerHandler {

    public void processAgreementStatusChange(List<AgreementStatusChange__e> AgreementStatusChangeList){
        
        set<Id> studentProgramIdsSet = new set<Id>();
        List<Event__c> eventList = new List<Event__c>();
        set<String> changeStatusSet = new set<String>();
        Map<string, EventType__c> eventTypeNameMap = new Map<string, EventType__c>();
        
        for(AgreementStatusChange__e asChange: AgreementStatusChangeList){
            if(asChange.studentprogram__c != null) studentProgramIdsSet.add(asChange.studentprogram__c);
            
            if(asChange.ToStatus__c != null){
                changeStatusSet.add(asChange.ToStatus__c);    
            }
        }
        
        if(changeStatusSet != null && changeStatusSet.size()>0){
            eventTypeNameMap = findEventTypes(changeStatusSet);
        }
        
        if(studentProgramIdsSet != null && studentProgramIdsSet.size()>0){
            Map<ID, StudentProgram__c> studPrgMap = StudentProgramQueries.getStudentProgramMapWithAgreementID(studentProgramIdsSet);
        
            for(Id id : studPrgMap.keyset()){
                if(studPrgMap.get(id).Program__c != null && studPrgMap.get(id).Program__r.school__c != null 
                    && eventTypeNameMap.get(studPrgMap.get(id).status__c) != null){
                    
                    Event__c ev = new Event__c();
                    ev.eventType__c = eventTypeNameMap.get(studPrgMap.get(id).status__c).id;
                    ev.Account__c = studPrgMap.get(id).Program__r.School__c;
                    ev.WhatId__c = id;
                    eventList.add(ev);
                }
            }
            
            if(eventList != null && eventList.size()>0) insert eventList;
        }
    }
    
    public Map<string, EventType__c> findEventTypes(set<string> eventTypeNameSet){
        Map<string, EventType__c> eventTypeNameMap = new Map<string, EventType__c>();
        List<EventType__c> eventTypeList = [select id, name from eventType__c where name IN: eventTypeNameSet]; 
        
        if(eventTypeList != null && eventTypeList.size()>0){
            for(EventType__c et: eventTypeList){
                eventTypeNameMap.put(et.name, et);
            }
        } 
        return eventTypeNameMap ;  
    }
    
}