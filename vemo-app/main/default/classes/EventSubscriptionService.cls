public with sharing class EventSubscriptionService {

    public static List<EventSubscription> getEventSubscriptionMapWithAuthUser(){    
        set<Id> contactIDSet = new set<ID>();
        
        if(UserService.userContext != null && UserService.userContext.VemoContact__c != null)
            contactIDSet.add(UserService.userContext.VemoContact__c);
        
        Map<Id, EventSubscription__c> eventSubscriptionMap = EventSubscriptionQueries.getEventSubscriptionMapWithAuthUser(contactIDSet);
        List<EventSubscription> eventSubscriptionList = new List<EventSubscription>();
        for(EventSubscription__c es : eventSubscriptionMap.values()){
            eventSubscriptionList.add(new EventSubscription(es));
        }
        return eventSubscriptionList;
    }
    /*public static List<EventSubscription> getEventSubscriptionMapWithEventSubscriptionID(Set<ID> eventSubscriptionIDs){
        Map<Id, EventSubscription__c> eventSubscriptionMap = EventSubscriptionQueries.getEventSubscriptionMapWithEventSubscriptionID(eventSubscriptionIDs);
        List<EventSubscription> eventSubscriptionList = new List<EventSubscription>();
        for(EventSubscription__c es : eventSubscriptionMap.values()){
            eventSubscriptionList.add(new EventSubscription(es));
        }
        return eventSubscriptionList;
    }*/
    
    public static Set<ID> createEventSubscription(List<EventSubscription> eSubs){
        System.debug('EventSubscriptionService.createEventSubscription');
        List<EventSubscription__c> newESubscriptionList = new List<EventSubscription__c>();
        
        for(EventSubscription es : eSubs){
            newESubscriptionList.add(EventSubscriptionToEventSubscription(es));
        }
        insert newESubscriptionList;
        Set<ID> eventSubscriptionIDs = new Set<ID>();
        for(EventSubscription__c es : newESubscriptionList){
            eventSubscriptionIDs.add(es.ID);
        }
        return eventSubscriptionIDs;
    }

    public static Set<ID> updateEventSubscription(List<EventSubscription> EventSubscriptions){
    
        List<EventSubscription__c> eSubscriptionList = new List<EventSubscription__c>();
        for(EventSubscription es : EventSubscriptions){
            eSubscriptionList.add(EventSubscriptionToEventSubscription(es));
        }
        update eSubscriptionList;
        
        Set<ID> eventSubscriptionIDs = new Set<ID>();
        for(EventSubscription__c es : eSubscriptionList){
          eventSubscriptionIDs.add(es.ID);
        }
        return eventSubscriptionIDs;
    }
    
    public static Integer deleteEventSubscriptions(Set<ID> eSubscriptionIDs){
        System.debug('EventSubscriptionService.deleteEventSubscriptions()');
        Map<ID, EventSubscription__c> esMap = EventSubscriptionQueries.getEventSubscriptionMapWithEventSubscriptionID(eSubscriptionIDs);
        Integer numToDelete = esMap.size();
        delete esMap.values();
        return numToDelete;
    }

    public static EventSubscription__c EventSubscriptionToEventSubscription(EventSubscription es){
        EventSubscription__c eSub = new EventSubscription__c();
        if(es.eventSubscriptionID != null) eSub.ID = es.eventSubscriptionID;
        if(es.account != null) eSub.Account__c = es.account;
        if(es.contact != null) eSub.Contact__c = es.contact;
        if(es.email != null) eSub.Email__c = es.email;
        if(es.eventType != null) eSub.EventType__c = es.eventType;
        return eSub;
    }

    public class EventSubscription{
        public String eventSubscriptionID{get;set;}
        public String account{get;set;}
        public String contact{get;set;}
        public boolean email{get;set;}
        public String eventType {get;set;}

        public EventSubscription(){}

        public EventSubscription(EventSubscription__c es){
            this.eventSubscriptionID = es.ID;
            this.account = es.Account__c;
            this.contact = es.Contact__c;
            this.email = es.Email__c;
            this.eventType = es.EventType__c;
        }
    }
}