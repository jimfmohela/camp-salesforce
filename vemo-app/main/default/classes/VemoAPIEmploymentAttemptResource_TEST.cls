@isTest
public class VemoAPIEmploymentAttemptResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(testDefMap, 1);
        
        Map<String, String> defParams = new Map<String, String>();
        defParams.put('defermentID', TestUtil.createStringFromIDSet(testDefMap.keySet()));
        defParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo defApiInfo = TestUtil.initializeAPI('v1', 'GET', DefParams, null);

        Map<String, String> attParams = new Map<String, String>();
        attParams.put('employmentSeekingAttemptID', TestUtil.createStringFromIDSet(testAttMap.keySet()));
        attParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo attApiInfo = TestUtil.initializeAPI('v1', 'GET', attParams, null);

        Test.startTest();
        VemoAPI.ResultResponse defResult = (VemoAPI.ResultResponse)VemoAPIEmploymentSeekingAttemptResource.handleAPI(DefApiInfo);
        System.assertEquals(testDefMap.size(), defResult.numberOfResults);

        VemoAPI.ResultResponse attResult = (VemoAPI.ResultResponse)VemoAPIEmploymentSeekingAttemptResource.handleAPI(attApiInfo);
        System.assertEquals(testDefMap.size(), attResult.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePostV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1> attList = new List<VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1 att = new VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1(true);
            att.defermentID = testDefMap.values().get(i).ID;
            attList.add(att);
        }
        String body = JSON.serialize(attList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentSeekingAttemptResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }

    static testMethod void testHandlePutV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(testDefMap ,1);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1> attList = new List<VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1 att = new VemoAPIEmploymentSeekingAttemptResource.EmploymentSeekingAttemptResourceInputV1(true);
            att.EmploymentSeekingAttemptID = testAttMap.values().get(i).ID;
            att.contactDate = date.today();
            attList.add(att);
        }
        String body = JSON.serialize(attList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentSeekingAttemptResource.handleAPI(apiInfo);
        //System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }

    static testMethod void testHandleDeleteV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Deferment__c> testDefMap = TestDataFactory.createAndInsertDeferments(testStudentAccountMap);
        Map<Id, EmploymentSeekingAttempt__c> testAttMap = TestDataFactory.createAndInsertEmploymentSeekingAttempts(testDefMap ,1);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('employmentSeekingAttemptID', TestUtil.createStringFromIDSet(testAttMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIEmploymentSeekingAttemptResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }
}