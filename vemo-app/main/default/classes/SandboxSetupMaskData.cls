public class SandboxSetupMaskData implements Database.Batchable<sObject>, Database.Stateful {
    
    public String query;
    public String sObj = 'Log__c';
    public Integer contactCount = 0;

    public SandboxSetupMaskData() {
        
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(sObj == 'Log__c'){
            query = 'Select id From Log__c'; 
        }
        if(sObj == 'Contact'){
            query = 'SELECT id, FirstName, LastName, AuthSystemUserID__c, SSNTaxID__c, Birthdate, DriversLicenseOrStateID__c, Email, Phone, HomePhone, MobilePhone, OtherPhone,  MailingStreet, MailingCity, MailingState, MailingPostalCode, OtherStreet, OtherCity, OtherState, OtherPostalCode from Contact';

        } else if(sObj == 'StudentProgram__c'){
            query = 'SELECT id, Birthdate__c, BirthdateCertification__c, BirthdatePostCertification__c, BirthdatePreCertification__c, BirthdateStudent__c, StudentEmail__c, CertificationComments__c, SignedAgreementID__c, FinalDisclosureID__c, CongaUnsignedAgreementID__c, CongaFinalDisclosureID__c, AdobeSignAgreementID__c, AdobeSignJavascript__c, AdobeSignURL__c  from StudentProgram__c';

        } else if(sObj == 'Program__c'){
            query = 'Select id,SchoolEmailNotification1__c,SchoolEmailNotification2__c From Program__c';
        
        } else if(sObj == 'Case'){
            query = 'SELECT id from Case';

        } else if(sObj == 'StudentProgramAudit__c'){
            query = 'SELECT id from StudentProgramAudit__c';

        } else if(sObj == 'Task'){
            query = 'SELECT id from Task';

        } else if(sObj == 'CreditCheck__c'){
            query = 'SELECT id, ConsentIPAddress__c, CreditCheckDeniedReasonText__c from CreditCheck__c';

        } else if(sObj == 'Notification__c'){
            query = 'SELECT id from Notification__c';

        } else if(sObj == 'Bill__c'){
            query = 'SELECT id, Name__c, StatementData__c, Email__c from Bill__c';

        } else if(sObj == 'Transaction__c'){
            query = 'SELECT id, NotificationEmail__c from Transaction__c';

        } else if(sObj == 'Attachment'){
            query = 'SELECT id,parent.type From Attachment'; 

        } else if(sObj == 'StewardshipACHBatchDetail__c'){
            query = 'SELECT id from StewardshipACHBatchDetail__c';

        } else if(sObj == 'StewardshipACHBatch__c'){
            query = 'SELECT id from StewardshipACHBatch__c';

        } else if(sObj == 'Note'){
            query = 'Select id From Note';
            
        } else if(sObj == 'OutboundEmail__c'){
            query = 'SELECT id from OutboundEmail__c';

        } else if(sObj == 'Account'){
            query = 'SELECT id, StewardshipDisbursementDonorGUID__c, StewardshipPaymentDonorGUID__c from Account';

        } else if(sObj == 'PaymentMethod__c'){
            query = 'SELECT id, BankRoutingNumber__c, BankAccountNumber__c, BankName__c, CreditCardNumber__c, Description__c, StewardshipDisbursementAccountGUID__c, StewardshipPaymentAccountGUID__c from PaymentMethod__c';

        } else if(sObj == 'ClrHouseStudentEnrollment__c'){
            query = 'Select First_Name__c,Middle_Initial__c,Last_Name__c From ClrHouseStudentEnrollment__c';
        
        } else if(sObj == 'Reconciliation__c'){
            query = 'Select id From Reconciliation__c';
        
        } else if(sObj == 'GenericDocument__c'){
            query = 'Select id From GenericDocument__c';
        
        } else if(sObj == 'EmploymentHistory__c'){
            query = 'Select id,Employer__c From EmploymentHistory__c';
        
        } else if(sObj == 'AccountHistory'){
            query = 'Select id From AccountHistory';
        
        } else if(sObj == 'StudentProgram__History'){
            query = 'Select id From StudentProgram__History';
        
        } else if(sObj == 'Transaction__History'){
            query = 'Select id From Transaction__History';
        
        } else if(sObj == 'PaymentMethod__History'){
            query = 'Select id From PaymentMethod__History';
        
        } else if(sObj == 'CreditCheck__History'){
            query = 'Select id From CreditCheck__History';
        
        } else if(sObj == 'AccountNotes'){
            query = 'Select id From Account';
        
        } else if(sObj == 'StudentProgramNotes'){
            query = 'Select id From StudentProgram__c';
            
        } else if(sObj == 'IncomeVerificationNotes'){
            query = 'Select id From IncomeVerification__c';
            
        } else if(sObj == 'EmploymentHistoryNotes'){
            query = 'Select id From EmploymentHistory__c';
            
        }
         
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        if(sObj == 'Log__c'){
            delete scope;
        } else if(sObj == 'Contact'){
            ApprovalSettings__c approvalSetting = ApprovalSettings__c.getOrgDefaults();
            approvalSetting.AccountNameChangeAllowed__c = true;
            if(approvalSetting.Id != null){
                update approvalSetting;
            }
            
            contactCount++;
            for(Contact cont : (List<Contact>)(scope)){
                System.debug('cont:'+cont);
                cont.FirstName = 'Masked';
                cont.LastName = 'Contact'+contactCount;
                if(String.isNotEmpty(cont.SSNTaxID__c)) cont.SSNTaxID__c = '123-45-6789';
                if(String.isNotEmpty(cont.MailingStreet)) cont.MailingStreet = '123 Main Street';
                if(String.isNotEmpty(cont.MailingPostalCode)) cont.MailingPostalCode = '98765-4321';
                if(String.isNotEmpty(cont.OtherStreet)) cont.OtherStreet = '12345 Secondary Street';
                if(String.isNotEmpty(cont.OtherPostalCode)) cont.OtherPostalCode = '11223';
                if(String.isNotEmpty(cont.Phone)) cont.Phone = '(555) 123-4567';
                if(String.isNotEmpty(cont.MobilePhone)) cont.MobilePhone = '(555) 123-4567';
                if(String.isNotEmpty(cont.OtherPhone)) cont.OtherPhone = '(555) 123-4567';
                if(String.isNotEmpty(cont.HomePhone)) cont.HomePhone = '(555) 123-4567';            
                if(String.isNotEmpty(cont.Email)) cont.Email = 'masked.email@vemo.com';
                if(String.isNotEmpty(cont.AuthSystemUserID__c)) cont.AuthSystemUserID__c = 'auth|'+cont.id;
                if(cont.Birthdate != null) cont.Birthdate = Date.newInstance(2000, 1, 1);

            }               

            update scope;

        } else if(sObj == 'StudentProgram__c'){
            for(StudentProgram__c agreement : (List<StudentProgram__c>) (scope)){
                System.debug('agreement:'+agreement);
                if(String.isNotEmpty(agreement.StudentEmail__c)) agreement.StudentEmail__c = 'masked.email@vemo.com';
                if(agreement.Birthdate__c != null) agreement.Birthdate__c = Date.newInstance(2000, 1, 1);
                if(agreement.BirthdateCertification__c != null) agreement.BirthdateCertification__c = Date.newInstance(2000, 1, 1);
                if(agreement.BirthdatePostCertification__c != null) agreement.BirthdatePostCertification__c = Date.newInstance(2000, 1, 1);
                if(agreement.BirthdatePreCertification__c != null) agreement.BirthdatePreCertification__c = Date.newInstance(2000, 1, 1);
                if(agreement.BirthdateStudent__c != null) agreement.BirthdateStudent__c = Date.newInstance(2000, 1, 1);
                agreement.SignedAgreementID__c = '';
                agreement.FinalDisclosureID__c = '';
                agreement.CongaUnsignedAgreementID__c = ''; 
                agreement.CongaFinalDisclosureID__c = '';
                agreement.AdobeSignAgreementID__c = '';
                agreement.AdobeSignJavascript__c = '';
                agreement.AdobeSignURL__c = '';
                agreement.CertificationComments__c = '';
            }               
 
            update scope;
        } else if(sObj == 'Program__c'){
            for(Program__c prg:(List<Program__c>) (scope)){
                if(String.isNotEmpty(prg.SchoolEmailNotification1__c)) prg.SchoolEmailNotification1__c = 'masked.email@vemo.com';
                if(String.isNotEmpty(prg.SchoolEmailNotification2__c)) prg.SchoolEmailNotification2__c = 'masked.email@vemo.com';
            }
            update scope;
        } else if(sObj == 'Case'){
            delete scope;        
        } else if(sObj == 'StudentProgramAudit__c'){
            delete scope;        
        } else if(sObj == 'Task'){
            delete scope;        
        } else if(sObj == 'CreditCheck__c'){
            for(CreditCheck__c cc : (List<CreditCheck__c>) (scope)){
                cc.ConsentIPAddress__c = '';
                cc.CreditCheckDeniedReasonText__c = '';  
            }     
            update scope;
        } else if(sObj == 'Notification__c'){
            delete scope;        
        } else if(sObj == 'Bill__c'){
            for(Bill__c bill : (List<Bill__c>) (scope)){
                bill.Email__c = 'maked.email@vemo.com';
                bill.StatementData__c = '';
                bill.Name__c = 'maked name';
            }     
            update scope;
        } else if(sObj == 'Transaction__c'){
            for(Transaction__c tx : (List<Transaction__c>) (scope)){
                tx.NotificationEmail__c = 'maked.email@vemo.com';
            }     
            update scope;       
        } else if(sObj == 'Attachment'){  
            List<Attachment> AttachmentsToDelete = new List<Attachment>();
            for(Attachment attach:(List<Attachment>) (scope)){
                if(attach.parent.type <> 'APXTConga4__Conga_Template__c'){
                    AttachmentsToDelete.add(attach);     
                }
            }
            delete AttachmentsToDelete;       
        } else if(sObj == 'StewardshipACHBatchDetail__c'){  
            delete scope;       
        } else if(sObj == 'StewardshipACHBatch__c'){  
            delete scope;       
        } else if(sObj == 'Note'){  
            delete scope;       
        } else if(sObj == 'OutboundEmail__c'){  
            delete scope;       
        } else if(sObj == 'Account'){
            for(Account acct : (List<Account>) (scope)){
                acct.StewardshipDisbursementDonorGUID__c = '';
                acct.StewardshipPaymentDonorGUID__c = '';
            }     
            update scope;
        } else if(sObj == 'PaymentMethod__c'){
            for(PaymentMethod__c pay : (List<PaymentMethod__c>) (scope)){
                pay.BankAccountNumber__c = '123456789';
                pay.BankName__c = 'Bank Name';
                pay.BankRoutingNumber__c = '123456789';
                pay.CreditCardNumber__c = '';
                pay.Description__c = '';
                pay.StewardshipDisbursementAccountGUID__c = '';
                pay.StewardshipPaymentAccountGUID__c = '';
            }     
            update scope;
        } else if(sObj == 'ClrHouseStudentEnrollment__c'){
            for(ClrHouseStudentEnrollment__c clrHouse:(List<ClrHouseStudentEnrollment__c>) (scope)){
                if(String.isNotEmpty(clrHouse.first_name__c)) clrHouse.first_name__c = 'masked';
                clrHouse.Middle_Initial__c = '';
                if(String.isNotEmpty(clrHouse.last_name__c)) clrHouse.last_name__c = 'masked';
            }
            update scope;
        } else if(sObj == 'Reconciliation__c'){
            delete scope;
        } else if(sObj == 'GenericDocument__c'){
            delete scope;
        } else if(sObj == 'EmploymentHistory__c'){
            for(EmploymentHistory__c emp:(List<EmploymentHistory__c>) (scope)){
                emp.employer__c = 'XYZ Inc.';
            }
            update scope;
        } else if(sObj == 'AccountHistory'){
            delete scope;
        } else if(sObj == 'StudentProgram__History'){
            delete scope;
        } else if(sObj == 'Transaction__History'){
            delete scope;
        } else if(sObj == 'PaymentMethod__History'){
            delete scope;
        } else if(sObj == 'CreditCheck__History'){
            delete scope;
        } else if(sObj == 'AccountNotes'){
            Set<ID> AccountIDs = new Set<ID>();
            for(Account acc:(List<Account>) (scope)){
                AccountIDs.add(acc.id);
            }
            
            List<ContentDocumentLink> contentDocLinks = [SELECT ContentDocumentId,LinkedEntityId 
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN :AccountIDs];
            Set<ID> contentDocIDs = new Set<ID>();
            for(ContentDocumentLink cdl:contentDocLinks){
                contentDocIDs.add(cdl.ContentDocumentId);
            }
            
            if(contentDocIDs.size()>0){
                List<ContentDocument> contentDocs = [Select id From ContentDocument Where Id In :contentDocIDs];
                if(contentDocs<>null && contentDocs.size()>0){
                    delete contentDocs; 
                }
            } 
        } else if(sObj == 'StudentProgramNotes'){
            Set<ID> AgreementIDs = new Set<ID>();
            for(StudentProgram__c agre:(List<StudentProgram__c>) (scope)){
                AgreementIDs.add(agre.id);
            }
            
            List<ContentDocumentLink> contentDocLinks = [SELECT ContentDocumentId,LinkedEntityId 
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN :AgreementIDs];
            Set<ID> contentDocIDs = new Set<ID>();
            for(ContentDocumentLink cdl:contentDocLinks){
                contentDocIDs.add(cdl.ContentDocumentId);
            }
            
            if(contentDocIDs.size()>0){
                List<ContentDocument> contentDocs = [Select id From ContentDocument Where Id In :contentDocIDs];
                if(contentDocs<>null && contentDocs.size()>0){
                    delete contentDocs; 
                }
            } 
        } else if(sObj == 'IncomeVerificationNotes'){
            Set<ID> incVerIDs = new Set<ID>();
            for(IncomeVerification__c iv:(List<IncomeVerification__c>) (scope)){
                incVerIDs.add(iv.id);
            }
            
            List<ContentDocumentLink> contentDocLinks = [SELECT ContentDocumentId,LinkedEntityId 
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN :incVerIDs];
            Set<ID> contentDocIDs = new Set<ID>();
            for(ContentDocumentLink cdl:contentDocLinks){
                contentDocIDs.add(cdl.ContentDocumentId);
            }
            
            if(contentDocIDs.size()>0){
                List<ContentDocument> contentDocs = [Select id From ContentDocument Where Id In :contentDocIDs];
                if(contentDocs<>null && contentDocs.size()>0){
                    delete contentDocs; 
                }
            } 
        } else if(sObj == 'EmploymentHistoryNotes'){
            Set<ID> empHistIDs = new Set<ID>();
            for(EmploymentHistory__c empHist:(List<EmploymentHistory__c>) (scope)){
                empHistIDs.add(empHist.id);
            }
            
            List<ContentDocumentLink> contentDocLinks = [SELECT ContentDocumentId,LinkedEntityId 
                                                         FROM ContentDocumentLink
                                                         WHERE LinkedEntityId IN :empHistIDs];
            Set<ID> contentDocIDs = new Set<ID>();
            for(ContentDocumentLink cdl:contentDocLinks){
                contentDocIDs.add(cdl.ContentDocumentId);
            }
            
            if(contentDocIDs.size()>0){
                List<ContentDocument> contentDocs = [Select id From ContentDocument Where Id In :contentDocIDs];
                if(contentDocs<>null && contentDocs.size()>0){
                    delete contentDocs; 
                }
            } 
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        if(sObj == 'Log__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Contact';
            Database.executeBatch(job);
        } else if(sObj == 'Contact'){
            ApprovalSettings__c approvalSetting = ApprovalSettings__c.getOrgDefaults();
            approvalSetting.AccountNameChangeAllowed__c = false;
            if(approvalSetting.Id != null){
                update approvalSetting;
            }
            
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StudentProgram__c';
            Database.executeBatch(job);
        } else if(sObj == 'StudentProgram__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Program__c';
            Database.executeBatch(job);
        } else if(sObj == 'Program__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Case';
            Database.executeBatch(job);
        } else if(sObj == 'Case'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StudentProgramAudit__c';
            Database.executeBatch(job);
        } else if(sObj == 'StudentProgramAudit__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Task';
            Database.executeBatch(job);
        } else if(sObj == 'Task'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'CreditCheck__c';
            Database.executeBatch(job);
        } else if(sObj == 'CreditCheck__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Notification__c';
            Database.executeBatch(job);
        } else if(sObj == 'Notification__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Bill__c';
            Database.executeBatch(job);
        } else if(sObj == 'Bill__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Transaction__c';
            Database.executeBatch(job);
        } else if(sObj == 'Transaction__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Attachment';
            Database.executeBatch(job);
        } else if(sObj == 'Attachment'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StewardshipACHBatchDetail__c';
            Database.executeBatch(job);
        } else if(sObj == 'StewardshipACHBatchDetail__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StewardshipACHBatch__c';
            Database.executeBatch(job);
        } else if(sObj == 'StewardshipACHBatch__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Note';
            Database.executeBatch(job);
        } else if(sObj == 'Note'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'OutboundEmail__c';
            //Database.executeBatch(job);
            //Not sure, why above line is commented, using below line for code coverage
            if(Test.isRunningTest()){
                Database.executeBatch(job);
            }
        } else if(sObj == 'OutboundEmail__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Account';
            Database.executeBatch(job);
        } else if(sObj == 'Account'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'PaymentMethod__c';
            Database.executeBatch(job);
        } else if(sObj == 'PaymentMethod__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'ClrHouseStudentEnrollment__c';
            Database.executeBatch(job);
        } else if(sObj == 'ClrHouseStudentEnrollment__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Reconciliation__c';
            Database.executeBatch(job);
        } else if(sObj == 'Reconciliation__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'GenericDocument__c';
            Database.executeBatch(job);
        } else if(sObj == 'GenericDocument__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'EmploymentHistory__c';
            Database.executeBatch(job);
        } else if(sObj == 'EmploymentHistory__c'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'AccountHistory';
            Database.executeBatch(job);
        } else if(sObj == 'AccountHistory'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StudentProgram__History';
            Database.executeBatch(job);
        } else if(sObj == 'StudentProgram__History'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'Transaction__History';
            Database.executeBatch(job);
        } else if(sObj == 'Transaction__History'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'PaymentMethod__History';
            Database.executeBatch(job);
        } else if(sObj == 'PaymentMethod__History'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'CreditCheck__History';
            Database.executeBatch(job);
        } else if(sObj == 'CreditCheck__History'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'AccountNotes';
            Database.executeBatch(job);
        } else if(sObj == 'AccountNotes'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'StudentProgramNotes';
            Database.executeBatch(job);
        } else if(sObj == 'StudentProgramNotes'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'IncomeVerificationNotes';
            Database.executeBatch(job);
        } else if(sObj == 'IncomeVerificationNotes'){
            SandboxSetupMaskData job = new SandboxSetupMaskData();
            job.sObj = 'EmploymentHistoryNotes';
            Database.executeBatch(job);
        }  
        
    }
}