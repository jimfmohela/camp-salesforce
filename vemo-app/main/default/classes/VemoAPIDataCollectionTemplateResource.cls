public with sharing class VemoAPIDataCollectionTemplateResource{
     
     public String dataCollectionTemplateID {get;set;}
     public String dataType {get;set;}
     public String label {get;set;}
     public String picklistChoices {get;set;}
     public String programID {get;set;}
     public String required {get;set;}
        
        
     public VemoAPIDataCollectionTemplateResource(DataCollectionTemplateService.Template template){
        this.dataCollectionTemplateID = template.dataCollectionTemplateId;
        this.dataType = template.DataType;
        this.label = template.Label;
        this.picklistChoices = template.picklistChoices; 
        this.programID = template.ProgramId;
        this.required = template.required;
     }
     
}