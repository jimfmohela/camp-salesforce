@isTest
public class EmploymentHistoryQueries2_TEST {

    public static DatabaseUtil dbUtil = new DatabaseUtil();
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEmploymentHistoryMap(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Map<Id, EmploymentHistory2__c> resultEmpHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMap();
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void getEmploymentHistoryMapWithStudentIdTest(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Map<Id, EmploymentHistory2__c> resultEmpHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithStudentId(testStudentAccountMap.keySet());
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void getEmploymentHistoryForCategoryTest(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Map<Id, EmploymentHistory2__c> resultEmpHisMap = EmploymentHistoryQueries2.getEmploymentHistoryForCategory(testStudentAccountMap.keySet());
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }

    static testMethod void testGetEmploymentHistoryMapWithEmploymentHistoryId(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Map<Id, EmploymentHistory2__c> resultEmpHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithEmploymentHistoryId(testEmpHisMap.keySet());
        System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }

    static testMethod void testGetEmploymentHistoryMapWithStudentId(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Map<Id, EmploymentHistory2__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory2(1, employerMap);
        Test.startTest();
        Map<Id, EmploymentHistory2__c> resultEmpHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithStudentIdAndOtherParams(
                                              testStudentAccountMap.keySet(),'false','true','','', 'false', 'false', 'false', 'false');
        Map<Id, EmploymentHistory2__c> resultEmpHisMap1 = EmploymentHistoryQueries2.getEmploymentHistoryMapWithStudentIdAndOtherParams(
                                            testStudentAccountMap.keySet(),'true','false','','', 'true', 'false', 'false', 'false');
        //System.assertEquals(testEmpHisMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
}