public class ReportStudentWeeklyNotes{
    
    transient public Date startDate{get;set;}
    transient public Date endDate{get;set;}
    //transient public List< ContentVersion > notes;
    transient public List< reportDataWrapper > reportData{get;set;}
    
    public ReportStudentWeeklyNotes(){
    
        if(startDate == null)
            startDate = date.Today().addDays(-7);
        if(endDate == null)
           endDate = date.Today().addDays(1);
           
       
       //reportData = new List<reportDataWrapper>();
       
           
       //runReport();      
        
    }
    public void runReport(){
        system.debug(startDate+' : '+endDate);
        reportData = new List<reportDataWrapper>();
        Map<Id,Id> cdIdObjId = new Map<Id,Id>();   // content document Id & Linked Entity Id Map
        Map<Id,ContentVersion> cdlIdContentVersion = new Map<Id,ContentVersion>();  // Content Document Id & Content Version Map
        Map<Id,List<Id>> objIdListCdId = new Map<Id,List<Id>>();  // Linked Entity Id & List of content document Id
        //Map<Id,ContentVersion> cdlIdContentVersion = Map<Id,ContentVersion>();  // Content Document Id & Content Version Map
                     
        Set<Id> cdlNew = new Set<Id>();
        for(ContentVersion cv: [SELECT id,Title, TextPreview, ContentDocumentId,lastmodifieddate,CreatedBy.Name,createdDate,LastModifiedBy.Name
                                FROM ContentVersion 
                                WHERE IsDeleted = False 
                                AND FileType = 'SNOTE' 
                                AND lastmodifieddate >= :startDate AND lastmodifieddate < :endDate
                                AND (TextPreview != null  OR TextPreview != '')]){
            cdlNew.add(cv.ContentDocumentId);
        }
        
        List<Id> cdl = new List<Id>();
        if(cdlNew.size()>0){
            for(ContentDocumentLink links : [SELECT LinkedEntityId, ContentDocumentId
                                                FROM ContentDocumentLink
                                                WHERE ContentDocumentId IN : cdlNew]){
                system.debug(links);
                if(string.valueof(links.LinkedEntityId).startswith('001')){
                    cdl.add(links.ContentDocumentId);
                    //cdIdObjId.put(links.ContentDocumentId,links.LinkedEntityID);
                    
                    if(!objIdListCdId.containsKey(links.LinkedEntityID))
                        objIdListCdId.put(links.LinkedEntityID,new List<Id>());
                    
                    objIdListCdId.get(links.LinkedEntityID).add(links.ContentDocumentId);
                }
            }
        }
        //system.debug('=='+cdl);
        
        //notes = new List< ContentVersion >();
        if ( !cdl.isEmpty() ) {        
            for(ContentVersion cv: [SELECT id,Title, TextPreview, ContentDocumentId,lastmodifieddate,CreatedBy.Name,createdDate,LastModifiedBy.Name
                                    FROM ContentVersion 
                                    WHERE ContentDocumentId IN: cdl AND IsDeleted = False 
                                    AND FileType = 'SNOTE' 
                                    AND lastmodifieddate >= :startDate AND lastmodifieddate <= :endDate
                                    AND (TextPreview != null  OR TextPreview != '')]){
                if(!cdlIdContentVersion.containsKey(cv.ContentDocumentId))
                    cdlIdContentVersion.put(cv.ContentDocumentId,new ContentVersion());
                    
                cdlIdContentVersion.put(cv.ContentDocumentId,cv);
            }
        }
        
        for(Account stud: [select id,name,PrimarySchool__pr.Name,VemoAccountNumber__c from account where recordType.name='Student' AND Id IN: objIdListCdId.keyset()]){
            if(objIdListCdId.containsKey(stud.Id)){
                reportDataWrapper rdw = new reportDataWrapper();
                rdw.studentId = stud.Id;
                rdw.studentName = stud.Name;
                rdw.schoolName = stud.PrimarySchool__pr.Name;
                rdw.vemoAccountNumber = stud.VemoAccountNumber__c;
                rdw.notesList = new List<ContentVersion>();
                for(Id cdlId: objIdListCdId.get(stud.Id)){
                    if(cdlIdContentVersion.containsKey(cdlId))
                        rdw.notesList.add(cdlIdContentVersion.get(cdlId));
                }
                if(rdw.notesList.size()>0)
                    reportData.add(rdw);
            }
         }
        
    }
    
    ////////////////////////////////////////
     ///Call the export VF Page
     ////////////////////////////////////////
     public PageReference exportToExcel(){
        PageReference pg = new PageReference('/apex/ReportStudentWeeklyNotesXLS');
        pg.setRedirect(false);
        return pg;
    }
    
    public class reportDataWrapper{
        public Id studentId{get;set;}
        public string studentName {get;set;}
        public string schoolName {get;set;}
        public string vemoAccountNumber {get;set;}
        public List<ContentVersion> notesList{get;set;}
        
        public reportDataWrapper(){
            this.notesList = new List<ContentVersion>();
        }
    }
}