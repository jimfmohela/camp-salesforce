/////////////////////////////////////////////////////////////////////////
// Class: EmploymentHistoryQueries2
// 
// Description: 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-06-05   Rini Gupta  Created 
/////////////////////////////////////////////////////////////////////////
public class EmploymentHistoryQueries2 {
    public static Map<String, Object> filterCriteria = new Map<String, Object>();
    public static void clearFilterCriteria(){
        filterCriteria.clear();
    }

    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMap(){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();        
        query += ' WHERE Id != null';
        query += ' AND Employer__c !=\'not mentioned\' ';
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;       
    }
    
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMapWithEmploymentHistoryId(Set<ID> empHisIDs){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(empHisIDs);
        query += ' AND Employer__c !=\'not mentioned\' ';
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;
    }
    
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMapWithEmployerId(Set<ID> empIDs){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query += ' WHERE EmployerMD__c IN ' + DatabaseUtil.inSetStringBuilder(empIDs);
        query += ' AND Employer__c !=\'not mentioned\' ';
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;
    }
    
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMapWithStudentId(Set<Id> studentIds){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query +=' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds);  
        query +=' AND Employer__c !=\'not mentioned\' '; 
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;       
    }
    
    //Use for Creation of Complete Employment Details 
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMapWithStudentIdForCED(Set<Id> studentIds){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query +=' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds);
        query +=' AND Employer__c !=\'not mentioned\' ';   
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' Order By EffectiveDate__c DESC nulls Last ';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;       
    }
    
    //OpenEmploymentHistory --> where end date is null
    public static Map<ID, EmploymentHistory2__c> getOpenEmploymentHistoryMapWithStudentId(Set<Id> studentIds){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query +=' WHERE EmploymentEndDate__c = null and Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds); 
        query +=' AND Employer__c !=\'not mentioned\' ';  
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;       
    }
    
    //Use for filling category
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryForCategory(Set<Id> studentIds){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();
        query +=' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds);
        query +=' and Category__c != null ';   
        query +=' AND Employer__c !=\'not mentioned\' ';
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query +=' Order By CreatedDate DESC ';
        query += buildFilterString();       
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        return empHisMap;  
    }
    
    public static Map<ID, EmploymentHistory2__c> getEmploymentHistoryMapWithStudentIdAndOtherParams(
                                                    Set<ID> studentIds, String recentRecords, String verifiedRecords, String employer, String jobTitle, 
                                                    String groupBy, String currentParam, String pastParam, String uploadedDoc
                                                ){
        Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
        String query = generateSOQLSelect();       
            
        query +=' WHERE Student__c IN ' + DatabaseUtil.inSetStringBuilder(studentIds);        
        query +=' AND Employer__c !=\'not mentioned\' ';
        
        if(verifiedRecords != null){
            if(verifiedRecords.toLowerCase() == 'true'){
                query += ' AND Verified__c = true';
            }
            else if(verifiedRecords.toLowerCase() == 'false'){
                query += ' AND Verified__c = false';
            }
        }
        if(employer != null && !String.isBlank(employer)){
            query += ' AND Employer__c =\''+employer+'\'';
        }
        if(jobTitle != null && !String.isBlank(jobTitle)){
            query += ' AND JobTitle__c =\''+jobTitle+'\'';
        }
        /*
        if(verifiedRecords != null){
            if(verifiedRecords == 'true')
                query += ' AND Verified__c = true';
            else
                query += ' AND Verified__c = false';
            
            if(employer != ''){
                query += ' AND Employer__c =\''+employer+'\'';
                if(jobTitle != ''){
                    query += ' AND JobTitle__c =\''+jobTitle+'\'';
                }
            }
            else{
                if(jobTitle != ''){
                    query += ' AND JobTitle__c =\''+jobTitle+'\'';
                }
            }
        }
        else{
            if(employer != ''){
                query += ' AND Employer__c =\''+employer+'\'';
                if(jobTitle != ''){
                    query += ' AND JobTitle__c =\''+jobTitle+'\'';
                }
            }
            else{
                if(jobTitle != ''){
                    query += ' AND JobTitle__c =\''+jobTitle+'\'';
                }
            }
        }
        */
        if(currentParam != null && currentParam.toLowerCase() == 'true'){
            //According to ticket CP-422, we need all Employment History record related to a Student
            //query +=' AND (EmploymentEndDate__c = null OR EmploymentEndDate__c >= today OR Verified__c = false)';
        }
        if(pastParam != null && pastParam.toLowerCase() == 'true'){
            //According to ticket CP-422, we need all Employment History record related to a Student
            //query +=' AND EmploymentEndDate__c < today AND Verified__c = true';
        }
        if(uploadedDoc != null){
            //if(uploadedDoc.toLowerCase() == 'true'){
            //    query +=' AND DocUploaded__c = true';
            //}
            //else if(uploadedDoc.toLowerCase() == 'false'){
            //    query +=' AND DocUploaded__c = false';
            //}
        }
            
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and EmployerMD__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID);            
        }
        query += buildFilterString();
        
        if(recentRecords == 'true' && groupBy != 'true'){
            query += ' order by createddate desc limit 1';
        }
        if(recentRecords != 'true' && groupBy == 'true'){
            query += ' order by employer__c '+ generateLIMITStatement();
        }
        if(recentRecords != 'true' && groupBy != 'true'){
            query += ' '+ generateLIMITStatement();
        }            
        DatabaseUtil db = new DatabaseUtil();
        
        if(!(recentRecords == 'true' && groupBy == 'true')){
            empHisMap = new Map<ID, EmploymentHistory2__c>((List<EmploymentHistory2__c>)db.query(query));
        }
        else{
            query += ' order by createddate '+ generateLIMITStatement();
            List<EmploymentHistory2__c> empHistoryList = db.query(query);
            Map<String,EmploymentHistory2__c> employerName_EmploymentHistoryMap = new Map<String,EmploymentHistory2__c>();
            for(EmploymentHistory2__c emp : empHistoryList){
                employerName_EmploymentHistoryMap.put(emp.Employer__c,emp);
            }
            for(EmploymentHistory2__c emp : employerName_EmploymentHistoryMap.values()){
                empHisMap.put(emp.Id,emp);
            }
        }
        return empHisMap;
        
    }

    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
    
    private static String buildFilterString(){
        String filterStr = '';
        if(filterCriteria.size()>0){
            for(String filter : filterCriteria.keyset()){
                filterStr += ' and ' +filter+'=\''+String.valueOf(filterCriteria.get(filter) +  '\' ');
            }           
        }
        return filterStr;
    }

    public static String generateSOQLSelect(){
        String soql;
        soql = 'SELECT ' + getEmploymentHistoryFieldNames() + ' FROM EmploymentHistory2__c ';
        return soql;
    }

    private static String getEmploymentHistoryFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Name, ';
        fieldNames += 'Category__c, ';
        //fieldNames += 'CreatedDate, ';
        //fieldNames += 'dateReported1__c, '; 
        fieldNames += 'DateReported__c, '; 
        //fieldNames += 'DocUploaded__c, ';
        fieldNames += 'EffectiveDate__c, ';
        fieldNames += 'Employer__c, ';
        fieldNames += 'EmploymentEndDate__c, ';
        fieldNames += 'EmploymentStartDate__c, ';
        fieldNames += 'EventType__c, ';
        //fieldNames += 'ExternalCompanyID__c, ';
        //fieldNames += 'LastModifiedDate, ';
        fieldNames += 'Student__c, ';
        //fieldNames += 'Verified__c, ';
        //fieldNames += 'EmploymentSummary__c, ';
        fieldNames += 'JobTitle__c, ';
        fieldNames += 'HoursPerWeek__c, ';
        fieldNames += 'HourlyRate__c, ';
        //fieldNames += 'YearlySalary__c, ';
        fieldNames += 'PaymentSchedule__c, ';
        fieldNames += 'BonusAmount__c, ';
        //fieldNames += 'BonusFrequency__c, ';
        //fieldNames += 'CommissionAmount__c, ';
        //fieldNames += 'CommissionFrequency__c, ';
        //fieldNames += 'NoLongerEmployedHere__c, ';
        //fieldNames += 'SalaryEffectiveDate__c, ';
        //fieldNames += 'HourlyRateEffectiveDate__c, ';
        //fieldNames += 'UnemploymentEffectiveDate__c, ';
        //fieldNames += 'BonusEffectiveDate__c, ';
        //fieldNames += 'CommissionEffectiveDate__c, ';
        fieldNames += 'Status__c, ';
        //fieldNames += 'TipAmount__c, ';
        fieldNames += 'MonthlyIncomeTotal__c, ';
        fieldNames += 'Type__c, ';
        //fieldNames += 'LookForEmployment__c, ';
        fieldNames += 'JobLocationCity__c, ';
        fieldNames += 'JobLocationState__c, '; 
        fieldNames += 'JobLocationCountry__c, ';
        fieldNames += 'AverageOvertimeHoursPerWeek__c, ';
        fieldNames += 'OvertimeHourlyRate__c, ';
        fieldNames += 'PaidForHolidays__c, ';
        fieldNames += 'MonthlyIncomeType__c, ';
        fieldNames += 'MonthlyIncomeReported__c, ';
        fieldNames += 'AdditionalIncomeType__c, ';
        fieldNames += 'AbnormalIncomeExplanation__c, '; 
        fieldNames += 'EmployerMD__c, ';
        fieldNames += 'MonthlyIncomeAdjustments__c, ';
        fieldNames += 'EmployerMD__r.Name, ';
        fieldNames += 'CreatedDate, ';
        fieldNames += 'LastModifiedDate ';
        return fieldNames;
    }
}