@isTest
public with sharing class GenericDocumentViewApexCTRL_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void getGenericDocumentList_ForReconcillation_Test(){
        Map<ID, Reconciliation__c> reconciliationMap = TestDataFactory.createAndInsertReconciliation(1);
        Map<ID, ReconciliationDetail__c> reconciliationDetailMap = TestDataFactory.createAndInsertReconciliationDetail(1, reconciliationMap);
        Map<ID, GenericDocument__c> genDocMap = TestDataFactory.createAndInsertGenericDocument(TestUtil.TEST_THROTTLE);
        Map<Id, GenericDocument__c> newGenDocMap = GenericDocumentQueries.getGenericDocumentMapByID(genDocMap.keySet());
        for(GenericDocument__c genDoc : newGenDocMap.values()){
            genDoc.ParentID__c = reconciliationMap.values()[0].Id;
        }
        //update newGenDocMap.values();
        dbUtil.updateRecords(newGenDocMap.values());
        Test.startTest();
        GenericDocumentViewApexCTRL.getGenericDocumentList(reconciliationMap.values()[0].Id);
        Test.stopTest();
    }
}