/*
LogBatchScheduled job = new LogBatchScheduled();
job.job = LogBatch.JobType.PURGE;
String cronStr1 = '0 0 9 * * ? *';
System.schedule('Auto Purge Daily', cronStr2, job);
*/
public class LogBatchScheduled implements Schedulable {
    public LogBatch.JobType job {get;set;}
    public String query {get;set;}
    public void execute(SchedulableContext sc) {
        LogBatch batchJob = new LogBatch();
        batchJob.job = job;
        batchJob.query = this.query;
        Database.executeBatch(batchJob);
    }
}