public with sharing class AcademicEnrollmentBatch implements Database.Batchable<sObject>{
    public enum JobType {RECALCULATE_ACADEMICENROLLMENT} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    Set<Id> academicEnrollmentIds = new Set<Id>();
    
    public AcademicEnrollmentBatch(set<Id> aeIds) {
        academicEnrollmentIds = aeIds;    
    }
    public AcademicEnrollmentBatch(String query) {
        this.query = query;
    }   
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('AcademicEnrollmentBatch.start()');
        if(job == JobType.RECALCULATE_ACADEMICENROLLMENT){
            if(String.isEmpty(this.query)){
                query = 'SELECT id from AcademicEnrollment__c WHERE ID IN '  + DatabaseUtil.inSetStringBuilder(academicEnrollmentIds);
            }
        }
        System.debug('job:'+job);
        System.debug('query:'+query);
        LogService.writeLogs();
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('AcademicEnrollmentBatch.execute()');
        System.debug(job);
        
        if(job == JobType.RECALCULATE_ACADEMICENROLLMENT){
            
            Set<ID> scopeIDs = new Set<ID>();
            for(sObject sobj : scope){
                scopeIDs.add(sobj.id);
            }
            Map<Id,StudentProgram__c> studentProgramsMap = StudentProgramQueries.getStudentProgramMapWithAcademicEnrollmentID(scopeIDs);
            
            Map<Id, StudentProgram__c> aeIdNSPMap = new Map<Id, StudentProgram__c>(); // Academic Enrollment Id -> Most recent SP
            
            if(studentProgramsMap != null && studentProgramsMap.size()>0){
                for(StudentProgram__c sp: studentProgramsMap.values()){
                    if(sp.AcademicEnrollment__c != null && !(aeIdNSPMap.get(sp.AcademicEnrollment__c) !=null)){
                        aeIdNSPMap.put(sp.AcademicEnrollment__c,sp);
                    }  
                }
            }
            
            Map<Id, AcademicEnrollment__c> AcademicEnrollmentById = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithId(scopeIDs);
            
            for(AcademicEnrollment__c ae: AcademicEnrollmentById.values()){
                if(aeIdNSPMap != null && aeIdNSPMap.get(ae.id) != null){
                    StudentProgram__c sp  = new StudentProgram__c();
                    sp = aeIdNSPMap.get(ae.id);
                    
                    /*if(sp.EnrollmentStatusPostCertification__c != null) ae.status__c = sp.EnrollmentStatusPostCertification__c;
                    else if(sp.EnrollmentStatusPreCertification__c != null) ae.status__c = sp.EnrollmentStatusPreCertification__c;
                    else if(sp.EnrollmentStatusStudent__c != null) ae.status__c = sp.EnrollmentStatusStudent__c;*/
                    
                    if(sp.EnrollmentStatusPostCertification__c != null) ae.AcademicEnrollmentStatus__c = sp.EnrollmentStatusPostCertification__c;
                    else if(sp.EnrollmentStatusPreCertification__c != null) ae.AcademicEnrollmentStatus__c = sp.EnrollmentStatusPreCertification__c;
                    else if(sp.EnrollmentStatusStudent__c != null) ae.AcademicEnrollmentStatus__c = sp.EnrollmentStatusStudent__c;
                    
                    if(sp.MajorPostCertification__c != null) ae.SchoolProgramOfStudy__c = sp.MajorPostCertification__c;
                    else if(sp.MajorPreCertification__c != null) ae.SchoolProgramOfStudy__c = sp.MajorPreCertification__c;
                    else if(sp.MajorStudent__c != null) ae.SchoolProgramOfStudy__c = sp.MajorStudent__c;
                    
                    if(sp.ExpectedGraduationDate__c != null) ae.GraduationDate__c = sp.ExpectedGraduationDate__c;
                    
                    if(sp.GradeLevelPostCertification__c != null) ae.Grade__c = sp.GradeLevelPostCertification__c;
                    else if(sp.GradeLevelPreCertification__c != null) ae.Grade__c = sp.GradeLevelPreCertification__c;
                    else if(sp.GradeLevelStudent__c != null) ae.Grade__c = sp.GradeLevelStudent__c;
                    
                    if(sp.LastDateofAttendance__c != null) ae.LastDateofAttendance__c = sp.LastDateofAttendance__c ;
                
                    ae.update__c = false;
                }   
            }
            new UpdateAcademicEnrollments().updateAErecords(AcademicEnrollmentById.values());
            //update AcademicEnrollmentById.values();
        }
        
        
        LogService.writeLogs(); 
    }
    
    private without sharing class UpdateAcademicEnrollments{
        public void updateAErecords(List<AcademicEnrollment__c> aeListtoUpdate) {
            update aeListtoUpdate;
        }
    }
    public void finish(Database.BatchableContext BC) {
        System.debug('AcademicEnrollmentBatch.finish()');
        LogService.writeLogs();
    }
}