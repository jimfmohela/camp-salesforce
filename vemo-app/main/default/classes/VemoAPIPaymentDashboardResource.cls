public with sharing class VemoAPIPaymentDashboardResource implements VemoAPI.ResourceHandler {
  public static Object handleAPI(VemoAPI.APIInfo api){
    if((api.version == 'v2') && (api.method == 'GET')){
      return handleGetV2(api);
    }
    //if((api.version == 'v1') && (api.method == 'POST')){
    //  return handlePostV1(api);
    //}
    //if((api.version == 'v1') && (api.method == 'PUT')){
    //  return handlePutV1(api);
    //}  
    //if((api.version == 'v1') && (api.method == 'DELETE')){
    //  return handleDeleteV1(api);
    //}      
    //throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    return null;
  }
  
  public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
      String schoolIDParam = api.params.get('schoolID');
      PaymentDashboardResourceOutputV2 result = new PaymentDashboardResourceOutputV2();
      List<PaymentDashboardResourceOutputV2> results = new List<PaymentDashboardResourceOutputV2>();
      
      if(schoolIDParam != null){
          Set<String> schoolIds = new Set<String>{schoolIDParam};
          //Map<ID, Account> studentMap = AccountQueries.getStudentMapWithSchoolID(schoolIds); 
          //Decimal TotalStudentCount = studentMap.size(); 
          
          
          /****** Month To Date Payments *******/
          List<AggregateResult> monthToDatePaymentList = [Select COUNT(ID) PaymentCount, SUM(AmountPosted__c) Amount, DAY_IN_MONTH(DatePosted__c) Day from PaymentInstruction__c where Status__c = 'Cleared' AND Student__r.PrimarySchool__pc =: schoolIDParam AND DatePosted__c = THIS_YEAR AND DatePosted__c = THIS_MONTH Group By DatePosted__c Order by DatePosted__c ASC];
          result.monthToDatePaymentList = new List<VemoAPIPaymentDashboardResource.MonthToDatePayment>();
          for(AggregateResult mtd : monthToDatePaymentList){
              result.monthToDatePaymentList.add(new MonthToDatePayment(double.valueOf(mtd.get('Amount')), integer.valueOf(mtd.get('PaymentCount')), integer.valueOf(mtd.get('Day'))));     
          }
          
          /****** Year To Date Payments *******/
          List<AggregateResult> yearToDatePaymentList = [Select COUNT(ID) PaymentCount, SUM(AmountPosted__c) Amount, CALENDAR_MONTH(DatePosted__c) Month from PaymentInstruction__c where Status__c = 'Cleared' AND Student__r.PrimarySchool__pc =: schoolIDParam AND DatePosted__c = THIS_YEAR Group By CALENDAR_MONTH(DatePosted__c) Order by CALENDAR_MONTH(DatePosted__c) ASC];
          result.yearToDatePaymentList = new List<VemoAPIPaymentDashboardResource.YearToDatePayment>();
          for(AggregateResult ytd : yearToDatePaymentList){
              result.yearToDatePaymentList.add(new YearToDatePayment(double.valueOf(ytd.get('Amount')), integer.valueOf(ytd.get('PaymentCount')), integer.valueOf(ytd.get('Month'))));     
          }
          
          /****** All Payments *******/
          //Integer startYear = Date.today().year()-4;
          List<AggregateResult> allPaymentList = [Select COUNT(ID) PaymentCount, SUM(AmountPosted__c) Amount, CALENDAR_YEAR(DatePosted__c) Year from PaymentInstruction__c where Status__c = 'Cleared' AND Student__r.PrimarySchool__pc =: schoolIDParam AND DatePosted__c != null Group By CALENDAR_YEAR(DatePosted__c) Order by CALENDAR_YEAR(DatePosted__c) ASC];
          result.allPaymentList = new List<VemoAPIPaymentDashboardResource.AllPayment>();
          for(AggregateResult all : allPaymentList){
              result.allPaymentList.add(new allPayment(double.valueOf(all.get('Amount')), integer.valueOf(all.get('PaymentCount')), integer.valueOf(all.get('Year'))));     
          }
          
          /****** Payment Type *******/
          try{
          String RecordTypeId = (String) GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student');
          List<AggregateResult> totalStudents = [Select COUNT(ID) Total from Account where RecordTypeId =: RecordTypeId And PrimarySchool__pc =: schoolIDParam]; 
          Decimal totalStudentCount = Double.valueOf(totalStudents[0].get('Total'));
          System.debug('Total Students---'+ totalStudentCount );
          
          List<AggregateResult> studentsByPaymentType = [Select COUNT(ID) StudentCount, Autopayment__pc AutoPay from Account where RecordTypeId =: RecordTypeId And PrimarySchool__pc =: schoolIDParam Group by AutoPayment__pc]; 
          result.paymentTypeList = new List<VemoAPIPaymentDashboardResource.PaymentType>();
          
          Integer autoPayStudentCount = 0; 
          Decimal autoPayStudentPercentage = 0;
          Integer otherPayStudentCount = 0;
          Decimal otherPayStudentPercentage = 0;
          
          for(AggregateResult student: studentsByPaymentType){
              if(String.valueOf(student.get('AutoPay')) == 'true'){
                  autoPayStudentCount = Integer.valueOf(student.get('StudentCount'));
                  System.debug('AutoPay Student Count--'+ autoPayStudentCount );
                  
                  autoPayStudentPercentage = ((autoPayStudentCount*100)/totalStudentCount).setScale(2);  
                  System.debug('AutoPay Student Percentage--'+ autoPayStudentPercentage);
                  
                  //result.paymentTypeList.add(new PaymentType('AutoPay', autoPayStudentCount, autoPayStudentPercentage));
              }
              else{
                  otherPayStudentCount = Integer.valueOf(student.get('StudentCount'));
                  otherPayStudentPercentage = ((otherPayStudentCount*100)/totalStudentCount).setScale(2); 
                  
                  //result.paymentTypeList.add(new PaymentType('Other', otherPayStudentCount, otherPayStudentPercentage)); 
              }    
          } 
          result.paymentTypeList.add(new PaymentType('AutoPay', autoPayStudentCount, autoPayStudentPercentage));
          result.paymentTypeList.add(new PaymentType('Other', otherPayStudentCount, otherPayStudentPercentage)); 
          
          /****** Payment Status *******/
          Set<String> paymentStatuses = new Set<String>{'Payment', 'Internship', 'Early Pay'};
          Set<String> otherStatuses = new Set<String>{'Grace', 'Deferment'};
          
          List<AggregateResult> totalStudentsInPaymentStatuses = [Select COUNT_DISTINCT(Student__c) StudentCount from StudentProgram__c where Program__r.School__c =:schoolIDParam AND Deleted__c = false AND (Status__c IN: paymentStatuses OR Status__c IN: otherStatuses) /*Group By Status__c*/];
          Decimal totalStudentCountInPaymentStatuses = Double.valueOf(totalStudentsInPaymentStatuses[0].get('StudentCount')); 
          
          List<AggregateResult> studentsByPaymentStatus = [Select COUNT_DISTINCT(Student__c) StudentCount, Status__c Status from StudentProgram__c where Program__r.School__c =:schoolIDParam AND Deleted__c = false AND (Status__c IN: paymentStatuses OR Status__c IN: otherStatuses) Group By Status__c];
          result.paymentStatusList = new List<VemoAPIPaymentDashboardResource.PaymentStatus>();
          
          Integer PaymentStudentCount = 0;
          Decimal PaymentStudentPercentage = 0;//((studentCount*100)/totalStudentCount).setScale(2);
          Boolean Deferment = false;
          Boolean Grace = false;
          
          for(AggregateResult stud: studentsByPaymentStatus){
              String status = String.valueOf(stud.get('Status'));
              if(paymentStatuses.contains(status)){
                  PaymentStudentCount += Integer.valueOf(stud.get('StudentCount'));
                  //PaymentStudentPercentage = ((PaymentStudentCount*100)/totalStudentCount).setScale(2);
              }
              else{
                  Integer OtherStudentCount = Integer.valueOf(stud.get('StudentCount'));
                  Decimal OtherStudentPercentage = ((OtherStudentCount*100)/totalStudentCountInPaymentStatuses).setScale(2);
                  if(String.valueOf(stud.get('Status')) == 'Deferment'){
                      Deferment = true;
                  }    
                  else if(String.valueOf(stud.get('Status')) == 'Grace'){
                      Grace = true; 
                  }    
                  
                  result.paymentStatusList.add(new PaymentStatus(String.valueOf(stud.get('Status')), OtherStudentCount, OtherStudentPercentage)); 
              }    
          }
          if(!Grace){
              result.paymentStatusList.add(new PaymentStatus('Grace', 0, 0.0)); 
          }    
          if(!Deferment){
              result.paymentStatusList.add(new PaymentStatus('Deferment', 0, 0.0));
          }     
          
          PaymentStudentPercentage = ((PaymentStudentCount*100)/totalStudentCountInPaymentStatuses).setScale(2);
          result.paymentStatusList.add(new PaymentStatus('Payment', PaymentStudentCount, PaymentStudentPercentage)); 
          
          /****** Pipeline ******/
          //Integer startMonth = Date.Today().month();
          //Integer endMonth = Date.Today().month()+6;
          List<AggregateResult> paymentPipelineData = [Select COUNT(ID) AgreementCount, GracePeriodEndDate__c GracePeriodEndDate, CALENDAR_MONTH(NextPaymentDueDate__c) Month from StudentProgram__c where Status__c = 'Grace' AND Program__r.School__c =: schoolIDParam /*'0013600000sQG4Z'*/ AND GracePeriodEndDate__c != null AND Deleted__c = false/*AND GracePeriodEndDate__c = THIS_YEAR*/ AND GracePeriodEndDate__c >= THIS_MONTH AND GracePeriodEndDate__c <= NEXT_N_MONTHS:6  AND NextPaymentDueDate__c != null Group by GracePeriodEndDate__c, CALENDAR_MONTH(NextPaymentDueDate__c) Order by GracePeriodEndDate__c/*, CALENDAR_MONTH(FirstPaymentDueDate__c)*/];
          result.paymentPipeline = new List<VemoAPIPaymentDashboardResource.PaymentPipeline>();
          for(AggregateResult payment: paymentPipelineData){
              String MonthString = getMonth(Integer.valueOf(payment.get('Month')));
              result.paymentPipeline.add(new PaymentPipeline(Date.valueOf(payment.get('GracePeriodEndDate')), MonthString, Integer.valueOf(payment.get('AgreementCount'))));
          }
          }catch(exception e){
              System.debug('Exception: '+e.getMessage());
          }
      } 
      //result.lastUpdateDateTimeStamp = DateTime.now();
      results.add(result);
      //system.debug('limit queries--'+Limits.getQueryRows());
      return (new VemoAPI.ResultResponse(results, results.size()));
      //return null;
  }
  
  public class PaymentDashboardResourceOutputV2{
      public List<MonthToDatePayment> monthToDatePaymentList {get;set;}
      public List<YearToDatePayment> yearToDatePaymentList {get;set;}
      public List<AllPayment> allPaymentList {get;set;}
      public List<PaymentType> paymentTypeList {get;set;}
      public List<PaymentStatus> paymentStatusList {get;set;}
      public List<PaymentPipeline> paymentPipeline {get;set;}
      public DateTime lastUpdatedDateTimeStamp {get;set;}
      //public Integer autoPayStudentCount {get;set;}
      //public Decimal autoPayStudentPercentage {get;set;}
      //public Integer otherPayStudentCount {get;set;}
      //public Decimal otherPayStudentPercentage {get;set;}
      
      public PaymentDashboardResourceOutputV2(){
          this.lastUpdatedDateTimeStamp = DateTime.now();
      }
  }
  
  public class MonthToDatePayment{
      public Decimal amount {get;set;}
      public Integer noOfPayments {get;set;}
      public Integer day {get;set;}
      
      public MonthToDatePayment(Decimal amount, Integer noOfPayments, Integer day){
          this.amount = amount;
          this.noOfPayments = noOfPayments;
          this.day = day;
      }
  }
  
  public class YearToDatePayment{
      public Decimal amount {get;set;}
      public Integer noOfPayments {get;set;}
      public Integer month {get;set;}
      
      public YearToDatePayment(Decimal amount, Integer noOfPayments, Integer month){
          this.amount = amount;
          this.noOfPayments = noOfPayments;
          this.month = month;
      }
  }
  
  public class allPayment{
      public Decimal amount {get;set;}
      public Integer noOfPayments {get;set;}
      public Integer year {get;set;}
      
      public allPayment(Decimal amount, Integer noOfPayments, Integer year){
          this.amount = amount;
          this.noOfPayments = noOfPayments;
          this.year = year;
      }
  }
  
  public class PaymentType{
      public String paymentType {get;set;}
      public Integer noOfStudents {get;set;}
      public Decimal percentageOfStudents {get;set;}
      
      public PaymentType(String paymentType, Integer noOfStudents, Decimal percentageOfStudents){
          this.paymentType = paymentType;
          this.noOfStudents = noOfStudents; 
          this.percentageOfStudents = percentageOfStudents; 
      }
  }
  
  public class PaymentStatus{
      public String paymentStatus {get;set;}
      public Integer noOfStudents {get;set;}
      public Decimal percentageOfStudents {get;set;}
      
      public PaymentStatus(String paymentStatus, Integer noOfStudents, Decimal percentageOfStudents){
          this.paymentStatus = paymentStatus;
          this.noOfStudents = noOfStudents; 
          this.percentageOfStudents = percentageOfStudents;     
      }
  }
  
  public class PaymentPipeline{
      public Date gracePeriodEndDate {get;set;}
      public String firstPayment {get;set;}
      public Integer noOfAgreements {get;set;}
      
      public PaymentPipeline(Date gracePeriodEndDate, String firstPayment, Integer noOfAgreements){
          this.gracePeriodEndDate = gracePeriodEndDate;
          this.firstPayment = firstPayment;
          this.noOfAgreements = noOfAgreements;
      }
  }
  
  public static String getMonth(Integer month){
      String monthName = '';
      if(month == 1)
          return 'January';
      else if(month == 2)
          return 'February';
      else if(month == 3)
          return 'March';
      else if(month == 4)
          return 'April';
      else if(month == 5)
          return 'May';
      else if(month == 6)
          return 'June';
      else if(month == 7)
          return 'July';
      else if(month == 8)
          return 'August'; 
      else if(month == 9)
          return 'September'; 
      else if(month == 10)
          return 'October';
      else if(month == 11)
          return 'November';
      else if(month == 12)
          return 'December'; 
          
      return null; 
              
  } 
}