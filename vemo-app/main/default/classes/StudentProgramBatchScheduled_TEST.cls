@isTest
public with sharing class StudentProgramBatchScheduled_TEST {
	
    @isTest
    static void delinquencySchedule_Test(){
        Test.startTest();
        StudentProgramBatchScheduled job = new StudentProgramBatchScheduled();
        job.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('delinquencySchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
    
    @isTest
    static void purgerDeletedSchedule_Test(){
        Test.startTest();
        StudentProgramBatchScheduled job = new StudentProgramBatchScheduled();
        job.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('purgerDeletedSchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
    
    @isTest
    static void monthEndAuditSchedule_Test(){
        Test.startTest();
        StudentProgramBatchScheduled job = new StudentProgramBatchScheduled();
        job.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('monthEndAuditSchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
    
    @isTest
    static void contractAssessmentSchedule_Test(){
        Test.startTest();
        StudentProgramBatchScheduled job = new StudentProgramBatchScheduled();
        job.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('contractAssessmentSchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
    
    @isTest
    static void generateFinalDisclosureSchedule_Test(){
        Test.startTest();
        StudentProgramBatchScheduled job = new StudentProgramBatchScheduled();
        job.jobType = StudentProgramBatch.JobType.PURGE_DELETED;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('generateFinalDisclosureSchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
}