public with sharing class Report_ApplicationController{
    public List<SchoolWrapper> schools {get;set;}
    private Set<String> schoolSet;
    //transient public List<reportDataWrapper> reportData {get;set;}
    transient public List<AgreementWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public Report_ApplicationController(){
        //selectedSchool = '';
        //reportData = new List<reportDataWrapper>();    
        reportData = new List<AgreementWrapper>();
        schools = new List<SchoolWrapper>();
        getSchools();
        
    }
    
    ///////////////////////////////
    ///Get Schools
    ///////////////////////////////
    public List<SchoolWrapper> getSchools(){
        List<Account> result = [Select id,name From Account Where recordType.developerName = 'SchoolCustomer' Order By name ASC];
        for(Account sch:result){
            schools.add(new SchoolWrapper(false,sch));    
        }
        system.debug(schools);
        return schools; 
    }
    
    /////////////////////////////////////////////////
    ///Build a set of Selected schools via the filter
    /////////////////////////////////////////////////
    private void buildSelectedSchoolsSet(){
        schoolSet = new Set<String>();
        for(SchoolWrapper sw:schools){
            if(sw.selected == true){
                schoolSet.add(sw.school.id);
            }
        }                    
    }
    
    ////////////////////////////////////////
    ///Run report to build report data 
    ///////////////////////////////////////
    public void runReport(){
        reportData = new List<AgreementWrapper>();
        buildSelectedSchoolsSet();
        getAgreements();
        
    }
    
    //////////////////////////////////////////////
    ///Build report data
    //////////////////////////////////////////////
    public List<AgreementWrapper> buildReportData(List<StudentProgram__c> spList){
        List<AgreementWrapper> output = new List<AgreementWrapper>();
        User u = [select timezonesidkey from user where id=:UserInfo.getUserId()];
        for(StudentProgram__c sp: spList){
            
            AgreementWrapper agre = new AgreementWrapper();
            agre.agreement = sp;
            if(sp.ApplicationStartDate__c != null){
                Datetime dt = Datetime.newInstanceGMT(sp.ApplicationStartDate__c.dateGmt(),sp.ApplicationStartDate__c.timeGmt());
                agre.applicationDateInLocalTimeZone = dt.format('MM/dd/yyyy HH:mm:ss', u.timezonesidkey);
            }
            for(CreditCheckEvaluation__c cce: sp.Credit_Check_Evaluations__r){
                agre.ccEvaluation = cce;
                break;
            }
            
            output.add(agre);
            
        }
        ////system.debug(output);
        return output;
    }
    
    ///////////////////////////////////////////////
    ///Get Student Programs
    ///////////////////////////////////////////////
    public void getAgreements(){
        
        List<StudentProgram__c> agreementList = new List<StudentProgram__c>();
        if(schoolSet <> null && schoolSet.size()>0){
            agreementList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                             PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,program__r.Id,program__r.name,
                             ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                             CancellationReason__c,CustomerWithdrawnReason__c,(select id,name,CreditCheckDeniedReason__c,
                             CreditCheckDeniedReasonText__c,Status__c,CreditCheckProcess__c,CreditCheck__r.Id,CreditCheck__r.Name
                             from Credit_Check_Evaluations__r order by createddate desc limit 1)
                             FROM StudentProgram__c
                             WHERE Program__r.School__c IN :schoolSet
                             ORDER BY ApplicationStartDate__c desc];
         }
         else{
             agreementList = [SELECT id,name,student__c,student__r.name,StudentVemoAccountNumber__c,VemoContractNumber__c,
                             PrimaryOwner__r.name,status__c,SchoolName__c,ProgramName__c,program__c,program__r.Id,program__r.name,
                             ApplicationStartDate__c,submittedDate__c,certificationDate__c,ServicingStartDate__c,
                             CancellationReason__c,CustomerWithdrawnReason__c,(select id,name,CreditCheckDeniedReason__c,
                             CreditCheckDeniedReasonText__c,Status__c,CreditCheckProcess__c,CreditCheck__r.Id,CreditCheck__r.Name
                             from Credit_Check_Evaluations__r order by createddate desc limit 1)
                             FROM StudentProgram__c
                             WHERE Program__r.CreditCheckRequired__c =true /*AND Program__r.School__c IN :schoolSet*/
                             ORDER BY ApplicationStartDate__c desc];
         }
         ////system.debug(agreementList);   
         reportData = buildReportData(agreementList);                                                
        
    }
    
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/Report_ApplicationExport');
        pg.setRedirect(false);
        return pg;
    }
    
    ////////////////////////////////////////
    ///Generate a csv string
    ////////////////////////////////////////
    public void buildCsvString(){
        csv = 'Student Program Number,Vemo Contract Number,Vemo Account Number,School Name,Program Name,Program Number,';
        csv += 'Application Start Date,Status,Cancellation Reason,Credit Check Evaluation Number,';
        csv += 'Credit Check Evaluation Status,Credit Check Process,Credit Check Denied Reason,';
        csv += 'Credit Check Denied Reason Text,Credit Check Number';
        csv += '\n';
        runReport();
        
        if(reportData == null || reportData.size()==0) return;
        
        
        
        for(AgreementWrapper agre:reportData){
            
            csv += agre.agreement.name + ',';
            csv += agre.agreement.vemoContractNumber__c + ',';
            csv += agre.agreement.StudentVemoAccountNumber__c + ',';              
            
            if(agre.agreement.schoolName__c <> null) csv += agre.agreement.schoolName__c.escapeCsv() + ',';
            else csv += ',';
            
            if(agre.agreement.programName__c <> null) csv += agre.agreement.programName__c.escapeCsv() + ',';
            else csv += ',';
            
            if(agre.agreement.Program__r.Name <> null) csv += agre.agreement.Program__r.Name.escapeCsv() + ',';
            else csv += ',';
            
            if(agre.applicationDateInLocalTimeZone <> null) csv += agre.applicationDateInLocalTimeZone + ',';    
            else csv += ',';
            
            csv += agre.agreement.status__c + ',';
            
            if(agre.agreement.CancellationReason__c <> null) csv += agre.agreement.CancellationReason__c.escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.Name <> null) csv += agre.ccEvaluation.Name.escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.Status__c <> null) csv += agre.ccEvaluation.Status__c.escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.CreditCheckProcess__c <> null) csv += agre.ccEvaluation.CreditCheckProcess__c.escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.CreditCheckDeniedReason__c <> null) csv += agre.ccEvaluation.CreditCheckDeniedReason__c.escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.CreditCheckDeniedReasonText__c <> null) csv += agre.ccEvaluation.CreditCheckDeniedReasonText__c.replace('<br>', '\n').escapeCsv() + ',';    
            else csv += ',';
            
            if(agre.ccEvaluation.CreditCheck__r.Name <> null) csv += agre.ccEvaluation.CreditCheck__r.Name.escapeCsv() + ',';    
            else csv += ',';
           
            csv += '\n';
        }
    }
    
    ///////////////////////////////////
    ///Wrapper to hold all school Customers
    ///////////////////////////////////
    public class SchoolWrapper{
        public boolean selected {get;set;}
        public Account school {get;set;}
        
        public schoolWrapper(boolean selected, Account school){
            this.selected = selected;
            this.school = school;
        }  
        
    }
    
    //////////////////////////////////////////
    ///Agreement Wrapper
    //////////////////////////////////////////
    public class AgreementWrapper{
        public StudentProgram__c agreement {get;set;}
        public String applicationDateInLocalTimeZone{get;set;}
        public CreditCheckEvaluation__c ccEvaluation{get;set;}
        
    }  
    
      
}