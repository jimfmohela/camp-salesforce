@isTest
public class OutboundEmailSchedule_TEST{
     static testMethod void testExecute(){
        
        Test.startTest();
        
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        
        OutboundEmailBatchScheduled BatchJob = new OutboundEmailBatchScheduled ();
        BatchJob.jobType = OutboundEmailBatch.JobType.SEND_EMAIL;
        System.schedule('OutboundEmailBatch_Test', CRON_EXP, BatchJob);
        
        Test.stopTest();
     }
}