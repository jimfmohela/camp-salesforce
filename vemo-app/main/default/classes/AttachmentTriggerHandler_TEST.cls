@isTest
public with sharing class AttachmentTriggerHandler_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();

    @isTest
    static void Insert_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Attachment> IdVSAttachmentMap = TestDataFactory.createAndInsertStudentAttachments(TestUtil.TEST_THROTTLE);
    }

    @isTest
    static void Update_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Attachment> IdVSAttachmentMap = TestDataFactory.createAndInsertStudentAttachments(TestUtil.TEST_THROTTLE);
        for(Attachment att : IdVSAttachmentMap.values()){
            att.Description = 'This file is updated on - '+Date.today();
        }
        //update IdVSAttachmentMap.values();
        dbUtil.updateRecords(IdVSAttachmentMap.values());
        System.assertEquals('This file is updated on - '+Date.today(), IdVSAttachmentMap.values()[0].Description);
    }

    @isTest
    static void Delete_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<ID, Attachment> IdVSAttachmentMap = TestDataFactory.createAndInsertStudentAttachments(TestUtil.TEST_THROTTLE);
        //delete IdVSAttachmentMap.values();
        dbUtil.deleteRecords(IdVSAttachmentMap.values());
    }

    @isTest
    static void Undelete_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<ID, Attachment> IdVSAttachmentMap = TestDataFactory.createAndInsertStudentAttachments(TestUtil.TEST_THROTTLE);
        //delete IdVSAttachmentMap.values();
        dbUtil.deleteRecords(IdVSAttachmentMap.values());
        //undelete IdVSAttachmentMap.values();
        dbUtil.undeleteRecords(IdVSAttachmentMap.values());
    }

    //handleStudentProgramAttachment - Unsigned Aggrement Case
    @isTest
    static void handleSPA_TestUnsignedAggrement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        List<Attachment> attachmentList = TestDataFactory.createAttachments(1, studentPrgMap.values()[0].Id);
        //insert attachmentList;
        dbUtil.insertRecords(attachmentList);
        System.assertEquals(1, attachmentList.size());
        Test.startTest();
        String unsignedKey = AgreementService.unsignedAgreementFileName(attachmentList[0].ParentID)+'.pdf';
        attachmentList[0].Name = unsignedKey;
        //update attachmentList;
        dbUtil.updateRecords(attachmentList);
        Test.stopTest();
        }
    }

    //handleStudentProgramAttachment - signed Aggrement Case
    @isTest
    static void handleSPA_TestSignedAggrement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        List<Attachment> attachmentList = TestDataFactory.createAttachments(1, studentPrgMap.values()[0].Id);
        //insert attachmentList;
        dbUtil.insertRecords(attachmentList);
        System.assertEquals(1, attachmentList.size());
        Test.startTest();
        String signedKey = AgreementService.signedAgreementFileName(attachmentList[0].ParentID)+'.pdf';
        attachmentList[0].Name = signedKey;
        //update attachmentList;
        dbUtil.updateRecords(attachmentList);
        Test.stopTest();
        }
    }

    //handleStudentProgramAttachment - Final Disclosure Case
    @isTest
    static void handleSPA_TestFinalDisclosure(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        List<Attachment> attachmentList = TestDataFactory.createAttachments(1, studentPrgMap.values()[0].Id);
        dbUtil.insertRecords(attachmentList);
        //insert attachmentList;
        System.assertEquals(1, attachmentList.size());
        Test.startTest();
        String finalDisKey = AgreementService.finalDisclosureFileName(attachmentList[0].ParentID)+'.pdf';
        attachmentList[0].Name = finalDisKey;
        //update attachmentList;
        dbUtil.updateRecords(attachmentList);
        Test.stopTest();
        }
    }

    @isTest
    static void handleGenericDocumentAttachments_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, GenericDocument__c> genDocMap = TestDataFactory.createAndInsertGenericDocument(1);
        List<Attachment> attachmentList = TestDataFactory.createAttachments(1, genDocMap.values()[0].Id);
        System.assertEquals(1, attachmentList.size());
        //insert attachmentList;
        dbUtil.insertRecords(attachmentList);
        attachmentList[0].Description = 'This document is updated on - '+Date.today();
        //update attachmentList;
        dbUtil.updateRecords(attachmentList);
    }

    @isTest
    static void handleOutbountEmailAttachments_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        List<OutboundEmail__c> outboundEmailList = TestDataFactory.createOutboundEmail(1);
        insert outboundEmailList;
        List<Attachment> attachmentList = TestDataFactory.createAttachments(3, outboundEmailList[0].Id);
        //insert attachmentList;
        dbUtil.insertRecords(attachmentList);
    }

    @isTest
    static void handleOutbountEmailAttachments_TestDelete(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        List<OutboundEmail__c> outboundEmailList = TestDataFactory.createOutboundEmail(1);
        //insert outboundEmailList;
        dbUtil.insertRecords(outboundEmailList);
        List<Attachment> attachmentList = TestDataFactory.createAttachments(3, outboundEmailList[0].Id);
        Test.startTest();
        //insert attachmentList;
        dbUtil.insertRecords(attachmentList);
        //delete attachmentList[0];
        dbUtil.deleteRecord(attachmentList[0]);
        Test.stopTest();
    }
}