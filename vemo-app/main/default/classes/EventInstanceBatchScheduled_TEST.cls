@isTest
public with sharing class EventInstanceBatchScheduled_TEST {
  
    @isTest
    static void sendInstanceMail_Test(){
        Test.startTest();
        EventInstanceBatchScheduled job = new EventInstanceBatchScheduled();
        job.jobType = EventInstanceBatch.JobType.SEND_EVENTINSTANCE_EMAIL;
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        System.schedule('delinquencySchedule_Test', CRON_EXP, job);
        Test.stopTest();
    }
}