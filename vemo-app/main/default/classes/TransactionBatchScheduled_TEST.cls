@isTest
public with sharing class TransactionBatchScheduled_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void TransactionBatchScheduledTest(){
        
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, Transaction__c> trxnMap = TestDataFactory.createAndInsertTransactions(TestUtil.TEST_THROTTLE, studentPrgMap, 'Disbursement');
        for(Transaction__c trxn : trxnMap.values()){
            trxn.Deleted__c = true;
        }
        //update trxnMap.values();
        dbUtil.updateRecords(trxnMap.values());
        
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        TransactionBatchScheduled BatchJob = new TransactionBatchScheduled();
        BatchJob.jobType = TransactionBatch.JobType.PURGE_DELETED_DISBURSEMENTS;
        System.schedule('TransactionBatch_Schedule_Test', CRON_EXP, BatchJob);
        Test.stopTest();
    }
}