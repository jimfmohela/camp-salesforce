public with sharing class CalendarService { 
    public static Set<Date> federalHolidays = new Set<Date>(); 
    static { 
        Map<ID, Calendar__c> federalHolidayMap = new Map<ID, Calendar__c>([SELECT id, Date__c from Calendar__c where Type__c = 'Federal Holiday' order by Date__c asc]); 
        for(Calendar__c cal : federalHolidayMap.values()){ 
            federalHolidays.add(cal.Date__c); 
        } 
        System.debug('federalHolidays:'+federalHolidays); 
    } 
}