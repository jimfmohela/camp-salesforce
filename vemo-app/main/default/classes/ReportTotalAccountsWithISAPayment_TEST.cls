@isTest
public class ReportTotalAccountsWithISAPayment_TEST {
  
   public static DatabaseUtil dbUtil = new DatabaseUtil();
   
    @TestSetup
    static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        //dbUtil.queryExecutor = new UserContext(); 
        //TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schools);
                
        Map<Id, StudentProgram__c> testStudPrgMap = TestDataFactory.createAndInsertStudentProgram(1,
                                                                                                  studentMap,
                                                                                                  programMap);
        for(StudentProgram__c sp : testStudPrgMap.values()){
            sp.Status__c = 'Payment';
        }
        dbUtil.updateRecords(testStudPrgMap.values());
        } 
    }
    @isTest
    public static void validateRep(){
        Test.startTest();
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        ReportTotalAccountsWithISAPayment rep = new ReportTotalAccountsWithISAPayment();
        rep.buildCsvString();
        rep.exportToCSV();
        Test.stopTest();
    }
}