public with sharing class MigrateCreditCheckBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts{
    
    public String query {get;set;}
    //public JobType job {get;set;}  
    
    public MigrateCreditCheckBatch() {
    }
    public MigrateCreditCheckBatch(String query) {
        this.query = query;
    } 
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('MigrateCreditCheckBatch.start()');
        if(String.isEmpty(this.query)){
            query = 'Select id from CreditCheck__c where MigratedCreditCheck__c = false ';
        }
      
        
        return Database.getQueryLocator(query);
    }   
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        System.debug('CreditCheckBatch.execute()');
              
        Set<ID> scopeIDs = new Set<ID>();
        for(sObject sobj : scope){
            scopeIDs.add(sobj.id);
        }
        
        Map<ID, CreditCheck__c> creditCheckMap = CreditCheckQueries.getCreditCheckMapWithCreditCheckID(scopeIDs);
        //Map<ID, StudentProgram__c> agreementMap = new Map<ID, StudentProgram__c>([Select id, MigratedCreditCheck__c, CreditCheckDeniedReasonText__c,CreditCheckProcess__c, CreditCheck__c, CreditCheck__r.Status__c, CreditCheck__r.CreditCheckDeniedReason__c, CreditCheck__r.MigratedCreditCheck__c from StudentProgram__c where  CreditCheck__c IN: scopeIDs]);
        creditCheckEvaluation(creditCheckMap);
       
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('CreitCheckBatch.finish()');
        LogService.writeLogs();
    }
    
    public void creditCheckEvaluation(Map<ID, CreditCheck__c> creditCheckMap){
        List<CreditCheckEvaluation__c> createCreditCheckEvaluation = new List<CreditCheckEvaluation__c>();
        //List<StudentProgram__c> updateStudentProgram = new List<StudentProgram__c>();
        Map<ID, StudentProgram__c> agreementMap = new Map<ID, StudentProgram__c>([Select id, MigratedCreditCheck__c, CreditCheckDeniedReasonText__c,CreditCheckProcess__c, CreditCheck__c from StudentProgram__c where CreditCheck__c IN: creditCheckMap.keyset()]);
        for(StudentProgram__c sp: agreementMap.values()){
            CreditCheckEvaluation__c cce = new  CreditCheckEvaluation__c();
            cce.CreditCheck__c = sp.CreditCheck__c;
            cce.Agreement__c = sp.id;
            cce.Status__c = creditCheckMap.get(sp.CreditCheck__c).Status__c;
            string val = '';
            if(creditCheckMap.get(sp.CreditCheck__c).CreditCheckDeniedReason__c != null){
                for(string s: creditCheckMap.get(sp.CreditCheck__c).CreditCheckDeniedReason__c.split(';')){
                    if(s == 'Collection'){
                        val += 'Collection(s)';        
                    } 
                    else  
                        val += s;    
                    val += ';'; 
                }
                val = val.removeEnd(';');
            }
            cce.CreditCheckDeniedReason__c = val;
            //cce.CreditCheckDeniedReason__c = creditCheckMap.get(sp.CreditCheck__c).CreditCheckDeniedReason__c;
            cce.CreditCheckDeniedReasonText__c = sp.CreditCheckDeniedReasonText__c;
            cce.CreditCheckProcess__c = sp.CreditCheckProcess__c;
            
            createCreditCheckEvaluation.add(cce);  
            
            sp.MigratedCreditCheck__c = true;
                        
            creditCheckMap.get(sp.CreditCheck__c).MigratedCreditCheck__c = true;
             
        }  
        
        if(createCreditCheckEvaluation.size()>0)
            insert createCreditCheckEvaluation;  
            
        if(agreementMap.size() > 0)
            update agreementMap.values();
        
        if(creditCheckMap.size()>0)
            update creditCheckMap.values();     
    }


}