@isTest
public class SandboxSetup_TEST {
    private static FINAL String DEV1_ORG_ID = '00DR0000001yu46';
    private static FINAL String DEV2_ORG_ID = '00D7A0000000SZi';
    private static FINAL String PROD_ORG_ID = '00D36000001HD53';

    @isTest static void testSandboxSetup(){
        DatabaseUtil dbUtil = new DatabaseUtil();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        SandboxSetup cls = new SandboxSetup();
        user userwithrole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Test.StartTest();
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Test.testSandboxPostCopyScript(cls, PROD_ORG_ID, DEV2_ORG_ID, 'Dev2');
        Test.StopTest();
        }
    }
}