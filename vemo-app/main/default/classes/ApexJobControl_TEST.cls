@isTest
public with sharing class ApexJobControl_TEST {
    
    @TestSetup
    static void createRequiredSetupRecords(){
        //Create and Insert all types of ApexJobControl Records
        SandboxSetup.createJobControls();
        List<ApexJobControl__c> jobControls = new List<ApexJobControl__c>();
        jobControls.add(new ApexJobControl__c(Name = 'LogBatch',
                                              BatchJob__c = 'LogBatch',
                                              BatchJobJobType__c = 'PURGE',
                                              ScheduleJob__c = 'LogBatchScheduled',
                                              CronMode__c = 'HOURLY'));
        
        jobControls.add(new ApexJobControl__c(Name = 'EventBatch',
                                              BatchJob__c = 'EventBatch',
                                              BatchJobJobType__c = 'INSTANTIATE_EVENTINSTANCE',
                                              ScheduleJob__c = 'EventBatchScheduled',
                                              CronMode__c = 'HOURLY'));
        
        jobControls.add(new ApexJobControl__c(Name = 'EventInstanceBatch',
                                              BatchJob__c = 'EventInstanceBatch',
                                              BatchJobJobType__c = 'SEND_EVENTINSTANCE_EMAIL',
                                              ScheduleJob__c = 'EventInstanceBatchScheduled',
                                              CronMode__c = 'HOURLY'));
        jobControls.add(new ApexJobControl__c(Name = 'RECURRING_PAYMENT_GENERATION',
                                              BatchJob__c = 'StudentAccountBatch',
                                              BatchJobJobType__c = 'RECURRING_PAYMENT_GENERATION',
                                              ScheduleJob__c = 'StudentAccountScheduled',
                                              CronMode__c = 'HOURLY'));
        jobControls.add(new ApexJobControl__c(Name = 'CUMULATIVE_PROGRAM_FUNDING',
                                              BatchJob__c = 'StudentAccountBatch',
                                              BatchJobJobType__c = 'CUMULATIVE_PROGRAM_FUNDING',
                                              ScheduleJob__c = 'StudentAccountScheduled',
                                              CronMode__c = 'HOURLY'));
        jobControls.add(new ApexJobControl__c(Name = 'CLEARINGHOUSE',
                                              BatchJob__c = 'StudentAccountBatch',
                                              BatchJobJobType__c = 'CLEARINGHOUSE',
                                              ScheduleJob__c = 'StudentAccountScheduled',
                                              CronMode__c = 'HOURLY'));
        upsert jobControls;
        TestUtil.abortJobsByName('All');
    }
    
    static testMethod void getCustomSettingsRecordTest(){
        List<ApexJobControl__c> apexJobControlList = new List<ApexJobControl__c>();
        Test.startTest();
        apexJobControlList = ApexJobControl.getCustomSettingsRecord();
        Test.stopTest();
    }
    
    static testMethod void getdetailsTest(){
        Test.startTest();
        ApexJobControl.getdetails('Annual Statement Generation');
        Test.stopTest();
    }
    
    static testMethod void contractAssessmentRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Assess Student Contracts');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void purgeDeleteRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Purge Deleted Agreements');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void monthEndAuditRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Student Program Audit Monthly');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void deliquencyRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Delinquency Tracking');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void generateFinalDisclosureRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Generate Final Disclosures');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void statusTransitionsRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Student Program Status Transitions');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void annualStatementGenrtnRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Annual Statement Generation');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void recurringPaymentGnrtnRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('RECURRING_PAYMENT_GENERATION');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void cumulativePrgmFundingRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('CUMULATIVE_PROGRAM_FUNDING');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void clearingHouseRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('CLEARINGHOUSE');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void clearingHouseRunNowTestOld(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Clearinghouse');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void queryApprovedOpenRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('ACH - Query Open Batches');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void purgeDeleteDisbursementsRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Purge Deleted Disbursements');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void sendOutboundEmailsRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Outbound Email');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void allocatePaymentsRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Allocate Payments');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    /*static testMethod void createAgreementsRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Reconciliation Jobs');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }*/
    static testMethod void sendIntroReconTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Reconciliation Intro');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void sendKickOffReconTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Reconciliation Intro');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void disbConfNotificationRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Reconciliation Kick-Off');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void purgeDeletedAgreementsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Purge Deleted Agreements');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void monthEndAuditScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Student Program Audit Monthly');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void annualStatementGenrtnScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Annual Statement Generation');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void recurringPymntGenrtnScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Recurring Payment Generation');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void clearingHousesScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Clearinghouse');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void clearingHousesScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Clearinghouse');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void sendOutboundEmailScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Outbound Email');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void sendOutboundEmailScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Outbound Email');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void queryApprovedOpenScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('ACH - Query Open Batches');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void queryApprovedOpenScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('ACH - Query Open Batches');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void allocatePaymentsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Allocate Payments');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void outboundACHScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Outbound ACH');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    /*static testMethod void createAgreementsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Reconciliation Jobs');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }*/
    static testMethod void sendReconIntroTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Reconciliation Intro');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    /*static testMethod void createAgreementsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Run Reconciliation Jobs');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }*/
    
    static testMethod void sendKickOffReconScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Reconciliation Kick-Off');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void purgeDeletedDisbsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Purge Deleted Disbursements');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void purgeDeletedDisbsScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Purge Deleted Disbursements');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void disbConfNotificationsScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Disbursement Confirmation');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void disbConfNotificationsScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Disbursement Confirmation');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void disbConfNotificationsUnScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('Send Disbursement Confirmation');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        apexJobControlObj.SchedulerActive__c = false;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void logBatchRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('LogBatch');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void logBatchScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('LogBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void logBatchScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('LogBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void eventBatchRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventBatch');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void eventBatchScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void eventBatchScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
    
    static testMethod void eventInstanceBatchRunNowTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventInstanceBatch');
        ApexJobControl.runJobNow(apexJobControlObj);
        Test.stopTest();
    }
    
    static testMethod void eventInstanceBatchScheduleTest(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventInstanceBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 10);
        Test.stopTest();
    }
    
    static testMethod void eventInstanceBatchScheduleTest_20Minutes(){
        Test.startTest();
        ApexJobControl__c apexJobControlObj = ApexJobControl.getdetails('EventInstanceBatch');
        apexJobControlObj.SchedulerActive__c = true;
        apexJobControlObj.CronMode__c = 'hourly';
        ApexJobControl.schedule_UnscheduleTheJob('hourly', apexJobControlObj, 20);
        Test.stopTest();
    }
}