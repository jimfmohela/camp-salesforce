/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIUserResource
// 
// Description: 
//  Direction Central for User API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-11-08   Greg Cook       Created                        
// 
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIUserResource implements VemoAPI.ResourceHandler {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        } else     
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        system.debug('VemoAPIUserResource.handleGetV2()');

        List<UserResourceOutputV2> results = new List<UserResourceOutputV2>();
        if(GlobalSettings.getSettings().vemoDomainAPI){
            results.add(new VemoUserResourceOutputV2());
        } else {
            results.add(new PublicUserResourceOutputV2());
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public virtual class UserResourceOutputV2{

        public String schoolID {get;set;}
        public String userEmail {get;set;}
        public String userID {get;set;}
        public String userName {get;set;}
        public Boolean authenticated {get;set;}
        public String name {get;set;}



        public UserResourceOutputV2(){
            userID = UserInfo.getUserId();
            //String siteUser =  KeyValuePair__c.getValues('School Admin Site UserId').ValueText__c;
            //String siteUser = Label.SchoolAdminSiteUserId;
            String siteUser =  GlobalSettings.getSettings().SchoolAdminSiteUserId;
            System.debug('siteUser:'+siteUser);
            if(userID == siteUser){
                authenticated = false;
                userId = null;
            } else {    
                authenticated = true;
                userEmail = UserInfo.getUserEmail();
                userName = UserInfo.getUserName();
                If(UserService.schoolAccount != null){
                    schoolID = UserService.schoolAccount.id;
                }
                name = UserService.userContext.Name;
            }
        }
    }
    public class PublicUserResourceOutputV2 extends UserResourceOutputV2{
        public PublicUserResourceOutputV2(){}
    }

    public class VemoUserResourceOutputV2 extends UserResourceOutputV2{
        public String contactID {get;set;}
        public String sessionID {get;set;}
        public VemoUserResourceOutputV2(){
            if(authenticated){
                sessionId = UserInfo.getSessionId();
                contactID = UserService.schoolContact.id;
            }
        }        
    }
}