public with sharing class VemoAPIEmployerResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }   
            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
    }
    
    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){
        List<EmployerService.Employer> empList = new List<EmployerService.Employer>();
        String studentIDParam = api.params.get('studentID');
        String employerIDParam = api.params.get('employerID');
        
        if(studentIDParam != null){
             empList = EmployerService.getEmployerWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else if(employerIDParam != null){
             empList = EmployerService.getEmployerWithEmployerID(VemoApi.parseParameterIntoIDSet(employerIDParam));
        }
        else 
             throw new VemoAPI.VemoAPIFaultException('Required parameter studentID or employerID');
        
        List<EmployerOutputV1> results = new List<EmployerOutputV1>();
        
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmployerService.Employer eh: empList){
            AccountIDsToVerify.add((ID) eh.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        for(EmployerService.Employer empHis : empList){
            if(VerifiedAccountMap.containsKey((ID) empHis.studentID)){
                results.add(new EmployerOutputV1(empHis));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));     
    }
    
    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        List<EmployerService.Employer> newEmployer = new List<EmployerService.Employer>();
        List<EmployerInputV1> employerJSON = (List<EmployerInputV1>)JSON.deserialize(api.body, List<EmployerInputV1>.class);
        
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmployerInputV1 empHis : employerJSON){
            AccountIDsToVerify.add((ID) empHis.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        
        for(EmployerInputV1 empHis : employerJSON){
            if(verifiedAccountMap.containsKey((ID)empHis.studentID)){
                empHis.validatePOSTFields();
                EmployerService.Employer empHisServ = new EmployerService.Employer();
                empHisServ = employerInputV1ToEmployer(empHis);
                newEmployer.add(empHisServ);
            }
        }
        Set<ID> employerIDs = EmployerService.createEmployer(newEmployer);
        return (new VemoAPI.ResultResponse(employerIDs, employerIDs.size()));
    }
    
    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        List<EmployerService.Employer> newEmployer = new List<EmployerService.Employer>();
        List<EmployerInputV1> employerJSON = (List<EmployerInputV1>)JSON.deserialize(api.body, List<EmployerInputV1>.class);
        
        Set<ID> EmployerIDsToVerify = new Set<ID>();
        for(EmployerInputV1 empHis : employerJSON){
            EmployerIDsToVerify.add((ID) empHis.employerID);    
        }
        Map<ID, Employer__c> verifiedEmployerMap = EmployerQueries.getEmployerMapWithEmployerID(EmployerIdsToVerify); //authorized records should be returned
        
        for(EmployerInputV1 empHis : employerJSON){
            if(verifiedEmployerMap.containsKey((ID)empHis.employerID)){
                empHis.validatePUTFields();
                EmployerService.Employer empHisServ = new EmployerService.Employer();
                empHisServ = employerInputV1ToEmployer(empHis);
                newEmployer.add(empHisServ);
            }
        }
        Set<ID> employerIDs = EmployerService.updateEmployer(newEmployer);
        return (new VemoAPI.ResultResponse(employerIDs, employerIDs.size()));
    }
    
    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        String employerIDParam = api.params.get('employerID');
        Map<ID, Employer__c> employerMap = EmployerQueries.getEmployerMapWithEmployerId(VemoApi.parseParameterIntoIDSet(employerIDParam));
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(Employer__c eh: employerMap.values()){
            AccountIDsToVerify.add(eh.Student__c);
        }
        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIDsToVerify); //authorized records should be returned
        Set<ID> employersToDelete = new Set<ID>();
        for(Employer__c eh: employerMap.values()){
             if(verifiedAccountMap.containsKey(eh.student__c)){
                employersToDelete.add(eh.id);
            }
        }
        Integer numToDelete = 0;
        if(employersToDelete.size()>0){
            numToDelete = EmployerService.deleteEmployer(employersToDelete);
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    
    public static EmployerService.Employer employerInputV1ToEmployer(EmployerInputV1 input){
        EmployerService.Employer output = new EmployerService.Employer();
        output.employerID = input.employerID;
        output.employerName = input.employerName;
        output.studentID = input.studentID;
        output.jobTitle = input.jobTitle;
        output.monthlyIncome = input.monthlyIncome;
        output.deleted = input.deleted;
        
        return output;
    }
    
    public class EmployerOutputV1{
        public String employerID {get;set;}
        public String employerName {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String jobTitle {get;set;}
        public String studentID {get;set;}
        public Decimal monthlyIncome {get;set;}
        public Decimal transactionCount {get;set;}
        public Boolean deleted {get;set;}
        
        public EmployerOutputV1(){}
        
        public EmployerOutputV1(EmployerService.Employer emp){
            this.employerID = emp.employerID;
            this.employerName = emp.employerName;
            this.employmentEndDate = emp.employmentEndDate;
            this.employmentStartDate = emp.employmentStartDate;
            this.jobTitle = emp.jobTitle;
            this.studentID = emp.studentID;
            this.monthlyIncome = emp.monthlyIncome; 
            this.transactionCount = emp.transactionCount;
            this.deleted = emp.deleted;
        }
    
    }
    
    public class EmployerInputV1{
        public String employerID {get;set;}
        public String employerName {get;set;}
        public String jobTitle {get;set;}
        public String studentID {get;set;}
        public Decimal monthlyIncome {get;set;}
        public Decimal transactionCount {get;set;}
        public Boolean deleted {get;set;}
        
        public EmployerInputV1(Boolean testValues){
            if(testValues){
                //this.category = 'Employee'; //Contractor, Internship
                //this.employer = 'Test Employer'; //Planned, Closed, Cancelled
                //this.type = 'Full Time'; //Part Time
            }
        }

        public void validatePOSTFields(){
            if(employerID != null) throw new VemoAPI.VemoAPIFaultException('employerID cannot be created in POST');
            if(studentID == null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID is a required input parameter on POST');
        }
        public void validatePUTFields(){
            if(employerID == null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID is a required input parameter on PUT');
            if(studentID != null) throw new VemoAPI.VemoAPIFaultException('studentID is not a writable input parameter on PUT');
        }
    
    }
}