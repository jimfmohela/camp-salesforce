@isTest
public with sharing class PaymentInstructionScheduled_TEST {
    
	@isTest public static void scheduleInboundACH_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        PaymentInstructionScheduled job = new PaymentInstructionScheduled();
        job.JobType = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
        System.schedule('scheduleInboundACH_Test', CRON_EXP, job);  
        Test.stopTest();
    }
    
    @isTest public static void scheduleOutboundACH_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        PaymentInstructionScheduled job = new PaymentInstructionScheduled();
        job.JobType = PaymentInstructionBatch.JobType.SCHEDULE_OUTBOUND_ACH;
        System.schedule('scheduleOutboundACH_Test', CRON_EXP, job);  
        Test.stopTest();
    } 
    
    @isTest public static void allocatePaymentsSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        PaymentInstructionScheduled job = new PaymentInstructionScheduled();
        job.JobType = PaymentInstructionBatch.JobType.ALLOCATE_PAYMENTS;
        System.schedule('allocatePaymentsSchedule_Test', CRON_EXP, job);  
        Test.stopTest();
    } 
}