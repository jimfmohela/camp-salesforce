public class EmpHistCompController {
    
    @AuraEnabled
    public static List<accordialLableWrapper> getAllEmployer(string studId){
        Set<Id> studIdSet = new Set<Id>();
        studIdSet.add(studId);
        Map<Id,EmploymentHistory__c> empHistMap = EmploymentHistoryQueries.getEmploymentHistoryMapWithStudentId(studIdSet);
        
        List<string> empList = new List<String>();
        List<accordialLableWrapper> wrapList = new List<accordialLableWrapper>(); 
        
        List<EmploymentHistory__c> empHistList = new List<EmploymentHistory__c>();
        for(integer j=empHistMap.values().size()-1; j>=0; j--){
            empHistList.add(empHistMap.values()[j]);
        }
        empHistList.sort();
        
        Map<string,string> employer_Category = new Map<string,string>();
        Map<string,string> employer_JobTitle = new Map<string,string>();
        Map<string,string> employer_StartDate = new Map<string,string>();
        Map<string,string> employer_EndDate = new Map<string,string>();
        Map<string,string> employer_Amount = new Map<string,string>();        
        
        for(EmploymentHistory__c emp: empHistList){
            if(!empList.contains(emp.employer__c)){
                empList.add(emp.employer__c);
            }
            
            if(emp.Category__c != null && emp.Category__c != '')
                employer_Category.put(emp.Employer__c,emp.Category__c);
            if(emp.jobTitle__c != null && emp.jobTitle__c != '')
                employer_JobTitle.put(emp.Employer__c,emp.jobTitle__c);
            if(emp.EmploymentStartDate__c != null)
                employer_StartDate.put(emp.Employer__c,string.valueof(emp.EmploymentStartDate__c).remove('00:00:00'));
            if(emp.EmploymentEndDate__c != null)
                employer_EndDate.put(emp.Employer__c,string.valueof(emp.EmploymentEndDate__c).remove('00:00:00'));
            else
                employer_EndDate.put(emp.Employer__c,'Current');
            if(emp.YearlySalary__c != null)
                employer_Amount.put(emp.Employer__c,emp.YearlySalary__c+' Yearly');
            else if(emp.HourlyRate__c != null)
                employer_Amount.put(emp.Employer__c,emp.HourlyRate__c+' Hourly');
            
        }
        for(integer i=empList.size()-1;i>=0;i--){
            string s=empList[i];
            accordialLableWrapper e = new accordialLableWrapper();
            e.employerName = s;                
            e.category = employer_Category.get(s);                
            e.jobTitle = employer_JobTitle.get(s);
            e.startDate = employer_StartDate.get(s);
            e.endDate = employer_EndDate.get(s);
            e.salary = employer_Amount.get(s);
            
            wrapList.add(e);                
        }
        return wrapList;
    }
    
    @AuraEnabled
    public static detailWrapper getEmplHistory(string studId, string employer){
        
        detailWrapper detail = new detailWrapper();
        Set<String> empHistoryIds = new Set<String>();
		String query = EmploymentHistoryQueries.generateSOQLSelect();
        query = query+ ' where Student__c =: studId AND employer__c =: employer order by createdDate desc';
        List<EmploymentHistory__c> empHistList = Database.query(query);
        
        for(EmploymentHistory__c emp: empHistList){
            string empId = emp.Id;
            empHistoryIds.add(empId.substring(0,empId.length()-3));
        }
        
        Map<Id,GenericDocument__c> genericDocumentMap = GenericDocumentQueries.getGenericDocumentMapByParentID(empHistoryIds);
        Map<Id,List<GenericDocument__c>> empHistId_GenericDocumentMap = new Map<Id,List<GenericDocument__c>>();
        for(GenericDocument__c gd: genericDocumentMap.values()){
            if(empHistId_GenericDocumentMap.get(gd.ParentId__c) == null)
                empHistId_GenericDocumentMap.put(gd.ParentId__c,new List<GenericDocument__c>());
            if(empHistId_GenericDocumentMap.get(gd.ParentId__c) != null)
                empHistId_GenericDocumentMap.get(gd.ParentId__c).add(gd);
        }
        List<empHistDetailWrapper> innerWrapList = new List<empHistDetailWrapper>();
        for(EmploymentHistory__c emp: empHistList){
            empHistDetailWrapper innerWrap = new empHistDetailWrapper();
            innerWrap.empHistType = emp.EventType__c;
            innerWrap.empHistId = emp.Id;
            innerWrap.empHistName = emp.Name;
            innerWrap.jobTitle = emp.JobTitle__c;
            if(emp.EventType__c == 'NOT_EMPLOYED_HERE_ANYMORE')
                innerWrap.effectiveDate = emp.EmploymentEndDate__c;
            else if(emp.EventType__c == 'CREATE_EMPLOYMENT')
                innerWrap.effectiveDate = emp.EmploymentStartDate__c;
            else
            	innerWrap.effectiveDate = emp.EffectiveDate__c;
            
            if(emp.EventType__c == 'HOURLY_COMPENSATION' || emp.EventType__c == 'UPDATE_HOURLY_COMPENSATION' || emp.EventType__c == 'UPDATE_HOURLY_WITH_DESIGNATION')
            	innerWrap.amount = '$'+string.valueof(emp.HourlyRate__c)+' Hourly';
            else if(emp.EventType__c == 'TIPS_COMPENSATION' || emp.EventType__c == 'UPDATE_TIPS_COMPENSATION')
            	innerWrap.amount = '$'+string.valueof(emp.TipAmount__c);
            else if(emp.EventType__c == 'COMMISSION_COMPENSATION' || emp.EventType__c == 'UPDATE_COMMISSION_COMPENSATION')
            	innerWrap.amount = '$'+string.valueof(emp.CommissionAmount__c);
            else if(emp.EventType__c == 'BONUS_COMPENSATION' || emp.EventType__c == 'UPDATE_BONUS_COMPENSATION')
            	innerWrap.amount = '$'+string.valueof(emp.BonusAmount__c);
            else{
                if(emp.YearlySalary__c != null)
                    innerWrap.amount = '$'+string.valueof(emp.YearlySalary__c)+' Yearly';
                else if(emp.HourlyRate__c != null)
                    innerWrap.amount = '$'+string.valueof(emp.HourlyRate__c)+' Hourly';
            }
                
            if(emp.Verified__c == true)    
            	innerWrap.verified = 'Verified';
            else
                innerWrap.verified = 'Unverified';
            innerWrap.supportingDocuments = empHistId_GenericDocumentMap.get(emp.Id);
            if(emp.MonthlyIncomeTotal__c != null)
                innerWrap.monthlyIncome = emp.MonthlyIncomeTotal__c;
            innerWrapList.add(innerWrap);
        }
        detail.EmpHistoryList = innerWrapList;
        return detail;
    }
    
    public class accordialLableWrapper{
        @AuraEnabled
        public string accordianLable{get;set;}
        
        @AuraEnabled
        public string employerName{get;set;}
        
        @AuraEnabled
        public string category{get;set;}
        
        @AuraEnabled
        public string jobTitle{get;set;}
        
        @AuraEnabled
        public string startDate{get;set;}
        
        @AuraEnabled
        public string endDate{get;set;}
        
        @AuraEnabled
        public string salary{get;set;}

    }
    
    public class detailWrapper{
        @AuraEnabled
        public list<empHistDetailWrapper> EmpHistoryList{get;set;}
    }
    
    public class empHistDetailWrapper{
        @AuraEnabled
        public string empHistType{get;set;}
        
        @AuraEnabled
        public string empHistId{get;set;}
        
        @AuraEnabled
        public string empHistName{get;set;}
        
        @AuraEnabled
        public string jobTitle{get;set;}
        
        @AuraEnabled
        public date effectiveDate{get;set;}
        
        @AuraEnabled
        public string amount{get;set;}
        
        @AuraEnabled
        public string verified{get;set;}
        
        @AuraEnabled
        public List<GenericDocument__c> supportingDocuments{get;set;}
        
        @AuraEnabled
        public Decimal monthlyIncome{get;set;}
        
    }

}