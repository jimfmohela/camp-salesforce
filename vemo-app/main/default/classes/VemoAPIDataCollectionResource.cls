public class VemoAPIDataCollectionResource {

    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v2') && (api.method == 'POST')){
            return handlePostV2(api);
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }
             
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
        
    }

    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIDataCollectionResource.handleGetV2()');
        List<DataCollectionService.DataCollection> dataCollection = new List<DataCollectionService.DataCollection>();
        
        String dataCollectionIDParam = api.params.get('dataCollectionID');
        String agreementIDParam = api.params.get('agreementID');
        
        if(dataCollectionIDParam != null)
            dataCollection = DataCollectionService.getDataCollectionMapWithId(VemoApi.parseParameterIntoIDSet(dataCollectionIDParam));
        else if(agreementIDParam != null)
            dataCollection = DataCollectionService.getDataCollectionMapWithAgreement(VemoApi.parseParameterIntoIDSet(agreementIDParam));
        else 
            throw new VemoAPI.VemoAPIFaultException('Required parameter dataCollectionId or agreementID');
            
        List<DataCollectionResourceOutputV2> results = new List<DataCollectionResourceOutputV2>();
        for(DataCollectionService.DataCollection data : DataCollection){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoDataCollectionResourceOutputV2(data));
            }else{
                results.add(new PublicDataCollectionResourceOutputV2(data));
            }
            
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handlePostV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIDataCollectionResource.handlePostV2');
        List<DataCollectionService.DataCollection> DataCollections = new List<DataCollectionService.DataCollection>();
        List<DataCollectionResourceInputV2> dataCollectionsJSON = (List<DataCollectionResourceInputV2>)JSON.deserialize(api.body, List<DataCollectionResourceInputV2>.class);
        for(DataCollectionResourceInputV2 dataCollectionJSON : dataCollectionsJSON){
            dataCollectionJSON.validatePOSTFields();
            DataCollections.add(dataCollectionResourceToDataCollection(dataCollectionJSON));
        }       
        Set<Id> collectionIds = DataCollectionService.createDataCollections(dataCollections);
        return (new VemoAPI.ResultResponse(collectionIds, collectionIds.size()));
    }
    
    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIDataCollectionResource.handlePutV2');
        List<DataCollectionService.DataCollection> DataCollections = new List<DataCollectionService.DataCollection>();
        List<DataCollectionResourceInputV2> dataCollectionsJSON = (List<DataCollectionResourceInputV2>)JSON.deserialize(api.body, List<DataCollectionResourceInputV2>.class);
        for(DataCollectionResourceInputV2 dataCollectionJSON : dataCollectionsJSON){
            dataCollectionJSON.validatePUTFields();
            DataCollections.add(dataCollectionResourceToDataCollection(dataCollectionJSON));
        }       
        Set<Id> collectionIds = DataCollectionService.updateDataCollections(dataCollections);
        return (new VemoAPI.ResultResponse(collectionIds, collectionIds.size()));
    }
    
    public static DataCollectionService.DataCollection dataCollectionResourceToDataCollection(DataCollectionResourceInputV2 dc){
        DataCollectionService.DataCollection data = new DataCollectionService.DataCollection();
        data.DataCollectionId = dc.dataCollectionId;
        data.agreementID = dc.agreementId;
        data.dataType = dc.dataType;
        data.booleanValue = dc.booleanValue;
        data.picklistValue = dc.picklistValue;
        data.dateValue = dc.dateValue;
        data.stringValue = dc.stringValue;
        data.templateID = dc.templateId; 
        data.label = dc.label;
        return data;
    }
    
    public class DataCollectionResourceInputV2{
        public String dataCollectionID {get;set;}
        public String agreementID {get;set;}
        public String booleanValue {get;set;}
        public String dataType {get;set;}
        public Date dateValue {get;set;}
        public String picklistValue {get;set;}
        public String stringValue {get;set;}
        public String templateID {get;set;}
        public String label {get;set;}
        
        public DataCollectionResourceInputV2(){
        
        } 
        
        public void validatePOSTFields(){
            if(dataCollectionID != null) throw new VemoAPI.VemoAPIFaultException('dataCollectionID cannot be created in POST');
        }
        
        public void validatePUTFields(){
            if(dataCollectionID == null) throw new VemoAPI.VemoAPIFaultException('dataCollectionID is a required input parameter on PUT');
        }
    }
    
    public virtual class DataCollectionResourceOutputV2{
        public String dataCollectionID {get;set;}
        public String agreementID {get;set;}
        public String booleanValue {get;set;}
        public String dataType{get;set;}
        public Date dateValue {get;set;}
        public String picklistValue {get;set;}
        public String stringValue {get;set;}
        public String templateId {get;set;}
        public String label {get;set;} 
        
        public DataCollectionResourceOutputV2(){
            
        }
        
        public DataCollectionResourceOutputV2(DataCollectionService.DataCollection data){
            this.dataCollectionId = data.dataCollectionId;
            this.agreementID = data.agreementId;
            this.booleanValue = data.booleanValue;
            this.dataType = data.dataType;
            this.dateValue = data.dateValue;
            this.picklistValue = data.picklistValue; 
            this.stringValue = data.stringValue;
            this.templateId = data.templateId;
            this.label = data.label; 
        } 
    }
    
    public class PublicDataCollectionResourceOutputV2 extends DataCollectionResourceOutputV2{
        public PublicDataCollectionResourceOutputV2(){
        
        }  
        
        public PublicDataCollectionResourceOutputV2(DataCollectionService.DataCollection data){
            super(data);
        } 
    }
    
    public class VemoDataCollectionResourceOutputV2 extends DataCollectionResourceOutputV2{
        //public String dataType{get;set;}
        
        public VemoDataCollectionResourceOutputV2(){
        
        }  
        
        public VemoDataCollectionResourceOutputV2(DataCollectionService.DataCollection data){
            super(data);
            //this.dataType = data.dataType;
        } 
    }
}