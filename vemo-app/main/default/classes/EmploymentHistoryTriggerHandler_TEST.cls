@isTest
public class EmploymentHistoryTriggerHandler_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    static testMethod void DMLTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        System.assertEquals(TestUtil.TEST_THROTTLE, testEmpHisMap.size());
        testEmpHisMap.values()[0].DocUploaded__c = true;
        testEmpHisMap.values()[0].Verified__c = true;
        testEmpHisMap.values()[0].DateVerified__c = Date.today();
        update testEmpHisMap.values();
        delete testEmpHisMap.values();
        Test.stopTest();
    }
    
    static testMethod void notEmployedHereAnymoreTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Set<String> eventTypes = new Set<String>{
                'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION', 'UPDATE_HOURLY_COMPENSATION',
                'BONUS_COMPENSATION', 'COMMISSION_COMPENSATION', 'TIPS_COMPENSATION'
        };
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), testEmpHisMap.size());
        Map<Id, EmploymentHistory__c> notEMployedHereAnymoreEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, new Set<String>{'NOT_EMPLOYED_HERE_ANYMORE'}
        );
        System.assertEquals(TestUtil.TEST_THROTTLE, notEMployedHereAnymoreEmpHisMap.size());
        Test.stopTest();
    }
    
    static testMethod void autoCloseEmpHisTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Test.startTest();
        Set<String> eventTypes = new Set<String>{
                'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION', 'UPDATE_HOURLY_COMPENSATION',
                'BONUS_COMPENSATION', 'COMMISSION_COMPENSATION', 'TIPS_COMPENSATION'
        };
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), testEmpHisMap.size());
        Map<Id, EmploymentHistory__c> updateEmpHisMap = TestDataFactory.createAndInsertEmploymentHistoryByEventTypes(
            1, testStudentAccountMap, eventTypes
        );
        System.assertEquals(TestUtil.TEST_THROTTLE*eventTypes.size(), updateEmpHisMap.size());
        Test.stopTest();
    }
}