@isTest
public class ClrHouseStudentEnrolBatch_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    @isTest
    static void CLEARINGHOUSE_Test(){
      DatabaseUtil.setRunQueriesInMockingMode(false);
	  dbUtil.queryExecutor = new UserContext(); 
      Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
      Map<Id, ClrHouseStudentEnrollment__c> clrHouseStudentEnrollments = TestDataFactory.createAndInsertClrHouseStudentEnrollment(TestUtil.TEST_THROTTLE,students);
      clrHouseStudentEnrollments.values()[0].RequestorReturnField__c = students.values()[0].VemoAccountNumber__c;
      //update  clrHouseStudentEnrollments.values();
      dbUtil.updateRecords(clrHouseStudentEnrollments.values());
      Test.startTest();
      ClrHouseStudentEnrolBatch job = new ClrHouseStudentEnrolBatch();
      job.job = ClrHouseStudentEnrolBatch.JobType.CLEARINGHOUSE;
      Database.executeBatch(job, 200);
      Test.stopTest();
    }
    
    @isTest
    static void CLEARINGHOUSE_TestWithBlankQuery(){
      Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
      Map<Id, ClrHouseStudentEnrollment__c> clrHouseStudentEnrollments = TestDataFactory.createAndInsertClrHouseStudentEnrollment(TestUtil.TEST_THROTTLE,students);
      Test.startTest();
      ClrHouseStudentEnrolBatch job = new ClrHouseStudentEnrolBatch('');
      job.job = ClrHouseStudentEnrolBatch.JobType.CLEARINGHOUSE;
      Database.executeBatch(job, 200);
      Test.stopTest();
    }
}