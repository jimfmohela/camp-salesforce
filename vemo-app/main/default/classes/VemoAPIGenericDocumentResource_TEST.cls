@isTest
public class VemoAPIGenericDocumentResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    
    static testMethod void testHandleGetV1(){
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        Map<Id, IncomeVerification__c> testIncVerMap = TestDataFactory.createAndInsertIncomeVerification(1, testEmpHisMap);
        Map<Id, GenericDocument__c> testDocMap = TestDataFactory.createAndInsertGenericDocument(TestUtil.TEST_THROTTLE);
        Set<String> incStrings = new Set<String>();     
        for(IncomeVerification__c inc : testIncVerMap.values()){
            incStrings.add(((String)inc.id).subString(0,15));
        }
        for(GenericDocument__c genDoc : testDocMap.values()){
            genDoc.ParentID__c = testIncVerMap.values()[0].id;
        }
        
        Map<String, String> genDocParams = new Map<String, String>();
        genDocParams.put('genericDocumentID', TestUtil.createStringFromIDSet(testDocMap.keySet()));
        genDocParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo genDocApiInfo = TestUtil.initializeAPI('v1', 'GET', genDocParams, null);

        Map<String, String> genParentParams = new Map<String, String>();
        genParentParams.put('parentID', TestUtil.createStringFromIDSet(testIncVerMap.keyset()));
        genParentParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo genParentAPIInfo = TestUtil.initializeAPI('v1', 'GET', genParentParams, null);

        Test.startTest();
        VemoAPI.ResultResponse genDocResult = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(genDocApiInfo);
        System.assertEquals(testDocMap.size(), genDocResult.numberOfResults);

        VemoAPI.ResultResponse genParentResult = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(genParentApiInfo);
        System.assertEquals(testDocMap.size(), genParentResult.numberOfResults);
        Test.stopTest();
    }
    
    static testMethod void testHandleGetV2(){
        
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        Map<Id, IncomeVerification__c> testIncVerMap = TestDataFactory.createAndInsertIncomeVerification(1, testEmpHisMap);
        Map<Id, GenericDocument__c> testDocMap = TestDataFactory.createAndInsertGenericDocument(TestUtil.TEST_THROTTLE);
        Set<String> incStrings = new Set<String>();     
        for(IncomeVerification__c inc : testIncVerMap.values()){
            incStrings.add(((String)inc.id).subString(0,15));
        }
        for(GenericDocument__c genDoc : testDocMap.values()){
            genDoc.ParentID__c = testIncVerMap.values()[0].id;
        }
        
        Map<String, String> genDocParams = new Map<String, String>();
        genDocParams.put('genericDocumentID', TestUtil.createStringFromIDSet(testDocMap.keySet()));
        genDocParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo genDocApiInfo = TestUtil.initializeAPI('v2', 'GET', genDocParams, null);

        Map<String, String> genParentParams = new Map<String, String>();
        genParentParams.put('parentID', TestUtil.createStringFromIDSet(testIncVerMap.keyset()));
        genParentParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo genParentAPIInfo = TestUtil.initializeAPI('v2', 'GET', genParentParams, null);

        Test.startTest();
        VemoAPI.ResultResponse genDocResult = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(genDocApiInfo);
        System.assertEquals(testDocMap.size(), genDocResult.numberOfResults);

        VemoAPI.ResultResponse genParentResult = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(genParentApiInfo);
        System.assertEquals(testDocMap.size(), genParentResult.numberOfResults);
        Test.stopTest();
    }
    
    static testMethod void testHandlePostV1(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, EmploymentHistory__c> testEmpHisMap = TestDataFactory.createAndInsertEmploymentHistory(1, testStudentAccountMap);
        Map<Id, IncomeVerification__c> testIncVerMap = TestDataFactory.createAndInsertIncomeVerification(1, testEmpHisMap);
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1> genDocList = new List<VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1 genDoc = new VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1(true);
            genDoc.parentID = testIncVerMap.values().get(0).id; 
            genDocList.add(genDoc);
        }
        String body = JSON.serialize(genDocList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'POST', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }
    
    static testMethod void testHandlePutV1(){
        Map<Id, GenericDocument__c> testDocMap = TestDataFactory.createAndInsertGenericDocument(TestUtil.TEST_THROTTLE);

        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');
        
        List<VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1> genDocList = new List<VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1 genDoc = new VemoAPIGenericDocumentResource.GenericDocumentResourceInputV1(true);
            genDoc.genericDocumentID = testDocMap.values().get(i).ID;
            genDoc.status = 'Uploaded';
            genDocList.add(genDoc);
        }
        String body = JSON.serialize(genDocList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);

        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();        
    }

    static testMethod void testHandleDeleteV1(){
        Map<Id, GenericDocument__c> testDocMap = TestDataFactory.createAndInsertGenericDocument(TestUtil.TEST_THROTTLE);
        
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'testStudent_'+'abc');      
        params.put('genericDocumentID', TestUtil.createStringFromIDSet(testDocMap.keySet()));

        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'DELETE', params, null);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIGenericDocumentResource.handleAPI(apiInfo);
        System.assertEquals(TestUtil.TEST_THROTTLE, result.numberOfResults);
        Test.stopTest();
    }
}