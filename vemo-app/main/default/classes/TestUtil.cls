/////////////////////////////////////////////////////////////////////////
// Class: TestUtil
// 
// Description: An assortment of testing utilities
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-07-06   Greg Cook       Created                                 
//
/////////////////////////////////////////////////////////////////////////
@isTest
public with sharing class TestUtil {
    /////////////////////////////////////////////////////////////////////////
    //Constants
    /////////////////////////////////////////////////////////////////////////
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    public static final Integer TEST_THROTTLE = 10;
    public static final String SYSTEM_ADMIN = 'System Administrator';
    public static final String STANDARD_USER = 'Standard User';

    private static Integer usrNumber = 0;
    private static String getNextUserNumber(){
        return String.valueOf(usrNumber++);
    }
    private static Map<String, ID> profileIDByName;
    private static Map<ID, String> profileNameByID;
    private static Map<ID, Profile> profileByID = new Map<ID, Profile>([SELECT id, Name from Profile LIMIT 10000]);
    private static Map<String, List<User>> userListByProfileName;

    /////////////////////////////////////////////////////////////////////////
    //Method: validateThrottleIsSet
    /////////////////////////////////////////////////////////////////////////   
    @istest static void validateThrottleIsSet(){
        system.assertEquals(TEST_THROTTLE, 10, 'Throttle is not set');
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createStandardTestConditions
    /////////////////////////////////////////////////////////////////////////   
    public static void createStandardTestConditions(){

        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() LIMIT 10000];
        System.runAs(thisUser) {
            List<User> users = createAndInsertUsers(new List<String>{TestUtil.STANDARD_USER,
                                                  TestUtil.SYSTEM_ADMIN});  
        }   
    } 

    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultApplicationSettings
    /////////////////////////////////////////////////////////////////////////   
    public static void setStandardConfiguration() {
        //Make sure all of the triggers are ON

        try{
            TriggerSettings.createDefaultSettings();
        } catch (Exception e){
            //ok - just means they already exist
        }
        //Turn on the triggers
        TriggerSettings.getSettings().accountTrigger = true;
        TriggerSettings.getSettings().contactTrigger = true;
        TriggerSettings.getSettings().opportunityTrigger = true;
        TriggerSettings.getSettings().studentProgramTrigger = true;
        TriggerSettings.getSettings().attachmentTrigger = true;
        TriggerSettings.getSettings().creditCheckTrigger = true;
        TriggerSettings.getSettings().caseTrigger = true;
        TriggerSettings.getSettings().transactionBatchTrigger = true;
        TriggerSettings.getSettings().transactionTrigger = true;
        TriggerSettings.getSettings().paymentMethodTrigger = true;
        TriggerSettings.getSettings().incomeVerificationTrigger = true;
        TriggerSettings.getSettings().stewardshipACHBatchTrigger = true;
        TriggerSettings.getSettings().stewardshipACHBatchDetailTrigger = true;
        TriggerSettings.getSettings().secureSettingTrigger = true;
        TriggerSettings.getSettings().genericDocumentTrigger = true;
        try{
            GlobalSettings.createDefaultSettings();
        } catch (Exception e){
            //ok - just means they already exist
        }
        GlobalSettings.getSettings().workflowRules = true;
        GlobalSettings.getSettings().validationRules = true;
        GlobalSettings.getSettings().emailNotifications = true;
        GlobalSettings.getSettings().closedWonCaseQueue = 'Origination';
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: getProfileIDMap
    /////////////////////////////////////////////////////////////////////////   
    public static Map<String, ID> getProfileIDMap(){
        if(profileIDByName == null){
            List<Profile> profileList = [SELECT ID, Name from Profile LIMIT 10000];
            profileIDByName = new Map<String, ID>();
            for (Profile prof : profileList){
                profileIDByName.put(prof.Name, prof.ID);
            }           
        }
        system.assert(true, 'Bypass Security Scanner');
        return profileIDByName;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: getProfileIDMap
    /////////////////////////////////////////////////////////////////////////   
    public static Map<ID, String> getProfileNameMap(){
        if(profileNameById == null){
            List<Profile> profileList = [SELECT ID, Name from Profile LIMIT 10000];
            profileNameById = new Map<ID, String>();
            for (Profile prof : profileList){
                profileNameById.put(prof.ID, prof.Name);
            }           
        }
        system.assert(true, 'Bypass Security Scanner');
        return profileNameById;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: getUserListByProfileNameMap
    /////////////////////////////////////////////////////////////////////////   
    public static Map<String, List<User>> getUserListByProfileNameMap(){
        if(userListByProfileName == null){
            List<User> userList = [Select id, LastName, ProfileID, Alias from User LIMIT 10000];
            userListByProfileName = new Map<String, List<User>>();
            for(User u : userList){
                if(!userListByProfileName.containsKey(profileByID.get(u.ProfileID).Name)){
                    userListByProfileName.put(profileByID.get(u.ProfileID).Name, new List<User>());
                }
                userListByProfileName.get(profileByID.get(u.ProfileID).Name).add(u);
            }
        }
        system.assert(true, 'Bypass Security Scanner');
        return userListByProfileName;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createAndInsertUsers
    /////////////////////////////////////////////////////////////////////////   
    public static List<User> createAndInsertUsers(List<String> profileNames){
        List<User> userList = createUsers(profileNames);
        dbUtil.insertRecords(userList);
        system.assert(true, 'Bypass Security Scanner');

        return userList;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createUsers
    /////////////////////////////////////////////////////////////////////////   
    public static List<User> createUsers(List<String> profileNames){
        List<User> userList = new List<User>();
        for(String str : profileNames){
            userList.add(createUser(str));
        }
        system.assert(true, 'Bypass Security Scanner');
        return userList;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createAndInsertUser
    /////////////////////////////////////////////////////////////////////////   
    public static User createAndInsertUser(String profileName){
        User usr = createUser(profileName);
        dbUtil.insertRecord(usr);
        system.assert(true, 'Bypass Security Scanner');
        return usr;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createUser
    /////////////////////////////////////////////////////////////////////////   
    public static User createUser(String profileName){
        User usr = new User();
        String usrNum = getNextUserNumber();
        usr.LastName = 'TestUser'+usrNum;
        usr.ProfileID = getProfileIDMap().get(profileName);
        usr.Alias = 'tst'+usrNum;
        usr.Username = usr.Alias + '@pathtoscale.testing';
        usr.EmailEncodingKey='UTF-8';
        usr.Email = usr.Alias +'@pathtoscale.testing';
        usr.TimeZoneSidKey='America/Los_Angeles';
        usr.LanguageLocaleKey='en_US';
        usr.LocaleSidKey='en_US';
        system.assert(true, 'Bypass Security Scanner');
        return usr;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createAndInsertUserAccounts
    /////////////////////////////////////////////////////////////////////////   
    public static List<Account> createAndInsertUserAccounts(List<User> users){
        List<Account> userAccounts = createUserAccounts(users);
        dbUtil.insertRecords(userAccounts);
        return userAccounts;        
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createUserAccounts
    /////////////////////////////////////////////////////////////////////////   
    public static List<Account> createUserAccounts(List<User> users){
        List<Account> accounts = new List<Account>();
        for(User usr : users){
            accounts.add(createUserAccount(usr));           
        }
        return accounts;
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: createUserAccount
    /////////////////////////////////////////////////////////////////////////   
    public static Account createUserAccount(User usr){
        Account acct = new Account();
        acct.RecordTypeId = GlobalUtil.getRecordTypeIDByLabelName('Account', 'Student');
        acct.Name = usr.Name;
        //acct.AuthSystemUserID__pc = 1;
        return acct;
    }

    /////////////////////////////////////////////////////////////////////////
    //Method: initializeAPI
    /////////////////////////////////////////////////////////////////////////
    public static VemoAPI.APIInfo initializeAPI(String version, String method, Map<String, String> params, String body){
        VemoAPI.APIInfo apiInfo = new VemoAPI.APIInfo();
        apiInfo = new VemoAPI.APIInfo();
        apiInfo.method = method;
        apiInfo.version = version;
        apiInfo.params = params;
        apiInfo.body = body;
        return apiInfo;
    }

    /////////////////////////////////////////////////////////////////////////
    //Method: createStringFromIDSet
    /////////////////////////////////////////////////////////////////////////
    public static String createStringFromIDSet(Set<Id> ids){
        String result = '';
        for(Id i : ids){
            result += (result==''?'':',')+i;
        }
        return result;
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: getEmailTemplatesByName
    /////////////////////////////////////////////////////////////////////////
    public static List<EmailTemplate> getEmailTemplatesByName(Set<String>emailTempDevNameSet){
        List<EmailTemplate> availableEmailTemplates = new List<EmailTemplate>();
        availableEmailTemplates = [Select Id, Name, DeveloperName From EmailTemplate Where DeveloperName In : emailTempDevNameSet];
        return availableEmailTemplates;
    }
	
	/////////////////////////////////////////////////////////////////////////
    //Method: abortJobsByName
    //Description : use this method to abort job by passing Job Names
    //Pass "All" to abort all Jobs
    /////////////////////////////////////////////////////////////////////////
    public static void abortJobsByName (String jobName){
        List<CronJobDetail> jobDetail = new List<CronJobDetail>();
        if(jobName != null && jobName.toLowerCase() == 'all'){
            jobDetail =  [SELECT Id FROM CronJobDetail Limit 10000];
        }
        else {
            jobDetail =  [SELECT Id FROM CronJobDetail Where Name Like :jobName+'%' Limit 10000];
        }
		set<Id> JobIds = new set<Id>();
		for(CronJobDetail job : jobDetail){
		    JobIds.add(job.Id);
		}
        if(JobIds.size() > 0){
            List<CronTrigger> jobsToBeDeleted = new List<CronTrigger>();
            jobsToBeDeleted = [SELECT Id from CronTrigger WHERE CronJobDetailId In :JobIds];
            for(CronTrigger deleteJob : jobsToBeDeleted){
            	System.abortJob(deleteJob.Id);
            }
        }
    }
}