@isTest
public class LogService_TEST {

    public static testMethod void testService() {
        LogService.purgeLogs();
        LogService.debug('hello','test');
        LogService.fine('hello','test');
        LogService.critical('world','test');
        LogService.writeLogs();
    }
}