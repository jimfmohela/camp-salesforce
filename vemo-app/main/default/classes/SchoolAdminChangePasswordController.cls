/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class SchoolAdminChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}
    public String pageMessage {get;set;}
    
    public PageReference changePassword() {
    	pageMessage = '';
        System.debug('SchoolAdminChangePasswordController.changePassword()');
    	oldPassword = Apexpages.currentPage().getParameters().get('oldPasswordJS');
    	newPassword = Apexpages.currentPage().getParameters().get('newPasswordJS');
    	verifyNewPassword = Apexpages.currentPage().getParameters().get('verifyNewPasswordJS');
    	System.debug('oldPassword-->'+oldPassword);
    	System.debug('newPassword-->'+newPassword);
    	System.debug('verifyNewPassword-->'+verifyNewPassword);
    	if(oldPassword != null && String.isBlank(oldPassword)){
    		oldPassword = null;
    	}
        PageReference pgToReturn = Site.changePassword(newPassword, verifyNewPassword, oldPassword);
        //PageReference pgToReturn = new PageReference('www.google.com');
        if(pgToReturn == NULL){
        	List<ApexPages.Message> pageMessages = new List<ApexPages.Message>();
        	pageMessages = ApexPages.getMessages();
        	if(pageMessages.size() > 0){
        		pageMessage = pageMessages[pageMessages.size()-1].getSummary();
        		System.debug(pageMessage);
        	}
        }
        else {
        	return pgToReturn;
        }
        return null;
    }     
    
   	public SchoolAdminChangePasswordController() {
           System.debug('SchoolAdminChangePasswordController()');
           pageMessage = '';
       }
}