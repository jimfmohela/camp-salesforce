public class CalculateTotals{

    //public class CalculateDisbursementTotalOnAgreement implements Queueable {
    
    //    public CalculateDisbursementTotalOnAgreement(set<Id> parentIds){
    //        this.parentIds = parentIds;
    //    }
        
    //    Set<Id> parentIds;
        
    //    public void execute(QueueableContext context){
    //        Map<ID, StudentProgram__c> AgreementMapWithID = StudentProgramQueries.getStudentProgramMapWithAgreementID(parentIds); 
    //        Map<ID, List<Transaction__c>> transByAgreementId = TransactionQueries.getTransactionMapByAgreementWithAgreementID(parentIds, 'Disbursement');
            
    //        List<StudentProgram__c> agreementsToUpdate = new List<StudentProgram__c>();
    //        for(Id agreementId: AgreementMapWithID.keySet()){
    //            Decimal DisbursedAmount = 0;
    //            if(transByAgreementID != null && transByAgreementID.containsKey(agreementId)){ 
    //                for(Transaction__c tx: transByAgreementID.get(agreementId)){
    //                    if(tx.Status__c == 'Complete' && tx.Confirmed__c == true){
    //                        if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;    
    //                    }
                        
    //                }
    //            }
    //            agreementsToUpdate.add(new StudentProgram__c(id = agreementId, 
    //                                                         AmountDisbursedToDate__c =  DisbursedAmount,
    //                                                         CalculateAmounts__c = false));
    //            //system.debug('Updated:'+agreementsToUpdate);                                                                                                         
    //        }
    //        if(agreementsToUpdate.size()>0)
    //            update agreementsToUpdate;
    //    }
    //}
    
    //public class CalculateTotalsOnProgram implements Queueable {
    
    //    public CalculateTotalsOnProgram(set<Id> parentIds){
    //        this.parentIds = parentIds;
    //    }
        
    //    Set<Id> parentIds;
        
    //    public void execute(QueueableContext context){
    //        Map<ID, Program__c> ProgramsWithID = ProgramQueries.getProgramMapWithProgramID(parentIds);
    //        Map<ID, StudentProgram__c> AgreementMapWithProgramID = StudentProgramQueries.getStudentProgramMapWithProgramID(parentIds); 
    //        Map<ID, List<Transaction__c>> transByAgreementId = new Map<ID, List<Transaction__c>>();
            
    //        if(AgreementMapWithProgramID != null){
    //            transByAgreementId  = TransactionQueries.getTransactionMapByAgreementWithAgreementID(AgreementMapWithProgramID.keySet(), 'Disbursement');
    //        }
            
    //        Map<ID, List<StudentProgram__c>> AgreementMapByProgramID = new Map<ID, List<StudentProgram__c>>();
            
    //        List<Program__c> programsToUpdate = new List<Program__c>();
            
    //        if(AgreementMapWithProgramID != null){
    //            for(StudentProgram__c agreement: AgreementMapWithProgramID.values()){
    //                if(!AgreementMapByProgramID.containsKey(agreement.Program__c)){
    //                    AgreementMapByProgramID.put(agreement.program__c, new List<StudentProgram__c>()); 
    //                }
    //                AgreementMapByProgramID.get(agreement.program__c).add(agreement);
    //            }
    //        }
            
    //        for(Program__c prog: ProgramsWithID.values()){
    //            Decimal CertifiedAmount = 0;
    //            Decimal DisbursedAmount = 0;
    //            if(AgreementMapByProgramID.containsKey(prog.Id)){
    //                for(StudentProgram__c sp: AgreementMapByProgramID.get(prog.Id)){
    //                    if(sp.CertificationDate__c != null){
    //                        if(sp.FundingAmountPostCertification__c != null) CertifiedAmount += sp.FundingAmountPostCertification__c;
    //                    }
                        
    //                    if(transByAgreementID != null && transByAgreementID.containsKey(sp.id)){ 
    //                        for(Transaction__c tx: transByAgreementID.get(sp.id)){
    //                            if(tx.Status__c == 'Complete' && tx.Confirmed__c == true){
    //                                if(tx.Amount__c != null) DisbursedAmount += tx.Amount__c;    
    //                            }
                                
    //                        }
    //                    }
    //                }
    //            }
                
    //            programsToUpdate.add(new Program__c(id = prog.Id,
    //                                                AmountCertifiedToDate__c = CertifiedAmount,
    //                                                AmountDisbursedToDate__c = DisbursedAmount,
    //                                                CalculateAmounts__c = false)); 
    //        }
            
    //        if(programsToUpdate.size()>0)
    //            update programsToUpdate;
    //    }
    //}
}