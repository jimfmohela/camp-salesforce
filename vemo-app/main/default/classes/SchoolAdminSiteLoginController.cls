global with sharing class SchoolAdminSiteLoginController {
    global String username {get; set;}
    global String password {get; set;}
    global SchoolAdminSiteLoginController(){
        System.debug('SchoolAdminSiteLoginController()');
    }

    global PageReference login() {
        System.debug('SchoolAdminSiteLoginController.login()');
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }
    
}