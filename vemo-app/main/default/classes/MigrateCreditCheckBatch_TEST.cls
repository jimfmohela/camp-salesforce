@isTest
public with sharing class MigrateCreditCheckBatch_TEST {
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest static void MigrateCreditCheckBatch() {
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        for(Account stud : studentMap.values()){
            stud.PrimarySchool__pc = schools.values()[0].id;
        }
        update studentMap.values();
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schools);//TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, CreditCheck__c> creditCheckMap = TestDataFactory.createAndInsertCreditCheck(1,studentMap);
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);
        
        for(CreditCheck__c cc: creditCheckMap.values()){
            cc.CreditCheckDeniedReason__c = 'Technical Error;'; 
        }
        dbUtil.updateRecords(creditCheckMap.values());   
        dbUtil.deleteRecords(creditCheckMap.values());   
        dbUtil.undeleteRecords(creditCheckMap.values());   
        
        Test.startTest();
        for(StudentProgram__c sp: agreementMap.values()){
            sp.CreditCheck__c = creditCheckMap.values()[0].id; 
        }
        dbUtil.updateRecords(agreementMap.values());   
        MigrateCreditCheckBatch job = new MigrateCreditCheckBatch('');
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    } 
}