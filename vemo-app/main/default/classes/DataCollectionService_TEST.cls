/////////////////////////////////////////////////////////////////////////
// Class: DataCollectionService_TEST
// 
// Description: 
//  Unit test for DataCollectionService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-05   Rini Gupta  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class DataCollectionService_TEST{
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void validateGetDataCollectionMapWithID(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
        Test.startTest();
        List<DataCollectionService.DataCollection> resultDataCollectionList = DataCollectionService.getDataCollectionMapWithId(dataCollectionMap.keyset());
        System.assertEquals(DataCollectionMap.keySet().size(), resultDataCollectionList.size());
        Test.stopTest();
    }
    
    static testMethod void validateGetDataCollectionMapWithAgreement(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
        Test.startTest();
        List<DataCollectionService.DataCollection> resultDataCollectionList = DataCollectionService.getDataCollectionMapWithAgreement(agreementMap.keyset());
        System.assertEquals(DataCollectionMap.keySet().size(), resultDataCollectionList.size());
        Test.stopTest();
    }
    
    static testMethod void validateCreateDataCollections(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        //Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);
        Test.startTest();
        List<DataCollectionService.DataCollection> resultDataCollectionList = new List<DataCollectionService.DataCollection>();
        for(Integer i = 0; i<TemplateMap.size(); i++){
            for(Integer j = 0; j<agreementMap.size(); j++){
                DataCollectionService.DataCollection dc = new DataCollectionService.DataCollection();
                dc.templateId = TemplateMap.values().get(i).id;
                dc.agreementId = agreementMap.values().get(j).id; 
                dc.DataType = 'String';
                dc.StringValue = 'test';
                resultDataCollectionList.add(dc);
            }
        }
        Set<ID> dcIds = DataCollectionService.CreateDataCollections(resultDataCollectionList);
        System.assertEquals(resultDataCollectionList.size(), DataCollectionQueries.getDataCollectionMapWithID(dcIds).size());
        Test.stopTest();
    }
    
 }