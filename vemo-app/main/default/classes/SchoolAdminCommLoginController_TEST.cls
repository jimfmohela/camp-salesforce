/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class SchoolAdminCommLoginController_TEST {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
     	SchoolAdminCommLoginController controller = new SchoolAdminCommLoginController();
     	System.assertEquals(null, controller.forwardToAuthPage());       
    }    
}