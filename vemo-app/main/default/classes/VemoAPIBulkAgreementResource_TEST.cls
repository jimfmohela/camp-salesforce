/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIBulkAgreementResource_TEST 
// 
// Description: 
//   Test Class for Bulk Agreement Resource API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-04   Kamini Singh       Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIBulkAgreementResource_TEST {
    @TestSetup static void setupData(){
      //  TestUtil.createStandardTestConditions();
    }
    
    @isTest
    static void validateHandleGetV1(){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(TestUtil.TEST_THROTTLE, students, programs);
        
        Map<String, String> schlParams = new Map<String, String>();
        schlParams.put('VEMO_AUTH', 'ABC');
        String schoolId = schools.values().get(0).Id;
        schlParams.put('schoolID', schoolId);
        VemoAPI.APIInfo schlApiInfo = TestUtil.initializeAPI('v2', 'GET', schlParams, null);
        
        Map<String, String> prgParams = new Map<String, String>();
        prgParams.put('VEMO_AUTH', 'ABC');
        prgParams.put('programID', (String)studProgramMap.values().get(0).Program__c);
        VemoAPI.APIInfo prgApiInfo = TestUtil.initializeAPI('v2', 'GET', prgParams, null);
        
        Test.startTest();
        VemoAPI.ResultResponse scholResult = (VemoAPI.ResultResponse)VemoAPIBulkAgreementResource.handleAPI(schlApiInfo);
        System.assertEquals(200, scholResult.numberOfResults);
        VemoAPI.ResultResponse prgResult = (VemoAPI.ResultResponse)VemoAPIBulkAgreementResource.handleAPI(prgApiInfo);
        System.assertEquals(200, prgResult.numberOfResults);
        Test.stopTest();        

    } 
}