/////////////////////////////////////////////////////////////////////////
// Class: EventSubscriptionQueries_TEST
// 
// Description: 
//  Unit test for EventSubscriptionQueries
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventSubscriptionQueries_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventSubscriptionMapWithAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes );
        Test.startTest();
        Map<Id, EventSubscription__c> resultEventSubscriptionMap = EventSubscriptionQueries.getEventSubscriptionMapWithAuthUser(testContactMap.keyset());
        System.assertEquals(testEventSubscriptionMap.keySet().size(), resultEventSubscriptionMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testGetEventSubscriptionMapWithEventSubscriptionID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> eventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, eventTypes );
        Test.startTest();
        Map<Id, EventSubscription__c> resultEventSubscriptionMap = EventSubscriptionQueries.getEventSubscriptionMapWithEventSubscriptionID(testEventSubscriptionMap.keyset());
        System.assertEquals(testEventSubscriptionMap.keySet().size(), resultEventSubscriptionMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testGetEventSubscriptionMapWithEventTypes(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> testEvenTypeMap = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<Id, EventSubscription__c> testEventSubscriptionMap = TestDataFactory.createAndInsertEventSubscription(testContactMap, testEvenTypeMap);
        Test.startTest();
        Map<Id, EventSubscription__c> resultEventSubscriptionMap = EventSubscriptionQueries.getEventSubscriptionMapWithEventTypes(testStudentAccountMap.keyset(), testEvenTypeMap.keyset());
        System.assertEquals(testEventSubscriptionMap.keySet().size(), resultEventSubscriptionMap.keySet().size());
        Test.stopTest();
    }
}