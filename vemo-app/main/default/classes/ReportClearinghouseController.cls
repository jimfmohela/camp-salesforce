public with sharing class ReportClearinghouseController{
    public String selectedSchool {get;set;}
    transient public List<reportDataWrapper> reportData {get;set;}   
    transient public String csv {get;set;}
    public ClrHouseStudentEnrollment__c clrHouseCreatedDate {get;set;}
    public ClrHouseStudentEnrollment__c clrHouseFileReceivedDate {get;set;}
    public String selectedUsers {get;set;}
    public String selectedOperator {get;set;}    
    public string mode {get;set;}
    
    
    private static Set<String> exclusionStatus = new Set<String>{'Draft','Invited','Application Incomplete','Application Complete','Application Under Review','Cancelled','Closed'};
    private static Set<String> exclusionStatusDateFilter = new Set<String>{'Cancelled','Closed','Pending Reconciliation'};  
    public string pageName; 
    public ReportClearinghouseController(){
        selectedSchool = '';
        reportData = new List<reportDataWrapper>();        
        clrHouseCreatedDate = new ClrHouseStudentEnrollment__c();
        clrHouseFileReceivedDate = new ClrHouseStudentEnrollment__c();
        
        if(ApexPages.currentPage().getParameters().get('mode') != null) mode = ApexPages.currentPage().getParameters().get('mode');
    }
    
    public List<SelectOption> getSchools(){
        //List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By name ASC];
        Map<ID, Account> acctMap = new Map<ID, Account>();
        string query ='Select id,name From Account ';
        query += ' WHERE ' + generateRecordTypeStatement('School - Customer');
        query += ' '+ 'Order By name ASC';
        
        DatabaseUtil db = new DatabaseUtil();
        acctMap = new Map<ID, Account>((List<Account>)db.query(query));
        List<SelectOption> options = new List<SelectOption>();
        for(Id id: AcctMap.keyset()){
            options.add(new SelectOption(id,AcctMap.get(id).name));    
        }
        return options;
    }
    
    public List<SelectOption> getUsers(){
        //List<Account> schools = [Select id,name From Account Where RecordType.developerName = 'SchoolCustomer' Order By name ASC];
        Map<ID, User> userMap = new Map<ID, User>();
        string query ='Select id,name From User WHERE IsActive = true Order By name ASC';
        
        
        DatabaseUtil db = new DatabaseUtil();
        userMap = new Map<ID, User>((List<User>)db.query(query));
        List<SelectOption> options = new List<SelectOption>();
        for(Id id: userMap.keyset()){
            options.add(new SelectOption(id,userMap.get(id).name));    
        }
        return options;
    }
    
    public List<SelectOption> getOperators(){
        
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('Contains','Contains'));
        options.add(new SelectOption('Does not Contains','Does not Contains'));    
        
        return options;
    }
    
    private static String generateRecordTypeStatement(String recordTypeLabel){
        ID recordTypeID = (String)GlobalUtil.getRecordTypeIDByLabelName('Account', recordTypeLabel);
        return 'RecordTypeID = \''+ String.valueOf(recordTypeID) + '\'';
    }
    
    public void runReport(){
        ///////////////////////////////////////////////////////////////////////////////////
        Map<ID,List<StudentProgram__c>> agreementsByStudent = new Map<ID,List<StudentProgram__c>>();
        Map<String,Date> studentID_1stDisbursementDate = new Map<String,Date>();
        Map<String,Set<Date>> studentID_SetGradDt = new Map<String,Set<Date>>();
        
        Map<ID,List<ClrHouseStudentEnrollment__c>> clrHouseEnrollmentsByStudent = new Map<ID,List<ClrHouseStudentEnrollment__c>>(); 
        ///////////////////////////////////////////////////////////////////////////////////    
        getAgreements(agreementsByStudent,studentID_1stDisbursementDate,studentID_SetGradDt);
        getClrHouseEnrollments(clrHouseEnrollmentsByStudent,agreementsByStudent,studentID_1stDisbursementDate,studentID_SetGradDt);
        
        reportData = buildReportData(agreementsByStudent,clrHouseEnrollmentsByStudent);
    }
    
    ///////////////////////////////////////////////
    ///Get Student Programs for the selected School
    ///////////////////////////////////////////////
    public void getAgreements(Map<ID,List<StudentProgram__c>> agreementsByStudent,Map<String,Date> studentID_1stDisbursementDate,Map<String,Set<Date>> studentID_SetGradDt){
        String pageName = ApexPages.CurrentPage().getUrl();
        List<StudentProgram__c> agreementList = new List<StudentProgram__c>();
        if(!pageName.contains('DateFilters')){
            agreementList = [SELECT id,name,student__c,student__r.name,student__r.vemoAccountNumber__c,certificationDate__c,SchoolName__c,
                             expectedGraduationDate__c,lastDateOfAttendance__c,Servicing__c,ServicingStartDate__c,status__c,student__r.PrimarySchool__pr.Id
                             FROM StudentProgram__c
                             WHERE Status__c NOT IN :exclusionStatus AND Program__r.school__c =: selectedSchool
                             ORDER BY CertificationDate__c DESC];
        }
        else{
            agreementList = [SELECT id,name,student__c,student__r.name,student__r.vemoAccountNumber__c,certificationDate__c,SchoolName__c,
                             expectedGraduationDate__c,lastDateOfAttendance__c,Servicing__c,ServicingStartDate__c,status__c,student__r.PrimarySchool__pr.Id,
                             Deferment__r.Type__c,Deferment__r.EndDate__c,
                             (Select Id,TransactionDate__c from Transactions__r order by TransactionDate__c asc limit 1)
                             FROM StudentProgram__c 
                             WHERE Status__c NOT IN :exclusionStatusDateFilter
                             ORDER BY CertificationDate__c DESC];                                                      
        }           
        
        for(StudentProgram__c agreement:agreementList){
            if(!agreementsByStudent.containsKey(agreement.student__c)){
                agreementsByStudent.put(agreement.student__c,new List<StudentProgram__c>());
            }
            agreementsByStudent.get(agreement.student__c).add(agreement);
            if(agreement.Transactions__r.size()>0 && !studentID_1stDisbursementDate.containsKey(agreement.student__c)){
                studentID_1stDisbursementDate.put(agreement.student__c,agreement.Transactions__r[0].TransactionDate__c);
            }
            if(!studentID_SetGradDt.containsKey(agreement.student__c)){
                studentID_SetGradDt.put(agreement.student__c,new Set<Date>());    
            }
            studentID_SetGradDt.get(agreement.student__c).add(agreement.expectedGraduationDate__c);
            
        }
    }
    
    ////////////////////////////////////////////////////
    ///Get Clearinghouse Data for the given students school
    ////////////////////////////////////////////////////
    public void getClrHouseEnrollments(Map<ID,List<ClrHouseStudentEnrollment__c>> clrHouseEnrollmentsByStudent,Map<ID,List<StudentProgram__c>> agreementsByStudent,Map<String,Date> studentID_1stDisbursementDate,Map<String,Set<Date>> studentID_SetGradDt){
        String pageName = ApexPages.CurrentPage().getUrl();
        //string mode;
        /************* Map to store school code and school name (Need to replace this with custom setting)***************/
        Map<string,string> schoolCodeSchoolName = new Map<string,string>();
        schoolCodeSchoolName.put('1505','Watson');
        schoolCodeSchoolName.put('1825','Purdue');
        schoolCodeSchoolName.put('2699','Clarkson');
        schoolCodeSchoolName.put('9126','Berkeley');
        schoolCodeSchoolName.put('7502','Berkeley');
        schoolCodeSchoolName.put('8556','Berkeley');
        schoolCodeSchoolName.put('7421','Berkeley');
        schoolCodeSchoolName.put('7394','Berkeley');
        schoolCodeSchoolName.put('4506','Colorado');
        schoolCodeSchoolName.put('3298','Messiah');
        schoolCodeSchoolName.put('1262','Point');
        schoolCodeSchoolName.put('1554','Berry');
        schoolCodeSchoolName.put('2791','Pace');
        schoolCodeSchoolName.put('1088','Lyon');
        schoolCodeSchoolName.put('3360','Rosemont');
        schoolCodeSchoolName.put('1081','Arizona');
        schoolCodeSchoolName.put('3376','Thiel');
        schoolCodeSchoolName.put('3692','Norwich');
        schoolCodeSchoolName.put('3283','Lackawanna');
        schoolCodeSchoolName.put('1196','Make');
        schoolCodeSchoolName.put('3657','Utah');
        schoolCodeSchoolName.put('1801','Hanover');        
        /*******************************************************************************/
        
        List<ClrHouseStudentEnrollment__c> clrHouseEnrollmentList = new List<ClrHouseStudentEnrollment__c>();
        /*********** Need to write this in seperate method ******************/
        if(!pageName.contains('DateFilters')){
            clrHouseEnrollmentList = [SELECT id,name,Enrollment_status__c,Graduated__c,Graduation_date__c,College_Code_Branch__c,Search_Date__c,
                                     Enrollment_begin__c,Enrollment_end__c,College_Name__c,account__c,ExpectedGraduationDate__c
                                     FROM ClrHouseStudentEnrollment__c
                                     WHERE Account__c IN :agreementsByStudent.keySet()
                                     ORDER BY Enrollment_begin__c DESC];
        }
        else{            
            if(selectedUsers == '[]'){
                clrHouseEnrollmentList = [SELECT id,name,Enrollment_status__c,Graduated__c,Graduation_date__c,College_Code_Branch__c,Search_Date__c,
                                     Enrollment_begin__c,Enrollment_end__c,College_Name__c,account__c,ExpectedGraduationDate__c
                                     FROM ClrHouseStudentEnrollment__c
                                     WHERE DAY_ONLY(createdDate) >=: clrHouseCreatedDate.ClearinghouseFileReceivedDate__c AND ClearinghouseFileReceivedDate__c >=: clrHouseFileReceivedDate.ClearinghouseFileReceivedDate__c AND Account__c IN :agreementsByStudent.keySet() 
                                     ORDER BY Enrollment_begin__c DESC];
                //system.debug(clrHouseEnrollmentList.size());
            }
            else{
                List<Id> idList = selectedUsers.remove('[').remove(']').split(', ');
                //system.debug(idList);
                if(selectedOperator == 'Contains'){
                    clrHouseEnrollmentList = [SELECT id,name,Enrollment_status__c,Graduated__c,Graduation_date__c,College_Code_Branch__c,Search_Date__c,
                                     Enrollment_begin__c,Enrollment_end__c,College_Name__c,account__c,ExpectedGraduationDate__c
                                     FROM ClrHouseStudentEnrollment__c
                                     WHERE DAY_ONLY(createdDate) >=: clrHouseCreatedDate.ClearinghouseFileReceivedDate__c AND ClearinghouseFileReceivedDate__c >=: clrHouseFileReceivedDate.ClearinghouseFileReceivedDate__c AND Account__c IN :agreementsByStudent.keySet()
                                     AND LastModifiedBy.Id IN:idList ORDER BY Enrollment_begin__c DESC];
                    //system.debug(clrHouseEnrollmentList.size());
                }
                else{
                    clrHouseEnrollmentList = [SELECT id,name,Enrollment_status__c,Graduated__c,Graduation_date__c,College_Code_Branch__c,Search_Date__c,
                                     Enrollment_begin__c,Enrollment_end__c,College_Name__c,account__c,ExpectedGraduationDate__c
                                     FROM ClrHouseStudentEnrollment__c
                                     WHERE DAY_ONLY(createdDate) >=: clrHouseCreatedDate.ClearinghouseFileReceivedDate__c AND ClearinghouseFileReceivedDate__c >=: clrHouseFileReceivedDate.ClearinghouseFileReceivedDate__c AND Account__c IN :agreementsByStudent.keySet()
                                     AND LastModifiedBy.Id NOT IN : idList ORDER BY Enrollment_begin__c DESC];
                }
            }
            
            
        }
        /*******************************************************************************/
        Set<Id> clrIds = new Set<Id>();
        for(ClrHouseStudentEnrollment__c clrHouseEnrollment:clrHouseEnrollmentList){
            if(mode == 'copy'){                
                if(convertTextToDate(clrHouseEnrollment.Enrollment_end__c) >= studentID_1stDisbursementDate.get(clrHouseEnrollment.account__c)){
                    for(StudentProgram__c sp: agreementsByStudent.get(clrHouseEnrollment.account__c)){
                        if(clrHouseEnrollment.ExpectedGraduationDate__c != null && clrHouseEnrollment.ExpectedGraduationDate__c != ''){
                            if(sp.status__c != 'Deferment'){
                                for(Date dt:studentID_SetGradDt.get(clrHouseEnrollment.account__c)){
                                    if(convertTextToDate(clrHouseEnrollment.ExpectedGraduationDate__c) != dt){
                                        putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                            
                                        clrIds.add(clrHouseEnrollment.id);
                                    }
                                    else{
                                        if(clrHouseEnrollment.Enrollment_status__c != 'F'){
                                           putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                                
                                           clrIds.add(clrHouseEnrollment.id);
                                        }
                                    }
                                    
                                }                            
                            }
                            else{
                                if(sp.Transactions__r.size()>0 && sp.Deferment__r.Type__c == 'Enrolled in School' && convertTextToDate(clrHouseEnrollment.ExpectedGraduationDate__c) != sp.Deferment__r.EndDate__c){
                                    putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                        
                                    clrIds.add(clrHouseEnrollment.id);
                                }
                            }
                        }
                        else{
                            if(sp.Transactions__r.size()>0 && sp.Deferment__r.Type__c == 'Enrolled in School' && convertTextToDate(clrHouseEnrollment.Enrollment_end__c) != sp.Deferment__r.EndDate__c){
                                putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                    
                                clrIds.add(clrHouseEnrollment.id);
                            }
                            if(sp.status__c != 'Deferment'){
                                if(clrHouseEnrollment.College_Name__c != null && clrHouseEnrollment.College_Name__c != ''){
                                    string collegeName = clrHouseEnrollment.College_Name__c.split('')[0];
                                    string schoolName = sp.SchoolName__c.split('')[0];
                                    if(collegeName.toLowerCase() != schoolName.toLowerCase()){
                                        putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                    
                                        clrIds.add(clrHouseEnrollment.id);
                                    }
                                }
                                else{
                                    if(clrHouseEnrollment.College_Code_Branch__c != null && clrHouseEnrollment.College_Code_Branch__c != ''){
                                        string schoolName = sp.SchoolName__c.split('')[0];
                                        string collegeCode = clrHouseEnrollment.College_Code_Branch__c.split('-')[0];
                                        if(collegeCode.length()>4)
                                            collegeCode = collegeCode.subString(collegeCode.length() - 4);
                                            
                                        if(schoolCodeSchoolName.containsKey(collegeCode) && schoolCodeSchoolName.get(collegeCode).toLowerCase() != schoolName.toLowerCase()){
                                            putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);                                    
                                            clrIds.add(clrHouseEnrollment.id);
                                        }
                                    }
                                }
                            }
                        }                     
                    }
                }
                
            }
            else{
                if(mode != 'copy'){
                    putInMap(clrHouseEnrollment,clrHouseEnrollmentsByStudent);
                }
            }
        }
        //system.debug(clrHouseEnrollmentsByStudent);
    }
    
    public void putInMap(ClrHouseStudentEnrollment__c clrHouseEnrollment, Map<ID,List<ClrHouseStudentEnrollment__c>> clrHouseEnrollmentsByStudent){
        if(!clrHouseEnrollmentsByStudent.containsKey(clrHouseEnrollment.account__c)){
            clrHouseEnrollmentsByStudent.put(clrHouseEnrollment.account__c,new List<ClrHouseStudentEnrollment__c>());    
        }
        clrHouseEnrollmentsByStudent.get(clrHouseEnrollment.account__c).add(clrHouseEnrollment);
    }
    
    ////////////////////////////////////////////////////////
    ///Build final report data
    ////////////////////////////////////////////////////////
    public List<ReportDataWrapper> buildReportData(Map<ID,List<StudentProgram__c>> agreementsByStudent,Map<ID,List<ClrHouseStudentEnrollment__c>> clrHouseEnrollmentsByStudent){
        List<ReportDataWrapper> output = new List<ReportDataWrapper>();
        //string mode;
        //if(ApexPages.currentPage().getParameters().get('mode') != null) mode = ApexPages.currentPage().getParameters().get('mode');
        for(ID studentID:clrHouseEnrollmentsByStudent.keySet()){
            Date earliestCertDate = null;
            //get the earliest cert date across all agreements
            for(StudentProgram__c agreement:agreementsByStudent.get(studentID)){
                if(agreement.certificationDate__c <> null){
                    if(earliestCertDate == null || earliestCertDate > agreement.certificationDate__c.date()) earliestCertDate = agreement.certificationDate__c.date();            
                }
            }
            
            if(earliestCertDate == null) continue;
            
            //iterate on ClrHouse enrollement records to check if the student needs to appear on the report OR not
            Boolean showOnReport = false;
            for(ClrHouseStudentEnrollment__c clrHouse:clrHouseEnrollmentsByStudent.get(studentID)){
                Date enrollmentEndDate;
                if(clrHouse.Graduated__c == 'N') enrollmentEndDate = convertTextToDate(clrHouse.Enrollment_end__c);
                if(clrHouse.Graduated__c == 'Y') enrollmentEndDate = convertTextToDate(clrHouse.Graduation_date__c);
                
                if(enrollmentEndDate == null) continue; 
                if(enrollmentEndDate < earliestCertDate) continue;
                if(clrHouse.Graduated__c == 'Y'){
                     showOnReport = true;
                     break;   
                }
                if(clrHouse.Enrollment_status__c == 'L' || clrHouse.Enrollment_status__c == 'W' ){
                     showOnReport = true;
                     break;
                }
            }
            
            //if showOnReport is true format the output
            String pageName = ApexPages.CurrentPage().getUrl();
            if(showOnReport || pageName.contains('DateFilters')){
                ReportDataWrapper wrapper = new ReportDataWrapper();        
                Integer sizeOfAgreements = agreementsByStudent.get(studentID).size();
                Integer sizeOfClrHouseEnrollments = clrHouseEnrollmentsByStudent.get(studentID).size();
                Integer interationCount = (sizeOfAgreements > sizeOfClrHouseEnrollments)? sizeOfAgreements:sizeOfClrHouseEnrollments; 
                List<StudentProgram__c> agreementList = agreementsByStudent.get(studentID);
                List<ClrHouseStudentEnrollment__c> clrHouseList = clrHouseEnrollmentsByStudent.get(studentID);
                
                for(integer index=0;index<interationCount;index++){
                    if(index == 0){
                        wrapper.vemoAccountNumber = agreementList[index].student__r.vemoAccountNumber__c;
                        wrapper.studentName = agreementList[index].student__r.name;
                        wrapper.studentId = agreementList[index].student__c;
                        /*wrapper.schoolName = agreementList[index].student__r.PrimarySchool__pr.Name;
                        wrapper.schoolId = agreementList[index].student__r.PrimarySchool__pr.Id;*/
                    }
                    
                    ReportRowWrapper row = new ReportRowWrapper();
                    if(index < sizeOfClrHouseEnrollments){
                        row.CHId = clrHouseList[index].id;
                        row.CHRecNumber = clrHouseList[index].name; 
                        row.collegeName = clrHouseList[index].college_Name__c;
                        row.collegeCode = clrHouseList[index].college_Code_Branch__c;
                        row.statusStartDate = convertTextToDate(clrHouseList[index].Search_Date__c);
                        row.ChExpectedGradDate = convertTextToDate(clrHouseList[index].ExpectedGraduationDate__c);
                        row.enrollmentStatus = clrHouseList[index].enrollment_status__c;
                        row.enrollmentBegin = convertTextToDate(clrHouseList[index].Enrollment_begin__c);
                        row.enrollmentEnd = convertTextToDate(clrHouseList[index].Enrollment_end__c);
                        row.graduated = clrHouseList[index].Graduated__c;
                        row.graduationDate = convertTextToDate(clrHouseList[index].Graduation_date__c);              
                    }
                    if(index<sizeOfAgreements){                        
                        row.SPId = agreementList[index].id;
                        row.SPNumber = agreementList[index].name;
                        row.SPSchoolName = agreementList[index].SchoolName__c; 
                        row.status = agreementList[index].status__c;
                        row.expectedGradDate = agreementList[index].expectedGraduationDate__c;
                        row.lastDateOfAttendance = agreementList[index].lastDateOfAttendance__c;
                        row.servicing = agreementList[index].servicing__c;
                        row.servicingStartDate = agreementList[index].servicingStartDate__c;                        
                    }
                    wrapper.rowData.add(row);                
                }
                
                output.add(wrapper);
            }        
        } 
        return output;       
    } 
    
    ////////////////////////////////////////////////////////
    ///Convert text field to date
    ///////////////////////////////////////////////////////
    public Date convertTextToDate(String dateStr){
        Integer year,month,day;
        Date outputDate;
        try{
            year = Integer.valueOf(dateStr.subString(0,4));
            month = Integer.valueOf(dateStr.subString(4,6)); 
            day = Integer.valueOf(dateStr.subString(6,8));
            outputDate = date.newinstance(year, month, day);
        }
        catch(Exception e){
        } 
        return outputDate; 

    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference export(){
        String pageName = ApexPages.CurrentPage().getUrl();
        if(pageName.contains('DateFilters')){
            PageReference pg = new PageReference('/apex/ReportClearinghouseDtFiltersExport');
            pg.setRedirect(false);
            return pg;
        }
        else{
            PageReference pg = new PageReference('/apex/ReportClearinghouseExport');
            pg.setRedirect(false);
            return pg;
        }
    }
    
    ////////////////////////////////////////////////
    ///Wrapper to hold complete report data
    //////////////////////////////////////////////// 
    public class ReportDataWrapper{
        public String vemoAccountNumber {get;set;}
        public String studentName {get;set;}
        public String studentId {get;set;}
        /*public String schoolName {get;set;}
        public String schoolId {get;set;}*/
        public List<reportRowWrapper> rowData {get;set;}
        
        public reportDataWrapper(){
            this.rowData = new List<reportRowWrapper>();    
        }
    }
    
    ////////////////////////////////////////////////
    ///Wrapper to hold row level Data
    ////////////////////////////////////////////////
    public class ReportRowWrapper{
        public String CHId {get;set;}
        public String CHRecNumber {get;set;}
        public String collegeName {get;set;}
        public String collegeCode {get;set;}
        public Date statusStartDate {get;set;}
        public Date ChExpectedGradDate {get;set;}
        public String enrollmentStatus {get;set;}
        public Date enrollmentBegin {get;set;}
        public Date enrollmentEnd {get;set;}
        public string graduated {get;set;}
        public Date graduationDate {get;set;}
        public String SPId {get;set;}
        public string SPNumber {get;set;}
        public string SpSchoolName {get;set;}
        public String status {get;set;}
        public Date expectedGradDate {get;set;}
        public Date lastDateOfAttendance {get;set;}
        public Boolean servicing {get;set;}
        public Date servicingStartDate {get;set;}
    }
}