public with sharing class EmploymentSeekingAttemptService {

    public static List<EmploymentSeekingAttempt> getEmploymentSeekingAttemptMapWithDefermentId(set<Id> defermentIDs){    
        Map<Id, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithDefermentID(defermentIDs);
        List<EmploymentSeekingAttempt> EmploymentSeekingAttemptList = new List<EmploymentSeekingAttempt>();
        for(EmploymentSeekingAttempt__c de : EmploymentSeekingAttemptMap.values()){
            EmploymentSeekingAttemptList.add(new EmploymentSeekingAttempt(de));
        }
        return EmploymentSeekingAttemptList;
    }    
     
    public static List<EmploymentSeekingAttempt> getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(set<Id> EmploymentSeekingAttemptIDs){    
        Map<Id, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(EmploymentSeekingAttemptIDs);
        List<EmploymentSeekingAttempt> EmploymentSeekingAttemptList = new List<EmploymentSeekingAttempt>();
        for(EmploymentSeekingAttempt__c de : EmploymentSeekingAttemptMap.values()){
            EmploymentSeekingAttemptList.add(new EmploymentSeekingAttempt(de));
        }
        return EmploymentSeekingAttemptList;
    }
    
    public static Set<ID> createEmploymentSeekingAttempts(List<EmploymentSeekingAttempt> deList){
        System.debug('EmploymentSeekingAttemptService.createEmploymentSeekingAttempt()');
        List<EmploymentSeekingAttempt__c> newDEs = new List<EmploymentSeekingAttempt__c>();
        for(EmploymentSeekingAttempt de : deList){
            EmploymentSeekingAttempt__c newDE = deToDE(de);
            newDEs.add(newDE);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newDEs);
        Set<ID> DEIDs = new Set<ID>();
        for(EmploymentSeekingAttempt__c de : newDEs){
            DEIDs.add(de.ID);
        }
        return DEIDs;
    }
    
    public static Set<ID> updateEmploymentSeekingAttempts(List<EmploymentSeekingAttempt> deList){
        System.debug('EmploymentSeekingAttemptService.updateEmploymentSeekingAttempt()');
        List<EmploymentSeekingAttempt__c> newDEs = new List<EmploymentSeekingAttempt__c>();
        for(EmploymentSeekingAttempt de : deList){
            EmploymentSeekingAttempt__c newDE = deToDE(de);
            newDEs.add(newDE);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newDEs);
        Set<ID> DEIDs = new Set<ID>();
        for(EmploymentSeekingAttempt__c de : newDEs){
            DEIDs.add(de.ID);
        }
        return DEIDs;
    }
    
    public static Integer deleteEmploymentSeekingAttempts(Set<ID> EmploymentSeekingAttemptIDs){
        System.debug('EmploymentSeekingAttemptService.deleteCreditChecks()');
        Map<Id, EmploymentSeekingAttempt__c> EmploymentSeekingAttemptMap = EmploymentSeekingAttemptQueries.getEmploymentSeekingAttemptMapWithEmploymentSeekingAttemptID(EmploymentSeekingAttemptIDs);
        Integer numToDelete = EmploymentSeekingAttemptMap.size();
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.deleteRecords(EmploymentSeekingAttemptMap.values());
        return numToDelete;
    }
    
    public static EmploymentSeekingAttempt__c deToDE(EmploymentSeekingAttempt de){
        EmploymentSeekingAttempt__c deObj = new EmploymentSeekingAttempt__c();
        if(de.EmploymentSeekingAttemptID != null) deObj.ID = de.EmploymentSeekingAttemptID; 
        if(de.DefermentID != null) deObj.Deferment__c = de.DefermentId;
        if(de.cityEmployment != null) deObj.CityEmployment__c = de.cityEmployment;
        if(de.contactDate != null) deObj.ContactDate__c = de.contactDate ;      
        if(de.countryEmployment != null) deObj.CountryEmployment__c = de.countryEmployment;        
        if(de.employerName != null) deObj.EmployerName__c = de.employerName ;
        if(de.followUpDate != null) deObj.FollowUpDate__c = de.followUpDate;        
        if(de.resultsOutcome != null) deObj.ResultsOutcome__c = de.resultsOutcome ;           
        if(de.stateEmployment != null) deObj.StateEmployment__c = de.stateEmployment ;           
        if(de.JobPositionTypeofWork != null) deObj.JobPositionTypeofWork__c = de.JobPositionTypeofWork;
        return deObj;
    } 

    public class EmploymentSeekingAttempt{
        public String EmploymentSeekingAttemptID{get;set;}
        public String DefermentID {get;set;}
        public String cityEmployment{get;set;}
        public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String employerName {get;set;}
        public Date followUpDate{get;set;}        
        public String resultsOutcome {get;set;}   
        public String stateEmployment {get;set;}           
        public String JobPositionTypeofWork {get;set;}
              
        public EmploymentSeekingAttempt(){}

        public EmploymentSeekingAttempt(EmploymentSeekingAttempt__c de){
            this.EmploymentSeekingAttemptID = de.ID;
            this.DefermentID = de.Deferment__c;
            this.cityEmployment = de.CityEmployment__c;
            this.contactDate = de.ContactDate__c;           
            this.countryEmployment = de.CountryEmployment__c;            
            this.employerName = de.EmployerName__c;  
            this.followUpDate = de.FollowUpDate__c;                      
            this.resultsOutcome = de.ResultsOutcome__c;
            this.stateEmployment = de.StateEmployment__c ;               
            this.JobPositionTypeofWork  = de.JobPositionTypeofWork__c;
            
        }
    }
}