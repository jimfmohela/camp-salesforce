public with sharing class EmploymentHistoryTriggerHandler2 implements TriggerDispatch.ITriggerHandlerClass {
    
    /**************************Static Variables***********************************/

    /**************************State Control Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution Control - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.TriggerContext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.TriggerContext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'EmploymentHistoryTriggerHandler2' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'EmploymentHistoryTriggerHandler2'){
            //Determine what to do with other triggers - either kill them or forward them along
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onBeforeInsert()');
       //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> newEmpHistryList = (List<EmploymentHistory2__c>)tc.newList;
        //This is where you should call your business logic
        setDefaultsOnInsert(newEmpHistryList);
        processNoLongerEmployed(newEmpHistryList, null);
        autoCloseEmploymentHistory(newEmpHistryList, null);
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onBeforeUpdate()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> newEmpHistryList = (List<EmploymentHistory2__c>)tc.newList;
        List<EmploymentHistory2__c> oldEmpHistryList = (List<EmploymentHistory2__c>)tc.oldList;
        Map<ID, EmploymentHistory2__c> newEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.newMap;
        Map<ID, EmploymentHistory2__c> oldEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.oldMap;
        //This is where you should call your business logic
        setDefaultsOnUpdate(oldEmpHistryMap, newEmpHistryMap);
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onBeforeDelete()');
       //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> oldEmpHistryList = (List<EmploymentHistory2__c>)tc.oldList;
        Map<ID, EmploymentHistory2__c> oldEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.oldMap;
        //This is where you should call your business logic

    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onAfterInsert()');
         //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> newEmpHistryList = (List<EmploymentHistory2__c>)tc.newList;
        Map<ID, EmploymentHistory2__c> newEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.newMap;
        //This is where you should call your business logic
        //createCasesForInfoVerification(newEmpHistryList);  // moving the logic to process builder PPN-106
        
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onAfterUpdate()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> newEmpHistryList = (List<EmploymentHistory2__c>)tc.newList;
        List<EmploymentHistory2__c> oldEmpHistryList = (List<EmploymentHistory2__c>)tc.oldList;
        Map<ID, EmploymentHistory2__c> newEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.newMap;
        Map<ID, EmploymentHistory2__c> oldEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.oldMap;
        //This is where you should call your business logic
        processNoLongerEmployed(newEmpHistryList, oldEmpHistryMap);
        autoCloseEmploymentHistory(newEmpHistryList, oldEmpHistryMap);
        closeVerificationCases(newEmpHistryList, oldEmpHistryMap);
        
   }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onAfterDelete()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> oldEmpHistryList = (List<EmploymentHistory2__c>)tc.oldList;
        Map<ID, EmploymentHistory2__c> oldEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.oldMap;
        //This is where you should call your business logic
        deleteRelatedEHRecords(oldEmpHistryMap.values());

     }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.TriggerContext tc){
        System.debug('EmploymentHistoryTriggerHandler2.onAfterUndelete()');
        //Recast the trigger context variables into the appropriate shandlerect types
        List<EmploymentHistory2__c> newEmpHistryList = (List<EmploymentHistory2__c>)tc.newList;
        Map<ID, EmploymentHistory2__c> newEmpHistryMap = (Map<ID, EmploymentHistory2__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnInsert
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnInsert(List<EmploymentHistory2__c> newEmpHistryList){
        System.debug('EmploymentHistoryTriggerHandler2.setDefaultsOnInsert()');
        //This is where you should call your business logic
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            for(EmploymentHistory2__c empHis : newEmpHistryList){
                //Mark Verified__c = true for Employment History which are not related to Monthly Income
                if(empHis.EventType__c != null && !empHis.Verified__c){
                    if(empHis.EventType__c == 'UPDATE_CURRENT_DESIGNATION'){
                        //empHis.Verified__c = true;
                    }
                }
                if(empHis.EffectiveDate__c == null && empHis.EmploymentStartDate__c != null){
                    empHis.EffectiveDate__c = empHis.EmploymentStartDate__c;
                }
                if(empHis.EventType__c == 'NOT_EMPLOYED_HERE_ANYMORE' && empHis.EmploymentEndDate__c == null && empHis.EffectiveDate__c != null){
                    empHis.EmploymentEndDate__c = empHis.EffectiveDate__c;
                }
                if(empHis.EventType__c == 'NOT_EMPLOYED_HERE_ANYMORE' && empHis.EmploymentEndDate__c != null){
                    //empHis.UnemploymentEffectiveDate__c = empHis.EmploymentEndDate__c;
                }
                if(empHis.Status__c == null){
                    //empHis.Status__c = 'Unverified';
                }
                //if(empHis.DocUploaded__c == true){
                //    empHis.Status__c = 'Under Review';
                //}
                //if(empHis.Verified__c == true && empHis.Status__c != 'Verified'){
                //    empHis.Status__c = 'Verified';
                //}
            }
            autoPopulateFields(newEmpHistryList);
            calculateMonthlyIncome(newEmpHistryList);
        }
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: setDefaultsOnUpdate
    /////////////////////////////////////////////////////////////////////////
    private void setDefaultsOnUpdate(Map<ID, EmploymentHistory2__c> oldEmpHistryMap, Map<ID, EmploymentHistory2__c> newEmpHistryMap){
        System.debug('EmploymentHistoryTriggerHandler2.setDefaultsOnUpdate()'); 
        //This is where you should call your business logic
        if(newEmpHistryMap != null && newEmpHistryMap.size() > 0){
            for(EmploymentHistory2__c empHis : newEmpHistryMap.values()){
                //if(empHis.DocUploaded__c == true && oldEmpHistryMap.get(empHis.Id).DocUploaded__c == false){
                //    empHis.Status__c = 'Under Review';
                //}
                //if(empHis.Verified__c == true && oldEmpHistryMap.get(empHis.Id).Verified__c != true && empHis.Status__c != 'Verified'){
                //    empHis.Status__c = 'Verified';
                //}
                //if(empHis.Status__c != 'Verified' && oldEmpHistryMap.get(empHis.Id).Status__c == 'Verified'){
                //    empHis.Verified__c = false;
                //}
                //if(empHis.Verified__c == false && oldEmpHistryMap.get(empHis.Id).Verified__c == true){
                //  empHis.Status__c = 'Unverified';
                //    if(empHis.DocUploaded__c){
                //        empHis.Status__c = 'Under Review';
                //    }
                //}
                //if(empHis.Verified__c && (empHis.DateVerified__c == null || empHis.DateVerified__c > Date.today())){
                //    empHis.DateVerified__c = Date.today();
                //}
            }
        }
        calculateMonthlyIncome(newEmpHistryMap.values());
    }
    
    private static void autoPopulateFields(List<EmploymentHistory2__c> newEmpHistryList){
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            Set<String> requiredEvtTypes = new Set<String>{
                'UPDATE_HOURLY_COMPENSATION','UPDATE_HOURLY_WITH_DESIGNATION',
                'UPDATE_SALARY_COMPENSATION','UPDATE_SALARY_WITH_DESIGNATION'
            };
            Set<Id> studentIds = new Set<Id>();
            for(EmploymentHistory2__c empHis : newEmpHistryList){
                if(empHis.EventType__c != null && requiredEvtTypes.contains(empHis.EventType__c)){
                    if(empHis.EventType__c == 'UPDATE_HOURLY_COMPENSATION' && (empHis.HourlyRate__c == null || empHis.HoursPerWeek__c == null)){
                        studentIds.add(empHis.Student__c);
                    }
                    else if(empHis.EventType__c == 'UPDATE_HOURLY_WITH_DESIGNATION' && (empHis.HourlyRate__c == null || empHis.HoursPerWeek__c == null)){
                        studentIds.add(empHis.Student__c);
                    }
                    else if(empHis.EventType__c == 'UPDATE_SALARY_COMPENSATION' && (empHis.YearlySalary__c == null || empHis.PaymentSchedule__c == null)){
                        studentIds.add(empHis.Student__c);
                    }
                    else if(empHis.EventType__c == 'UPDATE_SALARY_WITH_DESIGNATION' && (empHis.YearlySalary__c == null || empHis.PaymentSchedule__c == null)){
                        studentIds.add(empHis.Student__c);
                    }
                }
            }
            if(studentIds.size() > 0){
                Map<String, EmploymentHistoryService.LatestCompleteEmploymentDeatil> LatestCompleteEmploymentDetails = EmploymentHistoryService.getLatestCompleteEmploymentDetails(studentIds);
                if(LatestCompleteEmploymentDetails != null && LatestCompleteEmploymentDetails.size() > 0){
                    for(EmploymentHistory2__c empHis : newEmpHistryList){
                        if(empHis.EventType__c == 'UPDATE_HOURLY_COMPENSATION' || empHis.EventType__c == 'UPDATE_HOURLY_WITH_DESIGNATION'){
                            String custmKey = '';
                            custmKey = empHis.Student__c + empHis.Employer__c ;
                            if(LatestCompleteEmploymentDetails.containsKey(custmKey)){
                                if(empHis.HourlyRate__c == null) empHis.HourlyRate__c = LatestCompleteEmploymentDetails.get(custmKey).hourly.hourlyRate;
                                if(empHis.HoursPerWeek__c == null) empHis.HoursPerWeek__c = LatestCompleteEmploymentDetails.get(custmKey).hourly.hoursPerWeek;
                            }
                        }
                        else if(empHis.EventType__c == 'UPDATE_SALARY_COMPENSATION' || empHis.EventType__c == 'UPDATE_SALARY_WITH_DESIGNATION'){
                            String custmKey = '';
                            custmKey = empHis.Student__c + empHis.Employer__c ;
                            if(LatestCompleteEmploymentDetails.containsKey(custmKey)){
                                if(empHis.YearlySalary__c == null) empHis.YearlySalary__c = LatestCompleteEmploymentDetails.get(custmKey).salary.yearlySalary;
                                if(empHis.PaymentSchedule__c == null) empHis.PaymentSchedule__c = LatestCompleteEmploymentDetails.get(custmKey).salary.paymentSchedule;
                            }
                        }
                    }
                }
            }
        }
    }
    
    private void calculateMonthlyIncome(List<EmploymentHistory2__c> newEmpHistryList){
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            for(EmploymentHistory2__c empHis : newEmpHistryList){
                Decimal reportedMonthlyIncome = 0;
                String frequency = '';
                if(empHis.YearlySalary__c != null){
                    reportedMonthlyIncome += (empHis.YearlySalary__c/12);
                }
                if(empHis.HourlyRate__c != null && empHis.HoursPerWeek__c != null){
                    reportedMonthlyIncome += ((empHis.HourlyRate__c*empHis.HoursPerWeek__c)*52)/12;
                }
                if(empHis.BonusAmount__c != null){
                    Decimal monthlyBonusAmount = empHis.BonusAmount__c;
                    //frequency = empHis.BonusFrequency__c;
                    //if(empHis.BonusFrequency__c == 'Annually'){
                    //    monthlyBonusAmount = empHis.BonusAmount__c / 12;
                    //}
                    //else if(empHis.BonusFrequency__c == 'Quarterly'){
                    //    monthlyBonusAmount = empHis.BonusAmount__c / 3;
                    //}
                    reportedMonthlyIncome += monthlyBonusAmount;
                }
                //if(empHis.CommissionAmount__c != null){
                //    frequency = empHis.CommissionFrequency__c;
                //    Decimal monthlyCommissionAmount = empHis.CommissionAmount__c;
                //    if(empHis.CommissionFrequency__c == 'Annually'){
                //        monthlyCommissionAmount = empHis.CommissionAmount__c / 12;
                //    }
                //    else if(empHis.CommissionFrequency__c == 'Quarterly'){
                //        monthlyCommissionAmount = empHis.CommissionAmount__c / 3;
                //    }
                //    reportedMonthlyIncome += monthlyCommissionAmount;
                //}
                //if(empHis.TipAmount__c != null){
                //    reportedMonthlyIncome += empHis.TipAmount__c ;
                //    frequency = 'One-Time';
                //}
                //empHis.MonthlyIncomeReported__c = reportedMonthlyIncome; Allowing user to provide the monthly salary from portal instead of yearly income.
                //if(frequency != null && frequency == 'One-Time' && empHis.EmploymentEndDate__c == null && empHis.DateVerified__c != null){
                //    empHis.EmploymentEndDate__c = Date.newInstance(empHis.DateVerified__c.year(), empHis.DateVerified__c.month(), date.daysInMonth(empHis.DateVerified__c.year(), empHis.DateVerified__c.month()));
                //}
            }
        }        
    }
    /////////////////////////////////////////////////////////////////////////
    //Method: autoCloseEmploymentHistory
    //Description : This method will close Old Employment Histories on the basis of StudentId, Employer, Job Title and Event Type
    /////////////////////////////////////////////////////////////////////////
    private void autoCloseEmploymentHistory(List<EmploymentHistory2__c> newEmpHistryList, Map<ID, EmploymentHistory2__c> oldEmpHistryMap){
        System.debug('>>>>>autoCloseEmploymentHistory<<<<<'); 
        if(oldEmpHistryMap == null){
            oldEmpHistryMap = new Map<ID, EmploymentHistory2__c>();
        }
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            Set<Id> currentEmpHisIds = new Set<Id>();
            Set<Id> studentIds = new Set<Id>();
            Map<String, EmploymentHistory2__c> custmEventTypeVsEmpHisMap = new Map<String, EmploymentHistory2__c>();
            for(EmploymentHistory2__c empHis : newEmpHistryList){
                String customKey = '';
                // && empHis.JobTitle__c != null && !String.isBlank(empHis.JobTitle__c)
                if(empHis.Student__c != null && empHis.Employer__c != null && !String.isBlank(empHis.Employer__c)){
                    //customKey = empHis.Student__c + empHis.Employer__c.toLowerCase() + empHis.JobTitle__c.toLowerCase();
                    customKey = empHis.Student__c + empHis.Employer__c.toLowerCase();
                    if(empHis.Id != null){
                        currentEmpHisIds.add(empHis.Id);
                    }
                    if(empHis.EventType__c != null && !String.isBlank(empHis.EventType__c) && empHis.effectiveDate__c != null
                            && (oldEmpHistryMap.size() == 0 || oldEmpHistryMap.get(empHis.Id).effectiveDate__c == null)
                            ){
                                
                        studentIds.add(empHis.Student__c);
                        String eventType = empHis.EventType__c;
                        if(eventType == 'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION' || eventType == 'UPDATE_HOURLY_COMPENSATION' || eventType == 'UPDATE_HOURLY_WITH_DESIGNATION'){
                            eventType = 'closePreviousHourlyCompensation';
                        }
                        if(eventType == 'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION' || eventType == 'UPDATE_SALARY_COMPENSATION' || eventType == 'UPDATE_SALARY_WITH_DESIGNATION'){
                            eventType = 'closePreviousSalaryCompensation';
                        }
                        String frequency = '';
                        //if(empHis.BonusFrequency__c != null || empHis.CommissionFrequency__c != null){
                        //    frequency = empHis.BonusFrequency__c ;
                        //    if(frequency == null || frequency == ''){
                        //        frequency = empHis.CommissionFrequency__c ;
                        //    } deprecated
                        //}
                        if(frequency == null){
                            frequency = '';
                        }
                        if(eventType.startsWith('UPDATE_')){
                            eventType = eventType.removeStart('UPDATE_');
                        }
                        
                        eventType = customKey + eventType + frequency;
                        System.debug('>>>>>eventType -->'+eventType);
                        if(!String.isBlank(eventType)){
                            if(!custmEventTypeVsEmpHisMap.containsKey(eventType)){
                                custmEventTypeVsEmpHisMap.put(eventType, empHis);
                            }
                            else {
                                if(empHis.effectiveDate__c > custmEventTypeVsEmpHisMap.get(eventType).effectiveDate__c){
                                    custmEventTypeVsEmpHisMap.put(eventType, empHis);
                                }
                            }
                        }
                    }
                }
            }
            if(studentIds.size() > 0){
                Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
                empHisMap = EmploymentHistoryQueries2.getOpenEmploymentHistoryMapWithStudentId(studentIds);
                if(empHisMap.size() > 0){
                    List<EmploymentHistory2__c> empHistryListToUpdate = new List<EmploymentHistory2__c>();
                    for(EmploymentHistory2__c empHis : empHisMap.values()){
                        Boolean addToUpdateList = false;
                        if(!currentEmpHisIds.contains(empHis.Id)){
                            String customKey = '';
                            if(empHis.Student__c != null){
                                customKey += empHis.Student__c;
                            }
                            if(empHis.Employer__c != null && !String.isBlank(empHis.Employer__c)){
                                customKey += empHis.Employer__c.toLowerCase();
                            }
                            if(empHis.JobTitle__c != null && !String.isBlank(empHis.JobTitle__c)){
                                //customKey += empHis.JobTitle__c.toLowerCase();
                            }
                            if(empHis.EmploymentEndDate__c == null){
                                if(empHis.EventType__c != null && !String.isBlank(empHis.EventType__c)){
                                    String eventType = empHis.EventType__c;
                                    if(eventType == 'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION' || eventType == 'UPDATE_HOURLY_COMPENSATION' || eventType == 'UPDATE_HOURLY_WITH_DESIGNATION'){
                                        eventType = 'closePreviousHourlyCompensation';
                                    }
                                    if(eventType == 'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION' || eventType == 'UPDATE_SALARY_COMPENSATION' || eventType == 'UPDATE_SALARY_WITH_DESIGNATION'){
                                        eventType = 'closePreviousSalaryCompensation';
                                    }
                                    String frequency = '';
                                    //if(empHis.BonusFrequency__c != null || empHis.CommissionFrequency__c != null){
                                    //    frequency = empHis.BonusFrequency__c ;
                                    //    if(frequency == null || frequency == ''){
                                    //        frequency = empHis.CommissionFrequency__c ;
                                    //    }
                                    //}
                                    if(frequency == null){
                                        frequency = '';
                                    }
                                    if(eventType.startsWith('UPDATE_')){
                                        eventType = eventType.removeStart('UPDATE_');
                                    }
                                    eventType = customKey + eventType + frequency;
                                    System.debug('>>>>>eventType -->'+eventType);
                                    if(custmEventTypeVsEmpHisMap.containsKey(eventType) && frequency != 'One-Time'){
                                        empHis.EmploymentEndDate__c = custmEventTypeVsEmpHisMap.get(eventType).effectiveDate__c.addDays(-1);
                                        empHistryListToUpdate.add(empHis);
                                    }
                                }
                            }
                        }
                    }
                    if(empHistryListToUpdate.size() > 0){
                        update empHistryListToUpdate;
                    }
                }
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: processNoLongerEmployed
    //Description : Mark No Longer Employed Here to True for all employment History belongs to same employer and fill end date
    /////////////////////////////////////////////////////////////////////////
    private void processNoLongerEmployed(List<EmploymentHistory2__c> newEmpHistryList, Map<ID, EmploymentHistory2__c> oldEmpHistryMap){
        System.debug('>>>>>processNoLongerEmployed<<<<<'); 
        if(oldEmpHistryMap == null){
            oldEmpHistryMap = new Map<ID, EmploymentHistory2__c>();
        }
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            Set<Id> currentEmpHisIds = new Set<Id>();
            Set<Id> studentIds = new Set<Id>();
            Map<String, EmploymentHistory2__c> noLongerEmpHisMap = new Map<String, EmploymentHistory2__c>();
            for(EmploymentHistory2__c empHis : newEmpHistryList){
                if(empHis.Id != null){
                    currentEmpHisIds.add(empHis.Id);
                }
                String customKey = '';
                if(empHis.Student__c != null && empHis.Employer__c != null && !String.isBlank(empHis.Employer__c)){
                    customKey = empHis.Student__c + empHis.Employer__c.toLowerCase();
                    //No Longer Employed Here
                    /*if(empHis.NoLongerEmployedHere__c && empHis.EmploymentEndDate__c != null && (oldEmpHistryMap.size() == 0 || oldEmpHistryMap.get(empHis.Id).NoLongerEmployedHere__c == false)){
                        System.debug('>>>>>No Longer Employed Here <<<<<<');
                        studentIds.add(empHis.Student__c);
                        if(!String.isBlank(customKey)){
                            if(!noLongerEmpHisMap.containsKey(customKey)){
                                noLongerEmpHisMap.put(customKey, empHis);
                            }
                            else {
                                if(empHis.EmploymentEndDate__c > noLongerEmpHisMap.get(customKey).EmploymentEndDate__c){
                                    noLongerEmpHisMap.put(customKey, empHis);
                                }
                            }
                        }
                    } deprecated */
                }
            }
            if(studentIds.size() > 0){
                Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
                empHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithStudentId(studentIds);
                if(empHisMap.size() > 0){
                    List<EmploymentHistory2__c> empHistryListToUpdate = new List<EmploymentHistory2__c>();
                    for(EmploymentHistory2__c empHis : empHisMap.values()){
                        Boolean addToUpdateList = false;
                        if(!currentEmpHisIds.contains(empHis.Id)){
                            String customKey = '';
                            if(empHis.Student__c != null){
                                customKey += empHis.Student__c;
                            }
                            if(empHis.Employer__c != null && !String.isBlank(empHis.Employer__c)){
                                customKey += empHis.Employer__c.toLowerCase();
                            }
                            //check if it belongs to old employer i.e. No Longer Employed Here case
                            if(noLongerEmpHisMap.containsKey(customKey)){
                                /*if(!empHis.NoLongerEmployedHere__c){
                                    empHis.NoLongerEmployedHere__c = true;
                                    addToUpdateList = true;
                                }
                                if(empHis.UnemploymentEffectiveDate__c == null){
                                    empHis.UnemploymentEffectiveDate__c = noLongerEmpHisMap.get(customKey).EmploymentEndDate__c;
                                    addToUpdateList = true;
                                }*/
                                if(empHis.EmploymentEndDate__c == null){
                                    empHis.EmploymentEndDate__c = noLongerEmpHisMap.get(customKey).EmploymentEndDate__c;
                                    addToUpdateList = true;
                                }
                            }
                        }
                        if(addToUpdateList){
                            empHistryListToUpdate.add(empHis);
                        }
                    }
                    if(empHistryListToUpdate.size() > 0){
                        update empHistryListToUpdate;
                    }
                }
            }
        }
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: createCasesForInfoVerification
    //Description : Create case to verify Employment History
    /////////////////////////////////////////////////////////////////////////
    //private void createCasesForInfoVerification(List<EmploymentHistory2__c> newEmpHistryList){
    //    if(newEmpHistryList != null && newEmpHistryList.size() > 0){
    //        List<Case> caseList = new List<Case>();
    //        Set<Id> studentIds = new Set<Id>();
    //        for(EmploymentHistory2__c empHis : newEmpHistryList){
    //            //if(empHis.Student__c != null && !empHis.Verified__c){
    //                studentIds.add(empHis.Student__c);
    //            //}
    //        }
    //        //if(studentIds.size() > 0){
    //            Map<Id, Account> IdVSStudentMap = new Map<Id, Account>();
    //            IdVSStudentMap = AccountQueries.getStudentMapWithStudentID(studentIds);
    //            //if(IdVSStudentMap != null && IdVSStudentMap.size() > 0){
    //                String caseOwnerId = GlobalUtil.getQueueId('Servicing');
    //                String caseRecordTypeID = GlobalUtil.getRecordTypeIdByLabelName('Case', GlobalUtil.CASE_RECTYPELBL_DEFAULT);
    //                for(EmploymentHistory2__c empHis : newEmpHistryList){
    //                    if(empHis.Status__c == 'Under Review' || empHis.Status__c == 'Incomplete Info'){
    //                        //if(IdVSStudentMap.containsKey(empHis.Student__c)){
    //                            Case caseToCreate = new Case();
    //                            caseToCreate = getDefaultCase();
    //                            caseToCreate.OwnerId = caseOwnerId;
    //                            caseToCreate.RecordTypeID = caseRecordTypeID;
    //                            caseToCreate.ContactID = IdVSStudentMap.get(empHis.Student__c).PersonContactId;
    //                            caseToCreate.EmploymentHistory2__c = empHis.Id;
    //                            caseList.add(caseToCreate);
    //                        //}
    //                    }
    //                }
    //                if(caseList.size() > 0){
    //                    insert caseList;
    //                }
    //            //}
    //        //}
    //    }
    //}
    
    /////////////////////////////////////////////////////////////////////////
    //Method: closeVerificationCases
    //Description : Close case once Employment History is verified
    /////////////////////////////////////////////////////////////////////////
    private void closeVerificationCases(List<EmploymentHistory2__c> newEmpHistryList, Map<ID, EmploymentHistory2__c> oldEmpHistryMap){
        if(oldEmpHistryMap == null){
            oldEmpHistryMap = new Map<Id, EmploymentHistory2__c>();
        }
        if(newEmpHistryList != null && newEmpHistryList.size() > 0){
            Set<Id> empHisIds = new Set<Id>();
            for(EmploymentHistory2__c empHist : newEmpHistryList){
                //if(empHist.Verified__c && (oldEmpHistryMap.size() == 0 || !oldEmpHistryMap.get(empHist.Id).Verified__c)){
                //    empHisIds.add(empHist.Id);
                //}
            }
            if(empHisIds.size() > 0){
                Map<ID, Case> caseMap = new Map<ID, Case>();
                caseMap = CaseQueries.getOpenCaseMapWithEmploymentHistoryID(empHisIds);
                if(caseMap != null && caseMap.size() > 0){
                    for(Case cseToClose : caseMap.values()){
                        cseToClose.Status = 'Closed';
                    }
                    update caseMap.values();
                }
            }
        }
    }
    
    //private static Case getDefaultCase(){
    //    Case caseToCreate = new Case();
    //    caseToCreate.ContactID = null;
    //    caseToCreate.Subject = 'Verification Required for New Income Event';
    //    caseToCreate.Status = 'New';
    //    caseToCreate.Priority = 'Medium';
    //    caseToCreate.Reason = 'Other';
    //    caseToCreate.OwnerId = null;
    //    caseToCreate.Type = 'Other';
    //    caseToCreate.Origin = 'Other';
    //    return caseToCreate;
    //}
    
    private static void deleteRelatedEHRecords(List<EmploymentHistory2__c> oldEmpHisList){
        if(oldEmpHisList != null && oldEmpHisList.size() > 0){
            Set<String> createEHEvents = new Set<String>{
                'CREATE_EMPLOYMENT', 'CREATE_EMPLOYMENT_WITH_HOURLY_COMPENSATION', 'CREATE_EMPLOYMENT_WITH_SALARY_COMPENSATION' 
            };
            Map<Id, Set<String>> studentIdVSEmplrList = new Map<Id, Set<String>>();
            for(EmploymentHistory2__c empHist : oldEmpHisList){
                if(empHist.EventType__c != null && empHist.Employer__c != null && createEHEvents.contains(empHist.EventType__c.trim())){
                    if(!studentIdVSEmplrList.containsKey(empHist.Student__c)){
                        studentIdVSEmplrList.put(empHist.Student__c, new Set<String>());
                    }
                    studentIdVSEmplrList.get(empHist.Student__c).add(empHist.Employer__c);
                }
            }
            if(studentIdVSEmplrList.size() > 0){
                Map<ID, EmploymentHistory2__c> empHisMap = new Map<ID, EmploymentHistory2__c>();
                empHisMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithStudentId(studentIdVSEmplrList.keySet());
                if(empHisMap.size() > 0){
                    List<EmploymentHistory2__c> empHisListToDelete = new List<EmploymentHistory2__c>();
                    for(EmploymentHistory2__c empHist : empHisMap.values()){
                        if(empHist.Employer__c != null && studentIdVSEmplrList.get(empHist.Student__c).contains(empHist.Employer__c)){
                            empHisListToDelete.add(empHist);
                        }
                    }
                    if(empHisListToDelete.size() > 0){
                        delete empHisListToDelete;
                    }
                }
            }
        }
    }
    
    public class EmploymentHistoryTriggerHandler2Exception extends Exception {}
    
}