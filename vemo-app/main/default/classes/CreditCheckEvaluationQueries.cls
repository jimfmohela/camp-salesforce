public class CreditCheckEvaluationQueries{

    public static Map<ID, CreditCheckEvaluation__c> getCreditCheckEvaluationMapWithCreditCheckEvalID(Set<ID> creditCheckEvalIDs){
        Map<ID, CreditCheckEvaluation__c> ccMap = new Map<ID, CreditCheckEvaluation__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(creditCheckEvalIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and CreditCheck__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' Order By createdDate desc';
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        ccMap = new Map<ID, CreditCheckEvaluation__c>((List<CreditCheckEvaluation__c>)db.query(query));
        return ccMap;
    }
    
    public static Map<ID, CreditCheckEvaluation__c> getCreditCheckEvaluationMapWithCreditCheckID(Set<ID> creditCheckIDs){
        Map<ID, CreditCheckEvaluation__c> ccMap = new Map<ID, CreditCheckEvaluation__c>();
        String query = generateSOQLSelect();
        query += ' WHERE CreditCheck__c IN ' + DatabaseUtil.inSetStringBuilder(creditCheckIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and CreditCheck__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' Order By createdDate desc';
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        ccMap = new Map<ID, CreditCheckEvaluation__c>((List<CreditCheckEvaluation__c>)db.query(query));
        return ccMap;
    }
    
    public static Map<ID, CreditCheckEvaluation__c> getCreditCheckEvaluationMapWithAgreementID(Set<ID> agreementIDs){
        Map<ID, CreditCheckEvaluation__c> ccMap = new Map<ID, CreditCheckEvaluation__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Agreement__c IN ' + DatabaseUtil.inSetStringBuilder(agreementIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and CreditCheck__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' Order By createdDate desc';
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        ccMap = new Map<ID, CreditCheckEvaluation__c>((List<CreditCheckEvaluation__c>)db.query(query));
        return ccMap;
    }
    
    private static String generateSOQLSelect(){
        String soql;        
        soql = 'SELECT ' + getFieldNames() + ' FROM CreditCheckEvaluation__c';
        return soql;
    }
    
     private static String getFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'Agreement__c, ';
        fieldNames += 'CreditCheck__c, ';
        fieldNames += 'Status__c, ';
        fieldNames += 'CreditCheckDeniedReason__c, ';
        fieldNames += 'CreditCheckDeniedReasonText__c, ';
        fieldNames += 'CreditCheckProcess__c, ';
        fieldNames += 'CreatedDate';
        

        return fieldNames;
    }
    
    
    private static String generateLIMITStatement(){
        String lim = 'LIMIT 50000';
        return lim;
    }
}