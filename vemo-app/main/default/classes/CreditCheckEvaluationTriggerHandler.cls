/////////////////////////////////////////////////////////////////////////
// Class: CreditCheckEvaluationTriggerHandler 
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-08-28   Rini Gupta       Created                              
// 
/////////////////////////////////////////////////////////////////////////

public with sharing class CreditCheckEvaluationTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {
    
    /**************************Static Variables***********************************/

    /**************************State acctrol Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution acctrol - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.Triggercontext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.Triggercontext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'CreditCheckEvaluationTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'CreditCheckEvaluationTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            system.debug('tc.handler--'+tc.handler);
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onBeforeInsert()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> newCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.newList;
        //This is where you should call your business logic
    

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onBeforeUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> newCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.newList;
        List<CreditCheckEvaluation__c> oldCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.oldList;
        Map<ID, CreditCheckEvaluation__c> newCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.newMap;
        Map<ID, CreditCheckEvaluation__c> oldCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.oldMap;
        //This is where you should call your business logic
    

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onBeforeDelete()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> oldCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.oldList;
        Map<ID, CreditCheckEvaluation__c> oldCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.oldMap;
        //This is where you should call your business logic

    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onAfterInsert()');
         //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> newCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.newList;
        Map<ID, CreditCheckEvaluation__c> newCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.newMap;
        //This is where you should call your business logic

        copyCreditEvaluationToAgreementFields(null, newCreditCheckEvaluationMap);

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onAfterUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> newCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.newList;
        List<CreditCheckEvaluation__c> oldCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.oldList;
        Map<ID, CreditCheckEvaluation__c> newCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.newMap;
        Map<ID, CreditCheckEvaluation__c> oldCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.oldMap;
        //This is where you should call your business logic

        copyCreditEvaluationToAgreementFields(oldCreditCheckEvaluationMap, newCreditCheckEvaluationMap);

   }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onAfterDelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> oldCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.oldList;
        Map<ID, CreditCheckEvaluation__c> oldCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.oldMap;
        //This is where you should call your business logic

     }
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.Triggercontext tc){
        system.debug('CreditCheckEvaluationTriggerHandler.onAfterUndelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<CreditCheckEvaluation__c> newCreditCheckEvaluationList = (List<CreditCheckEvaluation__c>)tc.newList;
        Map<ID, CreditCheckEvaluation__c> newCreditCheckEvaluationMap = (Map<ID, CreditCheckEvaluation__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: copyCreditEvaluationToAgreementFields
    /////////////////////////////////////////////////////////////////////////
    private void copyCreditEvaluationToAgreementFields(Map<ID, CreditCheckEvaluation__c> oldCreditCheckEvalMap, Map<ID, CreditCheckEvaluation__c> newCreditCheckEvalMap){
        system.debug('CreditCheckEvaluationTriggerHandler.copyCreditEvaluationToAgreementFields()'); 
        
        Map<ID, StudentProgram__c> agreementsToUpdate = new Map<ID, StudentProgram__c>();
        for(CreditCheckEvaluation__c cc : newCreditCheckEvalMap.values()){
            StudentProgram__c agreement = new StudentProgram__c(id = cc.Agreement__c,
                                            CreditCheckProcess__c = cc.CreditCheckProcess__c,
                                            CreditCheckDeniedReasonText__c = cc.CreditCheckDeniedReasonText__c);
            if(String.isBlank(cc.CreditCheckProcess__c) && cc.Status__c == 'Approved' && oldCreditCheckEvalMap == null){
                //Created as Approved
                agreement.CreditCheckProcess__c = 'Credit Approved';
            }
            if(String.isBlank(cc.CreditCheckProcess__c) && cc.Status__c == 'Denied' && oldCreditCheckEvalMap == null){
                //Created as Denied
                agreement.CreditCheckProcess__c = 'Credit Denied';
                agreement.CreditCheckDeniedReasonText__c = cc.CreditCheckDeniedReasonText__c;
            }
            agreementsToUpdate.put(cc.agreement__c, agreement);
        }  
        
        System.debug(agreementsToUpdate);
        if(agreementsToUpdate.size()>0){
            update agreementsToUpdate.values();
        }
    }

}