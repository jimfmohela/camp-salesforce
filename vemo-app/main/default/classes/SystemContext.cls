public without sharing class SystemContext  implements IQueryExecutor{    
  
    public List<SObject> databaseQuery(string query){
        system.debug('DatabaseUtil.UserContext query:'+query);
        List<sobject> records = Database.query(query);
        return records;   
    }
    
    public void updateRecords(List<SObject> objs){
        update objs;
    }
    public void updateRecord(SObject obj){
        update obj;
    }
    
    public void upsertRecords(List<SObject> objs){
        upsert objs;
    }
    public void upsertRecord(SObject obj){
        upsert obj;
    }
    
    public void deleteRecords(List<SObject> objs){
        delete objs;
    }
    
    public void deleteRecord(SObject obj){
        delete obj;
    }
    
    public void insertRecords(List<SObject> objs){
        insert objs;
    }
    
    public void insertRecord(SObject obj){
        insert obj;
    }
    
    public void undeleteRecords(List<SObject> objs){
        undelete objs;
    }
    
    public void undeleteRecord(SObject obj){
        undelete obj;
    }
}