public with sharing class TransactionBatch implements Database.Batchable<sObject>, Database.ALlowsCallouts{
    public enum JobType {PURGE_DELETED_DISBURSEMENTS} 
    
    public String query {get;set;}
    public JobType job {get;set;}
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(job == JobType.PURGE_DELETED_DISBURSEMENTS){
            if(String.isEmpty(this.query)){
                query = query = 'SELECT id from Transaction__c where Deleted__c = true';
            }
        }
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        System.debug('TransactionBatch.execute()');
        System.debug('scope:'+scope);   
        if(job == JobType.PURGE_DELETED_DISBURSEMENTS)
             delete scope;      
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug('TransactionBatch .finish()');
    }
}