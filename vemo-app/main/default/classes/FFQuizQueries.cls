public class FFQuizQueries{

    public static Map<ID, FFQuizAttempt__c> getFFQuizAttemptMapWithFFQuizAttemptID(Set<ID> FFQuizAttemptIDs){
        Map<Id, FFQuizAttempt__c> FFQuizAttemptMap = new Map<Id, FFQuizAttempt__c>();
        String query = generateSOQLSelect();
        query += ' WHERE ID IN ' + DatabaseUtil.inSetStringBuilder(FFQuizAttemptIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Contract__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        FFQuizAttemptMap = new Map<ID, FFQuizAttempt__c>((List<FFQuizAttempt__c>)db.query(query));
        return FFQuizAttemptMap;
    } 
    
     public static Map<ID, FFQuizAttempt__c> getFFQuizAttemptMapWithStudentProgramID(Set<ID> StudentProgramIDs){
        Map<Id, FFQuizAttempt__c> FFQuizAttemptMap = new Map<Id, FFQuizAttempt__c>();
        String query = generateSOQLSelect();
        query += ' WHERE Contract__c IN ' + DatabaseUtil.inSetStringBuilder(StudentProgramIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and Contract__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        FFQuizAttemptMap = new Map<ID, FFQuizAttempt__c>((List<FFQuizAttempt__c>)db.query(query));
        return FFQuizAttemptMap;
    }
    
    public static Map<ID, FFQuizResponse__c> getFFQuizResponseMapWithFFQuizAttemptID(Set<ID> FFQuizAttemptIDs){
        Map<Id, FFQuizResponse__c> FFQuizResponseMap = new Map<Id, FFQuizResponse__c>();
        String query = generateFFQuizResponseSOQLSelect();
        query += ' WHERE FFQuizAttempt__c IN ' + DatabaseUtil.inSetStringBuilder(FFQuizAttemptIDs);
        if(DatabaseUtil.filterByStudentID){
            ID studentID = DatabaseUtil.studentPersonAccountID;
            query += ' and FFQuizAttempt__r.Contract__r.Student__c = ' + DatabaseUtil.inStringBuilder(studentID)+ ' ';
        }
        query += ' '+ generateLIMITStatement();
        DatabaseUtil db = new DatabaseUtil();
        FFQuizResponseMap = new Map<ID, FFQuizResponse__c>((List<FFQuizResponse__c>)db.query(query));
        return FFQuizResponseMap;  
    }
    
    private static String generateSOQLSelect(){
        String soql;        
        soql = 'SELECT ' + getFFQuizAttemptFieldNames() + ' FROM FFQuizAttempt__c';
        return soql;
    }
    
    private static String generateFFQuizResponseSOQLSelect(){
        String soql;        
        soql = 'SELECT ' + getFFQuizResponseFieldNames() + ' FROM FFQuizResponse__c';
        return soql;
    }
    
    private static String getFFQuizAttemptFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'AttemptOrder__c, ';
        fieldNames += 'ClientID__c, ';
        fieldNames += 'Contract__c, ';
        fieldNames += 'Result__c, ';
        fieldNames += 'ResultLink__c, ';
        fieldNames += 'StartTime__c, ';
        fieldNames += 'SubmitTime__c ';
        
        return fieldNames;
    }
    
    private static String getFFQuizResponseFieldNames(){
        String fieldNames;
        fieldNames = 'id, ';
        fieldNames += 'AnswerID__c, ';
        fieldNames += 'AnswerText__c, ';
        fieldNames += 'CorrectAnswer__c, ';
        fieldNames += 'QuestionID__c, ';
        fieldNames += 'QuestionText__c, ';
        fieldNames += 'FFQuizAttempt__c ';        
        
        return fieldNames;
    }
    
    private static String generateLIMITStatement(){
      String lim = 'LIMIT 50000';
      return lim;
    }

}