/////////////////////////////////////////////////////////////////////////
// Class: GlobalSettings_TEST
// 
// Description: 
//  Unit test for GlobalSettings
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-05-28  Jared Hagemann  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class GlobalSettings_TEST {
	@TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetSettings(){
    	Test.startTest();
        GlobalSettings GlobalSettingsClass = new GlobalSettings ();
    	GlobalSettings settings = GlobalSettings.getSettings();
        System.assertEquals(true, GlobalSettingsClass.workflowRules);
        System.assertEquals(true, GlobalSettingsClass.validationRules);
        System.assertEquals(true, GlobalSettingsClass.emailNotifications);
        System.assertEquals(GlobalSettingsClass.allowTestStudent, GlobalSettingsClass.allowTestStudent);
        System.assertEquals('Origination', GlobalSettingsClass.closedWonCaseQueue);
        System.assertEquals(null, GlobalSettingsClass.vemoAccountID);
        System.assertEquals(null, GlobalSettingsClass.oauthUtilUserID);
        System.assertEquals(null, GlobalSettingsClass.oauthUtilPassword);
        System.assertEquals(null, GlobalSettingsClass.oauthUtilConsumerID);
        System.assertEquals(null, GlobalSettingsClass.oauthUtilConsumerSecret);
    	Test.stopTest();
    }
}