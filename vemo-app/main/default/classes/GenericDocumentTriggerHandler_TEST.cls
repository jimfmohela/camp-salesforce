@isTest
public with sharing class GenericDocumentTriggerHandler_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    @isTest private static void testDML(){
        
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        
        Map<ID, GenericDocument__c> docMap = TestDataFactory.createAndInsertGenericDocument(10);
        dbUtil.updateRecords(docMap.values());
        dbUtil.deleteRecords(docMap.values());
        dbUtil.undeleteRecords(docMap.values());
        //update docMap.values();
        //delete docMap.values();
        //undelete docMap.values();
    }
}