public class VemoAPIStudentProgramCountResource{
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        else{     
            throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
            //return null;
        }
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIStudentProgramCountResource.handleGetV2()');
        List<AggregateResult> arrList = new List<AggregateResult>();
        
        String agreementIDParam = api.params.get('agreementID');
        String studentIDParam = api.params.get('studentID');
        String schoolIDParam = api.params.get('schoolID');
        String programIDParam = api.params.get('programID');
        
        if(agreementIDParam != null){
            arrList = [SELECT Count(Id) recordCount from StudentProgram__c where ID =: agreementIDParam and Deleted__c = false];    
        }
        else if(studentIDParam != null){
            arrList = [SELECT Count(Id) recordCount from StudentProgram__c where Student__c =: studentIDParam and Deleted__c = false];    
        }
        else if(schoolIDParam != null){
            arrList = [SELECT Count(Id) recordCount from StudentProgram__c where Program__r.School__c =: schoolIDParam and Deleted__c = false];    
        }
        else if(programIDParam != null){
            arrList = [SELECT Count(Id) recordCount from StudentProgram__c where Program__c =: programIDParam and Deleted__c = false];    
        }
        else{
            arrList = [SELECT Count(Id) recordCount from StudentProgram__c where Deleted__c = false];    
        }
        
        StudentProgramCountResourceOutputV2 result = new StudentProgramCountResourceOutputV2();
        if(arrList.size()>0){
            result.recordCount = integer.ValueOf(arrList[0].get('recordCount'));
        } 
        
        List<StudentProgramCountResourceOutputV2> resultResponse = new List<StudentProgramCountResourceOutputV2>();
        resultResponse.add(result);
         return (new VemoAPI.ResultResponse(resultResponse, resultResponse.size()));
    }
    
    public class StudentProgramCountResourceOutputV2{
        public Integer recordCount {get;set;}
        
        public StudentProgramCountResourceOutputV2(){
            this.recordCount = 0;
        }
    }
}