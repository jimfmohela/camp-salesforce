public class VemoAPIEventSubscriptionResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v2') && (api.method == 'POST')){
            return handlePostV2(api);
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }  
        if((api.version == 'v2') && (api.method == 'DELETE')){
            return handleDeleteV2(api);
        }      
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }
  
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventTypeResource.handleGetV2()');
        List<EventSubscriptionService.EventSubscription> esList = new List<EventSubscriptionService.EventSubscription>();
        esList = EventSubscriptionService.getEventSubscriptionMapWithAuthUser();
        
        List<EventSubscriptionResourceOutputV2> results = new List<EventSubscriptionResourceOutputV2>();
        for(EventSubscriptionService.EventSubscription es : esList){
            if(GlobalSettings.getSettings().vemoDomainAPI){
                results.add(new VemoEventSubscriptionResourceOutputV2(es));
            }else{
                results.add(new PublicEventSubscriptionResourceOutputV2(es));
            }
            
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
  
    public static VemoAPI.ResultResponse handlePostV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventSubscriptionResource.handlePostV2');
        
        List<EventSubscriptionService.EventSubscription> newEventSubscriptions = new List<EventSubscriptionService.EventSubscription>();
        List<EventSubscriptionResourceInputV2> eSubscriptionJSON = (List<EventSubscriptionResourceInputV2>)JSON.deserialize(api.body, List<EventSubscriptionResourceInputV2>.class);
        
        for(EventSubscriptionResourceInputV2 es : eSubscriptionJSON){
          es.validatePOSTFields();
          newEventSubscriptions.add(eventSubscriptionResourceV2ToEventSubscriptionObj(es));
        }
        Set<ID> eventSubscriptionIDs = EventSubscriptionService.createEventSubscription(newEventSubscriptions);
        return (new VemoAPI.ResultResponse(eventSubscriptionIDs, eventSubscriptionIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventSubscriptionResource.handlePutV2');
        List<EventSubscriptionService.EventSubscription> updateEventSubscriptions = new List<EventSubscriptionService.EventSubscription>();
        List<EventSubscriptionResourceInputV2> eSubscriptionJSON = (List<EventSubscriptionResourceInputV2>)JSON.deserialize(api.body, List<EventSubscriptionResourceInputV2>.class);
        for(EventSubscriptionResourceInputV2 es : eSubscriptionJSON){
          es.validatePUTFields();
          updateEventSubscriptions.add(eventSubscriptionResourceV2ToEventSubscriptionObj(es));
        }
        Set<ID> eventSubscriptionIDs = EventSubscriptionService.updateEventSubscription(updateEventSubscriptions);
        
        return (new VemoAPI.ResultResponse(eventSubscriptionIDs, eventSubscriptionIDs.size()));
    }
      
    public static VemoAPI.ResultResponse handleDeleteV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEventSubscriptionResource.handleDeleteV2()');
        String eventSubscriptionIDparam = api.params.get('eventSubscriptionID');
        Integer numToDelete = EventSubscriptionService.deleteEventSubscriptions(VemoApi.parseParameterIntoIDSet(eventSubscriptionIDparam)); 
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    
    public static EventSubscriptionService.EventSubscription eventSubscriptionResourceV2ToEventSubscriptionObj(EventSubscriptionResourceInputV2 esRes){
        
        EventSubscriptionService.EventSubscription es = new EventSubscriptionService.EventSubscription();
        es.eventSubscriptionID = esRes.eventSubscriptionID;
        es.account = UserService.userContext.Contact.AccountId;
        if(UserService.userContext != null && UserService.userContext.VemoContact__c != null)
            es.contact = UserService.userContext.VemoContact__c;
        es.email = esRes.email;
        es.eventType = esRes.eventType ;
        return es;
    }

    public virtual class EventSubscriptionResourceOutputV2{
        public String eventSubscriptionID{get;set;}
        public boolean email{get;set;}
        public String eventType {get;set;}
        
        public EventSubscriptionResourceOutputV2(){}
        
        public EventSubscriptionResourceOutputV2(EventSubscriptionService.EventSubscription es){
            this.eventSubscriptionID = es.eventSubscriptionID;
            this.email = es.email;
            this.eventType = es.eventType;
        }
    }
    
    public class PublicEventSubscriptionResourceOutputV2 extends EventSubscriptionResourceOutputV2{
        public PublicEventSubscriptionResourceOutputV2(){}
        public PublicEventSubscriptionResourceOutputV2(EventSubscriptionService.EventSubscription eSub){
            super(eSub);
        }
    }

    public class VemoEventSubscriptionResourceOutputV2 extends EventSubscriptionResourceOutputV2{
        public String account{get;set;}
        public String contact{get;set;}
                
        public VemoEventSubscriptionResourceOutputV2(){}        
        public VemoEventSubscriptionResourceOutputV2(EventSubscriptionService.EventSubscription eSub){
            super(eSub);
            this.account = eSub.account;
            this.contact = eSub.contact;
        }
    }       

  
    public class EventSubscriptionResourceInputV2{
        public String eventSubscriptionID{get;set;}
        public String eventType {get;set;}
        public boolean email{get;set;}
        
        public EventSubscriptionResourceInputV2(){
        }
        
        public void validatePOSTFields(){
            if(eventSubscriptionID != null) throw new VemoAPI.VemoAPIFaultException('eventSubscriptionID cannot be created in POST');
            if(eventType == null) throw new VemoAPI.VemoAPIFaultException('eventType is a required input parameter on POST');
        }
        
        public void validatePUTFields(){
            if(eventSubscriptionID == null) throw new VemoAPI.VemoAPIFaultException('eventSubscriptionID is a required input parameter on PUT');
        }
    }
}