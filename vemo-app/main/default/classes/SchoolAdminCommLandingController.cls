/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class SchoolAdminCommLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        System.debug('SchoolAdminCommLandingController.forwardToStartPage()');
        return Network.communitiesLanding();
    }
    
    public SchoolAdminCommLandingController() {
        System.debug('SchoolAdminCommLandingController()');
    }
}