@isTest
public class EmployerQueries_TEST {
  @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
   }
   
   static testMethod void testGetEmployerMapWithStudentID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Test.startTest();
        Map<Id, Employer__c> resultEmpHisMap = EmployerQueries.getEmployerMapWithStudentID(testStudentAccountMap.keySet());
        System.assertEquals(employerMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
    
    static testMethod void testGetEmployerMapWithEmployerID(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Employer__c> employerMap = TestDataFactory.createAndInsertEmployer(1, testStudentAccountMap);
        Test.startTest();
        Map<Id, Employer__c> resultEmpHisMap = EmployerQueries.getEmployerMapWithEmployerID(employerMap.keySet());
        System.assertEquals(employerMap.keySet().size(), resultEmpHisMap.keySet().size());
        Test.stopTest();
    }
}