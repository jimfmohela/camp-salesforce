@isTest
public class ReportInvestorAccountsTEST{
     private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com123', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com123', communitynickname = 'testcommunity123');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
            Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
            Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
            Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
            Map<ID, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
            Map<ID, StudentProgram__C> agreements = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
            
            for(StudentProgram__c sp:agreements.values()){
                sp.fundOwner__c = students.values()[0].id;  
                sp.primaryOwner__c = students.values()[0].id;  
                sp.paymentStreamOwner__c = students.values()[0].id;
            }
            
            //update agreements.values();
            dbUtil.updateRecords(agreements.values());
        }
        
    }
    
    @isTest public static void validateReport(){
               
        Test.StartTest(); 

            ReportInvestorAccounts cntrl = new ReportInvestorAccounts();
        
        Test.StopTest();  
    }               
}