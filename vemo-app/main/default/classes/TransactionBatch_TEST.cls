@isTest
public with sharing class TransactionBatch_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void purgeDeletedDisbursements_Test(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){ 
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, StudentProgram__c> studentPrgMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<ID, Transaction__c> trxnMap = TestDataFactory.createAndInsertTransactions(TestUtil.TEST_THROTTLE, studentPrgMap, 'Disbursement');
        for(Transaction__c trxn : trxnMap.values()){
            trxn.Deleted__c = true;
        }
        //update trxnMap.values();
        dbUtil.updateRecords(trxnMap.values());
        
        Test.startTest();
        TransactionBatch job = new TransactionBatch();
        job.job = TransactionBatch.JobType.PURGE_DELETED_DISBURSEMENTS;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }
}