public with sharing class DefermentService {

    public static List<Deferment> getDefermentMapWithStudent(set<Id> studentIDSet){    
        Map<Id, Deferment__c> defermentMap = DefermentQueries.getDefermentMapWithStudentID(studentIDSet);
        List<Deferment> defermentList = new List<Deferment>();
        for(Deferment__c de : defermentMap.values()){
            defermentList.add(new Deferment(de));
        }
        return defermentList;
    }    
     
    public static List<Deferment> getDefermentMapWithDefermentID(set<Id> defermentIDs){    
        Map<Id, Deferment__c> DefermentMap = DefermentQueries.getDefermentMapWithDefermentID(defermentIDs);
        List<Deferment> DefermentList = new List<Deferment>();
        for(Deferment__c de : DefermentMap.values()){
            DefermentList.add(new Deferment(de));
        }
        return DefermentList;
    }
    
    public static Set<ID> createDeferments(List<Deferment> deList){
        System.debug('DefermentService.createDeferment()');
        List<Deferment__c> newDEs = new List<Deferment__c>();
        for(Deferment de : deList){
            Deferment__c newDE = deToDE(de);
            newDEs.add(newDE);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.insertRecords(newDEs);
        Set<ID> DEIDs = new Set<ID>();
        for(Deferment__c de : newDEs){
            DEIDs.add(de.ID);
        }
        return DEIDs;
    }
    
    public static Set<ID> updateDeferments(List<Deferment> deList){
        System.debug('DefermentService.updateDeferment()');
        List<Deferment__c> newDEs = new List<Deferment__c>();
        for(Deferment de : deList){
            Deferment__c newDE = deToDE(de);
            newDEs.add(newDE);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newDEs);
        Set<ID> DEIDs = new Set<ID>();
        for(Deferment__c de : newDEs){
            DEIDs.add(de.ID);
        }
        return DEIDs;
    }
    
    public static Integer deleteDeferments(Set<ID> defermentIDs){
        System.debug('DefermentService.deleteCreditChecks()');
        Map<Id, Deferment__c> defermentMap = DefermentQueries.getDefermentMapWithDefermentID(defermentIDs);
        Integer numToDelete = defermentMap.size();
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.deleteRecords(defermentMap.values());
        return numToDelete;
    }
    
    public static Deferment__c deToDE(Deferment de){
        Deferment__c deObj = new Deferment__c();
        if(de.DefermentID != null) deObj.ID = de.DefermentID; 
        if(de.annualIncome != null) deObj.AnnualIncome__c = de.annualIncome; 
        if(de.averageHoursPerWeek != null) deObj.AverageHoursPerWeek__c = de.averageHoursPerWeek;
        if(de.averageSalaryPerMonth != null) deObj.AverageSalaryPerMonth__c = de.averageSalaryPerMonth;
        if(de.beginDate != null) deObj.BeginDate__c = de.beginDate;
        if(de.cityEmployment != null) deObj.CityEmployment__c = de.cityEmployment;
        //if(de.contactDate != null) deObj.ContactDate__c = de.contactDate ;      
        if(de.countryEmployment != null) deObj.CountryEmployment__c = de.countryEmployment;        
        if(de.countrySchool != null) deObj.CountrySchool__c = de.countrySchool;
        if(de.dateReported != null) deObj.DateReported__c = de.dateReported;        
        if(de.dateRequestSubmitted  != null) deObj.DateRequestSubmitted__c = de.dateRequestSubmitted ;        
        if(de.dateVerified != null) deObj.DateVerified__c = de.dateVerified;   
        if(de.employerAddress != null) deObj.EmployerAddress__c = de.employerAddress;
        if(de.employerName != null) deObj.EmployerName__c = de.employerName ;
        if(de.endDate != null) deObj.EndDate__c = de.endDate ;
        if(de.enrollmentBeginDate != null) deObj.EnrollmentBeginDate__c = de.enrollmentBeginDate ;
        if(de.enrollmentEndDate  != null) deObj.EnrollmentEndDate__c = de.enrollmentEndDate ;
        if(de.enrollmentStatus != null) deObj.EnrollmentStatus__c = de.enrollmentStatus ;        
        if(de.explanation != null) deObj.Explanation1__c = de.explanation;        
        //if(de.followUpDate != null) deObj.FollowUpDate__c = de.followUpDate;        
        if(de.hourlyCompensation != null) deObj.HourlyCompensation__c = de.hourlyCompensation ;                
        if(de.monthlyIncome != null) deObj.MonthlyIncome__c= de.monthlyIncome ;        
        if(String.isNotEmpty(de.noPaymentPaymentTerm)){
            deObj.NoPaymentPaymentTerm__c = de.noPaymentPaymentTerm == 'true' ?  true : false;
        }
        if(String.isNotEmpty(de.receivingUnemploymentBenefits)){
            deObj.ReceivingUnemploymentBenefits__c = de.receivingUnemploymentBenefits == 'true' ?  true : false;
        } 
        //if(de.resultsOutcome != null) deObj.ResultsOutcome__c = de.resultsOutcome ;           
        if(de.schoolAddress != null) deObj.SchoolAddress__c = de.schoolAddress;        
        if(de.schoolName != null) deObj.SchoolName__c = de.schoolName ; 
        if(de.SchoolPhone != null) deObj.SchoolPhone__c = de.SchoolPhone;               
        if(de.servicingCommentsNotes != null) deObj.ServicingCommentsNotes__c = de.servicingCommentsNotes ;        
        if(de.stateEmployment != null) deObj.StateEmployment__c = de.stateEmployment ;           
        if(de.stateSchool != null) deObj.StateSchool__c = de.stateSchool ;         
        if(de.status != null) deObj.Status__c = de.status;                      
        if(de.student != null) deObj.Student__c = de.student ;        
        if(de.type != null) deObj.Type__c = de.type ;
        if(de.unemploymentBeginDate != null) deObj.UnemploymentBeginDate__c = de.unemploymentBeginDate ;
        
        if(String.isNotEmpty(de.verified )){
            deObj.Verified__c = de.verified == 'true' ?  true : false;
        } 
        if(de.otherEnrollmentStatus != null) deObj.OtherEnrollmentStatus__c = de.otherEnrollmentStatus;
        if(de.EmployerPhoneNumber != null) deObj.EmployerPhoneNumber__c = de.EmployerPhoneNumber;
        if(de.EmploymentStartDate != null) deObj.EmploymentStartDate__c = de.EmploymentStartDate ;
        if(de.EmploymentEndDate != null ) deObj.EmploymentEndDate__c = de.EmploymentEndDate;
        if(de.EmploymentType != null ) deObj.EmploymentType__c = de.EmploymentType;
        //if(de.JobPositionTypeofWork != null) deObj.JobPositionTypeofWork__c = de.JobPositionTypeofWork;
        if(de.IWork35hrsorMoreaWeek != null) deObj.IWork35hrsorMoreaWeek__c = de.IWork35hrsorMoreaWeek;        
        if(de.EstimatedGraduationDate != null) deObj.EstimatedGraduationDate__c = de.EstimatedGraduationDate;
        if(de.SchoolCity != null) deObj.SchoolCity__c = de.SchoolCity;
        return deObj;
    } 

    public class Deferment{
        public String DefermentID{get;set;}
        public Decimal annualIncome{get;set;}
        public Decimal averageHoursPerWeek{get;set;}
        public Decimal averageSalaryPerMonth{get;set;}
        public Date beginDate{get;set;}
        public String cityEmployment{get;set;}
        //public Date contactDate {get;set;}
        public String countryEmployment{get;set;}
        public String countrySchool{get;set;}
        public Date dateReported{get;set;}
        public Date dateRequestSubmitted {get;set;}
        public Date dateVerified{get;set;}
        public String employerAddress {get;set;}        
        public String employerName {get;set;}
        public Date endDate{get;set;}
        public Date enrollmentBeginDate{get;set;}
        public Date enrollmentEndDate {get;set;}
        public String enrollmentStatus {get;set;}        
        public String explanation {get;set;}
        //public Date followUpDate{get;set;}        
        public Decimal hourlyCompensation {get;set;}
        public Decimal monthlyIncome {get;set;}
        public String noPaymentPaymentTerm {get;set;}        
        public String receivingUnemploymentBenefits {get;set;}        
        //public String resultsOutcome {get;set;}   
        public String schoolAddress {get;set;}
        public String schoolName {get;set;} 
        public String SchoolPhone {get;set;} 
        public String servicingCommentsNotes {get;set;}      
        public String stateEmployment {get;set;}           
        public String stateSchool {get;set;}        
        public String status {get;set;}         
        public String student {get;set;}     
        public String type {get;set;}        
        public Date unemploymentBeginDate {get;set;}
        public String verified {get;set;}
        public String otherEnrollmentStatus {get;set;}
        public String EmployerPhoneNumber {get;set;}
        //public String JobPositionTypeofWork {get;set;}
        public Date EmploymentEndDate {get;set;}
        public Date EmploymentStartDate {get;set;}
        public String EmploymentType {get;set;}
        public Boolean IWork35hrsorMoreaWeek {get;set;}
        public Date EstimatedGraduationDate {get;set;}
        public String SchoolCity {get;set;}
        
        public Deferment(){}

        public Deferment(Deferment__c de){
            this.DefermentID = de.ID;
            this.annualIncome = de.AnnualIncome__c;
            this.averageHoursPerWeek = de.AverageHoursPerWeek__c;
            this.averageSalaryPerMonth = de.AverageSalaryPerMonth__c;
            this.beginDate = de.BeginDate__c;
            this.cityEmployment = de.CityEmployment__c;
            //this.contactDate = de.ContactDate__c;           
            this.countryEmployment = de.CountryEmployment__c;            
            this.countrySchool = de.CountrySchool__c;            
            this.dateReported = de.DateReported__c;            
            this.dateRequestSubmitted = de.DateRequestSubmitted__c;            
            this.dateVerified = de.DateVerified__c;    
            this.employerAddress = de.EmployerAddress__c;            
            this.employerName = de.EmployerName__c;  
            this.endDate = de.EndDate__c;
            this.enrollmentBeginDate = de.EnrollmentBeginDate__c;
            this.enrollmentEndDate = de.EnrollmentEndDate__c ;          
            this.enrollmentStatus  = de.EnrollmentStatus__c;            
            //this.explanation = de.Explanation__c;
            this.explanation = de.Explanation1__c;
            //this.followUpDate = de.FollowUpDate__c;                      
            this.hourlyCompensation = de.HourlyCompensation__c;
            this.monthlyIncome = de.MonthlyIncome__c;            
            this.noPaymentPaymentTerm =  String.valueOf(de.NoPaymentPaymentTerm__c);
            this.receivingUnemploymentBenefits =  String.valueOf(de.ReceivingUnemploymentBenefits__c);
            //this.resultsOutcome = de.ResultsOutcome__c;
            this.schoolAddress = de.SchoolAddress__c ;   
            this.schoolName = de.SchoolName__c;
            this.SchoolPhone = de.SchoolPhone__c;   
            this.servicingCommentsNotes = de.ServicingCommentsNotes__c;
            this.stateEmployment = de.StateEmployment__c ;               
            this.stateSchool = de.StateSchool__c;            
            this.status = de.Status__c;              
            this.student = de.Student__c;             
            this.type = de.Type__c;  
            this.unemploymentBeginDate = de.UnemploymentBeginDate__c; 
            this.verified =  String.valueOf(de.Verified__c);
            this.OtherEnrollmentStatus = de.OtherEnrollmentStatus__c ;
            this.IWork35hrsorMoreaWeek = de.IWork35hrsorMoreaWeek__c;
            this.EmploymentType = de.EmploymentType__c;
            this.EmploymentStartDate = de.EmploymentStartDate__c;  
            this.EmploymentEndDate = de.EmploymentEndDate__c; 
            //this.JobPositionTypeofWork  = de.JobPositionTypeofWork__c;
            this.EmployerPhoneNumber  = de.EmployerPhoneNumber__c; 
            this.EstimatedGraduationDate = de.EstimatedGraduationDate__c;
            this.SchoolCity = de.SchoolCity__c;
        }
    }
}