/////////////////////////////////////////////////////////////////////////
// Class: PaymentAllocationService_TEST
// 
// Description: 
//      Test class for PaymentAllocationService
//
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-17   Jared Hagemann  Created                                 
// 
/////////////////////////////////////////////////////////////////////////
@isTest
public class PaymentAllocationService_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        //TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, PaymentMethod__c> testPaymentMethodMap = TestDataFactory.createAndInsertPaymentMethod(1, testStudentAccountMap);
        Map<ID, PaymentInstruction__c> testPaymentInsturctionMap = TestDataFactory.createAndInsertPaymentInstruction(1, testStudentAccountMap, testPaymentMethodMap);

        //Add a few fees
        //5 fees @ $1000 apiece
        Map<ID, Fee__c> testFeeMap = TestDataFactory.createAndInsertFee(5, testStudentAccountMap);

        //Add Agreements
        //20 Agreements @ $1000 a piece
        Map<ID, Account> schooolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schooolMap);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schooolMap);
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, testStudentAccountMap, programMap);
        Map<ID, StudentProgramMonthlyStatus__c> agreementMonthlyStatusMap = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(20, agreementMap);
        Map<ID, StudentProgramAmountDue__c> agreementBillMap = TestDataFactory.createAndInsertStudentProgramAmountDue(agreementMap, agreementMonthlyStatusMap);
        Date today = System.today();
        Integer count = 0;
        for(StudentProgramAmountDue__c bill : agreementBillMap.values()){
            //Past Due Reconciliations
            if(count < 5){
                bill.AssessmentDateTime__c = today.addMonths(-2);
                bill.Type__c = 'Reconciliation';
                bill.Amount__c = 1000;
            }
            //Current Reconciliations
            else if(count >= 5 && count < 10){
                bill.AssessmentDateTime__c = today;
                bill.Type__c = 'Reconciliation';
                bill.Amount__c = 1000;
            }
            //Past Due Monthly Amounts
            else if(count >= 10 && count < 15){
                bill.AssessmentDateTime__c = today.addMonths(-2);
                bill.Type__c = 'Monthly Amount';
                bill.Amount__c = 1000;
            }
            //Current Monthly Amounts
            else{
                bill.AssessmentDateTime__c = today;
                bill.Type__c = 'Monthly Amount';
                bill.Amount__c = 1000;
            }
            count = count + 1;
        }
        //update agreementBillMap.values();
        dbUtil.updateRecords(agreementBillMap.values());
        Map<Id, PaymentInstruction__c> paymenetInstructionMap = TestDataFactory.createAndInsertPaymentInstruction(1, testStudentAccountMap, testPaymentMethodMap);
        }
    }

    static testMethod void testAllocatePaymentInFull(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();   
        //setupData();      
        PaymentInstruction__c pi = PaymentInstructionQueries.getPaymentInstructionMap().values().get(0);
        //pi.fullyAllocated__c = false;
        pi.Amount__c = 25000;
        //update pi;
        dbUtil.updateRecord(pi);

        Test.startTest();
        List<PaymentAllocationService.PaymentInstruction> resultPaymentInstructions = PaymentAllocationService.allocatePaymentV2(pi.Id, false);
        Test.stopTest();
        PaymentAllocationService.PaymentInstruction result = resultPaymentInstructions.get(0);
        System.assertEquals(25000, result.paymentAmount);
        //System.assertEquals(0, result.remainingAllocation);
    }

    static testMethod void testAllocatePaymentParital(){ 
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();   
        //setupData();           
        PaymentInstruction__c pi = PaymentInstructionQueries.getPaymentInstructionMap().values().get(0);
        //pi.fullyAllocated__c = false;
        pi.Amount__c = 500;
        //update pi;
        dbUtil.updateRecord(pi);
        
        Test.startTest();
        List<PaymentAllocationService.PaymentInstruction> resultPaymentInstructions = PaymentAllocationService.allocatePaymentV2(pi.Id, false);
        Test.stopTest();
        PaymentAllocationService.PaymentInstruction result = resultPaymentInstructions.get(0);
        System.assertEquals(500, result.paymentAmount);
        //System.assertEquals(500, result.remainingAllocation);
        //List<Case> cases = [select Id from Case];
        //System.assertEquals(1, caseMap.size());
    }
}