/////////////////////////////////////////////////////////////////////////
// Class: EventTypeService_TEST
// 
// Description: 
//  Test class for EventTypeService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created              
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventTypeService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventTypeWithEventTypeID(){
        Map<Id, EventType__c> testEvntTypeMap = TestDataFactory.createAndInsertEventType(1);
        Test.startTest();
        List<EventTypeService.EventType> resultEvntTypeList = EventTypeService.getEventTypeWithEventTypeID(testEvntTypeMap.keySet());
        System.assertEquals(testEvntTypeMap.keySet().size(), resultEvntTypeList.size());
        Test.stopTest();
    }
    static testMethod void testGetAllEventTypesMap(){
        Map<Id, EventType__c> testEvntTypeMap = TestDataFactory.createAndInsertEventType(1);
        Test.startTest();
        List<EventTypeService.EventType> resultEvntTypeList = EventTypeService.getAllEventTypesMap();
        System.assertEquals(testEvntTypeMap.keySet().size(), resultEvntTypeList.size());
        Test.stopTest();
    }
}