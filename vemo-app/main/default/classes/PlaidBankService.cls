public with sharing class PlaidBankService{

    public static List<PlaidBank> getPlaidBankWithPlaidBankID(Set<ID> plaidBankIds){
        System.debug('PlaidBankService.getPlaidBankWithPlaidBankID()');
        Map<ID, PlaidBank__c> plaidBankMap = PlaidBankQueries.getPlaidBankMapWithID(plaidBankIds);
        List<PlaidBank> plaidBankList = new List<PlaidBank>();
        for(plaidBank__c pb : plaidBankMap.values()){
            PlaidBank pbObj = new PlaidBank(pb);
            plaidBankList.add(pbObj);
        }
        return plaidBankList;
    }
    
    public static List<PlaidBank> getPlaidBankWithStudentID(Set<ID> studentIDs){
        System.debug('PlaidBankService.getPlaidBankWithPlaidBankStudentID()');
        Map<ID, PlaidBank__c> plaidBankMap = PlaidBankQueries.getPlaidBankMapWithStudentID(studentIDs);
        List<PlaidBank> plaidBankList = new List<PlaidBank>();
        for(plaidBank__c pb : plaidBankMap.values()){
            PlaidBank pbObj = new PlaidBank(pb);
            plaidBankList.add(pbObj);
        }
        return plaidBankList;
    }
    
    public static Set<ID> updatePlaidBanks(List<PlaidBank> pBanks){
        System.debug('PlaidBankService.updatePlaidBanks()');
        List<PlaidBank__c> newPBs = new List<PlaidBank__c>();
        for(PlaidBank pb : pBanks){
            //pb.studentID = null;
            PlaidBank__c newPB = pbToPB(pb);
            newPBs.add(newPB);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newPBs);
        Set<ID> pbIDs = new Set<ID>();
        for(PlaidBank__c pb : newPBs){
            pbIDs.add(pb.ID);
        }
        return pbIDs;
    }
    
    public static PlaidBank__c pbToPB(PlaidBank pb){
        PlaidBank__c pbObj = new PlaidBank__c();
        if(pb.plaidBankId!= null) pbObj.ID = pb.plaidBankId; 
        if(pb.plaidStatus!= null) pbObj.PlaidStatus__c = pb.plaidStatus;
               
        return pbObj;
    }
    
    
    public class PlaidBank{
        public String plaidBankId {get;set;}
        public String bankName {get;set;}
        public String studentId {get;set;}
        public String accessToken {get;set;}
        public String itemID {get;set;}
        public String plaidStatus {get;set;}
        public Boolean updatePlaidPassword {get;set;}
        public String accountNumber {get;set;}
        
        public plaidBank(){}
        
        public plaidBank(PlaidBank__c pb){
            this.plaidBankId = pb.ID;
            this.bankName = pb.Name;
            this.studentId = pb.Student__c;
            this.accessToken = pb.AccessToken__c; 
            this.itemID = pb.ItemId__c;
            this.plaidStatus = pb.PlaidStatus__c;
            this.updatePlaidPassword = pb.updatePlaidPassword__c ;
        }
    }
}