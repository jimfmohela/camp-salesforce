public with sharing class ReportTotalAccountsWithISAPayment{
    transient public List<reportDataWrapper> reportData {get;set;}
    transient public String csv {get;set;}
    transient public List<school_StudentWithISAWrapper> school_StudentWithISAWrapperList {get;set;}
    public Integer countAccounts {get;set;}
    
    /////////////////////////////////////
    ///Constructor 
    /////////////////////////////////////
    public ReportTotalAccountsWithISAPayment(){
        reportData = new List<reportDataWrapper>();
        school_StudentWithISAWrapperList = new List<school_StudentWithISAWrapper>();
        countAccounts = 0;
        createReport();
    }
    
    ////////////////////////////////////////
    ///Method to build report data 
    ///////////////////////////////////////
    public void createReport(){
        reportData = new List<reportDataWrapper>();
        school_StudentWithISAWrapperList = new List<school_StudentWithISAWrapper>();
        
        Id sRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Student').getRecordTypeId();
        List<Account> accList = new List<Account>();
        accList = [Select Id, Name, PersonEmail, PrimarySchool__pr.Name, (Select ID, Name, Status__c from Student_Programs__r Where Status__c = 'Payment') from Account where RecordTypeID = :sRecordId Order By Name];
        System.debug('accList*** ' + accList.size());
        
        Integer countAccounts = 0;
        Map<String,Integer> school_StudentWithISA = new Map<String,Integer>();
        for(Account acc : accList){
            ReportDataWrapper rdw = new ReportDataWrapper();
            rdw.allISAsNumbers = '';
            rdw.totalISAsCount = 0;
            for(StudentProgram__c stu : acc.Student_Programs__r){
                if(stu.Status__c ==  'Payment'){
                    rdw.allISAsNumbers = stu.Name + '; ' + rdw.allISAsNumbers;
                    rdw.status = stu.Status__c;
                    rdw.totalISAsCount = rdw.totalISAsCount + 1;
                }
            }
            rdw.allISAsNumbers = (rdw.allISAsNumbers).removeEnd('; ');
            if(String.isNotBlank(rdw.allISAsNumbers)){
                if(!school_StudentWithISA.containsKey(acc.PrimarySchool__pr.Name))
                    school_StudentWithISA.put(acc.PrimarySchool__pr.Name,1);
                else
                    school_StudentWithISA.put(acc.PrimarySchool__pr.Name,school_StudentWithISA.get(acc.PrimarySchool__pr.Name)+1);
                rdw.schoolName = acc.PrimarySchool__pr.Name;
                rdw.studentName = acc.Name;
                rdw.studentEmail = acc.PersonEmail;
                //rdw.totalISAsCount = aggregateMap.get(acc.Name);
                reportData.add(rdw);             
                countAccounts = countAccounts + 1;
            }            
        }
        for(string key: school_StudentWithISA.keySet()){
            school_StudentWithISAWrapper newWrap = new school_StudentWithISAWrapper();
            newWrap.schoolName = key;
            newWrap.studentWithISA = school_StudentWithISA.get(key);
            school_StudentWithISAWrapperList.add(newWrap);
        }
    }
    
    public void buildCsvString(){
        csv = 'School Name, Student Name, Student Email, All ISAs Number, Total ISAs Count, Status';
        csv += '\n';
        createReport();
        
        for(ReportDataWrapper rdw :reportData){
            csv += rdw.schoolName + ',';
            csv += rdw.studentName + ',';
            csv += rdw.studentEmail + ',';
            csv += rdw.allISAsNumbers + ',';
            csv += rdw.totalISAsCount + ',';
            csv += rdw.status + ',';
            csv += '\n';
        
            countAccounts = countAccounts + 1;
        }
        System.debug('countAccounts*** ' + countAccounts);
        csv += '\n';
        csv += '\n';
        csv += 'Total Accounts with at least 1 ISA in Payment Status' + ',';
        csv += countAccounts + ',';
        csv += '' + ',';
        csv += '' + ',';
        csv += '' + ',';
        csv += '' + ',';
        csv += '\n';
        for(school_StudentWithISAWrapper s: school_StudentWithISAWrapperList){
            csv += '\n';
            csv += s.schoolName + ',';
            csv += s.studentWithISA + ',';
            csv += '' + ',';
            csv += '' + ',';
            csv += '' + ',';
            csv += '' + ',';
        }
        
    }
    
    ////////////////////////////////////////
    ///Call the export VF Page
    ////////////////////////////////////////
    public PageReference exportToCSV(){
        PageReference pg = new PageReference('/apex/ReportTotalAccountsWithISAPaymentExport');
        pg.setRedirect(false);
        return pg;
    }

    
    ///////////////////////////////////////
    ///Wrapper to hold complete report data
    ///////////////////////////////////////
    public class ReportDataWrapper{
        public String schoolName{get;set;}
        public String studentName{get;set;}
        public String studentEmail{get;set;}
        public String allISAsNumbers{get;set;}
        public Integer totalISAsCount{get;set;}
        public String status{get;set;}
    }
    
    ///////////////////////////////////////
    ///Wrapper to hold school and account with ISA
    ///////////////////////////////////////
    public class school_StudentWithISAWrapper{
        public String schoolName{get;set;}
        public Integer studentWithISA {get;set;}
    }
}