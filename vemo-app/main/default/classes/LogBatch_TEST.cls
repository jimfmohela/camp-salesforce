@isTest
private class LogBatch_TEST
{

    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void instantiateEventInstance(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        
        log__c lg = new Log__c(Log__c = 'test',
                            SecondsLogged__c = DateTime.now().second(),
                            RelatedTo__c = 'relatedTo',           
                            DateTimeLogged__c = DateTime.now(),
                            MillisecondsLogged__c = DateTime.now().millisecond());
        dbUtil.insertRecord(lg);
        Datetime yesterday = Datetime.now().addDays(-2);
        Test.setCreatedDate(lg.Id, yesterday);

        Test.startTest();
        
        LogBatch job = new LogBatch ();
        job.job = LogBatch.JobType.PURGE;
        Database.executeBatch(job, 200);
        Test.stopTest();
      }
}