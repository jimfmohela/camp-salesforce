/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIPaymentInstructionResource
// 
// Description: 
//  Direction Central for PaymentInstruction API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-05-18   Jared Hagemann  Created              
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIPaymentInstructionResource {
    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }   
            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){  
        System.debug('VemoAPIPaymentInstructionResource.handleGetV1');
        String paymentInstructionIDParam = api.params.get('paymentInstructionID');
        String studentIDParam = api.params.get('studentID');
        List<PaymentInstructionService.PaymentInstruction> pymntInsList = new List<PaymentInstructionService.PaymentInstruction>();
        if(paymentInstructionIDParam != null){
            pymntInsList = PaymentInstructionService.getPaymentInstructionWithPaymentInstructionID(VemoApi.parseParameterIntoIDSet(paymentInstructionIDParam));
        }
        else if(studentIDParam != null){
            pymntInsList = PaymentInstructionService.getPaymentInstructionWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter paymentInstructionID or studentID');
        }
        List<PaymentInstructionOutputV1> results = new List<PaymentInstructionOutputV1>();
        
        Set<ID> idsToVerify = new Set<ID>();
        for(PaymentInstructionService.PaymentInstruction pymntIns : pymntInsList){
            idsToVerify.add((ID)pymntIns.studentID); 
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        for(PaymentInstructionService.PaymentInstruction pymntIns : pymntInsList){
            if(verifiedAccountMap.containsKey((ID)pymntIns.studentId)){
                results.add(new PaymentInstructionOutputV1(pymntIns));
            }    
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){  
        System.debug('VemoAPIPaymentInstructionResource.handleGetV2');
        String paymentInstructionIDParam = api.params.get('paymentInstructionID');
        String studentIDParam = api.params.get('studentID');
        String statusParam = api.params.get('status');
        String latestParam = api.params.get('latest');
        
        List<PaymentInstructionService.PaymentInstruction> pymntInsList = new List<PaymentInstructionService.PaymentInstruction>();
        if(paymentInstructionIDParam != null){
            pymntInsList = PaymentInstructionService.getPaymentInstructionWithPaymentInstructionID(VemoApi.parseParameterIntoIDSet(paymentInstructionIDParam));
        }
        else if(studentIDParam != null){
            if(statusParam != null && statusParam != 'null')
                pymntInsList = PaymentInstructionService.getPaymentInstructionWithStudentIDNStatus(VemoApi.parseParameterIntoIDSet(studentIDParam), 
                                                                                            VemoApi.parseParameterIntoStringSet(statusParam));
            else
                pymntInsList = PaymentInstructionService.getPaymentInstructionWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam));
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter paymentInstructionID or studentID');
        }
        List<PaymentInstructionOutputV2> results = new List<PaymentInstructionOutputV2>();
        
        Set<ID> idsToVerify = new Set<ID>();
        for(PaymentInstructionService.PaymentInstruction pymntIns : pymntInsList){
            idsToVerify.add((ID)pymntIns.studentID); 
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(PaymentInstructionService.PaymentInstruction pymntIns : pymntInsList){
            if(verifiedAccountMap.containsKey((ID)pymntIns.studentId)){
            
                if(GlobalSettings.getSettings().vemoDomainAPI){
                    if(latestParam == 'true' && pymntIns.paymentDate != null && pymntIns.paymentDate <= Date.today())
                        results.add(new VemoPaymentInstructionOutputV2(pymntIns));
                }else if(latestParam == 'true' && pymntIns.paymentDate != null && pymntIns.paymentDate <= Date.today()){
                    results.add(new PublicPaymentInstructionOutputV2(pymntIns));
                }
            } 
               
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIPaymentInstructionResource.handlePostV1');
        List<PaymentInstructionService.PaymentInstruction> newPaymentInstructions = new List<PaymentInstructionService.PaymentInstruction>();
        List<PaymentInstructionInputV1> paymentInstructionJSON = (List<PaymentInstructionInputV1>)JSON.deserialize(api.body, List<PaymentInstructionInputV1>.class);
        
        /* Check to make sure this authID can access these records*/
        Set<ID> idsToVerify = new Set<ID>();
        
        for(PaymentInstructionInputV1 pymntIns : paymentInstructionJSON){
            idsToVerify.add((ID)pymntIns.studentID);
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(PaymentInstructionInputV1 pymntIns : paymentInstructionJSON){
            if(verifiedAccountMap.containsKey((ID)pymntIns.studentID)){
                pymntIns.validatePOSTFields();
                PaymentInstructionService.PaymentInstruction pymntInsServ = new PaymentInstructionService.PaymentInstruction();
                pymntInsServ = paymentInsturctionInputV1ToPaymentInstruction(pymntIns);
                newPaymentInstructions.add(pymntInsServ);
            }
        }
        Set<ID> paymentInstructionIDs = PaymentInstructionService.createPaymentInstruction(newPaymentInstructions);
        return (new VemoAPI.ResultResponse(paymentInstructionIDs, paymentInstructionIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIPaymentInstructionResource.handlePostV1');
        List<PaymentInstructionService.PaymentInstruction> newPaymentInstructions = new List<PaymentInstructionService.PaymentInstruction>();
        List<PaymentInstructionInputV1> paymentInstructionJSON = (List<PaymentInstructionInputV1>)JSON.deserialize(api.body, List<PaymentInstructionInputV1>.class);
        
        //Set<ID> PaymentInstructionIdsToBeUpdated = new Set<ID>();
        Set<ID> idsToVerify = new Set<ID>();
        for(PaymentInstructionInputV1 pymntIns : paymentInstructionJSON){
            idsToVerify.add((ID)pymntIns.paymentInstructionId); 
        }
        
        Map<ID, PaymentInstruction__c> VerifiedPaymentInstructionMap = PaymentInstructionQueries.getPaymentInstructionMapWithPaymentInstructionID(idsToVerify); //authorized records should be returned
        //for(PaymentInstruction__c pymntIns: paymentInstructionMap.values()){
        //    idsToVerify.add((ID)pymntIns.Student__c);
        //}
        //Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        for(PaymentInstructionInputV1 pymntIns : paymentInstructionJSON){
            if(VerifiedPaymentInstructionMap.containsKey((ID)pymntIns.paymentInstructionID)){
                pymntIns.validatePUTFields();
                PaymentInstructionService.PaymentInstruction pymntInsServ = new PaymentInstructionService.PaymentInstruction();
                pymntInsServ = paymentInsturctionInputV1ToPaymentInstruction(pymntIns);
                newPaymentInstructions.add(pymntInsServ);
            }
        }
        Set<ID> paymentInstructionIDs = PaymentInstructionService.updatePaymentInstruction(newPaymentInstructions);
        return (new VemoAPI.ResultResponse(paymentInstructionIDs, paymentInstructionIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIPaymentInstructionResource.handleDeleteV1');
        String paymentInstructionIDParam = api.params.get('paymentInstructionID');
        
        Map<ID, PaymentInstruction__c> paymentInstructionMap = PaymentInstructionQueries.getPaymentInstructionMapWithPaymentInstructionID(VemoApi.parseParameterIntoIDSet(paymentInstructionIDParam));
        
        Set<ID> idsToVerify = new Set<ID>();
        for(PaymentInstruction__c  pymntIns: paymentInstructionMap.values()){
            idsToVerify.add(pymntIns.Student__c); 
        }
        /* Check to make sure this authID can access these records*/
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(idsToVerify); //authorized records should be returned
        
        Set<ID> paymentInstructionsToBeDeleted = new Set<ID>();
        
        for(PaymentInstruction__c  pymntIns: paymentInstructionMap.values()){
            if(verifiedAccountMap.containsKey(pymntIns.Student__c)){
                paymentInstructionsToBeDeleted.add(pymntIns.id); 
            }
        }
        Integer numToDelete = 0;
        if(paymentInstructionsToBeDeleted.size()>0){
            numToDelete = PaymentInstructionService.deletePaymentInstruction(paymentInstructionsToBeDeleted);
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    
    public static PaymentInstructionService.PaymentInstruction paymentInsturctionInputV1ToPaymentInstruction(PaymentInstructionInputV1 input){
        PaymentInstructionService.PaymentInstruction output = new PaymentInstructionService.PaymentInstruction();
        output.paymentInstructionID = input.paymentInstructionID;           
        output.amount = input.amount;
        output.paymentDate = input.paymentDate;
        output.paymentMethodID = input.paymentMethodID;
        output.studentID = input.studentID;
        //output.status = input.status;
        if(input.status == 'Deleted By Student')
            output.status = 'Deleted By Student';
        else
            output.status = 'Created By Student';
        //output.transactionDirection = input.transactionDirection;
        output.transactionDirection = 'Inbound';
        output.description = String.isBlank(input.description) ? 'blank' : input.description;
        return output;
    }

    public class PaymentInstructionInputV1{
        public String paymentInstructionID {get;set;}
        public Decimal amount {get;set;}
        public Date paymentDate {get;set;}
        public String paymentMethodID {get;set;}
        public String studentID {get;set;}
        public String status {get;set;}
        public String transactionDirection {get;set;}
        public String description {get;set;}

        public PaymentInstructionInputV1(Boolean testValues){
            if(testValues){
                this.amount = 1000;
                this.paymentDate = System.today();
                this.status = 'None';
            }
        }

        public void validatePOSTFields(){
            if(paymentInstructionID != null) throw new VemoAPI.VemoAPIFaultException('paymentInstructionID cannot be created in POST');
            if(amount != null && amount < 1) throw new VemoAPI.VemoAPIFaultException('Amount should not be less than $1');
        }
        public void validatePUTFields(){
            if(paymentInstructionID == null) throw new VemoAPI.VemoAPIFaultException('paymentInstructionID is a required input parameter on PUT');
            if(studentID != null) throw new VemoAPI.VemoAPIFaultException('studentID cannot be updated');
            if(amount != null && amount < 1) throw new VemoAPI.VemoAPIFaultException('Amount should not be less than $1');
        }
    }

    public class PaymentInstructionOutputV1{
        
        public PaymentInstructionOutputV1(PaymentInstructionService.PaymentInstruction pymntIns){
            this.paymentInstructionID = pymntIns.paymentInstructionID;  
            this.amount = pymntIns.amount;
            this.paymentDate = pymntIns.paymentDate;
            this.paymentMethodID = pymntIns.paymentMethodID;
            this.studentID = pymntIns.studentID; 
            this.status = pymntIns.status;
            this.transactionDirection = pymntIns.transactionDirection;
            this.description = pymntIns.description;
        }
        public String paymentInstructionID {get;set;}
        public Decimal amount {get;set;}
        public Date paymentDate {get;set;}
        public String paymentMethodID {get;set;}
        public String studentID {get;set;}
        public String status {get;set;}
        public String transactionDirection {get;set;}
        public string description {get;set;}
    }
    
    public virtual class PaymentInstructionOutputV2{
        public PaymentInstructionOutputV2(){
        }
        public PaymentInstructionOutputV2(PaymentInstructionService.PaymentInstruction pymntIns){
            this.paymentInstructionID = pymntIns.paymentInstructionID;  
            this.amount = pymntIns.amount;
            this.paymentDate = pymntIns.paymentDate;
            this.paymentMethodID = pymntIns.paymentMethodID;
            this.studentID = pymntIns.studentID; 
            this.status = pymntIns.status;
        }
        public String paymentInstructionID {get;set;}
        public Decimal amount {get;set;}
        public Date paymentDate {get;set;}
        public String paymentMethodID {get;set;}
        public String studentID {get;set;}
        public String status {get;set;}
    }
    
    public class PublicPaymentInstructionOutputV2 extends PaymentInstructionOutputV2{
        public PublicPaymentInstructionOutputV2(){
        }
        public PublicPaymentInstructionOutputV2(PaymentInstructionService.PaymentInstruction pymntIns){
            super(pymntIns);
        }
    }

    public class VemoPaymentInstructionOutputV2 extends PaymentInstructionOutputV2{
        public String privateInfo {get;set;}
        public VemoPaymentInstructionOutputV2(){
        }
        public VemoPaymentInstructionOutputV2(PaymentInstructionService.PaymentInstruction pymntIns){
            super(pymntIns);
            this.privateInfo = 'test';
        }
    }
    
}