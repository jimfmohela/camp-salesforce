public class FinancialFitnessReportCls{
    
    public List<FFQuizAttempt__c> quizAttemptList{get;set;}
    public List<FFQuizResponse__c> quizResponsesList{get;set;}
    
    public FinancialFitnessReportCls(){
        string studentProgramID = ApexPages.currentPage().getParameters().get('spID');
        string quizAttemptID = ApexPages.currentPage().getParameters().get('qaID');
        Set<String> quizAttemptIDSet = new Set<String>();
        
        quizAttemptList = new List<FFQuizAttempt__c>();
        quizResponsesList = new List<FFQuizResponse__c>();
        
        string query = 'select id,AttemptOrder__c,CreatedDate,StartTime__c,SubmitTime__c,'+
                        'Result__c,Contract__r.Program__r.name,Contract__r.Program__r.ProgramName__c,'+
                        'Contract__r.Student__r.Name,Contract__r.Student__r.PersonEmail from FFQuizAttempt__c where ';
        
        if(studentProgramID != null && studentProgramID != ''){
            /*quizAttemptList = [select id,AttemptOrder__c,CreatedDate,StartTime__c,SubmitTime__c,
                                Result__c,Contract__r.Program__r.name,Contract__r.Program__r.ProgramName__c,
                                Contract__r.Student__r.Name,Contract__r.Student__r.PersonEmail
                                from FFQuizAttempt__c where Contract__c =: studentProgramID order by AttemptOrder__c DESC];*/
            query = query+'Contract__c =: studentProgramID order by AttemptOrder__c DESC';
            quizAttemptList = database.query(query);
                                
            for(FFQuizAttempt__c qa: quizAttemptList){
                quizAttemptIDSet.add(qa.id);
            }
        }
        else{        
            if(quizAttemptID != null && quizAttemptID != ''){
                 /*quizAttemptList = [select id,AttemptOrder__c,CreatedDate,StartTime__c,SubmitTime__c,
                                    Result__c,Contract__r.Program__r.name,Contract__r.Program__r.ProgramName__c,
                                    Contract__r.Student__r.Name,Contract__r.Student__r.PersonEmail
                                    from FFQuizAttempt__c where Id =: quizAttemptID order by AttemptOrder__c DESC];*/
                 query = query+'Id =: quizAttemptID order by AttemptOrder__c DESC';
                 quizAttemptList = database.query(query);
                                  
                 quizAttemptIDSet.add(quizAttemptID);
            }
        }
        
        quizResponsesList = [select id,QuestionText__c,AnswerText__c,CorrectAnswer__c,FFQuizAttempt__c from FFQuizResponse__c
                                where FFQuizAttempt__c IN :quizAttemptIDSet];
        
        
    }
}