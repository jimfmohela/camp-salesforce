@isTest
public with sharing class StudProgMnthStsTriggerHandler_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    //to do jared - this is just a dummy method to get past test levels
    @isTest
    public static void validateDML(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(USerWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(TestUtil.TEST_THROTTLE, students, programs);

        List<StudentProgramMonthlyStatus__c> monthlyStatus = new List<StudentProgramMonthlyStatus__c>();
        monthlyStatus.add(new StudentProgramMonthlyStatus__c(Activity__c = 'Grace',
                                                             Month__c = 'January',
                                                             Year__c = '2017',
                                                             Agreement__c = studProgramMap.values()[0].id));
        //insert monthlyStatus;
        //update monthlyStatus;
        //delete monthlyStatus;
        //undelete monthlyStatus;
        dbUtil.insertRecords(monthlyStatus);
        dbUtil.updateRecords(monthlyStatus);
        dbUtil.deleteRecords(monthlyStatus);
        dbUtil.undeleteRecords(monthlyStatus);
        }
    }
}