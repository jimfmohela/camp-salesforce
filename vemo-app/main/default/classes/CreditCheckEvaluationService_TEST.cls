@isTest
public class CreditCheckEvaluationService_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, 
                                                                                    TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, CreditCheck__c> creditCheckMap = TestDataFactory.createAndInsertCreditCheck(1, 
                                                                                    studentMap);
        Map<ID, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, programMap);

    }
    
    @isTest public static void validateGetCreditCheckEvaluationWithCreditCheckEvalID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        List<CreditCheckEvaluationService.CreditCheckEvaluation> resultCCMap = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckEvalID(cceMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.size());
        Test.stopTest();
    }
    
    @isTest public static void validateGetCreditCheckEvaluationWithCreditCheckID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__C> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        List<CreditCheckEvaluationService.CreditCheckEvaluation> resultCCMap = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckID(ccMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.size());
        Test.stopTest();
    }
    
    @isTest public static void validateGetCreditCheckEvaluationWithAgreementID(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__C> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 
        Test.startTest();
        List<CreditCheckEvaluationService.CreditCheckEvaluation> resultCCMap = CreditCheckEvaluationService.getCreditCheckEvaluationWithAgreementID(agreementMap.keySet());
        System.assertEquals(cceMap.keySet().size(), resultCCMap.size());
        Test.stopTest();
    }
    
    @isTest public static void validateCreateCreditCheckEvaluations(){

        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        
        List<CreditCheckEvaluationService.CreditCheckEvaluation> cceList = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        for(Integer i = 0; i<ccMap.size(); i++){
             CreditCheckEvaluationService.CreditCheckEvaluation cce = new CreditCheckEvaluationService.CreditCheckEvaluation();
             cce.CreditCheckID = ccMap.values().get(i).id;
             cce.AgreementID = agreementMap.values().get(0).id; 
             cceList.add(cce);
            
        }
        Test.startTest();
        Set<ID> cceIDs = CreditCheckEvaluationService.createCreditCheckEvaluations(cceList);
        System.assertEquals(cceList.size(), CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(cceIDs).size());
        Test.stopTest();
    }
    
    @isTest public static void  validateUpdateCreditCheckEvaluations(){
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        
        List<CreditCheckEvaluationService.CreditCheckEvaluation> cceList = new List<CreditCheckEvaluationService.CreditCheckEvaluation>();
        for(Integer i = 0; i<ccMap.size(); i++){
            CreditCheckEvaluationService.CreditCheckEvaluation cce = new CreditCheckEvaluationService.CreditCheckEvaluation();
            cce.CreditCheckID = ccMap.values().get(i).id;
            cce.AgreementID = agreementMap.values().get(0).id; 
            cceList.add(cce);
        }
        Set<ID> cceIDs = CreditCheckEvaluationService.createCreditCheckEvaluations(cceList);
        List<CreditCheckEvaluationService.CreditCheckEvaluation> newCCEs = CreditCheckEvaluationService.getCreditCheckEvaluationWithCreditCheckEvalID(cceIDs);
        for(CreditCheckEvaluationService.CreditCheckEvaluation cce : newCCEs){
            cce.Status = 'Approved';
        }
        Test.startTest();
        Set<ID> updatedCCEIDs = CreditCheckEvaluationService.updateCreditCheckEvaluations(newCCEs);
        Test.stopTest();
        Map<ID, CreditCheckEvaluation__c> cceMap = CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckEvalID(updatedCCEIDs);
        for(CreditCheckEvaluation__c cce : cceMap.values()){
            System.assertEquals('Approved', cce.status__c);
        }
    }
    
    @isTest public static void validateDeleteCreditCheckEvaluations(){
        Map<ID, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<ID, CreditCheck__c> ccMap = CreditCheckQueries.getCreditCheckMap();
        Map<ID, CreditCheckEvaluation__c> cceMap = TestDataFactory.createAndInsertCreditCheckEvaluation(1, ccMap, agreementMap); 

        Test.startTest();
        Integer numDeleted = CreditCheckEvaluationService.deleteCreditCheckEvaluations(cceMap.keySet());
        System.assertEquals(cceMap.size(), numDeleted);
        System.assertEquals(0, CreditCheckEvaluationQueries.getCreditCheckEvaluationMapWithCreditCheckID(ccMap.keyset()).size());
        Test.stopTest();
    }

}