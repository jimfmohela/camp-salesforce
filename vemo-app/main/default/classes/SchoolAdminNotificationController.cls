public class SchoolAdminNotificationController{
    public Id eventInstanceId {get;set;}
    public EmailAttributes emailAttributes {get;set;}
    public string status{get;set;}
    public string dateStamp{get;set;}
    
    public string getLoadData(){
        List<EventInstance__c> eInstnaceList = [select id,EventAttributes__c, Event__r.createdDate,Event__r.EventType__r.name from EventInstance__c where Id=:  eventInstanceId ];
        if(eInstnaceList != null && eInstnaceList.size()>0 && eInstnaceList[0].EventAttributes__c != null){
            EmailAttributes attributes = new EmailAttributes();
            EmailAttributes = attributes.parse(eInstnaceList[0].EventAttributes__c); 
            
            if(eInstnaceList[0].Event__r.EventType__r.name != null)
                status = eInstnaceList[0].Event__r.EventType__r.name;  
            if(eInstnaceList[0].Event__c != null){
                dateStamp = eInstnaceList[0].Event__r.createdDate.format('MM/dd/yyyy HH:mm:ss', 'America/New_York') + ' EST';
            }
        }
        return ''; //dummy return string
    }
    
    public class EmailAttributes{
        public string status {get;set;}
        public string vemoContractNumber{get;set;}
        public string studentName{get;set;}
        public string programName{get;set;}  
        
        public EmailAttributes parse(String json) {
            return (EmailAttributes) System.JSON.deserialize(json, EmailAttributes.class);
        }      
    }
}