/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIEventTypeResource_TEST
// 
// Description: 
//      Test class for VemoAPIEventTypeResource
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-18   Kamini Singh  Created 
/////////////////////////////////////////////////////////////////////////
@isTest
public class VemoAPIEventTypeResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV2(){
        Map<Id, EventType__c> testEvntTypeMap = TestDataFactory.createAndInsertEventType(1);
        
        Map<String, String> etParams = new Map<String, String>();
        etParams.put('eventTypeID', TestUtil.createStringFromIDSet(testEvntTypeMap.keySet()));
        etParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo etApiInfo = TestUtil.initializeAPI('v2', 'GET', etParams, null);
        
        //insert new GlobalSettings__c (SetupOwnerId=UserInfo.getOrganizationId(), vemoDomainAPI__c = true);

        Test.startTest();
        VemoAPI.ResultResponse eTypeResult = (VemoAPI.ResultResponse)VemoAPIEventTypeResource.handleAPI(etApiInfo);
        System.assertEquals(testEvntTypeMap.size(), eTypeResult.numberOfResults);

        Test.stopTest();
    }
}