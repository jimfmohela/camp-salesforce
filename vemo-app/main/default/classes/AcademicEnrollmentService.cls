public with sharing class AcademicEnrollmentService {

    public static List<AcademicEnrollment> getAcademicEnrollmentMapWithStudent(set<Id> studentIDSet){    
        Map<Id, AcademicEnrollment__c> academicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithStudent(studentIDSet);
        List<AcademicEnrollment> academicEnrollmentList = new List<AcademicEnrollment>();
        for(AcademicEnrollment__c ae : academicEnrollmentMap.values()){
            academicEnrollmentList.add(new AcademicEnrollment(ae));
        }
        return academicEnrollmentList;
    }
    
    public static List<AcademicEnrollment> getAcademicEnrollmentMapWithStudentNStatus(set<Id> studentIDSet, Set<String> statuses){    
        Map<Id, AcademicEnrollment__c> academicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithStudentNStatus(studentIDSet, statuses);
        List<AcademicEnrollment> academicEnrollmentList = new List<AcademicEnrollment>();
        for(AcademicEnrollment__c ae : academicEnrollmentMap.values()){
            academicEnrollmentList.add(new AcademicEnrollment(ae));
        }
        return academicEnrollmentList;
    }
    
    public static List<AcademicEnrollment> getAcademicEnrollmentMapWithProvider(set<Id> providerIDSet){    
        Map<Id, AcademicEnrollment__c> academicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithProvider(providerIDSet);
        List<AcademicEnrollment> academicEnrollmentList = new List<AcademicEnrollment>();
        for(AcademicEnrollment__c ae : academicEnrollmentMap.values()){
            academicEnrollmentList.add(new AcademicEnrollment(ae));
        }
        return academicEnrollmentList;
    }
    
    public static List<AcademicEnrollment> getAcademicEnrollmentMapWithProviderNStatus(set<Id> providerIDSet, Set<String> statuses){    
        Map<Id, AcademicEnrollment__c> academicEnrollmentMap = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithProviderNStatus(providerIDSet, statuses);
        List<AcademicEnrollment> academicEnrollmentList = new List<AcademicEnrollment>();
        for(AcademicEnrollment__c ae : academicEnrollmentMap.values()){
            academicEnrollmentList.add(new AcademicEnrollment(ae));
        }
        return academicEnrollmentList;
    }
    
    public static Set<ID> updateAcademicEnrollments(List<AcademicEnrollment> aeList){
        System.debug('AcademicEnrollmentService.updateAcademicEnrollment()');
        List<AcademicEnrollment__c> newAEs = new List<AcademicEnrollment__c>();
        for(AcademicEnrollment ae : aeList){
            AcademicEnrollment__c newAE = aeToAE(ae);
            newAEs.add(newAE);
        }
        DatabaseUtil dbUtil = new DatabaseUtil();
        dbUtil.updateRecords(newAEs);
        Set<ID> AEIDs = new Set<ID>();
        for(AcademicEnrollment__c ae : newAEs){
            AEIDs.add(ae.ID);
        }
        return AEIDs;
    }
    
    public static AcademicEnrollment__c aeToAE(AcademicEnrollment ae){
        AcademicEnrollment__c aeObj = new AcademicEnrollment__c();
        if(ae.academicEnrollmentID != null) aeObj.ID = ae.academicEnrollmentID; 
        if(ae.pendingStatus != null) aeObj.PendingStatus__c = ae.pendingStatus; 
        if(ae.pendingGrade != null) aeObj.PendingGrade__c = ae.pendingGrade;
        if(ae.pendingGraduationDate != null) aeObj.PendingGraduationDate__c = ae.pendingGraduationDate;
        if(ae.pendingLastDateofAttendance != null) aeObj.PendingLastDateOfAttendance__c = ae.pendingLastDateofAttendance; 
        if(ae.pendingSchoolProgramOfStudyID != null) aeObj.PendingSchoolProgramOfStudy__c = ae.pendingSchoolProgramOfStudyID;
        //if(ae.pendingUpdate != null) aeObj.PendingUpdate__c = ae.pendingUpdate;
        if(ae.DateOfLastStatusChange != null) aeObj.DateOfLastStatusChange__c = ae.DateOfLastStatusChange;
        if(ae.ChangeRequestedBy != null) aeObj.ChangeRequestedBy__c = ae.ChangeRequestedBy;
        if(ae.NotesComments != null) aeObj.NotesComments__c = ae.NotesComments;
        if(ae.PendingEffectiveDateOfChange != null) aeObj.PendingEffectiveDateOfChange__c = ae.PendingEffectiveDateOfChange;
        if(ae.PendingLeaveOfAbsenceEndDate != null) aeObj.PendingLeaveOfAbsenceEndDate__c = ae.PendingLeaveOfAbsenceEndDate;
        if(ae.PendingLeaveOfAbsenceStartDate != null) aeObj.PendingLeaveOfAbsenceStartDate__c = ae.PendingLeaveOfAbsenceStartDate;
        return aeObj;
    } 

    public class AcademicEnrollment{
        public String academicEnrollmentID{get;set;}
        public String grade{get;set;}
        public Date graduationDate{get;set;}
        public Date lastDateofAttendance {get;set;}
        public String providerID{get;set;}
        public String schoolProgramOfStudyID{get;set;}
        public String status{get;set;}
        public String studentID {get;set;}
        //public Boolean pendingUpdate {get;set;}
        public String pendingStatus {get;set;}
        public String pendingGrade {get;set;}
        public Date PendingLastDateOfAttendance {get;set;}
        public Date PendingGraduationDate {get;set;}
        public String PendingSchoolProgramOfStudyID {get;set;}
        public Date DateOfLastStatusChange {get;set;}
        public Boolean RecordLocked {get;set;}
        public String ChangeRequestedBy {get;set;}
        public String NotesComments {get;set;}
        public Date DateOfLastApprovedChange {get;set;}
        public Date PendingEffectiveDateOfChange {get;set;}
        public Date PendingLeaveOfAbsenceStartDate {get;set;}
        public Date PendingLeaveOfAbsenceEndDate {get;set;}
        public Date effectiveDateofChange {get;set;}
        public Date LeaveofAbsenceStartDate {get;set;}
        public Date LeaveofAbsenceEndDate {get;set;}

        public AcademicEnrollment(){}

        public AcademicEnrollment(AcademicEnrollment__c ae){
            this.academicEnrollmentID = ae.ID;
            this.grade = ae.Grade__c;
            this.graduationDate = ae.GraduationDate__c;
            this.providerID = ae.Provider__c;
            this.schoolProgramOfStudyID = ae.SchoolProgramOfStudy__c;
            this.status = ae.AcademicEnrollmentStatus__c;
            this.studentID = ae.Student__c;
            this.lastDateofAttendance = ae.LastDateofAttendance__c;
            //this.pendingUpdate = ae.PendingUpdate__c;
            this.pendingStatus = ae.PendingStatus__c;
            this.pendingGrade = ae.PendingGrade__c;
            this.pendingLastDateOfAttendance = ae.PendingLastDateOfAttendance__c;
            this.pendingGraduationDate = ae.pendingGraduationDate__c;
            this.DateOfLastStatusChange = ae.DateOfLastStatusChange__c;
            this.RecordLocked = ae.Record_Locked__c;
            this.ChangeRequestedBy = ae.ChangeRequestedBy__c;
            this.NotesComments = ae.NotesComments__c; 
            this.PendingSchoolProgramOfStudyID = ae.PendingSchoolProgramOfStudy__c;
            this.DateOfLastApprovedChange = ae.DateOfLastApprovedChange__c;
            this.PendingEffectiveDateOfChange = ae.PendingEffectiveDateOfChange__c;
            this.PendingLeaveOfAbsenceEndDate = ae.PendingLeaveOfAbsenceEndDate__c;
            this.PendingLeaveOfAbsenceStartDate = ae.PendingLeaveOfAbsenceStartDate__c;
            this.effectiveDateofChange = ae.EffectiveDateofChange__c;
            this.LeaveofAbsenceStartDate = ae.LeaveofAbsenceStartDate__c;
            this.LeaveofAbsenceEndDate = ae.LeaveofAbsenceEndDate__c;
        }
    }
}