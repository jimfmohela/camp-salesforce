/////////////////////////////////////////////////////////////////////////
// Class: AgreementService_TEST
// 
// Description: 
//  Test class for AgreementService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2016-12-18   Greg Cook       Created                          
/////////////////////////////////////////////////////////////////////////
@isTest
public class AgreementService_TEST {
    
     public static DatabaseUtil dbUtil = new DatabaseUtil();
     
    @TestSetup static void setupData(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        TestUtil.createStandardTestConditions();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(userWithRole){
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programMap = TestDataFactory.createAndInsertPrograms(TestUtil.TEST_THROTTLE, schools);//TestDataFactory.createAndInsertSchoolCustomerAccounts(1));
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        for(Account stud : studentMap.values()){
            stud.PrimarySchool__pc = schoolMap.values()[0].id;
        }
        Map<Id, StudentProgram__c> testStudPrgMap = TestDataFactory.createAndInsertStudentProgram(TestUtil.TEST_THROTTLE,
                                                                                                  studentMap,
                                                                                                  programMap);
        update studentMap.values(); 
        }
    }

    @isTest public static void validateGetAgreementWithAgreementID(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programMap = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> testStudPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        Test.startTest();
        List<AgreementService.Agreement> agreements = AgreementService.getAgreementWithAgreementID(testStudPrgMap.keySet());
        System.assertEquals(testStudPrgMap.keySet().size(), agreements.size());
        Test.stopTest();
    }

    @isTest public static void validateGetAgreementWithStudentID(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programMap = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> testStudPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        Set<ID> studentIDs = new Set<ID>();
        for(StudentProgram__c studPrg : testStudPrgMap.values()){
            studentIDs.add(studPrg.Student__c);
        }
        Test.startTest();
        List<AgreementService.Agreement> agreements = AgreementService.getAgreementWithStudentID(studentIDs);
        System.assertEquals(testStudPrgMap.keySet().size(), agreements.size());
        Test.stopTest();
    }

    @isTest public static void validateGetAgreementWithSchoolID(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programMap = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> testStudPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        Set<ID> schoolIDs = new Set<ID>();
        for(StudentProgram__c studPrg : testStudPrgMap.values()){
            schoolIDs.add(studPrg.Program__r.School__c);
        }
        Test.startTest();
        List<AgreementService.Agreement> agreements = AgreementService.getAgreementWithSchoolID(schoolIDs);
        System.assertEquals(testStudPrgMap.keySet().size(), agreements.size());
        Test.stopTest();
    }

    @isTest public static void validateGetAgreementWithProgramID(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, Account> schoolMap = AccountQueries.getSchoolMap();
        Map<Id, Program__c> programMap = ProgramQueries.getProgramMap();
        Map<Id, StudentProgram__c> testStudPrgMap = StudentProgramQueries.getStudentProgramMap();
        
        Set<ID> programIDs = new Set<ID>();
        for(StudentProgram__c studPrg : testStudPrgMap.values()){
            programIDs.add(studPrg.Program__c);
        }
        Test.startTest();
        List<AgreementService.Agreement> agreements = AgreementService.getAgreementWithProgramID(programIDs);
        System.assertEquals(testStudPrgMap.keySet().size(), agreements.size());
        Test.stopTest();
    }

    @isTest public static void validateCreateAgreement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        List<AgreementService.Agreement> agreements = new List<AgreementService.Agreement>();
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            AgreementService.Agreement agr = new AgreementService.Agreement(true);
            agr.studentID = students.values().get(i).ID;
            agr.programID = programs.values().get(i).ID;
            agreements.add(agr);
        }
        Test.startTest();
        Set<ID> agreementIDs = AgreementService.createAgreement(agreements);
        System.assertEquals(110, StudentProgramQueries.getStudentProgramMap().size());
        Test.stopTest();
    }

    @isTest public static void validateUpdateAgreement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        List<AgreementService.Agreement> agreements = new List<AgreementService.Agreement>();
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            AgreementService.Agreement agr = new AgreementService.Agreement(true);
            agr.studentID = students.values().get(i).ID;
            agr.programID = programs.values().get(i).ID;
            agreements.add(agr);
        }        
        Set<ID> agreementIDs = AgreementService.createAgreement(agreements);
        List<AgreementService.Agreement> agreementAfter = AgreementService.getAgreementWithAgreementID(agreementIDs);
        for(AgreementService.Agreement agr : agreementAfter){
            agr.agreementStatus = 'Application Complete';
        }
        Test.startTest();
        Set<ID> updatedIDs = AgreementService.updateAgreement(agreementAfter);
        Test.stopTest();
        Map<ID, StudentProgram__c> updatedStudProgs = StudentProgramQueries.getStudentProgramMap();
        for(StudentProgram__c studProg : updatedStudProgs.values()){
            System.assertEquals('Application Complete', studProg.Status__c);
        }
    }

    @isTest public static void validateDeleteAgreement(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        List<AgreementService.Agreement> agreements = new List<AgreementService.Agreement>();
        Map<ID, Account> students = AccountQueries.getStudentMap();
        Map<ID, Program__c> programs = ProgramQueries.getProgramMap();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE; i++){
            AgreementService.Agreement agr = new AgreementService.Agreement(true);
            agr.studentID = students.values().get(i).ID;
            agr.programID = programs.values().get(i).ID;
            agreements.add(agr);
        }        
        Set<ID> agreementIDs = AgreementService.createAgreement(agreements);
        System.assertEquals(110, StudentProgramQueries.getStudentProgramMap().size());

        Test.startTest();
        Integer deletedNum = AgreementService.deleteAgreement(agreementIDs);
        Test.stopTest();
        System.assertEquals(deletedNum, agreementIDs.size());
        System.assertEquals(100, StudentProgramQueries.getStudentProgramMap().size());
    }
    
    @isTest public static void generateFinalDisclosureTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, StudentProgram__c> testStudPrgMap = StudentProgramQueries.getStudentProgramMap();
        AgreementService.generateFinalDisclosure(testStudPrgMap.values()[0].Id, UserInfo.getSessionId());
    }
    
    @isTest public static void determineAmountsDueTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext(); 
        Map<Id, StudentProgram__c> studentPrgAllMap = StudentProgramQueries.getStudentProgramMap();
        Map<Id, StudentProgram__c> studentPrgMap = new Map<Id, StudentProgram__c>{
            studentPrgAllMap.values()[0].Id => studentPrgAllMap.values()[0]
        };
        List<AgreementService.Agreement> agreements = new List<AgreementService.Agreement>();
        for(StudentProgram__c studPrg : studentPrgMap.values()){
            studPrg.FundingAmountPostCertification__c = 1000;
            AgreementService.Agreement agr = new AgreementService.Agreement(studPrg);
            agreements.add(agr);
        }
        update studentPrgMap.values();
        Map<ID, StudentProgramMonthlyStatus__c> studProgMonthlyStatusMap = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(
                12, studentPrgMap
        );
        Map<ID, StudentProgramAmountDue__c> studProgAmountDueMap = TestDataFactory.createAndInsertStudentProgramAmountDue(
                studentPrgMap,
                studProgMonthlyStatusMap
        );
        AgreementService.determineAmountsDue(agreements);
    }
}