public with sharing class VemoAPIEmploymentHistory2Resource{
    
     public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        if((api.version == 'v2') && (api.method == 'POST')){
            return handlePostV2(api);
        }
        if((api.version == 'v2') && (api.method == 'PUT')){
            return handlePutV2(api);
        }   
        if((api.version == 'v2') && (api.method == 'DELETE')){
            return handleDeleteV2(api);
        }   
            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        //return null;
    }
    
     public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){  
        System.debug('VemoAPIEmployeeHistoryResource.handleGetV2');
        String employmentHistoryIDParam = api.params.get('employmentHistoryID');
        String employerIDParam = api.params.get('employerID');
        String employerParam = api.params.get('employer');
        String jobTitleParam = api.params.get('jobTitle');
        String studentIDParam = api.params.get('studentID');
        String groupByParam = api.params.get('groupBy');        
        String recentRecordParam = api.params.get('mostRecent');        
        String allVerifiedRecordParam = api.params.get('allVerified');
        String currentParam = api.params.get('current');
        String pastParam = api.params.get('past');
        String uploadedDocParam = api.params.get('uploadedDoc');
        
        List<EmploymentHistoryService2.EmploymentHistory> empHisList = new List<EmploymentHistoryService2.EmploymentHistory>();
        if(employmentHistoryIDParam != null){
            empHisList = EmploymentHistoryService2.getEmploymentHistoryWithEmployentHistoryID(VemoApi.parseParameterIntoIDSet(employmentHistoryIDParam));
        }
        else if(employerIDParam != null){
            empHisList = EmploymentHistoryService2.getEmploymentHistoryWithEmployerID(VemoApi.parseParameterIntoIDSet(employerIDParam));
        }
        else if(studentIDParam != null){
              //This variable will define whether we need to return consolidated JSON form or not
              Boolean needConsolidatedForm = false;
              if(currentParam != null && currentParam.toLowerCase() == 'true'){
                needConsolidatedForm = true;
              }
              if(pastParam != null && pastParam.toLowerCase() == 'true'){
                needConsolidatedForm = true;
              }
              List<EmploymentHistory2__c> EmploymentHistorySobjList = new List<EmploymentHistory2__c>();
              //verify accounts this student can access
              Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam)); //authorized records should be returned
              if(verifiedAccountMap.size()>0){
                  EmploymentHistorySobjList = EmploymentHistoryService2.getEmploymentHistoryWithStudentIDAndOtherParams(
                          VemoApi.parseParameterIntoIDSet(studentIDParam), recentRecordParam, 
                          allVerifiedRecordParam, employerParam, jobTitleParam, groupByParam, 
                          currentParam, pastParam, uploadedDocParam
                    );
              }  
              if(!needConsolidatedForm){
                    for(EmploymentHistory2__c empHis : EmploymentHistorySobjList){
                        empHisList.add(new EmploymentHistoryService2.EmploymentHistory(empHis));
                    }
              }
              else {
                //return consolidated form of Employment Histories
                //List<EmploymentHistoryService2.EmploymentHistoryConsolidatedForm> EmploymentHistoryConsolidatedFormList = new List<EmploymentHistoryService2.EmploymentHistoryConsolidatedForm>();
                //EmploymentHistoryConsolidatedFormList = EmploymentHistoryService2.getEmploymentHistoryConsolidatedForm(EmploymentHistorySobjList, currentParam);
                //return (new VemoAPI.ResultResponse(EmploymentHistoryConsolidatedFormList, EmploymentHistoryConsolidatedFormList.size()));
              }
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter employmentHistoryID or studentID');
        }
        List<EmploymentHistoryOutputV2> results = new List<EmploymentHistoryOutputV2>();
        
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistoryService2.EmploymentHistory eh: empHisList){
            AccountIDsToVerify.add((ID) eh.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        for(EmploymentHistoryService2.EmploymentHistory empHis : empHisList){
            if(VerifiedAccountMap.containsKey((ID) empHis.studentID)){
                results.add(new EmploymentHistoryOutputV2(empHis));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

       
    public static VemoAPI.ResultResponse handlePostV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentHistory2Resource.handlePostV2');
        List<EmploymentHistoryService2.EmploymentHistory> newEmploymentHistory = new List<EmploymentHistoryService2.EmploymentHistory>();
        List<EmploymentHistoryInputV2> employmentHistoryJSON = (List<EmploymentHistoryInputV2>)JSON.deserialize(api.body, List<EmploymentHistoryInputV2>.class);
        
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistoryInputV2 empHis : employmentHistoryJSON){
            AccountIDsToVerify.add((ID) empHis.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        
        for(EmploymentHistoryInputV2 empHis : employmentHistoryJSON){
            if(verifiedAccountMap.containsKey((ID)empHis.studentID)){
                empHis.validatePOSTFields();
                EmploymentHistoryService2.EmploymentHistory empHisServ = new EmploymentHistoryService2.EmploymentHistory();
                empHisServ = employmentHistoryInputV2ToEmploymentHistory(empHis);
                newEmploymentHistory.add(empHisServ);
            }
        }
        Set<ID> employmentHistoryIDs = EmploymentHistoryService2.createEmploymentHistory(newEmploymentHistory);
        return (new VemoAPI.ResultResponse(employmentHistoryIDs, employmentHistoryIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentHistory2Resource.handlePutV2');
        List<EmploymentHistoryService2.EmploymentHistory> newEmploymentHistory = new List<EmploymentHistoryService2.EmploymentHistory>();
        List<EmploymentHistoryInputV2> employmentHistoryJSON = (List<EmploymentHistoryInputV2>)JSON.deserialize(api.body, List<EmploymentHistoryInputV2>.class);
        
        Set<ID> IDsToVerify = new Set<ID>();
        for(EmploymentHistoryInputV2 empHis : employmentHistoryJSON){
            IDsToVerify.add((ID) empHis.employmentHistoryID);    
        }
        Map<ID, EmploymentHistory2__c> verifiedEmploymentHistoryMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithEmploymentHistoryId(IdsToVerify); //authorized records should be returned
        
        for(EmploymentHistoryInputV2 empHis : employmentHistoryJSON){
            if(verifiedEmploymentHistoryMap.containsKey((ID)empHis.employmentHistoryID)){
                empHis.validatePUTFields();
                EmploymentHistoryService2.EmploymentHistory empHisServ = new EmploymentHistoryService2.EmploymentHistory();
                empHisServ = employmentHistoryInputV2ToEmploymentHistory(empHis);
                newEmploymentHistory.add(empHisServ);
            }
        }
        Set<ID> employmentHistoryIDs = EmploymentHistoryService2.updateEmploymentHistory(newEmploymentHistory);
        return (new VemoAPI.ResultResponse(employmentHistoryIDs, employmentHistoryIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV2(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmploymentHistory2Resource.handleDeleteV2');
        String employmentHistoryIDParam = api.params.get('employmentHistoryID');
        Map<ID, EmploymentHistory2__c> employmentHistoryMap = EmploymentHistoryQueries2.getEmploymentHistoryMapWithEmploymentHistoryId(VemoApi.parseParameterIntoIDSet(employmentHistoryIDParam));
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistory2__c eh: employmentHistoryMap.values()){
            AccountIDsToVerify.add(eh.Student__c);
        }
        
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIDsToVerify); //authorized records should be returned
        Set<ID> employmentHistoriesToDelete = new Set<ID>();
        
        for(EmploymentHistory2__c eh: employmentHistoryMap.values()){
            if(verifiedAccountMap.containsKey(eh.Student__c)){
                employmentHistoriesToDelete.add(eh.id); 
            }
        }
        Integer numToDelete = 0;
        if(employmentHistoriesToDelete.size()>0){
            numToDelete = EmploymentHistoryService2.deleteEmploymentHistory(employmentHistoriesToDelete);
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }
    
    public class EmploymentHistoryOutputV2{
        
        public EmploymentHistoryOutputV2(EmploymentHistoryService2.EmploymentHistory empHis){
            this.employmentHistoryID = empHis.employmentHistoryID;          
            this.category = empHis.category;
            this.employer = empHis.employer;
            this.employmentEndDate = empHis.employmentEndDate;
            this.employmentStartDate = empHis.employmentStartDate;
            this.studentID = empHis.studentID;
            this.employerID = empHis.employerID;
            this.employerName = empHis.employerName;
            this.type = empHis.type;
            //this.verified = empHis.verified;
            
            //this.employmentSummaryID = empHis.employmentSummaryID;          
            this.jobTitle = empHis.jobTitle;
            this.hoursPerWeek = empHis.hoursPerWeek;
            this.hourlyRate = empHis.hourlyRate;
            //this.yearlySalary = empHis.yearlySalary;
            this.paymentSchedule = empHis.paymentSchedule;
            //this.bonusAmount = empHis.bonusAmount;
            this.additionalIncome = empHis.bonusAmount;
            //this.bonusFrequency = empHis.bonusFrequency;
            //this.commissionAmount = empHis.commissionAmount;
            //this.commissionFrequency = empHis.commissionFrequency;
            //this.noLongerEmployedHere = empHis.noLongerEmployedHere;
            //this.externalCompanyID  = empHis.externalCompanyID;
            this.eventType = empHis.eventType;
            //this.unemploymentEffectiveDate = empHis.unemploymentEffectiveDate;
            //this.lookForEmployment = empHis.lookForEmployment;
            this.dateReported = empHis.dateReported;
            this.status = empHis.status;
            //this.tipAmount = empHis.tipAmount;
            this.effectiveDate = empHis.effectiveDate;
            //this.uploadedDoc = empHis.uploadedDoc ;
            this.jobLocationCity = empHis.jobLocationCity;
            this.jobLocationState = empHis.jobLocationState;
            this.jobLocationCountry = empHis.jobLocationCountry;
            this.overtimeHourlyRate = empHis.overtimeHourlyRate;
            this.averageOvertimeHoursPerWeek = empHis.averageOvertimeHoursPerWeek;
            this.paidForHolidays = empHis.paidForHolidays;
            this.monthlyIncomeType = empHis.monthlyIncomeType; 
            this.monthlyIncome = empHis.monthlyIncome; 
            this.monthlyIncomeTotal = empHis.monthlyIncomeTotal;
            this.additionalIncomeType = empHis.additionalIncomeType;
            this.abnormalIncomeExplanation = empHis.abnormalIncomeExplanation;
            this.monthlyIncomeAdjustments = empHis.monthlyIncomeAdjustments;
            this.createdDate = empHis.createdDate;
            this.lastModifiedDate = empHis.lastModifiedDate;
        }
        public String employmentHistoryID {get;set;}
        public String category {get;set;}
        public String employer {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String studentID {get;set;}
        public String employerID {get;set;}
        public String employerName {get;set;}
        public String type {get;set;}
        //public Boolean verified {get;set;}
        
        //public String employmentSummaryID {get;set;}
        public String jobTitle {get;set;}
        public Decimal hoursPerWeek {get;set;}
        public Decimal hourlyRate {get;set;}
        //public Decimal yearlySalary {get;set;}
        public String paymentSchedule {get;set;}        
        //public Decimal bonusAmount {get;set;}
        public Decimal additionalIncome {get;set;}
        //public String bonusFrequency {get;set;}
        //public Decimal commissionAmount {get;set;}
        //public String commissionFrequency {get;set;}
        //public Boolean noLongerEmployedHere {get;set;}
        //public String externalCompanyID {get;set;}
        public String eventType {get;set;}
        //public Date unemploymentEffectiveDate {get;set;}
        //public Boolean lookForEmployment {get;set;}
        public Date dateReported {get;set;}
        public String status {get;set;}
        //public Decimal tipAmount {get;set;}
        public Date effectiveDate {get;set;}
        //public Boolean uploadedDoc {get;set;}
        public String jobLocationCity {get;set;}
        public String jobLocationState {get;set;}
        public String jobLocationCountry {get;set;}
        public Decimal averageOvertimeHoursPerWeek {get;set;}
        public Decimal overtimeHourlyRate {get;set;}
        public Boolean paidForHolidays {get;set;}
        public String monthlyIncomeType {get;set;}
        public Decimal monthlyIncome {get;set;}
        public Decimal monthlyIncomeTotal {get;set;}
        public String additionalIncomeType {get;set;}
        public String abnormalIncomeExplanation {get;set;}
        public Decimal monthlyIncomeAdjustments {get;set;}
        public DateTime createdDate {get;set;}
        public DateTime lastModifiedDate {get;set;}
    }

    public class EmploymentHistoryInputV2{
        public String employmentHistoryID {get;set;}
        public String category {get;set;}
        public String employer {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String studentID {get;set;}
        public String employerID {get;set;}
        public String type {get;set;}
        //public Boolean verified {get;set;}
        
        //public String employmentSummaryID {get;set;}
        public String jobTitle {get;set;}
        public Decimal hoursPerWeek {get;set;}
        public Decimal hourlyRate {get;set;}
        //public Decimal yearlySalary {get;set;}
        public String paymentSchedule {get;set;}        
        //public Decimal bonusAmount {get;set;}
        public Decimal additionalIncome {get;set;}
        //public String bonusFrequency {get;set;}
        //public Decimal commissionAmount {get;set;}
        //public String commissionFrequency {get;set;}
        //public Boolean noLongerEmployedHere {get;set;}
        //public String externalCompanyID {get;set;}
        public String eventType {get;set;}
        //public Date unemploymentEffectiveDate {get;set;}
        //public Boolean lookForEmployment {get;set;}
        public Date dateReported {get;set;}
        public String status {get;set;}
        //public Decimal tipAmount {get;set;}
        public Date effectiveDate {get;set;}
        //public Boolean uploadedDoc {get;set;}
        public String jobLocationCity {get;set;}
        public String jobLocationState {get;set;}
        public String jobLocationCountry {get;set;}
        public Decimal averageOvertimeHoursPerWeek {get;set;}
        public Decimal overtimeHourlyRate {get;set;}
        public Boolean paidForHolidays {get;set;}
        public String monthlyIncomeType {get;set;}
        public Decimal monthlyIncome {get;set;}
        public String additionalIncomeType {get;set;}
        public String abnormalIncomeExplanation {get;set;}
        public Decimal monthlyIncomeAdjustments {get;set;}

        public EmploymentHistoryInputV2(Boolean testValues){
            if(testValues){
                this.category = 'Employee'; //Contractor, Internship
                this.employer = 'Test Employer'; //Planned, Closed, Cancelled
                this.type = 'Full Time'; //Part Time
            }
        }

        public void validatePOSTFields(){
            if(employmentHistoryID != null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID cannot be created in POST');
            if(employerID == null) throw new VemoAPI.VemoAPIFaultException('employerID is a required input parameter on POST');
        }
        public void validatePUTFields(){
            if(employmentHistoryID == null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID is a required input parameter on PUT');
            if(employerID != null) throw new VemoAPI.VemoAPIFaultException('employerID is not a writable input parameter on PUT');
        }
    }

    public static EmploymentHistoryService2.EmploymentHistory employmentHistoryInputV2ToEmploymentHistory(EmploymentHistoryInputV2 input){
        EmploymentHistoryService2.EmploymentHistory output = new EmploymentHistoryService2.EmploymentHistory();
        output.employmentHistoryID = input.employmentHistoryID;         
        output.category = input.category;
        output.employer = input.employer;
        output.employmentEndDate = input.employmentEndDate;
        output.employmentStartDate = input.employmentStartDate;
        output.studentID = input.studentID;
        output.employerID = input.employerID;
        output.type = input.type;
        //output.verified = input.verified;
        
        //output.employmentSummaryID = input.employmentSummaryID;         
        output.jobTitle = input.jobTitle;
        output.hoursPerWeek = input.hoursPerWeek;
        output.hourlyRate = input.hourlyRate;
        //output.yearlySalary = input.yearlySalary;
        output.paymentSchedule = input.paymentSchedule;
        //output.bonusAmount = input.bonusAmount;
        output.bonusAmount = input.additionalIncome;
        //output.bonusFrequency = input.bonusFrequency;
        //output.commissionAmount = input.commissionAmount;
        //output.commissionFrequency = input.commissionFrequency;
        //output.noLongerEmployedHere = input.noLongerEmployedHere;
        //output.externalCompanyID = input.externalCompanyID;
        output.eventType = input.eventType;
        //output.unemploymentEffectiveDate = input.unemploymentEffectiveDate;
        //output.lookForEmployment = input.lookForEmployment;
        output.dateReported = input.dateReported;
        output.status = input.status;
        //output.tipAmount = input.tipAmount;
        output.effectiveDate = input.effectiveDate;
        //output.uploadedDoc = input.uploadedDoc;
        output.jobLocationCity = input.jobLocationCity;
        output.jobLocationState = input.jobLocationState;
        output.jobLocationCountry = input.jobLocationCountry;
        output.averageOvertimeHoursPerWeek = input.averageOvertimeHoursPerWeek;
        output.overtimeHourlyRate = input.overtimeHourlyRate;  
        output.paidForHolidays = input.paidForHolidays;
        output.monthlyIncomeType = input.monthlyIncomeType;
        output.monthlyIncome = input.monthlyIncome;
        output.additionalIncomeType = input.additionalIncomeType;
        output.abnormalIncomeExplanation = input.abnormalIncomeExplanation;
        output.monthlyIncomeAdjustments = input.monthlyIncomeAdjustments;
        return output;
    }
}