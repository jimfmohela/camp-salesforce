@isTest
public class VemoAPIProgramCountResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }
    static testMethod void testHandleGetV2(){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);        
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> studProgramMap = TestDataFactory.createAndInsertStudentProgram(TestUtil.TEST_THROTTLE, students, programs);
        
        Map<String, String> schoolParams = new Map<String, String>();
        schoolParams.put('schoolID', TestUtil.createStringFromIDSet(schools.keySet()));
        schoolParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo schoolPCountApiInfo = TestUtil.initializeAPI('v2', 'GET', schoolParams, null);

        Map<String, String> programParams = new Map<String, String>();
        programParams.put('programID', TestUtil.createStringFromIDSet(programs.keyset()));
        programParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo programCountAPIInfo = TestUtil.initializeAPI('v2', 'GET', programParams, null);

        Test.startTest();
        VemoAPI.ResultResponse schoolPCountResult = (VemoAPI.ResultResponse)VemoAPIProgramCountResource.handleAPI(schoolPCountApiInfo);
        
        VemoAPI.ResultResponse programCountResult = (VemoAPI.ResultResponse)VemoAPIProgramCountResource.handleAPI(programCountAPIInfo);
        Test.stopTest();
    }
}