@IsTest global with sharing class SchoolAdminSiteLoginController_TEST {
    @IsTest(SeeAllData=true) global static void testSiteLoginController () {
        // Instantiate a new controller with all parameters in the page
        SchoolAdminSiteLoginController controller = new SchoolAdminSiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }    
}