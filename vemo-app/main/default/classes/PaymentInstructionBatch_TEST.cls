/*
* Description - This is test class for Batch "PaymentInstructionBatch".
*               Currently it is only covering scheduleInboundSch case because others can't be called.
*               Need to write other testMethods once the PaymentInstructionBatch will be updated.
*/
@isTest
public with sharing class PaymentInstructionBatch_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @isTest
    static void scheduleInboundSchTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, PaymentMethod__c> paymentsMap = TestDataFactory.createAndInsertPaymentMethod(1, studentMap);
        Map<ID, PaymentInstruction__c> pymntInstrnMap = TestDataFactory.createAndInsertPaymentInstruction(1, studentMap, paymentsMap);
        for(Account student : studentMap.values()){
            student.StewardshipPaymentDonorGUID__c = 'Test-SPD-Guid';
        }
        
        //update studentMap.values();
        dbUtil.updateRecords(studentMap.values());
        
        for(PaymentMethod__c pymntMethod : paymentsMap.values()){
            pymntMethod.StewardshipPaymentAccountGUID__c = 'Test-SPA-Guid';
        }
        //update paymentsMap.values();
        dbUtil.updateRecords(paymentsMap.values());
        
        for(PaymentInstruction__c pymntInstrn : pymntInstrnMap.values()){
            pymntInstrn.Status__c = 'Automatically Created';
            pymntInstrn.TransactionDirection__c = 'Inbound';
            pymntInstrn.Description__c = 'This is test payment Instruction.';
            pymntInstrn.Date__c = Date.today();
        }
        //update pymntInstrnMap.values();
        dbUtil.updateRecords(pymntInstrnMap.values());

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PaymentInstructionBatch_TEST.StewardshipTechServiceCallouts());
        PaymentInstructionBatch BatchJob = new PaymentInstructionBatch();
        BatchJob.job = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
        Database.executeBatch(BatchJob);
        Test.stopTest();
    }
    @isTest
    static void scheduleInboundSch_ExceptionTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, PaymentMethod__c> paymentsMap = TestDataFactory.createAndInsertPaymentMethod(1, studentMap);
        Map<ID, PaymentInstruction__c> pymntInstrnMap = TestDataFactory.createAndInsertPaymentInstruction(1, studentMap, paymentsMap);
        for(Account student : studentMap.values()){
            student.StewardshipPaymentDonorGUID__c = 'Test-SPD-Guid';
        }
        //update studentMap.values();
        dbUtil.updateRecords(studentMap.values());
        
        for(PaymentMethod__c pymntMethod : paymentsMap.values()){
            pymntMethod.StewardshipPaymentAccountGUID__c = 'Test-SPA-Guid';
        }
        //update paymentsMap.values();
        dbUtil.updateRecords(paymentsMap.values());
        
        for(PaymentInstruction__c pymntInstrn : pymntInstrnMap.values()){
            pymntInstrn.Status__c = 'Automatically Created';
            pymntInstrn.TransactionDirection__c = 'Inbound';
            pymntInstrn.Date__c = Date.today();
        }
        //update pymntInstrnMap.values();
        dbUtil.updateRecords(paymentsMap.values());

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PaymentInstructionBatch_TEST.StewardshipTechServiceCallouts());
        PaymentInstructionBatch BatchJob = new PaymentInstructionBatch();
        BatchJob.job = PaymentInstructionBatch.JobType.SCHEDULE_INBOUND_ACH;
        Database.executeBatch(BatchJob);
        Test.stopTest();
    }
    
    @isTest
    static void scheduleOutboundSchTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, PaymentMethod__c> paymentsMap = TestDataFactory.createAndInsertPaymentMethod(1, studentMap);
        Map<ID, PaymentInstruction__c> pymntInstrnMap = TestDataFactory.createAndInsertPaymentInstruction(1, studentMap, paymentsMap);
        for(Account student : studentMap.values()){
            student.StewardshipPaymentDonorGUID__c = 'Test-SPD-Guid';
        }
        
        //update studentMap.values();
        dbUtil.updateRecords(studentMap.values());
        
        for(PaymentMethod__c pymntMethod : paymentsMap.values()){
            pymntMethod.StewardshipPaymentAccountGUID__c = 'Test-SPA-Guid';
        }
        //update paymentsMap.values();
        dbUtil.updateRecords(paymentsMap.values());
        
        for(PaymentInstruction__c pymntInstrn : pymntInstrnMap.values()){
            pymntInstrn.Status__c = 'None';
            pymntInstrn.TransactionDirection__c = 'Outbound';
            pymntInstrn.Description__c = 'This is test payment Instruction.';
            pymntInstrn.Date__c = Date.today();
        }
        //update pymntInstrnMap.values();
        dbUtil.updateRecords(pymntInstrnMap.values());

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PaymentInstructionBatch_TEST.StewardshipTechServiceCallouts());
        PaymentInstructionBatch BatchJob = new PaymentInstructionBatch();
        BatchJob.job = PaymentInstructionBatch.JobType.SCHEDULE_OUTBOUND_ACH;
        Database.executeBatch(BatchJob);
        Test.stopTest();
    }
    
    @isTest
    static void scheduleAllocatePaymentTest(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(1);
        Map<Id, PaymentMethod__c> paymentsMap = TestDataFactory.createAndInsertPaymentMethod(1, studentMap);
        Map<ID, PaymentInstruction__c> pymntInstrnMap = TestDataFactory.createAndInsertPaymentInstruction(1, studentMap, paymentsMap);
        for(Account student : studentMap.values()){
            student.StewardshipPaymentDonorGUID__c = 'Test-SPD-Guid';
        }
        
        //update studentMap.values();
        dbUtil.updateRecords(studentMap.values());
        
        for(PaymentMethod__c pymntMethod : paymentsMap.values()){
            pymntMethod.StewardshipPaymentAccountGUID__c = 'Test-SPA-Guid';
        }
        //update paymentsMap.values();
        dbUtil.updateRecords(paymentsMap.values());
        
        for(PaymentInstruction__c pymntInstrn : pymntInstrnMap.values()){
            pymntInstrn.Status__c = 'Cleared';
            pymntInstrn.Amount__c = 100.25;
        }
        //update pymntInstrnMap.values();
        dbUtil.updateRecords(pymntInstrnMap.values());

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PaymentInstructionBatch_TEST.StewardshipTechServiceCallouts());
        PaymentInstructionBatch BatchJob = new PaymentInstructionBatch();
        BatchJob.job = PaymentInstructionBatch.JobType.ALLOCATE_PAYMENTS;
        Database.executeBatch(BatchJob);
        Test.stopTest();
    }

    public class StewardshipTechServiceCallouts implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse mockResponse = new HTTPResponse();
            if(req.getEndpoint().toLowerCase().contains('CreateTransactionResponse'.toLowerCase())){
                mockResponse.setBody('{"Root":{"Status":{"ErrorCode":"0","Description":"This is Test-description"}, "Transaction":{"AccountScheduleGUID":"Test-SGUID"}}}');
            }
            else {
                mockResponse.setBody('{"Root":{"Status":{"ErrorCode":"0","Description":"This is Test-description"},"BatchGUID":"123"}}');
            }
            mockResponse.setStatusCode(200);
            mockResponse.setStatus('success');
            return mockResponse;
        }
    }
}