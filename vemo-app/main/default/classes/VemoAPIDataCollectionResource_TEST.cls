@isTest
public class VemoAPIDataCollectionResource_TEST{
  @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
   }
    
    static testMethod void validateHandleGetV2(){
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);  
        
        Map<String, String> dcParams = new Map<String, String>();
        dcParams.put('dataCollectionID', (String)dataCollectionMap.values().get(0).Id);
        
        VemoAPI.APIInfo dcApiInfo = TestUtil.initializeAPI('v2', 'GET', dcParams, null);
        
        Map<String, String> agreementParams = new Map<String, String>();
        agreementParams.put('agreementID', (String)agreementMap.values().get(0).Id);
        
        VemoAPI.APIInfo agreementApiInfo = TestUtil.initializeAPI('v2', 'GET', agreementParams, null);
        
        Test.startTest();
        MockedQueryExecutor.setRecordLimit(1);
        MockedQueryExecutor.setResetRecordLimit(false);        
        VemoAPI.ResultResponse dcResult = (VemoAPI.ResultResponse)VemoAPIDataCollectionResource.handleAPI(dcApiInfo);
        System.assertEquals(1, dcResult.numberOfResults);
        
        MockedQueryExecutor.setRecordLimit(1);        
        VemoAPI.ResultResponse agreementResult = (VemoAPI.ResultResponse)VemoAPIDataCollectionResource.handleAPI(agreementApiInfo);
        System.assertEquals(1, agreementResult.numberOfResults);
        Test.stopTest();    
        
    }
    
     static testMethod void validateHandlePostV2(){
        Map<String, String> params = new Map<String, String>();
        Map<ID, Account> schoolMap = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<Id, Account> StudentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Program__c> programMap = TestDataFactory.createAndInsertPrograms(1, schoolMap);
        Map<Id, DataCollectionTemplate__c> TemplateMap = TestDataFactory.createAndInsertDataCollectionTemplate(1, programMap);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(2, StudentMap , programMap);
        //Map<ID, DataCollection__c> dataCollectionMap = TestDataFactory.createAndInsertDataCollection(TemplateMap, agreementMap);  
        
        List<VemoAPIDataCollectionResource.DataCollectionResourceInputV2> resultDataCollectionList = new List<VemoAPIDataCollectionResource.DataCollectionResourceInputV2>(); 
        
        for(Integer i = 0; i<TemplateMap.size(); i++){
            for(Integer j = 0; j<agreementMap.size(); j++){
                VemoAPIDataCollectionResource.DataCollectionResourceInputV2 dc = new VemoAPIDataCollectionResource.DataCollectionResourceInputV2();
                dc.templateId = TemplateMap.values().get(i).id;
                dc.agreementId = agreementMap.values().get(j).id; 
                dc.DataType = 'String';
                dc.StringValue = 'test';
                resultDataCollectionList.add(dc);
            }
        }
        
        String body = JSON.serialize(resultDataCollectionList);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v2', 'POST', params, body);
        
        Test.startTest();
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIDataCollectionResource.handleAPI(apiInfo);
        System.assertEquals(resultDataCollectionList.size(), result.numberOfResults);
        Test.stopTest();   
     }
    
    
}