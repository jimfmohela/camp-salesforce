@isTest
public class VemoAPIPlaidBankResource_TEST {
    private static DatabaseUtil dbUtil = new DatabaseUtil();

    static void setupData(){
        TestUtil.createStandardTestConditions();
        Map<ID, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(TestUtil.TEST_THROTTLE);
        Map<ID, Account> studentMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
    }
    
    static testMethod void testHandleGetV1(){
        setupData();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, PlaidBank__c> plaidBankMap = TestDataFactory.createAndInsertPlaidBank(studentMap, 1); 
        
        Map<String, String> pbParams = new Map<String, String>();
        pbParams.put('plaidBankID', (String)plaidBankMap.values().get(0).Id);
        pbParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo pbApiInfo = TestUtil.initializeAPI('v1', 'GET', pbParams, null);
        
        Map<String, String> studParams = new Map<String, String>();
        studParams.put('studentID', (String)plaidBankMap.values().get(0).Student__c);
        studParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo studApiInfo = TestUtil.initializeAPI('v1', 'GET', studParams, null);
        
        
        Test.startTest();
        MockedQueryExecutor.setRecordLimit(1);
        VemoAPI.ResultResponse pbResult = (VemoAPI.ResultResponse)VemoAPIPlaidBankResource.handleAPI(pbApiInfo);
        System.assertEquals(1, pbResult.numberOfResults);

        MockedQueryExecutor.setRecordLimit(1);
        VemoAPI.ResultResponse studResult = (VemoAPI.ResultResponse)VemoAPIPlaidBankResource.handleAPI(studApiInfo);
        System.assertEquals(1, studResult.numberOfResults);
        Test.stopTest();  
    }
    
     static testMethod void testHandlePutV1(){
        setupData();
        Map<ID, Account> schools = AccountQueries.getSchoolMap();
        Map<ID, Account> studentMap = AccountQueries.getStudentMap();
        Map<ID, PlaidBank__c> plaidBankMap = TestDataFactory.createAndInsertPlaidBank(studentMap, 1); 
        Map<String, String> params = new Map<String, String>();
        params.put('VEMO_AUTH', 'ABC');

        List<VemoAPIPlaidBankResource.PlaidBankResourceInputV1> pbResMap = new List<VemoAPIPlaidBankResource.PlaidBankResourceInputV1>();
        for(PlaidBank__c pb : plaidBankMap.values()){
            VemoAPIPlaidBankResource.PlaidBankResourceInputV1 pbRes = new VemoAPIPlaidBankResource.PlaidBankResourceInputV1();
            pbRes.plaidBankID = pb.ID;
            pbRes.plaidStatus = 'Linked';
            pbResMap.add(pbRes);
        }
        String body = JSON.serialize(pbResMap);
        VemoAPI.APIInfo apiInfo = TestUtil.initializeAPI('v1', 'PUT', params, body);
        VemoAPI.ResultResponse result = (VemoAPI.ResultResponse)VemoAPIPlaidBankResource.handleAPI(apiInfo);
     }
    
}