public class VemoAPISearchResource{

    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v2') && (api.method == 'GET')){
            return handleGetV2(api);
        }
        //if((api.version == 'v2') && (api.method == 'POST')){
            //Add logic for POST
        //}
        //if((api.version == 'v2') && (api.method == 'PUT')){
            //Add logic for PUT
        //}  
        //if((api.version == 'v2') && (api.method == 'DELETE')){
            //Add logic for DELETE
        //}      
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        
    }
    
    public static VemoAPI.ResultResponse handleGetV2(VemoAPI.APIInfo api){ 
         List<SearchResourceOutputV2> results = new List<SearchResourceOutputV2>();
         SearchResourceOutputV2 result = new SearchResourceOutputV2();
         List<List<Sobject>> searchResult = new List<List<Sobject>>();
         
         String searchParam = api.params.get('search');
         if(searchParam != null){
             searchResult = searchObject(searchParam);  
         }
         
         //*********** Map school obj**********//
         List<Account> schools = new List<Account>(((List<Account>)searchResult[0]));
         Set<Id> schoolIDs = (new Map<Id,Account>(schools)).keySet();
         List<SchoolService.School> schoolList = SchoolService.getSchoolsWithSchoolID(schoolIDs);
         
         List<VemoAPISchoolResource.SchoolResourceOutputV2> schoolResourceList = new List<VemoAPISchoolResource.SchoolResourceOutputV2>();
         for(SchoolService.School school: schoolList){
             if(GlobalSettings.getSettings().vemoDomainAPI){
                 schoolResourceList.add(new VemoAPISchoolResource.VemoSchoolResourceOutputV2(school));
             }else{
                 schoolResourceList.add(new VemoAPISchoolResource.PublicSchoolResourceOutputV2(school));
             }
         }
         school schoolObj = new school(schoolResourceList, schoolResourceList.size());
         result.schools = schoolObj; 
         
         //********** Map student obj**********//
         //List<Account> students = new List<Account>(((List<Account>)searchResult[0]));
         //Set<ID> studentIDs = (new Map<Id,Account>(students)).keySet();
         //List<StudentService.Student> studentList = StudentService.getStudentsWithStudentID(studentIDs);
         
         //List<VemoAPIStudentResource.StudentResourceOutputV2> studentResourceList = new List<VemoAPIStudentResource.StudentResourceOutputV2>();
         //for(StudentService.Student student: studentList){
         //    if(GlobalSettings.getSettings().vemoDomainAPI){
         //        studentResourceList.add(new VemoAPIStudentResource.VemoStudentResourceOutputV2(student));
         //    }else{
         //        studentResourceList.add(new VemoAPIStudentResource.PublicStudentResourceOutputV2(student));
         //    }
             
         //}
         //student studentObj = new student(studentResourceList , studentResourceList.size());
         //result.students = studentObj; 
         
         
         
         //********* Map AcademicEnrollment Obj *********//
         List<Account> students = new List<Account>(((List<Account>)searchResult[0]));
         Set<ID> studentIDs = (new Map<Id,Account>(students)).keySet();
         //List<AcademicEnrollmentService.AcademicEnrollment> academicEnrollmentList = AcademicEnrollmentService.getAcademicEnrollmentMapWithStudent(studentIDs);
         List<AcademicEnrollmentService.AcademicEnrollment> academicEnrollmentList = new List<AcademicEnrollmentService.AcademicEnrollment>();
         if(studentIDs!= null && studentIDs.size()>0)
             academicEnrollmentList = AcademicEnrollmentService.getAcademicEnrollmentMapWithStudent(studentIDs);
             
         Set<Id> providerIDs = new Set<Id>();
         Set<Id> schoolProgramOfStudyIDs = new Set<Id>();
         Set<Id> academicEnrollmentIDs = new Set<Id>();
        
         for(AcademicEnrollmentService.AcademicEnrollment es : academicEnrollmentList ){
             //studentIDs.add(es.studentID);    
             if(es.providerID != null) providerIDs.add(es.providerID);
             if(es.schoolProgramOfStudyID != null) schoolProgramOfStudyIDs.add(es.schoolProgramOfStudyID);
             academicEnrollmentIDs.add(es.AcademicEnrollmentID);
         }
        
         Map<Id, StudentService.Student> studentMapWithStudentId = new Map<Id, StudentService.Student>(); 
         List<StudentService.Student> studentList = StudentService.getStudentsWithStudentID(studentIds);
         for(StudentService.Student student : studentList){
             studentMapWithStudentId.put(student.personAccountID, student);
         }
        
         Map<Id, ProgramOfStudyService.ProgramOfStudy> ProgramOfStudyWithPOSIDMap = new Map<Id, ProgramOfStudyService.ProgramOfStudy>(); 
         Map<Id, SchoolProgramsOfStudy__c> sPOSMap = SchoolProgramsOfStudyQueries.getSchoolProgramOfStudyMapWithSPOSID(schoolProgramOfStudyIDs);
         for(SchoolProgramsOfStudy__c sp : sPOSMap.values()){
             ProgramOfStudyWithPOSIDMap.put(sp.id , new ProgramOfStudyService.ProgramOfStudy(sp));
         }
         Map<Id, List<VemoAPIAgreementResource.AgreementResourceOutputV2>> AEIdNAgreementsMap = new Map<Id, List<VemoAPIAgreementResource.AgreementResourceOutputV2>>();
         //Map<ID, StudentProgram__c> spMap = StudentProgramQueries.getStudentProgramMapForAErecords(studentids, providerIds, academicEnrollmentIDs, schoolProgramOfStudyIDs);
         Map<ID, StudentProgram__c> spMap = StudentProgramQueries.getStudentProgramMapForAErecords(studentids, providerIds, academicEnrollmentIDs);
         for(StudentProgram__c sp: spMap.values()){
             if(sp.AcademicEnrollment__c != null){
                 if(AEIdNAgreementsMap.get(sp.AcademicEnrollment__c) != null && AEIdNAgreementsMap.get(sp.AcademicEnrollment__c).size()>0){
                     List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreementList = AEIdNAgreementsMap.get(sp.AcademicEnrollment__c);
                     agreementList.add(new VemoAPIAgreementResource.AgreementResourceOutputV2(new AgreementService.Agreement(sp)));
                     AEIdNAgreementsMap.put(sp.AcademicEnrollment__c,agreementList);
                 }
                 else{
                     List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreementList = new List<VemoAPIAgreementResource.AgreementResourceOutputV2>();
                     agreementList.add(new VemoAPIAgreementResource.AgreementResourceOutputV2(new AgreementService.Agreement(sp)));
                     AEIdNAgreementsMap.put(sp.AcademicEnrollment__c,agreementList);
                 }
             }
         }
         
         List<VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2> academicEnrollmentResourceList = new List<VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2>();
             
         
        // for(AcademicEnrollmentService.AcademicEnrollment acd: academicEnrollmentList){
        //     if(GlobalSettings.getSettings().vemoDomainAPI){
        //         //academicEnrollmentResourceList.add(new VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2(acd));
        //     }else{
        //         //academicEnrollmentResourceList.add(new VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2(acd));
        //     }
        // }
         
         for(AcademicEnrollmentService.AcademicEnrollment acd: academicEnrollmentList){
            VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2 acdObj = new VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2();
            StudentService.Student stud = new StudentService.Student();
            ProgramOfStudyService.ProgramOfStudy pos = new ProgramOfStudyService.ProgramOfStudy();
            
            if(studentMapWithStudentId.get(acd.StudentID) != null)
                stud = studentMapWithStudentId.get(acd.StudentID);
            
            if(ProgramOfStudyWithPOSIDMap.get(acd.schoolProgramOfStudyID) != null)
                pos = ProgramOfStudyWithPOSIDMap.get(acd.schoolProgramOfStudyID);
            
            if(GlobalSettings.getSettings().vemoDomainAPI){
                acdObj = new VemoAPIAcademicEnrollmentResource.VemoAcademicEnrollmentResourceOutputV2(acd, stud, pos);
            }else{
                acdObj = new VemoAPIAcademicEnrollmentResource.PublicAcademicEnrollmentResourceOutputV2(acd, stud, pos);
            }
            if(AEIdNAgreementsMap.get(acdObj.AcademicEnrollmentID) != null)
                acdObj.agreements.addAll(AEIdNAgreementsMap.get(acdObj.AcademicEnrollmentID));
            academicEnrollmentResourceList.add(acdObj);
         }
         academicEnrollment academicEnrollmentObj = new academicEnrollment(academicEnrollmentResourceList, academicEnrollmentResourceList.size());
         result.students = academicEnrollmentObj; 
         
         
         //******* Map program obj**********//
         List<Program__c> programs = new List<Program__c>(((List<Program__c>)searchResult[1]));
         Set<Id> programIDs = (new Map<Id,Program__c>(programs)).keySet();
         List<ProgramService.Program> ProgramList = ProgramService.getProgramsWithProgramID(programIDs);
         
         List<VemoAPIProgramResource.ProgramResourceOutputV2> programResourceList = new List<VemoAPIProgramResource.ProgramResourceOutputV2>();
         for(ProgramService.Program program: programList){
             //programResourceList.add(new VemoAPIProgramResource.ProgramResourceOutputV1(program));
             if(GlobalSettings.getSettings().vemoDomainAPI){
                 programResourceList.add(new VemoAPIProgramResource.VemoProgramResourceOutputV2(program));
             }else{
                 programResourceList.add(new VemoAPIProgramResource.PublicProgramResourceOutputV2(program));
             }
             
         }
         program programObj = new program(programResourceList, programResourceList.size()); 
         result.programs = programObj; 
         
         
         //********* Map agreement obj**********/
         List<StudentProgram__c> agreements = new List<StudentProgram__c>(((List<StudentProgram__c>)searchResult[2]));
         Set<Id> agreementIDs = (new Map<Id,StudentProgram__c>(agreements)).keySet();
         List<AgreementService.agreement> agreementList = AgreementService.getAgreementWithAgreementID(agreementIDs);
         
         List<VemoAPIAgreementResource.AgreementResourceOutputV2> agreementResourceList = new List<VemoAPIAgreementResource.AgreementResourceOutputV2>();
         for(agreementService.Agreement agreement: agreementList){
             //agreementResourceList.add(new VemoAPIAgreementResource.AgreementResourceOutputV1(agreement));
             if(GlobalSettings.getSettings().vemoDomainAPI){
                 agreementResourceList.add(new VemoAPIAgreementResource.VemoAgreementResourceOutputV2(agreement));
             }else{
                 agreementResourceList.add(new VemoAPIAgreementResource.PublicAgreementResourceOutputV2(agreement));
             }
             
         }
         agreement agreementObj = new agreement(agreementResourceList, agreementResourceList.size());
         result.agreements = agreementObj; 
         
         results.add(result);
         
         return (new VemoAPI.ResultResponse(results, results.size()));
    } 
    
    public static List<List<Sobject>> searchObject(String searchString){
        string query = '';
        query =  'FIND \''+searchString +'\' IN ALL FIELDS RETURNING Account, Program__c, StudentProgram__c';
        List<List<Sobject>> objectList = search.query(query);
        return objectList;
    }
    
        
    public class SearchResourceOutputV2{
        public academicEnrollment students {get;set;} //named to students for front end
        public school schools {get;set;}
        //public student students {get;set;}
        public program programs {get;set;}
        public agreement agreements {get;set;}
        
    }
    
    public class academicEnrollment{
        public list<VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2> records {get;set;}
        public integer numberOfResults {get;set;}
        
        public academicEnrollment(List<VemoAPIAcademicEnrollmentResource.AcademicEnrollmentResourceOutputV2> records, integer numberOfResults){
            this.records = records;
            this.numberOfResults = numberOfResults;
        }
    }
    
    public class school{
        public list<VemoAPISchoolResource.SchoolResourceOutputV2> records{get;set;}
        public integer numberOfResults {get;set;}
        
        public school(List<VemoAPISchoolResource.SchoolResourceOutputV2> records, integer numberOfResults){
            this.records = records;
            this.numberOfResults = numberOfResults;
        }
    }
    
    //public class student{
    //    public list<VemoAPIStudentResource.StudentResourceOutputV2> records{get;set;}
    //    public integer numberOfResults {get;set;}
        
    //    public student(list<VemoAPIStudentResource.StudentResourceOutputV2> records, integer numberOfResults){
    //        this.records = records;
    //        this.numberOfResults = numberOfResults;
    //    }
    //}
    
    public class program{
        public list<VemoAPIProgramResource.ProgramResourceOutputV2> records{get;set;}
        public integer numberOfResults {get;set;}
        
        public program(list<VemoAPIProgramResource.ProgramResourceOutputV2> records, integer numberOfResults){
            this.records = records;
            this.numberOfResults = numberOfResults;
        }
    }
    
    public class agreement{
        public list<VemoAPIAgreementResource.AgreementResourceOutputV2> records{get;set;}
        public integer numberOfResults {get;set;}
        
        public agreement(list<VemoAPIAgreementResource.AgreementResourceOutputV2> records, integer numberOfResults){
            this.records = records;
            this.numberOfResults = numberOfResults;
        }
    }
}