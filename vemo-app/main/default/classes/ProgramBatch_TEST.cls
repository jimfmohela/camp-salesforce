@isTest
public with sharing class ProgramBatch_TEST {
    
    private static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @testSetup
    static void CreateSetupRecords(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        TestDataFactory.createAndInsertEmailTemplates(new Set<String>{'AllSchoolsAwaitingDisbursementConfirmation'});
    }

    @isTest
    static void DISB_CONF_NOTIFICATION_Test (){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(3);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id, Program__c> prgMap = TestDataFactory.createAndInsertPrograms(3, schools);
        for(Program__c program : prgMap.values()){
            program.DisbursementConfRequiredNotification__c = true;
            program.AutomaticallyConfirmTransactions__c = false;
            program.SchoolEmailNotification2__c = 'Test.email@test.com';
        }
        //update prgMap.values();
        dbUtil.updateRecords(prgMap.values());
        
        system.assertEquals(true, prgMap.values()[0].DisbursementConfRequiredNotification__c);
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertTestStudentAccount();
        Map<ID, StudentProgram__c> studentProgramMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, prgMap);
        for(StudentProgram__c stuPrg :  studentProgramMap.values()){
            stuPrg.Status__c  = 'Partially Funded';
        }
        //update studentProgramMap.values();
        dbUtil.updateRecords(studentProgramMap.values());
        
        Map<ID, Transaction__c> trxnMap = TestDataFactory.createAndInsertTransactions(1, studentProgramMap, 'Disbursement');
        //system.assertEquals(9, studentProgramMap.size());
        Test.startTest();
        ProgramBatch job = new ProgramBatch();
        job.job = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }

    @isTest
    static void DISB_CONF_NOTIFICATION_Test_withBlankQuery (){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        dbUtil.queryExecutor = new UserContext();
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(3);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<Id, Program__c> prgMap = TestDataFactory.createAndInsertPrograms(3, schools);
        for(Program__c program : prgMap.values()){
            program.DisbursementConfRequiredNotification__c = true;
            program.AutomaticallyConfirmTransactions__c = false;
            program.SchoolEmailNotification2__c = 'Test.email@test.com';
        }
        //update prgMap.values();
        dbUtil.updateRecords(prgMap.values());
        
        system.assertEquals(true, prgMap.values()[0].DisbursementConfRequiredNotification__c);
        Map<Id, Account> studentMap = TestDataFactory.createAndInsertTestStudentAccount();
        Map<ID, StudentProgram__c> studentProgramMap = TestDataFactory.createAndInsertStudentProgram(1, studentMap, prgMap);
        for(StudentProgram__c stuPrg :  studentProgramMap.values()){
            stuPrg.Status__c  = 'Partially Funded';
        }
        //update studentProgramMap.values();
        dbUtil.updateRecords(studentProgramMap.values());
        
        //system.assertEquals(9, studentProgramMap.size());
        Map<ID, Transaction__c> trxnMap = TestDataFactory.createAndInsertTransactions(1, studentProgramMap, 'Disbursement');
        Test.startTest();
        ProgramBatch job = new ProgramBatch('');
        job.job = ProgramBatch.JobType.DISB_CONF_NOTIFICATION;
        Database.executeBatch(job, 200);
        Test.stopTest();
        }
    }
}