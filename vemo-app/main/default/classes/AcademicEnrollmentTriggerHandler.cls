/////////////////////////////////////////////////////////////////////////
// Class: AcademicEnrollmentTriggerHandler 
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2019-02-18   Rini Gupta       Created                              
// 
/////////////////////////////////////////////////////////////////////////

public with sharing class AcademicEnrollmentTriggerHandler implements TriggerDispatch.ITriggerHandlerClass {
    
    /**************************Static Variables***********************************/

    /**************************State acctrol Variables**********************************/
    public static boolean mainHasRun = false;
    public static boolean inProgressHasRun = false;
    
    /**************************Constructors**********************************************/
    
    /**************************Execution acctrol - Entry Points**********************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: mainEntry
    /////////////////////////////////////////////////////////////////////////
    public void mainEntry(TriggerDispatch.Triggercontext tc){       
        mainHasRun = true;
            
        if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        
        //the following lines should theoretically never be called but could be called from a simulated transaction
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
            

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: inProgressEntry
    /////////////////////////////////////////////////////////////////////////
    public void inProgressEntry(TriggerDispatch.Triggercontext tc){
        inProgressHasRun = true;
    
         //Call Desired Functions - Or Not - Given this is InProgressEntry
        if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isInsert) onBeforeInsert(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isUpdate) onBeforeUpdate(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isBefore && tc.isDelete) onBeforeDelete(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isInsert) onAfterInsert(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isUpdate) onAfterUpdate(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isDelete) onAfterDelete(tc);
        else if(tc.handler == 'AcademicEnrollmentTriggerHandler' && tc.isAfter && tc.isUndelete) onAfterUndelete(tc);
        
        //This is where to decide whether or not to allow other triggers to fire based upon DML on other handlerects
        else if(tc.handler != 'AcademicEnrollmentTriggerHandler'){
            //Determine what to do with other triggers - either kill them or forward them along
            system.debug('tc.handler--'+tc.handler);
            TriggerDispatch.forwardTrigger(tc, this);               
        }

    }
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeInsert
    /////////////////////////////////////////////////////////////////////////    
    public void onBeforeInsert(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onBeforeInsert()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> newAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.newList;
        //This is where you should call your business logic
    

    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onBeforeUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> newAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.newList;
        List<AcademicEnrollment__c> oldAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.oldList;
        Map<ID, AcademicEnrollment__c> newAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.newMap;
        Map<ID, AcademicEnrollment__c> oldAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.oldMap;
        //This is where you should call your business logic
    

    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onBeforeDelete
    /////////////////////////////////////////////////////////////////////////
    public void onBeforeDelete(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onBeforeDelete()');
       //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> oldAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.oldList;
        Map<ID, AcademicEnrollment__c> oldAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.oldMap;
        //This is where you should call your business logic
        

    }
    
    /****************************After logic****************************************/
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterInsert
    /////////////////////////////////////////////////////////////////////////
    public void onAfterInsert(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onAfterInsert()');
         //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> newAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.newList;
        Map<ID, AcademicEnrollment__c> newAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.newMap;
        //This is where you should call your business logic
        insertAccessRule(null, newAcademicEnrollmentMap);
        manageAudit(null, newAcademicEnrollmentMap);
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUpdate
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUpdate(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onAfterUpdate()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> newAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.newList;
        List<AcademicEnrollment__c> oldAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.oldList;
        Map<ID, AcademicEnrollment__c> newAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.newMap;
        Map<ID, AcademicEnrollment__c> oldAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.oldMap;
        //This is where you should call your business logic
        
        populateAcademicEnrollment(oldAcademicEnrollmentMap, newAcademicEnrollmentMap);
        manageAudit(oldAcademicEnrollmentMap, newAcademicEnrollmentMap);
        //manageCases(oldAcademicEnrollmentMap, newAcademicEnrollmentMap); // moving this logic to Process Builder as per Mark: SCHOOL-560
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterDelete
    /////////////////////////////////////////////////////////////////////////    
    public void onAfterDelete(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onAfterDelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> oldAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.oldList;
        Map<ID, AcademicEnrollment__c> oldAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.oldMap;
        //This is where you should call your business logic
    }
    
    
    /////////////////////////////////////////////////////////////////////////
    //Method: onAfterUndelete
    /////////////////////////////////////////////////////////////////////////
    public void onAfterUndelete(TriggerDispatch.Triggercontext tc){
        system.debug('AcademicEnrollmentTriggerHandler.onAfterUndelete()');
        //Recast the trigger acctext variables into the appropriate shandlerect types
        List<AcademicEnrollment__c> newAcademicEnrollmentList = (List<AcademicEnrollment__c>)tc.newList;
        Map<ID, AcademicEnrollment__c> newAcademicEnrollmentMap = (Map<ID, AcademicEnrollment__c>)tc.newMap;
        //This is where you should call your business logic
        
    }
    
    private void insertAccessRule(Map<Id, AcademicEnrollment__c> oldAcademicEnrollmentMap, Map<Id, AcademicEnrollment__c> newAcademicEnrollmentMap){
        Map<Id, AcademicEnrollment__c> AcademicEnrollmentById = AcademicEnrollmentQueries.getAcademicEnrollmentMapWithId(newAcademicEnrollmentMap.keySet());
        List<AccessRule__c> accessRules = new List<AccessRule__c>();
        for(AcademicEnrollment__c ae: newAcademicEnrollmentMap.values()){
            AccessRule__c rule = new AccessRule__c();
            rule.AcademicEnrollment__c = ae.id;
            rule.Account__c = AcademicEnrollmentById.get(ae.id).Provider__c;
            //rule.AcademicEnrollmentReadAccess__c = true;
            rule.AcademicEnrollmentEditAccess__c = true;            
            accessRules.add(rule);    
        }
        
        if(accessRules.size()>0)
            insert accessRules;
    } 
    
    private void populateAcademicEnrollment(Map<Id, AcademicEnrollment__c> oldAcademicEnrollmentMap, Map<Id, AcademicEnrollment__c> newAcademicEnrollmentMap){
        set<Id> aeIds = new set<Id>();
        for(AcademicEnrollment__c ae : newAcademicEnrollmentMap.values()){
            if(ae.Update__c && !oldAcademicEnrollmentMap.get(ae.id).Update__c){
                aeIds.add(ae.id);
            }
        }
        if(aeIds != null && aeIds.size()>0){
            //System.enqueueJob(new RecalculateAcademicEnrllmentQueueable(aeIds));
            AcademicEnrollmentBatch aeBatch = new AcademicEnrollmentBatch(aeIds);
            aeBatch.job = AcademicEnrollmentBatch.JobType.RECALCULATE_ACADEMICENROLLMENT;
            Database.executeBatch(aeBatch);
            //Database.executeBatch(new AcademicEnrollmentBatch(aeIds));
        }
    }
    
    /////////////////////////////////////////////////////////////////////////
    //Method: manageAudit
    /////////////////////////////////////////////////////////////////////////
    private void manageAudit(Map<Id, AcademicEnrollment__c> oldAcademicEnrollmentMap, 
                                Map<Id, AcademicEnrollment__c> newAcademicEnrollmentMap){
                                
        System.debug('***********Running manageAudit*************');
        List<AcademicEnrollmentAudit__c> auditToCreate = new List<AcademicEnrollmentAudit__c>();
        Set<String> excludeFields = new Set<String>{'academicenrollment__c'};    
        
        for(AcademicEnrollment__c ae : newAcademicEnrollmentMap.values()){
            
            Boolean createAuditRecord = false;
            if(oldAcademicEnrollmentMap == null){
                createAuditRecord = true;
                //continue;
            }
            SObjectType academicEnrollmentAuditDescribe = Schema.getGlobalDescribe().get('AcademicEnrollmentAudit__c');
            Map<String,Schema.SObjectField> acadEnrollAudFields = academicEnrollmentAuditDescribe.getDescribe().fields.getMap();
            
            if(oldAcademicEnrollmentMap != null){
                for(String fld : acadEnrollAudFields.keySet()){
                    if(fld.endsWith('__c') && !excludeFields.contains(fld)){
                        if(ae.get(fld) != oldAcademicEnrollmentMap.get(ae.id).get(fld)){
                            System.debug('***********A Field Changed!!!!*************');
                            createAuditRecord = true;
                        }
                    }
                }
            }
            System.debug('***********createAuditRecord = ' + createAuditRecord);
            if(createAuditRecord){
                AcademicEnrollmentAudit__c audit = new AcademicEnrollmentAudit__c();
                audit.AcademicEnrollment__c = ae.Id;
                for(String fld : acadEnrollAudFields.keySet()){
                    if(fld.endsWith('__c') && !excludeFields.contains(fld)){
                        audit.put(fld, ae.get(fld));
                    }
                }
                auditToCreate.add(audit);
            }
        }
        if(auditToCreate.size()>0){
            insert auditToCreate;
        }
    }
    
    private void manageCases(Map<Id, AcademicEnrollment__c> oldAcademicEnrollmentMap, Map<Id, AcademicEnrollment__c> newAcademicEnrollmentMap){
        //moving this logic to Process Builder as per Mark: SCHOOL-560
        //System.debug('AcademicEnrollmentTriggerHandler.manageCases()');  
        //List<Case> casesToAdd = new List<Case>();
        //List<AcademicEnrollment__c> AcademicEnrollmentToUpdate = new List<AcademicEnrollment__c>();
        //Set<ID> studentIDs = new Set<ID>();
        //for(AcademicEnrollment__c ae: newAcademicEnrollmentMap.values()){
        //    studentIDs.add(ae.Student__c); 
        //}
        //Map<ID, Account> studentMap = AccountQueries.getStudentMapWithStudentID(studentIDs);
        //if(oldAcademicEnrollmentMap != null){
        //    for(AcademicEnrollment__c ae: newAcademicEnrollmentMap.values()){
        //        if(ae.PendingStatus__c != null && ae.PendingStatus__c != oldAcademicEnrollmentMap.get(ae.Id).PendingStatus__c){
        //            casesToAdd.add(new Case(RecordTypeID = GlobalUtil.getRecordTypeIdByLabelName('Case', GlobalUtil.CASE_RECTYPELBL_DEFAULT),
        //                                    ContactID = studentMap.get(ae.Student__c).PersonContactID,
        //                                    Subject = 'Enrollment Status Change',
        //                                    Reason =  'Enrollment Update',
        //                                    Type = 'School Update',
        //                                    Origin = 'School Admin',
        //                                    Status = 'New',
        //                                    Priority = 'Medium',
        //                                    OwnerId = GlobalUtil.getQueueId('Servicing'),
        //                                    School__c = ae.Provider__c,
        //                                    AcademicEnrollment__c = ae.id ));
                                            
        //            AcademicEnrollmentToUpdate.add(new AcademicEnrollment__c(Id = ae.id,
        //                                                                     PendingUpdate__c = true)); 
        //        }
        //    }
        //}   
        
        //if(casesToAdd.size()>0){
        //    insert casesToAdd;
        //}
        
        //if(AcademicEnrollmentToUpdate.size()>0){
        //    update AcademicEnrollmentToUpdate;
        //}
    }
}