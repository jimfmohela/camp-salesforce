/////////////////////////////////////////////////////////////////////////
// Class: StudProgAmountDueTriggerHandler_TEST
// 
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-06-04   Jared Hagemann  Created                              
// 
/////////////////////////////////////////////////////////////////////////
@isTest
public class StudProgAmountDueTriggerHandler_TEST {

    private static DatabaseUtil dbUtil = new DatabaseUtil();

    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
        DatabaseUtil.setRunQueriesInMockingMode(false);
        User USerWithRole;
        if(userWithRole == null) {  
            if(UserInfo.getUserRoleId() == null) {
                UserRole r = new UserRole(name = 'TEST ROLE');
                Database.insert(r);
                userWithRole = new User(alias = 'hasrole', email='mohelatest@mohela1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(),
                                    timezonesidkey='America/Los_Angeles', username = 'mohelatest@mohela1.com', communitynickname = 'testcommunity');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            System.assert(userWithRole.userRoleId != null,
                          'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }
        System.RunAs(UserWithRole){
        Map<Id, Account> students = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Map<Id, Account> schools = TestDataFactory.createAndInsertSchoolCustomerAccounts(1);
        Map<ID, User> adminMap = TestDataFactory.createAndInsertSchoolAdminUser(1, schools);
        Map<ID, Program__c> programs = TestDataFactory.createAndInsertPrograms(1, schools);
        Map<Id, StudentProgram__c> agreementMap = TestDataFactory.createAndInsertStudentProgram(1, students, programs);
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats = TestDataFactory.createAndInsertStudentProgramMonthlyStatus(5, agreementMap);
        }
    }


    static testMethod void testManageAgreementsInsert(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats =new Map<Id, StudentProgramMonthlyStatus__c>([select Id, Agreement__c from StudentProgramMonthlyStatus__c]);
        Test.startTest();
        Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(agreementMap, agreementMonthStats);
        Test.stopTest();

        List<StudentProgram__c> resultAgreements = [select Id, AmountDueToDate__c, ReconciliationDueToDate__c, MonthlyAmountDueToDate__c from StudentProgram__c where Id in: agreementMap.keySet() and AmountDueToDate__c > 0];
        System.assert(!resultAgreements.isEmpty());
        
        for(StudentProgram__c agreement : resultAgreements){
            System.assertEquals(5000 ,agreement.AmountDueToDate__c);
        }   
        
    }

    static testMethod void testManageAgreementsUpdate(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats =new Map<Id, StudentProgramMonthlyStatus__c>([select Id, Agreement__c from StudentProgramMonthlyStatus__c]);
        Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(agreementMap, agreementMonthStats);
        for(StudentProgramAmountDue__c spad : agreementAmountDue.values()){
            spad.Amount__c = 500;
        }
        Test.startTest();
            //update agreementAmountDue.values();
            dbUtil.updateRecords(agreementAmountDue.values());
        Test.stopTest();

        List<StudentProgram__c> resultAgreements = [select Id, AmountDueToDate__c, ReconciliationDueToDate__c, MonthlyAmountDueToDate__c from StudentProgram__c where Id in: agreementMap.keySet() and AmountDueToDate__c > 0];
        System.assert(!resultAgreements.isEmpty());
        
        /*for(StudentProgram__c agreement : resultAgreements){
            System.assertEquals(2500 ,agreement.AmountDueToDate__c);
        }*/  
        
    }

    static testMethod void testManageAgreementsDelete(){
        DatabaseUtil.setRunQueriesInMockingMode(false);
        TestUtil.setStandardConfiguration();
        Map<Id, StudentProgram__c> agreementMap = StudentProgramQueries.getStudentProgramMap();
        Map<Id, StudentProgramMonthlyStatus__c> agreementMonthStats =new Map<Id, StudentProgramMonthlyStatus__c>([select Id, Agreement__c from StudentProgramMonthlyStatus__c]);
        Map<Id, StudentProgramAmountDue__c> agreementAmountDue = TestDataFactory.createAndInsertStudentProgramAmountDue(agreementMap, agreementMonthStats);
        
        Test.startTest();
            //delete agreementAmountDue.values();
            dbUtil.deleteRecords(agreementAmountDue.values());
        Test.stopTest();

        //List<StudentProgram__c> resultAgreements = [select Id, AmountDueToDate__c, ReconciliationDueToDate__c, MonthlyAmountDueToDate__c from StudentProgram__c where Id in: agreementMap.keySet() and AmountDueToDate__c > 0];
        //System.assert(resultAgreements.isEmpty());      
    }
}