@isTest
public with sharing class InboundAPIPETriggerHdlr_TEST {
    @isTest
    public static void testPEInsert(){
        InboundAPI__e testEvent = new InboundAPI__e();
        
        Test.startTest();
        Database.SaveResult sr = EventBus.publish(testEvent);
        Test.stopTest();

        List<InboundAPILog__c> logs = new List<InboundAPILog__c>();
        logs = [SELECT id from InboundAPILog__c];

        System.assertEquals(1, logs.size());

    }
}