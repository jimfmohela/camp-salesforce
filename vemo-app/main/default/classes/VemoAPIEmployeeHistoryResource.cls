/////////////////////////////////////////////////////////////////////////
// Class: VemoAPIEmployeeHistoryResource
// 
// Description: 
//      Direction Central for EmployeeHistory API
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2017-05-11   Jared Hagemann  Created 
/////////////////////////////////////////////////////////////////////////
public with sharing class VemoAPIEmployeeHistoryResource {

    public static Object handleAPI(VemoAPI.APIInfo api){
        if((api.version == 'v1') && (api.method == 'GET')){
            return handleGetV1(api);
        }
        if((api.version == 'v1') && (api.method == 'POST')){
            return handlePostV1(api);
        }
        if((api.version == 'v1') && (api.method == 'PUT')){
            return handlePutV1(api);
        }   
        if((api.version == 'v1') && (api.method == 'DELETE')){
            return handleDeleteV1(api);
        }   
            
        throw new VemoAPI.VemoAPIFaultException('Not a supported method ('+api.method+') for resource: '+api.resource +' and version: '+ api.version);
        return null;
    }

    public static VemoAPI.ResultResponse handleGetV1(VemoAPI.APIInfo api){  
        System.debug('VemoAPIEmployeeHistoryResource.handleGetV1');
        String employmentHistoryIDParam = api.params.get('employmentHistoryID');
        String employerParam = api.params.get('employer');
        String jobTitleParam = api.params.get('jobTitle');
        String studentIDParam = api.params.get('studentID');
        String groupByParam = api.params.get('groupBy');        
        String recentRecordParam = api.params.get('mostRecent');        
        String allVerifiedRecordParam = api.params.get('allVerified');
        String currentParam = api.params.get('current');
        String pastParam = api.params.get('past');
        String uploadedDocParam = api.params.get('uploadedDoc');
        
        List<EmploymentHistoryService.EmploymentHistory> empHisList = new List<EmploymentHistoryService.EmploymentHistory>();
        if(employmentHistoryIDParam != null){
            empHisList = EmploymentHistoryService.getEmploymentHistoryWithEmployentHistoryID(VemoApi.parseParameterIntoIDSet(employmentHistoryIDParam));
        }
        else if(studentIDParam != null){
            //This variable will define whether we need to return consolidated JSON form or not
            Boolean needConsolidatedForm = false;
            if(currentParam != null && currentParam.toLowerCase() == 'true'){
                needConsolidatedForm = true;
            }
            if(pastParam != null && pastParam.toLowerCase() == 'true'){
                needConsolidatedForm = true;
            }
            List<EmploymentHistory__c> EmploymentHistorySobjList = new List<EmploymentHistory__c>();
           
            //verify accounts this student can access
            Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(VemoApi.parseParameterIntoIDSet(studentIDParam)); //authorized records should be returned
            if(verifiedAccountMap.size()>0){
                EmploymentHistorySobjList = EmploymentHistoryService.getEmploymentHistoryWithStudentIDAndOtherParams(
                                verifiedAccountMap.keySet(), recentRecordParam, 
                                allVerifiedRecordParam, employerParam, jobTitleParam, groupByParam, 
                                currentParam, pastParam, uploadedDocParam
                            );
            }            
            if(!needConsolidatedForm){
                for(EmploymentHistory__c empHis : EmploymentHistorySobjList){
                    empHisList.add(new EmploymentHistoryService.EmploymentHistory(empHis));
                }
            }
            else {
                //return consolidated form of Employment Histories
                List<EmploymentHistoryService.EmploymentHistoryConsolidatedForm> EmploymentHistoryConsolidatedFormList = new List<EmploymentHistoryService.EmploymentHistoryConsolidatedForm>();
                EmploymentHistoryConsolidatedFormList = EmploymentHistoryService.getEmploymentHistoryConsolidatedForm(EmploymentHistorySobjList, currentParam);
                return (new VemoAPI.ResultResponse(EmploymentHistoryConsolidatedFormList, EmploymentHistoryConsolidatedFormList.size()));
            }
        }
        else{
            throw new VemoAPI.VemoAPIFaultException('Required parameter employmentHistoryID or studentID');
        }
        List<EmploymentHistoryOutputV1> results = new List<EmploymentHistoryOutputV1>();
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistoryService.EmploymentHistory eh: empHisList){
            AccountIDsToVerify.add((ID) eh.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        for(EmploymentHistoryService.EmploymentHistory empHis : empHisList){
            if(VerifiedAccountMap.containsKey((ID) empHis.studentID)){
                results.add(new EmploymentHistoryOutputV1(empHis));
            }
        }
        return (new VemoAPI.ResultResponse(results, results.size()));
    }

    public static VemoAPI.ResultResponse handlePostV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmployeeHistoryResource.handlePostV1');
        List<EmploymentHistoryService.EmploymentHistory> newEmploymentHistory = new List<EmploymentHistoryService.EmploymentHistory>();
        List<EmploymentHistoryInputV1> employmentHistoryJSON = (List<EmploymentHistoryInputV1>)JSON.deserialize(api.body, List<EmploymentHistoryInputV1>.class);
        
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistoryInputV1 empHis : employmentHistoryJSON){
            AccountIDsToVerify.add((ID) empHis.studentID);    
        }
        Map<ID, Account> verifiedAccountMap = AccountQueries.getStudentMapWithStudentID(AccountIdsToVerify); //authorized records should be returned
        
        for(EmploymentHistoryInputV1 empHis : employmentHistoryJSON){
            if(verifiedAccountMap.containsKey((ID)empHis.studentID)){
                empHis.validatePOSTFields();
                EmploymentHistoryService.EmploymentHistory empHisServ = new EmploymentHistoryService.EmploymentHistory();
                empHisServ = employmentHistoryInputV1ToEmploymentHistory(empHis);
                newEmploymentHistory.add(empHisServ);
            }
        }
        Set<ID> employmentHistoryIDs = EmploymentHistoryService.createEmploymentHistory(newEmploymentHistory);
        return (new VemoAPI.ResultResponse(employmentHistoryIDs, employmentHistoryIDs.size()));
    }

    public static VemoAPI.ResultResponse handlePutV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmployeeHistoryResource.handlePutV1');
        List<EmploymentHistoryService.EmploymentHistory> newEmploymentHistory = new List<EmploymentHistoryService.EmploymentHistory>();
        List<EmploymentHistoryInputV1> employmentHistoryJSON = (List<EmploymentHistoryInputV1>)JSON.deserialize(api.body, List<EmploymentHistoryInputV1>.class);
        
        Set<ID> IDsToVerify = new Set<ID>();
        for(EmploymentHistoryInputV1 empHis : employmentHistoryJSON){
            IDsToVerify.add((ID) empHis.employmentHistoryID);    
        }
        Map<ID, EmploymentHistory__c> verifiedEmploymentHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMapWithEmploymentHistoryId(IdsToVerify); //authorized records should be returned
        
        for(EmploymentHistoryInputV1 empHis : employmentHistoryJSON){
            if(verifiedEmploymentHistoryMap.containsKey((ID)empHis.employmentHistoryID)){
                empHis.validatePUTFields();
                EmploymentHistoryService.EmploymentHistory empHisServ = new EmploymentHistoryService.EmploymentHistory();
                empHisServ = employmentHistoryInputV1ToEmploymentHistory(empHis);
                newEmploymentHistory.add(empHisServ);
            }
        }
        Set<ID> employmentHistoryIDs = EmploymentHistoryService.updateEmploymentHistory(newEmploymentHistory);
        return (new VemoAPI.ResultResponse(employmentHistoryIDs, employmentHistoryIDs.size()));
    }

    public static VemoAPI.ResultResponse handleDeleteV1(VemoAPI.APIInfo api){
        System.debug('VemoAPIEmployeeHistoryResource.handleDeleteV1');
        String employmentHistoryIDParam = api.params.get('employmentHistoryID');
        
        Map<ID, EmploymentHistory__c> employmentHistoryMap = EmploymentHistoryQueries.getEmploymentHistoryMapWithEmploymentHistoryId(VemoApi.parseParameterIntoIDSet(employmentHistoryIDParam));
        Set<ID> AccountIDsToVerify = new Set<ID>();
        for(EmploymentHistory__c eh: employmentHistoryMap.values()){
            AccountIDsToVerify.add(eh.Student__c);
        }
        Set<ID> employmentHistoriesToDelete = new Set<ID>();
        for(EmploymentHistory__c eh: employmentHistoryMap.values()){
            employmentHistoriesToDelete.add(eh.Student__c); 
        }
        Integer numToDelete = 0;
        if(employmentHistoriesToDelete.size()>0){
            numToDelete = EmploymentHistoryService.deleteEmploymentHistory(employmentHistoriesToDelete);
        }
        return (new VemoAPI.ResultResponse(true, numToDelete));
    }



    public class EmploymentHistoryOutputV1{
        
        public EmploymentHistoryOutputV1(EmploymentHistoryService.EmploymentHistory empHis){
            this.employmentHistoryID = empHis.employmentHistoryID;          
            this.category = empHis.category;
            this.employer = empHis.employer;
            this.employmentEndDate = empHis.employmentEndDate;
            this.employmentStartDate = empHis.employmentStartDate;
            this.studentID = empHis.studentID;
            this.type = empHis.type;
            this.verified = empHis.verified;
            
            this.employmentSummaryID = empHis.employmentSummaryID;          
            this.jobTitle = empHis.jobTitle;
            this.hoursPerWeek = empHis.hoursPerWeek;
            this.hourlyRate = empHis.hourlyRate;
            this.yearlySalary = empHis.yearlySalary;
            this.paymentSchedule = empHis.paymentSchedule;
            this.bonusAmount = empHis.bonusAmount;
            this.bonusFrequency = empHis.bonusFrequency;
            this.commissionAmount = empHis.commissionAmount;
            this.commissionFrequency = empHis.commissionFrequency;
            this.noLongerEmployedHere = empHis.noLongerEmployedHere;
            this.externalCompanyID  = empHis.externalCompanyID;
            this.eventType = empHis.eventType;
            this.unemploymentEffectiveDate = empHis.unemploymentEffectiveDate;
            this.lookForEmployment = empHis.lookForEmployment;
            this.dateReported = empHis.dateReported;
            this.status = empHis.status;
            this.tipAmount = empHis.tipAmount;
            this.effectiveDate = empHis.effectiveDate;
            this.uploadedDoc = empHis.uploadedDoc ;
        }
        public String employmentHistoryID {get;set;}
        public String category {get;set;}
        public String employer {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String studentID {get;set;}
        public String type {get;set;}
        public Boolean verified {get;set;}
        
        public String employmentSummaryID {get;set;}
        public String jobTitle {get;set;}
        public Decimal hoursPerWeek {get;set;}
        public Decimal hourlyRate {get;set;}
        public Decimal yearlySalary {get;set;}
        public String paymentSchedule {get;set;}        
        public Decimal bonusAmount {get;set;}
        public String bonusFrequency {get;set;}
        public Decimal commissionAmount {get;set;}
        public String commissionFrequency {get;set;}
        public Boolean noLongerEmployedHere {get;set;}
        public String externalCompanyID {get;set;}
        public String eventType {get;set;}
        public Date unemploymentEffectiveDate {get;set;}
        public Boolean lookForEmployment {get;set;}
        public Date dateReported {get;set;}
        public String status {get;set;}
        public Decimal tipAmount {get;set;}
        public Date effectiveDate {get;set;}
        public Boolean uploadedDoc {get;set;}
    }

    public class EmploymentHistoryInputV1{
        public String employmentHistoryID {get;set;}
        public String category {get;set;}
        public String employer {get;set;}
        public Date employmentEndDate {get;set;}
        public Date employmentStartDate {get;set;}
        public String studentID {get;set;}
        public String type {get;set;}
        public Boolean verified {get;set;}
        
        public String employmentSummaryID {get;set;}
        public String jobTitle {get;set;}
        public Decimal hoursPerWeek {get;set;}
        public Decimal hourlyRate {get;set;}
        public Decimal yearlySalary {get;set;}
        public String paymentSchedule {get;set;}        
        public Decimal bonusAmount {get;set;}
        public String bonusFrequency {get;set;}
        public Decimal commissionAmount {get;set;}
        public String commissionFrequency {get;set;}
        public Boolean noLongerEmployedHere {get;set;}
        public String externalCompanyID {get;set;}
        public String eventType {get;set;}
        public Date unemploymentEffectiveDate {get;set;}
        public Boolean lookForEmployment {get;set;}
        public Date dateReported {get;set;}
        public String status {get;set;}
        public Decimal tipAmount {get;set;}
        public Date effectiveDate {get;set;}
        public Boolean uploadedDoc {get;set;}

        public EmploymentHistoryInputV1(Boolean testValues){
            if(testValues){
                this.category = 'Employee'; //Contractor, Internship
                this.employer = 'Test Employer'; //Planned, Closed, Cancelled
                this.type = 'Full Time'; //Part Time
            }
        }

        public void validatePOSTFields(){
            if(employmentHistoryID != null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID cannot be created in POST');
        }
        public void validatePUTFields(){
            if(employmentHistoryID == null) throw new VemoAPI.VemoAPIFaultException('employmentHistoryID is a required input parameter on PUT');
            if(studentID != null) throw new VemoAPI.VemoAPIFaultException('studentID is not a writable input parameter on PUT');
        }
    }

    public static EmploymentHistoryService.EmploymentHistory employmentHistoryInputV1ToEmploymentHistory(EmploymentHistoryInputV1 input){
        EmploymentHistoryService.EmploymentHistory output = new EmploymentHistoryService.EmploymentHistory();
        output.employmentHistoryID = input.employmentHistoryID;         
        output.category = input.category;
        output.employer = input.employer;
        output.employmentEndDate = input.employmentEndDate;
        output.employmentStartDate = input.employmentStartDate;
        output.studentID = input.studentID;
        output.type = input.type;
        output.verified = input.verified;
        
        output.employmentSummaryID = input.employmentSummaryID;         
        output.jobTitle = input.jobTitle;
        output.hoursPerWeek = input.hoursPerWeek;
        output.hourlyRate = input.hourlyRate;
        output.yearlySalary = input.yearlySalary;
        output.paymentSchedule = input.paymentSchedule;
        output.bonusAmount = input.bonusAmount;
        output.bonusFrequency = input.bonusFrequency;
        output.commissionAmount = input.commissionAmount;
        output.commissionFrequency = input.commissionFrequency;
        output.noLongerEmployedHere = input.noLongerEmployedHere;
        output.externalCompanyID = input.externalCompanyID;
        output.eventType = input.eventType;
        output.unemploymentEffectiveDate = input.unemploymentEffectiveDate;
        output.lookForEmployment = input.lookForEmployment;
        output.dateReported = input.dateReported;
        output.status = input.status;
        output.tipAmount = input.tipAmount;
        output.effectiveDate = input.effectiveDate;
        output.uploadedDoc = input.uploadedDoc;
        return output;
    }
}