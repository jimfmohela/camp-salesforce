/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class SchoolAdminForgotPasswordController {
    public String username {get; set;}   
       
    public SchoolAdminForgotPasswordController() {
        System.debug('SchoolAdminForgotPasswordController()');
    }
  
    public PageReference redirectToForgotPasswordConfirmPage() {
      
    PageReference ForgotPasswordConfirmPage = Page.SchoolAdminForgotPasswordConfirm;
    ForgotPasswordConfirmPage.setRedirect(true);
    return ForgotPasswordConfirmPage;
    }
    
    public PageReference redirectToLoginPage(){
      PageReference loginPage = Page.SchoolAdminSiteLogin;
      loginPage.setRedirect(true);
      return loginPage;
    }
    
    @RemoteAction
    public Static ProcessingResult forgotPassword(String credentialObj){
      ProcessingResult resultObj = new ProcessingResult ();
      resultObj.isSuccess = false;
      resultObj.result = 'Opps ! An error had occured, please contact your admin !';
      try {
        System.debug(credentialObj);
        String username = '';
        if(credentialObj != null){
        JSONParser parser = JSON.createParser(credentialObj);
        while (parser.nextToken() != null) {
           if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME && parser.getText() == 'email') {
             parser.nextToken();
             username = parser.getText();
           }
        }
        }
        System.debug('>>>>>username-->'+username);
        Boolean isValidUserName = Site.isValidUsername(username);
          if(isValidUserName){
            Boolean success = Site.forgotPassword(username);
          PageReference ForgotPasswordConfirmPage = Page.SchoolAdminForgotPasswordConfirm;
          ForgotPasswordConfirmPage.setRedirect(true);
          if (success) {
            resultObj.isSuccess = true;        
            resultObj.result = ForgotPasswordConfirmPage.getURL();
          }
          }
          else {
            resultObj.result = 'Username is invalid !';
          }
      } catch(Exception e){
        resultObj.result = 'Error - '+e.getMessage();
      }
      System.debug(JSON.serialize(resultObj));
      return resultObj;
    }
    
    public class ProcessingResult {
      public Boolean isSuccess {get;set;}
      public String result {get;set;}
    }
}