@isTest
public class VemoAPIUserResource_TEST {
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testHandleGetV2(){
        
        Map<String, String> userParams = new Map<String, String>();
        userParams.put('VEMO_AUTH', 'ABC');
        VemoAPI.APIInfo userApiInfo = TestUtil.initializeAPI('v2', 'GET', userParams, null);

        Test.startTest();
        VemoAPI.ResultResponse userResult = (VemoAPI.ResultResponse)VemoAPIUserResource.handleAPI(userApiInfo);
        Test.stopTest();
    }
}