/////////////////////////////////////////////////////////////////////////
// Class: EventInstanceService_TEST
// 
// Description: 
//  Test class for EventInstanceService
// 
// Version Log
// DATE---------AUTHOR----------DESCRIPTION-----------------------------
// 2018-12-14   Kamini Singh  Created              
/////////////////////////////////////////////////////////////////////////
@isTest
public class EventInstanceService_TEST {
    
    public static DatabaseUtil dbUtil = new DatabaseUtil();
    
    @TestSetup static void setupData(){
        TestUtil.createStandardTestConditions();
    }

    static testMethod void testGetEventInstanceMapForAuthUser(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        
        Test.startTest();
        List<EventInstanceService.EventInstance> resultEventInstanceList = EventInstanceService.getEventInstanceMapForAuthUser();
        System.assertEquals(testEventInstanceMap.keySet().size(), resultEventInstanceList.size());
        Test.stopTest();
    }
    static testMethod void testGetEventInstanceMapForAuthUserwithRead(){
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        
        Test.startTest();
        List<EventInstanceService.EventInstance> resultEventInstanceList = EventInstanceService.getEventInstanceMapForAuthUser(true);
        System.assertEquals(testEventInstanceMap.keySet().size(), resultEventInstanceList.size());
        Test.stopTest();
    }
    
    static testMethod void testUpdateEventInstance(){
        DatabaseUtil.setRunQueriesInMockingMode(false); 
        Map<Id, Account> testStudentAccountMap = TestDataFactory.createAndInsertStudentAccounts(TestUtil.TEST_THROTTLE);
        Set<Id> contactIds = new Set<Id>();
        for(Account acc: testStudentAccountMap.values()){
            contactIds.add(acc.PersonContactId);
        }
        Map<Id, EventType__c> EventTypes = TestDataFactory.createAndInsertEventType(TestUtil.TEST_THROTTLE);
        Map<id, Event__c> Events = TestDataFactory.createAndInsertEvent(testStudentAccountMap , EventTypes);
        
        Map<Id, Contact> testContactMap = ContactQueries.getContactMapByIDWithContactID(contactIds);
        Map<Id, EventInstance__c> testEventInstanceMap = TestDataFactory.createAndInsertEventInstance(testContactMap, Events );
        List<EventInstanceService.EventInstance> evntInsList = new List<EventInstanceService.EventInstance>();
        for(Integer i = 0; i<TestUtil.TEST_THROTTLE*TestUtil.TEST_THROTTLE; i++){
            EventInstanceService.EventInstance evntIns = new EventInstanceService.EventInstance();
            evntIns.EventInstanceID = testEventInstanceMap.values().get(i).Id;
            evntIns.read = true;
            evntInsList.add(evntIns);
        }
        Test.startTest();
        Set<ID> empHisIDs = EventInstanceService.updateEventInstance(evntInsList,'true');        
        Test.stopTest();
        System.assertEquals(TestUtil.TEST_THROTTLE*TestUtil.TEST_THROTTLE, evntInsList.size());
    }
}