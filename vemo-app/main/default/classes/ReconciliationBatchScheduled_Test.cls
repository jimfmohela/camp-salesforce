@isTest
public with sharing class ReconciliationBatchScheduled_Test {
    
    /*@isTest
    static void createAgreementsSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ReconciliationBatchScheduled BatchJob = new ReconciliationBatchScheduled();
        BatchJob.jobType = ReconciliationBatch.JobType.CREATE_AGREEMENTS;
        System.schedule('createAgreementsSchedule_Test', CRON_EXP, BatchJob);
        Test.stopTest();
        
    }*/
    
    @isTest
    static void sendIntroSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ReconciliationBatchScheduled BatchJob = new ReconciliationBatchScheduled();
        BatchJob.jobType = ReconciliationBatch.JobType.SEND_INTRO;
        System.schedule('sendIntroSchedule_Test', CRON_EXP, BatchJob);
        Test.stopTest();
        
    }
    @isTest
    static void sendKickOffSchedule_Test(){
        Test.startTest();
        Datetime dt = Datetime.now().addMinutes(5);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        ReconciliationBatchScheduled BatchJob = new ReconciliationBatchScheduled();
        BatchJob.jobType = ReconciliationBatch.JobType.SEND_KICK_OFF;
        System.schedule('sendKickOffSchedule_Test', CRON_EXP, BatchJob);
        Test.stopTest();
        
    }
}