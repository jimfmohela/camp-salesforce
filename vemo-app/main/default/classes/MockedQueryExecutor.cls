public class MockedQueryExecutor implements IQueryExecutor{

    private static Map<String, List<sobject>> records = new Map<String, List<sobject>>();    
    private static Integer s_num = 1;
    private static Map<String, Schema.SObjectType> schemas = null;
    private static Integer recordLimit = null;   
    private static Boolean resetRecordLimit = true; 
    
    public List<SObject> databaseQuery(string query){
        String tableName = getTableName(query);
        List<SObject> result = findByClassName(tableName);
        if(recordLimit == null){
            return result;
        }else{
            system.debug('Record limit for table: '+tableName+ ' is: '+recordLimit);        
            Integer maxRecords = recordLimit;        
            maxRecords = (maxRecords < result.size()) ? maxRecords : result.size();
            List<SObject> newList = new List<SObject>();
            for(Integer i = 0; i< maxRecords ;i++){
                newList.add(result[i]);
            }
            if(resetRecordLimit){
                setRecordLimit(null);                    
            }
            return newList;
        }
    }
    
    public void insertRecord(sobject recordsN){
        List<sObject> recordsList = new List<sObject>();
        recordsList.add(recordsN);
        insertRecords(recordsList);
    }
    public void insertRecords(List<sobject> recordsN){
        if(recordsN.size()>0){
            for(sobject s: recordsN){
                string className = getClassName(s);
                String prefix = getIdPrefix(className);
                s.id = getFakeId(prefix);
                className = className.toLowerCase();
                List<sobject> data = records.get(className);
                if(data == null){
                    data = new List<sobject>();
                } 
                data.add(s);
                records.put(className,data);
            }
        }else{
            system.debug('No data found to insert records');
        }        
    }
    
    public void deleteRecord(sobject recordsN){
        List<sObject> recordsList = new List<sObject>();
        recordsList.add(recordsN);
        deleteRecords(recordsList);
    }
    
    public void deleteRecords(List<sobject> recordsN){
        for(sObject s : recordsN){
            List<sobject> result = findByClassName(getClassName(s));
            if(result.size() > 0){
                for(Integer i = result.size() - 1; i >= 0; i--) {
                    if(result.get(i).id == s.id) {
                        result.remove(i);
                    }
                }
            }            
        }
    }
    
    public void updateRecord(sobject recordsN){
        List<sObject> recordsList = new List<sObject>();
        recordsList.add(recordsN);
        updateRecords(recordsList);
    }
    
    public void upsertRecord(sobject recordsN){
        List<sObject> recordsList = new List<sObject>();
        recordsList.add(recordsN);
        upsertRecords(recordsList);
    }
    
    public void undeleteRecord(SObject objs){
        insertRecord(objs);
    }
    
    public void undeleteRecords(List<SObject> objs){
        insertRecords(objs);
    }
        
    public void updateRecords(List<sobject> recordsN){
        for(sObject s : recordsN){
            List<sobject> result = findByClassName(getClassName(s));
            if(result.size() > 0){
                for(sObject record: result){
                   if(record.id == s.id){
                       record = s;
                       break;
                   }
                }
            }            
        }
    }
    
    public void upsertRecords(List<sobject> recordsN){
        for(sObject s : recordsN){
            List<sobject> result = findByClassName(getClassName(s));
            if(result.size() > 0){
                for(sObject record: result){
                   if(record.id == s.id){
                       record = s;
                       break;
                   }
                }
            }            
        }
    }
    
    public static Map<String, List<sobject>> getRecords(){
        return records;
    } 
    
    public static void setResetRecordLimit(Boolean status){
        resetRecordLimit = status ;
    }
        
    public static void setRecordLimit(Integer maxRecords ){
        recordLimit = maxRecords ;
    }    
    
    public static string getClassName(sObject s){
        return String.valueOf(s).split(':')[0];
    }      
        
    public static List<sObject> findByClassName(string className){
        className = className.toLowerCase();
        List<sobject> result = records.get(className.toLowerCase());
        if(result == null){
            system.debug('Data Not found for tableName: '+className.toLowerCase());
            result = new List<sobject>();
        }
        return result;
    }
    
    public static String getFakeId(string prefix){
        String result = String.valueOf(s_num++);
        return prefix + '0'.repeat(12-result.length()) + result;
    }
    
    public static string getIdPrefix(string type){
        if( schemas == null){
            schemas = Schema.getGlobalDescribe() ;        
        }
        return schemas.get(type).getDescribe().getKeyPrefix();
    }
    
    public static string getTableName(string query){
        return query.toLowercase().split(' from ')[1].split(' ')[0];   
    }
    
    public static Map<String,Integer> getRecordsInfo(){
        Map<String,Integer> info = new map<String, Integer>();
        for (String recordKey: records.keySet()){
            info.put(recordKey,records.get(recordKey).size());
        }
        return info;
    }
}