@isTest
public class AdobeSignAuthController_TEST {

    public static testMethod void testPage() {
        PageReference pageRef = Page.authorizeAdobe;
        Test.setCurrentPage(pageRef);
        AdobeSignAuthController controller = new AdobeSignAuthController();
        controller.redirectToAdobe();
		Test.setMock(HttpCalloutMock.class, new AdobeSignService_TEST.accessTokenRequestMockPass());
        controller.updateSettings();
    }
    
    public static testMethod void initTest(){
    	PageReference authorizeAdobePage = Page.authorizeAdobe;
        Test.setCurrentPage(authorizeAdobePage);
        authorizeAdobePage.getParameters().put('code','CBNCKBAAHBCAABAADoxRw8WPfT6Z1nZRQiyFyIfBRRm18cXy');
        authorizeAdobePage.getParameters().put('api_access_point','https%3A%2F%2Fapi.na1.echosign.com%2F');
        AdobeSignAuthController controller = new AdobeSignAuthController();
        controller.initialized = false;
        controller.init();
    }
}