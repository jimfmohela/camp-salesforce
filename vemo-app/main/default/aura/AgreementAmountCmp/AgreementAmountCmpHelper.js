({
    doInit_helper : function (c, e, h) {
        try {
            c.set("v.agreementObj",{});
            var agreementIdJS = c.get("v.recordId");
            var getAgreementList_Apex = c.get("c.getAgreementList");
            getAgreementList_Apex.setParams({
                "agreementId" : agreementIdJS
            });
            getAgreementList_Apex.setCallback(this, function(response) {
                var state = response.getState();
                //console.log("state");
                //console.log(state);
                if (state === "SUCCESS") {
                    var returnValue = JSON.parse(response.getReturnValue());
                    //console.log("agreementObj");
                    //console.log(JSON.stringify(returnValue));
                    if(returnValue !== undefined && returnValue.length > 0){
                        c.set("v.agreementObj",returnValue[0]);
                    }
                }
                else if(state === "ERROR"){
                    console.log("Error");
                    console.log(response.getError());
                }
            });
            $A.enqueueAction(getAgreementList_Apex);
        } catch(Ex){
            console.log('Exception');
            console.log(Ex);
        }
    },
})