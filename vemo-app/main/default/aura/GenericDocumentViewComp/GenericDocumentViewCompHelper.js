({
	getGenericDocuments : function(component){
		var action = component.get("c.getGenericDocumentList");
        action.setParams({
            recordId:component.get("v.recordId")
        })
		action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set('v.genericDocumentData', response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
		});
		$A.enqueueAction(action);
		
	}
})