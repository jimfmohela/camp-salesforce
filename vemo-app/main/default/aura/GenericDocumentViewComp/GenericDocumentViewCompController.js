({
	doInit : function(component, event, helper) {
        var objectNameJS = component.get("v.objectAPIName");
        var dataTableColumns = [];
        dataTableColumns.push({label: 'Document ID', fieldName: 'Id', type: 'text'});
        dataTableColumns.push({label: 'Document Type', fieldName: 'Type__c', type: 'text'});
        if(objectNameJS == "EmploymentHistory__c" || objectNameJS == "EmploymentHistory2__c"){
            dataTableColumns.push({label: 'Comments', fieldName: 'Comments__c', type: 'text'});
        }
        dataTableColumns.push({label: 'Document Status', fieldName: 'Status__c', type: 'text'});
        dataTableColumns.push({label: 'Attachment ID', fieldName: 'AttachmentID__c', type: 'text'});
        dataTableColumns.push({label: 'Attachment URL', fieldName: 'AttachmentCleanURL__c', type: 'url'});
        helper.getGenericDocuments(component);
        component.set('v.genericDocumentDataColumns', dataTableColumns);
    }
})