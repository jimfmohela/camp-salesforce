({
    doInit_helper : function (c, e, h) {
        try {
            c.set("v.statementList",[]);
            var studentIdJS = c.get("v.recordId");
            //console.log("Record Id");
            //console.log(studentIdJS);
            var getCurrentStatementV1List_Apex = c.get("c.getCurrentStatementV1List");
            getCurrentStatementV1List_Apex.setParams({
                "studentId" : studentIdJS
            });
            getCurrentStatementV1List_Apex.setCallback(this, function(response) {
                var state = response.getState();
                //console.log("state");
                //console.log(state);
                if (state === "SUCCESS") {
                    var returnValue = JSON.parse(response.getReturnValue());
                    //console.log("statementList");
                    //console.log(returnValue);
                    if(returnValue !== undefined && returnValue.length > 0){
                        c.set("v.statementList",returnValue);
                    }
                }
                else if(state === "ERROR"){
                    console.log("Error");
                    console.log(response.getError());
                }
            });
            $A.enqueueAction(getCurrentStatementV1List_Apex);
        } catch(Ex){
            console.log('Exception');
            console.log(Ex);
        }
    },
})