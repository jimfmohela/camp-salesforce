({
    doInit : function(cmp, event, helper) {
        var action = cmp.get("c.getAllEmployer");
        action.setParams({
           studId : cmp.get("v.studentID")           
        });
        
        action.setCallback(this, function(response){
            var responseState = response.getState();
        	if (responseState === "SUCCESS"){
                cmp.set("v.employers",response.getReturnValue());                
            }
        });
        $A.enqueueAction(action);
		
	},
    onClickMethod : function(component, event, helper) {        
       	helper.helperMethod(component,event,event.currentTarget.dataset.name);
    }
});