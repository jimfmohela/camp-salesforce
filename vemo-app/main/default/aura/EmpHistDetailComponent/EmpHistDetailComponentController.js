({
	doInit : function(cmp, event, helper) {
        
        var action = cmp.get("c.getEmplHistory");
        action.setParams({
           studId : cmp.get("v.studentID"),
           employer : cmp.get("v.employer")
        });
        
        action.setCallback(this, function(response){
            var responseState = response.getState();
        	if (responseState === "SUCCESS"){
				cmp.set("v.empHistoryList",response.getReturnValue().EmpHistoryList);
            }
        });
        $A.enqueueAction(action);
		
	},
    
    getDetail : function(cmp, event, helper) {
        
        window.open("/"+event.currentTarget.dataset.name);
		
	}
})