({
   onLoad: function(component, event) {
      //call apex class method
      var action = component.get('c.runReport');
      action.setCallback(this, function(response){
         //store state of response
         var state = response.getState();
          console.log(state);
         if (state === "SUCCESS") {
            //set response value in ListOfData attribute on component.
            component.set('v.ListOfData', response.getReturnValue());
            console.log(component.get('v.ListOfData'));
         }
      });
      $A.enqueueAction(action);
   },
    
   convertArrayOfObjectsToCSV : function(component,objectRecords){
        // declare variables
        var csvStringResult, counter, keys, columnDivider, lineDivider;
       
        // check if "objectRecords" parameter is null, then return from function
        if (objectRecords == null || !objectRecords.length) {
            return null;
         }
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
 
        // in the keys valirable store fields API Names as a key 
        // this labels use in CSV file header  
        keys = ['studentProgramNumber','vemoContactNumber','vemoAccountNumber','schoolName','programName','programNumber','status','primaryOwnerName','fundOwnerName',
               'paymentStreamOwnerName','applicationStartDate','submittedDate','certificationDate','servicingStartDate','majorPostCertificationName','gradeLevelPostCertification',
               'enrollmentStatusPostCertification','fundingAmountPostCertification','incomeSharePostCertification','paymentTermPostCertification',
               'paymentCapPostCertification','defermentMonthsAllowed','defermentMonthsRemaining','graceMonthsAllowed','graceMonthsRemaining',
               'paymentTermAssessed','paymentTermRemaining','minimumIncomePerMonth','cumulativePayments','lastMonthPayments','delinquencyBucket',
               'employmentHistoryName','employmentHistoryEmployer','employmentHistoryDateReported','employmentHistoryType','employmentHistoryStartDate',
               'employmentHistoryEndDate','employmentHistoryCatogery','incomePerMonth','beginDate','endDate'];
        
        csvStringResult = '';
        //csvStringResult += keys.join(columnDivider);
        csvStringResult += ['Student Program Number','Vemo Contract Number','Vemo Account Number','School Name','Program Name','Program Number',
                           'Status','Primary Owner','Fund Owner','Payment Stream Owner','Application Start Date','Submitted Date','Certification Date','Servicing Start Date',
                           'Major(Post-Certification)','Grade Level(Post-Certification)','Enrollment Status(Post-Certification)',
                           'Funding Amount(Post-Certification)','Income Share(Post-Certification)','Payment Term(Post-Certification)',
                           'Payment Cap(Post-Certification)','Deferment Months Allowed','Deferment Months Remianing','Grace Months Allowed',
                           'Grace Months Remaining','Payment Term Assessed','Payment Term Remaining','Minimum Income Per Month','Sum of Payments($)',
                           'Current Month Payment($)','Payment Status','Employment History Number','Employer Name','Date Reported',
                           'Type','Employment Start Date','Employment End Date','Category','Reported Income Per Month','Begin Date','End Date'];
        csvStringResult += lineDivider;
 
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
           
             for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
 
              // add , [comma] after every String value,. [except first]
                  if(counter > 0){ 
                      csvStringResult += columnDivider; 
                   }   
               
               if(objectRecords[i][skey] != undefined){
                   if(skey == 'applicationStartDate' || skey == 'submittedDate' || skey == 'certificationDate' || skey == 'servicingStartDate')
                   	csvStringResult += '"'+ $A.localizationService.formatDate(objectRecords[i][skey])+'"';
                   else
                       csvStringResult += '"'+ objectRecords[i][skey]+'"';
               }
               else{
                   csvStringResult += '"'+ '' +'"';
               } 
               
               counter++;
 
            } // inner for loop close 
             csvStringResult += lineDivider;
          }// outer main for loop close 
       
       // return the CSV formate String 
        return csvStringResult;        
    },
})