module.exports =  {
  verbose: true,
  testRegex: '\\.test\\.js',
  transform: {
    '^.+\\.jsx?$': 'babel-jest'
  },
}
