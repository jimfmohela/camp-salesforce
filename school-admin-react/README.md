#  Description


## Local Development & Testing

### Suggested workflow:

- Build your components in Storybook using mock data sets and structures.
- Once UI functionally is properly abstracted and defined, start hooking up API using the Localhost + INT environment; test for desired functionality against INT environment
-

#### Storybook

This runs the [Storybook](https://storybook.js.org/) application. Use this to quickly compose and test your components in an isolated environment. Should open on the localhost:6006 port.

```
yarn run storybook
```

#### Localhost

Runs the application on localhost:3000. This is not fully functional as the salesforce API is not available and will fail all API calls.  You will need to either proxy the API (not sure how to resolve the AUTH tokens) or provide your own redux store and render the application states individually.  The primary difference between this and storybook is that you should have application state, redux, and routing available, mirring INT as closely as possible while maintaining developer velocity.

```
yarn run start:dev
```

#### Localhost + INT environment
Hot reloading does not work using this method.

You could also do this against the QA environment or PROD environments, which could be useful for some bug fix or duplicating unique user case scenerios.

Talk to Kyle about how to configure Charles
```
 - yarn run start:dev
 - open INT environment in browser
 - map App.bundle.js & App.bundle.css back to local environment using Charles or other network proxy software
 - May need disable caching in the browser
```

---

## Application
### Structure
- pages define layout compose containers are composed of components with can compose each other
  ```
  pages <-- containers <-- components <-=-> components
  ```
- containers hook up application / business logic
- components could come from a 3rd party library such as Material UI

```
+-- pages
|  +-- PageName
|  |  +-- index.js
|   ¯¯¯
¯¯¯

+-- containers
|  +-- ConnectComponent
|  |  +-- index.js
|  |  +-- ConnectComponentChild
|  |  |  +-- index.js
|  |   ¯¯¯
|   ¯¯¯
¯¯¯

+-- components
|  +-- GenericComponent
|  |  +-- index.js
|   ¯¯¯
¯¯¯
```

## React Components

React Components are divided into three core sections:
  - Pages
  - Containers
  - Components

React Components should encapsulate their functionality & styles fully.  You should be able to copy and paste your entire component into another project with minimal side effects.
<br><small>* (Assuming common global variables)</small>

#### Pages
Logical compositions of Containers and/or components. Responsible for the general structuring of pages (aka templates). Should have minimal structure and code.

#### Containers
Typically "connected" to a data source and may contain state. Could render Containers or Components. Nest the child Containers inside of their parents. Business rules and application specific logic should happen here as well as handling redux/hooks actions/state.

#### Components
Individual pieces that accept data from pages. Responsible for
styling and passing callbacks and action firings up to parent. Components should
NOT alter Redux or hooks data (use a container).  Enforce the components API through PropTypes.

- CamelCase your components
- Related components use the same pattern
- Follow ABEM naming convention for child tags
- Some components may be just wrappers for other components

```
+-- ComponentName
|  +-- index.js
|  +-- styles.scss || styles.jss
|  +-- index.stories.js
|
|  <Optional>
|  +-- images
|  +-- tests
|  +-- mocks
|  +-- test.js
|  +-- mock.data.js
¯¯¯
```

## Styles
Styles for each component are set and stored with the component /Component/style.scss.  These are imported by the component and the styles should be scoped to the component.  If a component's presentational styles are manipulated from a parent or sibling, whichever component would be used first in the ruleset should contain the code, typically this is the parent component and usually related to layout & spacing concerns.

Do NOT set colors & fonts (or font properties). directly.  These should come from the global mixin's that are derived from our style guide and will allow us to follow sematic document structures while achieving the presentational outcome desired.

If the style guide does not have an appropriate scenerio defined that is a conversation that should probably take place.  Either the style guide is too limited or the components presentational design has deviated too far from the design specs and needs to re-examined.

Ideally our common design patterns and UI elements will all be encapsulated in simple components that can be imported as needed.

## Store
```
Store configuration is done through a typical actions + reducers setup
```

## Configuration
```
Primary configurations (webpack, babel, linters) can be found in the config
folder.

This does not exclude other elements of configurations outside the folder, such
as .env and flow
```

## Testing
```
(NOT FUNCTIONING AT THE MOMENT)
----------
¯\_(ツ)_/¯

Testing is currently done a unit basis utilizing jest. Any files meant for
testing should be suffixed with *.test.js; Any configuration files should be
located at the root of the __tests__ folder
```

<!--
# React Front-end Structure
Base front-end structure for Vemo Portals

What is included:
```
1. React (includes: Flow, Protected Routes)
2. Redux (Initial setup in store)
3. Service Workers
4. Atomic Design (SASS/CSS styling)
5. Testing (Jest) INCOMPLETE -- awaiting Jest 23 for Babel 7 upgrade
6. Minimal webpack configuration with SOC (Development & Production)
7. Priv keys stored in .env
``` -->

<!-- # Setup
Follow the instructions below to get a basic front-end setup running

```
1. Clone Repo
2. Run `touch .env` and copy input from `.env.example` into it
3. Run `yarn install`
4. Run `npm start` and open browser to port 8000
```

If you want to use the proxy
```
Webpack setup is ready to proxy a server. All you need is to make sure your server url is in the .env file under API_URL.

If you need to hit additional proxy endpoints, add any and all endpoints inside proxy.context in the dev.config.js file
``` -->

<!-- ## PWA Functionality
```
TBD
``` -->

<!-- ## Docker

*It is required that you have an internet connection for the steps below. 2018,
duh!*

0. Install Docker for Mac.
1. Ensure you have the latest from the `vemo.school_portal_client` repository
and the `vemo.school_portal_api` repository. Both contain files necessary for
Docker Compose to operate correctly.
2. Have a look at `.env.example`. You'll notice environment variable
declarations starting with `COMPOSE_`. Ensure you set these in your own `.env`
variable at the root of the project. The values shown in `.env.example` should
work fine. You can play with `COMPOSE_PROJECT_NAME` to rename the `networks` and
`volumes` names that `docker-compose` creates, but leave the value of
`COMPOSE_FILE` intact, unless we reorganize the folder structure of the project.
    * **Nota bene:** don't use quotes to flank the variable's value, unless you
    want the quotes themselves to be part of the value.
    * You must at least declare `COMPOSE_FILE` in your `.env` file, or stuff
    won't work.
2. Run this from the project's root: `docker-compose up`.
3. The API is on port `3030`. The client is on port `8000`. Both are accessible
on `localhost`.

### Coming soon:

1. Named local development URLs and subdomain support.
2. Fully baked production setup deploying to AWS.
3. Cleanup of images using `.dockerignore`.
3. Fully baked test setup on CI.

*Please tell Ryan right away if you have any trouble and he'll fix it and get it
documented faster than two shakes of a lamb's tail. (?)* -->

## Third-party Libraries (Too watch for maintenance)
- react-aria-menubutton
- react-animated-number
- react-paginate (This number is 0 indexed, so that might get you)
