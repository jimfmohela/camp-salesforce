import React, { createContext, useState } from 'react'
import PropTypes from 'prop-types'

export const AppContext = createContext()

export function AppContextProvider({ isDevelopment, baseRoute: _baseRoute, children }) {
  const [isDev] = useState(isDevelopment)
  const [baseRoute] = useState(_baseRoute)
  const [isMobile, updateIsMobileState] = useState(false)
  const [activeSidebar, updateActiveSidebar] = useState(false)

  return (
    <AppContext.Provider
      value={{
        activeSidebar,
        baseRoute,
        isDev,
        isMobile,
        updateActiveSidebar,
        updateIsMobileState
      }}
    >
      {children}
    </AppContext.Provider>
  )
}

AppContextProvider.defaultProps = {
  isDevelopment: false
}

AppContextProvider.propTypes = {
  isDevelopment: PropTypes.bool,
  baseRoute: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
}
