import theme from '../theme'

const statusColors = (status) => {
  let color
  let textColor
  let icon
  let cursor
  let backgroundColor= theme.palette.background.default

  switch (status) {
    case 'disabled':
      color = theme.palette.action.disabled
      break
    case 'primary':
      color = theme.palette.primary.main
      break
    case 'secondary':
      color = theme.palette.secondary.main
      break
    case 'success':
      color = theme.palette.text.success
      textColor = theme.palette.text.primary
      icon = 'check'
      break
    case 'warning':
      color = theme.palette.warning.main
      textColor = theme.palette.warning.main
      icon = 'warning-fill'
      break
    case 'error':
      color = theme.palette.error.main
      textColor = theme.palette.text.primary
      icon = 'error-line'
      break
    case 'textSecondary':
      color = theme.palette.text.secondary
      break
    default:
      color = 'inherit'
  }

  return { color, textColor, icon, backgroundColor, cursor }
}

export default statusColors
