import moment from 'moment';

export const formatToYYYYMMDD = (date) => {

    if(date){
      return moment(date).format('YYYY/MM/DD');
    } else{
      return '';
    }
    
  };

  export const formatDateToMMDDYYYY=(date)=>{
    if(date){
      return moment(date).format('MM/DD/YYYY');
    }else{
       return '';
    }
    
  }
  