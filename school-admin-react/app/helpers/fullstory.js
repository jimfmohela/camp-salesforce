import React, { isValidElement } from 'react'
import cn from 'classnames'

// Caveat, this will not work with connected components and prevents render comparisions as
// each clone is technically a new instance.
// You'll want utilize at the deepest node possible for maximum props,
// you may need to implement differently if your render tree has advanced behaviors.

const maskFS = (Component, useWrapper = false) => {
  if (
    useWrapper &&
    (isValidElement(Component) ||
      typeof Component === 'string' ||
      typeof Component === 'number')
  ) {
    return maskFS(
      <span style={{ display: 'inline-table', margin: 0 }}>{Component}</span>
    )
  }

  if (isValidElement(Component)) {
    return React.cloneElement(Component, {
      className: cn('fs-block', Component.props.className)
    })
  }

  // eslint-disable-next-line no-console
  console.warn(`Unable to mask [${typeof Component}]`)
  return Component
}

const noop = null

export { maskFS, noop }
