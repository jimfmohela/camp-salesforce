export const AmountFormat =(amt)=>{
    // needed when decimal value remove from the currency
    //new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD',minimumFractionDigits: 0,maximumFractionDigits: 0 }).format(1234567)
    
    if(amt!=undefined){
        const amount=  new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(amt)
        return amount;
    }else{
        return '$0.00';
    }
   
   
   }