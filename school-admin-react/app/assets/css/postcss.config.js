const supportedBrowsers = ['last 2 versions']

module.exports = {
  plugins: {
    'autoprefixer': { browsers: supportedBrowsers },
    'cssnano': {}
  }
}
