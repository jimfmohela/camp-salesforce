const Common = {
  sortDefault: (a, b, desc = true) => {
    a = a === null || a === undefined ? '' : a
    b = b === null || b === undefined ? '' : b
    a = typeof a === 'string' ? a.toLowerCase() : a
    b = typeof b === 'string' ? b.toLowerCase() : b

    if (desc) {
      if (a > b) return 1
      if (a < b) return -1
    } else {
      if (a > b) return -1
      if (a < b) return 1
    }

    return 0
  },

  isEmpty: (obj) => !obj || Object.keys(obj).length === 0,

  AmountFormat: (amt) =>
    // needed when decimal value remove from the currency
    // new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD',minimumFractionDigits: 0,maximumFractionDigits: 0 }).format(1234567)
    (amt !== undefined ? (new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD'
    }).format(amt)) : '$0.00'),
  SeparateAmountCent: (amt) => (amt ? new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
  }).format(amt).substr(1).split('.') : null),
  phoneNumberFormat: (num) => {
    // thie phoneNumberFormat func is deprecated
    const reg = /\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})/g
    const newFormat = '($1)-$2-$3'
    const phone = num ? num.replace(reg, newFormat) : null
    return phone
  },
  formatPhoneNumber: (num) => {
    if (!num) {
      return 'N/A'
    }
    num = num.toLowerCase().split('ext')[0].replace(/,\s*$/, '').trim()
    const reg = /\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})/g
    const newFormat = '($1)-$2-$3'
    const phone = num ? num.replace(reg, newFormat) : null
    return phone
  },
  orderByDescDate: (arr, key_name) => arr.sort((cur_item, next_item) => new Date(next_item[key_name]) - new Date(cur_item[key_name]))
}

export default Common
