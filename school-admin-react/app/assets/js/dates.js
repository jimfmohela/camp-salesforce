const Dates = {
  getYear: () => {
    return new Date().getFullYear();
  },

  formatToMMDDYY: date => {
    let newDate = new Date(date)

    return newDate.toLocaleDateString('en-US', {
      month: '2-digit',
      day: '2-digit',
      year: '2-digit',
      timeZone: 'UTC'
    })
  },
  formatToMMDDYYYY: date => {
    let newDate = new Date(date)
    if(date){
       return newDate.toLocaleDateString('en-US', {
         month: '2-digit',
         day: '2-digit',
         year: 'numeric',
         timeZone: 'UTC'
      })
    }else{
       return 'N/A';
    }
  },
  DiffInDays: date=>{
    let date1 = new Date(date);
    let currentDate= new Date();
    var timeDiff = currentDate.getTime() - date1.getTime();
     return(Math.floor(timeDiff / (1000 * 3600 * 24)));
  },
  DateFormat:(date,format)=>{
    let newDate = new Date(date)
    if(date){
       return newDate.toLocaleDateString(format, {
         month: '2-digit',
         day: '2-digit',
         year: 'numeric',
         // timeZone: 'UTC'
      })
    }else{
       return '';
    }
  }
};

export default Dates;
