const getUnencodedQueryString = (obj) =>
  Object
    .keys(obj)
    .map(key => (`${key}=${JSON.stringify(obj[key])}`))
    .join('&')
    .replace(/"/g,'')

export const CallVemoAPI = async (method, url, entity, params) => {

  // if (process.env.NODE_ENV === 'production') {
    return await new Promise((resolve, reject) => {
      inlineApexAdaptor.Visualforce.remoting.Manager.invokeAction(
        inlineApexAdaptor.vemoAPI,
        method,
        url,
        JSON.stringify(entity),
        getUnencodedQueryString({...params}),
        (result, event) => {
          if (event.statusCode >= 200 && event.statusCode < 400) {
            const response = JSON.parse(result);
            resolve(!response.result.length ? null : response.result)
          } else if ( event.statusCode == 402 || (event.statusCode == 400 && event.type === 'exception' && url === '/vemo/v2/eventinstance') ) {
            window.location = `${window.baseRoute}/SchoolAdminSiteLogin`
          } else if (event.type === 'exception') {
            reject(event);
          }else {
            reject(event.message);
          }
        },
        { escape: false }
      )
    })
  // }
  // else {
  //   return new Promise((resolve, reject)=>{
  //     reject('salesforce api only available in production')
  //   })
  // }
}

export default CallVemoAPI
