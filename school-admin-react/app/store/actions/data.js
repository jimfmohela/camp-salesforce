import CallVemoApi from '~/app/store/salesforce'
import {
  LOAD__AGREEMENTS_BY_ID,
  LOAD__AGREEMENT_MAJOR,
  FETCH__AGREEMENT,
  LOAD__AGREEMENT,
  FETCH__AGREEMENTS,
  LOAD__AGREEMENTS,
  LOADED__AGREEMENTS,
  LOADED__STUDENTS,
  FETCH__DASHBOARD,
  LOAD__DASHBOARD,
  FETCH__PROGRAMS,
  LOAD__PROGRAMS,
  FETCH__PROGRAM,
  LOAD__PROGRAM,
  FETCH__STUDENTS,
  LOAD__STUDENTS,
  FETCH__STUDENT,
  LOAD__STUDENT,
  LOAD__NOTIFICATIONS_DATA,
  FETCH__COUNT_DATA,
  LOAD__COUNT_DATA,
  FETCH__COMMUNICATIONS_HISTORY_DATA,
  LOAD__COMMUNICATIONS_HISTORY_DATA,
  LOAD__COUNT_TOTALS,
  LOAD_EVENT_TYPE_DATA,
  LOAD_NOTIFICATION_SETTING_DATA,
  UPDATE_NOTIFICATION_SETTING,
  FETCH__AGREEMENTS_V1,
  LOAD__AGREEMENTS_V1,
  LOADED__AGREEMENTS_V1,
  FETCH__FEES,
  LOADED__FEES,
  LOAD__FEES,
  FETCH__INCOME_VERIFICATION,
  LOADED__INCOME_VERIFICATION,
  LOAD__INCOME_VERIFICATION,
  FETCH_STUDENT_V1,
  LOADED__STUDENT_V1,
  LOAD_STUDENT_COUNT,
  FETCH__STUDENT_COUNT_DATA,
  LOAD__STUDENT_V1,
  FETCH__ENROLLMENT_STATUS,
  LOADED__ENROLLMENT_STATUS,
  LOAD__ENROLLMENT_STATUS,
  LOAD_PAYMENT_HISTORY,
  FORM_SUBMITTED,
  LOAD__SCHOOL_DATA,
  FETCH__SCHOOL
} from '~/app/store/constants'
import Common from "~/app/assets/js/common";

const isDevelopment = process.env.NODE_ENV === 'development'

// Sort closed/cancelled items to the bottom of the list
const sortArchivedData = (data, status) => {
  return data
    ? data.reduce((acc, element) => {
        return /(closed|cancelled)/i.test(element[status])
          ? [...acc, element]
          : [element, ...acc]
      }, [])
    : null
}

//Sort Notification Setting data

const sortNotificationSetting = (data) => {
  return Object.keys(data)
    .sort()
    .reduce(
      (acc, key) => ({
        ...acc,
        [key]: data[key]
      }),
      {}
    )
}

const addAgreementsTotalToPrograms = (programs, agreements) => {
  return programs
    ? programs.map((item) => {
        return { ...item, agreementsTotal: agreements[item.programID] }
      })
    : null
}

export function loadAgreementsByID(data) {
  return {
    type: LOAD__AGREEMENTS_BY_ID,
    agreementsByID: data
  }
}
export function loadAgreementMajor(data) {
  return {
    type: LOAD__AGREEMENT_MAJOR,
    agreementMajorData: data
  }
}
export function fetchingAgreement(isFetching) {
  return {
    type: FETCH__AGREEMENT,
    isFetching
  }
}
export function loadAgreement(data) {
  return {
    type: LOAD__AGREEMENT,
    agreementData: data
  }
}
export function fetchingAgreements(isFetching) {
  return {
    type: FETCH__AGREEMENTS,
    isFetching
  }
}
export function loadAgreements(data) {
  return {
    type: LOAD__AGREEMENTS,
    agreementsData: data
  }
}
export function loadedAgreements(isLoaded) {
  return {
    type: LOADED__AGREEMENTS,
    isLoaded
  }
}
export function loadedStudents(isLoaded) {
  return {
    type: LOADED__STUDENTS,

    isLoaded
  }
}
export function fetchingDashboard(bool) {
  return {
    type: FETCH__DASHBOARD,
    isFetching: bool
  }
}
export function loadDashboard(data) {
  return {
    type: LOAD__DASHBOARD,
    dashboardData: data
  }
}
export function fetchingPrograms(isFetching) {
  return {
    type: FETCH__PROGRAMS,
    isFetching
  }
}
export function loadPrograms(data) {
  return {
    type: LOAD__PROGRAMS,
    programsData: data
  }
}
export function fetchingProgram(bool) {
  return {
    type: FETCH__PROGRAM,
    isFetching: bool
  }
}
export function loadProgram(data) {
  return {
    type: LOAD__PROGRAM,
    programData: data
    // programAgreementsData: data.agreements
  }
}
export function fetchingStudent(isFetching) {
  return {
    type: FETCH__STUDENT,
    isFetching
  }
}
export function loadStudentData(data) {
  return {
    type: LOAD__STUDENT,
    studentData: data
  }
}
export function fetchingStudents(isFetching) {
  return {
    type: FETCH__STUDENTS,
    isFetching
  }
}
export function loadStudentsData(data) {
  return {
    type: LOAD__STUDENTS,
    studentsData: data
  }
}
export function loadNotificationsData(data) {
  return {
    type: LOAD__NOTIFICATIONS_DATA,
    notificationsData: data
  }
}
export function fetchCountData(bool) {
  return {
    type: FETCH__COUNT_DATA,
    isFetching: bool
  }
}
export function loadCountData(data) {
  return {
    type: LOAD__COUNT_DATA,
    countData: data
  }
}
export function loadCountsTotal(data) {
  return {
    type: LOAD__COUNT_TOTALS,
    countTotals: data
  }
}

export function fetchingCommunicationHistoryData(isFetching) {
  return {
    type: FETCH__COMMUNICATIONS_HISTORY_DATA,
    isFetching
  }
}
export function loadCommunicationHistoryData(data) {
  return {
    type: LOAD__COMMUNICATIONS_HISTORY_DATA,
    communicationHistoryData: data
  }
}

/*export function loadEventTypeData(data){
   return{
     type:LOAD_EVENT_TYPE_DATA,
     eventTypeData:data
   }
}*/
export function loadEventSubscriptionData(data) {
  return {
    type: LOAD_NOTIFICATION_SETTING_DATA,
    notificationSettingData: data
  }
}
export function loadStudentCount(data) {
  return {
    type: LOAD_STUDENT_COUNT,
    studentCountData: data
  }
}

export function fetchStudentCountData(bool) {
  return {
    type: FETCH__STUDENT_COUNT_DATA,
    isFetching: bool
  }
}

/*export function updateNotificationSetting(data){
   return{
     type:UPDATE_NOTIFICATION_SETTING,
     notificationSettingData:data
   }
}*/

/*export function loadNotificationSetting(){
     return{
         type:
     }
}*/

export function fetchAgreementMajor(majorID) {
  return async function(dispatch) {
    await CallVemoApi('GET', '/vemo/v2/programofstudy', null, {
      schoolProgramOfStudyID: majorID
    })
      .then((res) => dispatch(loadAgreementMajor(res[0])))
      .catch((err) => {
        dispatch(
          loadAgreementMajor({
            definition: '',
            description: '',
            programOfStudyID: '',
            schoolID: '',
            schoolProgramOfStudyID: ''
          })
        )
        return err
      })
      .finally(() => {})
  }
}
export function fetchAgreement(agreementID) {
  return async function(dispatch) {
    dispatch(fetchingAgreement(true))

    return await CallVemoApi('GET', '/vemo/v2/agreement', null, { agreementID })
      .then((res) => dispatch(loadAgreement(res[0])))
      .catch((err) => console.log('Fetching agreements error:', err))
      .finally(() => dispatch(fetchingAgreement(false)))
  }
}

export function fetchAgreements(schoolID, pageNumber, status = 'null') {
  return async function(dispatch) {
    dispatch(fetchingAgreements(true))

    status = encodeURIComponent(status)
    return await CallVemoApi('GET', '/vemo/v2/agreement', null, {
      schoolID,
      page: pageNumber,
      pageSize: '50',
      status
    })
      .then((res) => sortArchivedData(res, 'agreementStatus'))
      .then((res) => dispatch(loadAgreements(res)))
      .then((res) => dispatch(loadedAgreements(true)))
      .catch((err) => console.log('Fetching agreements error:', err))
      .finally(() => dispatch(fetchingAgreements(false)))
  }
}

// For payment screen

//Student Agreements
export function fetchStudentAgreements_V1(studentID) {
  return async function(dispatch) {
    dispatch(fetchingStudentAgreements_V1(true))
    return CallVemoApi('GET', `/vemo/v1/agreement`, null, {
      studentID: studentID
    })
      .then((res) => dispatch(loadStudentAgreements_V1(res)))
      .then((res) => dispatch(loadedStudentAgreements_V1(true)))
      .catch((err) => console.error('Fetching Student Agreements error:', err))
      .finally(() => dispatch(fetchingStudentAgreements_V1(false)))
  }
}

export function fetchingStudentAgreements_V1(isFetching) {
  return {
    type: FETCH__AGREEMENTS_V1,
    isFetching
  }
}

export function loadedStudentAgreements_V1(isLoaded) {
  return {
    type: LOADED__AGREEMENTS_V1,
    isLoaded
  }
}

export function loadStudentAgreements_V1(data) {
  return {
    type: LOAD__AGREEMENTS_V1,
    agreementsDataV1: data
  }
}

// Fees
export function fetchStudentFees(studentID) {
  return async function(dispatch) {
    dispatch(fetchingStudentFees_V1(true))
    return CallVemoApi('GET', `/vemo/v1/fee`, null, { customerID: studentID })
      .then((res) => dispatch(loadStudentFees_V1(res)))
      .then((res) => dispatch(loadedStudentFees_V1(true)))
      .catch((err) => console.error('Fetching Student Fees error:', err))
      .finally(() => dispatch(fetchingStudentFees_V1(false)))
  }
}
export function fetchingStudentFees_V1(isFetching) {
  return { type: FETCH__FEES, isFetching }
}
export function loadedStudentFees_V1(isLoaded) {
  return { type: LOADED__FEES, isLoaded }
}
export function loadStudentFees_V1(data) {
  return { type: LOAD__FEES, studentFees: data }
}

// Income verification
export function fetchStudentIncomeVerification(studentID) {
  return async function(dispatch) {
    dispatch(fetchingStudentIncomeVerification_V1(true))
    return CallVemoApi('GET', `/vemo/v1/incomeverification`, null, {
      studentID: studentID,
      latest: true
    })
      .then((res) => dispatch(loadStudentIncomeVerification_V1(res)))
      .then((res) => dispatch(loadedStudentIncomeVerification_V1(true)))
      .catch((err) =>
        console.error('Fetching Student Income Verification error:', err)
      )
      .finally(() => dispatch(fetchingStudentIncomeVerification_V1(false)))
  }
}
export function fetchingStudentIncomeVerification_V1(isFetching) {
  return { type: FETCH__INCOME_VERIFICATION, isFetching }
}
export function loadedStudentIncomeVerification_V1(isLoaded) {
  return { type: LOADED__INCOME_VERIFICATION, isLoaded }
}
export function loadStudentIncomeVerification_V1(data) {
  return { type: LOAD__INCOME_VERIFICATION, studentIncomeVerfication: data }
}

//StudentV1 data
export function fetchStudentV1(studentID) {
  return async function(dispatch) {
    dispatch(fetchingStudent_V1(true))
    return CallVemoApi('GET', `/vemo/v1/student`, null, {
      studentID: studentID
    })
      .then((res) => dispatch(loadStudent_V1(res)))
      .then((res) => dispatch(loadedStudent_V1(true)))
      .catch((err) => console.error('Fetching Student_V1 error:', err))
      .finally(() => dispatch(fetchingStudent_V1(false)))
  }
}
export function fetchingStudent_V1(isFetching) {
  return { type: FETCH_STUDENT_V1, isFetching }
}
export function loadedStudent_V1(isLoaded) {
  return { type: LOADED__STUDENT_V1, isLoaded }
}
export function loadStudent_V1(data) {
  return { type: LOAD__STUDENT_V1, studentv1: data }
}

//END

//StudentV1 data create
export function updateStudentV1(data) {
  return async function(dispatch) {
    dispatch(creatingStudent_V1(true))
    return CallVemoApi('PUT', `/vemo/v1/student`, data)
      .then((res) => {
        console.log('????!!!!!!!', res, '*****', data)
        return dispatch(updateStudent_V1(data))
      })
      .then((res) => dispatch(updatedStudent_V1(true)))
      .catch((err) => console.error('Creating Student_V1 error:', err))
      .finally(() => dispatch(creatingStudent_V1(false)))
  }
}
export function creatingStudent_V1(isFetching) {
  return { type: FETCH_STUDENT_V1, isFetching }
}
export function updatedStudent_V1(isLoaded) {
  return { type: LOADED__STUDENT_V1, isLoaded }
}
export function updateStudent_V1(data) {
  return { type: LOAD__STUDENT_V1, studentv1: data }
}

//END

export function fetchDashboard(schoolID) {
  const draft = /\b(draft)/i
  const invited = /\b(invited)/i
  const cancelled = /\b(cancelled)/i
  const applicationIncomplete = /\b(incomplete)/i
  const applicationUnderReview = /\b(review)/i
  const applicationComplete = /\b(complete)/i
  const certified = /\b(certified)/i
  const partiallyFunded = /\b(partially)/i
  let dashboard = {
    draft: 0,
    invited: 0,
    cancelled: 0,
    applicationIncomplete: 0,
    applicationUnderReview: 0,
    applicationComplete: 0,
    certified: 0,
    partiallyFunded: 0
  }
  // const inProgress = /\b(invited|draft|review|incomplete|complete)/i
  // const underReview = /\b(review)/i
  // const pendingCertification = /\b(complete)/i
  // const pendingDisbursement = /\b(certified|partially)/i
  // const cancelled = /\b(cancelled)/i
  // let dashboard = {
  //   inProgress: 0,
  //   cancelled: 0,
  //   dashboard: 0,
  //   underReview: 0,
  //   pendingCertification: 0,
  //   pendingDisbursement: 0,
  //   promisedFunding: 0,
  // }

  return async function(dispatch, getState) {
    dispatch(fetchingDashboard(true))

    let countData = getState().data.countData

    ;(await countData.count)
      ? countData.count
          .map((item) => item.statusCountList)
          .flat()
          .reduce((acc, item) => {
            draft.test(item.status) ? (acc.draft += item.count) : null

            invited.test(item.status) ? (acc.invited += item.count) : null

            cancelled.test(item.status) ? (acc.cancelled += item.count) : null

            applicationIncomplete.test(item.status)
              ? (acc.applicationIncomplete += item.count)
              : null

            applicationUnderReview.test(item.status)
              ? (acc.applicationUnderReview += item.count)
              : null

            applicationComplete.test(item.status)
              ? (acc.applicationComplete += item.count)
              : null

            certified.test(item.status) ? (acc.certified += item.count) : null

            partiallyFunded.test(item.status)
              ? (acc.partiallyFunded += item.count)
              : null

            return acc
          }, dashboard)
      : null
    // Total the subtotals of status. Total amount not disbursed yet in pendingDisbursement.
    // await countData.count ?
    //   countData.count
    //     .map(item => item.statusCountList)
    //     .flat()
    //     .reduce((acc, item) => {
    //       switch(true) {
    //         case inProgress.test(item.status):
    //           acc.inProgress += item.count
    //           break;
    //         case cancelled.test(item.status):
    //           acc.cancelled += item.count
    //           break;
    //         case underReview.test(item.status):
    //           acc.underReview += item.count
    //           break;
    //         case pendingCertification.test(item.status):
    //           acc.pendingCertification += item.count
    //           break;
    //         case pendingDisbursement.test(item.status):
    //           acc.pendingDisbursement += item.count
    //           acc.promisedFunding += (item.amountCertified - item.amountDisbursed)
    //           break;
    //       }

    //       return acc
    //     }, dashboard) : null

    await dispatch(loadDashboard(dashboard))
    return await dispatch(fetchingDashboard(false))
  }
}

export function fetchProgram(programID, schoolID) {
  return async function(dispatch, getState) {
    dispatch(fetchingProgram(true))

    let agreementsByProgramID = await dispatch(
      fetchAgreementsByProgram(programID)
    )
    await dispatch(fetchPrograms(schoolID))

    let programs = getState().data.programsData.data
    let program = programs.find((program) => program['programID'] == programID)

    // Sort agreements array inside program object
    let sortedProgram = {
      ...program,
      agreements: sortArchivedData(agreementsByProgramID, 'agreementStatus')
    }

    await dispatch(loadProgram(sortedProgram))
    return dispatch(fetchingProgram(false))
  }
}

export function fetchAgreementsByProgram(programID) {
  return async function(dispatch) {
    return await CallVemoApi('GET', '/vemo/v2/agreement', null, { programID })
      .then((res) => res)
      .catch((err) => console.log('Fetching agreements error:', err))
      .finally(() => {})
  }
}

export function fetchPrograms(schoolID) {
  return async function(dispatch, getState) {
    dispatch(fetchingPrograms(true))

    let agreementsByPrograms = {}
    let countData = getState().data.countData

    // Try to avoid nested data structures because this is what happens
    ;(await countData.count)
      ? countData.count.map((item) =>
          item.statusCountList.reduce((acc, status) => {
            acc[item.programID]
              ? (acc[item.programID] += status.count)
              : (acc[item.programID] = status.count)

            return acc
          }, agreementsByPrograms)
        )
      : null

    return await CallVemoApi('GET', '/vemo/v2/program', null, {
      schoolID,
      pageSize: '500'
    })
      .then((res) => addAgreementsTotalToPrograms(res, agreementsByPrograms))
      .then((res) => sortArchivedData(res, 'programStatus'))
      .then((res) => dispatch(loadPrograms(res)))
      .catch((err) => console.log('Fetching program error:', err))
      .finally(() => dispatch(fetchingPrograms(false)))
  }
}

export function fetchStudentData(studentID) {
  return async function(dispatch) {
    dispatch(fetchingStudent(true))
    return await CallVemoApi('GET', '/vemo/v2/academicenrollment', null, {
      studentID
    })
      .then((res) => dispatch(loadStudentData(res)))
      .catch((err) => console.log('Fetching students error:', err))
      .finally(() => dispatch(fetchingStudent(false)))
  }
}

// ProviderID is schoolID cause academicenrollment model
export function fetchStudentsData(providerID, page, status = 'null') {
  return async function(dispatch) {
    dispatch(fetchingStudents(true))
    status = encodeURIComponent(status)
    return await CallVemoApi('GET', '/vemo/v2/academicenrollment', null, {
      providerID,
      page,
      pageSize: '50',
      status
    })
      .then((res) => dispatch(loadStudentsData(res)))
      .then((res) => dispatch(loadedStudents(true)))
      .catch((err) => console.log('Fetching students error:', err))
      .finally(() => dispatch(fetchingStudents(false)))
  }
}

export function fetchNotificationsData() {
  return function(dispatch) {
    return CallVemoApi('GET', '/vemo/v2/eventinstance', null, {
      latestOnly: true
    })
      .then((res) => {
        dispatch(loadNotificationsData(res))
      })
      .catch((err) => console.log('Fetching notifications error:', err))
      .finally(() => {})
  }
}

// Hide schoolID

export function fetchCountsData(_schoolID) {
  return async function(dispatch, getState) {
    const schoolID = _schoolID || getState().auth.adminData.schoolID

    dispatch(fetchCountData(true))
    await CallVemoApi('GET', '/vemo/v2/programCount', null, { schoolID })
      .then((res) => {
        dispatch(loadCountData(res))
      })
      .catch((err) => {
        console.warn('Fetching counts error:', err)
        dispatch(() => {})
      })
      .finally(() => dispatch(fetchCountData(false)))

    return false
  }
}

// fetch count data for student

export function fetchCountStudentData(schoolID) {
  return async function(dispatch) {
    dispatch(fetchStudentCountData(true))
    await CallVemoApi('GET', '/vemo/v2/academicEnrollmentCount', null, {
      schoolID
    })
      .then((res) => {
        var inProgress = /\b(full|half|quarter)/i
        var approve = /\b(graduated)/i
        var data = { 'in-school': 0, graduated: 0, all: 0 }

        res.map((filter) => {
          data['all'] += filter.count
          switch (true) {
            case inProgress.test(filter.status):
              data['in-school'] += filter.count
              break
            case approve.test(filter.status):
              data['graduated'] += filter.count
              break
            default:
              null
              break
          }
        })

        dispatch(loadStudentCount(data))
      })
      .catch((err) => console.log('Fetching counts error:', err))
      .finally(() => dispatch(fetchStudentCountData(false)))
  }
}
export function fetchCountsTotal(schoolID) {
  return async function(dispatch) {
    await CallVemoApi('GET', '/vemo/v2/studentProgramCount', null, { schoolID })
      .then((res) => dispatch(loadCountsTotal(res)))
      .catch((err) => console.log('Fetching agreements counts error:', err))
      .finally(() => {})
  }
}

export function fetchCommunicationHistoryData(studentID) {
  return function(dispatch) {
    dispatch(fetchingCommunicationHistoryData(true))

    return CallVemoApi('GET', '/vemo/v2/communicationHistory', null, {
      type: 'email',
      studentID
    })
      .then((res) => dispatch(loadCommunicationHistoryData(res)))
      .catch((err) => console.log('Fetching communication history error:', err))
      .finally(() => dispatch(fetchingCommunicationHistoryData(false)))
  }
}

export function fetchDownloadData(schoolID) {
  return function(dispatch) {
    return CallVemoApi('GET', '/vemo/v2/agreement', null, {
      schoolID: schoolID,
      page: 1,
      pageSize: '50000'
    })
      .then((res) => dispatch(loadDownloadData(res)))
      .catch((err) => {
        console.log('Fetching agreements error:', err)
      })
  }
}
//Created by Payal, To fetch eventType info for notification's setting section
export function fetchNotificatonSettingData() {
  return function(dispatch) {
    return CallVemoApi('GET', '/vemo/v2/eventType', null, null)
      .then((res) => dispatch(fetchEventSubscriptionData(res)))
      .catch((err) => {
        console.log('Fetching event type error:', err)
      })
  }
}

export function fetchEventSubscriptionData(eventType) {
  var obj = {}
  return function(dispatch) {
    return CallVemoApi('GET', '/vemo/v2/eventSubscription', null, null)
      .then((eventsub) => {
        eventType.map(function(event) {
          if (eventsub && eventsub.length) {
            eventsub.map(function(subscription) {
              if (event['eventTypeID'] === subscription['eventType']) {
                obj[event['name']] = {
                  eventType: event['eventTypeID'],
                  onVemo: true,
                  email: subscription['email'],
                  eventSubscriptionID: subscription['eventSubscriptionID']
                }
              }
            })
          } else {
            obj[event['name']] = {
              eventType: event['eventTypeID'],
              onVemo: false,
              email: false,
              eventSubscriptionID: null
            }
          }

          if (!obj[event['name']]) {
            obj[event['name']] = {
              eventType: event['eventTypeID'],
              onVemo: false,
              email: false,
              eventSubscriptionID: null
            }
          }
          //obj[event['name']]={'id':event['eventTypeID'],'onVemo':false,'onEmail':false};
        })
      })
      .then(() => {
        console.log('before sorting --------', obj)
        const unsortednotificationData = sortNotificationSetting(obj)
        return unsortednotificationData
      })
      .then((unsortednotificationData) => {
        dispatch(loadEventSubscriptionData(unsortednotificationData))
      })
      .catch((err) => {
        console.log('Fetching event type error:', err)
      })
  }
}

export function createNotification(
  clickedStatusData,
  AllStatuses,
  eventTypeName
) {
  return function(dispatch) {
    return CallVemoApi(
      'POST',
      '/vemo/v2/eventSubscription',
      clickedStatusData,
      null
    )
      .then((res) => {
        AllStatuses[eventTypeName].eventSubscriptionID = res[0]
        dispatch(loadEventSubscriptionData(AllStatuses))
      })
      .catch((err) => {
        console.log('create event subscription error:', err)
      })
  }
}

export function deleteNotification(
  clickedStatusData,
  AllStatuses,
  eventTypeName
) {
  return function(dispatch) {
    return CallVemoApi('DELETE', '/vemo/v2/eventSubscription', null, {
      eventSubscriptionID: clickedStatusData.eventSubscriptionID
    })
      .then((res) => {
        AllStatuses[eventTypeName].eventSubscriptionID = null
        dispatch(loadEventSubscriptionData(AllStatuses))
      })
      .catch((err) => {
        console.log('delete event subscription error:', err)
      })
  }
}

export function updateNotification(clickedStatusData, AllStatuses) {
  return function(dispatch) {
    return CallVemoApi(
      'PUT',
      '/vemo/v2/eventSubscription',
      clickedStatusData,
      null
    )
      .then((res) => dispatch(loadEventSubscriptionData(AllStatuses)))
      .catch((err) => {
        console.log('Update event subscription error:', err)
      })
  }
}
// end notification setting
//.... read a notitifcation ....
export function readNotification(dataToDelete, allRecord) {
  var data = []
  data.push(dataToDelete)
  return function(dispatch) {
    return CallVemoApi('PUT', '/vemo/v2/eventinstance', data, null)
      .then((res) => {
        console.log('notification read ', res)
        dispatch(loadNotificationsData(allRecord))
      })

      .catch((err) => console.log('Deleting notifications error:', err))
      .finally(() => {})
  }
}

//
export function readAllNotifications(allRecord) {
  // var data=[];
  // data.push(dataToDelete)
  allRecord.map((not) => {
    not.read = true
  })

  return function(dispatch) {
    return CallVemoApi('PUT', '/vemo/v2/eventinstance', null, {
      MarkAllAsRead: true
    })
      .then((res) => {
        console.log('notification read ', res)
        dispatch(loadNotificationsData(allRecord))
      })

      .catch((err) => console.log('Deleting notifications error:', err))
      .finally(() => {})
  }
}
//academic enrollment status
export function fetchAcademicEnrollmentStatus() {
  return function(dispatch) {
    return CallVemoApi('GET', `/vemo/v2/picklist`, null, {
      resource: 'academicenrollment',
      attribute: 'status'
    })
      .then((res) => dispatch(loadAcademicEnrollmentStatus(res)))
      .catch((err) => console.error('Fetching AE status error:', err))
      .finally(() => {})
  }
}
export function fetchPaymentHistory(studentID) {
  let status = 'Cleared,Complete,Bounced,Refund'
  status = encodeURIComponent(status)
  return function(dispatch) {
    return CallVemoApi('GET', `/vemo/v2/paymentinstruction`, null, {
      studentID: studentID,
      status: status,
      latest: true
    })
      .then((res) => {
        const payment_history = Common.orderByDescDate(res, 'paymentDate');
        dispatch(loadPaymentHistoryData(payment_history))
      })
      .catch((err) => console.error('Fetching AE status error:', err))
      .finally(() => {})
  }
}

export function loadPaymentHistoryData(data) {
  return { type: LOAD_PAYMENT_HISTORY, paymentHistoryData: data }
}
export function loadAcademicEnrollmentStatus(data) {
  return { type: LOAD__ENROLLMENT_STATUS, academicEnrollmentStatuses: data }
}

// update Academic enrollment status

export function formSubmitted(status, automaticUpdate) {
  return { type: FORM_SUBMITTED, status, automaticUpdate}
}

export function UpdatedAcademicEnrollmentStatus(updateSuccess) {
  return function(dispatch) {
    dispatch(formSubmitted(updateSuccess, updateSuccess))
  }
}

export function UpdateAcademicEnrollmentStatus(data, automaticUpdate) {
  return function(dispatch) {
    return CallVemoApi('PUT', '/vemo/v2/academicenrollment', data)
      .then((res) => {
        dispatch(formSubmitted(true, !!automaticUpdate))
        dispatch(fetchStudentData(data[0].studentID))
      })
      .catch((err) => console.log('Update Student status error:', err))
      .finally(() => {})
  }
}


// action creators for fetching school details
export function fetchingSchool(isFetching) {
  return {
    type: FETCH__SCHOOL,
    isFetching
  }
}

export function loadSchoolData(data) {
  return {
    type: LOAD__SCHOOL_DATA,
    schoolData: data
  }
}

export function fetchSchoolDetails(schoolID) {
  return async function(dispatch) {
    dispatch(fetchingSchool(true))
    return await CallVemoApi('GET', '/vemo/v1/school', null, {schoolID})
      .then((res) => dispatch(loadSchoolData(res)))
      .catch((err) => console.error('Fetching school details error:', err))
      .finally(() => dispatch(fetchingSchool(false)))
  }
}
