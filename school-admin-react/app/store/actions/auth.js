import CallVemoApi from '~/app/store/salesforce'
import { 
  LOAD__ADMIN_DATA,
  IS_AUTHENTICATED, 
  IS_AUTH_PENDING,
  LOAD__SCHOOL_PROFILE
} from '~/app/store/constants'

export function loadAdmin(data) {
  return {
    type: LOAD__ADMIN_DATA,
    adminData: data
  }
}
export function isAuthPending(bool) {
  return {
    type: IS_AUTH_PENDING,
    isAuthPending: bool
  }
}
export function isAuthenticated(bool) {
  return {
    type: IS_AUTHENTICATED,
    isAuthenticated: bool
  }
}
export function loadSchoolProfile(data) {
  return {
    type: LOAD__SCHOOL_PROFILE,
    schoolProfileData: data
  }
}

// Check if user data is returnable, otherwise, not authenticated.
export function checkAuth() {
  return function(dispatch) {
    dispatch(isAuthPending(true))

    return CallVemoApi('GET', '/vemo/v2/user', null, null)
      .then(res => {
        dispatch(loadAdmin(res[0]))
        
        if (!res[0]['authenticated']) dispatch(isAuthenticated(false))
        else {
          dispatch(fetchSchoolProfile(res[0]['schoolID']))
          dispatch(isAuthenticated(true))
        }
      })
      .catch(err => {
        throw new Error(err)
      })
      .finally(() => dispatch(isAuthPending(false)))
  }
}

export function fetchSchoolProfile(schoolID) {
  return async function(dispatch) {
    return await CallVemoApi('GET', '/vemo/v2/school', null, { schoolID })
      .then(res => dispatch(loadSchoolProfile(res[0])))
      .catch(err => {
        throw new Error(err)
      })
      .finally(() => {})
  }
}
