import CallVemoApi from '~/app/store/salesforce'
import { 
  FETCH__SEARCH_RESULTS,
  LOAD__SEARCH_RESULTS,
} from '~/app/store/constants'

export function fetchingSearchResults(bool) {
  return {
    type: FETCH__SEARCH_RESULTS,
    isFetching: bool
  }
}
export function loadSearchResults(data) {
  return {
    type: LOAD__SEARCH_RESULTS,
    searchResults: data
  }
}

export function fetchSearchResults(searchQuery) {
  return function(dispatch) {
    dispatch(fetchingSearchResults(true))

    return CallVemoApi('GET', '/vemo/v2/search', null, { search: searchQuery })
      .then(res => dispatch(loadSearchResults(res)))
      .catch(err => console.log('Fetch search error', err))
      .finally(() => dispatch(fetchingSearchResults(false)))
  }
}
