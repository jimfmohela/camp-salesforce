export default { 
  isAuthPending: true,
  isAuthenticated: true, // Update this with salesforce integration,
  adminData: [{
    authenticated: false,
    name: '',
    schoolID: '',
    userEmail: '',
    userID: '',
    userName: ''
  }],
  schoolProfileData: {
    amountCertifiedToDate: 0,
    amountDisbursedToDate: 0,
    schoolID:'',
    schoolLogoURL: '',
    schoolName: ''
  },
}
