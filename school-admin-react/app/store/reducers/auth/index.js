import initialState from './state'
import { 
  LOAD__ADMIN_DATA,
  IS_AUTHENTICATED, 
  IS_AUTH_PENDING,
  LOAD__SCHOOL_PROFILE
} from '~/app/store/constants'

export function auth(state=initialState, action) {
  switch(action.type) {
    case IS_AUTH_PENDING:
      return {...state, isAuthPending: action.isAuthPending}
    case IS_AUTHENTICATED:
      return {...state, isAuthenticated: action.isAuthenticated}
    case LOAD__ADMIN_DATA:
      return {...state, adminData: action.adminData}
    case LOAD__SCHOOL_PROFILE:
      return {...state, schoolProfileData: action.schoolProfileData}
    default:
      return state
  }
}
