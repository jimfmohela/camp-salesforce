const 
  agreementsData = {
    agreementID: '',
    agreementStatus: '',
    amountDisbursedToDate: 0,
    expectedGraduationDate: '',
    fundingAmount: 0,
    incomeShare: 0,
    majorID: '',
    paymentCap: 0,
    paymentTerm: 0,
    programID: '',
    programName: '',
    requestedAmount: 0,
    studentID: '',
    studentName: '',
    studentProgramNumber: '',
    vemoContractNumber: ''
  },
  agreementMajorData = {
    definition: '',
    description: '',
    programOfStudyID: '',
    schoolID: '',
    schoolProgramOfStudyID: ''
  },
  programsData = {
    isFetching: false,
    data:[
      {
        agreements: [], // This is a custom data field, not from api
        amountCertifiedToDate: 0,
        amountDisbursedToDate: 0,
        attendancePeriods: [],
        creditCheckRequired: '',
        // cumulativeIncomeShareCap: '',
        enrollmentBeingDate: '',
        enrollmentEndDate: '',
        enrollementType: '',
        programEligibility: [{}],
        programID: '',
        programName: '',
        programStatus: '',
        registrationBeginDate: '',
        registrationEndDate: '',
        // templates: []
        vemoProgramNumber: ''
      }
    ]
  },
  studentsData = {
    isFetching: false,
    isLoaded:false,
    data: [
      {
        agreements: [
          { /* initialState.agreementsData */ }
        ],
        grade: '',
        graduationDate: '',
        programOfStudy: {},
        providerID: '',
        status: '',
        studentID: '',
        amountCertifiedToDate: 0,
        amountDisbursedToDate: 0,
        birthdate: '',
        city: '',
        country: '',
        email: '',
        firstName: '',
        lastName: '',
        middleName: '',
        mobilePhone: '',
        postalCode: '',
        state: '',
        street: '',
        studentID: '',
        vemoAccountNumber: ''
      }
    ]
  },
  countData = {
    isFetching: true,
    count: [
      {
        programID: '',
        statusCountList: [
          {
            count: 0,
            status: ''
          }
        ]
      }
    ],
  },
  dashboardData = {
    isFetching: true,
    isProgress: false,
    // pendingCertification: 0,
    // pendingDisbursement: 0,
    // underReview: 0,
    // cancelled: 0,
    // promisedFunding: 0,
    // requestedFunding: 0
    draft: 0,
    invited: 0,
    cancelled: 0,
    applicationIncomplete: 0,
    applicationUnderReview: 0,
    applicationComplete: 0,
    certified: 0,
    partiallyFunded: 0
  },
  programData = {
    isFetching: true,
    agreements: [],
    ...programsData,
  };

export default { 
  agreementsByID: {}, // Data revolves around AgreementID
  agreementsData: {
    isFetching: false,
    isLoaded: false,
    data: [agreementsData]
  },
  agreementData: {
    isFetching: false,
    data: agreementsData,
  },
  agreementMajorData,
  dashboardData,
  programsData,
  programData,
  programAgreementsData: agreementsData,
  studentsData: studentsData,
  studentData: {
    isFetching: false,
    data: studentsData
  },
  communicationHistoryData: { // Custom field to fetch communication history
    isFetching: false,
    data: []
  },
  countData,
  // countTotals: {
  //   inProgress: 0,
  //   pendingCertification: 0,
  //   pendingDisbursement: 0
  // },
  countTotals: [
    {
      recordCount: 0
    }
  ],
  studentCountData:[],
  paymentHistoryData:[]
  // notificationsData: []
}
