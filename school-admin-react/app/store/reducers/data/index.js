import initialState from './state'
import {
  LOAD__AGREEMENTS_BY_ID,
  LOAD__AGREEMENT_MAJOR,
  FETCH__AGREEMENT,
  LOAD__AGREEMENT,
  FETCH__AGREEMENTS,
  LOAD__AGREEMENTS,
  LOADED__AGREEMENTS,
  FETCH__DASHBOARD,
  LOAD__DASHBOARD,
  LOAD__PROGRAMS,
  FETCH__PROGRAM,
  FETCH__PROGRAMS,
  LOAD__PROGRAM,
  LOAD__STUDENTS,
  FETCH__STUDENTS,
  FETCH__STUDENT,
  LOAD__STUDENT,
  LOAD__NOTIFICATIONS_DATA,
  FETCH__COUNT_DATA,
  LOAD__COUNT_DATA,
  FETCH__COMMUNICATIONS_HISTORY_DATA,
  LOAD__COMMUNICATIONS_HISTORY_DATA,
  LOAD__COUNT_TOTALS,
  LOAD_EVENT_TYPE_DATA,
  LOAD_NOTIFICATION_SETTING_DATA,
  UPDATE_NOTIFICATION_SETTING,
  FETCH__AGREEMENTS_V1,
  LOAD__AGREEMENTS_V1,
  LOADED__AGREEMENTS_V1,
  FETCH__FEES,
  LOADED__FEES,
  LOAD__FEES,
  FETCH__INCOME_VERIFICATION,
  LOADED__INCOME_VERIFICATION,
  LOAD__INCOME_VERIFICATION,
  FETCH_STUDENT_V1,
  LOADED__STUDENT_V1,
  LOAD__STUDENT_V1,
  LOADED__STUDENTS,

  LOAD_STUDENT_COUNT,
  LOAD__ENROLLMENT_STATUS,
  LOAD_PAYMENT_HISTORY,
  FORM_SUBMITTED,
  LOAD__SCHOOL_DATA,
  FETCH__SCHOOL
} from '~/app/store/constants'

export function data(state = initialState, action) {
  switch (action.type) {
    case LOAD__AGREEMENTS_BY_ID:
      return { ...state, agreementsByID: action.agreementsByID };
    case LOAD__AGREEMENT_MAJOR:
      return { ...state, agreementMajorData: action.agreementMajorData };
    case FETCH__AGREEMENT:
      return {
        ...state,
        agreementData: {
          ...state.agreementData,
          isFetching: action.isFetching
        }
      };
    case LOAD__AGREEMENT:
      return {
        ...state,
        agreementData: {
          ...state.agreementData,
          data: action.agreementData
        }
      };
    case FETCH__AGREEMENTS:
      return {
        ...state,
        agreementsData: {
          ...state.agreementsData,
          isFetching: action.isFetching
        }
      };
    case LOAD__AGREEMENTS:
      return {
        ...state,
        agreementsData: {
          ...state.agreementsData,
          data: action.agreementsData
        }
      };
    case LOADED__AGREEMENTS:
      return {
        ...state,
        agreementsData: {
          ...state.agreementsData,
          isLoaded: action.isLoaded
        }
      };
      case LOADED__STUDENTS:

      return {

       ...state,

        studentsData: {

          ...state.studentsData,

          isLoaded: action.isLoaded

        }

      };
    case FETCH__AGREEMENTS_V1:
      return {
        ...state,
        agreementsDataV1: {
          ...state.agreementsDataV1,
          isFetching: action.isFetching
        }
      };
    case LOAD__AGREEMENTS_V1:
      return {
        ...state,
        agreementsDataV1: {
          ...state.agreementsDataV1,
          data: action.agreementsDataV1
        }
      };
    case LOADED__AGREEMENTS_V1:
      return {
        ...state,
        agreementsDataV1: {
          ...state.agreementsDataV1,
          isLoaded: action.isLoaded
        }
      };
    case FETCH__FEES:
      return {
        ...state,
        studentFees: {
          ...state.studentFees,
          isFetching: action.isFetching
        }
      };
    case LOAD__FEES:
      return {
        ...state,
        studentFees: {
          ...state.studentFees,
          data: action.studentFees
        }
      };
    case LOADED__FEES:
      return {
        ...state,
        studentFees: {
          ...state.studentFees,
          isLoaded: action.isLoaded
        }
      };
    case FETCH__INCOME_VERIFICATION:
      return {
        ...state,
        studentIncomeVerfication: {
          ...state.studentIncomeVerfication,
          isFetching: action.isFetching
        }
      };
    case LOAD__INCOME_VERIFICATION:
      return {
        ...state,
        studentIncomeVerfication: {
          ...state.studentIncomeVerfication,
          data: action.studentIncomeVerfication
        }
      };
    case LOADED__INCOME_VERIFICATION:
      return {
        ...state,
        studentIncomeVerfication: {
          ...state.studentIncomeVerfication,
          isLoaded: action.isLoaded
        }
      };
    case FETCH_STUDENT_V1:
      return {
        ...state,
        studentv1: {
          ...state.studentv1,
          isFetching: action.isFetching
        }
      };
    case LOAD__STUDENT_V1:
      return {
        ...state,
        studentv1: {
          ...state.studentv1,
          data: action.studentv1
        }
      };
    case LOADED__STUDENT_V1:
      return {
        ...state,
        studentv1: {
          ...state.studentv1,
          isLoaded: action.isLoaded
        }
      };
    case FETCH__DASHBOARD:
      return {
        ...state,
        dashboardData: {
          ...state.dashboardData,
          isFetching: action.isFetching
        }
      };
    case LOAD__DASHBOARD:
      return {
        ...state,
        dashboardData: {
          ...state.dashboardData,
          ...action.dashboardData
        }
      };
    case FETCH__PROGRAMS:
      return {
        ...state,
        programsData: {
          ...state.programsData,
          isFetching: action.isFetching
        }
      };
    case LOAD__PROGRAMS:
      return {
        ...state,
        programsData: {
          ...state.programsData,
          data: action.programsData
        }
      };
    case FETCH__PROGRAM:
      return {
        ...state,
        programData: {
          ...state.programData,
          isFetching: action.isFetching
        }
      };
    case LOAD__PROGRAM:
      return {
        ...state,
        programData: {
          ...state.programData,
          ...action.programData
        }
      };
    case FETCH__STUDENT:
      return {
        ...state,
        studentData: {
          ...state.studentData,
          isFetching: action.isFetching
        },
        communicationHistoryData: initialState.communicationHistoryData
      };
    case LOAD__STUDENT:
      return {
        ...state,
        studentData: {
          ...state.studentData,
          data: action.studentData
        },
        communicationHistoryData: initialState.communicationHistoryData
      };
    case FORM_SUBMITTED:
      return {
        ...state,
        enrollmentFormStatus: {
          status: action.status,
          automaticUpdate: action.automaticUpdate
        }
      }
    case FETCH__STUDENTS:
      return {
        ...state,
        studentsData: {
          ...state.studentsData,
          isFetching: action.isFetching
        }
      };
    case FETCH__SCHOOL: 
      return {
        ...state,
        schoolData: {
          ...state.schoolData,
          isFetching: action.isFetching
        }
    }
    case LOAD__STUDENTS:
      return {
        ...state,
        studentsData: {
          ...state.studentsData,
          data: action.studentsData
        }
      };
    case LOAD__SCHOOL_DATA:
      return {
        ...state,
        schoolData: {
          ...state.schoolData,
          data: action.schoolData,
        }
      };
    case FETCH__COMMUNICATIONS_HISTORY_DATA:
      return {
        ...state,
        communicationHistoryData: {
          ...state.communicationHistoryData,
          isFetching: action.isFetching
        }
      };
    case LOAD__COMMUNICATIONS_HISTORY_DATA:
      return {
        ...state,
        communicationHistoryData: {
          ...state.communicationHistoryData,
          data: action.communicationHistoryData
        }
      };
    case LOAD__NOTIFICATIONS_DATA:
      return { ...state, notificationsData: action.notificationsData };
    case FETCH__COUNT_DATA:
      return {
        ...state,
        countData: {
          ...state.countData,
          isFetching: action.isFetching
        }
      };
    case LOAD__COUNT_DATA:
      return {
        ...state,
        countData: {
          ...state.countData,
          count: action.countData
        }
      };
    case LOAD__COUNT_TOTALS:
      return { ...state, countTotals: action.countTotals }
    case LOAD_EVENT_TYPE_DATA:
      return {...state, eventTypeData:action.eventTypeData}
    case LOAD_NOTIFICATION_SETTING_DATA:
     return {...state, notificationSettingData:action.notificationSettingData}
     case LOAD_STUDENT_COUNT:

       return {...state,studentCountData:action.studentCountData}
       case LOAD__ENROLLMENT_STATUS:
        return {
          ...state,
          academicEnrollmentStatuses: {
            ...state.academicEnrollmentStatuses,
            data: action.academicEnrollmentStatuses
          }
        };
        case LOAD_PAYMENT_HISTORY:
          return {
            ...state,
            paymentHistoryData: {
              ...state.paymentHistoryData,
              data: action.paymentHistoryData
            }
          };
    default:
      return state;
  }
}
