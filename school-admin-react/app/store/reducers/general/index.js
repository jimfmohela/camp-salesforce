// import initialState from './state'
import {
  TOGGLE_ACCESSIBILITY_TABBING
} from '~/app/store/constants'

const initialState = {
  accessibilityTabbing: false
}

export function general(state=initialState, action) {
  switch(action.type) {
    case TOGGLE_ACCESSIBILITY_TABBING:
      return {...state, accessibilityTabbing: action.accessibilityTabbing}
    default:
      return state
  }
}
