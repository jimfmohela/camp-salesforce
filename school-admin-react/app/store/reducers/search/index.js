import initialState from './state'
import {
  FETCH__SEARCH_RESULTS,
  LOAD__SEARCH_RESULTS,
} from '~/app/store/constants'

export function search(state=initialState, action) {
  switch(action.type) {
    case FETCH__SEARCH_RESULTS:
      return {
        ...state,
        searchResults: {
          ...state.searchResults,
          isFetching: action.isFetching
        }
      }
    case LOAD__SEARCH_RESULTS:
      return {
        ...state, 
        searchResults: {
          ...state.searchResults,
          results: action.searchResults
        }
      }
    default:
      return state
  }
}
