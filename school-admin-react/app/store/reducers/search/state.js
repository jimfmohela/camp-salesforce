export default {
  searchResults: {
    isFetching: false,
    results: [{
      students: {
        numberOfResults: 0,
        records: []
      },
      schools: {
        numberOfResults: 0,
        records: []
      },
      programs: {
        numberOfResults: 0,
        records: []
      },
      agreements: {
        numberOfResults: 0,
        records: []
      },
    }]
  }
}
