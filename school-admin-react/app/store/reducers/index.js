import { combineReducers }  from 'redux'
import { auth } from './auth/'
import { general } from './general/'
import { data } from './data/'
import { search } from './search/'

const rootReducer = combineReducers({
  auth,
  general,
  data,
  search
})

export default rootReducer
