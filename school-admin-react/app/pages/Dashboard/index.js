import React from 'react'

import Layout from '~/app/containers/SharedLayout'
import DashboardPanel from '~/app/containers/DashboardPanel'

export default () => (
  <Layout title="Dashboard Panel">
    <DashboardPanel />
  </Layout>
)
