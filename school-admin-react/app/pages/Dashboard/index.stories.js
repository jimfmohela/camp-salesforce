import React from 'react'
import { storiesOf, addDecorator, addParameters } from '@storybook/react'
import Provider from '~/.storybook/Provider'
import Dashboard from './index'
import Notes from './notes.md'
import { AppContext } from '~/app/hooks/app'
import { boolean, button } from '@storybook/addon-knobs'

storiesOf('Pages', module)
  .addDecorator(Provider)
  .add(
    'Dashboard',
    () => {
      let _activeSidebar = false
      const { activeSidebar, updateIsMobileState, updateActiveSidebar } = React.useContext(
        AppContext
      )
      const isMobile = boolean('Is Mobile', true)
      _activeSidebar = _activeSidebar
      isMobile && button("Toggle Sidebar", () => {
        _activeSidebar = !_activeSidebar
        updateActiveSidebar(_activeSidebar)
      })

      updateIsMobileState(isMobile)

      return <Dashboard />
    },
    {
      notes: { markdown: Notes }
    }
  )
