import produce from "immer"

const environments = {
  PROD: {
    sitebaseurl: 'https://schooladmin.vemo.com',
    name: 'prod'
  },
  UAT: {
    sitebaseurl: 'https://uat-vemo.cs42.force.com/schooladmin',
    name: 'uat'
  },
  QA: {
    sitebaseurl: 'https://qa-vemo.cs34.force.com/schooladmin',
    name: 'qa'
  },
  INT: {
    sitebaseurl: 'https://int-vemo.cs62.force.com/schooladmin',
    name: 'int'
  }
}

const isProd = () => {
  return [environments.PROD.sitebaseurl, environments.UAT.sitebaseurl].includes(
    window.baseRoute
  )
}

const hideInProd = (Component) => {
  if (isProd()) {
    return null
  }

  return Component
}

export { environments, isProd, hideInProd }
