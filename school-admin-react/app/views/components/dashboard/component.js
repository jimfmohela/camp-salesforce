import React          from 'react'
import { NavLink }    from 'react-router-dom'
import Icon           from '~/app/assets/fonts/icon'
// import AnimatedNumber from 'react-animated-number'

function DashboardComponent(props) {
  let { data, count } = props,
      type,
      dots,
      dotsCount       = {0: 5, 1: 7, 2: 9}; //dot count and styling depending on programs type
          

  let initiationType = (programCount) => {
    if (!programCount) return 2
    let type         = 0,
        programTypes = { "Invite Only": false, "Open": false },
        i            = 0;
    if(programCount){
    while (type < 2 && i < programCount.length) {
      if (!programTypes[programCount[i].enrollmentType]) {
        programTypes[programCount[i].enrollmentType] = true
        type++
      }
      i++
    }
  }
    
    if (type === 2) return type
    return (type = programTypes["Invite Only"] ? type : 0)
    
  };

  type = initiationType(count)
  dots = Array(dotsCount[type]).fill(0)

  return (
    <section className='t__dashboard-page'>
      <article className='dashboard-applications'>
        <div className='o__dashboard-layout o__dashboard-layout--header'>
          <Icon icon='applications-fill' />
          <h2 className='main'>APPLICATIONS</h2>
        </div>

        <div className='o__dashboard-layout o__dashboard-layout--body'>
          <aside className='dashboard-dots'>
            {dots.map((dot, i) => (<span />))}
          </aside>

          {type !== 0 ?
            <aside className='dashboard-block'>
              <h3>Draft</h3>
              <p><h1 class="main">{data.draft}</h1> drafts need to be finished</p>

              <a href={`${process.env.PUBLIC_URL}/agreements/applications/1?filters=draft`}>
                <h2 className="white">Finish</h2>
            </a>
            </aside> : null}
          
          {type !== 0 ? 
            <aside className='dashboard-block'>
              <h3>Invited</h3>
              <p><h1 class="main">{data.invited}</h1> invitations are waiting for students to sign up</p>

              <a href={`${process.env.PUBLIC_URL}/agreements/applications/1?filters=invited`}>
              <h2 className="white">View</h2>
            </a>
            </aside> : null}

          <aside className='dashboard-block'>
            <h3>Application Incomplete</h3>
            <p><h1 class="main">{data.applicationIncomplete}</h1> applications have been started but not yet finished</p>

            <a href={`${process.env.PUBLIC_URL}/agreements/applications/1?filters=application%20incomplete`}>
            <h2 className="white">View</h2>
            </a>
          </aside>

          <aside className='dashboard-block'>
            <h3>Application Under Review</h3>
            <p><h1 class="main">{data.applicationUnderReview}</h1> applications are under review</p>

            <a href={`${process.env.PUBLIC_URL}/agreements/applications/1?filters=application%20under%20review`}>
            <h2 className="white">View</h2>
            </a>
          </aside>

          {type !== 1 ?
            <aside className='dashboard-block'>
              <h3>Application Complete</h3>
              <p><h1 class="main">{data.applicationComplete}</h1> applications are complete</p>

              <a href={`${process.env.PUBLIC_URL}/agreements/applications/1?filters=application%20complete`}>
              <h2 className="white">View</h2>
            </a>
            </aside> : null}
        </div>
      </article>

      <article className='dashboard-agreements'>
        <div className='o__dashboard-layout o__dashboard-layout--header'>
          <Icon icon='applications-fill' />
          <h2 className='main'>AGREEMENTS (APPLICATIONS THAT HAVE BEEN CERTIFIED)</h2>
        </div>

        <div className='o__dashboard-layout o__dashboard-layout--body'>
          <aside className='dashboard-dots'>
            <span></span>
            <span></span>
            <span></span>
          </aside>

          <aside className='dashboard-block'>
            <h3>Certified</h3>
            <p><h1 class="main">{data.certified}</h1> agreements are waiting for their first disbursement</p>

            <a href={`${process.env.PUBLIC_URL}/agreements/agreements/1?filters=certified`}>
            <h2 className="white">Confirm Disbursement</h2>
            </a>

          </aside>

          <aside className='dashboard-block'>
            <h3>Partially Funded</h3>
            <p><h1 class="main">{data.partiallyFunded}</h1> agreements are partially funded and waiting for their remaining disbursements</p>

            <a href={`${process.env.PUBLIC_URL}/agreements/agreements/1?filters=partially%20funded`}>
            <h2 className="white">Confirm Disbursement</h2>
            </a>
          </aside>

          <span className='cancelled-block'>
            <a href={`${process.env.PUBLIC_URL}/agreements/cancelled/1`}>
            Cancelled: {data.cancelled}  <Icon icon='arrow-right' />
            </a>
          </span>
        </div>
      </article>
    </section>
  )
}

export default DashboardComponent
