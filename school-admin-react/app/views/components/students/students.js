import React from 'react'
import VemoTable from '~/app/views/components/shared/table/table'
import VemoTableCell from '~/app/views/components/shared/table/cell'
import Icon from '~/app/assets/fonts/icon'
import Dates from '~/app/assets/js/dates'
import { maskFS } from '../../../helpers/fullstory'

const displayAgreements = (data) => {
  const { agreements } = data
  if (!agreements.length) {
    return <span>No Agreements</span>
  }
  return agreements.map((program) => (
    <span key={Math.random()}>
      <p>{program.programName}</p>
      <p>{program.agreementStatus}</p>
    </span>
  ))
}
const columns = [
  {
    header: 'Name',
    headerClassName: 'name',
    accessor: 'firstName',
    accessorCell(data) {
      return (
        maskFS(<VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor] || data.lastName}>
          <Icon icon="student-fill" />
          <span>
            <h3 className="main">
              {data[this.accessor]}
              {' '}
              {data.lastName}
            </h3>
            <br />
            <p className="main">{data.vemoAccountNumber}</p>
          </span>
               </VemoTableCell>))
    }
  }, {
    header: 'ISA Program',
    headerClassName: 'program',
    accessor: 'isaProgram',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data.agreements}>
          <h3>
            {data.agreements.length}
            {' '}
            agreements
          </h3>

          <div className="m__student-list-info">
            <p>...</p>
            <article className="list-info-bubble">
              {displayAgreements(data)}
            </article>
          </div>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Program/Course',
    headerClassName: 'program-course',
    accessor: 'description',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <p>{data[this.accessor]}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Graduation',
    headerClassName: 'graduation',
    accessor: 'graduationDate',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data.graduationDate}>
          <p className="main">Graduation Date:&nbsp;</p>
          <p className="main">{data.graduationDate}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Enrollment',
    headerClassName: 'enrollment',
    accessor: 'status',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data.status}>
          <h3>{data.status}</h3>
        </VemoTableCell>
      )
    }
  }
]


const StudentsComponent = (props) => {
  const { data } = props

  return (
    <VemoTable
      numberOfColumns={columns.length}
      columns={columns}
      headerClass="o__students-table o__students-table--header"
      rowClass="o__students-table o__students-table--row"
      data={data}
      pathData={{
        path: '/student/',
        pathId: 'studentID',
        pathFilter: '/agreements/'
      }}
    />
  )
}

export default StudentsComponent
