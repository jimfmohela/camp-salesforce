import React from 'react'
import { NavLink } from 'react-router-dom'
import Common from '~/app/assets/js/common'
import Icon from '~/app/assets/fonts/icon'
import { maskFS } from '../../../helpers/fullstory'

class AgreementInfo extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data, major } = this.props


    return (
      <div className="o__agreement-info">
        <article className="agreement-info-block">
          <div className="m__agreement-info-block">
            <h1>Agreement Details</h1>

            <h4>Program Name</h4>
            <p className="navigate_css">
              {data.programName
                ? (
                  <NavLink
                    to={{
                      pathname: `/programs/program/${data.programID}/agreements`

                    }}
                  >
                    {data.programName}
                    <Icon icon="arrow-right2" viewBox="0 0 22 22" />
                  </NavLink>
                )
                : 'NA'}
            </p>

            <h4>Program Number</h4>
            <p>{data.programNumber || 'N/A'}</p>

            {/* <h3>Student Program Number</h3>
            <p>{data.studentProgramNumber || 'N/A'}</p> */}

            <h4>Vemo Contract Number</h4>
            <p>{data.vemoContractNumber || 'N/A'}</p>

            <h4>Student</h4>
            {data.studentName
              ? maskFS(
                <p className="navigate_css">
                  <NavLink
                    to={{ pathname: `/student/${data.studentID}/agreements` }}
                  >
                    {data.studentName}
                    <Icon icon="arrow-right2" viewBox="0 0 22 22" />
                  </NavLink>
                  {' '}

                </p>
              )
              : <p>N/A</p>}
            {/* <p>{data.studentName || 'N/A'}</p> */}

            {/* <h3>Credit Review Status</h3>
            <p>N/A</p> */}

            <h4>Status</h4>
            <p>{data.agreementStatus || 'N/A'}</p>
          </div>
          {/* <hr className="m__line"></hr> */}
          <div className="m__agreement-info-block">
            <h1>Student Info</h1>

            <h4>Major At Time Of Application</h4>
            <p>{major.description || 'N/A'}</p>

            {/* <h3>Expected Grad. Date</h3>
            <p>{expectedGraduationDate}</p> */}
          </div>
        </article>

        <article className="agreement-info-block">
          {/* <hr className="m__line"></hr> */}
          <div className="m__agreement-info-block">
            <h1>Agreement Terms</h1>

            <h4>Funding Amount</h4>
            <p>
              {data.fundingAmount
                ? Common.AmountFormat(data.fundingAmount) : 'N/A'}
              {' '}

            </p>

            <h3>Income Share</h3>
            <p>{data.incomeShare ? `${data.incomeShare}%` : 'N/A'}</p>

            <h3>Payment Term</h3>
            <p>{data.paymentTerm ? `${data.paymentTerm} months` : 'N/A'}</p>

            <h4>Payment Cap</h4>
            <p>
              {data.paymentCap
                ? Common.AmountFormat(data.paymentCap) : 'N/A'}
              {' '}

            </p>
          </div>
        </article>
      </div>
    )
  }
}

export default AgreementInfo
