import React from 'react'
import VemoTable from '~/app/views/components/shared/table/table'
import VemoTableCell from '~/app/views/components/shared/table/cell'
import Icon from '~/app/assets/fonts/icon'
import Common from '~/app/assets/js/common'
import Dates from '~/app/assets/js/dates'
import { maskFS } from '../../../helpers/fullstory'

const columns = [
  {
    header: 'Student Name',
    headerClassName: 'student-name',
    accessor: 'studentName',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <Icon icon="applications-fill" />
          <span>
            {maskFS(<h3 className="main">{data[this.accessor]}</h3>)}
            <p className="main">{data.vemoContractNumber}</p>
          </span>
        </VemoTableCell>
      )
    }
  }, {
    header: 'ISA Program Name',
    headerClassName: 'program-name',
    accessor: 'programName',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <h3 className="main">{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Contract Num',
    headerClassName: 'contract-num',
    accessor: 'vemoContractNumber',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <h2 className="agreement-label">Contract #-&nbsp;</h2>
          <h2 className="agreement-value">{data[this.accessor]}</h2>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Funding Amount',
    headerClassName: 'funding-amount',
    accessor: 'fundingAmount',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          {data[this.accessor]
            ? (
              <div className="funding-div">
                <p className="agreement-label">&nbsp;Funding-&nbsp;</p>

                <p className="agreement-value">{Common.AmountFormat(data[this.accessor])}</p>

              </div>
            )
            : null}
        </VemoTableCell>
      )
    }
  }, {
    header: 'Expected Grad. Date',
    headerClassName: 'grad-date',
    accessor: 'expectedGraduationDate',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <p>{Dates.formatToMMDDYYYY(data[this.accessor])}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Status',
    headerClassName: 'status',
    accessor: 'agreementStatus',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <p>{data[this.accessor]}</p>
        </VemoTableCell>
      )
    }
  }
]

function AgreementsComponent(props) {
  const { data } = props

  return (
    <VemoTable
      numberOfColumns={columns.length}
      columns={columns}
      headerClass="o__agreements-table o__agreements-table--header"
      rowClass="o__agreements-table o__agreements-table--row"
      data={data}
      status="agreementStatus"
      pathData={{
        path: '/agreements/agreement/',
        pathId: 'agreementID'
      }}
    />
  )
}

export default AgreementsComponent
