import React from 'react'
import Icon from '~/app/assets/fonts/icon'

import StudentsComponent from '~/app/views/components/students/students'
import AgreementsComponent from '~/app/views/components/agreements/component'
import ProgramsComponent from '~/app/views/components/programs/component'

class SearchComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  renderSection = (type) => {
    let { data, filterOption } = this.props

    if (data[type]['numberOfResults'] <= 0) return false
    if ((/all/i).test(filterOption)) return true

    let filter = new RegExp(type, 'i')
    return filter.test(filterOption)
  }

  render() {
    let { data } = this.props

    return (
      <>
        {this.renderSection('students') ? (
          <article>
            <Icon icon='students-fill' />
            <h2>STUDENTS</h2>

            <StudentsComponent 
              key={Math.random()} 
              data={data.students.records} />
          </article>
        ) : null}

        {this.renderSection('agreements')  ? (
          <article>
            <Icon icon='applications-fill' />
            <h2>AGREEMENTS</h2>

            <AgreementsComponent 
              key={Math.random()} 
              data={data.agreements.records} />
          </article>
        ) : null }

        {this.renderSection('programs')  ? (
          <article>
            <Icon icon='isa-fill' />
            <h2>ISA PROGRAMS</h2>

            <ProgramsComponent 
              key={Math.random()} 
              data={data.programs.records} />
          </article>
        ) : null }
      </>
    )
  }
}

export default SearchComponent
