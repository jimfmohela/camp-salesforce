import React from 'react'
import Icon from '~/app/assets/fonts/icon';
//import Programs from '~/app/assets/fonts/svgs/zeroState/ISA-programs.svg'

const ProgramsZeroState = () => {
    return (
        <div className='zeroState'>
            <center>
                <div className='zeroState--img'>
                <img src='https://s3.amazonaws.com/ftp-server-deepak/assets/programs.svg' />
                </div>

            <h1>Hmm.. no ISA Programs</h1><br />
            <p>Change your filter setting or contact vemo. We can help you find what's missing</p><br/>
            <a href="mailto:campusserv@vemo.com" target="_top">vemo support
            <Icon icon='arrow-right' />
            </a>
            </center>
        </div>
    )
}

export default ProgramsZeroState
