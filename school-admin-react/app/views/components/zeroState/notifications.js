import React from "react";
import Icon from "~/app/assets/fonts/icon";

const NotificationZeroState = () => {
  return (
    <div className="zeroState">
        <div className="zeroState--img">
          <img src="https://s3.amazonaws.com/ftp-server-deepak/assets/notifications.svg" />
        </div>

        <h1>No Notifications as of now...</h1>
        <br />
        <p>
        Contact Vemo if you're missing something.
        <br/>
         We can help you find it. 
        </p>
        <a href="mailto:campusserv@vemo.com" target="_top">
          Vemo Support
          <Icon icon="arrow-right" />
        </a>
    </div>
  );
};

export default NotificationZeroState;
