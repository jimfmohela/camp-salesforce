import React from "react";
import Icon from "~/app/assets/fonts/icon";
//import Agreements from '~/app/assets/fonts/svgs/zeroState/agreements.svg'

const AgreementsZeroState = () => {
  return (
    <div className="zeroState">
      <center>
        <div className="zeroState--img">
          <img src="https://s3.amazonaws.com/ftp-server-deepak/assets/agreements.svg" />
        </div>

        <h1>No Agreements here yet...</h1>
        <br />
        <p>
          Change your filter setting or contact vemo. We can help you find
          what's missing
        </p>
        <br />
        <a href="mailto:campusserv@vemo.com" target="_top">
          vemo support
          <Icon icon="arrow-right" />
        </a>
      </center>
      <br />
    </div>
  );
};

export default AgreementsZeroState;
