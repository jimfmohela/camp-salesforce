import React from 'react'
import Icon from '~/app/assets/fonts/icon'

const StudentsZeroState = () => {
    return (
        <div className='zeroState'>
            <center>
                <div className='zeroState--img'>
                <img src='https://s3.amazonaws.com/ftp-server-deepak/assets/student.svg' />
                </div>

            <h1>No Students Here... </h1><br />
            <p>Change your filter setting or contact vemo. We can help you find what's missing</p><br/>
            <a href="mailto:campusserv@vemo.com" target="_top">vemo support
            <Icon icon='arrow-right' />
            </a>
            </center>
        </div>
    )
}
 
export default StudentsZeroState
