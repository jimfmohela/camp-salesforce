import React from 'react'
import Icon from '~/app/assets/fonts/icon'
//import Emails from '~/app/assets/fonts/svgs/zeroState/emails.svg'

const HistoryZeroState = () => {
  return (
    <div className='zeroState'>
      <center>
        <div className='zeroState--img'>
          <img src='https://s3.amazonaws.com/ftp-server-deepak/assets/email.svg' />
        </div>
        <h1>No Emails to show... yet</h1><br />
        <p>Change your filter setting or contact vemo. we can help you find what's missing</p><br />
        <a href="mailto:campusserv@vemo.com" target="_top">vemo support
            <Icon icon='arrow-right' />
        </a>
        {/*<p>Sorry! No communications yet</p>*/}
      </center>
    </div>
  )
} 

export default HistoryZeroState
