import React from 'react'
import { Link } from 'react-router-dom'
import Icon from '~/app/assets/fonts/icon'
import Dates from '~/app/assets/js/dates'
import { maskFS } from '../../../helpers/fullstory'

const PaymentZeroState = (props) => {
  const { studentv1 } = props
  let studentId = ''
  let studentFirstName = ''
  let studentName = ''

  if (studentv1 && studentv1.length) {
    studentFirstName = studentv1[0].firstName
    studentName = (studentFirstName || '') + studentv1[0].lastName
    studentId = studentv1[0].studentID
  }

  return (
    <div className="zeroState">
      <center>
        <div className="zeroState--img">
          <img src="https://s3.amazonaws.com/ftp-server-deepak/assets/agreements.svg" />
        </div>
        <h1>This student is not in payment yet...</h1>
        <br />
        <br />
        <Link to={`/student/${studentId}/agreements`}>
          View all of&nbsp;
          {' '}
          {maskFS(studentFirstName || 'N/A', true)}
          {' '}
          's agreements
          <Icon icon="arrow-right" />
        </Link>
      </center>
    </div>
  )
}

export default PaymentZeroState
