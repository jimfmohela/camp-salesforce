import React from 'react'
import Icon from '~/app/assets/fonts/icon'
import { NavLink } from "react-router-dom";

const SearchZeroState = () => {
    return (
        <div className='zeroState'>
            <center>
                <div className='zeroState--img'>
                <img src='https://s3.amazonaws.com/ftp-server-deepak/assets/search.svg' />
                </div>

            <h1>No search result</h1><br />
            <p>We've searched the globe and come up with nothing. Try refining your search.</p><br/>
            <NavLink activeClassName="is-active" to="/dashboard">
                  <span>Search Again </span>
                 <Icon icon='arrow-right' />
           
            </NavLink>
            </center>
        </div>
    )
}
 
export default SearchZeroState
