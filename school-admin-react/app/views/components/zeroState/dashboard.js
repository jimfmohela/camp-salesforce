import React from 'react'
import Icon from '~/app/assets/fonts/icon'
//import Emails from '~/app/assets/fonts/svgs/zeroState/emails.svg'

const DashboardZeroState = () => {
  return (
    <div className='zeroState'>
      <center>
        <div className='zeroState--img'>
          <img src='https://s3.amazonaws.com/ftp-server-deepak/assets/dashboard.svg' />
        </div>
        <h1>Nothing to Show... yet</h1><br />
        <p>Change your time range or contact vemo. We can help you find what's missing</p><br />
        <a href="mailto:campusserv@vemo.com" target="_top">vemo support
            <Icon icon='arrow-right' />
        </a>
        {/*<p>Sorry! No communications yet</p>*/}
      </center>
    </div>
  )
}

export default DashboardZeroState
