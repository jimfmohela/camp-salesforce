import React from 'react'
import { connect } from 'react-redux'
import { NavLink, withRouter } from 'react-router-dom'
import window from 'global'
import Icon from '~/app/assets/fonts/icon'
import Dates from '~/app/assets/js/dates'
import VemoLogoImage from '~/app/assets/fonts/svgs/vemo-logo-color.svg'
import VemoLogo from './VemoLogo'
import RefreshModal from './refreshModal'
import ResponsiveFlyout from '../../../components/FlyOut/ResponsiveFlyout'

class HeaderMenu extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      modalOpen: false
    }
  }

  modalControl = () => {
    this.setState({ modalOpen: !this.state.modalOpen })
  }

  componentDidMount() {
    const { adminData } = this.props
    if (typeof window.FS !== 'undefined') { window.FS.identify(adminData.userID, { displayName: adminData.name, email: adminData.userEmail }) }
  }

  render() {
    const { adminData, schoolProfileData } = this.props
    const { modalOpen } = this.state
    return (
      <article
        className="header-hamburger--menu"
        id="header-menu"
        role="navigation"
        aria-label="Main Navigation"
      >
        <div className="m__header-menu m__header-menu--logo">
          <img src={schoolProfileData.schoolLogoURL} />
          {/* <Haverton /> */}
        </div>

        <div className="m__header-menu m__header-menu--profile">
          <input id="hamburger-profile-checkbox" type="checkbox" />
          <label
            htmlFor="hamburger-profile-checkbox"
            className="hamburger-profile-checkbox"
          >
            <p>{adminData.name}</p>
            <Icon icon="arrow-down" />
          </label>

          <article className="hamburger-profile-links">
            <NavLink activeClassName="is-active" to="/settings">
              <Icon icon="settings" />
              <p>Settings</p>
            </NavLink>

            {/* <NavLink to="/school">
              <Icon icon="settings" />
              <p>Settings</p>
            </NavLink> */}

            <NavLink to="/school details">
              <Icon icon="colleges-line" />
              <p>School Details</p>
            </NavLink>

            <a href={`${window.baseRoute}/secur/logout.jsp`}>
              <Icon icon="sign-out" />
              <p>Sign out</p>
            </a>
          </article>
        </div>

        <div className="m__header-menu m__header-menu--submenu">
          <article className="header-submenu-links">
            <NavLink activeClassName="is-active" to="/dashboard">
              <Icon icon="dashboard-line" />
              <Icon icon="dashboard-fill" />
              <h5>Dashboard</h5>
            </NavLink>

            {/* <NavLink activeClassName='is-active' to='/colleges'>
              <Icon icon='colleges-line' />
              <Icon icon='colleges-fill' />
              <h5>Colleges</h5>
            </NavLink> */}

            <NavLink activeClassName="is-active" to="/programs">
              <Icon icon="isa-line" />
              <Icon icon="isa-fill" />
              <h5>ISA Programs</h5>
            </NavLink>

            <NavLink activeClassName="is-active" to="/agreements">
              <Icon icon="applications-line" />
              <Icon icon="applications-fill" />
              <h5>Agreements</h5>
            </NavLink>

            <NavLink activeClassName="is-active" to="/students">
              <Icon icon="students-line" />
              <Icon icon="students-fill" />
              <h5>Students</h5>
            </NavLink>
          </article>
        </div>

        <div className="m__header-menu m__header-menu--footer">
          <div className="beta-version">
            <h5>Beta Version</h5>
            <ResponsiveFlyout />
            {/* <Icon icon="help" viewBox="0 0 24 24" /> */}
          </div>
          <aside className="certification-link">
            <Icon icon="link-out" />
            <a
              target="_blank"
              href={`${window.baseRoute}/Certification`}
              onClick={this.modalControl}
            >
              Certification
            </a>
          </aside>

          <div
            style={{
              backgroundImage: `url(${VemoLogo})`,
              backgroundSize: 'cover',
              minHeight: '10rem',
              textAlign: 'center',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <div>
              <a href="https://vemoeducation.com/term-of-use/" target="_blank">
                Terms of Use
              </a>
              <a
                href="https://vemoeducation.com/privacy-policy/"
                target="_blank"
              >
                Privacy Policy
              </a>
            </div>
            <br />
            <VemoLogoImage />
            <br />
            <p>
              &copy;
              {Dates.getYear()}
              Vemo Education
            </p>
          </div>
        </div>

        <RefreshModal open={modalOpen} onClose={this.modalControl} />
      </article>
    )
  }
}

const mapStateToProps = (state) => ({
  adminData: state.auth.adminData,
  schoolProfileData: state.auth.schoolProfileData
})

// WithRouter is necessary for updating nav-state, should look into why
export default withRouter(
  connect(
    mapStateToProps,
    null
  )(HeaderMenu)
)
