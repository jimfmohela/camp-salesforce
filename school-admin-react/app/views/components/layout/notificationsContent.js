import React from "react"
import PropTypes from "prop-types"
import Icon from '~/app/assets/fonts/icon'
import { NavLink } from 'react-router-dom'
import { MenuItem } from 'react-aria-menubutton'
import Notification from './notification'
import NotificationZeroState from '~/app/views/components/zeroState/notifications'

const NotificationsContent = props => {
  let { data, className } = props

  return (
    <>
      <div className={className}>
        <div className='header-option'>
          <Icon icon='notifications-fill' />
          <h2 className='secondary'>NOTIFICATIONS</h2>
        </div>
        <MenuItem>
          <NavLink to='/settings' className='secondary header-option'>
            <Icon icon='settings-line' viewBox='0 0 22 22' />
            Settings
          </NavLink>
        </MenuItem>
      </div>
      {/* Checking if there is any unread notifications */}
      {data && data.length ?
        <>{data.slice(0, 4).map((notification, index) =>
          <>
            <MenuItem>
              <Notification notification={notification} allNotifications={data} index={index} />
            </MenuItem>
          </>
        )}
        </> :
        <>
          <NotificationZeroState />
          <br />
        </>
      }
      <MenuItem>
        <div className='m__mobile-notifications m__mobile-notifications--all'>
          <NavLink to='/notifications'>View all Notifications
            <Icon icon='arrow-up' />
          </NavLink>
        </div>
      </MenuItem>
    </>
  )
}

NotificationsContent.PropTypes = {
  className: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired
}

export default NotificationsContent
