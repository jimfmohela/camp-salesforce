import React from "react";
import PropTypes from "prop-types";

const InfoCard = props => {
  return (
    <div className="info-card">
      <h2 className="title">{props.title}</h2>
      <div className="student-info-block">
        {props.children && props.children}
      </div>
    </div>
  );
};

InfoCard.protoTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.element
};

export default InfoCard;
