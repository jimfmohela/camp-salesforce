import React from 'react'
import cn from 'classnames'
import { NavLink } from 'react-router-dom'
import Icon from '~/app/assets/fonts/icon'
import Dates from '~/app/assets/js/dates'
import Delete from '~/app/views/pages/layout/deleteRecord'
import { maskFS } from '../../../helpers/fullstory'


class Notification extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { notification, allNotifications, index } = this.props
    const statusClasses = cn(
      'm__notifications',
      'm__notifications--item',
      {
        closed: /(closed)/i.test(notification.status)
      }
    )

    return (
      <div className={statusClasses}>
        <NavLink to={`/agreements/agreement/${notification.whatID}`}>
          <Icon icon="applications-fill" />

          <aside className="notification-info">

            <div>
              {notification.eventAttributes ? maskFS(
                <h3 className="notification-sname">
                  {JSON.parse(notification.eventAttributes).studentName}
                </h3>
              ) : null}
              <h3>
                {/* notification.title */}
                's agreement changed to:
                {' '}
              </h3>
              <h3 className="notification-status">
                {notification.eventType}
              </h3>
            </div>

            <div className="notification-desc">
              <h5 className="notification-date">
                {Dates.formatToMMDDYYYY(notification.createdDate)}
              </h5>
              {notification.eventAttributes ? (
                <h5 className="notification-ids">
                  {'  '}
                  {/* notification.description */}
                  &#8226;
                  {'  '}
                  {
                    JSON.parse(notification.eventAttributes)
                      .vemoContractNumber
                  }
                  {'  '}
                  &#8226;
                  {' '}
                  {JSON.parse(notification.eventAttributes).programName}
                </h5>
              ) : null}
            </div>
          </aside>
        </NavLink>

        <Delete
          data={{
            deleteData: notification,
            objectName: 'eventInstance',
            allRecord: allNotifications,
            index
          }}
        />
        {' '}

      </div>
    )
  }
}

export default Notification
