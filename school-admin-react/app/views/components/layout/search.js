import React  from 'react'
import ClassNames from'classnames'
import Icon   from '~/app/assets/fonts/icon'
import { Wrapper, Button, Menu, MenuItem } from 'react-aria-menubutton'

const filters = ['all', 'students', 'programs', 'agreements']

class HeaderSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchQuery: '',
      filterOpen: false,
      filterOption: filters[0].toUpperCase()
    }
  }

  updateSearchQuery = (searchQuery) => {
    this.setState({ searchQuery })
  }

  render() {
    let { submitSearch } = this.props
    let { searchQuery, filterOpen, filterOption } = this.state
    let searchButtonClass = ClassNames(
      'header-search-button',
      'button',
      { 'search-button-active': searchQuery.length > 2 }
    )
    let filterClass = ClassNames(
      'header-filter-toggle',
      { 'filter-active': filterOpen }
    )
    
    return (
      <>
        <Icon icon='search' />
        <input
          className='search-input'
          placeholder='Search student, ISA Program, etc...'
          value={this.state.searchQuery}
          ref={this.props.setRef}
          onChange={(e) => this.updateSearchQuery(e.target.value)}/>

        <Wrapper
          className={filterClass}
          onSelection={value => this.setState({ filterOption: value[1] })}
          onMenuToggle={({isOpen}) => this.setState({ filterOpen: isOpen })}>

          <Button className='header-filter-wrapper button' >
            <p>{this.state.filterOption}</p>
            <Icon icon='arrow-down' />
          </Button>

          <Menu className='header-filter-options'>
            <ul>
              {filters.map((item, i) => 
                <li key={`menu-item-${i}`}>
                  <MenuItem> {item.toUpperCase()} </MenuItem>
                </li>
              )}
            </ul>
          </Menu>

          <button
            type='submit'
            className={searchButtonClass} 
            onClick={() => submitSearch(searchQuery, filterOption.toLowerCase())} >

            <h3>Search</h3>
          </button>
        </Wrapper>
      </>
    )
  }
}

export default HeaderSearch
