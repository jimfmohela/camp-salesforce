import React from 'react'
import HeaderMenu from './menu'

class Hamburger extends React.Component {
  constructor(props){
    super(props)
  }
  
  closeMenu = () => {
    this.refs.hamburgerRef.checked = false
  }

  render() {
    return (
      <div className='o__header-hamburger'>
        <input ref='hamburgerRef' id='hamburger-checkbox' type='checkbox' />
        <label className='header-hamburger--checkbox' htmlFor='hamburger-checkbox'>
          <span></span>
          <span></span>
          <span></span>
        </label>

        <HeaderMenu />

        <article className='header-hamburger--background' onClick={this.closeMenu}></article>
      </div>
    )
  }
}

export default Hamburger
