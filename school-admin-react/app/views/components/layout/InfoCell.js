import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@material-ui/core'
import cn from 'classnames'
import Icon from '~/app/assets/fonts/icon';
import Fab from '~/app/views/components/layout/Fab';
import Dates from '~/app/assets/js/dates';
import FlyOutHelp from '~/app/components/FlyOut/FlyOutHelp'

// TODO: This entire component is poorly implemented, way to many unique scenerios in what should be a generic component
// will need to be refactored later

const InfoCell = (props) => {
  const { text, list, placement, className } = props
  const [editActive, setEditActive] = useState(false)
  const data = props.data || 'N/A'
  const [value, setValue] = useState(data)

  const handleSetEditActive = () => {
    setEditActive(true)
  }
  const handleUpdateInput = async () => {
    // setValue(value);
    await props.updateInfoCellValue(value)
    await setEditActive(false)
  }

  const handleEditCancel = () => {
    setValue(data)
    setEditActive(false)
  }
  const handleChange = (event) => setValue(event.target.value)

  const fsclass = cn(props.className, 'info-cell')

  return (
    <div className={fsclass}>
      {editActive ? (
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end'
          }}
        >
          <div className="form-control">
            <input
              id="alternate-email"
              type="text"
              className="input"
              value={value}
              autoFocus
              onChange={handleChange}
            />
            <label htmlFor="alternate-email">Alternate Email</label>
          </div>

          <Fab ariaLabel="Cancel" onClick={handleEditCancel}>
            <Icon icon="close" viewBox="0 0 24 24" />
          </Fab>

          <Fab ariaLabel="Submit" variant="submit" onClick={handleUpdateInput}>
            <Icon icon="check" viewBox="0 0 24 24" />
          </Fab>
        </div>
      ) : (
          <div className="readOnlyInput">
            <div className="label">
              <p>{props.label}</p>
              {props.help && (
                <FlyOutHelp
                  text={text}
                  list={list}
                  placement={placement}
                  styleProps={{ width: '12.5625rem' }}
                />
              )}
            </div>
            <div className="content">
              <h4>
                {value}
                {' '}
              </h4>
              {props.editable && (
                <Fab ariaLabel="Edit" onClick={handleSetEditActive}>
                  <Icon icon="edit" viewBox="0 0 24 24" />
                </Fab>
              )}
            </div>
          </div>
        )}
    </div>
  )
}

InfoCell.propTypes = {
  data: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.date
  ]).isRequired,
  editable: PropTypes.boolean,
  help: PropTypes.boolean,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string
}

export default InfoCell
