import React from 'react'
import Icon from '~/app/assets/fonts/icon'
import ClassNames from 'classnames'
import { Wrapper, Button, Menu } from 'react-aria-menubutton'
import NotificationsContent from "~/app/views/components/layout/notificationsContent"

class Notifications extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  render() {
    let { data } = this.props
    let notificationWrapper = ClassNames(
      'o__header-desktop',
      'o__header-desktop--notifications',
      { 'notification-active': this.state.isOpen }
    )

    return (
      <Wrapper
        className={notificationWrapper}
        onSelection={this.updateFilter}
        onMenuToggle={({ isOpen }) => this.setState({ isOpen })}>

        <Button className='header-notifications-name' >
          {data && data.length ?
            <Icon icon='blue-dot' />
            : null}
          <Icon icon='notifications-fill' />
        </Button>

        <Menu className='header-notifications-list'>
          <NotificationsContent className='m__notifications m__notifications--header' data={data}/>
        </Menu>
      </Wrapper>
    )
  }
}

export default Notifications
