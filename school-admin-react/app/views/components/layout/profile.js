import React from 'react'
import { NavLink } from 'react-router-dom'
import Icon from '~/app/assets/fonts/icon'
import ClassNames from 'classnames'
import { Wrapper, Button, Menu, MenuItem } from 'react-aria-menubutton'

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false
    }
  }

  render() {
    let { data } = this.props
    let profileWrapper = ClassNames(
      'o__header-desktop',
      'o__header-desktop--profile',
      { 'profile-active': this.state.isOpen }
    )

    return (
      <Wrapper
        className={profileWrapper}
        onSelection={this.updateFilter}
        onMenuToggle={({ isOpen }) => this.setState({ isOpen })}>

        <Button className='header-profile-name' >
          <h5 className="main">{data.name}</h5>
          <Icon icon='arrow-down' />
        </Button>

        <Menu className='header-profile-links'>
          <MenuItem>
            <p>{data.name}</p>
          </MenuItem>
          <MenuItem>
            <NavLink activeClassName='is-active' to='/settings'>
              <Icon icon='settings' />
              <p>Settings</p>
            </NavLink>
          </MenuItem>

          <MenuItem>
            <NavLink activeClassName='is-active' to='/school details'>
              <Icon icon='colleges-line' />
              <p>School Details</p>
            </NavLink>
          </MenuItem>

          {/* regular link tag to do server-side call */}
          <MenuItem>
            <a href={`${window.baseRoute}/secur/logout.jsp`}>
              <Icon icon='sign-out' />
              <p>Sign out</p>
            </a>
          </MenuItem>
        </Menu>
      </Wrapper>
    )
  }
}

export default Profile
