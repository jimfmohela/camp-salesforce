import React from "react";
import Icon from "~/app/assets/fonts/icon";
import ReactDOM from "react-dom";

const RefreshModal = ({ onClose, open }) =>
  open
    ? ReactDOM.createPortal(
        <div className="m__header-menu m__header-menu--modal">
          <span className="header-menu-fadeout" />
          <aside className="header-menu-exit" onClick={onClose}>
          <Icon icon="close" viewBox="0 0 24 24"/>
          </aside>
          <aside className="header-menu-modal">
            <h1 className="main">Things may have changed</h1>
            <h4 className="main">
              Please reload this page to view updates from the Certification
              App.
            </h4>
            <button
              type="button"
              className="button button--general"
              onClick={() => window.location.reload()}
            >
              RELOAD
            </button>
          </aside>
        </div>,
        document.body
      )
    : null;

export default RefreshModal;
