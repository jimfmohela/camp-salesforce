import React from 'react'
import { matchPath } from 'react-router'
import { NavLink }  from 'react-router-dom'
import Icon from '~/app/assets/fonts/icon'
import Pluralize from 'pluralize'
import StudentsComponent from '../students/students';

class HeaderComponent extends React.Component {
  render() {
    let { filterPageName } = this.props
    // Update this page eventually
    let pageName = this.props.location.pathname.split('/')[1]

    // If params in match, load them
    let match = matchPath(this.props.history.location.pathname, {
      path: `/${pageName}/${Pluralize.singular(pageName)}/:id`
    })

    // temporary fix to make sure back button renders in Student 

    let backToStudents = false
    if (pageName === 'student') {
      match = matchPath(this.props.history.location.pathname, {
        path: `/${pageName}/:id`
      })
      pageName = 'Students'
      backToStudents = 'Students'
    }

    let tempPageName = ''
    if (pageName === 'programs') {
      tempPageName = 'ISA Programs'
    }

    return (
      <div className='o__header-main-title'>
        { match && match.params.id ? (
          <NavLink to={`/${backToStudents || pageName}`}>
            <Icon icon='arrow-up' />
            <h3 className='main'>{tempPageName ? tempPageName : pageName}</h3>
          </NavLink>
        ) : (
          <h1 className='main'>{tempPageName ? tempPageName : pageName}</h1>
        )}
      </div>
    )
  }
}

export default HeaderComponent
