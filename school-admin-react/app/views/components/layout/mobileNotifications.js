import React from 'react'
import Icon from '~/app/assets/fonts/icon'
import ClassNames from 'classnames'
import { Wrapper, Button, Menu } from 'react-aria-menubutton'
import NotificationsContent from "~/app/views/components/layout/notificationsContent"

class MobileNotifications extends React.Component {
  constructor(props) {
    super(props)
    this.mobileNotificationsRef = React.createRef()
    this.state = {
      mobileNotificationsOpen: false
    }
  }

  _onMenuToggle = () => {
    this.openSearch()
  }

  openSearch = () => {
    this.setState({ mobileNotificationsOpen: !this.state.mobileNotificationsOpen })
  }

  notificationIcon = (data) => {
    return (
      <Button className='header-notifications-name' >
        {data && data.length ?
          <Icon icon='blue-dot' />
          : null}
        <Icon icon='notifications-fill' />
      </Button>)
  }


  render() {
    const { data } = this.props
    const { mobileNotificationsOpen } = this.state

    const mobileNotificationsButton = ClassNames("header-filter-button", {
      "filter-open": mobileNotificationsOpen
    })

    const filterClass = ClassNames(
      "filter",
      "o__mobile-notifications",
      { "filter-active": mobileNotificationsOpen }
    )

    return (
      <Wrapper
        className={filterClass}
        closeOnSelection={true}
        onMenuToggle={this._onMenuToggle}>
        <Button className={mobileNotificationsButton}>
          {/* Providing the close or notification icon depending on the value of "mobileNotificationsOpen" */}
          {mobileNotificationsOpen ? <Icon icon="close" /> :
            <span className='header-notifications-name' >
              {data && data.length ?
                <Icon icon='blue-dot' />
                : null}
              <Icon icon='notifications-fill' />
            </span>
          }
        </Button>
        <Menu className="header-filter-options">
          <div className="m__mobile-notifications m__mobile-notifications--background" />
          <div className='m__mobile-notifications m__mobile-notifications--modal'>
            <NotificationsContent className='m__mobile-notifications m__mobile-notifications--header' data={data} />
          </div>
        </Menu>
      </Wrapper>
    )
  }
}

export default MobileNotifications