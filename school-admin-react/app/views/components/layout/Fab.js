import React, { useState } from "react";
import PropTypes from "prop-types";
import Icon from "~/app/assets/fonts/icon";

const InfoCell = props => {
  return (
    <button
      onClick={props.onClick}
      aria-label={props.ariaLabel}
      className={`fab ${props.variant === 'submit' ? 'fab-submit' : ''}`}
    >
      {props.children}
    </button>
  );
};

InfoCell.propTypes = {
  onClick: PropTypes.func.isRequired,
  ariaLabel: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  variant: PropTypes.oneOf(['submit']),
};

export default InfoCell;
