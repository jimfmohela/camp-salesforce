import React from "react";
import ClassNames from "classnames";
import Icon from "~/app/assets/fonts/icon";
import { Wrapper, Button, Menu, MenuItem } from "react-aria-menubutton";
const filters = ["all", "students", "programs", "agreements"];

class MobileSearch extends React.Component {
  constructor(props) {
    super(props);
    this.mobileSearchRef = React.createRef();
    this.state = {
      searchQuery: '',
      mobileSearchOpen: false,
      filterOption: filters[0]
    };
  }

  _onSelection = value => {
    this.setState({ filterOption: value });
  };

  _onMenuToggle = () => {
    this.openSearch();
  };

  _onSubmit = e => {
    let { submitSearch } = this.props;
    let { searchQuery, filterOption } = this.state;

    e.preventDefault();
    this.mobileSearchRef.current.handleClick();
    submitSearch(searchQuery, filterOption);
    this.setState({ mobileSearchOpen: false, filterOption: filters[0], searchQuery: '' });
  };

  _onChange = e => {
    this.updateSearchQuery(e.target.value);
  };

  updateSearchQuery = searchQuery => {
    this.setState({ searchQuery });
  };

  openSearch = () => {
    this.setState({mobileSearchOpen: !this.state.mobileSearchOpen});
  };

  render() {
    let { mobileSearchOpen } = this.state;

    let filterClass = ClassNames(
      "filter",
      "o__mobile-search",
      { "filter-active": mobileSearchOpen }
    );
    let mobileSearchButton = ClassNames("header-filter-button", {
      "filter-open": mobileSearchOpen
    });

    return (
      <Wrapper
        className={filterClass}
        closeOnSelection={false}
        onSelection={this._onSelection}
        onMenuToggle={this._onMenuToggle}
      >
        <Button ref={this.mobileSearchRef} className={mobileSearchButton}>
          {mobileSearchOpen ? <Icon icon="close" /> : <Icon icon="search" />}
        </Button>

        <Menu className="header-filter-options">
          <div className="m__mobile-search m__mobile-search--background" />
          <form
            className="m__mobile-search m__mobile-search--form"
            action="."
            onSubmit={this._onSubmit}
          >
            <aside className="mobile-search-input-bar">
              <Icon icon="search" />
              <input
                className="search-input"
                placeholder="Search Here"
                value={this.state.searchQuery}
                ref={this.props.setRef}
                type="search"
                onChange={this._onChange}
              />
            </aside>
            <div className="mobile-search-options">
              {filters.map((item, i) => (
                <>
                  <input
                    id={item}
                    type="radio"
                    name="option"
                    value={item}
                    checked={item === this.state.filterOption}
                  />
                  <MenuItem
                    value={item}
                    key={`menu-item-${i}`}
                    tag="label"
                    for={item}
                  >
                    <h3 className="main">{`${item.toUpperCase()}`}</h3>
                    <span>&#10003;</span>
                  </MenuItem>
                </>
              ))}
            </div>
          </form>
        </Menu>
      </Wrapper>
    );
  }
}

export default MobileSearch;
