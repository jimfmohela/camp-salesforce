import React from 'react'
import Dates from '~/app/assets/js/dates'
import InfoCell from '~/app/views/components/layout/InfoCell'
import InfoCard from '~/app/views/components/layout/InfoCard'
import Icon from '~/app/assets/fonts/icon'
import { Tooltip } from '@material-ui/core'

class ProgramInfo extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let { data } = this.props
    const showDatePeriod = (beginDate, endDate) =>
      endDate
        ? `${Dates.formatToMMDDYYYY(beginDate)} - ${Dates.formatToMMDDYYYY(
            endDate
          )}`
        : Dates.formatToMMDDYYYY(beginDate)
    return (
      <div className="o__program-info">
        <InfoCard title="Program Details">
          <InfoCell label="Program Name" data={data.programName} />
          <InfoCell label="Program Number" data={data.programNumber} />
          <InfoCell
            label="Program Status"
            data={data.programStatus}
            help={true}
            text="Three program statuses exist:"
            list={[
              'Planned – The registration period has not started yet.',
              'Open – The registration period has started.',
              'Closed – The registration period has ended.'
            ]}
            placement="top"
          ></InfoCell>
          <InfoCell label="Enrollment Type" data={data.enrollmentType} />
          {data.registrationBeginDate || data.registrationEndDate ? (
            <InfoCell
              label="Registration Period"
              data={showDatePeriod(
                data.registrationBeginDate,
                data.registrationEndDate
              )}
              help={true}
              text="The Registration Period represents the earliest and latest dates that a student or school can initiate an application or certification for ISA funding for a given program."
              placement="top"
            />
          ) : (
            <InfoCell
              label="Registration Period"
              data={`NA`}
              help={true}
              text="The Registration Period represents the earliest and latest dates that a student or school can initiate an application or certification for ISA funding for a given program."
              placement="top"
            />
          )}
          {data.enrollmentBeginDate || data.enrollmentEndDate ? (
            <InfoCell
              label="Enrollment Period"
              data={showDatePeriod(
                data.enrollmentBeginDate,
                data.enrollmentEndDate
              )}
              help={true}
              text="The enrollment period encompasses the attendance period(s) associated with a specific ISA program. ISA Programs can encompass one attendance period or be used for multiple attendance periods."
              placement="top"
            />
          ) : (
            <InfoCell
              label="Enrollment Period"
              data={`NA`}
              help={true}
              text="The enrollment period encompasses the attendance period(s) associated with a specific ISA program. ISA Programs can encompass one attendance period or be used for multiple attendance periods."
              placement="top"
            />
          )}

          <div className="info-cell">
            <div className="main">
              <p>Attendance Period(s)</p>
              <Tooltip title="An attendance period is a time period in which courses are in session." placement="top">
                <span>
                  <Icon icon='help' viewBox='0 0 24 24' />
                </span>
              </Tooltip>
            </div>
            <div className="m__program-attendance-period">
              <span>
                <h3 className="attendance-header">Begin Date</h3>
                <h3 className="attendance-header">End Date</h3>
              </span>

              {data.attendancePeriods.map((period) =>
                period.attendanceBeginDate || period.attendanceEndDate ? (
                  <span>
                    {period.attendanceBeginDate && (
                      <h4 className="attendance-date">
                        {showDatePeriod(period.attendanceBeginDate)}
                      </h4>
                    )}

                    {period.attendanceEndDate && (
                      <>
                        <h4 className="dash">—</h4>
                        <h4 className="attendance-date">
                          {showDatePeriod(period.attendanceEndDate)}
                        </h4>
                      </>
                    )}
                  </span>
                ) : null
              )}
            </div>
          </div>
        </InfoCard>
      </div>
    )
  }
}

export default ProgramInfo
