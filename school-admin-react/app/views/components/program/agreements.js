import React from 'react'

import VemoTable from '~/app/views/components/shared/table/table'
import VemoTableCell from '~/app/views/components/shared/table/cell'
import Icon from '~/app/assets/fonts/icon'
import Common from '~/app/assets/js/common'
import { maskFS } from '../../../helpers/fullstory'

const columns = [
  {
    header: 'Student Name',
    headerClassName: 'student-name',
    accessor: 'studentName',
    accessorCell(data) {
      return maskFS(
        <VemoTableCell
          cellClass={this.headerClassName}
          cellData={data[this.accessor]}
        >
          <Icon icon="applications-fill" />
          <h3 className="main">{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Vemo Contract Number',
    headerClassName: 'sp-number',
    accessor: 'vemoContractNumber',
    accessorCell(data) {
      return (
        <VemoTableCell
          cellClass={this.headerClassName}
          cellData={data[this.accessor]}
        >

          <h3 className="main">{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'School Program',
    headerClassName: 'school-program',
    accessor: 'programName',
    accessorCell(data) {
      return (
        <VemoTableCell
          cellClass={this.headerClassName}
          cellData={data[this.accessor]}
        >

          <h3 className="main">{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Funding Amount',
    headerClassName: 'funding',
    accessor: 'fundingAmount',
    accessorCell(data) {
      return (
        <VemoTableCell
          cellClass={this.headerClassName}
          cellData={data[this.accessor]}
        >
          <label className="table-cell--funding-amount-label">Funding Amount :&nbsp;</label>
          <h3 className="table-cell--funding-main">{Common.AmountFormat(data[this.accessor])}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Status',
    headerClassName: 'status',
    accessor: 'agreementStatus',
    accessorCell(data) {
      return (
        <VemoTableCell
          cellClass={this.headerClassName}
          cellData={data[this.accessor]}
        >

          <h3 className="main">{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }
]

class ProgramComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data } = this.props

    return (
      data.programID
        ? (
          <VemoTable
            numberOfColumns={columns.length}
            columns={columns}
            headerClass="o__programs-table__agreements o__programs-table__agreements--header"
            rowClass="o__programs-table__agreements o__programs-table__agreements--row"
            status="programStatus"
            data={data.agreements}
          />
        )
        : <div>No Agreements Available</div>
    )
  }
}

export default ProgramComponent
