import React from 'react'
import VemoTable from '~/app/views/components/shared/table/table'
import VemoTableCell from '~/app/views/components/shared/table/cell'
import Icon from '~/app/assets/fonts/icon'
import Dates from '~/app/assets/js/dates'
import { maskFS } from '../../../helpers/fullstory'


const columns = [
  {
    header: 'Status',
    headerClassName: 'notification-name',
    accessor: 'eventAttributes',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <Icon icon="applications-fill" />

          <div className="table-row--notification-name--table-item">
            {data.eventAttributes
              ? maskFS(
                <h3>
                  {JSON.parse(data.eventAttributes).studentName}
                  's
                </h3>
              )
              : null}
            <h3 className="main"> agreement changed to: </h3>
            <h3>{data.eventType}</h3>
          </div>


        </VemoTableCell>
      )
    }
  }, {
    header: 'Details',
    headerClassName: 'notification-date',
    accessor: 'createdDate',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <div>
            <h5 className="main">{Dates.formatToMMDDYY(data[this.accessor])}</h5>
            {data.eventAttributes
              ? (
                <>
                  {' '}
                  &#8226;
                  {' '}
                  <h5>
                    {' '}
                    {JSON.parse(data.eventAttributes).vemoContractNumber}
                  </h5>
                  {' '}
                  &#8226;
                  {' '}
                  <h5>{JSON.parse(data.eventAttributes).programName}</h5>
                </>
              )
              : null}
          </div>


        </VemoTableCell>
      )
    }
  }
  /* {
    header: '',
    headerClassName: 'notification-delete',
    accessor: 'eventInstanceID',
    accessorCell(data) {
      return (
        <VemoTableCell  cellData={data[this.accessor]} cellClass={this.headerClassName}>
            <Icon icon='delete' />
        </VemoTableCell>
      )
    }
  } */
]

class NotificationsComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data } = this.props
    return (
      <VemoTable
        numberOfColumns={columns.length}
        columns={columns}
        headerClass="o__notifications-table o__notifications-table--header"
        rowClass="o__notifications-table o__notifications-table--row"
        pathData={{
          path: '/agreements/agreement/',
          pathId: 'whatID'
        }}
        isDelete
        objectName="eventInstance"
        data={data}
      />
    )
  }
}

export default NotificationsComponent
