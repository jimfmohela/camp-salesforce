import React from 'react'

import VemoTable      from '~/app/views/components/shared/table/table'
import VemoTableCell  from '~/app/views/components/shared/table/cell'
import Icon           from '~/app/assets/fonts/icon'
import ErrorBoundary  from '~/app/views/pages/errors/errorBoundary'
import Loader         from '~/app/views/components/loader'
import HistoryZeroState from '~/app/views/components/zeroState/history'
import Dates from '~/app/assets/js/dates'


const columns = [
  {
    header: 'Communication Type',
    headerClassName: 'communication',
    accessor: 'type',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          <Icon icon='applications-fill' />
          <h3 className='main'>{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Date',
    headerClassName: 'date',
    accessor: 'communicationDateTime',
    accessorCell(data) {
      return (
        <VemoTableCell
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          <p className='main'>{Dates.formatToMMDDYYYY(data[this.accessor])}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Content',
    headerClassName: 'content',
    accessor: 'subject',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data.emailResource.subject}>

          <p className='main'>{data.emailResource.subject}</p>
        </VemoTableCell>
      )
    }
  }
]

class StudentHistory extends React.Component {
  constructor(props) {
    super(props)
  }

  _onClick = () => {
    
  }

  render() {
    let { communicationHistory } = this.props

    return (
      <ErrorBoundary>
        <Loader 
          data={communicationHistory.data} 
          zeroState={<HistoryZeroState />}
          isFetching={communicationHistory.isFetching} >

          <VemoTable 
            numberOfColumns={columns.length}
            columns={columns}
            headerClass='o__student-table__history o__student-table__history--header'
            rowClass='o__student-table__history o__student-table__history--row'
            onClick={this._onClick}
            data={communicationHistory.data} /> 
        </Loader>
      </ErrorBoundary>
    )
  }
}

export default StudentHistory
