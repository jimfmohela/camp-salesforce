/* eslint-disable react/prop-types */
import React, { Component } from 'react'
import Dates from '~/app/assets/js/dates'
import Common from '~/app/assets/js/common'
import InfoIcon from '~/app/assets/fonts/svgs/info-bold.svg'

class PaymentHistory extends Component {
  displayPaymentStatus = (paymentStatus) => {
    switch (paymentStatus) {
    case 'Refund':
      return 'Refunded'
    case 'Bounced':
      return 'Returned'
    case 'Complete':
      return 'Cleared'
    default:
      return paymentStatus
    }
  }

  render() {
    const { paymentHistoryData } = this.props
    return (
      <div>
        <h3 className="main">Payment History</h3>
        <br />
        <section className="t__settings-page">
          <div className="notifications-page notifications-page--notifications">
            <table className="pymnt-hstry-tbl">
              <thead>
                <th>Date</th>
                <th>Amount Paid by Student</th>
                <th>
                  <div className="tooltip">
                    Payment Status
                    <span className="tooltiptext">
                      Cleared - Vemo&apos;s bank acknowledges the transaction.
                      <br />
                      <br />
                      Returned - the payment didn&apos;t clear the student&apos;s bank.
                      <br />
                      <br />
                      Refunded - occurs when the student processed a payment in
                      error.
                    </span>
                    <InfoIcon
                      style={{
                        verticalAlign: 'middle',
                        width: '16px',
                        margin: '0 0 0 10px'
                      }}
                    />
                  </div>
                </th>
              </thead>
              <tbody>
                {(paymentHistoryData.data) ? paymentHistoryData.data.map((paymentInfo, i) => (
                  <tr>
                    <td key={i} className="titles">
                      {Dates.formatToMMDDYY(paymentInfo.paymentDate)}
                    </td>
                    <td>{Common.AmountFormat(paymentInfo.amount)}</td>
                    <td>{this.displayPaymentStatus(paymentInfo.status)}</td>
                  </tr>
                )) : ''}
              </tbody>
            </table>
          </div>
        </section>
      </div>
    )
  }
}
export default PaymentHistory
