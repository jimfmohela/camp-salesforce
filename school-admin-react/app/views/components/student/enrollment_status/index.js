import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import CircularProgress from '@material-ui/core/CircularProgress'
import { makeStyles } from '@material-ui/core/styles'
import cn from 'classnames'
import Icon from '~/app/assets/fonts/icon'
import Fab from '~/app/views/components/layout/Fab'
import Modal2 from '~/app/components/Modal/index'
import BlockButton from '~/app/components/BlockButton'
import Dates from '~/app/assets/js/dates'
import WithdrawnStatusSection from './withdrawn_section'
import GraduationStatusSection from './graduation_section'
import ExitModal from '~/app/components/ExitModal'
import ThanksModal from '~/app/components/ThanksModal'
import Snackbar from '~/app/components/Snackbar'
import LessThanHalfTimeSection from './less_than_half_time_section'
import LeaveOfAbsence from './LeaveOfAbsence'
import { hideInProd } from '~/app/utils/environments'
import './style.scss'
import { maskFS } from '../../../../helpers/fullstory'

const componentID = 'm-blockButton'
const useStyles = makeStyles(() => ({
  progress: {
    color: 'white',
    marginLeft: '-4em',
    marginTop: '0.8em',
    position: 'absolute'
  }
}))

const EnrollmentStatus = (props) => {
  const {
    recordLocked,
    academicEnrollmentStatuses,
    studentData,
    admin,
    enrollmentFormStatus
  } = props

  const data = props.data || 'N/A'
  const [value, setValue] = useState(data)
  const [showModal, setModal] = useState(false)
  const [currentStatus, setStatus] = React.useState(  {
    status: ''
  })
  const [isError, setError] = useState()
  const [errorObj, setErrorObj] = useState({
    status: false,
    date: false,
    minDateError: { error: false, minDate: '' },
    maxDateError: { error: false, maxDate: '' },
    effectiveDateMinError: { error: false, minDate: '' },
    effectiveDateMaxError: { error: false, maxDate: '' },
    effectiveDate: false,
    absenceStartDate: false,
    absenceStartMinDate: { error: false, minDate: '' },
    absenceStartMaxDate: { error: false, maxDate: '' },
    sixMonthValidation: false,
    isStartDateSmaller: false,
    absenceEndDate: false,
    absenceEndMinDate: { error: false, minDate: '' },
    absenceEndMaxDate: { error: false, maxDate: '' },
    other: false,
    notes: false
  })
  const [student, setStudent] = useState({})
  const [showExitModal, setExitModal] = useState(false)
  const [showThanksModal, setThanksModal] = useState(false)
  const [showAutomaticUpdateSuccess, setAutomaticUpdateSuccess] = useState(
    false
  )
  const [lastDateofAttendance, setlastDateofAttendance] = useState('')
  const [
    pendingEffectiveDateOfChange,
    setEffectiveDateOfChangeValue
  ] = useState('')
  const [notesComments, setnotesComments] = useState('')
  const [fieldDisabled, setFieldDisabled] = useState(false)
  const dateRef = useRef(null)
  const notesRef = useRef(null)
  const statusRef = useRef(null)
  const effectiveDateRef = useRef(null)
  const LOAStartDateRef = useRef(null)
  const LOAEndDateRef = useRef(null)
  const allRef = {
    dateRef,
    notesRef,
    effectiveDateRef,
    LOAStartDateRef,
    LOAEndDateRef
  }
  const [maxDate, setMaxDate] = useState(0)
  const [minDate, setMinDate] = useState(0)
  const inSchoolStatus = ['Full Time', 'Three Quarter Time', 'Half Time']
  const outOfSchoolStatus = ['Withdrawn', 'Graduated', 'Leave of Absence']
  const [
    LeaveOfAbsenceStartDateValue,
    setLeaveOfAbsenceStartDateValue
  ] = useState('')
  const [LeaveOfAbsenceEndDateValue, setLeaveOfAbsenceEndDateValue] = useState(
    ''
  )
  const classes = useStyles()
  const isLeaveOfAbsence =
    studentData.recordLocked === false &&
    studentData.status === 'Leave of Absence'
  useEffect(() => {
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth()
    const day = date.getDate()
    const maxValue = new Date(year + 10, month, day)
    setMaxDate(Dates.DateFormat(maxValue, 'sv-SE'))
    setMinDate(Dates.DateFormat('01/01/2015', 'sv-SE'))
  }, [])

  const clearCommonData = () => {
    setStudent({})
    setError(false)
    setFieldDisabled(false)
    setErrorObj((prevState) => ({
      ...prevState,
      status: false,
      date: false,
      notes: false,
      minDateError: { error: false, minDate: '' },
      maxDateError: { error: false, maxDate: '' },
      effectiveDate: false,
      effectiveDateMinError: { error: false, minDate: '' },
      effectiveDateMaxError: { error: false, maxDate: '' },
      absenceStartDate: false,
      absenceStartMinDate: { error: false, minDate: '' },
      absenceStartMaxDate: { error: false, maxDate: '' },
      absenceEndDate: false,
      absenceEndMinDate: { error: false, minDate: '' },
      absenceEndMaxDate: { error: false, maxDate: '' },
      other: false,
      sixMonthValidation: false,
      isStartDateSmaller: false
    }))
    setnotesComments('')
    setlastDateofAttendance('')
    setEffectiveDateOfChangeValue('')
    setLeaveOfAbsenceStartDateValue('')
    setLeaveOfAbsenceEndDateValue('')
  }
  const clearAllData = () => {
    setStatus('')
    clearCommonData()
    setModal(!showModal)
  }
  const setStatusModal = () => {
    !showModal ? clearAllData() : setModal(!showModal)
  }
  const statusModal = () => {
    setStatusModal()
    setExitModalStatus()
  }
  const SetAcademicenrollmentStatus = (event) => {
    const selectedState = event.target.value
    clearCommonData()
    setStatus({ ...currentStatus, status: selectedState })
    setStudent((prevState) => ({ ...prevState, pendingStatus: selectedState }))
  }
  const ReferToStatus = () => {
    statusRef.current.focus()
  }
  // ========================== Min Max Date Validation ===================================
  const CheckDateMin = (e) => {
    setError(true)
    if (e === 'LDA') {
      setErrorObj((prevState) => ({
        ...prevState,
        minDateError: {
          ...prevState.minDateError,
          error: true,
          minDate: Dates.formatToMMDDYYYY(minDate)
        }
      }))
    } else if (e === 'EffectiveDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        effectiveDateMinError: {
          ...prevState.effectiveDateMinError,
          error: true,
          minDate: Dates.formatToMMDDYYYY(minDate)
        }
      }))
    } else if (e === 'LeaveOfAbsenceStartDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        absenceStartMinDate: {
          ...prevState.absenceStartMinDate,
          error: true,
          minDate: Dates.formatToMMDDYYYY(minDate)
        }
      }))
    } else if (e === 'LeaveOfAbsenceEndDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        absenceEndMinDate: {
          ...prevState.absenceStartMDinate,
          error: true,
          minDate: Dates.formatToMMDDYYYY(minDate)
        }
      }))
    }
  }
  const CheckDateMax = (e) => {
    setError(true)
    if (e === 'LDA') {
      setErrorObj((prevState) => ({
        ...prevState,
        maxDateError: {
          ...prevState.maxDateError,
          error: true,
          maxDate: Dates.formatToMMDDYYYY(maxDate)
        }
      }))
    } else if (e === 'EffectiveDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        effectiveDateMaxError: {
          ...prevState.effectiveDateMaxError,
          error: true,
          maxDate: Dates.formatToMMDDYYYY(maxDate)
        }
      }))
    } else if (e === 'LeaveOfAbsenceStartDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        absenceStartMaxDate: {
          ...prevState.absenceStartMaxDate,
          error: true,
          maxDate: Dates.formatToMMDDYYYY(maxDate)
        }
      }))
    } else if (e === 'LeaveOfAbsenceEndDate') {
      setErrorObj((prevState) => ({
        ...prevState,
        absenceEndMaxDate: {
          ...prevState.absenceStartMaxDate,
          error: true,
          maxDate: Dates.formatToMMDDYYYY(maxDate)
        }
      }))
    }
  }
  const dateDiffernce = () => {
    if (
      LeaveOfAbsenceEndDateValue.length > 0 &&
      LeaveOfAbsenceStartDateValue.length > 0
    ) {
      const dateStart = new Date(LeaveOfAbsenceStartDateValue)
      const dateEnd = new Date(LeaveOfAbsenceEndDateValue)
      const datediff =
        (dateEnd.getTime() - dateStart.getTime()) / (1000 * 3600 * 24)
      if (datediff > 182.5) {
        setError(true)
        setErrorObj((prevState) => ({ ...prevState, sixMonthValidation: true }))
      }
      if (datediff < 0) {
        setError(true)
        setErrorObj((prevState) => ({ ...prevState, isStartDateSmaller: true }))
      }
    }
  }
  const CheckMinMaxDate = (e, event) => {
    const receivedDate = new Date(event.target.value).getTime()
    const minDateTime = new Date(minDate).getTime()
    const maxDateTime = new Date(maxDate).getTime()

    event.target.value &&
      (receivedDate < minDateTime && CheckDateMin(e),
      receivedDate > maxDateTime && CheckDateMax(e))
    if (
      currentStatus.status === 'Leave of Absence' &&
      errorObj.absenceStartMinDate.error === false &&
        errorObj.absenceStartMaxDate.error === false &&
        errorObj.absenceEndMinDate.error === false &&
        errorObj.absenceEndMaxDate.error === false
    ) {
      dateDiffernce()
    }
  }
  // ============================ END =====================================================

  // =============== SUBMIT BUTTON FUNCTIONALITIES ====================================
  const AcademicEnrollmentAPICall = (automaticUpdate) => {
    const studentArray = []
    student.changeRequestedBy = admin.name
    student.dateOfLastStatusChange = Dates.DateFormat(new Date(), 'sv-SE')
    student.academicEnrollmentID = studentData.academicEnrollmentID
    student.studentID = studentData.studentID
    studentArray.push(student)
    props.UpdateAcademicEnrollmentStatus(studentArray, automaticUpdate)
  }
  const CheckCommonValidation = () => {
    if (
      errorObj.minDateError.error === true ||
      errorObj.maxDateError.error === true
    ) {
      setError(true)
      setFieldDisabled(false)
    }
    if (notesComments && notesComments.length > 500) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, notes: true }))
      setFieldDisabled(false)
    }
  }
  const SubmitWithdrwanAndGraduated = () => {
    CheckCommonValidation()
    if (!lastDateofAttendance) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, date: true }))
      setFieldDisabled(false)
    }
    if (
      lastDateofAttendance &&
      notesComments.length < 500 &&
      errorObj.minDateError.error === false &&
      errorObj.maxDateError.error === false
    ) {
      AcademicEnrollmentAPICall(false)
    }
  }

  const SubmitLTHT = () => {
    CheckCommonValidation()
    if (!lastDateofAttendance) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, date: true }))
      setFieldDisabled(false)
    }
    if (!pendingEffectiveDateOfChange) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, effectiveDate: true }))
      setFieldDisabled(false)
    }
    if (
      errorObj.effectiveDateMinError.error ||
      errorObj.effectiveDateMaxError.error
    ) {
      setError(true)
      setFieldDisabled(false)
    }
    if (
      lastDateofAttendance &&
      pendingEffectiveDateOfChange &&
      errorObj.effectiveDateMinError.error === false &&
      errorObj.effectiveDateMaxError.error === false &&
      notesComments.length < 500 &&
      errorObj.minDateError.error === false &&
      errorObj.maxDateError.error === false
    ) {
      AcademicEnrollmentAPICall(false)
    }
  }
  const SubmitLOA = () => {
    if (!LeaveOfAbsenceStartDateValue) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, absenceStartDate: true }))
      setFieldDisabled(false)
    }
    if (!LeaveOfAbsenceEndDateValue) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, absenceEndDate: true }))
      setFieldDisabled(false)
    }
    if (notesComments && notesComments.length > 500) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, notes: true }))
      setFieldDisabled(false)
    }
    if (
      errorObj.absenceStartMinDate.error ||
      errorObj.absenceStartMaxDate.error ||
      errorObj.absenceEndMinDate.error ||
      errorObj.absenceEndMaxDate.error ||
      errorObj.sixMonthValidation ||
      errorObj.isStartDateSmaller
    ) {
      setError(true)
      setFieldDisabled(false)
    }
    if (
      LeaveOfAbsenceStartDateValue &&
      LeaveOfAbsenceEndDateValue &&
      errorObj.absenceStartMinDate.error === false &&
      errorObj.absenceStartMaxDate.error === false &&
      errorObj.absenceEndMinDate.error === false &&
      errorObj.absenceEndMaxDate.error === false &&
      errorObj.sixMonthValidation === false &&
      errorObj.isStartDateSmaller === false &&
      notesComments.length < 500
    ) {
      AcademicEnrollmentAPICall(false)
    }
  }
  const SubmitAcademicEnrollmentStatus = () => {
    setFieldDisabled(true)
    if (!currentStatus.status) {
      setError(true)
      setErrorObj((prevState) => ({ ...prevState, status: true }))
      setFieldDisabled(false)
    } else if (
      (value === 'Three Quarter Time' ||
        value === 'Half Time' ||
        value === 'Full Time') &&
      (currentStatus.status === 'Three Quarter Time' ||
        currentStatus.status === 'Half Time' ||
        currentStatus.status === 'Full Time')
    ) {
      AcademicEnrollmentAPICall(true)
    } else if (
      currentStatus.status === 'Withdrawn' ||
      currentStatus.status === 'Graduated'
    ) {
      SubmitWithdrwanAndGraduated()
    } else if (
      currentStatus.status === 'Less than Half Time' ||
      (value === 'Less than Half Time' &&
        inSchoolStatus.includes(currentStatus.status)) ||
      (outOfSchoolStatus.includes(value) &&
        inSchoolStatus.includes(currentStatus.status))
    ) {
      SubmitLTHT()
    } else if (currentStatus.status === 'Leave of Absence') {
      SubmitLOA()
    } else {
      AcademicEnrollmentAPICall(false)
    }
  }

  // ===================================== END ===========================================

  const setExitModalStatus = () => {
    if (currentStatus.status) {
      setExitModal(!showExitModal)
    } else {
      setStatusModal()
    }
  }

  const setThanksModalStatus = () => {
    setThanksModal(!showThanksModal)
  }

  const setAutomaticUpdateSuccessStatus = () => {
    setAutomaticUpdateSuccess(!showAutomaticUpdateSuccess)
  }
  const onAutomaticUpdateSuccessStatus = () => {
    props.UpdatedAcademicEnrollmentStatus(false)
  }

  const ChangelastDateofAttendance = (event) => {
    const lastDateofAttendanceValue = event.target.value
        
    setError(false)
    setErrorObj((prevState) => ({
      ...prevState,
      minDateError: { ...prevState.minDateError, error: false, minDate: '' },
      maxDateError: { ...prevState.maxDateError, error: false, maxDate: '' },
      date: false
    }))
    setlastDateofAttendance(lastDateofAttendanceValue)
    if (currentStatus.status === 'Withdrawn') {
      setStudent((prevState) => ({
        ...prevState,
        pendingLastDateOfAttendance: Dates.DateFormat(lastDateofAttendanceValue, 'sv-SE')
      }))
    }
    if (!(currentStatus.status === 'Withdrawn')) {
      setStudent((prevState) => ({
        ...prevState,
        pendingGraduationDate: Dates.DateFormat(lastDateofAttendanceValue, 'sv-SE')
      }))
    }
  }

  const ChangeNotesComments = (event) => {
    const notesValue = event.target.value
    setnotesComments(notesValue)
    setStudent((prevState) => ({ ...prevState, notesComments: notesValue }))
    setError(false)
    setErrorObj((prevState) => ({ ...prevState, notes: false }))
  }

  const SetEffectiveDateOfChange = (event) => {
    const effectiveDate = event.target.value
    setError(false)
    setErrorObj((prevState) => ({
      ...prevState,
      effectiveDateMinError: {
        ...prevState.effectiveDateMinError,
        error: false,
        minDate: ''
      },
      effectiveDateMaxError: {
        ...prevState.effectiveDateMaxError,
        error: false,
        maxDate: ''
      }
    }))
    setEffectiveDateOfChangeValue(effectiveDate)
    setStudent((prevState) => ({
      ...prevState,
      pendingEffectiveDateOfChange: Dates.DateFormat(effectiveDate, 'sv-SE')
    }))
    setErrorObj((prevState) => ({ ...prevState, effectiveDate: false }))
  }
  const SetLeaveOfAbsenceStartDate = (event) => {
    const leaveofAbsenceStartDate = event.target.value
    setError(false)
    setErrorObj((prevState) => ({
      ...prevState,
      absenceStartDate: false,
      absenceStartMinDate: {
        ...prevState.absenceStartMinDate,
        error: false,
        minDate: ''
      },
      absenceStartMaxDate: {
        ...prevState.absenceStartMaxDate,
        error: false,
        maxDate: ''
      },
      isStartDateSmaller: false,
      sixMonthValidation: false
    }))
    setLeaveOfAbsenceStartDateValue(leaveofAbsenceStartDate)
    setStudent((prevState) => ({
      ...prevState,
      pendingLeaveOfAbsenceStartDate: Dates.DateFormat(leaveofAbsenceStartDate, 'sv-SE')
    }))
  }
  const SetLeaveOfAbsenceEndDate = (event) => {
    const leaveofAbsenceEndDate = event.target.value
    setError(false)
    setErrorObj((prevState) => ({
      ...prevState,
      absenceEndDate: false,
      absenceEndMinDate: {
        ...prevState.absenceEndMinDate,
        error: false,
        minDate: ''
      },
      absenceEndMaxDate: {
        ...prevState.absenceEndMaxDate,
        error: false,
        maxDate: ''
      },
      sixMonthValidation: false,
      isStartDateSmaller: false
    }))
    setStudent((prevState) => ({
      ...prevState,
      pendingLeaveOfAbsenceEndDate: Dates.DateFormat(leaveofAbsenceEndDate, 'sv-SE')
    }))
    setLeaveOfAbsenceEndDateValue(event.target.value)
  }
  useEffect(() => {
    if (
      enrollmentFormStatus &&
      enrollmentFormStatus.status &&
      !enrollmentFormStatus.automaticUpdate
    ) {
      setThanksModalStatus()
    } else if (
      enrollmentFormStatus &&
      enrollmentFormStatus.status &&
      enrollmentFormStatus.automaticUpdate
    ) {
      setAutomaticUpdateSuccessStatus()
    }
  }, [
    data
  ])
  const { firstName, lastName } = props.studentData
  const fullName = [firstName, lastName].reduce((o, s) =>
    o && o.length > 0
      ? s && s.length > 0
        ? `${o} ${s}`
        : o
      : s && s.length > 0
      ? s
      : ''
  )

  return (
    <>
      <div className={cn(props.className, 'info-cell')}>
        <div
          style={{
            display: 'flex',
            alignItems: isLeaveOfAbsence ? 'center' : 'flex-end'
          }}
        >
          <div
            className="readOnlyInput"
            style={{ flexBasis: isLeaveOfAbsence ? '70%' : 'auto' }}
          >
            <p className="label">{props.label}</p>
            <div className="content">
              <h4>
                {value}
                {studentData.recordLocked === false &&
                  studentData.status === 'Leave of Absence' &&
                  `, ${Dates.formatToMMDDYYYY(
                    studentData.leaveofAbsenceStartDate
                  )} - ${Dates.formatToMMDDYYYY(
                    studentData.leaveofAbsenceEndDate
                  )} `}
                {studentData.recordLocked &&
                  studentData.pendingStatus === 'Leave of Absence' &&
                  `, ${Dates.formatToMMDDYYYY(
                    studentData.pendingLeaveOfAbsenceStartDate
                  )} - ${Dates.formatToMMDDYYYY(
                    studentData.pendingLeaveOfAbsenceEndDate
                  )}`}
                {recordLocked ? ' (Pending confirmation)' : null}
              </h4>
              {!recordLocked && (
                <Fab ariaLabel="Edit" onClick={setStatusModal}>
                  <Icon icon="edit" viewBox="0 0 24 24" />
                </Fab>
              )}
            </div>
          </div>
        </div>
        {recordLocked ? (
          <div className="pending-enrollment-message">
            <p>
              {
                'We have notified the Vemo Servicing Team of your request to change '
              }
              {maskFS(<p className="capitalize-text">{`${fullName}'s`}</p>)}
              {` enrollment status to ${studentData.pendingStatus}. The team will confirm and update `}
              {maskFS(
                <p className="capitalize-text">
                  {firstName && `${firstName}'s`}
                </p>
              )}
              {` information within 2-4 business days.`}
            </p>
          </div>
        ) : props.studentData.dateOfLastApprovedChange &&
          Dates.DiffInDays(props.studentData.dateOfLastApprovedChange) <= 30 ? (
          <div className="pending-enrollment-message">
            <p>
              Updated by {props.studentData.changeRequestedBy || 'N/A'} on{' '}
              {Dates.formatToMMDDYYYY(props.studentData.dateOfLastStatusChange)}{' '}
            </p>
          </div>
        ) : null}
      </div>
      <Modal2
        open={showModal}
        className={showExitModal ? 'hidden' : 'show'}
        title="Edit Enrollment Status"
        setStatusModal={setStatusModal}
        isError={isError}
        errorObj={errorObj}
        ReferToStatus={ReferToStatus}
        setExitModalStatus={setExitModalStatus}
        allRef={allRef}
      >
        <div className="statusContainer">
          <div className="statusContainer--inner">
            <h6 className="current-status-label">Current Enrollment Status</h6>
            <h4>{value}</h4>
            <br />
            <br />
            <div
              className={errorObj.status === false ? 'withStatus' : 'no-status'}
            >
              <div style={{ display: 'flex', flexDirection: 'column-reverse' }}>
                <select
                  value={currentStatus.status}
                  onChange={SetAcademicenrollmentStatus}
                  disabled={fieldDisabled}
                  ref={statusRef}
                >
                  <option value="">Select an enrollment status</option>
                  {academicEnrollmentStatuses.data.map((option) =>
                    option !== value ? (
                      <>
                        <option key={option} value={option}>
                          {option}
                        </option>
                      </>
                    ) : null
                  )}
                </select>
                <h5 className="new-status-label">New Enrollment Status</h5>
              </div>

              <p>Please select an enrollment status before submitting.</p>
            </div>
            <br />
          </div>
          {currentStatus.status === 'Withdrawn' && (
            <WithdrawnStatusSection
              studentData={studentData}
              ChangelastDateofAttendance={ChangelastDateofAttendance}
              lastDateofAttendance={lastDateofAttendance}
              ChangeNotesComments={ChangeNotesComments}
              notesComments={notesComments}
              errorObj={errorObj}
              fieldDisabled={fieldDisabled}
              newStatus={currentStatus.status}
              ref={allRef}
              CheckMinMaxDate={CheckMinMaxDate}
              maxDate={maxDate}
              minDate={minDate}
              inSchoolStatus={inSchoolStatus}
            />
          )}
          {currentStatus.status === 'Graduated' && (
            <GraduationStatusSection
              studentData={studentData}
              ChangelastDateofAttendance={ChangelastDateofAttendance}
              lastDateofAttendance={lastDateofAttendance}
              ChangeNotesComments={ChangeNotesComments}
              notesComments={notesComments}
              errorObj={errorObj}
              fieldDisabled={fieldDisabled}
              newStatus={currentStatus.status}
              ref={allRef}
              CheckMinMaxDate={CheckMinMaxDate}
              maxDate={maxDate}
              minDate={minDate}
              inSchoolStatus={inSchoolStatus}
            />
          )}
          {/* show less than half time status on selecting 1) lessthan half time
              2) less than half time to in school 3) out of school to inschool */}
          {(currentStatus.status === 'Less than Half Time' ||
            (value === 'Less than Half Time' &&
              inSchoolStatus.includes(currentStatus.status)) ||
            (outOfSchoolStatus.includes(value) &&
              inSchoolStatus.includes(currentStatus.status))) && (
            <LessThanHalfTimeSection
              studentData={studentData}
              ChangeNotesComments={ChangeNotesComments}
              notesComments={notesComments}
              errorObj={errorObj}
              fieldDisabled={fieldDisabled}
              newStatus={currentStatus.status}
              ref={allRef}
              CheckMinMaxDate={CheckMinMaxDate}
              maxDate={maxDate}
              minDate={minDate}
              SetEffectiveDateOfChange={SetEffectiveDateOfChange}
              pendingEffectiveDateOfChange={pendingEffectiveDateOfChange}
              ChangelastDateofAttendance={ChangelastDateofAttendance}
              inSchoolStatus={inSchoolStatus}
              lastDateofAttendance={lastDateofAttendance}
            />
          )}
          {currentStatus.status === 'Leave of Absence' && (
            <LeaveOfAbsence
              studentData={studentData}
              ChangeNotesComments={ChangeNotesComments}
              notesComments={notesComments}
              errorObj={errorObj}
              fieldDisabled={fieldDisabled}
              newStatus={currentStatus.status}
              ref={allRef}
              CheckMinMaxDate={CheckMinMaxDate}
              maxDate={maxDate}
              minDate={minDate}
              LeaveOfAbsenceEndDateValue={LeaveOfAbsenceEndDateValue}
              SetLeaveOfAbsenceEndDate={SetLeaveOfAbsenceEndDate}
              SetLeaveOfAbsenceStartDate={SetLeaveOfAbsenceStartDate}
              LeaveOfAbsenceStartDateValue={LeaveOfAbsenceStartDateValue}
              inSchoolStatus={inSchoolStatus}
            />
          )}
          <div className="statusContainer--inner">
            <div className="button_section">
              <BlockButton
                type="button"
                className={`${componentID}-cancel`}
                onClick={setExitModalStatus}
              >
                CANCEL
              </BlockButton>
              <BlockButton
                type="button"
                className={`${componentID}-submit`}
                onClick={SubmitAcademicEnrollmentStatus}
                disabled={fieldDisabled}
              >
                SUBMIT
              </BlockButton>
              {fieldDisabled && (
                <CircularProgress
                  size={33}
                  className={classes.progress}
                  thickness={4}
                />
              )}
            </div>
          </div>
        </div>
      </Modal2>
      <ExitModal
        open={showExitModal}
        setExitModalStatus={setExitModalStatus}
        SubmitAcademicEnrollmentStatus={SubmitAcademicEnrollmentStatus}
        setEditModalStatus={statusModal}
      />
      <ThanksModal
        open={showThanksModal}
        data={data}
        studentData={studentData}
        setThanksModalStatus={setThanksModalStatus}
        SubmitAcademicEnrollmentStatus={SubmitAcademicEnrollmentStatus}
        UpdatedAcademicEnrollmentStatus={props.UpdatedAcademicEnrollmentStatus}
      />
      <Snackbar
        open={showAutomaticUpdateSuccess}
        status="success"
        message="Enrollment status updated successfully."
        autoHideDuration={6000}
        onClose={setAutomaticUpdateSuccessStatus}
        onExited={onAutomaticUpdateSuccessStatus}
        vertical="top"
        horizontal="center"
      />
    </>
  )
}
EnrollmentStatus.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  data: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  recordLocked: PropTypes.bool.isRequired,
  UpdateAcademicEnrollmentStatus: PropTypes.func.isRequired,
  academicEnrollmentStatuses: PropTypes.arrayOf(PropTypes.string).isRequired,
  admin: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired
}

export default EnrollmentStatus
