import React from 'react'
import PropTypes from 'prop-types'
import '../style.scss'

const DateSection = (props, ref) => {
  const {
    maxDate,
    minDate,
    fieldDisabled,
    CheckMinMaxDate,
    setDate,
    dateValue,
    checkError,
    dateRefValue,
    inputMessage,
    minDateError,
    maxDateError,
    fieldName,
    errorMessage,
    label
  } = props
  // const isSafari=!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/) // this is working for all safari browsers

  // TODO: [SCHOOL-852] Avoid Browser Detection Find a better solution, browser detection is bleh
  const isSafari = window.safari != undefined // this is working for Mac Safari only
  // console.log('is safari browser',isSafari,navigator.userAgent)
  return (
    <>
      <section>
        <div
          className={
            checkError || minDateError || maxDateError
              ? 'dateError'
              : 'dates-section'
          }
        >
          <div style={{ display: 'flex', flexDirection: 'column-reverse' }}>
            <input
              type="date"
              onChange={setDate}
              max={maxDate}
              min={minDate}
              value={dateValue}
              disabled={fieldDisabled}
              ref={ref[`${dateRefValue}`]}
              onBlur={(e) => CheckMinMaxDate(`${fieldName}`, e)}
              style={{
                background: isSafari && '#fff'
              }}
              placeholder="mm/dd/yyyy"
            />
            <h5>{label}</h5>
          </div>
          <h6 className="input_message">{inputMessage}</h6>
          {!(minDateError || maxDateError) ? (
            <h6 className="input-error-label">{errorMessage}</h6>
          ) : null}
        </div>
      </section>
    </>
  )
}
DateSection.defaultProps = {
  dateValue: '',
  inputMessage: '',
  checkError: false
}
DateSection.propTypes = {
  setDate: PropTypes.func.isRequired,
  fieldDisabled: PropTypes.bool.isRequired,
  dateValue: PropTypes.instanceOf(Date),
  CheckMinMaxDate: PropTypes.func.isRequired,
  minDate: PropTypes.instanceOf(Date).isRequired,
  maxDate: PropTypes.instanceOf(Date).isRequired,
  label: PropTypes.string.isRequired,
  inputMessage: PropTypes.string,
  checkError: PropTypes.bool,
  minDateError: PropTypes.bool.isRequired,
  maxDateError: PropTypes.bool.isRequired,
  fieldName: PropTypes.string.isRequired,
  dateRefValue: PropTypes.string.isRequired
}

const forwardInput = React.forwardRef(DateSection)
export default forwardInput
