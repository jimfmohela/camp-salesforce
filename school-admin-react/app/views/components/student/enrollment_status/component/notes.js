import React from 'react'
import PropTypes from 'prop-types'
import '../style.scss'

const NoteSection = (props, ref) => {
  const { errorObj, ChangeNotesComments, notesComments, fieldDisabled, label, inputMessage } = props
  return (
    <>
      <section>
        <div className={errorObj.notes ? 'notesError' : 'notes-section'}>
          <div style={{ display: 'flex', flexDirection: 'column-reverse' }}>
            <textarea
              onChange={ChangeNotesComments}
              value={notesComments}
              disabled={fieldDisabled}
              className="notesBox"
              ref={ref.notesRef}
            />
            <div>
              <h5 className="label-for-notes">{label}</h5>
              <h6 className="char_limits">
                {notesComments.length}
                /500 characters
              </h6>
            </div>
          </div>
          <h6 className="input_message">
            {inputMessage}
          </h6>
          <h6 className="input-error-label">Please make sure your notes/comments are fewer than 500 characters</h6>
        </div>
      </section>
    </>
  )
}
NoteSection.defaultProps = {
  notesComments: '',
  inputMessage: ''
}
NoteSection.propTypes = {
  errorObj: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  ChangeNotesComments: PropTypes.func.isRequired,
  fieldDisabled: PropTypes.bool.isRequired,
  notesComments: PropTypes.string,
  label: PropTypes.string.isRequired,
  inputMessage: PropTypes.string
}

const forwardInput = React.forwardRef(NoteSection)
export default forwardInput
