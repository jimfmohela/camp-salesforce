import React from 'react'
import PropTypes from 'prop-types'
import '../style.scss'

const MoreInformationSection = (props) => {
  const { newStatus } = props
  return (
    <>
      <div className="more_information">
        <div style={{ textAlign: 'center', marginBottom: '1em' }}>
          <span>We need more information</span>
        </div>
        {newStatus === 'Withdrawn' ? (
          <p>
            Please review the student’s information and agreement information
            below and provide us with additional information that our servicing
            representatives will need in order to confirm this adjustment to the
            student’s academic status.
          </p>
        ) : (
          <p>
            Please review the student’s information
            below and provide us with additional information that our
            servicing representatives will need in order to confirm
            this adjustment to the student’s academic status.
          </p>
        )}
      </div>
    </>
  )
}
MoreInformationSection.propTypes = {
  newStatus: PropTypes.string.isRequired
}

export default MoreInformationSection
