import React from 'react'
import PropTypes from 'prop-types'
import Common from '~/app/assets/js/common'
import '../style.scss'

const AgreementShare = (props) => {
  const { studentData } = props
  const activeAgreements = studentData.agreements.filter((data) => (data.active))
  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h2 className="innerheader">
            Student’s Active Income - Share Agreement(s)
        </h2>
      </div>
      <section>
        { (activeAgreements && activeAgreements.length > 0) ? (
          <>
            {activeAgreements.map((agreement) =>
              (
                <div className="agreement_section">
                  <h3 className="main">{agreement.programName}</h3>
                  <br />
                  <h5 className="program-info">
                    {agreement.vemoContractNumber}
                    &#8226;
                    {' '}
                    {Common.AmountFormat(agreement.fundingAmount)}
                  </h5>
                </div>
              ))}
          </>
        ) : <h5 className="no_student_message">This student does not have any Active Income Share Agreements currently. </h5> }
      </section>
    </>
  )
}
AgreementShare.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired
}

export default AgreementShare
