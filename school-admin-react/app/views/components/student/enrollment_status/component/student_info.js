import React from 'react'
import PropTypes from 'prop-types'
import Dates from '~/app/assets/js/dates'
import '../style.scss'
import { maskFS } from '../../../../../helpers/fullstory'

const StudentInformation = (props) => {
  const { studentData, newStatus, inSchoolStatus } = props
  const { firstName, lastName } = props.studentData
  return (
    <>
      <div style={{ textAlign: 'center' }}>
        <h2 className="innerheader">STUDENT&apos;S INFORMATION</h2>
      </div>
      <section className="studentInfoSection">
        <div className="studentInfo">
          <label>Student Name </label>
          <h4>
            {maskFS(firstName && firstName, true)}
            {maskFS(lastName, true)}
          </h4>
        </div>
        <div className="studentInfo">
          <label>Vemo Account Number </label>
          <h4>{studentData.vemoAccountNumber}</h4>
        </div>
        <div className="studentInfo">
          <label>
            {(newStatus === 'Withdrawn' || newStatus === 'Graduated') && 'Expected Graduation Date'}
            {(newStatus === 'Less than Half Time' || newStatus === 'Leave of Absence') && 'Current Graduation Date'}
            {(inSchoolStatus.includes(newStatus)) && 'Graduation Date'}
          </label>


          <h4>{Dates.formatToMMDDYYYY(studentData.graduationDate)}</h4>
        </div>
        {newStatus === 'Withdrawn'
          ? (
            <div className="studentInfo">
              <label>
                Total Number of ISA Agreement(s)
              </label>
              <h4>{studentData.agreements.length}</h4>
            </div>
          )
          : null}
      </section>
    </>
  )
}
StudentInformation.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  newStatus: PropTypes.string.isRequired,
  inSchoolStatus: PropTypes.arrayOf(PropTypes.string).isRequired
}

export default StudentInformation
