import React from 'react'
import PropTypes from 'prop-types'
import './extra_section.scss'
import StudentInformation from './component/student_info'
import DateSection from './component/date'
import NoteSection from './component/notes'
import MoreInformationSection from './component/more_info'

const LessThanHalfTimeSection = (props, ref) => {
  const {
    studentData,
    ChangelastDateofAttendance,
    lastDateofAttendance,
    ChangeNotesComments,
    notesComments,
    fieldDisabled,
    errorObj,
    CheckMinMaxDate,
    minDate,
    maxDate,
    newStatus,
    SetEffectiveDateOfChange,
    pendingEffectiveDateOfChange,
    inSchoolStatus,
    oldStatus
  } = props
  const isOldGraduationDate = ((oldStatus === 'Graduated' || oldStatus === 'Withdrawn')
  && (inSchoolStatus.includes(newStatus) || newStatus === 'Less than Half Time'))
  return (
    <div className="withdrawnContainer">
      <div className="withdrawnContainer--inner">
        <MoreInformationSection newStatus={newStatus} />
        <StudentInformation
          studentData={studentData}
          newStatus={newStatus}
          inSchoolStatus={inSchoolStatus}
        />
        <DateSection
          errorObj={errorObj}
          setDate={ChangelastDateofAttendance}
          minDate={minDate}
          maxDate={maxDate}
          dateValue={lastDateofAttendance}
          fieldDisabled={fieldDisabled}
          CheckMinMaxDate={CheckMinMaxDate}
          ref={ref}
          label={isOldGraduationDate ? 'Expected Graduation Date' : 'New Expected Graduation Date'}
          checkError={errorObj.date}
          dateRefValue="dateRef"
          inputMessage="If there is a change to the expected grad date, enter it here."
          minDateError={errorObj.minDateError.error}
          maxDateError={errorObj.maxDateError.error}
          fieldName="LDA"
          errorMessage="Please enter a date"
        />
        <DateSection
          errorObj={errorObj}
          setDate={SetEffectiveDateOfChange}
          minDate={minDate}
          maxDate={maxDate}
          dateValue={pendingEffectiveDateOfChange}
          fieldDisabled={fieldDisabled}
          CheckMinMaxDate={CheckMinMaxDate}
          ref={ref}
          label="What is the effective date of this change?"
          checkError={errorObj.effectiveDate}
          dateRefValue="effectiveDateRef"
          minDateError={errorObj.effectiveDateMinError.error}
          maxDateError={errorObj.effectiveDateMaxError.error}
          fieldName="EffectiveDate"
          errorMessage="Please enter the effective date"
        />
        <NoteSection
          errorObj={errorObj}
          ChangeNotesComments={ChangeNotesComments}
          notesComments={notesComments}
          fieldDisabled={fieldDisabled}
          ref={ref}
          label="Notes/Comments (Optional)"
          inputMessage="Please add any additional details we may need to know in order to process this request."

        />
      </div>
    </div>
  )
}
LessThanHalfTimeSection.defaultProps = {
  notesComments: '',
  lastDateofAttendance: '',
  pendingEffectiveDateOfChange: ''
}
LessThanHalfTimeSection.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  errorObj: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  ChangelastDateofAttendance: PropTypes.func.isRequired,
  ChangeNotesComments: PropTypes.func.isRequired,
  fieldDisabled: PropTypes.bool.isRequired,
  notesComments: PropTypes.string,
  lastDateofAttendance: PropTypes.instanceOf(Date),
  CheckMinMaxDate: PropTypes.func.isRequired,
  minDate: PropTypes.instanceOf(Date).isRequired,
  maxDate: PropTypes.instanceOf(Date).isRequired,
  newStatus: PropTypes.string.isRequired,
  SetEffectiveDateOfChange: PropTypes.func.isRequired,
  pendingEffectiveDateOfChange: PropTypes.instanceOf(Date),
  inSchoolStatus: PropTypes.arrayOf(PropTypes.string).isRequired,
  oldStatus: PropTypes.string.isRequired
}
const forwardInput = React.forwardRef(LessThanHalfTimeSection)
export default forwardInput
