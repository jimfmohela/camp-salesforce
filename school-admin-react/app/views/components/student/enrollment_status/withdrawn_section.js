import React from 'react'
import PropTypes from 'prop-types'
import './extra_section.scss'
import StudentInformation from './component/student_info'
import AgreementShare from './component/agreements'
import DateSection from './component/date'
import NoteSection from './component/notes'
import MoreInformationSection from './component/more_info'

const WithdrawnStatusSection = (props, ref) => {
  const {
    studentData,
    ChangelastDateofAttendance,
    lastDateofAttendance,
    ChangeNotesComments,
    notesComments,
    fieldDisabled,
    errorObj,
    CheckMinMaxDate,
    minDate,
    maxDate,
    newStatus,
    inSchoolStatus
  } = props
  return (
    <div className="withdrawnContainer">
      <div className="withdrawnContainer--inner">
        <MoreInformationSection newStatus={newStatus} />
        <StudentInformation studentData={studentData} newStatus={newStatus} inSchoolStatus={inSchoolStatus}/>
        <AgreementShare studentData={studentData} />
        <DateSection
          errorObj={errorObj}
          setDate={ChangelastDateofAttendance}
          minDate={minDate}
          maxDate={maxDate}
          dateValue={lastDateofAttendance}
          fieldDisabled={fieldDisabled}
          CheckMinMaxDate={CheckMinMaxDate}
          ref={ref}
          label="Last Date of Attendance"
          checkError={errorObj.date}
          dateRefValue="dateRef"
          inputMessage="This is the last date the student attended school."
          fieldName="LDA"
          minDateError={errorObj.minDateError.error}
          maxDateError={errorObj.maxDateError.error}
          errorMessage="Please enter a date"
        />
        <NoteSection
          errorObj={errorObj}
          ChangeNotesComments={ChangeNotesComments}
          notesComments={notesComments}
          fieldDisabled={fieldDisabled}
          ref={ref}
          label="Please indicate the new funding amount for each ISA, if applicable. (Optional)"
          newStatus={newStatus}
          inputMessage=""

        />
      </div>
    </div>
  )
}
WithdrawnStatusSection.defaultProps = {
  notesComments: '',
  lastDateofAttendance: ''
}
WithdrawnStatusSection.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  errorObj: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  ChangelastDateofAttendance: PropTypes.func.isRequired,
  ChangeNotesComments: PropTypes.func.isRequired,
  fieldDisabled: PropTypes.bool.isRequired,
  notesComments: PropTypes.string,
  lastDateofAttendance: PropTypes.instanceOf(Date),
  CheckMinMaxDate: PropTypes.func.isRequired,
  minDate: PropTypes.instanceOf(Date).isRequired,
  maxDate: PropTypes.instanceOf(Date).isRequired,
  newStatus: PropTypes.string.isRequired,
  inSchoolStatus: PropTypes.arrayOf(PropTypes.string).isRequired,
  
}
const forwardInput = React.forwardRef(WithdrawnStatusSection)
export default forwardInput
