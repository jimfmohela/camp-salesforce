import React from 'react'
import PropTypes from 'prop-types'
import './extra_section.scss'
import StudentInformation from './component/student_info'
import NoteSection from './component/notes'
import DateSection from './component/date'

const leaveOfAbsence = (props, ref) => {
  const {
    studentData,
    ChangeNotesComments,
    notesComments,
    minDate,
    maxDate,
    fieldDisabled,
    LeaveOfAbsenceEndDateValue,
    LeaveOfAbsenceStartDateValue,
    SetLeaveOfAbsenceStartDate,
    SetLeaveOfAbsenceEndDate,
    CheckMinMaxDate,
    errorObj,
    inSchoolStatus,
    newStatus
  } = props

  return (
    <div className="withdrawnContainer">
      <div className="withdrawnContainer--inner">
        <div className="more_information">
          <div style={{ textAlign: 'center', marginBottom: '1em' }}>
            <span>We need more information</span>
          </div>
          <p>
             Please review the student’s information
            below and provide us with additional information that our
            servicing representatives will need in order to confirm
            this adjustment to the student’s academic status.
            {' '}
          </p>
        </div>
        <StudentInformation
          studentData={studentData}
          newStatus={newStatus}
          inSchoolStatus={inSchoolStatus}
        />
        <DateSection
          setDate={SetLeaveOfAbsenceStartDate}
          CheckMinMaxDate={CheckMinMaxDate}
          dateValue={LeaveOfAbsenceStartDateValue}
          ref={ref}
          fieldName="LeaveOfAbsenceStartDate"
          label="Leave of Absence Start Date"
          errorObj={errorObj}
          minDate={minDate}
          maxDate={maxDate}
          fieldDisabled={fieldDisabled}
          checkError={errorObj.absenceStartDate || errorObj.isStartDateSmaller}
          dateRefValue="LOAStartDateRef"
          minDateError={errorObj.absenceStartMinDate.error}
          maxDateError={errorObj.absenceStartMaxDate.error}
          errorMessage={(errorObj.absenceStartDate && 'Please enter a date') || (errorObj.isStartDateSmaller && 'Start date should be before end date')}
        />
        <DateSection
          errorObj={errorObj}
          setDate={SetLeaveOfAbsenceEndDate}
          minDate={minDate}
          maxDate={maxDate}
          dateValue={LeaveOfAbsenceEndDateValue}
          fieldDisabled={fieldDisabled}
          CheckMinMaxDate={CheckMinMaxDate}
          ref={ref}
          inputMessage="A leave of absence cannot be longer than 6 months. Please contact your CampusServ Account Manager if you have any questions."
          label="Leave of Absence End Date"
          checkError={errorObj.absenceEndDate || errorObj.sixMonthValidation}
          dateRefValue="LOAEndDateRef"
          minDateError={errorObj.absenceEndMinDate.error}
          maxDateError={errorObj.absenceEndMaxDate.error}
          fieldName="LeaveOfAbsenceEndDate"
          errorMessage={(errorObj.absenceEndDate && 'Please enter a date')
                      || (errorObj.sixMonthValidation && 'A leave of absence cannot be longer than 6 months. Please contact your CampusServ Account Manager if you have any questions.')}
        />
        <NoteSection
          errorObj={errorObj}
          ChangeNotesComments={ChangeNotesComments}
          notesComments={notesComments}
          fieldDisabled={fieldDisabled}
          ref={ref}
          label="Notes/Comments (Optional)"
        />
      </div>
    </div>
  )
}
leaveOfAbsence.defaultProps = {
  notesComments: '',
  LeaveOfAbsenceEndDateValue: '',
  LeaveOfAbsenceStartDateValue: ''
}
leaveOfAbsence.propTypes = {
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired,
  errorObj: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  ChangeNotesComments: PropTypes.func.isRequired,
  fieldDisabled: PropTypes.bool.isRequired,
  notesComments: PropTypes.string,
  LeaveOfAbsenceEndDateValue: PropTypes.instanceOf(Date),
  CheckMinMaxDate: PropTypes.func.isRequired,
  minDate: PropTypes.instanceOf(Date).isRequired,
  maxDate: PropTypes.instanceOf(Date).isRequired,
  newStatus: PropTypes.string.isRequired,
  LeaveOfAbsenceStartDateValue: PropTypes.instanceOf(Date),
  SetLeaveOfAbsenceStartDate: PropTypes.func.isRequired,
  SetLeaveOfAbsenceEndDate: PropTypes.func.isRequired,
  inSchoolStatus: PropTypes.arrayOf(PropTypes.string).isRequired
}
const forwardInput = React.forwardRef(leaveOfAbsence)
export default forwardInput
