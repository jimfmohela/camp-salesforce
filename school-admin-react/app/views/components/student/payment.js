import React from 'react'
import CallVemoApi from "~/app/store/salesforce";
import PaymentByISAItem from "./PaymentByISAItem";
import PaymentInfo from './PaymentInfo';

import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'
import Loader from '~/app/views/components/loader'
import PaymentZeroState from '~/app/views/components/zeroState/payment'
import PaymentHistory from './PaymentHistory'

class StudentPayment extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    let hideISA = ["Cancelled", "Invited", "Draft", "Application Complete", "Application Incomplete",
      "Application Under Review", "Certified", "Fully Funded", "Partially Funded"];
    let {
      studentAgreements,
      studentFees,
      incomeVerification,
      studentv1
    } = this.props,
      showZeroState = true;

    if (studentAgreements.data) {
      for (let i = 0; i < studentAgreements.data.length; i++) {
        if (studentAgreements.data[i]["servicing"] == "true") {
          showZeroState = false;
        }
      }
    }

    return (
      <ErrorBoundary>
        <Loader
          data={showZeroState ? null : studentAgreements.data}
          zeroState={<PaymentZeroState studentv1={studentv1.data}/>}
          isFetching={studentAgreements.isFetching} >
          <div className="o__student-payment">
            <PaymentInfo
              incomeverification={incomeVerification.data}
              fees={studentFees.data}
              agreements={studentAgreements.data}
              studentv1={studentv1.data} />
            <br />
            {(studentAgreements.data) ? <div> <h3 className="main">Payment Breakdown By ISA</h3>
              {
                studentAgreements.data.map(e => {
                  if (hideISA.indexOf(e.agreementStatus) == -1)
                    return <PaymentByISAItem agreement={e}> </PaymentByISAItem>
                })
              }</div> : ""}
              <br/>
             <PaymentHistory paymentHistoryData={this.props.paymentHistoryData}></PaymentHistory>
          </div>
        </Loader>
      </ErrorBoundary>)
  }
}

export default StudentPayment
