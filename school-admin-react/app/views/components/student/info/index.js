import React from 'react'
import Dates from '~/app/assets/js/dates'
import InfoCell from '../../layout/InfoCell'
import InfoCard from '../../layout/InfoCard'
import Common from '~/app/assets/js/common'
import EnrollmentStatus from '~/app/views/components/student/enrollment_status/index'
import { maskFS } from '../../../../helpers/fullstory'

class StudentInfo extends React.Component {
  constructor(props) {
    super(props)
  }

  updateInfoCellValue = (value) => {
    const student = this.props.studentv1.data[0]
    const data = [
      {
        ...student,
        alternateEmail: value
      }
    ]
    this.props.updateStudentV1(data)
  }

  render() {
    const { studentData, updateStudentV1, studentv1, admin, enrollmentFormStatus } = this.props
    const data = studentData
    const studentV1Data = studentv1.data[0]
    const comma = studentV1Data.city && studentV1Data.stateCode ? ',' : ''
    const streetAddress = studentV1Data.street || ''
    const city = studentV1Data.city || ''
    const stateCode = studentV1Data.stateCode || ''
    const postalCode = studentV1Data.postalCode || ''
    const cityAddress = `${city + comma} ${stateCode} ${postalCode}`
    const address = (
      <>
        {streetAddress}
        <br />
        {cityAddress}
      </>
    )

    return (
      <div className="o__student-info">
        <InfoCard title="Student Details">
          {maskFS(<InfoCell
            label="Name"
            data={`${data.firstName || ''} ${data.lastName || ''}`}
          />)}

          <InfoCell label="Vemo Account Number" data={data.vemoAccountNumber} />

          {data.birthdate ? (
            maskFS(<InfoCell
              label="Date of Birth"
              data={Dates.formatToMMDDYYYY(data.birthdate)}
            />)
          ) : (
            maskFS(<InfoCell label="Date of Birth" data={data.birthdate} />)
          )}

          {maskFS(<InfoCell
            label="Student ID"
            data={studentV1Data.primarySchoolStudentID}
          />)}
        </InfoCard>

        <InfoCard title="Contact Information">
          {maskFS(<InfoCell label="Email Address" data={data.email} />)}

          {maskFS(<InfoCell
            label="Phone Number"
            data={Common.phoneNumberFormat(data.mobilePhone)}
          />)}

          {maskFS(<InfoCell label="Mailing Address" data={<h3>{address}</h3>} />)}

          {maskFS(<InfoCell
            label="Alternate Email"
            editable
            data={studentV1Data.alternateEmail}
            updateInfoCellValue={this.updateInfoCellValue}
          />)}
        </InfoCard>

        <InfoCard title="Enrollment">
          <EnrollmentStatus
            data={data.recordLocked ? data.pendingStatus : data.status}
            label="Enrollment"
            academicEnrollmentStatuses={this.props.academicEnrollmentStatuses}
            UpdateAcademicEnrollmentStatus={
              this.props.UpdateAcademicEnrollmentStatus
            }
            UpdatedAcademicEnrollmentStatus={
              this.props.UpdatedAcademicEnrollmentStatus
            }
            studentData={data}
            recordLocked={data.recordLocked}
            admin={admin}
            enrollmentFormStatus={enrollmentFormStatus}
          />

          <InfoCell label="Grade" data={data.grade} />

          <InfoCell label="Major" data={data.description} />

          {(data.recordLocked === false && !(data.status === 'Graduated' || data.status === 'Withdrawn')) && (
            <>
              <InfoCell
                label="Expected Graduation Date"
                data={Dates.formatToMMDDYYYY(data.graduationDate)}
              />
              <InfoCell
                label="Effective Date of Change"
                data={Dates.formatToMMDDYYYY(data.effectiveDateofChange)}
              />

            </>
          )}
          {(data.recordLocked && !(data.pendingStatus === 'Graduated' || data.pendingStatus === 'Leave of Absence' || data.pendingStatus === 'Withdrawn')) && (
            <>
              <InfoCell
                label="Expected Graduation Date"
                data={data.pendingGraduationDate && `${Dates.formatToMMDDYYYY(data.pendingGraduationDate)} (Pending confirmation)`}
              />
              <InfoCell
                label="Effective Date of Change"
                data={data.pendingEffectiveDateOfChange && `${Dates.formatToMMDDYYYY(data.pendingEffectiveDateOfChange)} (Pending confirmation)`}
              />
            </>
          )}
          {(data.recordLocked && (data.pendingStatus === 'Graduated')) && (
            <InfoCell
              label="Graduation Date"
              data={`${Dates.formatToMMDDYYYY(data.pendingGraduationDate)} (Pending confirmation)`}
            />
          )}
          {(data.recordLocked === false && (data.status === 'Graduated')) && (
            <>
              <InfoCell
                label="Graduation Date"
                data={Dates.formatToMMDDYYYY(data.graduationDate)}
              />

            </>
          )}
          {(data.recordLocked && data.pendingStatus === 'Withdrawn') && (
            <>
              <InfoCell
                label="Graduation Date"
                data={Dates.formatToMMDDYYYY(data.graduationDate)}
              />
              <InfoCell
                label="Last Date of Attendance"
                data={`${Dates.formatToMMDDYYYY(data.pendingLastDateOfAttendance)} (Pending confirmation)`}
              />
            </>
          )}
          {(data.recordLocked === false && data.status === 'Withdrawn') && (
            <>
              <InfoCell
                label="Graduation Date"
                data={Dates.formatToMMDDYYYY(data.graduationDate)}
              />
              <InfoCell
                label="Last Date of Attendance"
                data={Dates.formatToMMDDYYYY(data.lastDateofAttendance)}
              />
            </>
          )}
          {(data.recordLocked && (data.pendingStatus === 'Leave of Absence')) && (
            <>
              <InfoCell
                label="Expected Graduation Date"
                data={data.graduationDate && `${Dates.formatToMMDDYYYY(data.graduationDate)} `}
              />
              <InfoCell
                label="Effective Date of Change"
                data={data.effectiveDateofChange && `${Dates.formatToMMDDYYYY(data.effectiveDateofChange)} `}
              />
            </>
          )}
          
        </InfoCard>
      </div>
    )
  }
}

export default StudentInfo
