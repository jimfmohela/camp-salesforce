import React from "react";
import Collapsible from "react-collapsible";
import WarningIcon from "~/app/assets/fonts/svgs/warning.svg";
import Icon from "~/app/assets/fonts/icon";
import Common from "~/app/assets/js/common";
import Dates from "~/app/assets/js/dates";
import { NavLink } from "react-router-dom";

class PaymentByISAItem extends React.Component {
  constructor(props) {
    super(props);
    this.setState({
      CollapsibleIcon: "down"
    });
  }

  progressbarValue = (at, warningorinfo) => {
    let { agreement } = this.props;
    let wPercentage = 0;

    let styleObj = {
      width: "0%",
      display: "none"
    };

    if (at == "Payments Received") {
      if (
        warningorinfo == "info" &&
        agreement.paymentTermPostCertification &&
        agreement.totalFullyAllocatedAmountDue
      ) {
        wPercentage =
          (agreement.totalFullyAllocatedAmountDue /
            agreement.paymentTermPostCertification) *
          100;
        if (wPercentage >= 100) {
          styleObj.borderBottomRightRadius = "10px";
          styleObj.borderTopRightRadius = "10px";
        }
        styleObj.width = wPercentage + "%";
        styleObj.display = "block";
      } else if (
        warningorinfo == "warning" &&
        agreement.daysDelinquent &&
        agreement.paymentTermPostCertification
      ) {
        let wPercentage_i =
          (agreement.totalFullyAllocatedAmountDue /
            agreement.paymentTermPostCertification) *
          100;
        if (wPercentage_i == 0) {
          styleObj.borderBottomLeftRadius = "10px";
          styleObj.borderTopLeftRadius = "10px";
        }
        // wPercentage = ((agreement.daysDelinquent / agreement.paymentTermPostCertification) * 100);
        wPercentage =
          ((agreement.paymentTermAssessed -
            agreement.totalFullyAllocatedAmountDue) /
            agreement.paymentTermPostCertification) *
          100;
        if (wPercentage_i + wPercentage >= 100) {
          styleObj.borderBottomRightRadius = "10px";
          styleObj.borderTopRightRadius = "10px";
        }
        styleObj.width = wPercentage + "%";
        styleObj.display = "block";
      }
    }

    if (at == "Payment Cap") {
      if (
        warningorinfo == "info" &&
        agreement.paidToDate &&
        agreement.paymentCapPostCertification
      ) {
        wPercentage =
          (agreement.paidToDate / agreement.paymentCapPostCertification) * 100;
        if (wPercentage >= 100) {
          styleObj.borderBottomRightRadius = "10px";
          styleObj.borderTopRightRadius = "10px";
        }
        styleObj.width = wPercentage + "%";
        styleObj.display = "block";
      } else if (
        warningorinfo == "warning" &&
        agreement.daysDelinquent &&
        agreement.nextPaymentDue &&
        agreement.paymentCapPostCertification
      ) {
        wPercentage =
          (agreement.nextPaymentDue / agreement.paymentCapPostCertification) *
          100;
        if (!agreement.paidToDate) {
          styleObj.borderRadiusTopLeft = "10px";
          styleObj.borderRadiusBottomLeft = "10px";
        }
        if (wPercentage >= 100) {
          styleObj.borderBottomRightRadius = "10px";
          styleObj.borderTopRightRadius = "10px";
        }
        styleObj.width = wPercentage + "%";
        styleObj.display = "block";
      }
    }
    if (
      at == "Payment Terms" &&
      agreement.paymentTermAssessed &&
      agreement.paymentTermPostCertification
    ) {
      styleObj.display = "block";
      wPercentage =
        (agreement.paymentTermAssessed /
          agreement.paymentTermPostCertification) *
        100;
      if (wPercentage >= 100) {
        styleObj.borderBottomRightRadius = "10px";
        styleObj.borderTopRightRadius = "10px";
      }
      wPercentage = wPercentage + "%";
      styleObj.width = wPercentage;
    }

    if (
      at == "Grace Period" &&
      agreement.graceMonthsUsed &&
      agreement.graceMonthsAllowed
    ) {
      styleObj.display = "block";
      wPercentage =
        (agreement.graceMonthsUsed / agreement.graceMonthsAllowed) * 100;
      if (wPercentage >= 100) {
        styleObj.borderBottomRightRadius = "10px";
        styleObj.borderTopRightRadius = "10px";
      }
      wPercentage = wPercentage + "%";
      styleObj.width = wPercentage;
    }
    if (
      at == "Deferment Months" &&
      agreement.defermentMonthsUsed &&
      agreement.defermentMonthsAllowed
    ) {
      styleObj.display = "block";
      wPercentage =
        (agreement.defermentMonthsUsed / agreement.defermentMonthsAllowed) *
        100;
      if (wPercentage >= 100) {
        styleObj.borderBottomRightRadius = "10px";
        styleObj.borderTopRightRadius = "10px";
      }
      wPercentage = wPercentage + "%";
      styleObj.width = wPercentage;
    }
    return styleObj;
  };

  togglecollapsibleIcon = value => {
    this.setState({
      CollapsibleIcon: value
    });
  };

  dueDateCheck = nextPaymentDate => {
    if (nextPaymentDate) {
      let dateDiff =
        new Date(Dates.formatToMMDDYYYY(nextPaymentDate)) - new Date(),
        noOfDays = 0;
      if (dateDiff < 0) {
        noOfDays = dateDiff / (24 * 3600 * 1000);
        noOfDays = Math.abs(parseInt(noOfDays));
      }
      return noOfDays;
    }
    return 0;
  };
  // return (
  //     <article className='o__student_paymentbyISA'>

  render() {
    let { agreement } = this.props;

    return (
      <article className="o__student_paymentbyISA">
        <Collapsible
          onOpen={() => this.togglecollapsibleIcon("up")}
          onClose={() => this.togglecollapsibleIcon("down")}
          key={agreement.agreementID}
          trigger={
            <div
              className={
                this.state && this.state.CollapsibleIcon
                  ? `paymentbyisa_items paymentbyisa_items--${
                  this.state.CollapsibleIcon
                  }`
                  : "paymentbyisa_items"
              }
            >
              <div className="paymentbyisa_itemsBlock">
                <p className="vemocontractNumber">
                  {" "}
                  <Icon
                    icon="applications-line"
                    style={{ fill: "#0F77C5", marginBottom: "6px", marginRight: "6px", verticalAlign: "middle" }}
                  />{" "}
                  <span>{agreement.vemoContractNumber}</span>
                </p>
                <br />
                <h3 className="program-name">{agreement.programName}</h3>
                <br />
                <p className="title">
                  Agreement Status:{" "}
                  {agreement.agreementStatus
                    ? agreement.agreementStatus
                    : "N/A"}
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Payments Satisfied</p>
                <br />
                <div className="progress-bar">
                  <div
                    className="filler"
                    style={this.progressbarValue("Payments Received", "info")}
                  />
                  <div
                    className="warningfiller"
                    style={this.progressbarValue(
                      "Payments Received",
                      "warning"
                    )}
                  />
                </div>
                <br />
                <p className="sub">
                  {agreement.totalFullyAllocatedAmountDue
                    ? agreement.totalFullyAllocatedAmountDue
                    : "0"}
                </p>{" "}
                <sup>
                  <p className="received">satisfied</p>
                  <p className="title">
                    {" "}
                    /{" "}
                    {agreement.paymentTermPostCertification
                      ? agreement.paymentTermPostCertification
                      : "N/A"}{" "}
                    total
                  </p>
                </sup>
                <br />
                <h5
                  className="warning"
                  style={{
                    display: agreement.daysDelinquent ? "block" : "none"
                  }}
                >
                  <WarningIcon style={{ fill: "#F25A55", marginRight: "6px", verticalAlign: "middle" }} />
                  <p class="tertiary" style={{ marginBottom: "1px", }}>
                    {agreement.paymentTermAssessed -
                      agreement.totalFullyAllocatedAmountDue}{" "}
                    payment(s) past due
                  </p>
                </h5>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Payment Cap</p>
                <br />
                <div className="progress-bar">
                  <div
                    className="filler"
                    style={this.progressbarValue("Payment Cap", "info")}
                  />
                  <div
                    className="warningfiller"
                    style={this.progressbarValue("Payment Cap", "warning")}
                  />
                </div>
                <br />
                <p className="sub">
                  {agreement.paidToDate
                    ? Common.AmountFormat(agreement.paidToDate)
                    : "$0.00"}
                </p>
                <sup>
                  <p className="received"> paid</p> /{" "}
                  <p className="title">
                    {agreement.paymentCapPostCertification
                      ? Common.AmountFormat(
                        agreement.paymentCapPostCertification
                      )
                      : "N/A"}{" "}
                    total
                  </p>
                </sup>
                <br />
                <h5
                  className="warning"
                  style={{
                    display:
                      agreement.nextPaymentDue && agreement.daysDelinquent != 0
                        ? "block"
                        : "none"
                  }}
                >
                  <WarningIcon style={{ fill: "#F25A55", marginRight: "6px", verticalAlign: "middle" }} />{" "}
                  <p class="tertiary" style={{ marginBottom: "1px" }}>
                    {agreement.nextPaymentDue
                      ? Common.AmountFormat(agreement.nextPaymentDue)
                      : "$0.00"}{" "}
                    past due
                  </p>
                </h5>
              </div>
              <div className="arrowIcon desktopUp">
                {this.state && this.state.CollapsibleIcon == "up" ? (
                  <Icon icon="arrow-up" />
                ) : (
                    <Icon icon="arrow-down" />
                  )}
              </div>
              <div className="arrowIcon mobileUp">
                {this.state && this.state.CollapsibleIcon == "up" ? (
                  <p>
                    Hide <Icon icon="arrow-up" />
                  </p>
                ) : (
                    <p>
                      Show More Details <Icon icon="arrow-down" />
                    </p>
                  )}
              </div>
            </div>
          }
        >
          <div className="paymentbyagreement_itemblocks">
            <div className="paymentbyisa_itemblock section1">
              {/* <div className="paymentbyisa_itemsBlock">
                                <h4 className="title">FINAL AGREEMENT TERMS</h4><br /><br />
                                <NavLink
                                    activeClassName='is-active'
                                    to={`/agreements/agreement/${agreement.agreementID}/info`}>
                                    <p className='link'>View Full Agreement Info</p>
                                </NavLink>
                            </div> */}
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Funding Amount (Final)</p>
                <br />
                <p className="value">
                  {agreement.fundingAmountPostCertification
                    ? Common.AmountFormat(
                      agreement.fundingAmountPostCertification
                    )
                    : "N/A"}
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Income Share (Final)</p>
                <br />
                <p className="value">
                  {agreement.incomeSharePostCertification
                    ? agreement.incomeSharePostCertification
                    + "%"
                    : "N/A"}
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Minimum Income Per Month</p>
                <br />
                <p className="value">
                  {agreement.minimumIncomePerMonth
                    ? Common.AmountFormat(agreement.minimumIncomePerMonth)
                    : "N/A"}
                </p>
              </div>
            </div>
            <div className="paymentbyisa_itemblock section2">
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Amount Due to Date</p>
                <br />
                <p className="value">
                  {agreement.amountDueToDate != null
                    ? Common.AmountFormat(agreement.amountDueToDate)
                    : "$0.00"}
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Amount Past Due</p>
                <br />
                <p
                  className="value"
                  style={{
                    color:
                      agreement.nextPaymentDue && agreement.daysDelinquent != 0
                        ? "#d92d27"
                        : "#001c33"
                  }}
                >
                  {agreement.nextPaymentDue != null &&
                    agreement.daysDelinquent != 0
                    ? Common.AmountFormat(agreement.nextPaymentDue)
                    : "$0.00"}
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Due Date</p>
                <br />
                <p
                  className="value"
                  style={{
                    color: agreement.daysDelinquent ? "#d92d27" : "#001c33"
                  }}
                >
                  {agreement.nextPaymentDueDate
                    ? Dates.formatToMMDDYYYY(agreement.nextPaymentDueDate)
                    : "N/A"}
                </p>
                <p
                  className="title warning tertiary"
                  style={{
                    display: agreement.daysDelinquent ? "inline" : "none"
                  }}
                >
                  <WarningIcon style={{ fill: "#d92d27", marginLeft: "6px", marginRight: "6px", verticalAlign: "middle" }} />
                  {agreement.daysDelinquent
                    ? agreement.daysDelinquent
                    : " "}{" "}
                  day(s) past due
                </p>
              </div>
            </div>
            <div className="paymentbyisa_itemblock section3">
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Payment Terms</p>
                <br />
                <div className="progress-bar">
                  <br />
                  <div
                    className="filler"
                    style={this.progressbarValue("Payment Terms", "info")}
                  />
                </div>
                <br />
                <p className="progress-bar-used">
                  {agreement.paymentTermAssessed != null
                    ? agreement.paymentTermAssessed
                    : "0"}{" "}
                  used
                </p>{" "}
                /{" "}
                <p className="progress-bar-total">
                  {agreement.paymentTermPostCertification
                    ? agreement.paymentTermPostCertification
                    : "N/A"}{" "}
                  total
                </p>{" "}
                <p className="progress-bar-remaining">
                  {" "}
                  {agreement.paymentTermRemaining} remaining
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Grace Period</p>
                <br />
                <div className="progress-bar">
                  <div
                    className="filler"
                    style={this.progressbarValue("Grace Period", "info")}
                  />
                </div>
                <br />
                <p className="progress-bar-used">
                  {agreement.graceMonthsUsed != null
                    ? agreement.graceMonthsUsed
                    : "0"}{" "}
                  used
                </p>{" "}
                /{" "}
                <p className="progress-bar-total">
                  {agreement.graceMonthsAllowed
                    ? agreement.graceMonthsAllowed
                    : "N/A"}{" "}
                  total
                </p>{" "}
                <p className="progress-bar-remaining">
                  {" "}
                  {agreement.graceMonthsRemaining} remaining
                </p>
              </div>
              <div className="paymentbyisa_itemsBlock">
                <p className="title">Deferment Months</p>
                <br />
                <div className="progress-bar">
                  <div
                    className="filler"
                    style={this.progressbarValue("Deferment Months", "info")}
                  />
                </div>
                <br />
                <p className="progress-bar-used">
                  {agreement.defermentMonthsUsed != null
                    ? agreement.defermentMonthsUsed
                    : "0"}{" "}
                  used
                </p>{" "}
                /{" "}
                <p className="progress-bar-total">
                  {agreement.defermentMonthsAllowed
                    ? agreement.defermentMonthsAllowed
                    : "N/A"}{" "}
                  total
                </p>{" "}
                <p className="progress-bar-remaining">
                  {" "}
                  {agreement.defermentMonthsRemaining} remaining
                </p>
              </div>
            </div>
          </div>
        </Collapsible>
      </article>
    );
  }
}

export default PaymentByISAItem;
