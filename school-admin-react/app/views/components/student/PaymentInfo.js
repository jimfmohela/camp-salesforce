import React from "react";
import Common from "~/app/assets/js/common";
import Dates from "~/app/assets/js/dates";
import Chart from "react-apexcharts";
import WarningIcon from "~/app/assets/fonts/svgs/warning.svg";
import InfoIcon from "~/app/assets/fonts/svgs/info-bold.svg";

class PaymentInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() { }

  calculateLateFee = () => {
    let { fees, agreements } = this.props;

    // Calculate late fees percentage

    let donoughtChart = {
      options: {
        labels: ["N/A% - On-Time", "N/A% - Late"],
        markers: {
          strokeColor: ["#0092ff", "#222"]
        },
        dataLabels: {
          enabled: false
        },
        plotOptions: {
          pie: {
            donut: {
              size: "65%"
            }
          }
        },
        colors: ["#0092ff", "#222"],
        legend: {
          offsetX: 10
        },
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 150
              },
              legend: {
                show: false,
                position: "bottom"
              }
            }
          }
        ]
      },
      series: [0, 0]
    };

    if (!fees || !fees.length) return donoughtChart;
    let latefeepercentage = 0,
      ontimefeepercentage = 0,
      lateFeeCount = 0,
      totalPaymentTermAssessed = 0;

    for (var i = 0; i < agreements.length; i++) {
      if (agreements[i]["paymentTermAssessed"]) {
        totalPaymentTermAssessed += agreements[i]["paymentTermAssessed"];
      }
    }

    for (var i = 0; i < fees.length; i++) {
      if (
        (fees[i]["type"] == "Late Fee" || fees[i]["type"] == "NSF Fee") &&
        (fees[i]["status"] == "Paid" || fees[i]["status"] == "Unpaid")
      ) {
        lateFeeCount++;
      }
    }
    //console.log('lateFeeCount,totalPaymentTermAssessed', lateFeeCount, totalPaymentTermAssessed);

    latefeepercentage = (lateFeeCount / totalPaymentTermAssessed) * 100;
    ontimefeepercentage = 100 - latefeepercentage;

    latefeepercentage = latefeepercentage.toFixed(0);
    ontimefeepercentage = ontimefeepercentage.toFixed(0);

    donoughtChart.options["labels"] = [
      ontimefeepercentage + "% - On-Time",
      latefeepercentage + "% - Late"
    ];
    donoughtChart.series = [
      parseInt(ontimefeepercentage),
      parseInt(latefeepercentage)
    ];

    return donoughtChart;
  };

  render() {
    let { incomeverification, agreements, studentv1 } = this.props;
    let donoughtChart = this.calculateLateFee(),
      noOfISAPastDue = 0;
    if (agreements && agreements.length)
      agreements.map(e => {
        if (e.daysDelinquent > 0) noOfISAPastDue++;
      });
    return (
      <div>
        <h3 className="main">Payment Behavior</h3>
        <br />
        <article className="student-paymentbehavior-block">
          <div className="student-paymentbehavior-block--monthlyIncome firstSection">
            <p className="main">
              {incomeverification && incomeverification[0].type == "Reported"
                ? "Verified "
                : "Estimated "}
              Monthly Income{" "}
              <div className="tooltip"><span className="tooltiptext">This is based on historical information and may change before the next due date</span><InfoIcon style={{ verticalAlign: "middle", width: "16px" }} /></div>
            </p>
            <br />
            <h1>
              <sup>$</sup>
              {incomeverification && incomeverification[0].incomePerMonth
                ? Common.SeparateAmountCent(
                  incomeverification[0].incomePerMonth
                )[0]
                : "00"}
              <sup>
                .
                {incomeverification && incomeverification[0].incomePerMonth
                  ? Common.SeparateAmountCent(
                    incomeverification[0].incomePerMonth
                  )[1]
                  : "00"}
              </sup>
            </h1>
            <div>
              <p className="asofDate">
                As of{" "}
                {incomeverification && incomeverification[0].beginDate
                  ? Dates.formatToMMDDYY(incomeverification[0].beginDate)
                  : "N/A"}
              </p>
            </div>
          </div>
          <div className="secondSection">
            <div className="student-paymentbehavior-block--autopay">
              <p className="main">Autopay</p>
              <br />
              {studentv1 && studentv1[0].autoPayment == "true" ? (
                <h1 className="student-paymentbehavior-block--autopay-on">
                  On
                </h1>
              ) : (
                  <h1 className="student-paymentbehavior-block--autopay-off">
                    Off
                </h1>
                )}
            </div>
            <div className="student-paymentbehavior-block--monthlyIncome">
              {/* <p className="main">Payment Timing Breakdown</p>
              <br /> */}
              {/* <div id="chart" style={{ height: "105px", width: "105px" }}>
                {donoughtChart != null ? (
                  <Chart
                    options={donoughtChart.options}
                    series={donoughtChart.series}
                    type="donut"
                    width="280"
                  />
                ) : (
                  <></>
                )}
              </div> */}
              {noOfISAPastDue > 0 ? (
                <p className="title warning tertiary">
                  <WarningIcon style={{ fill: "#f25a55", marginRight: "6px", verticalAlign: "middle" }} /> {noOfISAPastDue}{" "}
                  ISA(s) past due
                </p>
              ) : (
                  <></>
                )}
            </div>

          </div>
        </article>
      </div>
    );
  }
}
export default PaymentInfo;
