import React from 'react'

import VemoTable      from '~/app/views/components/shared/table/table'
import VemoTableCell from '~/app/views/components/shared/table/cell'
import Icon  from '~/app/assets/fonts/icon'
import Common from '~/app/assets/js/common'
import Dates from '~/app/assets/js/dates'

const columns = [
  {
    header: 'ISA Program Name',
    headerClassName: 'name',
    accessor: 'programName',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]} >
          
          <Icon icon='applications-fill' />
          <h3 className='main'>{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Contract Num',
    headerClassName: 'program',
    accessor: 'vemoContractNumber',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]} >

          <p >{data[this.accessor]}</p>
          <p >&nbsp;- {data.agreementStatus}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Funding Amount',
    headerClassName: 'funding',
    accessor: 'requestedAmount',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]} >

          <p className='main'>Requested:&nbsp;</p>
          <p className='main'>{Common.AmountFormat(data[this.accessor])}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Expected Grad. Date',
    headerClassName: 'grad-date',
    accessor: 'expectedGraduationDate',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <p>{Dates.formatToMMDDYYYY(data[this.accessor])}</p>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Status',
    headerClassName: 'status',
    accessor: 'agreementStatus',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]} >

          <p>{data[this.accessor]}</p>
        </VemoTableCell>
      )
    }
  }
]

class StudentAgreementsComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let { data } = this.props
    let data1 = []; 
    if(Array.isArray(data)){
      data = data.map(item => item.agreements);
      data.map(item => data1.push(...item));
    }
    data = data1;

    return (
      <VemoTable 
        numberOfColumns={columns.length}
        columns={columns}
        headerClass='o__students-table__agreements o__students-table__agreements--header'
        rowClass='o__students-table__agreements o__students-table__agreements--row'
        data={data} 
        pathData={{
          path: '/agreements/agreement/',
          pathId: 'agreementID'
        }}/>
    )
  }
}

export default StudentAgreementsComponent
