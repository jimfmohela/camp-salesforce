import React from 'react'
import cn from 'classnames'

class VemoTableCell extends React.Component {
  constructor(props) {
    super(props)
  }

  checkData = (data) => {
    if (Array.isArray(data) && data.length <= 0) return false
    if (data === null || data === undefined) return false
    return true
  }

  render() {
    const { cellClass, cellData } = this.props
    const tableClasses = cn(this.props.className,
      'table-cell',
      {
        [`table-cell--${cellClass}`]: cellClass,
        [`table-row--${cellClass}`]: cellClass
      })

    return (
      <div className={tableClasses}>
        {this.checkData(cellData) ? (
          this.props.children
        ) : (
          <div className="empty-cell">
            {' '}
            {this.props.children}
            {' '}
          </div>
        )}
      </div>
    )
  }
}

export default VemoTableCell
