import React from 'react'
import { NavLink }  from 'react-router-dom'

import ClassNames from 'classnames'
import Icon  from '~/app/assets/fonts/icon'
import Delete from '~/app/views/pages/layout/deleteRecord'

class VemoRow extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filter: ''
    }
  }

  componentDidMount() {
    // temporary fix to handle Pathfilter that is only coming from Students currently, will be updated for Agreements and Programs
    let { pathData } = this.props
    if (pathData && pathData.pathFilter) {
      this.setState({filter: pathData.pathFilter})
    }
  }

  render() {
    const { data, pathData, rowClass, columns, status,objectName,allRecords,index } = this.props
    let classNames = ClassNames(
      's__vemo-table' ,
      's__vemo-table--row',
      {
        [`${rowClass}`]: rowClass,
        //'closed': /(closed)/i.test(data[status])
        'closed': /(planned)|(cancelled)|(closed)/i.test(data[status])

      }
    )

    return pathData ? (
      <>
        <NavLink
          key={data.name}
          className={classNames}
          to={{
            pathname: `${pathData.path}${data[pathData.pathId]}${this.state.filter}`,
            state: { data: data }
          }}
        >
          {columns.map(column => column.accessorCell(data))}
          <Icon icon="arrow-up" />
        </NavLink>

        {this.props.isDelete ? (
          <Delete
            data={{
              deleteData: data,
              objectName: objectName,
              allRecord: allRecords,
              index: index
            }}
          />
        ) : null}
      </>
    ) : (
      <div key={data.name} className={classNames}>
        {columns.map(column => column.accessorCell(data))}
      </div>
    );
  }
}

export default VemoRow
