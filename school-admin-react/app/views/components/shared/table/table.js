import React from 'react'
import { withRouter } from 'react-router-dom'
import Common from '~/app/assets/js/common'
import VemoTableHeader from './header'
import VemoRow from './row'
import ErrorBoundary  from '~/app/views/pages/errors/errorBoundary'

class VemoTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      sortActiveFor: null,
      sortDesc: true,
      tableData: [],
    }
  }

  componentDidMount() {
    this.setState({ tableData: this.props.data })
  }
  componentWillReceiveProps(props){
    this.setState({ tableData: props.data })
  }
  

  sortOnClick = async(column) => {
    // Check active status for new column clicks vs repeated clicks
    await this.state.sortActiveFor === column.headerClassName ?
      this.setState({sortDesc: !this.state.sortDesc}) :
      this.setState({
        sortDesc: true,
        sortActiveFor: column.headerClassName,
      })
  
    // You can never escape race conditions
    this.setState({tableData: this.sortData(this.state.tableData, column)})
  }

  sortData = (dataArray, column) => {
    return dataArray.sort((a,b) =>
      Common.sortDefault(a[column.accessor], b[column.accessor], this.state.sortDesc)
    )
  }

  render() {
    let { columns, rowClass, headerClass, pathData, status,isDelete,objectName } = this.props
    let { tableData, sortActiveFor, sortDesc } = this.state
   
    return (
      <ErrorBoundary>
        <div className={`s__vemo-table s__vemo-table--header ${headerClass}`}>
          {columns.map(column =>
            <VemoTableHeader
              key={column.headerClassName}
              column={column}
              sortActiveFor={sortActiveFor}
              sortDesc={sortDesc}
              sortOnClick={this.sortOnClick} />
          )}
        </div>

        {tableData &&
          tableData.map((data,index) =>
            <VemoRow
              key={Math.random()}
              data={data}
              columns={columns}
              pathData={pathData}
              rowClass={rowClass}
              status={status}
              isDelete={isDelete} 
              objectName={objectName}
              allRecords={tableData}
              index={index}/>
          ) 
        }
      </ErrorBoundary>
    )
  }
}

export default withRouter(VemoTable)
