import React from 'react'
import Icon  from '~/app/assets/fonts/icon'
import ClassNames from 'classnames'

class VemoTableHeader extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let { column, sortDesc, sortActiveFor, sortOnClick } = this.props
    let headerClasses= ClassNames(
      'table-cell',
      {
        [`table-cell--${column.headerClassName}`]: column.headerClassName,
        [`table-header--${column.headerClassName}`]: column.headerClassName,
        'active': sortActiveFor === column.headerClassName,
        '--desc': sortDesc,
        '--asce': !sortDesc
      }
    )

    return (
      <div
        className={headerClasses} 
        onClick={() => sortOnClick(column)}>
          
        <p>{column.header}</p>
        <Icon icon='sort-arrow' />
      </div>
    )
  }
}

export default VemoTableHeader
