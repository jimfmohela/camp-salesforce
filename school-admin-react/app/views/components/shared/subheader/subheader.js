import React from 'react'
import cn from 'classnames';
import SubHeaderButtons from './buttons'
import SubHeaderTabs from './tabs'

class Subheader extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showModal: false,
      percentage: 0
    }
  }

  componentDidMount() {
    const { fundingAmount } = this.props

    if (fundingAmount) {
      this.setState({
        percentage: (fundingAmount.disbursed / fundingAmount.certified) * 100
      })
    }
  }

  handleFilterButton = () => {
    this.setState({
      showModal: !this.state.showModal
    })
  }

  render() {
    const { header, fundingAmount, csvHeaders, csvData, schoolID, countData, page, programData, download, showFundingAmount, location_url } = this.props
    const { percentage } = this.state
    const className = cn(this.props.className, '')
    return (
      <>
        <div className="s__shared-submenu s__shared-submenu--tabs">
          <SubHeaderTabs
            header={header}
            className={className}
            children={this.props.children}
          />
        </div>

        <div className="s__shared-submenu s__shared-submenu--buttons">
          <SubHeaderButtons
            key={Math.random()}
            percentage={percentage}
            fundingAmount={fundingAmount}
            csvHeaders={csvHeaders}
            csvData={csvData}
            programData={programData}
            page={page}
            schoolID={schoolID}
            countData={countData}
            download={download}
            showFundingAmount={showFundingAmount}
            location_url={location_url}
          />
        </div>
      </>
    )
  }
}

export default Subheader
