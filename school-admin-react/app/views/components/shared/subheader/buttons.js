import React from 'react'
import { Fab, Hidden } from '@material-ui/core'
import Icon from '~/app/assets/fonts/icon'
import { CSVLink } from 'react-csv'
import Common from '~/app/assets/js/common'
import CallVemoApi from '~/app/store/salesforce'
import Dates from '~/app/assets/js/dates'

class SubHeaderButtons extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      csvData: [],
      csvFileName: 'all.csv'
    }
  }

  componentDidMount() { }

  downloadCSVData = () => {
    const { schoolID, page, programData, location_url } = this.props
    const urlPath = location_url.search
      ? location_url.search.split('=')[1]
      : location_url.pathname.split('/')[2]
    const pageTab = [
      'applications',
      'agreements',
      'application incomplete',
      'application complete',
      'application under review',
      'cancelled',
      'fully funded',
      'deferment',
      'grace',
      'partially funded',
      'certified',
      'invited',
      'payment',
      'draft',
      'school',
      'all',
      'planned',
      'open',
      'closed'
    ]

    const capitalizeFirstLetter = (str) => {
      str = str.split(' ')
      str = str.map((s) => s.charAt(0).toUpperCase() + s.slice(1)).join(' ')
      return str
    }

    // filter download and renaming the downloaded file
    const filterResult = (res, urlPath, Obj, status, filename) => {
      if (pageTab.includes(urlPath)) {
        res = res.filter((item) => Obj[urlPath].includes(item[status]))
        if (filename === 'Agreements') {
          const firstpart = location_url.pathname.split('/')[2]
          const secondpart = location_url.search
            ? ' - ' + location_url.search.split('=')[1]
            : null
          filename = `${firstpart}${secondpart || ''}`
          if (filename === 'all') {
            filename = `${filename} ISAs`
          }
        } else if (filename === 'ISA Programs') {
          filename = `${filename} - ${urlPath}`
        }
        const csvFileName = `${capitalizeFirstLetter(filename)}.csv`
        this.setState({ csvFileName })
      } else {
        res = null
      }
      return res
    }

    CallVemoApi('GET', `/vemo/v2/${page}`, null, {
      schoolID: schoolID,
      page: 1,
      pageSize: '50000'
    })
      .then((res) => {
        if (page == 'program') {
          let record = {}

          programData.forEach((program) => {
            record[program.programID] = program.agreementsTotal
          })

          res.forEach((resProgram) => {
            resProgram.agreements = record[resProgram.programID]
            resProgram.amountCertifiedToDate = Common.AmountFormat(
              resProgram.amountCertifiedToDate
            )
            resProgram.amountDisbursedToDate = Common.AmountFormat(
              resProgram.amountDisbursedToDate
            )
          })

          // Program tabs => planned, open, closed, all
          // Programs Status => Planned, Open, Closed

          const prgObj = {
            planned: ['Planned'],
            open: ['Open'],
            closed: ['Closed'],
            all: ['Open', 'Closed', 'Planned']
          }

          if (urlPath === 'all') {
            this.setState({csvFileName: 'ISA Programs - all.csv'});
          } else {
            res = filterResult(
              res,
              urlPath,
              prgObj,
              'programStatus',
              'ISA Programs'
            )
          }
        } else if (page == 'bulkAgreement') {
          res.forEach((resAgreement) => {
            resAgreement.fundingAmount = Common.AmountFormat(
              resAgreement.fundingAmount
            )
            resAgreement.expectedGraduationDate = Dates.formatToMMDDYYYY(
              resAgreement.expectedGraduationDate
            )
          })

          // Agreement tabs => applications, agreements, cancelled, all
          // Agreement Status => 'Draft', 'Invited', 'Application Incomplete', 'Application Under Review', 'Application Complete',
          // 'Partially Funded', 'Certified', 'Cancelled', 'Payment', 'School'

          const agObj = {
            applications: [
              'Draft',
              'Invited',
              'Application Incomplete',
              'Application Under Review',
              'Application Complete'
            ],
            agreements: ['Partially Funded', 'Certified'],
            cancelled: ['Cancelled'],
            'application incomplete': ['Application Incomplete'],
            'application complete': ['Application Complete'],
            'application under review': ['Application Under Review'],
            'partially funded': ['Partially Funded'],
            'fully funded': ['Fully Funded'],
            deferment: ['Deferment'],
            grace: ['Grace'],
            certified: ['Certified'],
            invited: ['Invited'],
            payment: ['Payment'],
            draft: ['Draft'],
            school: ['School'],
            all: [
              'Draft',
              'Invited',
              'Application Incomplete',
              'Application Under Review',
              'Application Complete',
              'Partially Funded',
              'Fully Funded',
              'Deferment',
              'Grace',
              'Certified',
              'Cancelled',
              'Payment',
              'School'
            ]
          } // all obj are depricated
              
          if (urlPath === 'all') {
            this.setState({csvFileName: 'All ISAs.csv'});
          } else {
            res = filterResult(
              res,
              urlPath,
              agObj,
              'agreementStatus',
              'Agreements'
            )
          }
        }
        if (res) {
          this.setState({ csvData: res })
          let btn = this.refs.CSVCom
          btn.link.click()
        } else {
          return null
        }
      })
      .catch((err) => console.log('Fetching agreements error:', err))
  }

  render() {
    const {
      fundingAmount,
      percentage,
      csvHeaders,
      download,
      showFundingAmount
    } = this.props

    return (
      <>
        {this.props.fundingAmount && showFundingAmount && (
          <div className="funding-amount">
            <h3 className="main">
              {Common.AmountFormat(fundingAmount.disbursed)} &nbsp;
            </h3>
            <p className="third">Disbursed &nbsp;</p>
            <p className="main grey">
              /{Common.AmountFormat(fundingAmount.certified)} Certified
            </p>

            <div className="progress-bar">
              <div className="filler" style={{ width: `${percentage}%` }} />
            </div>
          </div>
        )}

        <div className="buttons">
          {download && (
            <Hidden smDown>
              <Fab onClick={this.downloadCSVData}>
                <Icon icon="download" />
              </Fab>
            </Hidden>
          )}

          {csvHeaders && this.state.csvData && (
            <CSVLink
              ref="CSVCom"
              headers={csvHeaders}
              data={this.state.csvData}
              filename={this.state.csvFileName}
              style={{ display: 'none' }}
            />
          )}

          {this.props.filter && (
            <div className="filter-button" onClick={this.handleFilterButton}>
              <Icon icon="filter" />
              <h2>Filter</h2>
            </div>
          )}
        </div>
      </>
    )
  }
}

export default SubHeaderButtons
