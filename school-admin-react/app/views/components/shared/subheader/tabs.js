import React from 'react'

class SubHeaderTabs extends React.Component {
  render() {
    const { header, className } = this.props

    return (
      <>
        {header ? (
          <h1 className={className}>{header}</h1>
        ) : null}

        <aside className="submenu-tabs">
          {this.props.children}
        </aside>
      </>
    )
  }
}

export default SubHeaderTabs
