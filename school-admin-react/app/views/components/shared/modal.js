import React from 'react'
import ClassNames from'classnames'

class Modal extends React.Component {
  constructor(props) {
    super(props)
    this.inputRef = React.createRef()
    this._timeoutID = null
    // this.canBlur = true
    this.state = {
      modalActive: false
    }
  }

  // handleMouseDown = () => {
  //   this.canBlur = false
  // }

  // handleMouseUp = () => {
    // this.canBlur = true
  // }

  _onMouseUp = () => {
    this.setState({ modalActive: true })
  }

  _onMouseDown = () => {
    this.setState({ modalActive: true })
  }

  // handleBlur = event => {
  //   this.canBlur ? this.props.onBlur(event) : null
  // }

  // handleKeyDown = event => {
  //   event.key === 'Escape' ? this.props.onBlur(event) : null
  // }

  _onBlur = () => {
    // this._timeoutID = setTimeout(() => {
      // if (this.state.modalActive) {
        // this.setState({ modalActive: false })
      // }
    // }, 0)
  }

  _onFocus = () => {
    // clearTimeout(this._timeoutID)

    // if (!this.state.modalActive) {
    //   this.inputRef.current.focus()
    //   this.setState({ modalActive: true })
    // }
  }

  render() {
    let { activeClass, customClass } = this.props
    let modalClass = ClassNames(
      's__modal',
      customClass,
      { 
        's__modal--active': this.state.modalActive,
        [activeClass]: this.state.modalActive
      }
    )
    
    return (
      <>
        <div
          className={modalClass}
          onFocus={this._onFocus}
          onClick={this._onFocus}
          onBlur={this._onBlur}
          onMouseUp={this._onMouseUp}
          onMouseDown={this._onMouseDown}
          aria-haspopup='true'
          aria-expanded={this.state.modalActive} >

          {this.props.children({
            setRef: this.inputRef
          })}
        </div>

        {this.state.modalActive ?
          <span 
            className='modal-background'
            onClick={() => this.setState({ modalActive: false })} /> : null }
      </>
    )
  }
}

export default Modal
