import React from "react";

class Loader extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let { data, zeroState, isFetching, id, countTotals } = this.props;

    //For Agreement Page; countTotals prop is only being passed on agreement page.
    if (data === null && isFetching === false && (countTotals && countTotals[0]['recordCount'] == 0)) {
      return zeroState ? zeroState : <div>No Data To Present</div>;
    } else if (data === null && isFetching === false && !countTotals) {
      return zeroState ? zeroState : <div>No Data To Present</div>;
    } else if (
      data &&
      (data.length === 0 || (data.length && data[0][id] === "")) &&
      isFetching === false
    ) {
      return zeroState ? zeroState : <div>No Data To Present</div>;
    } else if (isFetching) {
      return (
        <main className="t__loader-page">
          <div className="loader-wrapper">
            <span />
            <span />
            <span />
            <span />
            <span />
          </div>
        </main>
      );
    } else {
      return this.props.children;
    }
  }
}

export default Loader;
