import React          from 'react'
import VemoTable      from '~/app/views/components/shared/table/table'
import VemoTableCell  from '~/app/views/components/shared/table/cell'
import Icon           from '~/app/assets/fonts/icon'
import Common from '~/app/assets/js/common'

const columns = [
  {
    header: 'Name',
    headerClassName: 'name',
    accessor: 'programName',
    accessorCell(data) {
      return (
        <VemoTableCell cellClass={this.headerClassName} cellData={data[this.accessor]}>
          <Icon icon='isa-fill' />

          <div className='m__isa-table-item'>
            <h3 className='main'>{data[this.accessor]}</h3>
            <h4 className='main'>{data.programNumber} <span>- {data.status} - {data.enrollment}</span></h4>
          </div>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Status',
    headerClassName: 'status',
    accessor: 'programStatus',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          <h3 className='main'>{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Enrollment Type',
    headerClassName: 'enrollment',
    accessor: 'enrollmentType',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          <h3 className='main'>{data[this.accessor]}</h3>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Certified',
    headerClassName: 'certified',
    accessor: 'amountCertifiedToDate',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          {data[this.accessor] ?
            <h4 className='main'>{Common.AmountFormat(data[this.accessor])} &nbsp;</h4> :
            '$0'
          }
          <h4 className='secondary'>Certified -&nbsp;</h4>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Disbursed',
    headerClassName: 'disbursed',
    accessor: 'amountDisbursedToDate',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

          {data[this.accessor] ? 
            <h4 className='main'>{Common.AmountFormat(data[this.accessor])} &nbsp;</h4> :
            '$0'
          }
          <h4 className='secondary'>Disbursed -&nbsp;</h4>
        </VemoTableCell>
      )
    }
  }, {
    header: 'Agreements',
    headerClassName: 'agreements',
    accessor: 'agreementsTotal',
    accessorCell(data) {
      return (
        <VemoTableCell 
          cellClass={this.headerClassName} 
          cellData={data[this.accessor]}>

            {data[this.accessor] ? <h4 className='main'>{data[this.accessor]} &nbsp;</h4> : '0'}
            <h4 className='secondary'>&nbsp;Funding </h4>
        </VemoTableCell>
      )
    }
  }
]

function ProgramsComponent(props){
  let { data } = props

  return (
    <VemoTable 
      numberOfColumns={columns.length}
      columns={columns}
      headerClass='o__programs-table o__programs-table--header'
      rowClass='o__programs-table o__programs-table--row'
      pathData={{
        path: '/programs/program/',
        pathId: 'programID'
      }}
      status='programStatus'
      data={data} />
  )
}

export default ProgramsComponent
