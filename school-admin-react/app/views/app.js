import React from 'react'
import PropTypes from 'prop-types'
import { Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { handleFirstTab } from '~/app/assets/js/accessibility'
import { checkAuth as checkAuthAction } from '~/app/store/actions/auth'

import Loader from '~/app/views/components/loader'
import Root from '~/app/views/pages/layout/root'
import PrivateRoute from '~/app/views/pages/layout/private_route'

class App extends React.Component {
  async componentWillMount() {
    const { checkAuth } = this.props
    await checkAuth()

    // Accessibility : check if user is tabbing or clicking for outlines
    window.addEventListener('keydown', handleFirstTab)
  }

  render() {
    const { auth } = this.props

    return (
      <Loader isFetching={auth.isAuthPending}>
        <PrivateRoute path="/" component={Root} />
      </Loader>
    )
  }
}

App.propTypes = {
  checkAuth: PropTypes.func.isRequired,
  auth: PropTypes.shape({
    isAuthPending: PropTypes.bool
  }).isRequired
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkAuth: () => dispatch(checkAuthAction())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  undefined,
  { pure: false }
)(App)
