import React from 'react'
import { NavLink } from 'react-router-dom'
import AgreementsComponent from '~/app/views/components/agreements/component'
import Loader from '~/app/views/components/loader'
import Subheader from '~/app/views/components/shared/subheader/subheader'
import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'
import ReactPaginate from 'react-paginate'
import { matchPath } from 'react-router'
import Icon from '~/app/assets/fonts/icon'
import AgreementsZeroState from '~/app/views/components/zeroState/agreements'
import RefreshModal from '~/app/views/components/layout/refreshModal.js'
import Pagination from '~/app/components/Pagination'

const csvHeaders = [
  { label: 'Student Name', key: 'studentName' },
  { label: 'Program Name', key: 'programName' },
  { label: 'Vemo Contract Number', key: 'vemoContractNumber' },
  { label: 'Funding Amount', key: 'fundingAmount' },
  { label: 'Expected Graduation Date', key: 'expectedGraduationDate' },
  { label: 'Status', key: 'agreementStatus' }
]

class AgreementsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      pageNumber: 0,
      filters: '',
      filtersArray: [], // Ugh
      tabs: {
        applications: [
          'Invited',
          'Draft',
          'Application Under Review',
          'Application Incomplete',
          'Application Complete'
        ],
        agreements: ['Certified', 'Partially Funded'],
        cancelled: ['Cancelled']
      },
      modalOpen: false,
      certOpen: false,
      csvData: [],
      page: 'bulkAgreement'
    }
  }

  _modalControl = () => {
    this.setState({ modalOpen: !this.state.modalOpen })
  }

  _paginationValidation = async (customMatch, status) => {
    let pageNumber = customMatch.params.pageNumber || 0

    if (pageNumber <= 0) {
      this.props.history.replace(`/agreements/${customMatch.params.category}/1`)

      return await this.props.updatePagination(1, status)
    }
  }

  getStatus(urlPath) {
    let lsstring = urlPath
    let splitUrl = ''
    let splitagreements = []
    if (lsstring != '') {
      splitUrl = lsstring.split('=')[1]
    }
    if (splitUrl != '') {
      splitagreements = splitUrl.split(',')
    }
    return {
      filteredData: splitagreements.map((url) => decodeURI(url)),
      filterData: splitUrl
    }
  }
  loadPagination = async () => {
    let { agreements, updatePagination, history, location } = this.props
    const category = this.props.match.params.category
    const status = this.state.tabs[category]
    let filters = this.getStatus(location.search)

    // Filters
    const query = this.getStatus(location.search)
    // SWITCH THIS TO USE PROPS.MATCH INSTEAD
    const customMatch = matchPath(this.props.location.pathname, {
      path:
        '/agreements/:category(applications|agreements|cancelled|all)/:pageNumber?',
      exact: false,
      strict: false
    })

    // Check if pagination is valid
    let pageNumber = customMatch.params.pageNumber || 0

    if (pageNumber <= 0) {
      this.props.history.replace(`/agreements/${customMatch.params.category}/1`)

      return await this.props.updatePagination(1, status)
    }

    // Update filters for re-mounts
    if (query && this.state.filters !== query.filterData) {
      await this.setState({
        filters: query.filterData,
        filtersArray: query.filteredData
      })
    }
    this.setState({
      filtersArray: query.filteredData
    })

    // Update page number for re-mounts
    if (!this.state.pageNumber) {
      await this.setState({ pageNumber: customMatch.params.pageNumber })
    }

    if (!this.props.agreements.isLoaded) {
      if (this.state.filtersArray.length !== 0) {
        await updatePagination(
          customMatch.params.pageNumber,
          this.state.filtersArray
        )
      } else {
        return await updatePagination(customMatch.params.pageNumber, status)
      }
    }

    //check cert app modalOpen requirements exist
    if (/\b(draft|complete|certified|partially)/i.test(this.state.filters)) {
      await this.setState({ certOpen: true })
    }
  }

  pageCount = () => {
    let { dashboard, match, countTotals, location } = this.props
    let category = match.params.category
    let filters = this.getStatus(location.search)
    filters = filters.filteredData
    let count = null

    // Temp
    const draft = /\b(draft)/i
    const invited = /\b(invited)/i
    const cancelled = /\b(cancelled)/i
    const applicationIncomplete = /\b(incomplete)/i
    const applicationUnderReview = /\b(review)/i
    const applicationComplete = /\b(complete)/i
    const certified = /\b(certified)/i
    const partiallyFunded = /\b(partially)/i
    if (filters.length > 0) {
      filters.map((filter) => {
        draft.test(filter) ? (count += dashboard['draft']) : null
        invited.test(filter) ? (count += dashboard['invited']) : null
        cancelled.test(filter) ? (count += dashboard['cancelled']) : null
        applicationUnderReview.test(filter)
          ? (count += dashboard['applicationUnderReview'])
          : null
        applicationIncomplete.test(filter)
          ? (count += dashboard['applicationIncomplete'])
          : null
        applicationComplete.test(filter)
          ? (count += dashboard['applicationComplete'])
          : null
        certified.test(filter) ? (count += dashboard['certified']) : null
        partiallyFunded.test(filter)
          ? (count += dashboard['partiallyFunded'])
          : null
      })
    } else {
      switch (true) {
        case /applications/.test(category):
          // count = dashboard['inProgress']
          count += dashboard['invited']
          count += dashboard['draft']
          count += dashboard['applicationUnderReview']
          count += dashboard['applicationIncomplete']
          count += dashboard['applicationComplete']
          break
        case /agreements/.test(category):
          count += dashboard['certified']
          count += dashboard['partiallyFunded']
          // count = dashboard['pendingDisbursement']
          break
        case /cancelled/.test(category):
          // count = dashboard['cancelled']
          count += dashboard['cancelled']
          break
        default:
          count = countTotals[0]['recordCount']
      }
    }
    return count
  }

  updatePageNumber = async () => {
    return await this.props.updatePagination(1, status)
  }

  clearFilter = async () => {
    const category = this.props.match.params.category
    const status = this.state.tabs[category]
    const customMatch = matchPath(this.props.location.pathname, {
      path:
        '/agreements/:category(applications|agreements|cancelled|all)/:pageNumber?',
      exact: false,
      strict: false
    })
    this.props.history.push(
      `/agreements/${customMatch.params.category}/${customMatch.params.pageNumber}`
    )

    return await this.props.updatePagination(1, status)
  }

  componentDidMount() {
    if (!this.props.agreements.isFetching) {
      this.loadPagination()
    }
  }

  render() {
    let { agreements, admin, location } = this.props
    let { modalOpen, certOpen, page } = this.state
    const agreementPageCount = this.pageCount()

    return (
      <section className="t__agreements-page">
        <article className="agreements-page agreements-page--submenu">
          <Subheader
            download={agreements.data ? true : false}
            filter={false}
            downloadActive={true}
            csvHeaders={csvHeaders}
            csvData={this.state.csvData}
            page={page}
            programData={agreements.data}
            schoolID={admin.schoolID}
            location_url={location}
          >
            <NavLink
              activeClassName="is-active"
              to={`/agreements/applications`}
            >
              <h3 className="main">Applications</h3>
            </NavLink>

            <NavLink activeClassName="is-active" to={`/agreements/agreements`}>
              <h3 className="main">Agreements</h3>
            </NavLink>

            <NavLink activeClassName="is-active" to={`/agreements/cancelled`}>
              <h3 className="main">Cancelled</h3>
            </NavLink>

            <NavLink activeClassName="is-active" to={`/agreements/all`}>
              <h3 className="main">All</h3>
            </NavLink>
          </Subheader>
        </article>

        <article className="agreements-page agreements-page--filters">
          {this.state.filtersArray.map((filter) => (
            <span className="agreement-filter" onClick={this.clearFilter}>
              <h3>Status: {filter}</h3>
            </span>
          ))}
        </article>

        <article className="agreements-page agreements-page--table">
          <ErrorBoundary>
            <Loader
              data={agreements.data}
              isFetching={agreements.isFetching}
              id={'agreementID'}
              zeroState={<AgreementsZeroState />}
            >
              <AgreementsComponent key={Math.random()} data={agreements.data} />

              <Pagination
                {...this.props}
                agreementsFlag={true}
                dataTabs={this.state.tabs}
                currentPage={'agreements'}
                matchUrl={
                  '/agreements/:category(applications|agreements|cancelled|all)/:pageNumber?'
                }
                students={agreements}
                handlePageChange={this.props.updatePagination}
                studentCountData={agreementPageCount}
              />
            </Loader>
          </ErrorBoundary>
        </article>

        {certOpen ? (
          <article className="m__cert-modal m__cert-modal--shadow">
            <div className="cert-modal--cert-banner">
              <h4>
                Go to the Certification App to finish drafts, certify students'
                agreements, and confirm disbursements.
              </h4>
              <button className="button" type="button">
                <Icon className="svg-link-out" icon="link-out" />
                <a
                  target="_blank"
                  href={`${window.baseRoute}/Certification`}
                  onClick={this._modalControl}
                >
                  CERTIFICATION
                </a>
              </button>
            </div>
          </article>
        ) : null}

        <RefreshModal open={modalOpen} onClose={this._modalControl} />
      </section>
    )
  }
}
export default AgreementsList
