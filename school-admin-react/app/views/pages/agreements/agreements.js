import React from 'react'
import { connect }  from 'react-redux'
import { matchPath } from 'react-router'
import { Route, Switch, Redirect } from 'react-router-dom'
import AgreementsList from './list'
import Agreement  from '~/app/views/pages/agreement/agreement'
import ErrorBoundary   from '~/app/views/pages/errors/errorBoundary'
import Loader from '~/app/views/components/loader'
import AgreementsZeroState from '~/app/views/components/zeroState/agreements'
import {
  loadAgreement,
  fetchAgreement,
  fetchAgreements
} from '~/app/store/actions/data'

class Agreements extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
    }
  }

  updatePagination = async(pageNumber, status) => {
    let { admin } = this.props

    await this.props.fetchAgreements(admin.schoolID, pageNumber, status)
  }

  componentDidMount() {
    const search = this.props.location.search
    const params = new URLSearchParams(search)
    const query = params.get('pageNumber') || 1
    let { admin } = this.props

    const match = matchPath(this.props.location.pathname, {
      path: '/agreements/agreement/:id',
      exact: false,
      strict: false
    })

    // If id found, find agreement, otherwise, just load all agreements
    match && match.params.id ?
      this.props.fetchAgreement(match.params.id, admin.schoolID) :
      null// this.props.fetchAgreements(admin.schoolID, query)
  }

  // This is a legacy method, update at some point
  componentWillReceiveProps(newProps) {
    let { location } = this.props
    //console.log('componentWillReceiveProps ',newProps, "\n location ",location.state)

    location.state ?
      this.props.loadAgreement(location.state.data) : null
  }
  componentDidUpdate(){
     let { admin } = this.props

    const match = matchPath(this.props.location.pathname, {
      path: '/agreements/agreement/:id',
      exact: false,
      strict: false
    })
    if(match && match.params.id ){
     //console.log('in component will receive props ',match.params.id,"\n new ",this.props.agreement.data.agreementID,"\n location",location.state)
     if((this.props.agreement.data.agreementID)&&(match.params.id!=this.props.agreement.data.agreementID)) {
         window.location.reload();
        
     }
    }
}

  render() {
    let { agreements, agreement, dashboard, countTotals, admin } = this.props
    
    return (
      <Switch>
        <Redirect
          exact from='/agreements'
          exact to='/agreements/applications' />
        <Route
          path={`/agreements/:category(applications|agreements|cancelled|all)/:pageNumber?`}
          component={props =>
            <ErrorBoundary>
              <Loader data={agreements.data} isFetching={agreements.isFetching} countTotals={countTotals} zeroState={<AgreementsZeroState />}>
                <AgreementsList
                  {...props}
                  agreements={agreements}
                  dashboard={dashboard}
                  countTotals={countTotals}
                  updatePagination={this.updatePagination}
                  admin={admin}/>
              </Loader>
            </ErrorBoundary>
          } />

        <Redirect
          exact from='/agreements/agreement/:id'
          exact to='/agreements/agreement/:id/info' />
        <Route
          exact path={`/agreements/agreement/:id/:category(info)`}
          component={props =>
            <ErrorBoundary>
              <Agreement {...props} agreement={agreement} />
            </ErrorBoundary>
          } />
      </Switch>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    agreements: state.data.agreementsData,
    agreement: state.data.agreementData,
    admin: state.auth.adminData,
    dashboard: state.data.dashboardData,
    countTotals: state.data.countTotals
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadAgreement: (agreement) => dispatch(loadAgreement(agreement)),
    fetchAgreement: (agreementID, schoolID) => dispatch(fetchAgreement(agreementID, schoolID)),
    fetchAgreements: (schoolID, page, status) => dispatch(fetchAgreements(schoolID, page, status)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Agreements)
