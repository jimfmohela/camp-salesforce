import React from 'react'
import { NavLink , Route, Switch, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'


import { matchPath } from 'react-router'
import Subheader from '~/app/views/components/shared/subheader/subheader'
import StudentAgreements from '~/app/views/components/student/agreements'
import StudentHistory from '~/app/views/components/student/history'
import StudentInfo from '~/app/views/components/student/info'
import StudentPayment from '~/app/views/components/student/payment'
import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'
import Loader from '~/app/views/components/loader'

import {fetchStudentData,
  fetchCommunicationHistoryData,
  loadStudentData,
  fetchStudentAgreements_V1,
  fetchStudentFees,
  fetchStudentIncomeVerification,
  fetchStudentV1,
  updateStudentV1,
  fetchAcademicEnrollmentStatus,
  UpdateAcademicEnrollmentStatus,
  UpdatedAcademicEnrollmentStatus,
  fetchPaymentHistory} from '~/app/store/actions/data'
import { maskFS } from '../../../helpers/fullstory'

const components = {
  agreements: StudentAgreements,
  history: StudentHistory,
  info: StudentInfo,
  payment: StudentPayment
}

class Student extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      component: 'agreements',
      funding: {
        certified: 0,
        disbursed: 0
      }
    }
  }

  componentDidMount() {
    const { student } = this.props
    this.props.fetchAcademicEnrollmentStatus()

    const match = matchPath(this.props.location.pathname, {
      path: '/student/:id',
      exact: false,
      strict: false
    })

    match && match.params.id
      ? (this.props.fetchStudentData(match.params.id),
      this.props.fetchStudentV1(match.params.id))
      : null
    if (student && !student.isFetching) {
      const studentFunding = student.data.length > 0 ? student.data : []
      if (studentFunding.length > 0) {
        this.setState({
          funding: {
            certified: studentFunding[0].amountCertifiedToDate || 0,
            disbursed: studentFunding[0].amountDisbursedToDate || 0
          }
        })
      }
      // console.log(match)
      this.props.fetchCommunicationHistoryData(match.params.id)
      this.props.fetchStudentAgreements_V1(match.params.id)
      this.props.fetchStudentFees(match.params.id)
      this.props.fetchStudentIncomeVerification(match.params.id)
      this.props.fetchPaymentHistory(match.params.id)
    }
  }

  // componentWillMount() {
  //   let { student } = this.props
  //   if (!student.isFetching) {
  //     console.log('will mount student', student)
  //     const studentFunding = student.data.length > 0 ? student.data : []
  //     console.log('fund', studentFunding)
  //     if (studentFunding.length > 0) {
  //       this.setState({
  //         funding: {
  //           certified: studentFunding[0].amountCertifiedToDate || 0,
  //           disbursed: studentFunding[0].amountDisbursedToDate || 0
  //         }
  //       })
  //     }
  //   }
  // }


  // componentWillReceiveProps() {
  //   let { location, } = this.props
  //   const match = matchPath(location.pathname, {
  //     path: '/students/student/:id',
  //     exact: false,
  //     strict: false
  //   })

  //   location.state ?
  //     (this.props.fetchStudentData(match.params.id),
  //      this.props.fetchStudentV1(match.params.id)) : null
  // }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.component !== this.props.match.params.filter) {
      this.setState({ component: this.props.match.params.filter })
    }
  }

  render() {
    const {
      match,
      student,
      communicationHistory,
      studentAgreements,
      studentFees,
      studentIncomeVerfication,
      studentv1,
      admin,
      enrollmentFormStatus,
      UpdatedAcademicEnrollmentStatus
    } = this.props
    let name = ''
    const StudentComponent = components[this.state.component]
    if (studentv1 && !studentv1.isFetching) {
      const studentInfo = studentv1 ? studentv1.data[0] : undefined
      name = studentInfo
        ? `${studentInfo.firstName || ''} ${studentInfo.lastName || ''}`
        || 'N/A'
        : ''
    }
    let showFundingAmount = true
    const currentPage = window.location.pathname.split('/')[
      window.location.pathname.split('/').length - 1
    ]
    if (currentPage === 'payment' || currentPage === 'info') {
      showFundingAmount = false
    }

    return (
      <ErrorBoundary>
        <Loader data={student.data} isFetching={student.isFetching}>
          <section className={`t__student-page t__student-page--${match.params.filter}`}>
            <article className="student-page student-page--submenu">
              {maskFS(<Subheader
                download={false}
                filter={false}
                header={name}
                fundingAmount={this.state.funding}
                showFundingAmount={showFundingAmount}
              >

                <NavLink
                  activeClassName="is-active"
                  to={`/student/${match.params.id}/agreements`}
                >
                  <h2 className="secondary">Agreements</h2>
                </NavLink>

                <NavLink
                  activeClassName="is-active"
                  to={`/student/${match.params.id}/info`}
                >
                  <h2 className="secondary">Info</h2>
                </NavLink>

                {/* <NavLink
                  activeClassName='is-active'
                  to={`/students/student/${match.params.id}/history`}>
                  <h2 className='secondary'>History</h2>
                </NavLink> */}

                <NavLink
                  activeClassName="is-active"
                  to={`/student/${match.params.id}/payment`}
                >
                  <h2 className="secondary">Payment</h2>
                </NavLink>

                      </Subheader>)}
            </article>
            <Switch>
              <Route
                exact
path={`/student/:id/:filter`}
                render={(props) => (
<article className='student-page student-page--table'>
                    <StudentComponent
                      {...props}
                      key={Math.random()}
                      data={student.data}
                      studentData={student.data[0]}
                      communicationHistory={communicationHistory}
                      studentAgreements={studentAgreements}
                      studentFees={studentFees}
                      incomeVerification={studentIncomeVerfication}
                      studentv1={studentv1}
                      updateStudentV1={this.props.updateStudentV1}
                      academicEnrollmentStatuses={this.props.academicEnrollmentStatuses}
                      paymentHistoryData={this.props.paymentHistoryData}
                      UpdateAcademicEnrollmentStatus={this.props.UpdateAcademicEnrollmentStatus}
                      admin={admin}
                      UpdatedAcademicEnrollmentStatus={UpdatedAcademicEnrollmentStatus}
                      enrollmentFormStatus={enrollmentFormStatus}
                    />
                  </article>
)}
              />
            </Switch>
          </section>
        </Loader>
      </ErrorBoundary>
    )
  }
}

const mapStateToProps = (state) => ({
    student: state.data.studentData,
    admin: state.auth.adminData,
    communicationHistory: state.data.communicationHistoryData,
    studentAgreements: state.data.agreementsDataV1,
    studentFees: state.data.studentFees,
    studentIncomeVerfication: state.data.studentIncomeVerfication,
    studentv1: state.data.studentv1,
    academicEnrollmentStatuses: state.data.academicEnrollmentStatuses,
    paymentHistoryData: state.data.paymentHistoryData,
    enrollmentFormStatus: state.data.enrollmentFormStatus,
    admin: state.auth.adminData
  })

const mapDispatchToProps = (dispatch) => ({
    fetchStudentData: (studentID, schoolID) => dispatch(fetchStudentData(studentID, schoolID)),
    fetchCommunicationHistoryData: (studentID) => dispatch(fetchCommunicationHistoryData(studentID)),
    loadStudentData: (student) => dispatch(loadStudentData(student)),
    fetchStudentAgreements_V1: (studentID) => dispatch(fetchStudentAgreements_V1(studentID)),
    fetchStudentFees: (studentID) => dispatch(fetchStudentFees(studentID)),
    fetchStudentIncomeVerification: (studentID) => dispatch(fetchStudentIncomeVerification(studentID)),
    fetchStudentV1: (studentID) => dispatch(fetchStudentV1(studentID)),
    updateStudentV1: (data) => dispatch(updateStudentV1(data)),
    fetchAcademicEnrollmentStatus: () => dispatch(fetchAcademicEnrollmentStatus()),
    UpdateAcademicEnrollmentStatus:(data, automaticUpdate)=>dispatch(UpdateAcademicEnrollmentStatus(data, automaticUpdate)),
    UpdatedAcademicEnrollmentStatus: (updateSuccess) => dispatch(UpdatedAcademicEnrollmentStatus(updateSuccess)),
    fetchPaymentHistory: (studentID) => { dispatch(fetchPaymentHistory(studentID)) }
  })

export default connect(mapStateToProps, mapDispatchToProps)(Student)
