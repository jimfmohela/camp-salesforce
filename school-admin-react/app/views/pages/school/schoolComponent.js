import React from 'react'
import InfoCard from '~/app/views/components/layout/InfoCard'
import InfoCell from '~/app/views/components/layout/InfoCell'
import Common from '~/app/assets/js/common'

const SchoolComponent = (props) => {
  const { data } = props

  return (
    <div className="o__student-info">
      <InfoCard title="School Information">
        <InfoCell label="School Name" className="single_row" data={data.schoolName || 'N/A'} />
        <InfoCell label="Vemo Account Number" className="single_row" data={data.accountNumber || 'N/A'} />
      </InfoCard>

      <InfoCard title="Your CampusServ Contact">
        <InfoCell label="CampusServ Contact" data={data.campusServiceName || 'N/A'} />
        <InfoCell label="CampusServ Email" data={data.campusServiceEmail || 'N/A'} />
        <InfoCell label="CampusServ Mobile" data={Common.formatPhoneNumber(data.campusServiceMobile)} />
        <InfoCell label="CampusServ Availability" data={data.campusServiceAvailability || 'N/A'} />
      </InfoCard>

      <InfoCard title="Your Students' Account Manager">
        <InfoCell label="Account Manager Contact" data={data.StudentCampusServiceName || 'N/A'} />
        <InfoCell label="Account Manager Email" data={data.StudentCampusServiceEmail || 'N/A'} />
        <InfoCell label="Account Manager Mobile" data={Common.formatPhoneNumber(data.StudentCampusServiceMobile)} />
        <InfoCell label="Account Manager Availability" data={data.StudentCampusServiceAvailability || 'N/A'} />
      </InfoCard>
    </div>
  )
}

export default SchoolComponent
