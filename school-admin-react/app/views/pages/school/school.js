import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchSchoolDetails } from '~/app/store/actions/data'
import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'
import Loader from '~/app/views/components/loader'
import SchoolComponent from './schoolComponent'

class SchoolPage extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        let { adminData } = this.props
        this.props.fetchSchoolDetails(adminData.schoolID)
        console.log('sdata', this.props.schoolData)
    }

    renderSchool = schoolData =>
        schoolData && schoolData.data ? (
            <section className={`t__student-page t__student-page--schooldetails`}>
                <article className="student-page student-page--table">
                    <ErrorBoundary>
                        <Loader data={schoolData.data} isFetching={schoolData.isFetching}>
                            <SchoolComponent data={schoolData.data[0]} />
                        </Loader>
                    </ErrorBoundary>
                </article>
            </section>
        ) :
            null

    render() {
        const { schoolData } = this.props
        return (
            <>
                {this.renderSchool(schoolData)}
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        adminData: state.auth.adminData,
        schoolData: state.data.schoolData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchSchoolDetails: (schoolID) => { dispatch(fetchSchoolDetails(schoolID)) }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(SchoolPage)