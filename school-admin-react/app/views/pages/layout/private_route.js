import React from 'react'
import { connect } from 'react-redux'
import { Route, withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'

const PrivateRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (auth.adminData.authenticated) {
        return <Component {...props} />
      }
      window.location = `${window.baseRoute}/SchoolAdminSiteLogin`
      return null
    }}
  />
)

PrivateRoute.propTypes = {
  component: PropTypes.any.isRequired,
  auth: PropTypes.shape({}).isRequired
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}

export default withRouter(connect(mapStateToProps)(PrivateRoute))
