import React, { Component } from 'react'
import Icon  from '~/app/assets/fonts/icon'
import { connect }  from 'react-redux'
import {
    readNotification
  } from '~/app/store/actions/data'

class deleteRecord extends Component {

    handleDeleteRecord =(event)=>{
        let { deleteData, index, objectName, allRecord } = this.props.data;
        console.log('this.props.data', this.props.data)

        if ( objectName === 'eventInstance'){
            allRecord[index]['read'] = true;
            deleteData['read'] = true;
            this.props.readNotification(deleteData,allRecord)
        }
        event.stopPropagation();
    }
    
    render() {
        return (
            <a onClick={this.handleDeleteRecord}>

               <Icon icon='delete' viewBox='0 0 22 22'/>  
            </a>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        readNotification: (dataToDelete,allRecord) => dispatch(readNotification(dataToDelete,allRecord)),
    }
  }

export default connect(null,mapDispatchToProps)(deleteRecord)
