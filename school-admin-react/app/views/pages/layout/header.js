import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import Modal from "~/app/views/components/shared/modal";
import Icon from "~/app/assets/fonts/icon";
import Hamburger from "~/app/views/components/layout/hamburger";
import Profile from "~/app/views/components/layout/profile";
import Notifications from "~/app/views/components/layout/notifications";
import MobileNotifications from "~/app/views/components/layout/mobileNotifications";
import HeaderSearch from "~/app/views/components/layout/search";
import MobileSearch from "~/app/views/components/layout/mobileSearch";
import HeaderComponent from "~/app/views/components/layout/header";

import { fetchSearchResults } from "~/app/store/actions/search";
import { fetchNotificationsData } from "~/app/store/actions/data";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state={
         unreadNotifications:[],
        
    }
  }

  componentDidMount() {
    this.props.fetchNotificationsData();
  }
  componentWillReceiveProps(props){
     if(props.notificationsData){
          const notificationsData=  props.notificationsData.filter(data => data.read==false);
          this.setState({
           unreadNotifications:notificationsData
          })
        }
  }

  filterPageName = () => {
    let name = this.props.location.pathname.split("/");
    // Strip any special characters
    return (name = name[1].replace(/[^\w\s]/gi, " "));
  };

  submitSearch = (searchQuery, filterOption) => {
    if (searchQuery.length > 1) {
      this.props.fetchSearchResults(searchQuery);

      this.props.history.replace({
        pathname: "/search",
        search: `?query=${searchQuery}&filter=${filterOption}`
      });
    }
  };
  componentWillReceiveProps(props){
   if(props.notificationsData){
      const notificationsData=  props.notificationsData.filter(data => data.read==false);
      this.setState({
       unreadNotifications:notificationsData
      })
    }
  }
  render() {
    let { props } = this;
    let { notificationsData, adminData } = props;
    return (
      <header>
        <nav className="t__header-main">
          <article className="header-main-title">
            <HeaderComponent {...props} filterPageName={this.filterPageName} />
          </article>

          <article className="header-main-desktop">
            <Modal
              activeClass="search-active"
              customClass="o__header-desktop o__header-desktop--search"
            >
              {bind => (
                <HeaderSearch {...bind} submitSearch={this.submitSearch} />
              )}
            </Modal>

             <Notifications 
              data={this.state.unreadNotifications} /> 

            <Profile data={adminData} />
          </article>

          <article className="header-main-mobile">
            <MobileSearch submitSearch={this.submitSearch} />
            {/* Adding notification icon for mobile display */}
            <MobileNotifications data={this.state.unreadNotifications} /> 
            <Hamburger />
          </article>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = state => {
  return {
    adminData: state.auth.adminData,
    notificationsData: state.data.notificationsData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchSearchResults: searchQuery =>
      dispatch(fetchSearchResults(searchQuery)),
    fetchNotificationsData: () => dispatch(fetchNotificationsData())
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Header)
);
