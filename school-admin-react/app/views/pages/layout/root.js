import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'

import Header from '~/app/views/pages/layout/header'
import Terms from '~/app/views/pages/static/terms'
import Privacy from '~/app/views/pages/static/privacy'
import About from '~/app/views/pages/static/about'
import Dashboard from '~/app/views/pages/dashboard/dashboard'
import DashboardNew from '~/app/pages/Dashboard'
import Students from '~/app/views/pages/students/students'
import Student from '~/app/views/pages/student/student'
import Agreements from '~/app/views/pages/agreements/agreements'
import Programs from '~/app/views/pages/programs/programs'
import Search from '~/app/views/pages/search/search'
import NotificationsPage from '~/app/views/pages/notifications/notifications'
import SettingsPage from '~/app/views/pages/settings/settings'
import SchoolPage from '~/app/views/pages/school/school'
import Error404 from '~/app/views/pages/errors/404'
import Loader from '~/app/views/components/loader'
import {
  fetchCountsData as fetchCountsDataAction,
  fetchDashboard as fetchDashboardAction,
  fetchCountsTotal as fetchCountsTotalAction,
  fetchCountStudentData as fetchCountStudentDataAction
} from '~/app/store/actions/data'

const renderOld = (Component) => {
  // eslint-disable-next-line react/prefer-stateless-function
  class oldWrapper extends React.Component {
    render() {
      return (
        <React.Fragment>
          <Header />
          <main>
            <Component {...this.props} />
          </main>
        </React.Fragment>
      )
    }
  }
  return oldWrapper
}

class Root extends React.Component {
  async componentDidMount() {
    const {
      fetchCountsData,
      fetchDashboard,
      fetchCountsTotal,
      fetchCountStudentData,
      auth: {
        adminData: { schoolID }
      },
      history
    } = this.props
    // Salesforce solution(hack, more like) for history-fallback routing
    const { controllerPath } = window

    if (typeof controllerPath !== 'undefined') {
      const routePath = controllerPath.split(/[[\]]{1,2}/)
      history.replace(routePath[1])
    }

    await fetchCountsData(schoolID)
    await fetchDashboard(schoolID)
    await fetchCountsTotal(schoolID)
    await fetchCountStudentData(schoolID)
  }

  render() {
    return (
      <React.Fragment>
        <Switch>
          <Redirect from="/" exact to={'/dashboard'} />
          <Route path="/dashboard/new" component={DashboardNew} />
          <Route path="/dashboard" component={renderOld(Dashboard)} />
          <Route path="/students" component={renderOld(Students)} />
          <Route path="/student/:id/:filter" component={renderOld(Student)} />
          <Route path="/programs" component={renderOld(Programs)} />
          <Route path="/agreements" component={renderOld(Agreements)} />
          <Route exact path="/about" component={renderOld(About)} />
          <Route exact path="/terms-of-use" component={renderOld(Terms)} />
          <Route exact path="/privacy-policy" component={renderOld(Privacy)} />
          <Route exact path="/search" component={renderOld(Search)} />
          <Route
            exact
            path="/notifications"
            component={renderOld(NotificationsPage)}
          />
          <Route exact path="/settings" component={renderOld(SettingsPage)} />
          <Route exact path="/school details" component={renderOld(SchoolPage)} />
          <Route path="" component={Error404} />
        </Switch>
      </React.Fragment>
    )
  }
}

Root.propTypes = {
  dashboard: PropTypes.shape({
    isFetching: PropTypes.bool
  }).isRequired,
  auth: PropTypes.shape({
    adminData: PropTypes.shape({
      schoolID: PropTypes.string
    })
  }).isRequired,
  fetchCountsData: PropTypes.func.isRequired,
  fetchDashboard: PropTypes.func.isRequired,
  fetchCountsTotal: PropTypes.func.isRequired,
  fetchCountStudentData: PropTypes.func.isRequired,
  history: PropTypes.any.isRequired
}

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    count: state.data.countData,
    dashboard: state.data.dashboardData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchCountsData: (schoolID) => dispatch(fetchCountsDataAction(schoolID)),
    fetchDashboard: (schoolID) => dispatch(fetchDashboardAction(schoolID)),
    fetchCountsTotal: (schoolID) => dispatch(fetchCountsTotalAction(schoolID)),
    fetchCountStudentData: (schoolID) => dispatch(fetchCountStudentDataAction(schoolID))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root)
