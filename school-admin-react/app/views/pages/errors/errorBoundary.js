import React from 'react'

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError(error) {
    return { hasError: true }
  }

  componentDidCatch(error, info) {
    console.log(error, info)
    // this.setState({ hasError: true })

    // You can also log the error to an error reporting service
    // logErrorToMyService(error, info);
  }

  _onClick = () => {
    console.log(this.props)
    this.props.history.goBack()
  }

  render() {
    return (
      this.state.hasError ?
        <div>
          <h1>We sincerly apologize, an error has occured on this page.</h1>
          <h2>The error has been logged.</h2> 
          <h2 onClick={this._onClick}>
            Please click here to go back to the last working page.
          </h2>
        </div> :
        this.props.children
    )
  }
}

export default ErrorBoundary
