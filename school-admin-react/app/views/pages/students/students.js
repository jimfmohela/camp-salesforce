import React from 'react'
import { connect } from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'
import { matchPath } from 'react-router'
import { fetchStudentsData } from '~/app/store/actions/data'

import StudentsList from './list'
import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'
import Loader from '~/app/views/components/loader'
import StudentsZeroState from '~/app/views/components/zeroState/students'
//import studentsPagination from '~/app/views/components/pagination/students'


class Students extends React.Component {
  constructor(props) {
    super(props)

  }
  updateStudentPagination = async (pageNumber, status) => {
    let { admin } = this.props
    //await this.props.fetchAgreements(admin.schoolID, pageNumber, status)
    await this.props.fetchStudentsData(admin.schoolID, pageNumber, status)
  }

  async  componentDidMount() {
    let { admin } = this.props
    const match = matchPath(this.props.location.pathname, {
      path: '/students/student/:id/',
      exact: false,
      strict: false
    })

    // If id found, find student, otherwise, just load all students
    match && match.params.id ?
      (this.props.fetchStudentData(match.params.id, admin.schoolID),
        this.props.fetchStudentV1(match.params.id)) :
      null //this.props.fetchStudentsData(admin.schoolID)
  }



  componentWillReceiveProps() {
    let { location, admin, } = this.props
    const match = matchPath(location.pathname, {
      path: '/students/student/:id',
      exact: false,
      strict: false
    })
    location.state ?
      (this.props.fetchStudentData(match.params.id, admin.schoolID),
        this.props.fetchStudentV1(match.params.id)) : null
  }

  render() {
    let { students } = this.props

    return (
      <Switch>
        <Redirect
          exact from='/students'
          exact to={'/students/in-school'} />
        <Route
          exact path={`/students/:filter(all|in-school|graduated)/:pageNumber?`}
          component={props =>
            <ErrorBoundary>
              {/*<Loader data={students.data} isFetching={students.isFetching} zeroState={<StudentsZeroState />}>*/}
              <StudentsList {...props} students={students} updateStudentPagination={this.updateStudentPagination} studentCountData={this.props.studentCountData} />
              {/*</Loader>*/}
              <studentsPagination />
            </ErrorBoundary>
          } />
      </Switch>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    students: state.data.studentsData,
    admin: state.auth.adminData,
    studentAgreements: state.data.agreementsDataV1,
    studentFees: state.data.studentFees,
    studentIncomeVerfication: state.data.studentIncomeVerfication,
    studentv1: state.data.studentv1,
    studentCountData: state.data.studentCountData,
    academicEnrollmentStatuses: state.data.academicEnrollmentStatuses,
    paymentHistoryData: state.data.paymentHistoryData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchStudentsData: (providerID, pageNumber, state) => dispatch(fetchStudentsData(providerID, pageNumber, state)),
    fetchStudentData: (studentID, schoolID) => dispatch(fetchStudentData(studentID, schoolID)),
    fetchCommunicationHistoryData: (studentID) => dispatch(fetchCommunicationHistoryData(studentID)),
    loadStudentData: (student) => dispatch(loadStudentData(student)),
    fetchStudentAgreements_V1: (studentID) => dispatch(fetchStudentAgreements_V1(studentID)),
    fetchStudentFees: (studentID) => dispatch(fetchStudentFees(studentID)),
    fetchStudentIncomeVerification: (studentID) => dispatch(fetchStudentIncomeVerification(studentID)),
    fetchStudentV1: (studentID) => dispatch(fetchStudentV1(studentID)),
    updateStudentV1: (data) => dispatch(updateStudentV1(data)),
    fetchAcademicEnrollmentStatus: () => dispatch(fetchAcademicEnrollmentStatus()),
    UpdateAcademicEnrollmentStatus:(data, automaticUpdate)=>dispatch(UpdateAcademicEnrollmentStatus(data, automaticUpdate)),
    fetchPaymentHistory: (studentID) => { dispatch(fetchPaymentHistory(studentID)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Students)
