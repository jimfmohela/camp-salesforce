import React from 'react'
import { NavLink } from 'react-router-dom'
import Subheader from '~/app/views/components/shared/subheader/subheader'

import Loader from '~/app/views/components/loader'
import StudentsComponent from '~/app/views/components/students/students'
import StudentsZeroState from '~/app/views/components/zeroState/students'
import { matchPath } from 'react-router'
import Icon from '~/app/assets/fonts/icon'
import Pagination from '~/app/components/Pagination'
import ErrorBoundary from '~/app/views/pages/errors/errorBoundary'

const csvHeaders = [
  { label: 'Name', key: 'name' },
  { label: 'Vemo Contract Number', key: 'vemoContractNumber' },
  { label: 'ISA Program Name', key: 'programs' },
  { label: 'Program/Course', key: 'programCourse' },
  { label: 'Graduation Date', key: 'graduation' },
  { label: 'Enrollment', key: 'enrollment' }
]

class StudentsList extends React.Component {
  constructor(props) {
    super(props)
    this.myRef = React.createRef()
    this.state = {
      csvFileName: 'all-students',
      csvData: [],
      data: [],
      filters: '',
      limit: 50,
      pageNumber: 0,
      finalRange: 0,
      tableOpacity: 1,
      totalCount: 0,
      totalPage: 0,
      showGoButton: false,
      tabs: {
        'in-school': [
          'Full Time',
          'Three Quarter Time',
          'Half Time',
          'Less than Half Time'
        ],
        graduated: ['Graduated']
      }
    }
  }

  componentDidMount() {
    if (!this.props.students.isFetching) {
      this.loadPagination()
    }
    //Calualte total page and set total number f records
    const filter = this.props.match.params.filter
    this.setState(
      {
        totalCount: this.props.studentCountData[filter]
      },
      () => {
        this.setState({
          totalPage: Math.ceil(
            parseInt(this.state.totalCount) / parseInt(this.state.limit)
          )
        })
      }
    )
  }

  filterStatus = (status) => {
    let { students } = this.props
    let inProgress = /\b(full|half|quarter)/i,
      approve = /\b(graduated)/i,
      filter = (regex) =>
        students.data.filter((student) => regex.test(student.status))

    switch (true) {
      case /in-school/.test(status):
        this.setState({ data: filter(inProgress) })
        break
      case /graduated/.test(status):
        this.setState({ data: filter(approve) })
        break
      default:
        this.setState({ data: students.data })
    }
  }

  updateTable = () => {
    let { students } = this.props
    let csvData = []

    students.data.forEach((student) => {
      if (student.agreements && student.agreements.length) {
        student.agreements.forEach((agreement) => {
          csvData.push({
            name: `${student.firstName} ${student.lastName}`,
            vemoContractNumber: student.vemoContractNumber || 'N/A',
            programCourse: student.description || 'N/A',
            programs: agreement.agreements || 'N/A',
            graduation: student.expectedGraduationDate || 'N/A',
            enrollment: student.status || 'N/A'
          })
        })
      } else {
        csvData.push({
          name: `${student.firstName} ${student.lastName}`,
          vemoContractNumber: student.vemoContractNumber || 'N/A',
          programCourse: student.description || 'N/A',
          programs: 'N/A',
          graduation: student.graduationDate || 'N/A',
          enrollment: student.status || 'N/A'
        })
      }
    })

    this.setState({ csvData })
  }

  loadPagination = async () => {
    let { updateStudentPagination, history, location } = this.props
    const filter = this.props.match.params.filter
    const status = this.state.tabs[filter]
    // Filters
    const params = new URLSearchParams(location.search)
    const query = params.get('filter')
    // Page Number
    // SWITCH THIS TO USE PROPS.MATCH INSTEAD
    const customMatch = matchPath(this.props.location.pathname, {
      path: '/students/:filter(in-school|all|graduated)/:pageNumber?',
      exact: false,
      strict: false
    })

    // Check if pagination is valid
    let pageNumber = customMatch.params.pageNumber || 0
    if (pageNumber <= 0) {
      this.props.history.replace(`/students/${customMatch.params.filter}/1`)

      return await this.props.updateStudentPagination(1, status)
    }

    // Update page number for re-mounts
    if (!this.state.pageNumber) {
      await this.setState({ pageNumber: customMatch.params.pageNumber })
    }
    // fetch record on reload
    if (!this.props.students.isLoaded) {
      return await updateStudentPagination(
        customMatch.params.pageNumber,
        status
      )
    }
  }

  render() {
    let { students } = this.props
    let { data, csvHeaders, csvData, csvFileName } = this.state
    return (
      <section className="t__students-page">
        <article className="students-page students-page--submenu">
          <Subheader
            download={false}
            filter={false}
            downloadActive={true}
            csvHeaders={csvHeaders}
            csvData={csvData}
            csvFileName={csvFileName}
          >
            <NavLink activeClassName="is-active" to={`/students/in-school`}>
              <h3 className="main">In School</h3>
            </NavLink>

            <NavLink activeClassName="is-active" to={`/students/graduated`}>
              <h3 className="main">Graduated</h3>
            </NavLink>

            <NavLink activeClassName="is-active" to={`/students/all`}>
              <h3 className="main">All</h3>
            </NavLink>
          </Subheader>
        </article>

        <article className="students-page students-page--table">
          <ErrorBoundary>
            <Loader
              data={students.data}
              isFetching={students.isFetching}
              id={'studentID'}
              zeroState={<StudentsZeroState />}
            >
              <div style={{ opacity: this.state.tableOpacity }}>
                <StudentsComponent key={Math.random()} data={students.data} />
              </div>
              <Pagination
                {...this.props}
                studentsFlag={true}
                currentPage={'students'}
                students={this.props.students}
                handlePageChange={this.props.updateStudentPagination}
                studentCountData={
                  this.props.studentCountData[this.props.match.params.filter]
                }
                matchUrl={
                  '/students/:filter(in-school|all|graduated)/:pageNumber?'
                }
              />
            </Loader>
          </ErrorBoundary>
        </article>
      </section>
    )
  }
}

export default StudentsList
