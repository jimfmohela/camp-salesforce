import React from "react";
import { connect } from "react-redux";
import { fetchDashboard, fetchCountsData,fetchCountStudentData } from "~/app/store/actions/data";
import DashboardComponent from "~/app/views/components/dashboard/component";
import Loader from '~/app/views/components/loader'
import DashboardZeroState from '~/app/views/components/zeroState/dashboard'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state={
       dashboardData:{},
    }
  }

 async componentDidMount() {
  //  console.log('mounting')
    // await this.props.fetchCountsData(this.props.auth.adminData.schoolID);
  }
  componentWillMount(){
    let { dashboard } = this.props
    let {draft,invited,cancelled,applicationIncomplete,applicationUnderReview,applicationComplete,certified,partiallyFunded}=dashboard;
    if(draft===0 && invited===0 && cancelled===0 && applicationIncomplete===0 && applicationUnderReview===0 && applicationComplete ===0 && certified===0 && partiallyFunded===0)
    {
          this.setState({dashboardData:null})
    }else{
      this.setState({dashboardData:dashboard})
    }
  }

  render() {
    let { dashboard, count } = this.props;

    return (
      <Loader data={this.state.dashboardData} isFetching={dashboard.isFetching} zeroState={<DashboardZeroState />} >
        <DashboardComponent data={dashboard} count={count}/>
      </Loader>
    )
  }
}

const mapStateToProps = state => {
  return {
    dashboard: state.data.dashboardData,
    count: state.data.countData.count,
    studentCountData:state.data.studentCountData
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCountsData: schoolID => dispatch(fetchCountsData(schoolID)),
    fetchCountStudentData: schoolID => dispatch(fetchCountStudentData(schoolID))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
