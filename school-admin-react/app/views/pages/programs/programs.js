import React from 'react'
import { connect }  from 'react-redux'
import { Route, Switch, Redirect } from 'react-router-dom'
import { matchPath } from 'react-router'
import Program     from '~/app/views/pages/program/program'
import ProgramsList from './list'
import ErrorBoundary   from '~/app/views/pages/errors/errorBoundary'
import { 
  fetchPrograms,
  fetchProgram,
  loadProgram,
} from '~/app/store/actions/data'
import ProgramsZeroState from '~/app/views/components/zeroState/programs'
import Loader from '~/app/views/components/loader'


class Programs extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const { admin } = this.props

    const match = matchPath(this.props.location.pathname, {
      path: '/programs/program/:id',
      exact: false,
      strict: false
    })

    // // If id found, find program, otherwise, just load all programs
    match && match.params.id ?
      this.props.fetchProgram(match.params.id, admin.schoolID) :
      this.props.fetchPrograms(admin.schoolID)
  }

  componentWillReceiveProps() {
    const { location, admin } = this.props

    const match = matchPath(location.pathname, {
      path: '/programs/program/:id',
      exact: false,
      strict: false
    })

    location.state ?
      this.props.fetchProgram(match.params.id, admin.schoolID) : null
  }

  render() {
    const { match, programs, program,admin,countData } = this.props

    return (
      <Switch>
        <Redirect 
          from='/programs' 
          exact to={'/programs/open'} />
        <Route 
          exact path={`${match.path}/:filter(planned|open|all|closed)`} 
          component={props => 
            <ErrorBoundary>
              <Loader data={programs.data} isFetching={programs.isFetching} zeroState={<ProgramsZeroState />}>
                <ProgramsList {...props} programs={programs} admin={admin} countData={countData}/> 
              </Loader>
            </ErrorBoundary>
          } />

        <Redirect 
          exact from='/programs/program/:id' 
          exact to={'/programs/program/:id/agreements'} />
        <Route 
          exact path={`/programs/program/:id/:filter(agreements|info)`} 
          component={props =>
            <ErrorBoundary>
              <Loader data={program.data} isFetching={program.isFetching} zeroState={<ProgramsZeroState />}>
               <Program {...props} program={program} />
              </Loader>
            </ErrorBoundary>
          } />
      </Switch>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    programs: state.data.programsData,
    program: state.data.programData,
    admin: state.auth.adminData,
    countData:state.data.countData
  }
}

const mapDispatchToProps = (dispatch) => ({
  loadProgram: (program) => dispatch(loadProgram(program)),
  fetchProgram: (programID, schoolID) => dispatch(fetchProgram(programID, schoolID)),
  fetchPrograms: (schoolID) => dispatch(fetchPrograms(schoolID)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Programs)
