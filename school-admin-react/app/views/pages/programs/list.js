import React from 'react'
import { NavLink } from 'react-router-dom'
import Subheader from '~/app/views/components/shared/subheader/subheader'
import ProgramsComponent from '~/app/views/components/programs/component'
import Loader from '~/app/views/components/loader'
import ProgramsZeroState from '~/app/views/components/zeroState/programs'

const csvHeaders = [
  { label: 'Program Name', key: 'programName' },
  { label: 'Program #', key: 'programNumber' },
  { label: 'Status', key: 'programStatus' },
  { label: 'Enrollment Type', key: 'enrollmentType' },
  { label: 'Certified Amount', key: 'amountCertifiedToDate' },
  { label: 'Disbursed Amount', key: 'amountDisbursedToDate' },
  { label: 'Number of Agreements', key: 'agreements' }
  ]

class ProgramsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      csvData: [],
      data: [],
      page: 'program'
    }
  }

  filterStatus = (status) => {
    const { programs } = this.props
    const open = /(open)/i,
        planned = /(planned)/i,
        closed = /(closed|cancelled)/i,
        filter = (regex) => programs.data.filter(program =>
          regex.test(program.programStatus)
        )

    switch (true) {
      case /planned/.test(status):
        this.setState({ data: filter(planned) })
        break 
      case /open/.test(status):
        this.setState({ data: filter(open) })
        break
      case /closed/.test(status):
        this.setState({ data: filter(closed) })
        break
      default:
        this.setState({ data: programs.data })
    }
  }

  updateTable() {
    const { programs } = this.props
    let csvData = []
    
    programs.data.forEach(data => {
      csvData.push({
        programName:  data.programName ? data.programName : 'N/A',
        programNumber:  data.programNumber ? data.programNumber : 'N/A',
        status:       data.programStatus ? data.programStatus : 'N/A',
        enrollment:   data.enrollmentType ? data.enrollmentType : 'N/A',
        certified:    data.amountCertifiedToDate ? `$${data.amountCertifiedToDate}` : 'N/A',
        disbursed:    data.amountDisbursedToDate ? `$${data.amountDisbursedToDate}` : 'N/A',
        agreements:   data.agreements ? data.agreements.length : 'N/A'
      })
    })

    this.setState({ csvData })
  }

  componentDidMount() {
    if (!this.props.programs.isFetching) {
      this.filterStatus(this.props.match.params.filter)
      //this.updateTable(this.props.match.params.filter)
    }
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.match.params.filter !== this.props.match.params.filter) {
      //this.updateTable(nextProps.match.params.filter)
    }
  }

  render() {
    const { programs,admin,countData, location } = this.props
    const { data, csvData, page } = this.state

    return (
      <section className='t__programs-page'>
        <article className='programs-page programs-page--submenu'>
          <Subheader
            filter={false}
            download={programs.data ? true : false}
            downloadActive={true}
            csvHeaders={csvHeaders} 
            csvData={csvData}
            page={page}
            programData={programs.data}
            schoolID={admin.schoolID}
            countData={countData}
            location_url={location}
            >

              <NavLink 
                activeClassName='is-active' 
                to={`/programs/planned`}>
                <h3>Planned</h3>
              </NavLink>

              <NavLink 
                activeClassName='is-active' 
                to={`/programs/open`}>
                <h3>Open</h3>
              </NavLink>
              
              <NavLink 
                activeClassName='is-active' 
                to={`/programs/closed`}>
                <h3>Closed</h3>
              </NavLink>

              <NavLink 
                activeClassName='is-active' 
                to={`/programs/all`}>
                <h3>All</h3>
              </NavLink>
          </Subheader>
        </article>

        <article className='programs-page programs-page--table isa-list'>
        <Loader data={programs.data} isFetching={programs.isFetching} zeroState={<ProgramsZeroState />} id={'programID'}>
            <ProgramsComponent key={Math.random()} data={data} />
        </Loader>
        </article>
      </section>
    )
  }
}

export default ProgramsList
