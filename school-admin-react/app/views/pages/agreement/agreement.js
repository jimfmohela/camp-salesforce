import React from 'react'
import { NavLink } from 'react-router-dom'
import { connect } from 'react-redux'

import Subheader from '~/app/views/components/shared/subheader/subheader'
import AgreementInfo from '~/app/views/components/agreement/info'
import Loader from '~/app/views/components/loader'
import {
  fetchAgreementMajor
} from '~/app/store/actions/data'

class Agreement extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    let { agreement, agreementMajor } = this.props;

    if (!agreement.isFetching
      && (agreement.data.majorID != agreementMajor.schoolProgramOfStudyID)) {
      this.props.fetchAgreementMajor(agreement.data.majorID);
    }

  }

  render() {
    let { match, agreement, agreementMajor } = this.props

    return (
      <section className='t__program-page'>
        <article className='program-page program-page--submenu'>
          <Subheader
            funding={true}
            download={false}
            header={agreement.data.vemoContractNumber || 'N/A'} >

            <NavLink
              activeClassName='is-active'
              to={`/agreements/agreement/${match.params.id}/info`}>
              <h3 className='main'>Info</h3>
            </NavLink>
          </Subheader>
        </article>

        <article className='program-page program-page--table'>
          <Loader isFetching={agreement.isFetching}>
            <AgreementInfo
              key={Math.random}
              data={agreement.data}
              major={agreementMajor} />
          </Loader>
        </article>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    agreementMajor: state.data.agreementMajorData,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchAgreementMajor: (majorID) => dispatch(fetchAgreementMajor(majorID)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Agreement)
