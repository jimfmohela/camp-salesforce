import React        from 'react'
import { connect }  from 'react-redux'
import Subheader    from '~/app/views/components/shared/subheader/subheader'

import SearchComponent from '~/app/views/components/search/component'
import Loader from '~/app/views/components/loader'
import SearchZeroState from '~/app/views/components/zeroState/search'

const filters = ['all', 'students', 'programs', 'agreements']

class Search extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      filterOption: '',
      SearchResult:[],
    }
  }

  updateFilter = () => {
    const search = this.props.location.search
    const params = new URLSearchParams(search)
    const query = params.get('query')
    const filter = params.get('filter')

    this.setState({ filterOption: filter })
  }

  componentDidMount() {
    this.updateFilter()
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.updateFilter()
    }
  }
  componentWillReceiveProps(props){
    let { searchResults } = props;
    console.log('search results', props)
    let result =searchResults.results[0];
    if(result['agreements'].numberOfResults===0 && result['programs'].numberOfResults===0 &&result['schools'].numberOfResults===0 &&result['students'].numberOfResults===0){
        this.setState({
          SearchResult:null
        })
    }else{
      this.setState({
        SearchResult:searchResults.result
      })
    }
   
  }


  render() {
    let { searchResults } = this.props
    return (
      <section className='t__search-page'>
        <article className='search-page search-page--submenu'>
        <Subheader download={false}>
            {filters.map(filter => 
              <>
                 <input 
                  type='radio' 
                  id={filter} 
                  name='filter-option' 
                  value={filter}
                  checked={filter === this.state.filterOption}
                  onChange={(e) => this.setState({ filterOption: e.target.value })} />
                <label for={filter}>{filter.toUpperCase()}</label>
              </>
            )}
          </Subheader>
        </article>

        <article className='search-page search-page--main'>
          <Loader isFetching={searchResults.isFetching} data={this.state.SearchResult} zeroState={<SearchZeroState />}> 
            {searchResults.results.map(result =>
              <SearchComponent 
                key={Math.random()} 
                data={result}
                filterOption={this.state.filterOption} />
            )}
          </Loader>
        </article>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    searchResults: state.search.searchResults
  }
}

export default connect(mapStateToProps, null)(Search)
