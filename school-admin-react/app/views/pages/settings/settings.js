import React from "react";
import { connect } from "react-redux";
import ClassNames from 'classnames'

import {
  fetchNotificatonSettingData,
  createNotification,
  deleteNotification,
  updateNotification

} from '~/app/store/actions/data'
import Icon from '~/app/assets/fonts/icon'

class SettingsPage extends React.Component {

  constructor(props) {
    super(props);
    this.statusRef = React.createRef();
    this.state = {
      // statuses: {
      //   "Application Under Review": { 'onVemo': false, 'onEmail': false },
      //   "Application Incomplete": { 'onVemo': false, 'onEmail': false },
      //   "Application Complete": { 'onVemo': false, 'onEmail': false },
      //   "Certified": { 'onVemo': false, 'onEmail': false },
      //   "Grace": { 'onVemo': false, 'onEmail': false },
      //   "Payment": { 'onVemo': false, 'onEmail': false },
      //   "Deferment": { 'onVemo': false, 'onEmail': false },
      //   "Cancelled": { 'onVemo': false, 'onEmail': false },
      //   "Pending Reconcillation": { 'onVemo': false, 'onEmail': false }
      // },
      statuses: {},
      updateStatus: [],
      checked: false,
      // toggle: true
    };
  }
  componentDidMount() {
    this.props.fetchNotificatonSettingData();
    //this.props.createNotification();
  }

  componentWillReceiveProps(nextProps) {
    let { notificationSettingData } = nextProps;
    //let newStatus = { ...this.state.statuses }

    this.setState({ statuses: { ...notificationSettingData } })

  }

  handleChange = (event) => {
    let newStasuses = { ...this.state.statuses }
    //when Onvemo checked
    if (event.target.name === 'onVemo' && (!newStasuses[event.target.value][event.target.name] === true)) {

      newStasuses[event.target.value][event.target.name] = !newStasuses[event.target.value][event.target.name]
      this.setState({
        statuses: { ...newStasuses },

      })
      var data = [];
      data.push(newStasuses[event.target.value])
      this.props.createNotification(data, newStasuses, event.target.value)

      //when onVemo is unchecked
    } else if (event.target.name === 'onVemo' && (!newStasuses[event.target.value][event.target.name] === false)) {
      newStasuses[event.target.value][event.target.name] = !newStasuses[event.target.value][event.target.name]
      //uncheck email checkbox
      newStasuses[event.target.value].email = false;
      this.setState({ statuses: { ...newStasuses } })

      this.props.deleteNotification(newStasuses[event.target.value], newStasuses, event.target.value)

    }
    else if (event.target.name === 'email' && this.state.statuses[event.target.value].onVemo) {
      newStasuses[event.target.value][event.target.name] = !newStasuses[event.target.value][event.target.name]
      this.setState({ statuses: { ...newStasuses } })
      var data = [];
      data.push(newStasuses[event.target.value])
      this.props.updateNotification(data, newStasuses)

    }


    // console.log('event: ', event.target.name)
    // console.log('event: ', event.target.value)
  }

  statusVisibility = (event) => {
    //let toggleStatus = { ...this.state.toggle };
    event.target.value==='true'  ?
      this.setState({ toggle: false })
      : this.setState({ toggle: true })

  }

  // statusVisibility = (event) => {
  //   let toggleStatus = { ...this.state.toggle };
  //   event.target.value==='true'  ?
  //     this.setState({ toggle: false })
  //     : this.setState({ toggle: true })

  // }

  render() {
    let { adminData, notificationSettingData, eventSubscriptionData } = this.props;

    let onEmailClass = ClassNames(
      {
        's__modal--active': this.state.modalActive,
      }
    )
    return (
      <section className="t__settings-page">
        <article className="notifications-page notifications-page--details">
          <h1>User Details</h1>
          <h6 for="profileName">Display Name</h6>
          <h4>{adminData.name}</h4>

          <h6 for="profileEmail">Email</h6>
          <h4>{adminData.userEmail}</h4>
        </article>
        <div className='switchDiv'>
          <h1>Notifications</h1>
          {/*<label class="switch">
            <input type="checkbox"  
               onChange={this.statusVisibility} 
               value={this.state.toggle} 
               checked={this.state.toggle}
            />
            <div class="slider round"></div>
    </label>*/}
        </div>
      
        <article className="notifications-page notifications-page--notifications" ref={this.statusRef}>

          <p>
            Set up your notifications to receive updates on Vemo and via email
            when an application's status changes to any of the statuses below
          </p>
          <ul className='titles'>
            <li><h2 className='secondary'>STATUS</h2></li>
            <li>
              <span><Icon icon='notifications-fill' /><h2 className='secondary'>ON VEMO</h2></span>
              <span><Icon icon='email-fill' viewBox='0 0 24 24'/><h2 className='secondary'> EMAIL</h2></span>
            </li>
          </ul>
          <ul className='rows'>
            {Object.keys(this.state.statuses).map((status, i) => (
              <li key={i} className='statuses'>
                <p>{status}</p>
                <div className="checkboxes">
                  <label>
                    <input
                      type="checkbox"
                      name="onVemo"
                      value={status}
                      checked={this.state.statuses[status].onVemo}
                      onChange={this.handleChange}
                    />
                    <span className="checkmark"></span>
                  </label>

                  <label className='emailLabel'>
                    <input
                      type="checkbox"
                      name="email"
                      value={status}
                      checked={this.state.statuses[status].email}
                      disabled={!(this.state.statuses[status].onVemo)}
                      onChange={this.handleChange}
                    />
                    <span className="checkmark"></span>
                  </label>
                </div>
              </li>
            ))}
          </ul>
        </article>
        
      </section>
    )
  }
}

const mapStateToProps = state => {
  return {
    adminData: state.auth.adminData,
    notificationSettingData: state.data.notificationSettingData,
    //eventSubscriptionData: state.data.eventSubscriptionData

  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetchNotificatonSettingData: () => dispatch(fetchNotificatonSettingData()),
    createNotification: (clickedStatusData, AllStatuses, eventTypeName) => dispatch(createNotification(clickedStatusData, AllStatuses, eventTypeName)),
    deleteNotification: (clickedStatusData, AllStatuses, eventTypeName) => dispatch(deleteNotification(clickedStatusData, AllStatuses, eventTypeName)),
    updateNotification: (clickedStatusData, AllStatuses) => dispatch(updateNotification(clickedStatusData, AllStatuses)),

  }

}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
