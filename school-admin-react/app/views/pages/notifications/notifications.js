import React from 'react'
import { connect }  from 'react-redux'
import { 
  fetchNotificationsData,
  readAllNotifications 
} from '~/app/store/actions/data'
import Icon from '~/app/assets/fonts/icon'
import { NavLink }  from 'react-router-dom'

import NotificationsComponent from '~/app/views/components/notifications/component'
import NotificationZeroState from '~/app/views/components/zeroState/notifications'

class NotificationsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      unreadNotifications:[], 
    }
  }

  componentWillMount() {
    let { notifications } = this.props
    
    notifications && (notifications.length && notifications[0]['eventID']) ?
      null :
      this.props.fetchNotificationsData()
  }
  componentDidMount(){
    const { notifications } = this.props
    if(notifications){
      const notificationsData=  notifications.filter(data => data.read==false);
      this.setState({
       unreadNotifications:notificationsData
      })
    }
  }
  

  componentWillReceiveProps(props){
       if(props.notifications){
          const notificationsData=  props.notifications.filter(data => data.read==false);
          this.setState({
           unreadNotifications:notificationsData
          })
        }
       
  }
  deleteAllNotifications=()=>{
    const { unreadNotifications } = this.state
    this.props.readAllNotifications(unreadNotifications)
  }

  render() {
    let { notifications } = this.props
   
    return (
      <section className='t__notifications-page'>
        <article className='notifications-page notifications-page--submenu'>
          <a className='notifications-page notifications-page--submenu--delete-all-notifications' onClick={this.deleteAllNotifications}>
          <Icon icon='delete' viewBox='0 0 22 22'/> 
                DELETE ALL NOTIFICATIONS
           </a>
          
           <NavLink className='notifications-page notifications-page--submenu--settings' to='/settings'>
            <Icon icon='settings-line'  viewBox='0 0 22 22' />
            <span>Notification Settings</span>
           </NavLink>
         
        </article>
        

        <article className='notifications-page notifications-page--table'>
          {this.state.unreadNotifications && (this.state.unreadNotifications.length && this.state.unreadNotifications[0]['eventID']) ?
            <NotificationsComponent data={this.state.unreadNotifications}/> :
            <NotificationZeroState />
          }
          </article>
      </section>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    notifications: state.data.notificationsData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchNotificationsData: () => dispatch(fetchNotificationsData()),
    readAllNotifications:(allRecord)=>dispatch(readAllNotifications(allRecord))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsPage)
