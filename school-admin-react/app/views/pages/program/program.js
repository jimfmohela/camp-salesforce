import React from 'react'
import { NavLink } from 'react-router-dom'

import Subheader  from '~/app/views/components/shared/subheader/subheader'
import ProgramAgreements from '~/app/views/components/program/agreements'
import ProgramInfo from '~/app/views/components/program/info'
import Loader from '~/app/views/components/loader'

const components = {
  agreements: ProgramAgreements,
  info: ProgramInfo,
}

class Program extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      component: 'agreements',
      fundingAmount: {
        certified: 0,
        disbursed: 0
      }
    }
  }
  componentWillMount(){
    let { match, program } = this.props

    this.setState({ 
      component: match.params.filter,
      fundingAmount: {
        certified: program.amountCertifiedToDate,
        disbursed: program.amountDisbursedToDate
      }
    })
  }

  componentDidMount() {
    let { match, program } = this.props

    this.setState({ 
      component: match.params.filter,
      fundingAmount: {
        certified: program.amountCertifiedToDate,
        disbursed: program.amountDisbursedToDate
      }
    })
  }

  render() {
    const { match, program } = this.props;
    const ProgramComponent = components[this.state.component]

    return (
      <section className={`t__program-page t__program-page--${match.params.filter}`}>
        <article className='program-page program-page--submenu'>
          <Subheader
            download={false}
            header={program['programName']}
            fundingAmount={this.state.fundingAmount}
            showFundingAmount={true}
            filter={true}>

            <NavLink 
              activeClassName='is-active' 
              to={`/programs/program/${match.params.id}/agreements`}>
              <h3 className='main'>Agreements</h3>
            </NavLink>

            <NavLink 
              activeClassName='is-active' 
              to={`/programs/program/${match.params.id}/info`}>
              <h3 className='main'>Info</h3>
            </NavLink>
          </Subheader>
        </article>

        <article className='program-page program-page--table'>
          <Loader data={program.data} isFetching={program.isFetching}>
            <ProgramComponent key={Math.random()} data={program} />
          </Loader>
        </article>
      </section>
    )
  }
}

export default Program
