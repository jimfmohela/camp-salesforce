import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import HeaderAction from '../HeaderAction'
// import Search from '~/app/views/components/search/component'
import Search from '~/app/components/Search'

const HeaderExpanded = (props) => {
  const { componentID, title, className } = props
  return (
    <div className={cn([`${componentID} --expanded`, className])}>
      <h1 className={`${componentID}-pageTitle`}>{title}</h1>
      <div className={`${componentID}-actions`}>
        <Search />
        <div>
          <HeaderAction icon="notificationsFill" />
        </div>
        <div>
          Kyle Wiedman
        </div>
      </div>
    </div>
  )
}

HeaderExpanded.defaultProps = {
  className: ''
}

HeaderExpanded.propTypes = {
  className: PropTypes.string,
  componentID: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default HeaderExpanded
