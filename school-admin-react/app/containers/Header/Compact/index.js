import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import HeaderAction from '../HeaderAction'

import { AppContext } from '~/app/hooks/app'

const HeaderMobile = (props) => {
  const {
    activeSidebar,
    updateActiveSidebar
  } = React.useContext(AppContext)
  const { componentID, className, title } = props
  return (
    <div className={cn([`${componentID} --compact`, className])}>
      <h1 className={`${componentID}-pageTitle`}>{title}</h1>
      <div className={`${componentID}-actions`}>
        <HeaderAction icon="search" />
        <HeaderAction icon="notificationsLine" />
        <HeaderAction
          icon="menu"
          onClick={() => {
            updateActiveSidebar(true)
          }}
        />
      </div>
    </div>
  )
}

HeaderMobile.defaultProps = {
  className: ''
}

HeaderMobile.propTypes = {
  className: PropTypes.string,
  componentID: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
}

export default HeaderMobile
