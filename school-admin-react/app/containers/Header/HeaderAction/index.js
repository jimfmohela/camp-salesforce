import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Icon from '~/app/components/Icon'
import BlockButton from '~/app/components/BlockButton'

const componentID = 'm-headerAction'

const HeaderAction = (props) => {
  const { className, icon, onClick, label} = props
  return (
    <BlockButton
      type="button"
      className={cn([componentID, className])}
      onClick={onClick}
      aria-label={label}
    >
      <Icon {...{ icon }} size={20} />
    </BlockButton>
  )
}

HeaderAction.defaultProps = {
  className: '',
  icon: undefined,
  onClick: (e) => {
    e.preventDefault()
    alert('you clicked!')
  }
}

HeaderAction.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
  onClick: PropTypes.func,
  label: PropTypes.string.isRequired
}

export default HeaderAction
