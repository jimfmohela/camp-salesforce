import React from 'react'
import PropTypes from 'prop-types'

import HeaderExpanded from './Expanded'
import HeaderCompact from './Compact'

import './styles.scss'
import { AppContext } from '~/app/hooks/app'

const componentID = 'o-header'

const Header = (props) => {
  const { isMobile } = React.useContext(AppContext)

  const { title } = props

  if (!isMobile) {
    return <HeaderExpanded {...{ componentID, title }} />
  }

  return <HeaderCompact {...{ componentID, title }} />
}

Header.defaultProps = {
  expanded: false
}

Header.propTypes = {
  expanded: PropTypes.bool,
  title: PropTypes.string.isRequired
}

export default Header
