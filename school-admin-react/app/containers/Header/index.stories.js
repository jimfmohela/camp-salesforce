import React from 'react'
import { storiesOf } from '@storybook/react'
import Provider from '~/.storybook/Provider'
import Header from './index'

import { AppContext } from '~/app/hooks/app'

storiesOf('Containers/Header', module)
  .addDecorator(Provider)
  .add(
    'Desktop',
    () => {
      const { updateIsMobileState } = React.useContext(AppContext)
      updateIsMobileState(false)
      return <Header title="Header" />
    }
  )
  .add(
    'Mobile',
    () => {
      const { updateIsMobileState } = React.useContext(AppContext)
      updateIsMobileState(true)
      return <Header title="Header" />
    },
    {
      options: { isMobile: true }
    }
  )
