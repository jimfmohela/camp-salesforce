import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'

import Link from "~/app/components/Link"
import VemoLogo from "~/app/components/VemoLogo"

const componentID = 'm-sidebarFooter'

const SidebarFooter = (props) => {
  const { className } = props
  return (
    <div className={cn([componentID, className])}>

      <Link icon="linkOut" href={`${window.baseRoute}/Certification`} className={`${componentID}-certLink`}>
        Certification
      </Link>

      <div className={`${componentID}-links`}>
        <Link href="#">Terms of Use</Link>
        <Link href="#">Privacy Policy</Link>
      </div>

      <div className={`${componentID}-logo`}>
        <VemoLogo />
      </div>

      <div className={`${componentID}-copyright`}>&copy; 2019 Vemo Education</div>
    </div>
  )
}

SidebarFooter.defaultProps = {
  className: ''
}

SidebarFooter.propTypes = {
  className: PropTypes.string
}

export default SidebarFooter
