import React from 'react'
import { storiesOf } from '@storybook/react'
import Sidebar from './index'
import Provider from '~/.storybook/Provider'

const links = [
  {
    href: '#',
    text: 'test',
    icon: 'notificationsFill'
  }
]
storiesOf('Containers', module)
.addDecorator(Provider)
.add('Sidebar', () => <Sidebar />)
