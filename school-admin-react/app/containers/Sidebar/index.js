import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import NavMenu from '~/app/components/NavMenu'
import SidebarFooter from './SidebarFooter'
import Link from '~/app/components/Link'
import UserMenu from '~/app/components/UserMenu'
import { AppContext } from '~/app/hooks/app'

import './styles.scss'

const componentID = 'o-sidebar'

const links = [
  {
    href: '/dashboard',
    icon: 'dashboardFill',
    text: 'Dashboard'
  },
  {
    href: '/programs',
    icon: 'isaLine',
    text: 'ISA Programs',
    active: true
  },
  {
    href: '/agreements',
    icon: 'applicationsLine',
    text: 'Agreements'
  },
  {
    href: '/students',
    icon: 'studentsLine',
    text: 'Students'
  }
]
const Sidebar = (props) => {
  const { className } = props
  const { isMobile } = React.useContext(AppContext)

  return (
    <div className={cn([componentID, className])}>
      <div className="logo">
        <img
          src="https://vemo--c.na82.content.force.com/servlet/servlet.ImageServer?id=01536000002MI72&oid=00D36000001HD53"
          alt="School Logo"
        />
      </div>

      {isMobile && (
        <UserMenu
          name="Kyle Wiedman"
          links={[
            <Link href="#" icon="settings" text="Settings" />,
            <Link href="#" icon="signout" text="Sign Out" />
          ]}
        />
      )}

      <NavMenu links={links} />
      <div className={`${componentID}-spacer`} />
      <SidebarFooter />
    </div>
  )
}

Sidebar.defaultProps = {
  className: ''
}

Sidebar.propTypes = {
  className: PropTypes.string
}

export default Sidebar
