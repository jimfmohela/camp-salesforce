import React from 'react'
import PropTypes from 'prop-types'
// // import { NavLink } from 'react-router-dom'
import Icon from '~/app/assets/fonts/icon'

import { connect } from 'react-redux'
import { fetchCountsData } from '~/app/store/actions/data'

// // import Loader from '~/app/views/components/loader'
import DashboardZeroState from '~/app/views/components/zeroState/dashboard'

import PanelRow from '~/app/components/PanelRow'
import PanelSection from '~/app/components/PanelSection'

// // import { nodeInternals } from 'stack-utils'
// //import AnimatedNumber from 'react-animated-number'

const componentID = 'o-dashboardPanel'

const buildTargetURL = (target) =>
  `${process.env.PUBLIC_URL}/agreements/applications/1?filters=${target}`

const initiationType = (programCount) => {
  if (!programCount) return 2

  const programTypes = { 'Invite Only': false, Open: false }

  let type = 0
  let i = 0

  if (programCount) {
    while (type < 2 && i < programCount.length) {
      if (!programTypes[programCount[i].enrollmentType]) {
        programTypes[programCount[i].enrollmentType] = true
        type += 1
      }
      i += 1
    }
  }

  if (type === 2) return type

  return (type = programTypes['Invite Only'] ? type : 0)
}

class DashboardPanel extends React.Component {
  async componentDidMount() {
    const { getCountsData } = this.props
    await getCountsData()
  }

  render() {
    const {
      dashboardData: data,
      dashboardData: {
        draft,
        invited,
        cancelled,
        applicationIncomplete,
        applicationUnderReview,
        applicationComplete,
        certified,
        partiallyFunded
      },
      count
    } = this.props

    const type = initiationType(count)

    if (
      [
        draft,
        invited,
        cancelled,
        applicationIncomplete,
        applicationUnderReview,
        applicationComplete,
        certified,
        partiallyFunded
      ].every((arg) => arg === 0)
    ) {
      return <DashboardZeroState />
    }

    return (
      <div className={`${componentID}`}>
        <PanelSection title="Applications" withDots>
          {type !== 0 && (
            <PanelRow
              title="Draft"
              count={data.draft}
              description="drafts need to be finished"
              action={{
                target: buildTargetURL('draft'),
                text: 'Finish'
              }}
            />
          )}

          {type !== 0 && (
            <PanelRow
              title="Invited"
              count={data.invited}
              description="invitations are waiting for students to sign up"
              action={{
                target: buildTargetURL('invited'),
                text: 'View'
              }}
            />
          )}

          <PanelRow
            title="Application Incomplete"
            count={data.applicationIncomplete}
            description="applications have been started but not yet finished"
            action={{
              target: buildTargetURL('application%20incomplete'),
              text: 'View'
            }}
          />

          <PanelRow
            title="Application Under Review"
            count={data.applicationUnderReview}
            description="applications are under review"
            action={{
              target: buildTargetURL('application%20under%20review'),
              text: 'View'
            }}
          />

          {type !== 1 && (
            <PanelRow
              title="Application Complete"
              count={data.applicationComplete}
              description="applications are complete"
              action={{
                target: buildTargetURL('application%20complete'),
                text: 'Finish'
              }}
            />
          )}
        </PanelSection>

        <PanelSection title="Agreements (Applications that have been certified)" withDots>
          <PanelRow
            title="Certified"
            count={data.certified}
            description="agreements are waiting for their first disbursement"
            action={{
              target: buildTargetURL('certified'),
              text: 'Confirm'
            }}
          />

          <PanelRow
            title="Partially Funded"
            count={data.partiallyFunded}
            description="agreements are partially funded and waiting for their remaining disbursements"
            action={{
              target: buildTargetURL('partially%20funded'),
              text: 'Confirm'
            }}
          />
        </PanelSection>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  dashboardData: state.data.dashboardData,
  count: state.data.countData.count,
  auth: state.auth
})

const mapDispatchToProps = (dispatch) => ({
  getCountsData: () => dispatch(fetchCountsData())
})

DashboardPanel.propTypes = {
  count: PropTypes.arrayOf(
    PropTypes.shape({
      statusCountList: PropTypes.any,
      programID: PropTypes.any,
      enrollmentType: PropTypes.any
    })
  ).isRequired,
  dashboardData: PropTypes.shape({
    isFetching: PropTypes.bool.isRequired,
    draft: PropTypes.number.isRequired,
    invited: PropTypes.number.isRequired,
    cancelled: PropTypes.number.isRequired,
    applicationIncomplete: PropTypes.number.isRequired,
    applicationUnderReview: PropTypes.number.isRequired,
    applicationComplete: PropTypes.number.isRequired,
    certified: PropTypes.number.isRequired,
    partiallyFunded: PropTypes.number.isRequired
  }).isRequired,
  getCountsData: PropTypes.func.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardPanel)
