import React from 'react'
import { storiesOf } from '@storybook/react'
import Provider from '~/.storybook/Provider'
import DashboardPanel from './index'

import mockStore from '~/.storybook/store.mocks'

const zeroState = {
  ...mockStore,
  data: {
    ...mockStore.data,
    dashboardData: {
      isFetching: false,
      isProgress: false,
      draft: 0,
      invited: 0,
      cancelled: 0,
      applicationIncomplete: 0,
      applicationUnderReview: 0,
      applicationComplete: 0,
      certified: 0,
      partiallyFunded: 0
    }
  }
}

storiesOf('Containers', module)
  .addDecorator(Provider)
  .add('DashboardPanel', () => <DashboardPanel />)
  .add('DashboardPanel--ZeroState', () => <DashboardPanel />, {
    options: { store: zeroState }
  })
