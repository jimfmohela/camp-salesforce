import React, { useContext } from 'react'
import PropTypes from 'prop-types'

import { SidebarFixed, SingleColumn } from '~/app/components/Layouts'
import Header from '~/app/containers/Header'
import Sidebar from '~/app/containers/Sidebar'

import { AppContext } from '~/app/hooks/app'

const DefaultLayout = (props) => {
  const { isDev, isMobile, activeSidebar } = useContext(AppContext)
  const { children, title, ...otherProps } = props

  const header = <Header {...{ title }} />

  if (isMobile) {
    return (
      <SingleColumn
        {...{
          activeSidebar,
          header,
          sidebar: <Sidebar className="--right" />,
          otherProps
        }}
      >
        {children}
      </SingleColumn>
    )
  }
  return (
    <React.Fragment>
      {isDev && (
        <div style={{ fontSize: '.7rem', background: '#efefef', textAlign: 'center', padding: '.25rem 1rem', color: '#934e00', borderBottom: '1px solid rgba(147, 78, 0, .25)', marginBottom: '.25rem' }}>
          Local Server
        </div>
      )}
      <SidebarFixed
        {...{
          header,
          sidebar: <Sidebar className="--left" />,
          otherProps
        }}
      >
        {children}
      </SidebarFixed>
    </React.Fragment>
  )
}

DefaultLayout.defaultProps = {
  children: undefined,
  header: undefined
}

DefaultLayout.propTypes = {
  children: PropTypes.node,
  header: PropTypes.node,
  title: PropTypes.string.isRequired
}

export default DefaultLayout
