import { createMuiTheme, fade } from '@material-ui/core/styles'

// Colors
const default_white = 'rgb(255, 255, 255)'
const catskill_white = 'rgb(245, 248, 250)'
const mystic_grey = 'rgb(218, 224, 230)'
const raven_grey = 'rgb(109, 115, 120)'
const dark_grey = 'rgb(102, 102, 102)'
const malachite_green = 'rgb(0, 215, 95)'
const green_haze = 'rgb(0, 159, 116)'
const error_red = 'rgb(242, 90, 85)'
const caution_yellow = 'rgb(247, 220, 62)'
const dark_yellow = 'rgb(245, 187, 61)'
const midnight = 'rgb(0, 28, 51)'
const vemo_blue = '#0073D1'
// const highlight_blue = 'rgb(0, 146, 255)'
const horizon_blue = 'rgb(92, 125, 153)'
const bismark_blue = 'rgb(64, 99, 128)'
const default_black = 'rgb(0, 0, 0)'
const accessible_error_red = '#D93025'
const saffron = '#F5BB3D'

// // Layout
// $header_height_mobile: 3.25rem;
// $header_height_tablet: 4.25rem;
// $max_width: 90rem;
// $desktop_nav_width: 12.5em;

// // Breakpoints
// $mobile:            32.5rem;
// $tablet:            43.75rem;
// $tablet_landscape:  64rem;
// $desktop:           75rem;

// // Indexes
// $lowest_index:  -1;
// $low_index:     100;
// $medium_index:  400;
// $high_index:    900;
// $higher_index:  990;
// $highest_index: 999;

const themeVariables = createMuiTheme({
  palette: {
    primary: {
      main: vemo_blue,
      darkest: midnight
    },
    secondary: {
      main: bismark_blue
    },

    success: {
      // light: 'blue',
      main: malachite_green
      // dark: 'blue',
      // contrastText: 'blue',
    },

    warning: {
      // light: 'blue',
      main: saffron
      // dark: 'blue',
      // contrastText: 'blue',
    },

    error: {
      // light: 'blue',
      main: error_red
      // dark: 'blue',
      // contrastText: 'blue',
    },

    text: {
      primary: midnight,
      secondary: raven_grey,
      success: green_haze,
      // warning: 'blue',
      error: accessible_error_red
    },
    divider: mystic_grey,
    border: mystic_grey,
    background: {
      default: catskill_white,
      secondary: mystic_grey
    }
  },
  spacing: 8,
  typography: {
    fontFamily:
      '"proxima-nova", "Montserrat", "Helvetica", "Arial", sans-serif',

    h1: {
      fontSize: '1.625rem',
      fontWeight: 700
    },
    h2: {
      fontSize: '0.8125rem',
      fontWeight: 700,
      letterSpacing: 1.5,
      textTransform: 'uppercase'
    },
    h3: {
      fontSize: '1.25rem',
      fontWeight: 700
    },
    h4: {
      fontSize: '1.125rem',
      fontWeight: 700
    },
    h5: {
      fontSize: '0.875rem',
      fontWeight: 700
    },
    h6: {
      fontSize: '0.75rem',
      fontWeight: 500
    },
    subtitle1: {
      fontSize: '0.875rem',
      fontWeight: 700
    },
    subtitle2: {
      fontSize: '1.4rem',
      fontWeight: 500
    },
    body1: {
      fontSize: '1rem',
      fontWeight: 600
    },
    body2: {
      fontSize: '0.875rem',
      fontWeight: 500
    },
    caption: {
      // fontSize: '1.6rem',
    },
    overline: {
      // fontSize: '1.4rem',
    }
  }
})

const theme = {
  ...themeVariables,
  overrides: {
    MuiButtonBase: {
      root: {
        background: 'none'
      }
    },
    MuiButton: {
      root: {
        borderRadius: 50,
        fontSize: '0.8125rem',
        fontWeight: 700,
        letterSpacing: '2px',
        lineHeight: 1,
        padding: '12px 30px',
        // textTransform: 'none' as 'none',
        whiteSpace: 'nowrap'
      },
      sizeSmall: {
        fontSize: '0.875rem',
        padding: '15px 30px'
      },
      sizeLarge: {
        fontSize: '1.25rem',
        padding: '25px 50px'
      },
      contained: {
        boxShadow: 'none'
      },
      outlined: {
        padding: '12px 30px',
        backgroundColor: catskill_white
      },
      fullWidth: {
        borderRadius: 0
      }
    },
    MuiCard: {
      root: {
        border: '1px solid #dee4e9',
        // boxShadow: 'none',
        borderRadius: themeVariables.shape.borderRadius * 2.5
      }
    },
    MuiFab: {
      root: {
        backgroundColor: themeVariables.palette.common.white,
        border: '1px solid rgba(64,99,128,0.2)'
      }
    },
    MuiInputBase: {
      root: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: themeVariables.palette.common.white,
        border: '2px solid #ced4da',
        fontSize: 16,
        transition: themeVariables.transitions.create([
          'border-color',
          'box-shadow'
        ]),
        width: '100%',

        'label + &': {
          marginTop: themeVariables.spacing(2.25)
        },

        '&$focused': {
          // boxShadow: 'inset 0 0 7px 0 rgba(0,0,0,0.2), 0 2px 10px 0 rgba(0,0,0,0.15)',
          // boxShadow: `0 0 0.5rem ${fade('#000', 0.25)}`,
          borderColor: themeVariables.palette.primary.main
        }
      },
      input: {
        padding: '10px 12px',

        '&:focus': {
          boxShadow: '0 0 7px 0 rgba(0,0,0,0.2), 0 2px 10px 0 rgba(0,0,0,0.15)'
          //   // boxShadow: `0 0 0.5rem ${fade('#000', 0.25)}`,
          //   borderColor: themeVariables.palette.primary.main
        },

        '&::-webkit-credentials-auto-fill-button': {
          // visibility: 'visible',
          backgroundColor: 'white'
        }
      },

      multiline: {
        padding: 0
      },

      inputMultiline: {
        lineHeight: '1.5rem',
        padding: '10px 12px'
      },

      formControl: {
        marginTop: '0 !important'
      }
    },
    MuiInputAdornment: {
      positionStart: {
        margin: '0 8px'
      }
    },
    MuiInputLabel: {
      formControl: {
        fontSize: '0.875rem',
        left: 'auto',
        position: 'initial',
        top: 'auto',
        transform: 'initial'
      }
    },
    MuiLinearProgress: {
      root: {
        borderRadius: 7,
        height: 14
      },
      colorPrimary: {
        backgroundColor: themeVariables.palette.divider
      },
      colorSecondary: {
        backgroundColor: themeVariables.palette.divider
      }
    },
    MuiLink: {
      root: {
        color: themeVariables.palette.text.secondary
      }
    },
    MuiListItemIcon: {
      root: {
        minWidth: 'auto'
      }
    },
    MuiSelect: {
      select: {
        borderRadius: 4
      },
      icon: {
        top: 'calc(50% - 14px)'
      }
    },
    MuiTooltip: {
      tooltip: {
        display: 'flex',
        fontSize: '0.875rem',
        borderRadius: '0.875rem',
        maxWidth: 200,
        backgroundColor: `${fade(themeVariables.palette.primary.darkest, 0.9)}`
      }
    }
  },

  props: {
    MuiCard: {
      elevation: 0
    },
    MuiFormControl: {
      fullWidth: true
    },
    MuiMenu: {
      getContentAnchorEl: null,
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'left'
      }
    },
    MuiTextField: {
      variant: 'standard',
      margin: 'normal'
    },
    MuiInput: {
      disableUnderline: true
    },
    MuiInputLabel: {
      shrink: false
    },
    MuiLink: {
      underline: 'always'
    }
  }
}

export default theme
