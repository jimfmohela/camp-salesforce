import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Helmet } from 'react-helmet'

// Store
import { Provider } from 'react-redux'
import configureStore from './store'
import serviceWorkers from '~/app/assets/js/service_workers'

// Components
import App from '~/app/views/app'

// Styles + Font
import WebFont from 'webfontloader'

import { AppContextProvider } from '~/app/hooks/app'
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import theme from '~/app/theme'

const isDevelopment = process.env.NODE_ENV === 'development'

const store = configureStore()
const supportsHistory = 'pushState' in window.history

WebFont.load({
  typekit: {
    id: 'zcj5bea'
  }
})

// Can't use .env cause salesforce, so this is what happens
const getControllerPath = () => {
  const sfTag = document.getElementById('rte')

  if (sfTag && sfTag.innerHTML.length > 0) {
    return sfTag.innerHTML
  }

  console.warn('no controller path found')
  return false
}

const getBaseRoute = () => {
  const sfTag = document.getElementById('sitebaseurl')

  if (sfTag && sfTag.innerHTML.length > 0) {
    return sfTag.innerHTML
  }

  if (isDevelopment) {
    return 'http://localhost:3000/schooladmin'
  }

  throw new Error(
    'Environment URL could not be determined; cannot run application'
  )
}

// Change this when there is time. There never is.
window.baseRoute = getBaseRoute()
window.controllerPath = getControllerPath()

const sfKeys = ['rte', 'sitebaseurl', 'backgroundPatternURL']
sfKeys.forEach((k) => {
  const ele = document.getElementById(k)
  if (ele) {
    ele.remove()
  }
})

if (typeof window !== 'undefined') {
  ReactDOM.render(
    <AppContextProvider {...{ isDevelopment, ...window }}>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
          <BrowserRouter
            basename={process.env.PUBLIC_URL}
            forceRefresh={!supportsHistory}
          >
            <React.Fragment>
              <Helmet>
                <title>School Admin : Vemo Education</title>
              </Helmet>
              <CssBaseline />
              <App />
            </React.Fragment>
          </BrowserRouter>
        </Provider>
      </ThemeProvider>
    </AppContextProvider>,
    document.getElementById('root')
  )
}

if (process.env.NODE_ENV === 'production') {
  serviceWorkers.register()
}
