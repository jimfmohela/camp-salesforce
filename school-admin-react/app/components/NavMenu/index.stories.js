import React from 'react'
import { storiesOf } from '@storybook/react'
import NavMenu from './index'

const links = [
  {
    href: 'http://www.google.com',
    icon: 'notificationsLine',
    text: 'Line'
  },
  {
    href: 'http://www.google.com',
    icon: 'notificationsLine',
    text: 'Line'
  }
]

storiesOf('Components', module).add('NavMenu', () => (
  <NavMenu {...{links}} />
))
