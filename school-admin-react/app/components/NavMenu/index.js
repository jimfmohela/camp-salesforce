import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'

import Icon from '~/app/components/Icon'
import Button from '~/app/components/Button'

const componentID = 'm-navMenu'

const NavMenu = (props) => {
  const { className, links } = props
  return (
    <nav className={cn([componentID, className])}>
      {links.map((l) => {
        const ico = l.icon ? <Icon icon={l.icon} size={24} /> : null
        const key = l.text.replace(' ', '')
        const className = cn(`${componentID}-link`, {
          '--active': l.active
        })
        return (
          <div {...{ key }} className={`${componentID}-row`}>
            <Button
              variant="NavLink"
              to={l.href}
              className={className}
              theme="nude"
            >
              {ico}
              {l.text}
            </Button>
          </div>
        )
      })}
    </nav>
  )
}

NavMenu.defaultProps = {
  className: '',
  active: false
}

NavMenu.propTypes = {
  className: PropTypes.string,
  links: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      icon: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      active: PropTypes.bool
    })
  ).isRequired
}

export default NavMenu
