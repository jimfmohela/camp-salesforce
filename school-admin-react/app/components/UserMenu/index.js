import React, { useState, useContext } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'
import Icon from '~/app/components/Icon'
import BlockButton from '~/app/components/BlockButton'

const componentID = 'o-userMenu'

const UserMenu = (props) => {
  const [isOpen, setOpen] = useState(false)
  const { className, name, links } = props

  const icon = isOpen ? 'arrowUp' : 'arrowDown'

  return (
    <div className={cn([componentID, className])}>
      <BlockButton
        className={`${componentID}-button`}
        onClick={() => {
          setOpen(!isOpen)
        }}
      >
        <span>{name}</span>
        <Icon {...{ icon }} />
      </BlockButton>
      {isOpen && (
        <nav className={`${componentID}-menu`} aria-label="User Settings Menu">
          {links.map((l, i) => (
            <div className={`${componentID}-menu-row`} key={i}>
              {l}
            </div>
          ))}
        </nav>
      )}
    </div>
  )
}

UserMenu.defaultProps = {
  className: '',
  name: 'Username'
}

UserMenu.propTypes = {
  className: PropTypes.string,
  link: PropTypes.arrayOf(PropTypes.Link),
  name: PropTypes.string
}

export default UserMenu
