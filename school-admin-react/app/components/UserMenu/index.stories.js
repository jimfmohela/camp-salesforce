import React from 'react'
import { storiesOf } from '@storybook/react'
import UserMenu from './index'
import Provider from '~/.storybook/Provider'

import Link from '~/app/components/Link'

const links =[
  <Link href="#" icon="settings" text="Settings" />,
  <Link href="#" icon="signout" text="Sign Out" />
]

storiesOf('Components', module)
  .addDecorator(Provider)
  .add('UserMenu', () => <UserMenu links={links} />)
