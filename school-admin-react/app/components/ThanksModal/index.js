import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography, DialogContent, Grid } from '@material-ui/core'
import Modal from '~/app/components/Modal'
import BlockButton from '~/app/components/BlockButton'
import '~/app/views/components/student/enrollment_status/style.scss'
import styles from './styles'
import { maskFS } from '../../helpers/fullstory'

const ThanksModal = (props) => {
  const classes = styles()
  const { open, setThanksModalStatus, data, studentData, UpdatedAcademicEnrollmentStatus } = props
  const componentID = 'm-blockButton'
  const closeModal = () => {
    setThanksModalStatus()
    UpdatedAcademicEnrollmentStatus(false)
  }
  return (
    <Modal
      open={open}
      title="Thanks for submitting your request"
      setExitModalStatus={closeModal}
    >
      <DialogContent className={classes.exitModal}>
        <Grid container alignContent="center" className={classes.content}>
          <Grid item className={classes.gridItem}>
            <Typography className={classes.contentText}>
              We have notified the Vemo Servicing Team of your request to change
                <b>
                {' '}
                {maskFS(studentData.firstName ? studentData.firstName : '', true)}
                {' '}
                {maskFS(studentData.lastName, true)}
                {'\'s '}
              </b>
              enrollment status to
              <b>
                {' '}
                {data}
              </b>
              . The team will confirm and update
                <b>
                {' '}
                {maskFS(studentData.firstName ? `${studentData.firstName}'s`  : '', true)}
              </b>
              information within 2-4 business days.
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignContent="center">
          <Grid item className={classes.gridItem}>
            <BlockButton
              type="button"
              className={`${componentID}-submit ${componentID}-thanks_modal_button`}
              onClick={closeModal}
            >
              I Understand
            </BlockButton>
          </Grid>
        </Grid>
      </DialogContent>
    </Modal>
  )
}

ThanksModal.propTypes = {
  open: PropTypes.bool.isRequired,
  setStatusModal: PropTypes.func.isRequired,
  setExitModalStatus: PropTypes.func.isRequired,
  data: PropTypes.string.isRequired,
  studentData: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Date)
  ]).isRequired
}

export default ThanksModal
