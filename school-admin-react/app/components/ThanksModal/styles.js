import { makeStyles } from '@material-ui/core/styles'


const styles = makeStyles((theme) => ({
    exitModal: {
        [theme.breakpoints.down('xs')]: {
            padding: `${theme.spacing(1)}px ${theme.spacing(1)}px`,
        }
    },
    content: {
        marginBottom: theme.spacing(5),
        textAlign: 'center',
        [theme.breakpoints.up('sm')]: {
            padding: `0 ${theme.spacing(17)}px`
        }
    },
    contentText: {
        fontWeight: 600,
        display: 'inline-block',
    },
    gridItem: {
        margin: 'auto'
    }
}))

export default styles
