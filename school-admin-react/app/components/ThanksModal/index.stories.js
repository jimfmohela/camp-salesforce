import React from 'react'
import { storiesOf } from '@storybook/react'
import { text, boolean, select } from '@storybook/addon-knobs'
import ThanksModal from './index'

storiesOf('Components', module).add('Thanks Modal', () => {
    const open = boolean('open', true)
    const data = select('data', {
      Withdrawn: 'Withdrawn',
      Graduated: 'Graduated',
      FullTime: 'Full Time'
    }, 'Withdrawn')
    return <ThanksModal open={open} studentData={{ firstName: 'Test', lastName: 'student'}} data={data} />
})
