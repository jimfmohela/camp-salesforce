import React from 'react'
import { storiesOf } from '@storybook/react'
import PanelSection from './index'
import { boolean, text, select } from '@storybook/addon-knobs'

const testArray = new Array(5).fill('row').map((r, i) => <div key={i}>{r}</div>)

const content = {
  Simple: 'No Component Here',
  Array: testArray,
  Mixed: ['Mixed String', testArray, <div>Mixed Div</div>, testArray]
}

storiesOf('Components/Panel', module).add('Section', () => {
  const contentSelection = select(
    'Content',
    ['Simple', 'Array', 'Mixed'],
    'Simple'
  )
  const withDots = contentSelection !== 'Simple' && boolean('With Dots', true)
  const showHeader = boolean('With Title', true)
  const title = showHeader && text('Title', 'Header')

  return (
    <PanelSection {...{ title, withDots }}>
      {content[contentSelection]}
    </PanelSection>
  )
})
