import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'

import PanelSectionHeader from '~/app/components/PanelSectionHeader'

const componentID = 'o-panelSection'

const getRow = (content, withDots = false, flatten = false, key = null) => {
  const rowClass = cn(`${componentID}-row`, {
    '--withDots': withDots
  })

  if (!flatten && Array.isArray(content)) {
    return content.map((r, i) => getRow(r, withDots, true, i))
  }

  return (
    <div className={rowClass} key={key}>
      {withDots && (
        <div className={`${componentID}-dot-container`}>
          <div className={`${componentID}-dot`} />
          <div className={`${componentID}-dot-bar`} />
        </div>
      )}
      <div className={`${componentID}-row-container`}>{content}</div>
    </div>
  )
}

const PanelSection = (props) => {
  const { title, withDots, withRows } = props
  let { children = [] } = props

  children = !Array.isArray(children) ? [children] : children

  if (withRows) {
    children = children.map((c, i) => {
      if (!c) {
        return null
      }

      if (Array.isArray(c)) {
        return getRow(c, withDots, false, i)
      }

      return getRow(c, withDots, true, i)
    })
  }
  return (
    <section className={`${componentID}`}>
      {title && <PanelSectionHeader {...{ title }} />}
      {children}
    </section>
  )
}

PanelSection.defaultProps = {
  withDots: false,
  withRows: true
}

PanelSection.propTypes = {
  children: PropTypes.node.isRequired,
  withDots: PropTypes.bool,
  withRows: PropTypes.bool,
  title: PropTypes.string.isRequired
}

export default PanelSection
