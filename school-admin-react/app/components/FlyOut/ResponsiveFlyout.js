import React from 'react';
import { withWidth } from "@material-ui/core/";
import { makeStyles } from "@material-ui/styles";
import FlyOutHelp from './FlyOutHelp';


const useStyles = makeStyles(theme => ({
    flyoutHeading: {
        fontSize: '.8125rem',
        fontWeight: 'bold',
        lineHeight: '1.063rem',
        letterSpacing: '0.09375rem',
        textTransform: 'uppercase'
    },
    flyoutPara: {
        fontSize: '.875rem', 
        fontWeight: 500, 
        lineHeight: '1.063rem'
    }
}));

const ResponsiveFlyout = ({ width }) => {
    const classes = useStyles();
    const isSmallScreen = /xs|sm/.test(width);
    const flyoutProps = {
        placement: isSmallScreen ? "top-start" : "right",
    };
    return (
        <FlyOutHelp
            htmltext={[<h4 className={classes.flyoutHeading}>What does Beta mean?</h4>, <br />,
            <p className={classes.flyoutPara}>This web app is brand new and is still being crafted as we start to accept new users. We are excited to release this first version (“Beta version”) to you and will continually improve it over time.</p>,
            ]}
            {...flyoutProps}
            styleProps={{ padding: '16px 20px', width: '328px' }}
        />
    );
}


export default withWidth()(ResponsiveFlyout);