import FlyOut from '~/app/components/FlyOut'
import FlyOutHelp from '~/app/components/FlyOut/FlyOutHelp'
import React from 'react'
import { storiesOf } from '@storybook/react'
import Icon from '~/app/assets/fonts/icon'
import { Typography } from '@material-ui/core'

storiesOf('Components', module).add('FlyOut', () => (
  <div>
    <FlyOut
      button={
        <Icon icon='help' viewBox='0 0 24 24' />
      }
      placement='top'
    >
      <Typography variant='h5' >
        What are income events?
      </Typography>
      <Typography variant='h6' >
        Income events are things like new jobs, promotions, bonuses, tips, and other changes related to your
        employment and income.
      </Typography>
    </FlyOut>
  </div>
))

storiesOf('Components', module).add('FlyOutHelp', () => (
  <FlyOutHelp
    text='Three program statuses: exist:'
    list={['Planned – The registration period has not started yet.', 'Open – The registration period has started.', 'Closed – The registration period has ended.']}
    placement='top'
  />
))