import { makeStyles } from '@material-ui/core/styles'


const styles = makeStyles((theme) => ({
  tooltip: {
    position: 'relative',
    maxWidth: '330px !important',
    opacity: '0.9 !important',
    borderRadius: '0.875em !important',
    backgroundColor: '#001C33 !important',
  },
  flyOutMenu: {
    padding: theme.spacing(.5)
  },
  listItem: {
    paddingTop: '0',
    paddingBottom: '0',
    alignItems: 'baseline',
    marginLeft: '0'
  },
  flyoutText: {
    color: '#FFF',
    fontSize: '.875rem',
    fontWeight: 500,
    lineHeight: '1.063rem',
  },
  helpIcon: {
    color: '#001C33',
    margin: 'auto .6rem',
  },
  bulletIcon: {
    color: '#FFF',
    minWidth: '0.75em',
    fontSize: '.875rem',
    fontWeight: 600,
    lineHeight: '1.063rem',
  }
}))

export default styles
