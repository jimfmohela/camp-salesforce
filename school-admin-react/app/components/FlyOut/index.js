import React from 'react'
import PropTypes from 'prop-types'
import { Tooltip } from '@material-ui/core'
import styles from './styles'

const FlyOut = (props) => {
  const { children, placement, button, styleProps } = props;
  const { flyOutMenu, ...classes } = styles();

  return (
    <Tooltip
      classes={classes}
      disableTouchListener
      title={
        <div className={flyOutMenu} style={styleProps}>
          {children}
        </div>
      }
      placement={placement}
    >
      <div>
        {button}
      </div>
    </Tooltip>
  )
}

FlyOut.propTypes = {
  children: PropTypes.node.isRequired,
  button: PropTypes.node.isRequired,
  placement: PropTypes.string
}

FlyOut.defaultProps = {
  placement: 'top'
}

export default FlyOut
