import React from 'react'
import PropTypes from 'prop-types'
import { Typography, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import Icon from '~/app/components/Icon'
import styles from '~/app/components/FlyOut/styles'
import FlyOut from '~/app/components/FlyOut'

const FlyOutHelp = (props) => {
  const classes = styles()
  const { placement, text, list, htmltext, styleProps } = props
  return (
    <FlyOut
      button={
        <Icon className={classes.helpIcon} icon="help" viewBox='0 0 24 24' size='22' />
      }
      placement={placement}
      styleProps={styleProps}
    >
    {
      text ? 
      <Typography
        classes={{ caption: classes.flyoutText }}
        variant="caption"
      >
        {text}
      </Typography>
      :
      htmltext
    }
      <List
        disablePadding={true}
      >
        {list && list.map(listItemText => {
          const bulletIcon = '\u2022'

          return (
            <ListItem
              disableGutters={true}
              classes={{ root: classes.listItem }}
            >
              <ListItemIcon
                classes={{ root: classes.bulletIcon }}
              >
                {bulletIcon}
              </ListItemIcon>
              <ListItemText
                classes={{ primary: classes.flyoutText }}
                primary={listItemText}
              />
            </ListItem>
          )
        })}
      </List>
    </FlyOut>
  )
}

FlyOutHelp.propTypes = {
  text: PropTypes.string,
  list: PropTypes.array,
  placement: PropTypes.string
}

export default FlyOutHelp
