import React from 'react'
import { storiesOf } from '@storybook/react'
import ExitModal from './index'

storiesOf('Components', module).add('Exit Modal', () => (
  <ExitModal open={true}/>
))
