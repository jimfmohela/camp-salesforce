import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Typography, DialogContent, Grid } from '@material-ui/core'
import Modal from '~/app/components/Modal/index'
import BlockButton from '~/app/components/BlockButton'
import '~/app/views/components/student/enrollment_status/style.scss'
import styles from './styles'

const ExitModal = (props) => {
  const classes = styles()
  const { open, setExitModalStatus, setEditModalStatus } = props
  const componentID = 'm-blockButton'
  return (
    <Modal
      open={open}
      title='You are currently editing information'
      closeIcon={false}
    >
      <DialogContent  className={classes.exitModal}>
        <Grid container alignContent="center" className={classes.content}>
          <Grid item className={classes.gridItem}>
            <Typography className={classes.contentText}>
              Are you sure you want to exit?
        </Typography>
          </Grid>
        </Grid>
        <Grid container alignContent="center">
          <Grid item className={classes.gridItem}>
            <BlockButton
              type="button"
              className={`${componentID}-submit ${componentID}-exit_modal_button`}
              onClick={setExitModalStatus}
            >
              No, go back and review
          </BlockButton>
          </Grid>
        </Grid>
        <Grid container alignContent="center">
          <Grid item className={classes.gridItem}>
            <BlockButton
              type="button"
              className={`${componentID}-cancel ${componentID}-exit_modal_button`}
              onClick={setEditModalStatus}
            >
              Yes, leave without saving
          </BlockButton>
          </Grid>
        </Grid>
      </DialogContent>
    </Modal>
  )
}

ExitModal.propTypes = {
  open: PropTypes.bool.isRequired,
  setStatusModal: PropTypes.func.isRequired,
  setEditModalStatus: PropTypes.func.isRequired,
}

export default ExitModal