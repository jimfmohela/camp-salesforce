import React from 'react'
import { storiesOf } from '@storybook/react'
import PanelRow from './index'

const storyProps = {
  title:"Test",
  description:"Test",
  count: 0,
  target:"Test",
  action:{
   target: "#",
   text: "Go"
  }
}

storiesOf('Components/Panel', module)
.add('Row', () => <PanelRow {...storyProps}  />)
