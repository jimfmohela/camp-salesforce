import React from 'react'
import PropTypes from 'prop-types'
import Button from '../Button'
import './styles.scss'

const componentID = `o-panelRow`

const PanelRow = (props) => {
  const {
    title,
    count,
    description,
    action: { target, text }
  } = props

  return (
    <div className={`${componentID}`}>
      <div className={`${componentID}-title`}>{title}</div>
      <div className={`${componentID}-count`}>{count}</div>
      <div className={`${componentID}-description`}>{description}</div>
      <div className={`${componentID}-action`}>
        <Button href={target}>{text}</Button>
      </div>
    </div>
  )
}

PanelRow.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  action: PropTypes.shape({
    target: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
  }).isRequired
}

export default PanelRow
