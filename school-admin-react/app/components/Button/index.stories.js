import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from './index'

const variant = (label, component) => ({ label, component })

const childElement = React.createElement(React.Fragment, {}, [
  React.createElement('i', {}, 'go to '),
  ' Google'
])

const linkProps = {
  text: 'Google',
  href: 'https://www.google.com'
}
const buttonProps = {
  variant: 'button',
  text: 'Google'
}
const inputProps = {
  variant: 'input',
  type: 'button',
  text: 'Google'
}

const variants = [
  variant('link', React.createElement(Button, linkProps)),
  variant(
    'link w/ children',
    React.createElement(
      Button,
      {
        ...linkProps,
        text: undefined
      },
      childElement
    )
  ),
  variant('button', React.createElement(Button, buttonProps)),
  variant(
    'button w/ children',
    React.createElement(
      Button,
      {
        ...buttonProps,
        text: undefined
      },
      childElement
    )
  ),
  variant('input', React.createElement(Button, inputProps)),
  { component: <hr /> },
  variant(
    'secondary',
    React.createElement(Button, {
      ...linkProps,
      theme: 'secondary'
    })
  ),
  { component: <hr /> },
  variant(
    'ghost',
    React.createElement(Button, {
      ...linkProps,
      theme: 'ghost'
    })
  )
]

storiesOf('Components', module).add('Button', () => (
  <React.Fragment>
    {variants.map((v) => (
      <div key={v.label} style={{ margin: '1rem' }}>
        {v.label && (
          <span
            style={{
              display: 'inline-block',
              marginRight: '.5rem',
              textAlign: 'right',
              width: '15rem'
            }}
          >
            {v.label}
          </span>
        )}
        {v.component}
      </div>
    ))}
  </React.Fragment>
))
