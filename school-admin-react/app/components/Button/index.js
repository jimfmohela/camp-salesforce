/* eslint-disable no-console */
import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { NavLink } from 'react-router-dom'
import './styles.scss'

const componentID = 'a-button'

const checks = (props) => {
  if (process.env.NODE_ENV === 'development') {
    const { variant, children, text } = props

    if (variant === 'input' && children) {
      console.warn(
        '<input /> variation cannot have children, they will be ignored.'
      )
    }

    if (!text && !children) {
      console.error(
        'either text or children should be provided to Button not both'
      )
    }

    if (text && children) {
      console.warn('children & text provided: text property will be ignored')
    }
  }
}

const Button = (props) => {
  const {
    variant,
    text,
    children,
    theme,
    type,
    className,
    disabled,
    to = props.href,
    ...addtProps
  } = props
  const isInput = variant === 'input'
  const isButton = variant === 'button'

  checks(props)

  // if (variant === 'NavLink') {
  //   return <NavLink className {...addtProps}>
  //     {children}
  //   </NavLink>
  // }

  return React.createElement(
    variant === 'NavLink' ? NavLink : variant,
    {
      className: cn([
        componentID,
        {
          '--disabled': disabled
        },
        `--${theme}`,
        className
      ]),
      defaultValue: isInput ? text : null,
      type: isInput || isButton ? type : null,
      to,
      disabled,
      ...addtProps
    },
    isInput ? null : children || text
  )
}

Button.defaultProps = {
  children: undefined,
  link: undefined,
  theme: 'default',
  text: undefined,
  type: undefined,
  variant: 'a',
  className: undefined,
  disabled: false,
  onClick: () => {}
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  link: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(['submit', 'button']),
  theme: PropTypes.oneOf(['default', 'primary', 'secondary', 'ghost', 'nude']),
  text: PropTypes.string,
  variant: PropTypes.oneOf(['a', 'button', 'input', 'NavLink']),
  disabled: PropTypes.bool
}

export default Button
