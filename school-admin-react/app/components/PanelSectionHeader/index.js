import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import Icon from '~/app/components/Icon'
import './styles.scss'

const componentID = 'm-panelSectionHeader'


const PanelSectionHeader = (props) => {
  const { title, className, icon } = props
  return (
    <header className={cn([componentID, className])}>
      <Icon className={cn([`${componentID}-icon`, className])} {...icon} />
      <div className={`${className}-text`}>{title}</div>
    </header>
  )
}

PanelSectionHeader.defaultProps = {
  className: '',
  icon: {
    icon: 'applicationsFill'
  }
}

PanelSectionHeader.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  icon: PropTypes.shape({ ...Icon.propTypes })
}

export default PanelSectionHeader
