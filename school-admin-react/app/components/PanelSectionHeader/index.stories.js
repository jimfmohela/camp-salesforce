import React from 'react'
import { storiesOf } from '@storybook/react'
import PanelSectionHeader from './index'

storiesOf('Components/Panel', module)
.add('Header', () => (
  <PanelSectionHeader title="Panel Section Header" />
))
