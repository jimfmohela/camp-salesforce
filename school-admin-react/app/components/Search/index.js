import React, { Component } from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'

import Icon from '~/app/components/Icon'
import Button from '~/app/components/Button'

const componentID = 'o-search'

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showMenu: false
    }
  }

  showMenu = () => {
    this.setState({ showMenu: !this.state.showMenu })
  }

  render() {
    const { className } = this.props

    return (
      <div className={cn([componentID, className])}>
        <div className={`${componentID}-categories`} onClick={this.showMenu}>
          <Icon icon="filter" />
          <span>All</span>
          <Icon icon="arrowDown" />

          {this.state.showMenu && (
            <ul className={`${componentID}-optionMenu --stacked`}>
              <li className="active">
                <span>All</span>
                <Icon icon="check" />
              </li>
              <li>Students</li>
              <li>Programs</li>
              <li>Agreements</li>
            </ul>
          )}
        </div>
        <input type="search" className={`${componentID}-searchInput`} />
        <Button type="submit" className={`${componentID}-action`} disabled>
          <Icon icon="search" />
          Search
        </Button>
      </div>
    )
  }
}

Search.defaultProps = {
  className: ''
}

Search.propTypes = {
  className: PropTypes.string
}

export default Search
