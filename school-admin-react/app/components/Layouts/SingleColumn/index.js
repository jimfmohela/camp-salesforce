import React from 'react'
import PropTypes from 'prop-types'

import './styles.scss'

const moduleID = 't-singleColumn'

const SingleColumn = (props) => {
  const { header, sidebar, children, activeSidebar } = props

  return (
    <div className={`${moduleID}`}>
      {header && <header className={`${moduleID}-header`}>{header}</header>}
      {activeSidebar && sidebar && <div className={`${moduleID}-sidebar`}>{sidebar}</div>}
      <main className={`${moduleID}-content`}>{children}</main>
    </div>
  )
}

SingleColumn.defaultProps = {
  header: <div>Default Header</div>,
  sidebar: <div>Default Sidebar</div>,
  children: null,
  activeSidebar: false
}

SingleColumn.propTypes = {
  header: PropTypes.element,
  sidebar: PropTypes.element,
  children: PropTypes.element,
  activeSidebar: PropTypes.bool
}

export default SingleColumn
