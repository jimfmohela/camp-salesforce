import React from 'react'
import PropTypes from 'prop-types'

// import '../../../scss/baseline.scss'
import './styles.scss'

const moduleID = 't-sidebarFixed'

const SidebarFixed = (props) => {
  const { header, sidebar, children } = props

  return (
    <div className={`${moduleID}`}>
      {header && <header className={`${moduleID}-header`}>{header}</header>}
      {sidebar && <div className={`${moduleID}-sidebar`}>{sidebar}</div>}
      <div className={`${moduleID}-content`} role="main">{children}</div>
    </div>
  )
}

SidebarFixed.defaultProps = {
  header: <div>Default Header</div>,
  sidebar: <div>Default Sidebar</div>,
  children: null
}

SidebarFixed.propTypes = {
  header: PropTypes.element,
  sidebar: PropTypes.element,
  children: PropTypes.element
}

export default SidebarFixed
