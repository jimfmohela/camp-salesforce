import React from 'react'
import { storiesOf } from '@storybook/react'

import { SingleColumn, SidebarFixed } from './index'

storiesOf('Components/Layouts', module)
  .add('SingleColumn', () => (
    <SingleColumn header={<div>Header</div>} sidebar={<div>Sidebar</div>}>
      <div>Content</div>
    </SingleColumn>
  ))
  .add('SidebarFixed', () => (
    <SidebarFixed header={<div>Header</div>} sidebar={<div>Sidebar</div>}>
      <div>Content</div>
    </SidebarFixed>
  ))
