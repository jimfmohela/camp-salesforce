import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'

import './styles.scss'

import icons from './index.data'

const componentID = 'svgIcon'

const Icon = (props) => {
  const {
    icon: iconID,
    className,
    viewBox: vb,
    size: height,
    size: width
  } = props

  const { icon = icons[iconID], viewBox = vb || '0 0 1024 1024' } = icons[
    iconID
  ] || {}

  return (
    <svg
      className={cn([componentID, className, `${componentID}-${iconID}`])}
      preserveAspectRatio="xMinYMin"
      {...{ viewBox, height, width }}
    >
      <path d={icon} style={{fill:props.fill}} />
    </svg>
  )
}

Icon.defaultProps = {
  className: undefined,
  viewBox: undefined,
  size: 16
}

Icon.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.oneOf([...Object.keys(icons)]).isRequired,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  viewBox: PropTypes.string
}

export default Icon
