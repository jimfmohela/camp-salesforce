import React from 'react'
import { storiesOf } from '@storybook/react'
import Icon from './index'
import icons from './index.data'

const style = {
  display: 'flex',
  alignItems: 'center',
  padding: '.25rem 1rem'
}

storiesOf('Components', module).add('Icon', () => (
  <React.Fragment>
    {Object.keys(icons).map((k) => (
      <div style={style} key={k}>
        <span
          style={{
            border: '1px solid gray'
          }}
        >
          <Icon
            icon={k}
          />
        </span>
        <span style={{ marginLeft: '.5rem' }}>{k}</span>
        &nbsp;&nbsp;
        <small>{icons[k].viewBox || 'default'}</small>
      </div>
    ))}
  </React.Fragment>
))
