import { makeStyles } from '@material-ui/styles'

const styles = makeStyles((theme) => {
  const borderRadius = 50
  const minHeightDesktop = theme.spacing(7.5)
  const minHeightMobile = theme.spacing(10.5)

  return {
    SnackbarContent: {
      backgroundColor: `${theme.palette.background.paper} !important`,
      borderRadius,
      padding: 0,
      [theme.breakpoints.up('md')]: {
        width: '100%',
        marginRight: theme.spacing(2.5)
      },
      [theme.breakpoints.down('sm')]: {
        borderRadius: theme.spacing(3.75)
      },
      marginTop: theme.spacing(6)
    },
    SnackbarContentMessage: {
      alignItems: 'center',
      display: 'flex',
      fontSize: theme.spacing(2.5),
      fontWeight: 'bold',
      lineHeight: '27px',
      height: minHeightDesktop,
      letterSpacing: theme.spacing(-0.026),
      justifyContent: 'space-between',
      minHeight: minHeightDesktop,
      [theme.breakpoints.down('sm')]: {
        height: theme.spacing(10.5),
        minHeight: minHeightMobile
      }
    },
    SnackbarContentMessageIconWrapper: {
      alignItems: 'center',
      borderRadius: `${borderRadius}px 0 0 ${borderRadius}px`,
      color: theme.palette.common.white,
      display: 'flex',
      minHeight: minHeightDesktop,
      padding: theme.spacing(0, 1.875),
      [theme.breakpoints.down('sm')]: {
        borderRadius: `${theme.spacing(3.75)}px 0 0 ${theme.spacing(3.75)}px`,
        minHeight: minHeightMobile
      }
    },
    SnackbarContentWrapper: { display: 'flex', alignItems: 'center'},
    SnackbarContentMessageTextWrapper: { padding: theme.spacing(0, 4.25, 0, 1.75) },
    SnackbarContentLinkIcon : { marginRight: theme.spacing(1.25) }
  }
})

export default styles
