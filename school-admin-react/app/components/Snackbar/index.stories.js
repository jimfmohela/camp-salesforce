import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { storiesOf } from '@storybook/react'
import { text, boolean, select } from '@storybook/addon-knobs'
import Snackbar from './index'

storiesOf('Snackbar', module).add('basic', () => {
  const open = boolean('open', true)
  const status = select('status', {
    success: 'success',
    error: 'error',
    warning: 'warning',
    bank:'bank',
    bankUpdate:'bank-update',
    pending:'pending'
  }, 'success')
  const link = boolean('link',true)
  const message = text('message', 'things go here')
  const vertical= select('vertical', { 
    top: 'top',
    bottom: 'bottom'
  }, 'top')
  const horizontal= select('horizontal', {
    center: 'center',
    left: 'left',
    right: 'right'
  }, 'center')
  return(
      <Snackbar 
        open={open} 
        status={status}
        message={message} 
        link={link}
        vertical={vertical}
        horizontal={horizontal}
      /> 
    )
})
