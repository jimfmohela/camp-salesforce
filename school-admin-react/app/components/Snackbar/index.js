import React from 'react'
import PropTypes from 'prop-types'
import { Snackbar as MuiSnackbar, SnackbarContent } from '@material-ui/core'
import Icon from '../Icon'
import styles from './styles'
import statusColors from '../../helpers/statusColors'

const Snackbar = (props) => {
  const { open, status, message, link, className, autoHideDuration, onClose, onExited, ClickAwayListenerProps, vertical, horizontal } = props
  const classes = styles()
  const statusObject = statusColors(status)

  return (
    <MuiSnackbar
      anchorOrigin={{ vertical, horizontal }}
      open={open}
      className={className}
      onClick={link}
      autoHideDuration={autoHideDuration}
      onClose={onClose}
      onExited={onExited}
      ClickAwayListenerProps={ClickAwayListenerProps}
    >
      <SnackbarContent
        classes={{
          root: classes.SnackbarContent,
          message: classes.SnackbarContentMessage,
        }}
        style={{ backgroundColor: statusObject.backgroundColor, cursor: statusObject.cursor }}
        message={
          <div className={classes.SnackbarContentMessage} style={{ color: statusObject.textColor }}>
            <div className={classes.SnackbarContentWrapper}>
              <div
                className={classes.SnackbarContentMessageIconWrapper}
                style={{ backgroundColor: statusObject.color }}
              >
                <Icon icon={statusObject.icon} fill='#fff' size="24" />
              </div>
              <div className={classes.SnackbarContentMessageTextWrapper}>{message}</div>
            </div>
            {link && <Icon className={classes.SnackbarContentLinkIcon} icon="arrow-right" />}
          </div>
        }
      />
    </MuiSnackbar>
  )
}

Snackbar.propTypes = {
  open: PropTypes.bool.isRequired,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  link: PropTypes.func,
  className: PropTypes.string,
  autoHideDuration: PropTypes.number,
  onClose: PropTypes.func,
  ClickAwayListenerProps: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  vertical: PropTypes.string,
  horizontal: PropTypes.string
}

Snackbar.defaultProps = {
  link: null,
  className: '',
  autoHideDuration: null,
  onClose: ()=>{},
  ClickAwayListenerProps: null,
  vertical: 'top',
  horizontal: 'center'
}
export default Snackbar
