import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import './styles.scss'

const componentID = 'm-blockButton'

const BlockButton = (props) => {
  const { className, children, ...otherProps } = props
  return (
    <button type="button" className={cn([componentID, className])} {...otherProps}>
      <span className={`${componentID}-wrapper`}>{children}</span>
    </button>
  )
}

BlockButton.defaultProps = {
  className: ''
}

BlockButton.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node.isRequired
}

export default BlockButton
