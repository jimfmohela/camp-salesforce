import React from 'react'
import PropTypes from 'prop-types'
import './styles.scss'
import Icon from '~/app/assets/fonts/icon'

const ErrorModal = (props) => {
  const { errorObj, ReferToStatus, allRef } = props
  
  const ReferToLOAStartDate = () => {
    allRef.LOAStartDateRef.current.focus()
  }
  const ReferToLOAEndDate = () => {
    allRef.LOAEndDateRef.current.focus()
  }
  const ReferToNotes = () => {
    allRef.notesRef.current.focus()
  }
  const ReferToLastToAttendance = () => {
    allRef.dateRef.current.focus()
  }
  const ReferToEffectiveDate = () => {
    allRef.effectiveDateRef.current.focus()
  }
  return (
    <>
      <div className="error_modal">
        <div className="error_modal--left">
          <Icon icon="error-fill" viewBox="0 0 24 24" />
        </div>
        <div className="error_modal--right">
          <>
            <h3>Something is not right...</h3>
            <br />
            {Object.keys(errorObj).map((item) => (
              <>
                {(item === 'status' && errorObj[item])
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToStatus}>
                        <span style={{ width: '93%' }}>Please select an enrollment status before submitting.</span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}

                {(item === 'date' && errorObj[item])
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLastToAttendance}>
                        <span style={{ width: '93%' }}>Please enter the date.</span>
                        <Icon icon="arrow-right" />
                      </a>

                    </div>
                  )}
                {(item === 'minDateError' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLastToAttendance}>
                        <span style={{ width: '93%' }}>
                          Please enter a date after
                          {' '}
                          {errorObj[item].minDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'maxDateError' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLastToAttendance}>
                        <span style={{ width: '93%' }}>
                          Please enter a date before
                          {' '}
                          {errorObj[item].maxDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'effectiveDate' && errorObj[item])
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToEffectiveDate} >
                        <span style={{ width: '93%' }}>
                          Please enter the effective date
                          {' '}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'effectiveDateMinError' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToEffectiveDate}>
                        <span style={{ width: '93%' }}>
                          Please enter a date after
                          {' '}
                          {errorObj[item].minDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'effectiveDateMaxError' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToEffectiveDate}>
                        <span style={{ width: '93%' }}>
                          Please enter a date before
                          {' '}
                          {errorObj[item].maxDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceStartDate' && errorObj[item])
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLOAStartDate} >
                        <span style={{ width: '93%' }}>
                          Please enter the Leave of Absence Start Date                         
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceStartMinDate' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLOAStartDate}>
                        <span style={{ width: '93%' }}>
                          Please enter a date after
                          {' '}
                          {errorObj[item].minDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceStartMaxDate' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLOAStartDate}>
                        <span style={{ width: '93%' }}>
                          Please enter a date before
                          {' '}
                          {errorObj[item].maxDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceEndDate' && errorObj[item])
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLOAEndDate} >
                        <span style={{ width: '93%' }}>
                          Please enter the Leave Of Absence End date.
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceEndMinDate' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick={ReferToLOAEndDate}>
                        <span style={{ width: '93%' }}>
                          Please enter a date after
                          {' '}
                          {errorObj[item].minDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'absenceEndMaxDate' && errorObj[item].error)
                  && (
                    <div style={{ marginTop: 10 }}>
                      <a onClick = {ReferToLOAEndDate} >
                        <span style={{ width: '93%' }}>
                          Please enter a date before
                          {' '}
                          {errorObj[item].maxDate}
                        </span>
                        <Icon icon="arrow-right" />
                      </a>
                    </div>
                  )}
                {(item === 'notes' && errorObj[item])
                    && (
                      <div style={{ marginTop: 10 }}>
                        <a onClick={ReferToNotes}>
                          <span style={{ width: '93%' }}>Please make sure your notes/comments are fewer than 500 characters.</span>
                          <Icon icon="arrow-right" />
                        </a>
                      </div>
                    )}
                {(item === 'sixMonthValidation' && errorObj[item])
                    && (
                      <div style={{ marginTop: 10 }}>
                        <a onClick={ReferToLOAEndDate}>
                          <span style={{ width: '93%' }}>Your request is longer than 6 months. Please contact your CampusServ Account Manager to discuss this.</span>
                          <Icon icon="arrow-right" />
                        </a>
                      </div>
                    )}
                {(item === 'isStartDateSmaller' && errorObj[item])
                    && (
                      <div style={{ marginTop: 10 }}>
                        <a onClick={ReferToLOAStartDate}>
                          <span style={{ width: '93%' }}>Start date should be before end date.</span>
                          <Icon icon="arrow-right" />
                        </a>
                      </div>
                    )}        


              </>
            ))}


          </>
        </div>
      </div>
    </>
  )
}

ErrorModal.propTypes = {
  errorObj: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool
  ]).isRequired,
  ReferToLastToAttendance: PropTypes.element,
  ReferToNotes: PropTypes.element,
  ReferToStatus: PropTypes.element,
  ReferToEffectiveDate: PropTypes.element
}
ErrorModal.defaultProps = {
  ReferToLastToAttendance: null,
  ReferToNotes: null,
  ReferToStatus: null,
  ReferToEffectiveDate: null
}
export default ErrorModal
