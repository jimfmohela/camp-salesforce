import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import PropTypes from 'prop-types'
import { Typography, Grid, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Icon from '~/app/assets/fonts/icon'
import ErrorModal from '~/app/components/Error/index'

const Transition = React.forwardRef((props, ref) => (
  <Slide direction="up" ref={ref} {...props} />
))
const useStyles = makeStyles((theme) => ({
  Modal: {
    borderRadius: 30,
    width: '48.125em',
    position: 'relative',
    overflow: 'initial',
    maxWidth: '100% !important',
    margin: 0,
    [theme.breakpoints.down('xs')]: {
      verticalAlign: 'bottom',
      borderRadius: '30px 30px 0px 0px'
    }
  },
  ModalWithError: {
    borderRadius: 30,
    width: '48.125em',
    marginTop: '14rem',
    position: 'relative',
    overflow: 'initial',
    maxWidth: '100% !important',
    margin: 0,
    [theme.breakpoints.down('xs')]: {
      verticalAlign: 'bottom',
      borderRadius: '30px 30px 0px 0px',
      marginTop: '22rem',
    }
  },
  closeButton: {
    top: 16,
    right: 18,
    width: 31,
    height: 30,
    position: 'absolute',
    background: '#DAE0E6',
    borderRadius: '50%',
    minWidth: 'auto',
    padding: '2px 1.8px 4px 0px',
    fill: '#001C33'
  },
  title: {
    color: '#001C33',
    fontSize: '1.625rem',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: '1em',
    display: 'block',
    [theme.breakpoints.down('sm')]: {
      padding: '0 32px'
    }
  }
}))

const Modal = (props) => {
  const {
    open,
    title,
    subtitle,
    subtitle2,
    children,
    setStatusModal,
    isError,
    errorObj,
    setExitModalStatus,
    closeIcon,
    className,
    ReferToStatus,
    // ReferToLOAStartDate,
    allRef
  } = props
  const classes = useStyles()
  return (
    <>
      <Dialog
        open={open}
        disableBackdropClick
        TransitionComponent={Transition}
        fullWidth
        maxWidth="sm"
        scroll="body"
        onClose={setStatusModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        PaperProps={
          isError
            ? { className: classes.ModalWithError }
            : {
              className: classes.Modal
            }
        }
        className={className}
      >
        {isError ? (
          <ErrorModal
            isError={isError}
            errorObj={errorObj}
            allRef={allRef}
            ReferToStatus={ReferToStatus}
          >
            {/*  <ErrorData /> */}
          </ErrorModal>
        ) : null}
        {(title || subtitle || subtitle2) && (
          <DialogTitle disableTypography>
            <Grid container justify="center">
              <Grid item>
                <Typography
                  align="center"
                  variant="h6"
                  className={classes.title}
                >
                 {title}
                </Typography>
                {(closeIcon)
                && (
                  <Button
                    onClick={setExitModalStatus}
                    size="small"
                    className={classes.closeButton}
                  >
                    <Icon icon="close" viewBox="0 0 22 22" />
                  </Button>
                )}
                <Typography align="center">{subtitle}</Typography>
                <Typography align="center" color="textSecondary">
                  {subtitle2}
                </Typography>
              </Grid>
            </Grid>
          </DialogTitle>
        )}
        <DialogContent dividers>{children}</DialogContent>
      </Dialog>
    </>
  )
}

Modal.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  subtitle2: PropTypes.string,
  children: PropTypes.node.isRequired,
  isError: PropTypes.bool.isRequired,
  setStatusModal: PropTypes.func.isRequired,
  setExitModalStatus: PropTypes.func.isRequired,
  closeIcon: PropTypes.bool,
  className: PropTypes.string,
  errorObj: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]).isRequired,
  ReferToStatus: PropTypes.element
}

Modal.defaultProps = {
  subtitle: '',
  subtitle2: '',
  closeIcon: true,
  ReferToStatus: null
}

export default Modal
