import React from 'react'
import PropTypes from 'prop-types'
import cn from 'classnames'
import { NavLink } from 'react-router-dom'
import './styles.scss'

import Icon from '~/app/components/Icon'

const componentID = 'a-link'

const Link = (props) => {
  const { className, children, href, text, icon } = props
  return (
    <NavLink to={href} className={cn([componentID, className])}>
      {icon && <Icon icon={icon} />}
      {text || children}
    </NavLink>
  )
}

Link.defaultProps = {
  className: '',
  text: undefined,
  icon: undefined,
  children: undefined
}

Link.propTypes = {
  className: PropTypes.string,
  href: PropTypes.string.isRequired,
  text: PropTypes.string,
  children: PropTypes.node,
  icon: PropTypes.string
}

export default Link
