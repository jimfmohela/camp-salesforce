import React from 'react'
import { storiesOf } from '@storybook/react'
import Link from './index'

storiesOf('Components', module).add('Link', () => (
  [
    <div><Link href="#">Link to nowhere</Link></div>,
    <div><Link icon='settings' href="#">Link with Icon</Link></div>
  ]
))
