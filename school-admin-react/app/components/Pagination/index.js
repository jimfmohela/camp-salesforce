import React from 'react'
import { matchPath } from 'react-router'
import Icon from '~/app/assets/fonts/icon'

class Pagination extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      totalCount: 0,
      showGoButton: false,
      totalPage: 0,
      limit: 50,
      pageNumber: 0,
      enteredPage: 1,
      customUrl: props.currentPage,
      tabs: {
        agreements: {
          applications: [
            'Invited',
            'Draft',
            'Application Under Review',
            'Application Incomplete',
            'Application Complete'
          ],
          agreements: ['Certified', 'Partially Funded'],
          cancelled: ['Cancelled']
        },
        students: {
          'in-school': [
            'Full Time',
            'Three Quarter Time',
            'Half Time',
            'Less than Half Time'
          ],
          graduated: ['Graduated']
        },
        programs: {}
      }
    }
  }
  getTabs() {
    if (this.props.agreementsFlag) return this.state.tabs.agreements
    if (this.props.studentsFlag) return this.state.tabs.students
    if (this.props.programsFlag) return this.state.tabs.programs
  }

  loadPagination = async () => {
    let { handlePageChange, location, matchUrl } = this.props
    const filter =
      this.props.match.params.filter || this.props.match.params.category
    const dataTabs = this.getTabs()
    const status = dataTabs[filter]
    // Filters
    const params = new URLSearchParams(location.search)
    const query = params.get('filter')
    // SWITCH THIS TO USE PROPS.MATCH INSTEAD
    const customMatch = matchPath(this.props.location.pathname, {
      path: matchUrl,
      exact: false,
      strict: false
    })
    const customMatchFilter =
      customMatch.params.filter || customMatch.params.category
    // Check if pagination is valid
    let pageNumber = customMatch.params.pageNumber || 0
    if (pageNumber <= 0) {
      this.props.history.replace(
        `/${this.state.customUrl}/${customMatchFilter}/1${location.search}`
      )

      return await this.props.handlePageChange(1, status)
    }

    // Update page number for re-mounts
    if (!this.state.pageNumber) {
      await this.setState({ pageNumber: customMatch.params.pageNumber })
    }
    // fetch record on reload
    if (!this.props.students.isLoaded) {
      return await handlePageChange(customMatch.params.pageNumber, status)
    }
  }
  componentDidMount() {
    if (!this.props.students.isFetching) {
      this.loadPagination()
    }
    //Calualte total page and set total number f records
    const filter = this.props.match.params.filter
    this.setState(
      {
        totalCount: this.props.studentCountData
      },
      () => {
        this.setState({
          totalPage: Math.ceil(
            parseInt(this.state.totalCount) / parseInt(this.state.limit)
          )
        })
      }
    )
    this.setState({ customUrl: this.props.currentPage })
  }

  calculateRange = () => {
    let initialRange =
      parseInt(this.state.limit) * [parseInt(this.state.pageNumber) - 1] + 1
    let finalRange =
      parseInt(initialRange) + parseInt(this.props.students.data.length - 1)
    return (
      <p>
        {initialRange}-{finalRange}
      </p>
    )
  }

  getStatus(urlPath) {
    let lsstring = urlPath
    let splitUrl = ''
    let splitagreements = []
    if (lsstring != '') {
      splitUrl = lsstring.split('=')[1]
    }
    if (splitUrl != '') {
      splitagreements = splitUrl.split(',')
    }
    return {
      filteredData: splitagreements.map((url) => decodeURI(url)),
      filterData: splitUrl
    }
  }

  nextPage = async () => {
    let { handlePageChange, history, location, matchUrl } = this.props
    const filter =
      this.props.match.params.filter || this.props.match.params.category
    const dataTabs = this.getTabs()
    const status = dataTabs[filter]
    const params = new URLSearchParams(location.search)
    const query = location.search
    let pageNumber = this.state.pageNumber
    if (this.state.totalPage > pageNumber) {
      this.setState({ pageNumber: parseInt(pageNumber) + 1 }, () => {
        const customMatch = matchPath(this.props.location.pathname, {
          path: matchUrl,
          exact: false,
          strict: false
        })
        const customMatchFilter =
          customMatch.params.filter || customMatch.params.category
        if (query) {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}${query}`
          )

          handlePageChange(
            this.state.pageNumber,
            this.getStatus(query).filteredData
          )
        } else {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}`
          )
          handlePageChange(this.state.pageNumber, status)
        }
      })
    }
  }

  //Previous button function
  previousPage = async () => {
    let { handlePageChange, history, location, matchUrl } = this.props
    const filter =
      this.props.match.params.filter || this.props.match.params.category
    const dataTabs = this.getTabs()
    const status = dataTabs[filter]
    const params = new URLSearchParams(location.search)
    const query = location.search
    let pageNumber = this.state.pageNumber
    if (pageNumber > 1) {
      this.setState({ pageNumber: parseInt(pageNumber) - 1 }, () => {
        const customMatch = matchPath(this.props.location.pathname, {
          path: matchUrl,
          exact: false,
          strict: false
        })
        const customMatchFilter =
          customMatch.params.filter || customMatch.params.category
        if (query) {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}${query}`
          )
          handlePageChange(
            this.state.pageNumber,
            this.getStatus(query).filteredData
          )
        } else {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}`
          )
          handlePageChange(this.state.pageNumber, status)
        }
      })
    }
  }
  handleJumpToPage = async () => {
    let { handlePageChange, history, location, matchUrl } = this.props
    const filter =
      this.props.match.params.filter || this.props.match.params.category
    const dataTabs = this.getTabs()
    const status = dataTabs[filter]
    const params = new URLSearchParams(location.search)
    const query = location.search
    let pageNumber = this.state.enteredPage
    if (this.state.totalPage >= pageNumber) {
      this.setState({ pageNumber: parseInt(pageNumber) }, () => {
        const customMatch = matchPath(this.props.location.pathname, {
          path: matchUrl,
          exact: false,
          strict: false
        })
        const customMatchFilter =
          customMatch.params.filter || customMatch.params.category
        if (query) {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}${query}`
          )

          handlePageChange(
            this.state.pageNumber,
            this.getStatus(query).filteredData
          )
        } else {
          this.props.history.replace(
            `/${this.state.customUrl}/${customMatchFilter}/${this.state.pageNumber}`
          )
          handlePageChange(this.state.pageNumber, status)
        }
      })
    }
  }
  //Show Go button section
  showGoButtonDiv = () => {
    this.setState({
      tableOpacity: 0.4
    })
    this.setState({
      showGoButton: true
    })
  }
  handlePageEnter = (e) => {
    this.setState({ enteredPage: e.target.value })
  }

  render() {
    return (
      <div className="pagination_section">
        <div className="left_section">
          <p>
            {' '}
            Showing {this.calculateRange()} of {this.state.totalCount}
          </p>
        </div>
        <div className="right_section">
          <a onClick={this.previousPage} className="round_button">
            <Icon icon="arrow-right2" viewBox="0 0 22 22" />
          </a>

          <span>Page</span>
          {!this.state.showGoButton ? (
            <a className="rectangle_button" onClick={this.showGoButtonDiv}>
              {this.state.pageNumber}
            </a>
          ) : this.state.showGoButton ? (
            <div className="direct_page_div">
              <input
                type="text"
                className="direct_page_input"
                ref={this.myRef}
                onChange={this.handlePageEnter}
              />
              <a onClick={this.handleJumpToPage} className="go">
                Go
              </a>
            </div>
          ) : null}

          <span>of {this.state.totalPage}</span>
          <a onClick={this.nextPage} className="round_button">
            <Icon icon="arrow-left2" viewBox="0 0 22 22" />
          </a>
        </div>
      </div>
    )
  }
}

export default Pagination
