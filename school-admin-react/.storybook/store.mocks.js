const mockStore = {
  auth: {
    adminData: {
      schoolID: 'vemo'
    },
    schoolProfileData: {}
  },
  data: {
    dashboardData: {
      "isFetching": false,
      "isProgress": false,
      "draft": 2,
      "invited": 0,
      "cancelled": 2,
      "applicationIncomplete": 4,
      "applicationUnderReview": 2,
      "applicationComplete": 34,
      "certified": 0,
      "partiallyFunded": 2
    },

    "countData": {
      "count": [
        {
          "statusCountList": [
            {
              "status": "Draft",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Application Incomplete",
              "count": 4,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Application Complete",
              "count": 34,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Partially Funded",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Fully Funded",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Payment",
              "count": 3,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Application Under Review",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "Cancelled",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            },
            {
              "status": "School",
              "count": 2,
              "amountDisbursed": 0,
              "amountCertified": 0
            }
          ],
          "programID": "a0n5C000000BO3CQAW",
          "enrollmentType": "Open"
        }
      ]
    }
  }
}

export default mockStore
