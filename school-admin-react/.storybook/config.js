import { configure, addParameters } from '@storybook/react'
import { muiTheme } from 'storybook-addon-material-ui'
import theme from '../app/theme'

import { addDecorator } from '@storybook/react'
import { withKnobs } from '@storybook/addon-knobs'
import { withA11y } from '@storybook/addon-a11y'

import '~/app/scss/baseline.scss'


addDecorator(withKnobs)
addDecorator(withA11y)
addDecorator(muiTheme(theme))

function loadStories() {
  const req = require.context('../app', true, /\.stories\.js$/)
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module)
