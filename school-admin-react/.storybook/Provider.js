import React from 'react'
import { Provider as ReduxProvider } from 'react-redux'
import configureStore from '../app/store'
import { BrowserRouter, Route, Switch, Redirect} from 'react-router-dom'

import mockStore from '~/.storybook/store.mocks'

import { AppContextProvider } from '~/app/hooks/app'

export default function Provider(getStory, opts) {
  const { store: _store } = opts.parameters.options
  const store = configureStore(_store || mockStore)

  return (
    <ReduxProvider store={store}>
      <AppContextProvider>
        <BrowserRouter>
          <Switch>
            <Route
              path="*"
              component={() => {
                return getStory()
              }}
            />
          </Switch>
        </BrowserRouter>
      </AppContextProvider>
    </ReduxProvider>
  )
}
