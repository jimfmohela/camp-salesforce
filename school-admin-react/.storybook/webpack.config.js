const path = require("path");

module.exports = async ({ config, mode }) => {
  let _config = {
    ...config,
    resolve: {
      extensions: [".js", ".jsx", ".scss"],
      alias: { "~": path.join(process.cwd()) }
    }
  };

  _config.module.rules.push({
    test: /\.stories\.jsx?$/,
    loaders: [require.resolve('@storybook/addon-storysource/loader')],
    enforce: 'pre',
  });

  return _config

};