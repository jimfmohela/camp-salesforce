const Path = require('path')
const Webpack = require('webpack')
const Merge = require('webpack-merge')
const GlobImporter = require('node-sass-glob-importer')
const BASE_CONFIG = require('./base.config')
MiniCssExtractPlugin = require('mini-css-extract-plugin')


const options = {
  disableImportOnce: true
}

// Plugins //
const webpackEnv = new Webpack.DefinePlugin({
  'process.env': {
    NODE_ENV: JSON.stringify(process.env.ENV_DEV),
    PUBLIC_URL: JSON.stringify(process.env.PUBLIC_URL_DEV),
    SITE_URL: JSON.stringify(process.env.SITE_URL) + 'schooladmin',
    HOST: JSON.stringify(process.env.HOST)
  }
})

// Loaders //
const cssLoader = {
  loader: 'css-loader',
  options: {
    importLoaders: 2,
    sourceMap: false
  }
}
const sassLoader = {
  loader: 'sass-loader',
  options: {
    importer: GlobImporter(options)
  }
}
const postcssLoader = {
  loader: 'postcss-loader',
  options: {
    config: {
      path: './app/assets/css/postcss.config.js'
    }
  }
}

// Rules //
const cssRule = {
  test: /\.scss$/,
  use: [
    'style-loader',
    cssLoader,
    postcssLoader,
    sassLoader
  ]
}

// const extractCSS = new MiniCssExtractPlugin({
//   filename: 'App.bundle.css'
// })

if (process.env.API_URL !== 'undefined') {
  if (console) {
    // eslint-disable-next-line no-console
    console.info(
      `Client configured to access the API at: ${process.env.API_URL}.`
    )
  }
}

module.exports = Merge(BASE_CONFIG, {
  mode: 'development',
  // devtool: 'inline-source-map',
  output: {
    filename: 'index.js',
    path: Path.join(process.cwd(), process.env.OUTPUT_PATH),
    publicPath: '/',
    // For chunking hot-reloading files
    hotUpdateChunkFilename: 'hot/hot-update.js',
    hotUpdateMainFilename: 'hot/hot-update.json'
  },
  devServer: {
    publicPath: '/',
    contentBase: Path.resolve(__dirname, 'app'),
    historyApiFallback: true,
    port: 3000,
    progress: true,
    hot: true,
    proxy: [
      {
        context: ['/vemo'],
        target: 'https://int-vemo.cs62.force.com/',
        changeOrigin: true,
        secure: true
      },
      {
        context: ['/schooladmin/apexremote'],
        target: 'https://int-vemo.cs62.force.com/schooladmin/apexremote',
        changeOrigin: true,
        secure: false,
        headers: {
          'host': 'int-vemo.cs62.force.com',
          'referrer': 'http://int-vemo.cs62.force.com/schooladmin'
        }
      }
    ]
  },
  module: {
    rules: [cssRule]
  },
  plugins: [
    // extractCSS,
    webpackEnv,
    new Webpack.HotModuleReplacementPlugin(),
    new Webpack.NoEmitOnErrorsPlugin()
  ]
})
