require('dotenv').config()

const HtmlWebpackPlugin = require('html-webpack-plugin')
const Path = require('path')

// Plugins //
const extractHtml = new HtmlWebpackPlugin({
  template: 'app/index.html',
  filename: 'index.html'
})

// Rules //
const jsRules = [
  // {
  //   test: /\.(js|jsx)$/,
  //   enforce: 'pre',
  //   loader: 'eslint-loader',
  //   exclude: /node_modules/,
  //   options: {
  //     emitWarning: true,
  //     configFile: './config/linters/.eslintrc.js'
  //   }
  // },
  {
    test: /\.(js|jsx)$/,
    loader: 'babel-loader',
    exclude: /node_modules/,
    options: {
      cacheDirectory: true,
      configFile: './config/babel/babel.config.js'
    }
  }
]

const svgRules = {
  test: /\.svg$/,
  loader: 'babel-loader!svg-react-loader'
}

const imageRules = {
  test: /\.(png|jpg|gif)$/,
  loader: 'file-loader',
  options: {
    name: '[name]-[hash:16].[ext]',
    publicPath: './assets/static/images',
    outputPath: './assets/staticimages'
  }
}

module.exports = {
  entry: ['./app/index.js', './app/assets/css/app.scss'],
  output: {
    filename: 'App.bundle.js',
    path: Path.join(process.cwd(), process.env.OUTPUT_PATH),
    publicPath: '/',
    // For chunking hot-reloading files
    hotUpdateChunkFilename: 'hot/hot-update.js',
    hotUpdateMainFilename: 'hot/hot-update.json'
  },
  node: {
    fs: 'empty'
  },
  module: {
    rules: [...jsRules, svgRules, imageRules]
  },
  plugins: [extractHtml],
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: { '~': Path.join(process.cwd()) }
  }
}
