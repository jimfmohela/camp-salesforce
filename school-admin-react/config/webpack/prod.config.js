const
  Path                  = require('path'),  
  Webpack               = require('webpack'),
  Merge                 = require('webpack-merge'),
  MiniCssExtractPlugin  = require('mini-css-extract-plugin'),
  //UglifyJsPlugin        = require('uglifyjs-webpack-plugin'),
  GlobImporter          = require('node-sass-glob-importer'),
  BASE_CONFIG           = require('./base.config');
  TerserPlugin          = require('terser-webpack-plugin')

// Plugins //
const 
  webpackEnv = new Webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify(process.env.ENV_PROD),
      'PUBLIC_URL': JSON.stringify(process.env.PUBLIC_URL_PROD),
      'SITE_URL': JSON.stringify(process.env.SITE_URL)
    }
  }),
  /*uglifyJsPluginConfig = new UglifyJsPlugin({
    sourceMap: true,
    parallel: true
  })*/
  //Added this to support es6
  uglifyJsPluginConfig = new TerserPlugin({
    sourceMap: true,
    parallel: true, 
    terserOptions: {
      ecma: 6,
    },
  }),
  extractCSS = new MiniCssExtractPlugin({
    filename: 'App.bundle.css',
  });

// Loaders //
const 
  cssLoader = {
    loader: 'css-loader',
    options: {
      importLoaders: 2
    }
  },
  sassLoader = { 
    loader: 'sass-loader',
    options: { 
      importer: GlobImporter()
    }
  },
  postcssLoader = { 
    loader: 'postcss-loader',
    options: {
      config: {
        path: './app/assets/css/postcss.config.js'
      }
    }
  };

// Rules //
const
  imageRules = {
    test: /\.(png|jpg|gif)$/,
    loader: 'file-loader',
    options: {
      name: '[name]-[hash:16].[ext]',
      publicPath: './assets/static/images',
      outputPath: './assets/staticimages'
    }
  },
  cssRule = { 
    test: /\.scss$/, 
    use: [
      MiniCssExtractPlugin.loader,
      cssLoader,
      postcssLoader,
      sassLoader
    ]
  };

module.exports = Merge(BASE_CONFIG, {
  mode: 'production',
  module: {
    rules: [
      cssRule,
      imageRules
    ]
  },
  optimization: {
    minimizer: [
      uglifyJsPluginConfig  
    ]
  },
  plugins: [
    extractCSS,
    webpackEnv,
    new Webpack.optimize.OccurrenceOrderPlugin(true),
  ]
});
